<?php

namespace Database\Factories;

use App\Facades\AnaOnline;
use App\Model\Tafiq1;
use Illuminate\Database\Eloquent\Factories\Factory;

class Tafiq1Factory extends Factory
{
    protected $model = Tafiq1::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "npa" => AnaOnline::generateRandomNPA(),
            "lokasi" => $this->faker->city,
            "tanggal_tafiq_start" => $this->faker->date(),
            "tanggal_tafiq_end" => $this->faker->date(),
            "kehadiran" => $this->faker->randomNumber(2),
            "keaktifan" => $this->faker->randomNumber(2),
            "makalah" => $this->faker->randomNumber(2),
            "status" => $this->faker->randomElement(AnaOnline::getStatusApproval()),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
