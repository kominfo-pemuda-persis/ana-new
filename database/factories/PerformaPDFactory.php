<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Model\PerformaPD::class, function () {
    return [
    	// di input form label nya "Nomor PD" id="noPD" name="noPD"
    	'kd_pd'=>'PD-1',
    	'kd_pw'=>'',

    	"q1a"=>"Utuh sesuai SK",
		"q2a"=>"Sebagian besar mempunyai",
		"q3a"=>"Sebagian besar memahami",
		"q4a"=>"Membuat secara berkala",
		"q5a"=>"Membuat secara berkala",
		"q6a"=>"Berjalan &gt; tiga kali sebulan",

		"q1b"=>"Jumlah anggota kurang dari 25 orang",
		"q2b"=>"Sering dengan seluruh anggota",
		"q3b"=>"Berjalan &gt; tiga kali sebulan",
		"q4b"=>"Kegiatan rutin &gt; tiga kali sebulan",
		"q5b"=>"50% anggota hadir",
		"q6b"=>"Ada",
		"q7b"=>"75% anggota hadir",
		"q8b"=>"Ada dan selalu mengikuti",
		"q9b"=>"Ada dan selalu mengikuti",
		"q10b"=>"Setiap kegiatan ada publikasi/undangan tertulis",
		"q1c"=>"Lebih dari 10 orang",
		"q2c"=>"Ada",
		"q3c"=>"Semua calon anggota yang tercatat mengikuti program pembinaan",
		            
		"q1d"=>"Sudah",
		"q2d"=>"Sudah",
		"q3d"=>"Sudah",
		"q4d"=>"Memiliki",
		"q5d"=>"Selalu digunakan",
		"q6d"=>"Seluruh surat masuk diarsipkan",
		"q7d"=>"Memiliki",
		"q8d"=>"Selalu digunakan",
		"q9d"=>"Seluruh surat keluar diarsipkan",
		"q10d"=>"Memiliki",
		"q11d"=>"Tidak ada Buku Induk Anggota",
		"q12d"=>"Sudah terisi",
		"q13d"=>"Sesuai",
		"q14d"=>"Belum memiliki",
		"q15d"=>"Sesuai",
		"q16d"=>"Memiliki",
		"q17d"=>"Sesuai",
		"q18d"=>"Sesuai",
		"q19d"=>"Belum memiliki",
		"q20d"=>"Selalu digunakan",
		"q21d"=>"Memiliki",
		"q22d"=>"Belum digunakan",
		"q23d"=>"Belum memiliki",
		"q24d"=>"Selalu digunakan",
		"q25d"=>"Belum memiliki",
		"q26d"=>"Belum memiliki",
		"q27d"=>"Sebagian besar data sudah lengkap diisi",
		"q28d"=>"Sebagian besar data sudah sesuai",

		"q1e"=>"Sesuai",
		"q2e"=>"Ada",
		"q3e"=>"Satu tahun satu kali",
		"q4e"=>"Ya",
		"q5e"=>"Ya",
		"q6e"=>"Ya",
		"q7e"=>"Tidak",
		"q8e"=>"Tidak",
		"q9e"=>"Belum pernah",
		"q10e"=>"Semua PJ hadir",
		"q1f"=>"100% anggota memenuhi",
		"q2f"=>"Ya",
		"q3f"=>"Ya",
		"q4f"=>"Ya",
		"q5f"=>"Ya",
		"q6f"=>"Ya",
		"q7f"=>"Saldo",
        'last_survey' => date(now()),
    ];
});

$factory->afterMaking(App\Model\PW::class, function ($post, $faker) {
    $post->save(factory(App\Model\PerformaPD::class)->make());
});
