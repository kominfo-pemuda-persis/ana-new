<?php
namespace Database\Seeders;

use App\Model\PW;
use Carbon\Carbon;
use Faker\Factory;
use App\Model\Province;
use App\Model\MonografiPw;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MonografiPWSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MonografiPw::query()->delete();

        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $count = 10;

        $dataMonografiPW = [];

        $pw = PW::all('kd_pw')->pluck('kd_pw')->toArray();
        $nama_pw = PW::all('nama_pw')->pluck('nama_pw')->toArray();
        $province = Province::all('id')->pluck('id')->toArray();

        for ($i = 0; $i <= $count; $i++) {

            $dataMonografiPW[] = [
                'kd_pw' => $faker->randomElement($pw),
                'nama_pw' => $faker->randomElement($nama_pw),
                'start_periode' => $faker->dateTime(),
                'end_periode' => $faker->dateTime(),
                'provinsi' => $faker->randomElement($province),
                'latitude' => $faker->randomFloat(),
                'longitude' => $faker->randomFloat(),
                'alamat_utama' => $faker->streetAddress(),
                'alamat_alternatif' => $faker->address(),
                'no_kontak' => $faker->numerify("089######"),
                'email' => $faker->email,
                'luas' => $faker->randomDigitNotNull(),
                'bw_utara' => $faker->city(),
                'bw_selatan' => $faker->city(),
                'bw_timur' => $faker->city(),
                'bw_barat' => $faker->city(),
                'jarak_dari_ibukota_kabupaten' => $faker->randomDigitNotNull(),
                'jarak_dari_ibukota_provinsi' => $faker->randomDigitNotNull(),
                'jarak_dari_ibukota_negara' => $faker->randomDigitNotNull(),
                'foto' => null,
                'created_at' => $date,
                'updated_at' => $date,
            ];

        }

        DB::table('t_monografi_pw')->insert($dataMonografiPW);

    }
}
