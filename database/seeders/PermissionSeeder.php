<?php
namespace Database\Seeders;

use App\Model\MasterPendidikan;
use App\Model\UserLogin;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;


class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->truncatePermissionsTables();

        $rolePermissions = config('permission_seeder.role_permissions');
        $listPendidikan = MasterPendidikan::all('id_tingkat_pendidikan')->pluck('id_tingkat_pendidikan')->toArray();

        $npaNum = 1;
        foreach ($rolePermissions as $rolename => $modules) {

            // Create a new role
            $role = \App\Model\RoleMaster::where('name', $rolename)->first();

            $permissions = [];

            $this->command->info('Creating Role '. strtoupper($rolename));

            // Reading role permission modules
            foreach ($modules as $module => $permissions) {

                foreach ($permissions as $perm) {

                    $permissions[] = \App\Model\PermissionRole::firstOrCreate([
                        'role_id' => $role->id_role,
                        'module' => $module,
                        'permission' => $perm,
                    ])->id;

                    $this->command->info('Creating Permission '.$module.'.'.$perm.' for role '. $rolename);
                }
            }

            $this->command->info("Creating '{$rolename}' user");

            $faker = Factory::create('id_ID');
            $date = Carbon::now()->format('Y-m-d H:i:s');

            // Create anggota data
            $anggota = \App\Model\Anggota::create([
                'id_anggota' => Uuid::uuid4()->getHex(),
                'npa' => '99.00' . sprintf('%02d', $npaNum),
                'nama_lengkap' => $faker->name('male') . ' (' . $rolename . ')',
                'tempat_lahir' => $faker->city,
                'tanggal_lahir' => '1990-12-12',
                'status_merital' => 'Menikah',
                'pekerjaan' => 'Guru',
                'provinsi' => '32',
                'kota' => '3273',
                'kecamatan' => '3273180',
                'desa' => '1101031009',
                'pw' => 'PW-1',
                'pd' => 'PD-1',
                'pc' => 'PC.1',
                'pj' => 'PJ-1',
                'nama_pj' => 'Konoha',
                'gol_darah' => 'O',
                'email' => $faker->email,
                'no_telpon' => '080989999',
                'alamat' => $faker->address,
                'jenis_keanggotaan' => 'Biasa',
                'status_aktif' => 'Aktif',
                'pendidikan_terakhir' => $faker->randomElement($listPendidikan),
                'status_her' => "SUDAH_BAYAR",
                'foto' => 'default.png',
                'id_otonom' => '3',
                'masa_aktif_kta' => $date,
                'reg_date' => $date,
                'last_updated' => $date,
                'created_by' => 'Machine',
                'created_date' => $date,
            ]);

            // Create default user for each role
            $user = UserLogin::create([
                'id_login' => Uuid::uuid4()->getHex(),
                'npa' => '99.00' . sprintf('%02d', $npaNum),
                'password' => '12345',
                'activ_code' => '',
                'email_confirm' => '1',
                'remember_token' => '',
                'status_aktif' => 1,
                'created_at' => $date,
                'updated_at' => $date
            ]);

            $user->attachRole($role);

            $npaNum++;
        }
    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncatePermissionsTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('t_permission_role')->truncate();
        DB::table('t_permission_user')->truncate();
        DB::table('t_role_user')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
