<?php
namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnggotaPendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $id_anggota = DB::table('t_anggota')->first();
        /** Generate fake for 1 members. */
        DB::table('t_anggota_pendidikan')->insert([
            'id_anggota' => $id_anggota->id_anggota,//1,
            'id_master_pendidikan' => 1,
            'instansi' => $faker->sentence,
            'jurusan' => $faker->sentence,
            'tahun_masuk' => 2020,//$faker->date,
            'tahun_keluar' => 2020,//$faker->date,
            'jenis_pendidikan' => 'Formal',
        ]);

        DB::table('t_anggota_pendidikan')->insert([
            'id_anggota' => $id_anggota->id_anggota,//1,
            'id_master_pendidikan' => 2,
            'instansi' => $faker->sentence,
            'jurusan' => $faker->sentence,
            'tahun_masuk' => 2020,//$faker->date,
            'tahun_keluar' => 2020,//$faker->date,
            'jenis_pendidikan' => 'Formal',
        ]);
    }
}
