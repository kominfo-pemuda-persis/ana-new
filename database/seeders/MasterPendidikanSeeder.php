<?php
namespace Database\Seeders;

use App\Model\MasterPendidikan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterPendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contents = [
            'SD',
            'MI',
            'SMP',
            'MTs',
            'SMA',
            'SMK',
            'STM',
            'MA',
            'MLN',
            'D1',
            'D2',
            'D3',
            'D4',
            'S1',
            'S2',
            'S3',
            'KOSONG'
        ];

        MasterPendidikan::query()->delete();

        foreach ($contents as $content) {
            DB::table('t_master_pendidikan')->insert([
                'pendidikan' => $content
            ]);
        }
    }
}
