<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnggotaKeterampilanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $anggota = DB::table('t_anggota')->limit(10)->get();
        $expertise = [
            'Pijat',
            'Memasak',
            'Bela diri',
            'Futsal',
            'Mebeul',
            'Bangunan',
            'Baca',
            'Menulis',
            'Memprogram',
            'MC',
            'Ceramah'
        ];

        /** Generate fake expertise for 100 members. */
//        for ($i=1;$i<=100;$i++) {

        foreach ($anggota as $val){
            DB::table('t_anggota_keterampilan')->insert([
                'id_anggota' => $val->id_anggota,//$i,
                'keterampilan' => $expertise[rand(0, 10)],
            ]);
        }
    }
}
