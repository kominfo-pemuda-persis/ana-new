<?php
namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnggotaPekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $date = Carbon::now()->format('Y-m-d');
        $anggota = DB::table('t_anggota')->limit(10)->get();

        $jobs = [
            'Pengacara',
            'IT',
            'Guru',
            'Dosen',
            'Buruh',
            'Karyawan',
            'Wiraswasta',
            'Penjaga Toko',
            'Pengusaha',
            'Penceramah',
            'Multimedia',
            'Artis',
            'PNS'
        ];

        /** Generate fake jobs for 100 members. */
//        for ($i=1;$i<=100;$i++) {
        foreach ($anggota as $val){
            DB::table('t_anggota_pekerjaan')->insert([
                'id_anggota' => $val->id_anggota,//$i,
                'pekerjaan' => $jobs[rand(0, 12)],
                'keterangan' => $faker->text,
                'alamat' => $faker->address,
                'tahun_mulai' => 2020,//$date,
                'tahun_selesai' => 2020,//$date,
            ]);
        }
    }
}
