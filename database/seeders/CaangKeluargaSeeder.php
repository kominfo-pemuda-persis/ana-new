<?php
namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CaangKeluargaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $relationships = ['Istri', 'Suami', 'Anak'];
        $caang = DB::table('t_caang')->first();

        /** Generate fake family member. */
        for ($i=1;$i<=4;$i++) {

            /** Insert */
            DB::table('t_caang_keluarga')->insert([
                'id_caang' => $caang->id_caang,//1,
                'nama_keluarga' => $faker->name,
                'hubungan' => $relationships[0],
                'jumlah_anak' => $faker->randomNumber(1),
                'alamat' => $faker->address,
            ]);
        }
    }
}
