<?php

namespace Database\Seeders;

use App\Model\PC;
use Faker\Factory;
use App\Model\Bulan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BendaharaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $pc = PC::all();
        // $count = PC::count();

        $tgl = [
            0 => 'Januari',
            1 => 'Februari',
            2 => 'Maret',
            3 => 'April',
            4 => 'Mei',
            5 => 'Juni',
            6 => 'Juli',
            7 => 'Agustus',
            8 => 'September',
            9 => 'Oktober',
            10 => 'November',
            11 => 'Desember'
        ];

        for($i =0; $i < count($tgl); $i++) {
            Bulan::create([
                'nama_bulan' => $tgl[$i]
            ]);
        }
        
        // for ($i=0;$i< 2;$i++) {
        DB::table('t_bendahara')->insert([
            'nama' => 'IWA (Iuran Wajib Anggota)',
            'jumlah' => 240000,
            'status' => 1,
        ]);

        DB::table('t_bendahara')->insert([
            'nama' => 'Muktamar',
            'jumlah' => 50000,
            'status' => 0,
        ]);
        // }
    }
}
