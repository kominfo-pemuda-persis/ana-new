<?php
namespace Database\Seeders;

use App\Model\MasterPendidikan;
use App\Model\PC;
use App\Model\PD;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $listPendidikan = MasterPendidikan::all('id_tingkat_pendidikan')->pluck('id_tingkat_pendidikan')->toArray();

        /** 1. Prepare id pw, pd and pc */
        $level = [
            0 => [32, 3273, 3273180, 1101031009], // Jabar, Kota Bandung, Andir
            1 => [32, 3273, 3273050, 1101031009], // Jabar, Kota Bandung, Astana Anyar
            2 => [32, 3273, 3273020, 1101031009], // Jabar, Kota Bandung, Bab. Ciparay
            3 => [32, 3273, 3273160, 1101031009], // Jabar, Kota Bandung, Batununggal
            4 => [32, 3273, 3273030, 1101031009], // Jabar, Kota Bandung, Bojongloa Kaler
            5 => [31, 3173, 3173040, 1101031009], // DKI, Jakpus, Johar Baru
            6 => [31, 3173, 3173030, 1101031009], // DKI, Jakpus, Senen
            7 => [31, 3174, 3174040, 1101031009], // DKI, Jakbar, Grogol
            8 => [31, 3174, 3174070, 1101031009], // DKI, Jakbar, Cengkareng
            9 => [31, 3174, 3174080, 1101031009], // DKI, Jakbar, Kalideres
        ];

        $pc = PC::all('kd_pc')->pluck('kd_pc')->toArray();
        $pj = ["PJ.1", "PJ.2", "PJ.3", "PJ.4", "PJ.5", "PJ.6", "PJ.7", "PJ.8", "PJ.9", "PJ.10", "PJ.11", "PJ.12", "PJ.13", "PJ.14", "PJ.15"];

        /** 2. Generate other fake member */
        for ($i = 1; $i <= 2000; $i++) {
            // Generate level index
            $levelIndex = $i % 10;

            /** Generate npa */
            $npa = sprintf('%02d', $faker->randomNumber(2)) . '.' . sprintf('%04d', $faker->randomNumber(4));

            /** Random related PW, PD, PC */
            $randomPC = $faker->randomElement($pc);
            $randomPD = PC::where('kd_pc', $randomPC)->first()->kd_pd;
            $randomPW = PD::where('kd_pd', $randomPD)->first()->kd_pw;

            /** Insert */
            DB::table('t_anggota')->insert([
                'id_anggota' => Uuid::uuid4()->getHex(),
                'npa' => $npa,
                'nama_lengkap' => $faker->name('male'),
                'tempat_lahir' => $faker->city,
                'tanggal_lahir' => $faker->dateTimeThisCentury->format('Y-m-d'),
                'status_merital' => $faker->randomElement(['SINGLE', 'MENIKAH', 'DUDA']),
                'pekerjaan' => 'Guru',
                'pw' => $randomPW,
                'pd' => $randomPD,
                'pc' => $randomPC,
                'pj' => $faker->randomElement($pj),
                'nama_pj' => $faker->city,
                'provinsi' => $level[$levelIndex][0],
                'kota' => $level[$levelIndex][1],
                'kecamatan' => $level[$levelIndex][2],
                'desa' => $level[$levelIndex][3],
                'gol_darah' => $faker->randomElement(['A', 'B', 'AB', 'O']),
                'email' => $faker->email,
                'no_telpon' => '080989999',
                'no_telpon2' => '081000000',
                'alamat' => $faker->address,
                'pendidikan_terakhir' => $faker->randomElement($listPendidikan),
                'jenis_keanggotaan' => $faker->randomElement(['BIASA', 'TERSIAR']),
                'status_aktif' => $faker->randomElement(['ACTIVE', 'NON ACTIVE', 'MENINGGAL', 'TIDAK HER-REGISTRASI', 'MUTASI']),
                'foto' => 'default.png',
                'id_otonom' => '3',
                'masa_aktif_kta' => $date,
                'reg_date' => $date,
                'last_updated' => $date,
                'created_by' => 'Machine',
                'created_date' => $date,
            ]);
        }
    }
}
