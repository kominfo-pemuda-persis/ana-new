<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnggotaKeluargaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $relationships = ['Istri', 'Suami', 'Anak'];
        $id_anggota = DB::table('t_anggota')->first();

        /** Generate fake family member. */
        for ($i=1;$i<=4;$i++) {

            /** Insert */
            DB::table('t_anggota_keluarga')->insert([
                'id_anggota' => $id_anggota->id_anggota,//1,
                'nama_keluarga' => $faker->name,
                'hubungan' => $relationships[0],
                'jumlah_anak' => $faker->randomNumber(1),
                'alamat' => $faker->address,
                'id_otonom' => 3
            ]);
        }
    }
}
