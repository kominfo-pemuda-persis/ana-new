<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterOtonomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contents = [
            'Persis', 'Persistri', 'Pemuda Persis', 'Pemudi Persis', 'HIMA Persis', 'HIMI Persis'
        ];

        foreach ($contents as $content) {
            DB::table('t_master_otonom')->insert([
                'nama_otonom' =>  $content
            ]);
        }
    }
}
