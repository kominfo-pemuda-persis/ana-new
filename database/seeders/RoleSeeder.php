<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contents = [
            [
                'id_role' => 1,
                'name' => 'anggota',
                'display_name' => 'Anggota',
                'description' => 'Memiliki NPA',
                'status_aktif' => 1
            ],
            [
                'id_role' => 2,
                'name' => 'superadmin',
                'display_name' => 'Super Admin',
                'description' => 'Memiliki NPA, Tim Pengembang Sistem yang diberi SK Oleh Ketua PP',
                'status_aktif' => 1
            ],
            [
                'id_role' => 3,
                'name' => 'admin_pp',
                'display_name' => 'Admin PP',
                'description' => 'Tasykil Kominfo PP atau yang mendapatkan SK dari Ketua PP',
                'status_aktif' => 1
            ],
            [
                'id_role' => 4,
                'name' => 'tasykil_pp',
                'display_name' => 'Tasykil PP',
                'description' => 'Pimhar PP, Tasykil PP, Tasykil Lembaga Khusus PP',
                'status_aktif' => 1
            ],
            [
                'id_role' => 5,
                'name' => 'admin_pw',
                'display_name' => 'Admin PW',
                'description' => 'Tasykil Kominfo PW atau yang mendapatkan SK dari Ketua PW',
                'status_aktif' => 1
            ],
            [
                'id_role' => 6,
                'name' => 'tasykil_pw',
                'display_name' => 'Tasykil PW',
                'description' => 'Pimhar PW, Tasykil PW, Tasykil Lembaga Khusus PW',
                'status_aktif' => 1
            ],
            [
                'id_role' => 7,
                'name' => 'admin_pd',
                'display_name' => 'Admin PD',
                'description' => 'Tasykil Kominfo PD atau yang mendapatkan SK dari Ketua PD',
                'status_aktif' => 1
            ],
            [
                'id_role' => 8,
                'name' => 'tasykil_pd',
                'display_name' => 'Tasykil PD',
                'description' => 'Pimhar PD, Tasykil PD, Tasykil Lembaga Khusus PD',
                'status_aktif' => 1
            ],
            [
                'id_role' => 9,
                'name' => 'admin_pc',
                'display_name' => 'Admin PC',
                'description' => 'Admin PC atau yang mendapatkan SK dari Ketua PC',
                'status_aktif' => 1
            ],
            [
                'id_role' => 10,
                'name' => 'tasykil_pc',
                'display_name' => 'Tasykil PC',
                'description' => 'Tasykil Kominfo PC atau yang mendapatkan SK dari Ketua PC',
                'status_aktif' => 1
            ],
            [
                'id_role' => 11,
                'name' => 'admin_pj',
                'display_name' => 'Admin PJ',
                'description' => 'Admin Kominfo PJ atau yang mendapatkan SK dari Ketua PJ',
                'status_aktif' => 1
            ]
        ];

        foreach ($contents as $content) {
            DB::table('t_role')->insert($content);
        }
    }
}    