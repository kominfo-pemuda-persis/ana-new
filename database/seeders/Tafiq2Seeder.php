<?php

namespace Database\Seeders;

use App\Model\Anggota;
use App\Model\Tafiq2;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tafiq2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("truncate", 0) === "1"){
            $this->command->info("truncating...");
            DB::table('t_tafiq2')->truncate();
        }
        $faker = Factory::create("id-ID");
        $anggota = Anggota::limit(10)->get()->pluck('npa');
        $this->command->info($anggota);
        Tafiq2::factory(10)->create([
            'npa' => $faker->randomElement($anggota)
        ]);
    }
}
