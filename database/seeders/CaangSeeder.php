<?php
namespace Database\Seeders;

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CaangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("truncate", 0) === "1"){
            $this->command->info("truncating...");
            DB::table('t_caang')->truncate();
        }

        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $pj = ["PJ.1", "PJ.2", "PJ.3", "PJ.4", "PJ.5", "PJ.6", "PJ.7", "PJ.8", "PJ.9", "PJ.10", "PJ.11", "PJ.12", "PJ.13", "PJ.14", "PJ.15"];

        /** 1. Prepare id pw, pd and pc */
        $level = [
            0 => [32, 3273, 3273180, 1101031009], // Jabar, Kota Bandung, Andir
            1 => [32, 3273, 3273050, 1101031009], // Jabar, Kota Bandung, Astana Anyar
            2 => [32, 3273, 3273020, 1101031009], // Jabar, Kota Bandung, Bab. Ciparay
            3 => [32, 3273, 3273160, 1101031009], // Jabar, Kota Bandung, Batununggal
            4 => [32, 3273, 3273030, 1101031009], // Jabar, Kota Bandung, Bojongloa Kaler
            5 => [31, 3173, 3173040, 1101031009], // DKI, Jakpus, Johar Baru
            6 => [31, 3173, 3173030, 1101031009], // DKI, Jakpus, Senen
            7 => [31, 3174, 3174040, 1101031009], // DKI, Jakbar, Grogol
            8 => [31, 3174, 3174070, 1101031009], // DKI, Jakbar, Cengkareng
            9 => [31, 3174, 3174080, 1101031009], // DKI, Jakbar, Kalideres
        ];

        for ($i = 1; $i <= 20; $i++) {
            // Generate level index
            $levelIndex = $faker->randomNumber(1);
            $tanggalMarufStart = $faker->dateTimeThisCentury;
            $tanggalMarufEnd = Carbon::createFromTimeStamp($tanggalMarufStart->getTimestamp())->addDays(3)->format('Y-m-d');
            /** Insert */
            DB::table('t_caang')->insert([
                'nama_lengkap' => $faker->name('male'),
                'tempat_lahir' => $faker->city,
                'gol_darah' => $faker->randomElement(['A', 'B', 'AB', 'O']),
                'tanggal_lahir' => $faker->dateTimeThisCentury->format('Y-m-d'),
                'status_merital' => $faker->randomElement(['SINGLE', 'MENIKAH', 'DUDA']),
                'pekerjaan' => 'Guru',
                'provinsi' => $level[$levelIndex][0],
                'kota' => $level[$levelIndex][1],
                'kecamatan' => $level[$levelIndex][2],
                'desa' => $level[$levelIndex][3],
                'pj' => $faker->randomElement($pj),
                'tempat_maruf' =>  $faker->city,
                'tanggal_maruf_start' => $tanggalMarufStart,
                'tanggal_maruf_end' => $tanggalMarufEnd,
                'nama_pj' => $faker->city,
                'email' => $faker->email,
                'no_telpon' => '080989999',
                'no_telpon2' => '081000000',
                'pendidikan_terakhir' => $faker->randomElement([1,2,3,4]),
                'alamat' => $faker->address,
                'foto' => 'default.png',
                'reg_date' => $date,
                'last_updated' => $date,
                'created_date' => $date,
            ]);
        }
    }
}
