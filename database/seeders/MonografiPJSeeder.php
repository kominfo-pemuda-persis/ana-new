<?php
namespace Database\Seeders;

use App\Model\PC;
use App\Model\PD;
use App\Model\PW;
use Carbon\Carbon;
use Faker\Factory;
use App\Model\Regency;
use App\Model\Village;
use App\Model\District;
use App\Model\Province;
use App\Model\MonografiPj;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MonografiPJSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MonografiPj::query()->delete();

        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $count = 10;

        $dataMonografiPJ = [];

        $pc = PC::all('kd_pc')->pluck('kd_pc')->toArray();
        $pd = PD::all('kd_pd')->pluck('kd_pd')->toArray();
        $pw = PW::all('kd_pw')->pluck('kd_pw')->toArray();
        $province = Province::all('id')->pluck('id')->toArray();
        $regency = Regency::all('id')->pluck('id')->toArray();
        $district = District::all('id')->pluck('id')->toArray();
        $village = Village::select('id')->limit(100)->pluck('id')->toArray();


        for ($i = 0; $i <= $count; $i++) {

            $dataMonografiPJ[] = [
                'kode_pj' => $faker->numerify("PJ-###"),
                'nama_pj' => $faker->numerify("Sample PJ-###"),
                'kd_pc' => $faker->randomElement($pc),
                'kd_pd' => $faker->randomElement($pd),
                'kd_pw' => $faker->randomElement($pw),
                'provinsi' => $faker->randomElement($province),
                'kabupaten' => $faker->randomElement($regency),
                'kecamatan' => $faker->randomElement($district),
                'desa' => $faker->randomElement($village),
                'latitude' => $faker->randomFloat(),
                'longitude' => $faker->randomFloat(),
                'alamat' => $faker->streetAddress(),
                'no_telpon' => $faker->numerify("089######"),
                'email' => $faker->email,
                'luas' => $faker->randomDigitNotNull(),
                'bw_utara' => $faker->city(),
                'bw_selatan' => $faker->city(),
                'bw_timur' => $faker->city(),
                'bw_barat' => $faker->city(),
                'jarak_provinsi' => $faker->randomDigitNotNull(),
                'jarak_kabupaten' => $faker->randomDigitNotNull(),
                'photo' => null,
                'created_at' => $date,
                'updated_at' => $date,
                'created_by' => null,
                'start_periode' => $faker->dateTime(),
                'end_periode' => $faker->dateTime(),

            ];

        }

        DB::table('t_monografi_pj')->insert($dataMonografiPJ);
    }
}
