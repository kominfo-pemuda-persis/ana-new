<?php
namespace Database\Seeders;

use App\Model\Anggota;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Support\Facades\DB;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("truncate", 0) === "1"){
            $this->command->info("truncating...");
            DB::table('t_document')->truncate();
        }
        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $id = Anggota::skip(0)->take(10)->pluck('id_anggota');

        for ($i = 1; $i <= 100; $i++) {
            DB::table('t_document')->insert([
                'id_anggota' => $faker->randomElement($id),
                'path' => 'pdf-test.pdf',
                'filetype' => 'pdf',
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
}
