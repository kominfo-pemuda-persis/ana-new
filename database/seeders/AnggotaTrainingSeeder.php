<?php
namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnggotaTrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $id_anggota = DB::table('t_anggota')->first();

        /** Generate fake for 1 members. */
        DB::table('t_anggota_training')->insert([
            'id_anggota' => $id_anggota->id_anggota,//1,
            'nama_training' => 'TAFIQ I',
            'penyelenggara' => 'Brainmatics',
            'tempat' => $faker->city,
            'tanggal_mulai' => $faker->date,
            'tanggal_selesai' => $faker->date,
            'jenis' => 'Training Pemuda'
        ]);

        DB::table('t_anggota_training')->insert([
            'id_anggota' => $id_anggota->id_anggota,//1,
            'nama_training' => 'TAFIQ I',
            'penyelenggara' => 'Brainmatics',
            'tempat' => $faker->city,
            'tanggal_mulai' => $faker->date,
            'tanggal_selesai' => $faker->date,
            'jenis' => 'Training Pemuda'
        ]);
    }
}
