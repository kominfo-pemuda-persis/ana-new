<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PWBantenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = './database/seeds/sql/bugfix_kd_pd_in_pw_banten.sql';

        DB::unprepared(File::get(base_path($path)));
    }
}
