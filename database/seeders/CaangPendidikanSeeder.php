<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CaangPendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $caang = DB::table('t_caang')->first();
        DB::table('t_caang_pendidikan')->insert([
            'id_caang' => $caang->id_caang,//1,
            'id_pendidikan' => $faker->randomElement([1,2,3,4]),
            'id_master_pendidikan' => $faker->randomElement([1,2,3,4]),
            'instansi' => $faker->sentence,
            'jurusan' => $faker->sentence,
            'jenis_pendidikan' => $faker->randomElement(['Formal', 'Non Formal']),
            'tahun_masuk' => $faker->year(),
            'tahun_keluar' => $faker->year()
        ]);
    }
}
