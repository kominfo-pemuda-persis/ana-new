<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class LoginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::now()->format('Y-m-d H:i:s');

        $contents = [
            [
                'npa' => '11.2002',
                'password' => Hash::make('12345'),
                'id_role' => 2,
                'activ_code' => '',
                'email_confirm' => '1', 
                'remember_token' => '', 
                'status_aktif' => 1, 
                'created_at' => $date, 
                'updated_at' => $date 
            ],  
            [
                'npa' => '03.1042',
                'password' => Hash::make('12345'),
                'id_role' => 1,
                'activ_code' => '',
                'email_confirm' => '1', 
                'remember_token' => '', 
                'status_aktif' => 1, 
                'created_at' => $date, 
                'updated_at' => $date 
            ]
        ];
        
        foreach ($contents as $content) {
            DB::table('t_login')->insert($content);
        }
    }
}
