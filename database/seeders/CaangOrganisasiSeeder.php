<?php
namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CaangOrganisasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $caang = DB::table('t_caang')->first();
        DB::table('t_caang_organisasi')->insert([
            'id_caang' => $caang->id_caang,
            'id_organisasi' => 1,
            'nama_organisasi' => $faker->company,
            'jabatan' => $faker->jobTitle,
            'tingkat' => $faker->jobTitle,
            'tahun_mulai' => $faker->year(),
            'tahun_selesai' => $faker->year()
        ]);
    }
}
