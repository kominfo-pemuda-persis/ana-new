<?php
namespace Database\Seeders;

use App\Model\Tafiq1;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory;
use Illuminate\Support\Facades\DB;

class EsyahadahTafiq1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("truncate", 0) === "1"){
            $this->command->info("truncating...");
            DB::table('t_esyahadah_tafiq1')->truncate();
        }
        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $id = Tafiq1::skip(0)->take(10)->pluck('id');

        for ($i = 1; $i <= 100; $i++) {
            DB::table('t_esyahadah_tafiq1')->insert([
                'id_tafiq1' => $faker->randomElement($id),
                'tahun' => $faker->year(),
                'bulan' => $faker->month(),
                'no_urut' => $faker->randomNumber(4),
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
}
