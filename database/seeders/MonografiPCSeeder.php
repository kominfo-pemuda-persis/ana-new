<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory;
use Carbon\Carbon;
use App\Model\PC;
use App\Model\PD;
use App\Model\PW;
use App\Model\Province;
use App\Model\Regency;
use App\Model\District;
use App\Model\MonografiPc;

class MonografiPCSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MonografiPc::query()->delete();

        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $count = 10;

        $dataMonografiPC = [];

        $pw = PW::all('kd_pw')->pluck('kd_pw')->toArray();
        $pc = PC::all('kd_pc')->pluck('kd_pc')->toArray();
        $pd = PD::all('kd_pd')->pluck('kd_pd')->toArray();
        $province = Province::all('id')->pluck('id')->toArray();
        $regency = Regency::all('id')->pluck('id')->toArray();
        $district = District::all('id')->pluck('id')->toArray();


        for ($i = 0; $i <= $count; $i++) {

            $dataMonografiPC[] = [
                'kd_pc' => $faker->randomElement($pc),
                'kd_pd' => $faker->randomElement($pd),
                'kd_pw' => $faker->randomElement($pw),
                'provinsi' => $faker->randomElement($province),
                'kota' => $faker->randomElement($regency),
                'kecamatan' => $faker->randomElement($district),
                'latitude' => $faker->randomFloat(),
                'longitude' => $faker->randomFloat(),
                'alamat_utama' => $faker->streetAddress(),
                'alamat_alternatif' => $faker->address(),
                'no_kontak' => $faker->numerify("089######"),
                'email' => $faker->email,
                'luas' => $faker->randomDigitNotNull(),
                'bw_utara' => $faker->city(),
                'bw_selatan' => $faker->city(),
                'bw_timur' => $faker->city(),
                'bw_barat' => $faker->city(),
                'jarak_dari_ibukota_kabupaten' => $faker->randomDigitNotNull(),
                'jarak_dari_ibukota_provinsi' => $faker->randomDigitNotNull(),
                'jarak_dari_ibukota_negara' => $faker->randomDigitNotNull(),
                'foto' => null,
                'created_at' => $date,
                'updated_at' => $date,
                'start_periode' => $faker->dateTime(),
                'end_periode' => $faker->dateTime(),

            ];

        }

        DB::table('t_monografi_pc')->insert($dataMonografiPC);
    }
}
