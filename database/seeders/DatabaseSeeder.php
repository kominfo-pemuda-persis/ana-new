<?php
namespace Database\Seeders;

namespace Database\Seeders;

use App\Model\Bendahara;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
//            LoginSeeder::class,
            WilayahProvinsiSeeder::class,
            WilayahKecamatanSeeder::class,
            WilayahKabupatenSeeder::class,
            WilayahDesaSeeder::class,
            ListPCSeeder::class,
            ListPDSeeder::class,
            ListPWSeeder::class,
            MasterJamiyyahSeeder::class,
            MasterOtonomSeeder::class,
            MasterPendidikanSeeder::class,
            MasterTrainingSeeder::class,
            RoleSeeder::class,
            AnggotaSeeder::class,
            AnggotaKeluargaSeeder::class,
            AnggotaKeterampilanSeeder::class,
            AnggotaPekerjaanSeeder::class,
            AnggotaPendidikanSeeder::class,
            AnggotaTrainingSeeder::class,
            MonografiPCSeeder::class,
            MonografiPJSeeder::class,
            MonografiPDSeeder::class,
            MonografiPWSeeder::class,
            PerformaPcSeeder::class,
            PerformaPdSeeder::class,
            PerformaPwSeeder::class,
            PerformaPjSeeder::class,
            KejamiyyahanPCSeeder::class,
            KejamiyyahanPJSeeder::class,
            PermissionSeeder::class,
            HerRegistrasiSeeder::class,
            RekapCaangSeeder::class,
            CaangSeeder::class,
            CaangKeluargaSeeder::class,
            CaangOrganisasiSeeder::class,
            CaangPendidikanSeeder::class,
            DocumentSeeder::class,
            Tafiq1Seeder::class,
            Tafiq2Seeder::class,
            Tafiq3Seeder::class,
            BendaharaSeeder::class
        ]);
    }
}
