<?php
namespace Database\Seeders;

use App\Model\Anggota;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Model\Registration;

class RekapCaangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("truncate", 0) === "1"){
            $this->command->info("truncating...");
            DB::table('t_rekap_caang')->truncate();
            DB::table('t_rekap_caang_file')->truncate();
        }

        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $npa = Anggota::skip(0)->take(10)->pluck('npa');

        /** 2. Generate other fake member */
        for ($i = 1; $i <= 10; $i++) {
            /** Insert */
            DB::table('t_rekap_caang')->insert([
                'npa' => $faker->randomElement($npa),
                'name' => $faker->name('male'),
                'email' => $faker->email,
                'phone_number' => '080989999',
                'jumlah' => $faker->randomNumber(2),
                'path_excel' => 'PC148_MARGAASIH.xlsx',
                'path_folder' => 'PC148_MARGAASIH',
                'path_image' => 'test.png',
                'status' => $faker->randomElement(['APPROVED', 'REJECTED', 'SUBMITTED']),
                'created_at' => $date,
                'tanggal_maruf' => $date,
                'updated_at' => $date
            ]);
        }

        $reg = Registration::skip(0)->take(10)->pluck('id');

        for ($i = 1; $i <= 10; $i++) {
            /** Insert */
            DB::table('t_rekap_caang_file')->insert([
                'rekap_caang_id' => $faker->randomElement($reg),
                'path_folder' => 'PC148_MARGAASIH',
                'path' => 'test.png',
                'filetype' => $faker->randomElement(["image", "excel"]),
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
}
