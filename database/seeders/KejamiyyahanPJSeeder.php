<?php
namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Model\MonografiPj;
use App\Model\KejamiyyahanPj;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KejamiyyahanPJSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KejamiyyahanPj::query()->truncate();

        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $dataKejamiyyahanPJ = [];
        $hari = ['Ahad', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];

        $monografi = MonografiPj::select(
            'id', 'kode_pj', 'nama_pj','kd_pc', 'kd_pd', 'kd_pw',
            'provinsi', 'kabupaten', 'kecamatan', 'desa'
        )->get();

        foreach ($monografi as $value) {
            $dataKejamiyyahanPJ[] = [
                'kode_pj' => $value->kode_pj,
                'nama_pj' => $value->nama_pj,
                'kd_monografi_pj' => $value->id,
                'kd_pc' => $value->kd_pc,
                'kd_pd' => $value->kd_pd,
                'kd_pw' => $value->kd_pw,
                'provinsi' => $value->provinsi,
                'kabupaten' => $value->kabupaten,
                'kecamatan' => $value->kecamatan,
                'desa' => $value->desa,
                'ketua' => $faker->name(),
                'sekretaris' => $faker->name(),
                'bendahara' => $faker->name(),
                'hari_ngantor' => $hari[$faker->numberBetween($min = 0, $max = (count($hari) - 1))],
                'waktu_ngantor' => $faker->dateTimeThisCentury->format('h:i'),
                'musjam_terakhir_masehi' => $faker->dateTimeBetween('-2 years'),
                'musjam_terakhir_hijriyyah' => $faker->numerify("1440-09-25"),
                'anggota_biasa' => $faker->randomDigitNotNull(),
                'anggota_luar_biasa' => $faker->randomDigitNotNull(),
                'tidak_herReg' => $faker->randomDigitNotNull(),
                'mutasi_persis' => $faker->randomDigitNotNull(),
                'mutasi_tempat' => $faker->randomDigitNotNull(),
                'mengundurkan_diri' => $faker->randomDigitNotNull(),
                'meninggal_dunia' => $faker->randomDigitNotNull(),
                'created_at' => $date,
                'updated_at' => $date,
                'deleted_at' => $date
            ];

        }

        DB::table('t_kejamiyyahan_pj')->insert($dataKejamiyyahanPJ);
    }
}
