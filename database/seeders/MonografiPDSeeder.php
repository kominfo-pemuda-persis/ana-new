<?php
namespace Database\Seeders;

use App\Model\PD;
use App\Model\PW;
use Carbon\Carbon;
use Faker\Factory;
use App\Model\Regency;
use App\Model\Province;
use App\Model\MonografiPd;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MonografiPDSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MonografiPd::query()->delete();

        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $count = 10;

        $dataMonografiPD = [];

        $pw = PW::all('kd_pw')->pluck('kd_pw')->toArray();
        $pd = PD::all('kd_pd')->pluck('kd_pd')->toArray();
        $nama_pd = PD::all('nama_pd')->pluck('nama_pd')->toArray();
        $province = Province::all('id')->pluck('id')->toArray();
        $regency = Regency::all('id')->pluck('id')->toArray();

        for ($i = 0; $i <= $count; $i++) {

            $dataMonografiPD[] = [
                'kd_pd' => $faker->randomElement($pd),
                'nama_pd' => $faker->randomElement($nama_pd),
                'start_periode' => $faker->dateTime(),
                'end_periode' => $faker->dateTime(),
                'kd_pw' => $faker->randomElement($pw),
                'provinsi' => $faker->randomElement($province),
                'kota' => $faker->randomElement($regency),
                'latitude' => $faker->randomFloat(),
                'longitude' => $faker->randomFloat(),
                'alamat_utama' => $faker->streetAddress(),
                'alamat_alternatif' => $faker->streetAddress(),
                'no_kontak' => $faker->numerify("089######"),
                'email' => $faker->email,
                'luas' => $faker->randomDigitNotNull(),
                'bw_utara' => $faker->city(),
                'bw_selatan' => $faker->city(),
                'bw_timur' => $faker->city(),
                'bw_barat' => $faker->city(),
                'jarak_dari_ibukota_kabupaten' => $faker->randomDigitNotNull(),
                'jarak_dari_ibukota_provinsi' => $faker->randomDigitNotNull(),
                'jarak_dari_ibukota_negara' => $faker->randomDigitNotNull(),
                'foto' => null,
                'created_at' => $date,
                'updated_at' => $date
            ];

        }

        DB::table('t_monografi_pd')->insert($dataMonografiPD);

    }
}
