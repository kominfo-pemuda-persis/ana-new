<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Model\PerformaPW;
use Faker\Factory;
use Carbon\Carbon;
use App\Model\PW;
use App\Model\Province;
use Illuminate\Support\Facades\DB;


class PerformaPwSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            PerformaPW::query()->delete();

            $faker = Factory::create('id_ID');
            $date = Carbon::now()->format('Y-m-d H:i:s');
            $count = 5;

            $dataPerformaPw = [];

            $pw = PW::all('kd_pw')->pluck('kd_pw')->toArray();
            $province = Province::all('id')->pluck('id')->toArray();

            $q1a = [
                "Utuh sesuai SK",
                "70'%' sesuai SK",
                "50'%' sesuai SK",
                "25'%' sesuai SK",
                "25'%' sesuai SK"
            ];

            $q2a = [
                "Semua mempunyai",
                "Sebagian besar mempunyai",
                "Sebagian besar tidak mempunyai",
                "Semua tidak mempunyai"
            ];

            $q3a = [
                "Semua memahami",
                "Sebagian besar memahami",
                "Sebagian besar tidak memahami",
                "Semua tidak memahami"
            ];

            $q4a = [
                "Membuat secara berkala",
                "Pernah membuat",
                "Tidak pernah"
            ];

            $q5a = [
                "Membuat secara berkala",
                "Pernah membuat",
                "Tidak pernah"
            ];

            $q6a = [
                "Berjalan > tiga kali sebulan",
                "Berjalan < tiga kali sebulan",
                "Tidak berjalan"
            ];

            $q1b = [
                "Jumlah anggota > 25 orang",
                "Jumlah anggota kurang dari 25 orang"
            ];

            $q2b = [
                "Sering dengan seluruh anggota",
                "Sering dengan sebagian besar anggota",
                "Sering dengan sebagian kecil anggota",
                "Jarang",
                "Tidak pernah"
            ];

            $q3b = [
                "Berjalan > tiga kali sebulan",
                "Berjalan < tiga kali sebulan",
                "Tidak berjalan"
            ];

            $q4b = [
                "Kegiatan rutin > tiga kali sebulan",
                "Kegiatan rutin jamaah kurang dari tiga kali sebulan",
                "Tidak ada"
            ];

            $q5b = [
                "100% anggota hadir",
                "75% anggota hadir",
                "50% anggota hadir",
                "Kurang dari 50% anggota hadir"
            ];

            $q6b = [
                "Ada",
                "Tidak"
            ];

            $q7b = [
                "100% anggota hadir",
                "75% anggota hadir",
                "50% anggota hadir",
                "Kurang dari 50% anggota hadir"
            ];

            $q8b = [
                "Ada dan selalu mengikuti",
                "Ada dan sering mengikuti",
                "Ada dan jarang mengikuti",
                "Tidak ada"
            ];

            $q9b = [
                "Ada dan selalu mengikuti",
                "Ada dan sering mengikuti",
                "Ada dan jarang mengikuti",
                "Tidak ada"
            ];

            $q10b = [
                "Setiap kegiatan ada publikasi",
                "Sebagian besar kegiatan ada publikasi",
                "Tidak ada"
            ];

            $q1c = [
                "Lebih dari 10 orang",
                "Kurang dari 10 orang",
                "Tidak ada"
            ];

            $q2c = [
                "Ada",
                "Tidak ada"
            ];

            $q3c = [
                "Semua calon anggota yang tercatat mengikuti program pembinaan",
                "Sebagian besar calon  anggota yang tercatat mengikuti program pembinaan",
                "Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan",
                "Tidak ada calon anggota yang mengikuti program pembinaan"
            ];

            $q1d = [
                "Semua calon anggota yang tercatat mengikuti program pembinaan",
                "Sebagian besar calon  anggota yang tercatat mengikuti program pembinaan",
                "Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan",
                "Tidak ada calon anggota yang mengikuti program pembinaan"
            ];

            $q2d = [
                "Sudah",
                "Sebagian besar sudah",
                "Sebagian besar belum",
                "Belum"
            ];

            $q3d = [
                "Sudah",
                "Belum"
            ];

            $q4d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q5d = [
                "Selalu digunakan",
                "Sering digunakan",
                "Jarang digunakan",
                "Belum digunakan",
                "Tidak ada Buku Agenda Surat Masuk"
            ];

            $q6d = [
                "Seluruh surat masuk diarsipkan",
                "Sebagian besar surat masuk diarsipkan",
                "Sebagian besar surat masuk tidak diarsipkan",
                "Surat-surat masuk tidak diarsipkan"
            ];

            $q7d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q8d = [
                "Selalu digunakan",
                "Sering digunakan",
                "Jarang digunakan",
                "Belum digunakan",
                "Tidak ada Buku Agenda Surat Keluar"
            ];

            $q9d = [
                "Seluruh surat keluar diarsipkan",
                "Sebagian besar surat keluar diarsipkan",
                "Sebagian besar surat keluar tidak diarsipkan",
                "Surat-surat keluar tidak diarsipkan"
            ];

            $q10d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q11d = [
                "Sudah digunakan",
                "Belum digunakan",
                "Tidak ada Buku Induk Anggota"
            ];

            $q12d = [
                "Sebagian besar data sudah diisi",
                "Sudah terisi",
                "Sebagian besar data belum diisi",
                "Belum terisi",
                "Tidak ada Buku Induk Anggota"
            ];

            $q13d = [
                "Sesuai",
                "Sebagian besar sesuai",
                "Sebagian besar tidak sesuai",
                "Tidak sesuai",
                "Tidak ada Buku Induk Anggota"
            ];

            $q14d = [
                "Belum memiliki",
                "Memiliki"
            ];

            $q15d = [
                "Sesuai",
                "Tidak sesuai"
            ];

            $q16d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q17d = [
                "Sesuai",
                "Tidak sesuai"
            ];

            $q18d = [
                "Sesuai",
                "Tidak sesuai"
            ];

            $q19d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q20d = [
                "Selalu digunakan",
                "Sering digunakan",
                "Jarang digunakan",
                "Belum digunakan",
                "Tidak ada Buku Agenda Kegiatan"
            ];

            $q21d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q22d = [
                "Selalu digunakan",
                "Sering digunakan",
                "Jarang digunakan",
                "Belum digunakan",
                "Tidak ada Buku Daftar Hadir Kegiatan"
            ];

            $q23d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q24d = [
                "Selalu digunakan",
                "Sering digunakan",
                "Jarang digunakan",
                "Belum digunakan",
                "Tidak ada Buku Notulen"
            ];

            $q25d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q26d = [
                "Memiliki",
                "Belum memiliki"
            ];

            $q27d = [
                "Sudah",
                "Sebagian besar data sudah lengkap diisi",
                "Sebagian besar data belum lengkap diisi",
                "Belum",
                "Tidak ada Buku Inventaris"
            ];

            $q28d = [
                "Sudah",
                "Sebagian besar data sudah sesuai",
                "Sebagian besar data belum sesuai",
                "Belum",
                "Tidak ada Buku Inventaris"
            ];

            $q1e = [
                "Sesuai",
                "Tidak sesuai"
            ];

            $q2e = [
                "Ada",
                "Tidak ada"
            ];

            $q3e = [
                "Satu tahun satu kali",
                "Satu kali",
                "Belum pernah"
            ];

            $q4e = [
                "Ya",
                "Tidak"
            ];

            $q5e = [
                "Ya",
                "Tidak"
            ];

            $q6e = [
                "Ya",
                "Tidak"
            ];

            $q7e = [
                "Ya",
                "Tidak"
            ];

            $q8e = [
                "Ya",
                "Tidak"
            ];

            $q9e = [
                "> 6 kali setahun",
                "Antara 3 – 5 kali setahun",
                "Kurang dari 3 kali setahun",
                "Belum pernah"
            ];

            $q10e = [
                "Semua PJ hadir",
                "75% PJ hadir",
                "50% PJ hadir",
                "Kurang dari 50% PJ hadir"
            ];

            $q1f = [
                "100% anggota memenuhi",
                "75% anggota memenuhi",
                "50% anggota memenuhi",
                "Kurang dari 50% anggota memenuhi"
            ];

            $q2f = [
                "Ya",
                "Tidak"
            ];

            $q3f = [
                "Ya",
                "Tidak"
            ];

            $q4f = [
                "Ya",
                "Tidak"
            ];

            $q5f = [
                "Ya",
                "Tidak"
            ];

            $q6f = [
                "Ya",
                "Tidak"
            ];

            $q7f = [
                "Saldo",
                "Nol",
                "Defisit",
            ];


            for ($i = 0; $i <= $count; $i++) {

                $dataPerformaPw[] = [
                    'kd_pw' => $faker->randomElement($pw),
                    'provinsi' => $faker->randomElement($province),
                    'alamat' => $faker->address,
                    'ketua_pw' => $faker->name,
                    'start_periode' => $faker->dateTime(),
                    'end_periode' => $faker->dateTime(),
                    'no_hp' => $faker->numerify("089######"),
                    'q1a' => $faker->randomElement($q1a),
                    'q2a' => $faker->randomElement($q2a),
                    'q3a' => $faker->randomElement($q3a),
                    'q4a' => $faker->randomElement($q4a),
                    'q5a' => $faker->randomElement($q5a),
                    'q6a' => $faker->randomElement($q6a),
                    'q1b' => $faker->randomElement($q1b),
                    'q2b' => $faker->randomElement($q2b),
                    'q3b' => $faker->randomElement($q3b),
                    'q4b' => $faker->randomElement($q4b),
                    'q5b' => $faker->randomElement($q5b),
                    'q6b' => $faker->randomElement($q6b),
                    'q7b' => $faker->randomElement($q7b),
                    'q8b' => $faker->randomElement($q8b),
                    'q9b' => $faker->randomElement($q9b),
                    'q10b' => $faker->randomElement($q10b),
                    'q1c' => $faker->randomElement($q1c),
                    'q2c' => $faker->randomElement($q2c),
                    'q3c' => $faker->randomElement($q3c),
                    'q1d' => $faker->randomElement($q1d),
                    'q2d' => $faker->randomElement($q2d),
                    'q3d' => $faker->randomElement($q3d),
                    'q4d' => $faker->randomElement($q4d),
                    'q5d' => $faker->randomElement($q5d),
                    'q6d' => $faker->randomElement($q6d),
                    'q7d' => $faker->randomElement($q7d),
                    'q8d' => $faker->randomElement($q8d),
                    'q9d' => $faker->randomElement($q9d),
                    'q10d' => $faker->randomElement($q10d),
                    'q11d' => $faker->randomElement($q11d),
                    'q12d' => $faker->randomElement($q12d),
                    'q13d' => $faker->randomElement($q13d),
                    'q14d' => $faker->randomElement($q14d),
                    'q15d' => $faker->randomElement($q15d),
                    'q16d' => $faker->randomElement($q16d),
                    'q17d' => $faker->randomElement($q17d),
                    'q18d' => $faker->randomElement($q18d),
                    'q19d' => $faker->randomElement($q19d),
                    'q20d' => $faker->randomElement($q20d),
                    'q21d' => $faker->randomElement($q21d),
                    'q22d' => $faker->randomElement($q22d),
                    'q23d' => $faker->randomElement($q23d),
                    'q24d' => $faker->randomElement($q24d),
                    'q25d' => $faker->randomElement($q25d),
                    'q26d' => $faker->randomElement($q26d),
                    'q27d' => $faker->randomElement($q27d),
                    'q28d' => $faker->randomElement($q28d),
                    'q1e' => $faker->randomElement($q1e),
                    'q2e' => $faker->randomElement($q2e),
                    'q3e' => $faker->randomElement($q3e),
                    'q4e' => $faker->randomElement($q4e),
                    'q5e' => $faker->randomElement($q5e),
                    'q6e' => $faker->randomElement($q6e),
                    'q7e' => $faker->randomElement($q7e),
                    'q8e' => $faker->randomElement($q8e),
                    'q9e' => $faker->randomElement($q9e),
                    'q10e' => $faker->randomElement($q10e),
                    'q1f' => $faker->randomElement($q1f),
                    'q2f' => $faker->randomElement($q2f),
                    'q3f' => $faker->randomElement($q3f),
                    'q4f' => $faker->randomElement($q4f),
                    'q5f' => $faker->randomElement($q5f),
                    'q6f' => $faker->randomElement($q6f),
                    'q7f' => $faker->randomElement($q7f),
                    'last_survey' => $date,
                    'created_at' => $date,
                    'updated_at' => $date,
                ];

            }

            DB::table('t_performa_pw')->insert($dataPerformaPw);

        }

    }
}
