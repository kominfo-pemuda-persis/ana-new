<?php
namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use App\Model\MonografiPc;
use App\Model\KejamiyyahanPC;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KejamiyyahanPCSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KejamiyyahanPC::query()->truncate();

        $faker = Factory::create('id_ID');
        $date = Carbon::now()->format('Y-m-d H:i:s');
        $dataKejamiyyahanPC = [];
        $hari = ['Ahad', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'];

        $monografi = MonografiPc::select(
            'id', 'kd_pc', 'kd_pd', 'kd_pw',
            'provinsi', 'kota', 'kecamatan'
        )->get();

        foreach ($monografi as $value) {
            $dataKejamiyyahanPC[] = [
                'kd_monografi_pc' => $value->id,
                'kd_pc' => $value->kd_pc,
                'kd_pd' => $value->kd_pd,
                'kd_pw' => $value->kd_pw,
                'provinsi' => $value->provinsi,
                'kota' => $value->kota,
                'kecamatan' => $value->kecamatan,
                'ketua' => $faker->name(),
                'wkl_ketua' => $faker->name(),
                'sekretaris' => $faker->name(),
                'wkl_sekretaris' => $faker->name(),
                'bendahara' => $faker->name(),
                'wkl_bendahara' => $faker->name(),
                'bid_jamiyyah' => $faker->name(),
                'wkl_bid_jamiyyah' => $faker->name(),
                'bid_kaderisasi' => $faker->name(),
                'wkl_bid_kaderisasi' => $faker->name(),
                'bid_administrasi' => $faker->name(),
                'wkl_bid_administrasi' => $faker->name(),
                'bid_pendidikan' => $faker->name(),
                'wkl_bid_pendidikan' => $faker->name(),
                'bid_dakwah' => $faker->name(),
                'wkl_bid_dakwah' => $faker->name(),
                'bid_humas_publikasi' => $faker->name(),
                'wkl_bid_humas_publikasi' => $faker->name(),
                'bid_hal' => $faker->name(),
                'wkl_bid_hal' => $faker->name(),
                'bid_or_seni' => $faker->name(),
                'wkl_bid_or_seni' => $faker->name(),
                'bid_sosial' => $faker->name(),
                'wkl_bid_sosial' => $faker->name(),
                'bid_ekonomi' => $faker->name(),
                'wkl_bid_ekonomi' => $faker->name(),
                'penasehat1' => $faker->name(),
                'penasehat2' => $faker->name(),
                'penasehat3' => $faker->name(),
                'penasehat4' => $faker->name(),
                'pembantu_umum1' => $faker->name(),
                'pembantu_umum2' => $faker->name(),
                'pembantu_umum3' => $faker->name(),
                'hari' => $hari[$faker->numberBetween($min = 0, $max = (count($hari) - 1))],
                'pukul' => $faker->dateTimeThisCentury->format('h:i'),
                'musycab_terakhir_m' => $faker->dateTimeBetween('-2 years'),
                'musycab_terakhir_h' => $faker->numerify("1440-09-25"),
                'anggota_biasa' => $faker->randomDigitNotNull(),
                'anggota_tersiar' => $faker->randomDigitNotNull(),
                'anggota_istimewa' => $faker->randomDigitNotNull(),
                'tdk_her' => $faker->randomDigitNotNull(),
                'mutasi_ke_persis' => $faker->randomDigitNotNull(),
                'mutasi_tempat' => $faker->randomDigitNotNull(),
                'mengundurkan_diri' => $faker->randomDigitNotNull(),
                'meninggal_dunia' => $faker->randomDigitNotNull(),
                'biasa' => $faker->randomDigitNotNull(),
                'istimewa' => $faker->randomDigitNotNull(),
                'tersiar' => $faker->randomDigitNotNull(),
                'created_at' => $date,
                'updated_at' => $date,
            ];

        }

        DB::table('t_kejamiyyahan_pc')->insert($dataKejamiyyahanPC);
    }
}
