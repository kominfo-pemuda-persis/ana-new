INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-1', 'PW-1', 'Kota Bandung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-2', 'PW-1', 'Kabupaten Bandung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-3', 'PW-1', 'Kabupaten Cianjur', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-4', 'PW-1', 'Kodya Bogor', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-5', 'PW-1', 'Kabupaten Bogor', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-6', 'PW-1', 'Kabupaten Purwakarta', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-7', 'PW-1', 'Kabupaten Subang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-8', 'PW-1', 'Kabupaten Garut', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-9', 'PW-1', 'Kabupaten Tasikmalaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-10', 'PW-1', 'Kabupaten Ciamis', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-11', 'PW-2', 'Jakarta Pusat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-12', 'PW-2', 'Jakarta Barat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-13', 'PW-2', 'Jakarta Timur', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-14', 'PW-2', 'Jakarta Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-15', 'PW-1', 'Kabupaten Majalengka', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-16', 'PW-1', 'Kabupaten Sukabumi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-17', 'PW-XYZ', 'Bima (NTB)', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-18', 'PW-1', 'Kota Cimahi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-19', 'PW-4', 'Kabupaten Gorontalo', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-20', 'PW-1', 'Kabupaten Sumedang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-21', 'PW-1', 'Kota Tasikmalaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-22', 'PW-XYZ', 'Bone - Bolango', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at) VALUES ('PD-23', 'PW-XYZ', 'Pohuhato ', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-24', 'PW-XYZ', 'Dungingi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-25', 'PW-4', 'Kota Gorontalo', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-26', 'PW-1', 'Kabupaten Bandung Barat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-27', 'PW-3', 'Kabupaten Pamekasan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-28', 'PW-1', 'Kota Banjar', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-29', 'PW-XYZ', 'Kabupaten Lampung Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-30', 'PW-XYZ', 'Kabupaten Lampung Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-31', 'PW-2', 'Jakarta Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-32', 'PW-3', 'Kabupaten Sumenep', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-33', 'PW-1', 'Kota Sukabumi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD-XYZ', 'PW-1', 'Belum Ada PD', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
