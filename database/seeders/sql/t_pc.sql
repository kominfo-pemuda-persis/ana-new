INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.1', 'PD-1', 'Andir', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.10', 'PD-1', 'Regol', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.100', 'PD-19', 'Kota Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.101', 'PD-4', 'Bogor Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.102', 'PD-2', 'Cibiru', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.103', 'PD-15', 'Cikijing', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.104', 'PD-26', 'Cililin', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.105', 'PD-26', 'Batujajar', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.106', 'PD-8', 'Pakenjeng', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.107', 'PD-26', 'Lembang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.108', 'PD-20', 'Sumedang Kota', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.109', 'PD-XYZ', 'Tanjung Tiram', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.11', 'PD-1', 'Sukajadi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.110', 'PD-26', 'Cikalong Wetan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.111', 'PD-3', 'Cilaku', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.112', 'PD-1', 'Bandung Kulon', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.113', 'PD-2', 'Arjasari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.114', 'PD-1', 'Cicendo', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.115', 'PD-1', 'Cibeunying Kaler', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.116', 'PD-7', 'Pagaden', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.117', 'PD-2', 'Cicalengka', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.118', 'PD-2', 'Paseh', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.119', 'PD-8', 'Bayongbong', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.12', 'PD-1', 'Sukasari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.120', 'PD-XYZ', 'Rengasdengklok', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.121', 'PD-2', 'Margahayu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.122', 'PD-XYZ', 'Sukmajaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.123', 'PD-8', 'Leuwigoong', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.124', 'PD-2', 'Majalaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.125', 'PD-8', 'Banyuresmi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.126', 'PD-20', 'Pamulihan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.127', 'PD-21', 'Mangkubumi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.128', 'PD-XYZ', 'Pondok Sugu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.129', 'PD-XYZ', 'Karawang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.13', 'PD-1', 'Sumur Bandung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.130', 'PD-1', 'Bandung Wetan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.131', 'PD-10', 'Banjarsari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.132', 'PD-20', 'Paseh (Sumedang)', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.133', 'PD-15', 'Jati Tujuh', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.134', 'PD-9', 'Kadipaten', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.135', 'PD-26', 'Ngamprah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.136', 'PD-2', 'Cimaung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.137', 'PD-XYZ', 'Cikarang Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.138', 'PD-2', 'Kertasari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.139', 'PD-2', 'Cileunyi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.14', 'PD-1', 'Bandung Kidul', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.140', 'PD-XYZ', 'Bilah Hilir', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.141', 'PD-XYZ', 'Perdagangan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.142', 'PD-8', 'Sucinaraja', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.143', 'PD-8', 'Pangatikan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.144', 'PD-4', 'Bogor Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.145', 'PD-20', 'Cimanggung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.146', 'PD-20', 'Rancakalong', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.147', 'PD-2', 'Cangkuang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.148', 'PD-2', 'Cimenyan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.149', 'PD-2', 'Pasir Jambu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.15', 'PD-1', 'Buahbatu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.150', 'PD-31', 'Jakarta Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.151', 'PD-2', 'Rancabali', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.152', 'PD-1', 'Cihampelas', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.153', 'PD-3', 'Pacet (Cianjur)', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.154', 'PD-3', 'Haur Wangi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.155', 'PD-XYZ', 'Tambun', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.156', 'PD-XYZ', 'Kota Bekasi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.157', 'PD-XYZ', 'Larangan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.158', 'PD-10', 'Sindangkasih', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.159', 'PD-26', 'Cipeundeuy', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.16', 'PD-2', 'Baleendah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.160', 'PD-10', 'Panumbangan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.161', 'PD-8', 'Ciamis Kota', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.162', 'PD-8', 'Tarogong Kaler', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.163', 'PD-29', 'Pasir Wangi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.164', 'PD-XYZ', 'Muara Sungkai', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.165', 'PD-30', 'Kebun Tebu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.166', 'PD-30', 'Anak Tuha', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.167', 'PD-31', 'Setiabudi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.168', 'PD-10', 'Sukamantri', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.169', 'PD-8', 'Karang Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.17', 'PD-2', 'Banjaran', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.170', 'PD-8', 'Pameungpeuk Garut', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.171', 'PD-5', 'Cibinong Bogor', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.172', 'PD-26', 'Gununghalu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.173', 'PD-15', 'Cingambul', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.174', 'PD-XYZ', 'Bekasi Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.175', 'PD-8', 'Banjarwangi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.176', 'PD-33', 'Cisaat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.177', 'PD-3', 'Sukanagara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.178', 'PD-20', 'Sumedang Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.179', 'PD-21', 'Tawang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.18', 'PD-18', 'Cimahi Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.180', 'PD-21', 'Bungursari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.181', 'PD-8', 'Leles', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.182', 'PD-8', 'Cisompet', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.183', 'PD-1', 'Cidadap', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.184', 'PD-3', 'Karang Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.185', 'PD-33', 'Gunung Puyuh', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.186', 'PD-33', 'Cikole', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.187', 'PD-33', 'Citamiang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.188', 'PD-8', 'Sukaresmi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.19', 'PD-18', 'Cimahi Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.2', 'PD-1', 'Astana Anyar', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.20', 'PD-18', 'Cimahi Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.21', 'PD-2', 'Ciwidey', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.22', 'PD-2', 'Bojongsoang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.23', 'PD-2', 'Dayeuhkolot', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.24', 'PD-2', 'Ciparay', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.25', 'PD-2', 'Pameungpeuk', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.26', 'PD-26', 'Padalarang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.27', 'PD-2', 'Rancaekek', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.28', 'PD-2', 'Soreang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.29', 'PD-2', 'Kutawaringin', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.3', 'PD-1', 'Babakan Ciparay', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.30', 'PD-2', 'Margaasih', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.31', 'PD-26', 'Cipatat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.32', 'PD-2', 'Katapang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.33', 'PD-8', 'Wanaraja', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.34', 'PD-8', 'Cisurupan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.35', 'PD-8', 'Cikajang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.36', 'PD-8', 'Garut Kota', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC.37', 'PD-8', 'Samarang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.38', 'PD-8', 'Tarogong', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.39', 'PD-8', 'Cilawu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.4', 'PD-1', 'Batununggal', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.40', 'PD-8', 'Cibatu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.41', 'PD-8', 'Kersamanah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.42', 'PD-21', 'Cihideung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.43', 'PD-9', 'Ciawi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.44', 'PD-21', 'Cipedes', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.45', 'PD-21', 'Indihiang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.46', 'PD-9', 'Kadipaten', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.47', 'PD-9', 'Sukaresik', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.48', 'PD-9', 'Jamanis', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.49', 'PD-9', 'Rajapolah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.5', 'PD-1', 'Bojongloa Kaler', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.50', 'PD-10', 'Cikoneng', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.51', 'PD-28', 'Banjar', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.52', 'PD-15', 'Majalengka', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.53', 'PD-15', 'Jatiwangi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.54', 'PD-XYZ', 'Sumberjaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.55', 'PD-XYZ', 'Serang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.56', 'PD-XYZ', 'Padarincang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.57', 'PD-XYZ', 'Rangkasbitung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.58', 'PD-3', 'Cianjur', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.59', 'PD-3', 'Ciranjang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.6', 'PD-1', 'Bojongloa Kidul', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.60', 'PD-3', 'Cibeber', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.61', 'PD-3', 'Cikalong Kulon', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.62', 'PD-3', 'Sukaresmi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.63', 'PD-3', 'Bojong Picung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.64', 'PD-16', 'Sukaraja', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.65', 'PD-16', 'Warudoyong', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.66', 'PD-7', 'Legonkulon', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.67', 'PD-7', 'Pusakanagara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.68', 'PD-7', 'Pamanukan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.69', 'PD-6', 'Purwakarta', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.7', 'PD-1', 'Mandalajati', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.70', 'PD-6', 'Plered', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.71', 'PD-XYZ', 'Bogor Kota', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.72', 'PD-5', 'Citeureup', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.73', 'PD-11', 'Johar Baru', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.74', 'PD-11', 'Senen', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.75', 'PD-12', 'Grogol Petamburan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.76', 'PD-12', 'Cengkareng', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.77', 'PD-12', 'Kalideres', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.78', 'PD-13', 'Jatinegara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.79', 'PD-13', 'Matraman', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.8', 'PD-1', 'Cibeuying Kidul', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.80', 'PD-14', 'Koja', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.81', 'PD-14', 'Tanjungpriok', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.82', 'PD-12', 'Pademangan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.83', 'PD-XYZ', 'Sapeken', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.84', 'PD-32', 'Saronggi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.85', 'PD-32', 'Abunten', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.86', 'PD-32', 'Sumenep', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.87', 'PD-27', 'Pamekasan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.88', 'PD-XYZ', 'Belo', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.89', 'PD-XYZ', 'RasanaE', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.9', 'PD-1', 'Kiaracondong', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.90', 'PD-XYZ', 'Sape', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.91', 'PD-XYZ', 'Wera', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.92', 'PD-XYZ', 'Woha', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.93', 'PD-27', 'Tlanakan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.94', 'PD-XYZ', 'Pancoran Mas', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.95', 'PD-20', 'Tanjungsari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.96', 'PD-20', 'Jatinangor', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.97', 'PD-8', 'Karangpawitan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.98', 'PD-2', 'Pangalengan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at) VALUES ('PC.99', 'PD-XYZ', 'Harjamukti', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
