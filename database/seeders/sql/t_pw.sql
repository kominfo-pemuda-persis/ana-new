INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-1', 'Jawa Barat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-2', 'DKI Jakarta', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-3', 'Jawa Timur', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-4', 'Sulawesi Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-5', 'Gorontalo', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-6', 'Bali', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-7', 'Sumatera Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-8', 'Banten', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW-XYZ', 'Belum Ada PW', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
