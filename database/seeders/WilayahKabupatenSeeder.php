<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WilayahKabupatenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = './database/seeders/sql/t_kabupaten.sql';

        $sql = file_get_contents($path);

        DB::statement($sql);
    }
}
