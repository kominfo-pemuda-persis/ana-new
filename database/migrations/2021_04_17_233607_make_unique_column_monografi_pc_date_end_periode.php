<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUniqueColumnMonografiPcDateEndPeriode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("t_monografi_pc", function (Blueprint $table) {
            $table->unique(['kd_pc', 'kd_pd', 'kd_pw', 'start_periode', 'end_periode'], "unique_pc_periode_monografi");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("t_monografi_pc", function (Blueprint $table) {
            $table->dropUnique("unique_pc_periode_monografi");
        });
    }
}
