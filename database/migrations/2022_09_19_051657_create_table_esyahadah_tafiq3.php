<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEsyahadahTafiq3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_esyahadah_tafiq3', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('id_tafiq3');
            $table->integer('tahun');
            $table->integer('bulan');
            $table->string('no_urut')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_esyahadah_tafiq3');
    }
}
