<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTKejamiyyahanPWSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_kejamiyyahan_pw', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kd_pw', 10);
            $table->string('nama_pw', 50);
            $table->bigInteger('kd_monografi_pw', false, true);
            $table->string('provinsi', 2);
            $table->string('ketua');
            $table->string('wkl_ketua')->nullable();
            $table->string('sekretaris');
            $table->string('wkl_sekretaris')->nullable();
            $table->string('bendahara');
            $table->string('wkl_bendahara')->nullable();
            $table->string('bid_jamiyyah')->nullable();
            $table->string('wkl_bid_jamiyyah')->nullable();
            $table->string('bid_kaderisasi')->nullable();
            $table->string('wkl_bid_kaderisasi')->nullable();
            $table->string('bid_administrasi')->nullable();
            $table->string('wkl_bid_administrasi')->nullable();
            $table->string('bid_pendidikan')->nullable();
            $table->string('wkl_bid_pendidikan')->nullable();
            $table->string('bid_dakwah')->nullable();
            $table->string('wkl_bid_dakwah')->nullable();
            $table->string('bid_humas_publikasi')->nullable();
            $table->string('wkl_bid_humas_publikasi')->nullable();
            $table->string('bid_hal')->nullable();
            $table->string('wkl_bid_hal')->nullable();
            $table->string('bid_or_seni')->nullable();
            $table->string('wkl_bid_or_seni')->nullable();
            $table->string('bid_sosial')->nullable();
            $table->string('wkl_bid_sosial')->nullable();
            $table->string('bid_ekonomi')->nullable();
            $table->string('wkl_bid_ekonomi')->nullable();
            $table->string('penasehat1')->nullable();
            $table->string('penasehat2')->nullable();
            $table->string('penasehat3')->nullable();
            $table->string('penasehat4')->nullable();
            $table->string('pembantu_umum1')->nullable();
            $table->string('pembantu_umum2')->nullable();
            $table->string('pembantu_umum3')->nullable();
            $table->string('hari');
            $table->time('pukul');
            $table->date('muswil_terakhir_m');
            $table->date('muswil_terakhir_h');
            $table->integer('anggota_biasa')->nullable();
            $table->integer('anggota_tersiar')->nullable();
            $table->integer('anggota_istimewa')->nullable();
            $table->timestamps();
            $table->foreign('provinsi')->references('id')->on('t_provinsi')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_kejamiyyahan_pw');
    }
}
