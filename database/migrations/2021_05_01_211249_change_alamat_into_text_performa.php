<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAlamatIntoTextPerforma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_performa_pj', function (Blueprint $table) {
            $table->text('alamat')->change();
        });

        Schema::table('t_performa_pc', function (Blueprint $table) {
            $table->text('alamat')->change();
        });

        Schema::table('t_performa_pd', function (Blueprint $table) {
            $table->text('alamat')->change();
        });

        Schema::table('t_performa_pw', function (Blueprint $table) {
            $table->text('alamat')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
