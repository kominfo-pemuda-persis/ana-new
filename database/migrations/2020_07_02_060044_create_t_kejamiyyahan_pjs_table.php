<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTKejamiyyahanPjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_kejamiyyahan_pj', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_pj', 6);
            $table->string('nama_pj', 50);
            $table->bigInteger('kd_monografi_pj', false, true);
            $table->string('kd_pc', 10);
            $table->string('kd_pd', 10);
            $table->string('kd_pw', 10);
            $table->string('provinsi', 2);
            $table->string('kabupaten', 4);
            $table->string('kecamatan', 7);
            $table->string('desa', 10);
            $table->string('ketua');
            $table->string('sekretaris');
            $table->string('bendahara');
            $table->enum('hari_ngantor', array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Ahad'));
            $table->time('waktu_ngantor');
            $table->dateTime('musjam_terakhir_masehi');
            $table->dateTime('musjam_terakhir_hijriyyah');
            $table->integer('anggota_biasa');
            $table->integer('anggota_luar_biasa');
            $table->string('tidak_herReg');
            $table->string('mutasi_persis');
            $table->string('mutasi_tempat');
            $table->string('meninggal_dunia');
            $table->string('mengundurkan_diri');
            $table->string('calon_anggota');
            $table->foreign('kd_monografi_pj')->references('id')->on('t_monografi_pj')->onDelete('cascade');
            $table->foreign('kd_pc')->references('kd_pc')->on('t_pc')->onDelete('cascade');
            $table->foreign('kd_pd')->references('kd_pd')->on('t_pd')->onDelete('cascade');
            $table->foreign('kd_pw')->references('kd_pw')->on('t_pw')->onDelete('cascade');
            $table->foreign('provinsi')->references('id')->on('t_provinsi')->onDelete('cascade');
            $table->foreign('kabupaten')->references('id')->on('t_kabupaten')->onDelete('cascade');
            $table->foreign('kecamatan')->references('id')->on('t_kecamatan')->onDelete('cascade');
            $table->foreign('desa')->references('id')->on('t_desa')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_kejamiyyahan_pj');
    }
}
