<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeNullableColumnHeregistrasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `t_her_registration` CHANGE `path_image` `path_image` TEXT NULL;");
        DB::statement("ALTER TABLE `t_her_registration` CHANGE `path_folder` `path_folder` TEXT NULL;");
        DB::statement("ALTER TABLE `t_her_registration` CHANGE `path_excel` `path_excel` TEXT NULL;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `t_her_registration` CHANGE `path_image` `path_image` TEXT NOT NULL;");
        DB::statement("ALTER TABLE `t_her_registration` CHANGE `path_folder` `path_folder` TEXT NOT NULL;");
        DB::statement("ALTER TABLE `t_her_registration` CHANGE `path_excel` `path_excel` TEXT NOT NULL;");
    }
}
