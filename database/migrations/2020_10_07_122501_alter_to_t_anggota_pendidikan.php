<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterToTAnggotaPendidikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('t_anggota_pendidikan')->truncate();
        Schema::table('t_anggota_pendidikan', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE `t_anggota_pendidikan` CHANGE `tahun_masuk` `tahun_masuk` INT(4) NOT NULL, CHANGE `tahun_keluar` `tahun_keluar` INT(4) NOT NULL;");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota_pendidikan', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE `t_anggota_pendidikan` CHANGE `tahun_masuk` `tahun_masuk` DATE NOT NULL, CHANGE `tahun_keluar` `tahun_keluar` DATE NOT NULL;");
        });
    }
}
