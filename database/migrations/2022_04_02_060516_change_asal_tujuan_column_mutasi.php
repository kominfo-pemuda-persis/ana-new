<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeAsalTujuanColumnMutasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_anggota_mutasi', function () {
            DB::statement("ALTER TABLE `t_anggota_mutasi` CHANGE `asal` `asal` VARCHAR(32) NOT NULL;");
            DB::statement("ALTER TABLE `t_anggota_mutasi` CHANGE `tujuan` `tujuan` VARCHAR(32) NOT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota_mutasi', function () {
            DB::statement("ALTER TABLE `t_anggota_mutasi` CHANGE `asal` `asal` INT(32) NOT NULL;");
            DB::statement("ALTER TABLE `t_anggota_mutasi` CHANGE `tujuan` `tujuan` INT(32) NOT NULL;");
        });
    }
}
