<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnEsyahadahIdTafiqAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_anggota_tafiq', function (Blueprint $table) {
            $table->integer('id_esyahadah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota_tafiq', function (Blueprint $table) {
            $table->dropColumn("id_esyahadah");
            $table->dropTimestamps();
        });
    }
}
