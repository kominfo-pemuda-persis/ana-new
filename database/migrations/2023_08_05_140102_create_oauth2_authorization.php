<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOauth2Authorization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth2_authorization', function (Blueprint $table) {
            $table->string('id', 100);
            $table->string('registered_client_id', 100);
            $table->string('principal_name', 200);
            $table->string('authorization_grant_type', 100);
            $table->string('authorized_scopes', 1000)->nullable()->default('NULL');
            $table->string('state', 500)->nullable()->default('NULL');
            $table->timestamp('authorization_code_issued_at')->useCurrent();
            $table->timestamp('authorization_code_expires_at')->useCurrent();
            $table->timestamp('access_token_issued_at')->useCurrent();
            $table->timestamp('access_token_expires_at')->useCurrent();
            $table->string('access_token_type', 100);
            $table->string('access_token_scopes', 1000);
            $table->timestamp('oidc_id_token_issued_at')->useCurrent();
            $table->timestamp('oidc_id_token_expires_at')->useCurrent();
            $table->timestamp('refresh_token_issued_at')->useCurrent();
            $table->timestamp('refresh_token_expires_at')->useCurrent();
            $table->timestamp('user_code_issued_at')->useCurrent();
            $table->timestamp('user_code_expires_at')->useCurrent();
            $table->timestamp('device_code_issued_at')->useCurrent();
            $table->timestamp('device_code_expires_at')->useCurrent();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth2_authorization');
    }
}
