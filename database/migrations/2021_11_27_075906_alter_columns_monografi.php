<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterColumnsMonografi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_monografi_pw', function (Blueprint $table) {
            DB::statement('ALTER TABLE t_monografi_pw MODIFY luas DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pw MODIFY jarak_dari_ibukota_negara DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pw MODIFY jarak_dari_ibukota_provinsi DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pw MODIFY jarak_dari_ibukota_kabupaten DOUBLE(15,2) DEFAULT 0');
        });

        Schema::table('t_monografi_pd', function (Blueprint $table) {
            DB::statement('ALTER TABLE t_monografi_pd MODIFY luas DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pd MODIFY jarak_dari_ibukota_negara DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pd MODIFY jarak_dari_ibukota_provinsi DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pd MODIFY jarak_dari_ibukota_kabupaten DOUBLE(15,2) DEFAULT 0');
        });

        Schema::table('t_monografi_pc', function (Blueprint $table) {
            DB::statement('ALTER TABLE t_monografi_pc MODIFY luas DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pc MODIFY jarak_dari_ibukota_negara DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pc MODIFY jarak_dari_ibukota_provinsi DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pc MODIFY jarak_dari_ibukota_kabupaten DOUBLE(15,2) DEFAULT 0');
        });

        Schema::table('t_monografi_pj', function (Blueprint $table) {
            DB::statement('ALTER TABLE t_monografi_pj MODIFY luas DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pj MODIFY jarak_provinsi DOUBLE(15,2) DEFAULT 0');
            DB::statement('ALTER TABLE t_monografi_pj MODIFY jarak_kabupaten DOUBLE(15,2) DEFAULT 0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
