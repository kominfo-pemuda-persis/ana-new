<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnNullableCaangNoTelfon2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `t_caang` CHANGE `no_telpon2` `no_telpon2` VARCHAR (15) NULL;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `t_caang` CHANGE `no_telpon2` `no_telpon2` VARCHAR (15) NOT NULL;");
    }
}
