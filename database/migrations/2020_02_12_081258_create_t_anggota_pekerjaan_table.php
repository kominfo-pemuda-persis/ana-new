<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTAnggotaPekerjaanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_anggota_pekerjaan', function(Blueprint $table)
		{
			$table->integer('id_pekerjaan', true);
			$table->integer('id_anggota');
			$table->string('pekerjaan', 130);
			$table->text('keterangan', 65535)->nullable();
			$table->text('alamat', 65535)->nullable();
			$table->date('tahun_mulai');
			$table->date('tahun_selesai');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_anggota_pekerjaan');
	}

}
