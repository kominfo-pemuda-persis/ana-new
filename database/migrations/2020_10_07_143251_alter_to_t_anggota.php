<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterToTAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_anggota', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE `t_anggota` CHANGE `level_tafiq` `level_tafiq` INT(2) NOT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE `t_anggota` CHANGE `level_tafiq` `level_tafiq` VARCHAR (100) NOT NULL;");
        });
    }
}
