<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPjPwPcPdStatusTCaang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_caang', function (Blueprint $table) {
            $table->string('pw', 10)->nullable();
            $table->string('pd', 10)->nullable();
            $table->string('pc', 10)->nullable();
            $table->string('pj', 10)->nullable();
            $table->string('nama_pj', 100)->nullable();
            $table->enum('status',
                [
                    "INIT",
                    "APPROVED_BY_PC",
                    "APPROVED_BY_PD",
                    "APPROVED_BY_PW",
                    "APPROVED_BY_PP"
                ])->default("INIT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_caang', function (Blueprint $table) {
            $table->dropColumn('pj');
            $table->dropColumn('pc');
            $table->dropColumn('pd');
            $table->dropColumn('pw');
            $table->dropColumn('nama_pj');
            $table->dropColumn('status');
        });
    }
}