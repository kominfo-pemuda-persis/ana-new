<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Oauth2AuthorizationConsent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth2_authorization_consent', function (Blueprint $table) {
            $table->string('registered_client_id', 100);
            $table->string('principal_name', 200);
            $table->string('authorities', 1000);
            $table->primary('registered_client_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth2_authorization_consent');
    }
}
