<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCloumnStartEndPeriodePerforma extends Migration
{
    private $tablePerforma = [
        't_performa_pw',
        't_performa_pd',
        't_performa_pc',
        't_performa_pj',
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tablePerforma as $value){
            Schema::table($value, function (Blueprint $table) {
                $table->date('start_periode');
                $table->date('end_periode');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tablePerforma as $value){
            Schema::table($value, function (Blueprint $table) {
                $table->dropColumn('start_periode');
                $table->dropColumn('end_periode');
            });
        }
    }
}
