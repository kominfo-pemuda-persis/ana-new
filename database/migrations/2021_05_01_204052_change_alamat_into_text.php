<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAlamatIntoText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_monografi_pj', function (Blueprint $table) {
            $table->text('alamat')->change();
        });

        Schema::table('t_monografi_pc', function (Blueprint $table) {
            $table->text('alamat_utama')->change();
            $table->text('alamat_alternatif')->change();
        });

        Schema::table('t_monografi_pd', function (Blueprint $table) {
            $table->text('alamat_utama')->change();
            $table->text('alamat_alternatif')->change();
        });

        Schema::table('t_monografi_pw', function (Blueprint $table) {
            $table->text('alamat_utama')->change();
            $table->text('alamat_alternatif')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
