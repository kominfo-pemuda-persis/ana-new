<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableEsyahadah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_esyahadah_maruf', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('id_anggota');
            $table->integer('tahun');
            $table->integer('bulan');
            $table->string('no_urut');
            $table->string('tempat_maruf');
            $table->timestamp('tanggal_maruf');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_esyahadah_maruf');
    }
}
