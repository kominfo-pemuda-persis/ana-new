<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeIntToDecimalTafiqNilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `t_tafiq1` CHANGE `kehadiran` `kehadiran` DECIMAL(10,2) NOT NULL, CHANGE `keaktifan` `keaktifan` DECIMAL(10,2) NOT NULL, CHANGE `makalah` `makalah` DECIMAL(10,2) NOT NULL;");
        DB::statement("ALTER TABLE `t_tafiq2` CHANGE `kehadiran` `kehadiran` DECIMAL(10,2) NOT NULL, CHANGE `keaktifan` `keaktifan` DECIMAL(10,2) NOT NULL, CHANGE `makalah` `makalah` DECIMAL(10,2) NOT NULL;");
        DB::statement("ALTER TABLE `t_tafiq3` CHANGE `kehadiran` `kehadiran` DECIMAL(10,2) NOT NULL, CHANGE `keaktifan` `keaktifan` DECIMAL(10,2) NOT NULL, CHANGE `makalah` `makalah` DECIMAL(10,2);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `t_tafiq1` CHANGE `kehadiran` `kehadiran` INT NOT NULL, CHANGE `keaktifan` `keaktifan` INT NOT NULL, CHANGE `makalah` `makalah` INT NOT NULL;");
        DB::statement("ALTER TABLE `t_tafiq2` CHANGE `kehadiran` `kehadiran` INT NOT NULL, CHANGE `keaktifan` `keaktifan` INT NOT NULL, CHANGE `makalah` `makalah` INT NOT NULL;");
        DB::statement("ALTER TABLE `t_tafiq3` CHANGE `kehadiran` `kehadiran` INT NOT NULL, CHANGE `keaktifan` `keaktifan` INT NOT NULL, CHANGE `makalah` `makalah` INT NOT NULL;");
    }
}
