<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableHerRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_her_registration', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->text("name");
			$table->text("email");
			$table->text("npa");
			$table->text("phone_number");
			$table->text("path_folder");
			$table->text("path_excel");
			$table->text("path_image");
            $table->softDeletes();
            $table->timestamps();
            $table->enum('status', array('SUBMITTED', 'APPROVED', 'REJECTED'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_her_registration');
    }
}
