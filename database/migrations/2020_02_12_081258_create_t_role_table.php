<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_role', function(Blueprint $table)
		{
			$table->integer('id_role', true);
			$table->string('nama_role', 50);
			$table->string('desc', 100)->nullable();
			$table->smallInteger('status_aktif');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_role');
	}

}
