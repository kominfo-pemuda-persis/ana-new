<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TempLastNoUrutEsyahadah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_temp_last_no_urut_esyahadah', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('id_anggota')->nullable();
            $table->integer('year')->nullable();
            $table->integer('counter')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_temp_last_no_urut_esyahadah');
    }
}
