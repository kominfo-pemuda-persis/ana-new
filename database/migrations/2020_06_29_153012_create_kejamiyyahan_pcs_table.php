<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKejamiyyahanPcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_kejamiyyahan_pc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kd_pc', 10);
            $table->string('kd_pd', 10);
            $table->string('kd_pw', 10);
            $table->bigInteger('kd_monografi_pc', false, true);
            $table->string('provinsi', 2);
            $table->string('kota', 4);
            $table->string('kecamatan', 7);
            $table->string('ketua');
            $table->string('wkl_ketua')->nullable();
            $table->string('sekretaris');
            $table->string('wkl_sekretaris')->nullable();
            $table->string('bendahara');
            $table->string('wkl_bendahara')->nullable();
            $table->string('bid_jamiyyah')->nullable();
            $table->string('wkl_bid_jamiyyah')->nullable();
            $table->string('bid_kaderisasi')->nullable();
            $table->string('wkl_bid_kaderisasi')->nullable();
            $table->string('bid_administrasi')->nullable();
            $table->string('wkl_bid_administrasi')->nullable();
            $table->string('bid_pendidikan')->nullable();
            $table->string('wkl_bid_pendidikan')->nullable();
            $table->string('bid_dakwah')->nullable();
            $table->string('wkl_bid_dakwah')->nullable();
            $table->string('bid_humas_publikasi')->nullable();
            $table->string('wkl_bid_humas_publikasi')->nullable();
            $table->string('bid_hal')->nullable();
            $table->string('wkl_bid_hal')->nullable();
            $table->string('bid_or_seni')->nullable();
            $table->string('wkl_bid_or_seni')->nullable();
            $table->string('bid_sosial')->nullable();
            $table->string('wkl_bid_sosial')->nullable();
            $table->string('bid_ekonomi')->nullable();
            $table->string('wkl_bid_ekonomi')->nullable();
            $table->string('penasehat1')->nullable();
            $table->string('penasehat2')->nullable();
            $table->string('penasehat3')->nullable();
            $table->string('penasehat4')->nullable();
            $table->string('pembantu_umum1')->nullable();
            $table->string('pembantu_umum2')->nullable();
            $table->string('pembantu_umum3')->nullable();
            $table->string('hari');
            $table->time('pukul');
            $table->date('musycab_terakhir_m');
            $table->date('musycab_terakhir_h');
            $table->integer('anggota_biasa')->nullable();
            $table->integer('anggota_tersiar')->nullable();
            $table->integer('anggota_istimewa')->nullable();
            $table->integer('tdk_her')->nullable();
            $table->integer('mutasi_ke_persis')->nullable();
            $table->integer('mutasi_tempat')->nullable();
            $table->integer('mengundurkan_diri')->nullable();
            $table->integer('meninggal_dunia')->nullable();
            $table->integer('biasa')->nullable();
            $table->integer('istimewa')->nullable();
            $table->integer('tersiar')->nullable();
            $table->timestamps();
            $table->foreign('kd_pc')->references('kd_pc')->on('t_pc')->onDelete('cascade');
            $table->foreign('kd_monografi_pc')->references('id')->on('t_monografi_pc')->onDelete('cascade');
            $table->foreign('kd_pd')->references('kd_pd')->on('t_pd')->onDelete('cascade');
            $table->foreign('kd_pw')->references('kd_pw')->on('t_pw')->onDelete('cascade');
            $table->foreign('provinsi')->references('id')->on('t_provinsi')->onDelete('cascade');
            $table->foreign('kota')->references('id')->on('t_kabupaten')->onDelete('cascade');
            $table->foreign('kecamatan')->references('id')->on('t_kecamatan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_kejamiyyahan_pc');
    }
}
