<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTempatTanggalMarufTCaang extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_caang', function (Blueprint $table) {
            $table->string('tempat_maruf', 100);
            $table->date('tanggal_maruf_start');
            $table->date('tanggal_maruf_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_caang', function (Blueprint $table) {
            $table->dropColumn('tempat_maruf');
            $table->dropColumn('tanggal_maruf_start');
            $table->dropColumn('tanggal_maruf_end');
        });
    }
}