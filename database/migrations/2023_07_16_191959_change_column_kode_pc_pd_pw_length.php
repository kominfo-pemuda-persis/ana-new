<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeColumnKodePcPdPwLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `t_pc` CHANGE `kd_pc` `kd_pc` VARCHAR(25) NOT NULL,
            CHANGE `kd_pd` `kd_pd` VARCHAR(25) NOT NULL;");
        DB::statement("ALTER TABLE `t_pd` CHANGE `kd_pd` `kd_pd` VARCHAR(25) NOT NULL,
            CHANGE `kd_pw` `kd_pw` VARCHAR(25) NOT NULL;");
        DB::statement("ALTER TABLE `t_pw` CHANGE `kd_pw` `kd_pw` VARCHAR(25) NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::statement("ALTER TABLE `t_pc` CHANGE `kd_pc` `kd_pc` VARCHAR(25) NOT NULL,
            CHANGE `kd_pd` `kd_pd` VARCHAR(10) NOT NULL;");
        DB::statement("ALTER TABLE `t_pd` CHANGE `kd_pd` `kd_pd` VARCHAR(10) NOT NULL,
            CHANGE `kd_pw` `kd_pw` VARCHAR(10) NOT NULL;");
        DB::statement("ALTER TABLE `t_pw` CHANGE `kd_pw` `kd_pw` VARCHAR(10) NOT NULL;");
        Schema::enableForeignKeyConstraints();
    }
}
