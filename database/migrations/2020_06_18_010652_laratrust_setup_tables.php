<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class LaratrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Alter t_role table
        DB::statement("ALTER TABLE `t_role` 
            CHANGE `id_role` `id_role` int unsigned NOT NULL AUTO_INCREMENT FIRST,
            ADD `name` varchar(191) NOT NULL AFTER `id_role`,
            CHANGE `nama_role` `display_name` varchar(191) COLLATE 'utf8mb4_unicode_ci' NOT NULL AFTER `name`,
            CHANGE `desc` `description` varchar(191) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `display_name`,
            CHANGE `status_aktif` `status_aktif` tinyint(1) NOT NULL DEFAULT 1 AFTER `description`;");

        // Create table for storing permissions
        // Schema::create('t_permissions', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name')->unique();
        //     $table->string('display_name')->nullable();
        //     $table->string('description')->nullable();
        //     $table->timestamps();
        // });

        // Create table for associating roles to users and teams (Many To Many Polymorphic)
        Schema::create('t_role_user', function (Blueprint $table) {
            $table->unsignedInteger('role_id');
            $table->string('user_id', 32);

            $table->foreign('role_id')->references('id_role')->on('t_role')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });

        // Create table for associating permissions to users (Many To Many Polymorphic)
        Schema::create('t_permission_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->string('module');
            $table->string('permission');

            $table->primary(['user_id','module','permission']);
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('t_permission_role', function (Blueprint $table) {
            $table->unsignedInteger('role_id');
            $table->string('module');
            $table->string('permission');

            $table->foreign('role_id')->references('id_role')->on('t_role')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['role_id','module','permission']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('t_permission_user');
        Schema::dropIfExists('t_permission_role');
        // Schema::dropIfExists('t_permissions');
        Schema::dropIfExists('t_role_user');

        DB::statement("ALTER TABLE `t_role` 
            CHANGE `id_role` `id_role` int NOT NULL AUTO_INCREMENT FIRST,
            DROP `name`,
            CHANGE `display_name` `nama_role` varchar(191) COLLATE 'utf8mb4_unicode_ci' NOT NULL AFTER `id_role`,
            CHANGE `description` `desc` varchar(191) COLLATE 'utf8mb4_unicode_ci' NULL AFTER `nama_role`;");
    }
}
