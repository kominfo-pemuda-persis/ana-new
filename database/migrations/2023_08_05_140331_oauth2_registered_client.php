<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Oauth2RegisteredClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth2_registered_client', function (Blueprint $table) {
            $table->string('id', 100);
            $table->string('client_id', 100);
            $table->timestamp('client_id_issued_at')->useCurrent();
            $table->string('client_secret', 200)->nullable()->default('NULL');
            $table->timestamp('client_secret_expires_at')->useCurrent();
            $table->string('client_name', 200);
            $table->string('client_authentication_methods', 1000);
            $table->string('authorization_grant_types', 1000);
            $table->string('redirect_uris', 1000)->nullable()->default('NULL');
            $table->string('post_logout_redirect_uris', 1000)->nullable()->default('NULL');
            $table->string('scopes', 1000);
            $table->string('client_settings', 2000);
            $table->string('token_settings', 2000);
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth2_registered_client');
    }
}
