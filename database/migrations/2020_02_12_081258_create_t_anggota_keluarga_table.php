<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTAnggotaKeluargaTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_anggota_keluarga', function (Blueprint $table) {
			$table->integer('id_keluarga', true);
			$table->integer('id_anggota');
			$table->string('nama_keluarga', 70);
			$table->enum('hubungan', array('Ayah', 'Ibu', 'Istri', 'Suami', 'Anak'));
			$table->integer('jumlah_anak')->nullable();
			$table->text('alamat', 65535)->nullable();
			$table->integer('id_otonom')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_anggota_keluarga');
	}
}
