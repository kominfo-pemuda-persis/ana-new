<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIdAnggotaVarcharToTAnggotaOrganisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_anggota_organisasi', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE `t_anggota_organisasi` CHANGE `id_anggota` `id_anggota` VARCHAR(32) NOT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota_organisasi', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE `t_anggota_organisasi` CHANGE `id_anggota` `id_anggota` INT(11) NOT NULL;");
        });
    }
}
