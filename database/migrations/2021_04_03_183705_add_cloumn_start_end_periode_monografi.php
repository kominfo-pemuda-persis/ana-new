<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCloumnStartEndPeriodeMonografi extends Migration
{
    private $tableMonografi = [
        't_monografi_pw',
        't_monografi_pd',
        't_monografi_pc',
        't_monografi_pj',
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tableMonografi as $value){
            Schema::table($value, function (Blueprint $table) {
                $table->date('start_periode');
                $table->date('end_periode');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tableMonografi as $value){
            Schema::table($value, function (Blueprint $table) {
                $table->dropColumn('start_periode');
                $table->dropColumn('end_periode');
            });
        }
    }
}
