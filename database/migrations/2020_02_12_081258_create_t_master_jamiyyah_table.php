<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTMasterJamiyyahTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_master_jamiyyah', function(Blueprint $table)
		{
			$table->integer('id_jamiyyah', true);
			$table->integer('id_wilayah');
			$table->string('nama_pimpinan', 50);
			$table->integer('id_otonom');
			$table->enum('pimpinan', array('PW','PD','PC','PC Non PD','Perwakilan PW'));
			$table->integer('nomor')->nullable();
			$table->text('alamat', 65535)->nullable();
			$table->float('latitude', 10, 0)->nullable();
			$table->float('longitude', 10, 0)->nullable();
            $table->smallInteger('status_aktif')->nullable();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_master_jamiyyah');
	}

}
