<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUniqueColumnPerformaPjDateEndPeriode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_performa_pj', function (Blueprint $table) {
            $table->unique(['kd_pj', 'nama_pj', 'kd_pc', 'kd_pd', 'kd_pw', 'start_periode', 'end_periode'], "unique_pj_periode_performa");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_performa_pj', function (Blueprint $table) {
            $table->dropUnique('unique_pj_periode_performa');
        });
    }
}
