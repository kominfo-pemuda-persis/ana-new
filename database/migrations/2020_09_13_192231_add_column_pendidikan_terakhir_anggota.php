<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPendidikanTerakhirAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_anggota', function (Blueprint $table) {
            $table->integer('pendidikan_terakhir')->default(11);
            $table->foreign('pendidikan_terakhir')->references('id_tingkat_pendidikan')->on('t_master_pendidikan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota', function (Blueprint $table) {
            $table->dropForeign('pendidikan_terakhir');
            $table->dropColumn('pendidikan_terakhir');
        });
    }
}
