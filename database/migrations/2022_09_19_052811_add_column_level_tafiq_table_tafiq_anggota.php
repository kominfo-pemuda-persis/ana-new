<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnLevelTafiqTableTafiqAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_anggota_tafiq', function (Blueprint $table) {
            $table->enum('level_tafiq', [
                "TAFIQ_1",
                "TAFIQ_2",
                "TAFIQ_3",
            ])->default("TAFIQ_1");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota_tafiq', function (Blueprint $table) {
            $table->dropColumn('level_tafiq');
        });
    }
}
