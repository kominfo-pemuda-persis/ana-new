<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTAnggotaPendidikanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_anggota_pendidikan', function(Blueprint $table)
		{
			$table->integer('id_pendidikan', true);
			$table->integer('id_anggota');
			$table->smallInteger('id_master_pendidikan');
			$table->string('instansi', 100);
			$table->string('jurusan', 100)->nullable();
			$table->date('tahun_masuk');
			$table->date('tahun_keluar');
			$table->enum('jenis_pendidikan', array('Formal','Non Formal'))->nullable()->default('Formal');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_anggota_pendidikan');
	}

}
