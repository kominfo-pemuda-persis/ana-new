<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTMonografiPJTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_monografi_pj', function (Blueprint $table) {
            $table->bigIncrements('id')->autoIncrement();
            $table->string('kode_pj', 6);
            $table->string('nama_pj', 50);
            $table->string('kd_pc', 10);
            $table->string('kd_pd', 10);
            $table->string('kd_pw', 10);
            $table->string('provinsi', 2);
            $table->string('kabupaten', 4);
            $table->string('kecamatan', 7);
            $table->string('desa', 10);
            $table->double('latitude');
            $table->double('longitude');
            $table->string('email', 50)->unique()->nullable();
            $table->string('no_telpon', 15);
            $table->string('alamat', 100);
            $table->string('luas');
            $table->string('bw_timur', 50);
            $table->string('bw_barat', 50);
            $table->string('bw_selatan', 50);
            $table->string('bw_utara', 50);
            $table->string('jarak_provinsi', 50);
            $table->string('jarak_kabupaten', 50);
            $table->string('photo', 100)->nullable();
            $table->string('created_by', 50);
            $table->foreign('kd_pc')->references('kd_pc')->on('t_pc')->onDelete('cascade');
            $table->foreign('kd_pd')->references('kd_pd')->on('t_pd')->onDelete('cascade');
            $table->foreign('kd_pw')->references('kd_pw')->on('t_pw')->onDelete('cascade');
            $table->foreign('provinsi')->references('id')->on('t_provinsi')->onDelete('cascade');
            $table->foreign('kabupaten')->references('id')->on('t_kabupaten')->onDelete('cascade');
            $table->foreign('kecamatan')->references('id')->on('t_kecamatan')->onDelete('cascade');
            $table->foreign('desa')->references('id')->on('t_desa')->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_monografi_pj');
    }
}
