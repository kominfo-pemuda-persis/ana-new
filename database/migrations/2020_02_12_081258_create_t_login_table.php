<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTLoginTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_login', function(Blueprint $table)
		{
			$table->integer('id_login', true);
			$table->string('npa', 11);
			$table->string('password');
			$table->string('activ_code');
			$table->integer('email_confirm');
			$table->string('remember_token', 100)->nullable();
            $table->string('reset_password_token', 100)->nullable();
			$table->integer('status_aktif');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_login');
	}

}
