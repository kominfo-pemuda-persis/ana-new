<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonografiPcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_monografi_pc', function (Blueprint $table) {
            $table->bigIncrements('id')->autoIncrement();
            $table->string('kd_pc', 10);
            $table->string('kd_pd', 10);
            $table->string('kd_pw', 10);
            $table->string('provinsi', 2);
            $table->string('kota', 4);
            $table->string('kecamatan', 7);
            $table->double('latitude');
            $table->double('longitude');
            $table->string('alamat_utama');
            $table->string('alamat_alternatif');
            $table->string('no_kontak', 15);
            $table->string('email')->unique()->nullable();
            $table->integer('luas');
            $table->string('bw_utara', 75);
            $table->string('bw_selatan', 75);
            $table->string('bw_timur', 75);
            $table->string('bw_barat', 75);
            $table->integer('jarak_dari_ibukota_negara');
            $table->integer('jarak_dari_ibukota_provinsi');
            $table->integer('jarak_dari_ibukota_kabupaten');
            $table->string('foto')->nullable();
            $table->timestamps();
            $table->foreign('kd_pc')->references('kd_pc')->on('t_pc')->onDelete('cascade');
            $table->foreign('kd_pd')->references('kd_pd')->on('t_pd')->onDelete('cascade');
            $table->foreign('kd_pw')->references('kd_pw')->on('t_pw')->onDelete('cascade');
            $table->foreign('provinsi')->references('id')->on('t_provinsi')->onDelete('cascade');
            $table->foreign('kota')->references('id')->on('t_kabupaten')->onDelete('cascade');
            $table->foreign('kecamatan')->references('id')->on('t_kecamatan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_monografi_pc');
    }
}
