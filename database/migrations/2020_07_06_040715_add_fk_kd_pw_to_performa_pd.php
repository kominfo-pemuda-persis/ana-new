<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkKdPwToPerformaPd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_performa_pd', function (Blueprint $table) {
            $table->string('kd_pw', 10);
            $table->foreign('kd_pw')->references('kd_pw')->on('t_pw')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_performa_pd', function (Blueprint $table) {
            //
        });
    }
}
