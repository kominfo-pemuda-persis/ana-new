<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTCaangOrganisasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_caang_organisasi', function (Blueprint $table) {
            $table->integer('id_organisasi', true);
			$table->integer('id_caang');
			$table->string('nama_organisasi', 100);
			$table->string('jabatan', 50)->nullable();
			$table->string('tingkat', 20)->nullable();
			$table->integer('tahun_mulai')->nullable();
			$table->integer('tahun_selesai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_caang_organisasi');
    }
}
