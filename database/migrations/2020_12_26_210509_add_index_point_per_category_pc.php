<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexPointPerCategoryPc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_performa_pc', function (Blueprint $table) {
            $table->integer('point_c1')->nullable();
            $table->integer('point_c2')->nullable();
            $table->integer('point_c3')->nullable();
            $table->integer('point_c4')->nullable();
            $table->integer('point_c5')->nullable();
            $table->integer('point_c6')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_performa_pc', function (Blueprint $table) {
            $table->dropColumn('point_c1');
            $table->dropColumn('point_c2');
            $table->dropColumn('point_c3');
            $table->dropColumn('point_c4');
            $table->dropColumn('point_c5');
            $table->dropColumn('point_c6');
        });
    }
}
