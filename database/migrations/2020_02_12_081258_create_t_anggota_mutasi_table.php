<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTAnggotaMutasiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_anggota_mutasi', function(Blueprint $table)
		{
			$table->integer('id_mutasi', true);
			$table->integer('id_anggota');
			$table->integer('asal');
			$table->integer('tujuan');
			$table->date('tanggal_mutasi');
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_anggota_mutasi');
	}

}
