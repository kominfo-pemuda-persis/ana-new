<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTCaangPendidikanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_caang_pendidikan', function (Blueprint $table) {
			$table->integer('id_pendidikan', true);
			$table->integer('id_caang');
			$table->smallInteger('id_master_pendidikan');
			$table->string('instansi', 100);
			$table->string('jurusan', 100)->nullable();
			$table->integer('tahun_masuk');
			$table->integer('tahun_keluar');
			$table->enum('jenis_pendidikan', array('Formal','Non Formal'))->nullable()->default('Formal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_caang_pendidikan');
    }
}
