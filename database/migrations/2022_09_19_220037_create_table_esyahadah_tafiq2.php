<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEsyahadahTafiq2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_esyahadah_tafiq2', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('id_tafiq2');
            $table->integer('tahun');
            $table->integer('bulan');
            $table->string('no_urut')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_esyahadah_tafiq2');
    }
}
