<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnggotaTafiqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_anggota_tafiq', function (Blueprint $table) {
            $table->integer('id_tafiq', true);
            $table->integer('id_anggota');
            $table->date('tanggal_masuk');
            $table->date('tanggal_selesai');
            $table->string('lokasi', 100);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_anggota_tafiq');
    }
}
