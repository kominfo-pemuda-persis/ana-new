<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCreatedByAndUpdatedByToTPerformaPw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_performa_pw', function (Blueprint $table) {
          $table->string('created_by', 50);
          $table->string('updated_by', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_performa_pw', function (Blueprint $table) {
          $table->dropColumn('created_by');
          $table->dropColumn('updated_by');
        });
    }
}
