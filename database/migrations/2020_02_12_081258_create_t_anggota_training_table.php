<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTAnggotaTrainingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_anggota_training', function(Blueprint $table)
		{
			$table->integer('id_training', true);
			$table->integer('id_anggota');
			$table->string('nama_training', 50)->nullable();
			$table->string('penyelenggara', 50)->nullable();
			$table->text('tempat', 65535)->nullable();
			$table->date('tanggal_mulai')->nullable();
			$table->date('tanggal_selesai')->nullable();
			$table->enum('jenis', array('Training Pemuda','Training Luar Pemuda'))->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_anggota_training');
	}

}
