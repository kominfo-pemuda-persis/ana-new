<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDateToTAnggotaPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('t_anggota_pekerjaan')->truncate();
        Schema::table('t_anggota_pekerjaan', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE `t_anggota_pekerjaan` CHANGE `tahun_mulai` `tahun_mulai` INT(4) NOT NULL, CHANGE `tahun_selesai` `tahun_selesai` INT(4) NOT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota_pekerjaan', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE `t_anggota_pekerjaan` CHANGE `tahun_mulai` `tahun_mulai` DATE NOT NULL, CHANGE `tahun_selesai` `tahun_selesai` DATE NOT NULL;");
        });
    }
}
