<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDelKetStatToCaangKeluarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_caang_keluarga', function (Blueprint $table) {
            $table->softDeletes();
            $table->string('keterangan', 50)->nullable();
            $table->enum('status_anggota', array('Anggota','Bukan Anggota'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_caang_keluarga', function (Blueprint $table) {
            //
        });
    }
}
