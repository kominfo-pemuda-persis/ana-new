<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTafiq3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_tafiq3', function (Blueprint $table) {
            $table->id();
            $table->string("npa", 7);
            $table->string("lokasi", 50);
            $table->dateTime("tanggal_tafiq_start");
            $table->dateTime("tanggal_tafiq_end");
            $table->integer("kehadiran");
            $table->integer("keaktifan");
            $table->integer("makalah");
            $table->integer("presentasi_makalah");
            $table->integer("pengujian_makalah");
            $table->enum('status',
                [
                    "INIT",
                    "APPROVED_BY_PC",
                    "APPROVED_BY_PD",
                    "APPROVED_BY_PW",
                    "APPROVED_BY_PP"
                ])->default("INIT");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_tafiq3');
    }
}
