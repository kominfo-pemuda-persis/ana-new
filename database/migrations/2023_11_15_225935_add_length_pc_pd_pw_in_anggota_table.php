<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `t_anggota` MODIFY `pc` VARCHAR(25);");
        DB::statement("ALTER TABLE `t_anggota` MODIFY `pd` VARCHAR(25);");
        DB::statement("ALTER TABLE `t_anggota` MODIFY `pw` VARCHAR(25);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota', function (Blueprint $table) {
            //
        });
    }
};
