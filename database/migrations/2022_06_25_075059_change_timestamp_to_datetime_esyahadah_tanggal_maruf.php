<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTimestampToDatetimeEsyahadahTanggalMaruf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_esyahadah_maruf', function () {
            DB::statement("ALTER TABLE `t_esyahadah_maruf` CHANGE `tanggal_maruf` `tanggal_maruf` DATETIME;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_esyahadah_maruf', function () {
            DB::statement("ALTER TABLE `t_esyahadah_maruf` CHANGE `tanggal_maruf` `tanggal_maruf` TIMESTAMP;");
        });
    }
}
