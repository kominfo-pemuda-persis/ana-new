<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempLogTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_temp_log_transaksi', function (Blueprint $table) {
            $table->id();
            $table->string('id_pc');
            $table->bigInteger('id_tagihan')->nullable();
            $table->bigInteger('jml_masuk')->nullable();
            $table->bigInteger('blm_setor')->nullable();
            $table->string('total_setor')->nullable()->default(0);
            $table->string('tahun');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_temp_log_transaksi');
    }
}
