<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTAnggotaKeterampilanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_anggota_keterampilan', function(Blueprint $table)
		{
			$table->integer('id_keterampilan', true);
			$table->integer('id_anggota');
			$table->string('keterampilan', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_anggota_keterampilan');
	}

}
