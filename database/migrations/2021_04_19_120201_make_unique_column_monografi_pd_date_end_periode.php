<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUniqueColumnMonografiPdDateEndPeriode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("t_monografi_pd", function (Blueprint $table) {
            $table->unique(['kd_pd', 'kd_pw', 'start_periode', 'end_periode'], "unique_pd_periode_monografi");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("t_monografi_pd", function (Blueprint $table) {
            $table->dropUnique("unique_pd_periode_monografi");
        });

    }
}
