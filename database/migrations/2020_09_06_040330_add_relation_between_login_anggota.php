<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationBetweenLoginAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Buat relasi antara tabel login dan tabel anggota dengan npa sebagai FK
        Schema::table('t_login', function (Blueprint $table) {
            $table->foreign('npa')->references('npa')->on('t_anggota')->nullable()->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
