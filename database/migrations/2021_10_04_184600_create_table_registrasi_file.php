<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableRegistrasiFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_rekap_caang_file', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->integer("rekap_caang_id");
            $table->text("path_folder");
            $table->text("path");
            $table->text("filetype");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_rekap_caang_file');
    }
}
