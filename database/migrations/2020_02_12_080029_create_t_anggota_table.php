<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTAnggotaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('t_anggota', function(Blueprint $table) {
            $table->integer('id_anggota', true);
            $table->string('npa', 10)->nullable()->index('npa');
            $table->string('nama_lengkap', 50);
            $table->string('tempat_lahir', 50);
            $table->date('tanggal_lahir');
            $table->enum('status_merital', array('SINGLE', 'MENIKAH', 'DUDA'));
            $table->string('pekerjaan', 25);
            $table->string('pw', 10)->nullable();
            $table->string('pd', 10)->nullable();
            $table->string('pc', 10)->nullable();
            $table->string('pj', 10)->nullable();
            $table->string('nama_pj', 100)->nullable();
            $table->string('provinsi', 10);
            $table->string('kota', 10);
            $table->string('kecamatan', 10);
            $table->string('desa', 100);
            $table->string('gol_darah', 3);
            $table->string('email', 50);
            $table->string('no_telpon', 15);
            $table->string('no_telpon2', 15);
            $table->string('alamat', 100);
            $table->enum('jenis_keanggotaan', array('BIASA', 'TERSIAR', 'KEHORMATAN'));
            $table->enum('status_aktif', array('ACTIVE', 'NON ACTIVE', 'TIDAK HEREGISTRASI', 'MUTASI', 'MENINGGAL'));
            $table->string('foto', 100);
            $table->smallInteger('id_otonom');
            $table->date('masa_aktif_kta')->nullable();
            $table->dateTime('reg_date');
            $table->dateTime('last_updated');
            $table->string('created_by', 50);
            $table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('pc')->references('kd_pc')->on('t_pc')->onDelete('cascade');
            $table->foreign('pd')->references('kd_pd')->on('t_pd')->onDelete('cascade');
            $table->foreign('pw')->references('kd_pw')->on('t_pw')->onDelete('cascade');
            $table->foreign('provinsi')->references('id')->on('t_provinsi')->onDelete('cascade');
            $table->foreign('kota')->references('id')->on('t_kabupaten')->onDelete('cascade');
            $table->foreign('kecamatan')->references('id')->on('t_kecamatan')->onDelete('cascade');
            $table->foreign('desa')->references('id')->on('t_desa')->onDelete('cascade');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_anggota');
	}

}
