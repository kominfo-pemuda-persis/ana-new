<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTCaangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_caang', function (Blueprint $table) {
            $table->integer('id_caang', true);
            $table->timestamp('created_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('last_updated');
            $table->string('nama_lengkap', 255);
            $table->string('tempat_lahir', 100);
            $table->string('gol_darah', 3);
            $table->date('tanggal_lahir');
            $table->enum('status_merital', array('SINGLE', 'MENIKAH', 'DUDA'));
            $table->string('pekerjaan', 100);
            $table->text('foto');
            $table->text('alamat');
            $table->string('provinsi', 10);
            $table->string('kota', 10);
            $table->string('kecamatan', 10);
            $table->string('desa', 100);
            $table->string('no_telpon', 15);
            $table->string('no_telpon2', 15);
            $table->text('email');
            $table->integer('pendidikan_terakhir')->nullable();
            $table->dateTime('reg_date');
            $table->foreign('provinsi')->references('id')->on('t_provinsi')->onDelete('cascade');
            $table->foreign('kota')->references('id')->on('t_kabupaten')->onDelete('cascade');
            $table->foreign('kecamatan')->references('id')->on('t_kecamatan')->onDelete('cascade');
            $table->foreign('desa')->references('id')->on('t_desa')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_caang');
    }
}
