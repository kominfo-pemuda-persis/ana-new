<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnKeteranganAnggotaKeluarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_anggota_keluarga', function (Blueprint $table) {
            $table->string('keterangan')->nullable();
            $table->enum('status_anggota', array('Anggota','Bukan Anggota'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_anggota_keluarga', function (Blueprint $table) {
            $table->dropColumn('keterangan');
            $table->dropColumn('status_anggota');
        });
    }
}
