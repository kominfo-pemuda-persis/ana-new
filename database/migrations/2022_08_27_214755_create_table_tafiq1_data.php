<?php

use App\Facades\AnaOnline;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTafiq1Data extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_tafiq1', function (Blueprint $table) {
            $table->id();
            $table->string("npa", 7);
            $table->string("lokasi", 50);
            $table->dateTime("tanggal_tafiq_start");
            $table->dateTime("tanggal_tafiq_end");
            $table->integer("kehadiran");
            $table->integer("keaktifan");
            $table->integer("makalah");
            $table->enum('status', AnaOnline::getStatusApproval())->default("INIT");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_tafiq1');
    }
}
