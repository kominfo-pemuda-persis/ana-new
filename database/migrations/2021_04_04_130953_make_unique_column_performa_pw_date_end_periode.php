<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUniqueColumnPerformaPwDateEndPeriode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("t_performa_pw", function (Blueprint $table) {
            $table->unique(['kd_pw','start_periode', 'end_periode']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("t_performa_pw", function (Blueprint $table) {
            $table->dropUnique(['kd_pw','start_periode', 'end_periode']);
        });
    }
}
