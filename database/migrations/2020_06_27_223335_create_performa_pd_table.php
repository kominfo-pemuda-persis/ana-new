<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerformaPdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_performa_pd', function (Blueprint $table) {
            $table->increments('kd');
            $table->string('kd_pd', 10);
            $table->string('provinsi', 2);
            $table->string('kota', 4);
            $table->string('alamat')->nullable();
            $table->string('ketua_pd');
            $table->string('no_hp')->nullable();
            $table->string('q1a', 150);
            $table->string('q2a', 150);
            $table->string('q3a', 150);
            $table->string('q4a', 150);
            $table->string('q5a', 150);
            $table->string('q6a', 150);
            $table->string('q1b', 150);
            $table->string('q2b', 150);
            $table->string('q3b', 150);
            $table->string('q4b', 150);
            $table->string('q5b', 150);
            $table->string('q6b', 150);
            $table->string('q7b', 150);
            $table->string('q8b', 150);
            $table->string('q9b', 150);
            $table->string('q10b', 150);
            $table->string('q1c', 150);
            $table->string('q2c', 150);
            $table->string('q3c', 150);
            $table->string('q1d', 150);
            $table->string('q2d', 150);
            $table->string('q3d', 150);
            $table->string('q4d', 150);
            $table->string('q5d', 150);
            $table->string('q6d', 150);
            $table->string('q7d', 150);
            $table->string('q8d', 150);
            $table->string('q9d', 150);
            $table->string('q10d', 150);
            $table->string('q11d', 150);
            $table->string('q12d', 150);
            $table->string('q13d', 150);
            $table->string('q14d', 150);
            $table->string('q15d', 150);
            $table->string('q16d', 150);
            $table->string('q17d', 150);
            $table->string('q18d', 150);
            $table->string('q19d', 150);
            $table->string('q20d', 150);
            $table->string('q21d', 150);
            $table->string('q22d', 150);
            $table->string('q23d', 150);
            $table->string('q24d', 150);
            $table->string('q25d', 150);
            $table->string('q26d', 150);
            $table->string('q27d', 150);
            $table->string('q28d', 150);
            $table->string('q1e', 150);
            $table->string('q2e', 150);
            $table->string('q3e', 150);
            $table->string('q4e', 150);
            $table->string('q5e', 150);
            $table->string('q6e', 150);
            $table->string('q7e', 150);
            $table->string('q8e', 150);
            $table->string('q9e', 150);
            $table->string('q10e', 150);
            $table->string('q1f', 150);
            $table->string('q2f', 150);
            $table->string('q3f', 150);
            $table->string('q4f', 150);
            $table->string('q5f', 150);
            $table->string('q6f', 150);
            $table->string('q7f', 150);
            $table->datetime('last_survey');
            $table->timestamps();
            $table->foreign('kd_pd')->references('kd_pd')->on('t_pd')->onDelete('CASCADE');
            $table->foreign('provinsi')->references('id')->on('t_provinsi')->onDelete('cascade');
            $table->foreign('kota')->references('id')->on('t_kabupaten')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_performa_pd');
    }
}
