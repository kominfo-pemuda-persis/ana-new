$(document).ready(function() {
    var t = $('#data_pc').dataTable( {
        // "lengthChange": false,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            // index number column
            var index = iDisplayIndex + this.fnSettings()._iDisplayStart +1;
            $('td:eq(0)',nRow).html(index);
            return nRow;
        },
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,.]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 4 ).footer() ).html(
                pageTotal +' ( '+ total +' total)'
            );
        },
        //disable search and order on index number column
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );

});
