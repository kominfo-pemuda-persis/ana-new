/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
let method;
var BaseTableDatatables = function () {
    var initDataTable = function () {
        window.tableData = jQuery('.js-dataTable-anggota-keluarga').dataTable({
            order: [[0, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1, 4] }],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            processing: true,
            serverSide: false,
            ajax: dataTableUrl,
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                // index number column
                var index = iDisplayIndex + this.fnSettings()._iDisplayStart +1;
                $('td:eq(0)',nRow).html(index);
                return nRow;
            },
            columns: [
                { data: 'DT_RowIndex' },
                {
                    data: 'nama_keluarga',
                    // render: function (data, type, row) {
                    //     let str = '<img src="' + data + '" width="75%" alt="' + row.title + '" class="img img-responsive" >';
                    //     return str;
                    // }
                },
                { data: 'hubungan' },
                {
                    data: 'alamat',
                    render: function (data, type, row) {
                        let str = '<small class="text-muted">'+data+'</small>';
                        return data?str:'';
                    },
                },
                {
                    data: 'id_keluarga',
                    render: function (data, type, row) {
                        let str = '<div class="btn-group float-right"><button type="button" class="btn btn-warning btn-sm" onclick="openModal(\'#modal-form\', \'update\', ' + data + ')" ><i class="si si-pencil"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="remove( ' + data + ')" ><i class="si si-trash"></i></button></div>';
                        return str;
                    },
                }
            ],
        })
    };

    // DataTables Bootstrap integration
    // var bsDataTables = function () {
    //     var $DataTable = jQuery.fn.dataTable;
    //
    //     // Set the defaults for DataTables init
    //     jQuery.extend(true, $DataTable.defaults, {
    //         dom:
    //             '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
    //             '<\'row\'<\'col-sm-12\'tr>>' +
    //             '<\'row\'<\'col-sm-6\'i><\'col-sm-6\'p>>',
    //         renderer: 'bootstrap',
    //         oLanguage: {
    //             sLengthMenu: '_MENU_',
    //             sInfo: 'Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>',
    //             oPaginate: {
    //                 sPrevious: '<i class="fa fa-angle-left"></i>',
    //                 sNext: '<i class="fa fa-angle-right"></i>',
    //             },
    //         },
    //     });
    //
    //     // Default class modification
    //     jQuery.extend(jQuery.fn.dataTable.ext.classes, { sWrapper: "dataTables_wrapper dt-bootstrap4", sFilterInput: "form-control form-control-sm", sLengthSelect: "form-control form-control-sm" }),
    //
    //         jQuery.extend(!0, jQuery.fn.dataTable.defaults, { language: { lengthMenu: "_MENU_", search: "_INPUT_", searchPlaceholder: "Search..", info: "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong>", paginate: { first: '<i class="fa fa-angle-double-left"></i>', previous: '<i class="fa fa-angle-left"></i>', next: '<i class="fa fa-angle-right"></i>', last: '<i class="fa fa-angle-double-right"></i>' } } })
    //
    // };

    var initValidation = function () {
        jQuery('.form-program').validate({
            errorClass: 'invalid-feedback animated fadeIn',
            errorElement: 'div',
            errorPlacement: function (e, r) { jQuery(r).addClass("is-invalid"), jQuery(r).parents(".form-group").append(e) },

            highlight: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid").addClass("is-invalid") },

            success: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid"), jQuery(e).remove() },

            rules: {
                nama_keluarga: {
                    required: true,
                },
                hubungan: {
                    required: true,
                },
            },
            messages: {
                nama_keluarga: {
                    required: 'Masukan nama',
                },
                hubungan: {
                    required: 'Pilih hubungan',
                },
            },

            submitHandler: function (form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('menyimpan..');
                $('.progress-upload-rec').css('width', 0 + '%').attr('aria-valuenow', 0).text(0 + '%');
                $('.progress-add').show();
                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('.progress-upload-rec').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete).text(percentComplete + '%');
                                console.log(percentComplete);
                                if (percentComplete === 100) {
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    url: $form.attr('action'),
                    type: method,
                    data: $form.serialize(),
                    success: function (res) {
                        if (res.data == '1') {
                            form.reset();
                            swal({
                                title: "Berhasil!",
                                text: "Data berhasil disimpan",
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                $('#modal-form').modal('hide');
                                var table = $('.js-dataTable-anggota-keluarga').DataTable();
                                table.ajax.reload();
                            });
                        } else {
                            swal("Oops...", res.data, "error");
                        }
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },

                    error: function (jqXHR, exception) {
                        swal("Oops...", jqXHR.status, "error");
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function () {
            initValidation();
            // bsDataTables();
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// modal
function openModal(target, type, id) {
    $('.progress-add').hide();
    $('#srcDataCrop').val('');

    if (type == 'update') {
        $('.modal-title').text('Ubah Anggota Keluarga');
        $.get('/anggota/keluarga/get/' + id, function (res) {
            let program = res.data;
            method = 'PUT',
                $('.form-program').attr('action', '/anggota/keluarga/' + id + '/update');
            $('.form-program').attr('method', 'PUT');
            $('#nama_keluarga').val(program.nama_keluarga);
            $('#hubungan').val(program.hubungan);
            $('#jumlah_anak').val(program.jumlah_anak);
            $('#alamat').val(program.alamat);

        });
    } else {
        method = 'POST',
            $('.form-program').attr('action', addUrl);
        $('.modal-title').text('Tambah Anggota Keluarga');
        $('#title').val('');
        $('#category').val('');
        $('#isActive').val('');
        $('#imageInput').val('');
    }
    $(target).modal('show');
}

// remove
function remove(id) {
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: '/anggota/keluarga/' + id + '/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function (resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        var table = $('.js-dataTable-anggota-keluarga').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function (resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}

// restore
function restore(id){
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#5c80d1",
        confirmButtonText: "Restore!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: 'trash/'+id+'/restore',
                type: 'PUT',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    swal("Berhasil!", "Data telah berhasil di Restore.", "success").then((result) => {
                        var table = $('.js-dataTable-anggota-keluarga-trash').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function(resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}
