/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
let method;
var BaseTableDatatables = function () {
    var initDataTable = function () {
        window.tableData = jQuery('.js-dataTable-pd').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            order: [[1, 'asc']],
            columnDefs: [{ orderable: false, targets: [0] }],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            processing: true,
            serverSide: false,
            ajax: dataTableUrl,
            columns: [
                { data: 'DT_RowIndex' },
                { data: 'kd_pd' },
                { data: 'nama_pd' },
                { data: 'pw.nama_pw' },
                { data: 'diresmikan',
                render: function(data, _type, _row) {
                    return moment(data).locale('id').format('LL');
                    }
                },
                {
                    data: 'action',
                    render: function (data, _type, row) {
                        if (!row["has_permission"]) {
                            window.tableData.api().columns([5]).visible(false);
                        }
                        return data;
                    }
                },
            ],
            autoWidth: true,
            buttons: [{
                extend: 'copy',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'csv',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'print',
                className: 'btn btn-sm btn-primary'
            }],
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        });
    }

    var initValidation = function () {
        jQuery('.form-program').validate({
            errorClass: 'invalid-feedback animated fadeIn',
            errorElement: 'div',
            errorPlacement: function (e, r) { jQuery(r).addClass("is-invalid"), jQuery(r).parents(".form-group").append(e) },

            highlight: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid").addClass("is-invalid") },

            success: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid"), jQuery(e).remove() },

            submitHandler: function (form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('menyimpan..');
                $('.progress-upload-rec').css('width', 0 + '%').attr('aria-valuenow', 0).text(0 + '%');
                $('.progress-add').show();
                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('.progress-upload-rec').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete).text(percentComplete + '%');
                                console.log(percentComplete);
                                if (percentComplete === 100) {
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    url: $form.attr('action'),
                    type: method,
                    data: $form.serialize(),
                    success: function (res) {
                        if (res.data == '1') {
                            form.reset();
                            swal({
                                title: "Berhasil!",
                                text: "Data berhasil disimpan",
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                $('#modal-form').modal('hide');

                                window.tableData.api().ajax.reload();
                            });
                        } else {
                            swal("Oops... Sepertinya ada yang salah!", res.data, "error");
                        }
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },

                    error: function (jqXHR, exception) {
                        swal("Oops... Sepertinya ada yang salah!", jqXHR.status, "error");
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function () {
            initValidation();
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// modal
function openModal(target, type, id) {
    $('.progress-add').hide();
    $('#srcDataCrop').val('');
    $(target).modal('show');
    $('#pw.picker').val(null).trigger('change');
    if (type == 'update') {
        $('.modal-title').text('Ubah Data PD');
        $('#kd_pd').val("Loading...");
        $('#nama_pd').val("Loading...");
        $.get(`/pd/get/${id}`, function (res) {
            let data = res.data;
            method = 'PUT',
                $('.form-program').attr('action', '/pd/update' );
            $('.form-program').attr('method', 'PUT');

            let newOption = new Option(`${data.pw.kd_pw} - ${data.pw.nama_pw}`, data.pw.kd_pw, true, true);
            $('#pw.picker').append(newOption).trigger('change');

            let kodePd = data.kd_pd;
            let namaPd = data.nama_pd;
            let date = data.diresmikan
            $('#kd_pd').val(kodePd);
            $('#nama_pd').val(namaPd);
            $('#diresmikan').val(date);
            $("#id_prev").val(id);
        });
    } else {
        method = 'POST',
            $('.form-program').attr('action', addUrl);
        $('.modal-title').text('Tambah Data PD');
        $('#kd_pd').val('')
        $('#nama_pd').val('');
        $('#diresmikan').val('');
    }
}

function putData(form, button){
    $form = new FormData($(form)[0]);
    $form.append('_method', 'PUT');

    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('.progress-upload-rec').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete).text(percentComplete + '%');
                    console.log(percentComplete);
                    if (percentComplete === 100) {
                    }
                }
            }, false);
            return xhr;
        },
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        },
        url: $(form).attr('action'),
        type: "POST",
        processData: false,
        contentType : false ,
        data: $form,
        success: function (res) {
            if (res.data == '1') {
                form.reset();
                swal({
                    title: "Berhasil!",
                    text: "Data berhasil disimpan",
                    type: "success",
                    confirmButtonColor: "#3498DB",
                    closeOnConfirm: true
                }).then((result) => {
                    $('#modal-form').modal('hide');

                    window.tableData.api().ajax.reload();
                });
            } else {
                swal("Oops... Sepertinya ada yang salah!", res.data, "error");
            }
            button.removeAttr('disabled');
            button.html('<i class="far fa-save mr-1"></i> Simpan');
        },

        error: function (jqXHR, exception) {
            swal("Oops... Sepertinya ada yang salah!", jqXHR.status, "error");
            button.removeAttr('disabled');
            button.html('<i class="far fa-save mr-1"></i> Simpan');
        },
    })
}

$('#pw.picker').select2({
    width: '100%',
    placeholder: 'Pilih PW...',
    ajax: {
        url: '/get-pw-select2',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: `${item.kd_pw} | ${item.nama_pw}`,
                        id: item.kd_pw
                    }
                })
            };
        },
        cache: true
    }
});

// remove
function remove(id) {
    swal({
        title: "Apakah anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: '/pd/' + id + '/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function (resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        window.tableData.api().ajax.reload();
                    });
                },
                error: function (resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}


