// remove
function remove(id){
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url:'/anggota/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        var table = $('.js-dataTable-anggota').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function(resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}

// destroy
function destroy(id){
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url:'/anggota/trash/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        var table = $('.js-dataTable-anggota-trash').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function(resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}

// restore
function restore(id){
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#5c80d1",
        confirmButtonText: "Restore!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url:'/anggota/trash/'+id+'/restore',
                type: 'PUT',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    swal("Berhasil!", "Data telah berhasil di Restore.", "success").then((result) => {
                        var table = $('.js-dataTable-anggota-trash').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function(resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}
