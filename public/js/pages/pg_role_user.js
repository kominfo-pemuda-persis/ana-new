/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
let method;
var BaseTableDatatables = function () {
    var initDataTable = function () {
        window.tableData = jQuery('.js-dataTable-role-user').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 25, 50, 100, 250, 500, 1000], [5, 10, 25, 50, 100, 250, 500, 1000]],
            order: [[0, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1, 4] }],
            processing: true,
            serverSide: false,
            ajax: dataTableUrl,
            columns: [
                { data: 'DT_RowIndex' },
                {
                    data: 'anggota.npa',
                },
                {
                    data: 'anggota.nama_lengkap',
                },
                { data: 'anggota.pimpinan_cabang.nama_pc' },
                {
                    data: 'email_confirm',
                    render: function (data, type, row) {
                        let str;
                        if (data == 1) {
                            str = ' <i class="fa fa-fw fa-check text-success"></i>';
                        } else {
                            str = '<i class="fa fa-fw fa-times text-danger"></i>';
                        }
                        return str;
                    }
                },
                {
                    data: 'status_aktif',
                    render: function (data, type, row) {
                        let str;
                        if (data == 1) {
                            str = '<span class="badge badge-success">Aktif</span>';
                        } else {
                            str = '<span class="badge badge-danger">Non Aktif</span>';
                        }
                        return str;
                    }
                },
                {
                    data:"roles[0].display_name"
                },
                {
                    data: 'npa',
                    render: function (data, type, row) {
                        let str = `<div class="btn-group float-right"><button  onclick="openModal('#modal-form','update',\'${data}\')" class="btn btn-warning btn-sm"><i class="si si-pencil"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="remove(\'${data}\')" ><i class="si si-trash"></i></button></div>`;
                        return str;
                    },
                }
            ],
            autoWidth: true,
            buttons: [{
                extend: 'copy',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'csv',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'print',
                className: 'btn btn-sm btn-primary'
            }],
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        });
    };

    return {
        init: function () {
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// modal
function openModal(target, type, id) {
    if (type == 'update') {
        $('.modal-title').text('Ubah Hak Akses');
        $.get('/role-user/get/' + id, function (res) {

             let user = res.data;
             console.log(user)

             method = 'PUT';

             $('.form-role').attr('action', '/role-user/update');
             $('.form-role').attr('method', 'PUT');
             $('#nama_lengkap').val(user.anggota.nama_lengkap);
             $('#npa').val(user.anggota.npa);
             $("[name='id_role']").val(user.roles[0].id_role);
             if(user.status_aktif == 1){
                 $('#status_aktif').prop('checked',true);
             }else{
                 $('#status_aktif').prop('checked',false);
             }

            $('#comp_aktif').show();

        });
    } else {
        let addUrl = "{{ route('role.store') }}";
        method = 'POST',
            $('.form-role').attr('action', addUrl);
        $('.modal-title').text('Tambah Role');
        $('#nama_role').val('');
        $('#desc').val('');
        $('#comp_aktif').hide();
    }
    $(target).modal('show');
}


// remove
function remove(id) {
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: 'role-user/delete',
                type: 'POST',
                data: { id: id },
                dataType: 'json',
                success: function (resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        var table = $('.js-dataTable-role-user').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function (resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}
