var FormValidation = function () {
    var initValidation = function () {
        jQuery('.js-wizard-anggota-form').validate({
            rules: {
                npa: {
                    required: true,
                    remote: {
                        url: "/anggota/npa?id="+$('#id').val(),
                        type: "get"
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "/anggota/email?id="+$('#id').val(),
                        type: "get"
                    }
                }
            },
            messages: {
                npa: {
                    required: "Please Enter NPA!",
                    remote: "NPA already in use!"
                },
                email: {
                    required: "Please Enter Email!",
                    email: "This is not a valid email!",
                    remote: "Email already in use!"
                }
            },
            submitHandler: function (form, event) {
                $form = $(form);
                event.preventDefault();
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('menyimpan..');
                $.ajax({
                    url: $form.attr('action'),
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    processData: false,
                    contentType : false,
                    data: new FormData($('.js-wizard-anggota-form')[0]),
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    success: function (res) {
                        console.log(res.data);

                        if (res.data == '1') {
                            form.reset();
                            swal({
                                title: "Berhasil!",
                                text: res.message,
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                window.location = "/data-calon-anggota";
                            });
                        } else {
                            if((typeof res.message) == "object"){
                                var h="<p>";
                                $.each(res.message,function (obj,callback) {
                                    h +=""+callback+" <br>";
                                });
                                h+="</p>";
                                swal("Oops...", h, "error");
                            }else {
                                swal("Oops...", res.message, "error");
                            }
                        }
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },

                    error: function (jqXHR, exception) {
                        if((typeof jqXHR.responseJSON.errors) == "object"){
                            var mess="<p>";
                            $.each(jqXHR.responseJSON.errors,function (obj,callback) {
                                mess +=""+callback+" <br>";
                            });
                            mess+="</p>";
                            swal("Oops...", mess, "error");
                        }else{
                            swal("Oops...", jqXHR.responseJSON.message, "error");
                        }
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function () {
            initValidation();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    FormValidation.init();
});
