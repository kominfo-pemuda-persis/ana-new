/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
let method;
var BaseTableDatatables = function () {
    var initDataTable = function () {
        window.tableData = jQuery('.js-dataTable-role').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            order: [[0, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1, 4] }],
            serverSide: false,
            ajax: dataTableUrl,
            columns: [
                { data: 'DT_RowIndex' },
                { data: 'display_name' },
                { data: 'name' },
                { data: 'description' },
                {
                    data: 'status_aktif',
                    render: function (data, type, row) {
                        let str;
                        if (data == 1) {
                            str = '<span class="badge badge-success">Aktif</span>';
                        } else {
                            str = '<span class="badge badge-danger">Non Aktif</span>';
                        }
                        return str;
                    }
                },
                {
                    data: 'id_role',
                    render: function (data, type, row) {
                        let str = `<div class="btn-group float-right"><a href="/role/permission/${data}" title="assign permissions" class="btn btn-secondary btn-sm"><i class="si si-check"></i></a><button onclick="openModal('#modal-form','update',\'${data}\')" class="btn btn-warning btn-sm"><i class="si si-pencil"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="remove(\'${data}\')" ><i class="si si-trash"></i></button></div>`;
                        return str;
                    },
                }
            ],
            autoWidth: true,
            buttons: [{
                extend: 'copy',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'csv',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'print',
                className: 'btn btn-sm btn-primary'
            }],
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        });
    };

    return {
        init: function () {
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// modal
function openModal(target, type, id) {
    if (type == 'update') {
        $('.modal-title').text('Ubah Role');
        $.get('/role/get/' + id, function (res) {

             let role = res.data;
             method = 'PUT',

             $('.form-role').attr('action', '/role/' + id + '/update');
             $('.form-role').attr('method', 'PUT');
             $('#name').val(role.name);
             $('#display_name').val(role.display_name);
             $("#id_role").val(role.id_role);
             $('#description').val(role.description);
             if(role.status_aktif == 1){
                 $('#status_aktif').prop('checked',true);
             }else{
                 $('#status_aktif').prop('checked',false);
             }

            $('#comp_aktif').show();

        });
    } else {
        let addUrl = "{{ route('role.store') }}";
        method = 'POST',
            $('.form-role').attr('action', addUrl);
        $('.modal-title').text('Tambah Role');
        $('#name').val('');
        $('#display_name').val('');
        $('#description').val('');
        $('#comp_aktif').hide();
    }
    $(target).modal('show');
}


// remove
function remove(id) {
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: 'role/delete',
                type: 'POST',
                data: { id: id },
                dataType: 'json',
                success: function (resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        var table = $('.js-dataTable-role').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function (resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}
