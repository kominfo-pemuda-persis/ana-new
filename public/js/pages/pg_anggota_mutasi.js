/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
let method;
var BaseTableDatatables = function () {
    var initDataTable = function () {
        window.tableData = jQuery('.js-dataTable-anggota-mutasi').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            columnDefs: [{orderable: false, targets: [0]}],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            processing: true,
            serverSide: false,
            ajax: dataTableUrl,
            columns: [
                {data: 'DT_RowIndex'},
                {
                    data: 'anggota',
                    render: function (data, _type, _row) {
                        console.log(_row)
                        let str = `<span class="text-muted">Anggota Tidak Ditemukan</span>`;
                        if (data) {
                            str = `<span class="text-muted">${data.npa} - ${data.nama_lengkap}</span>`;
                        }
                        return str;
                    },
                },
                {
                    data: 'asal',
                    render: function (data, _type, _row) {
                        let str = `<span class="text-muted">${data.kd_pc} | ${data.nama_pc}</span>`;
                        return str;
                    },
                },
                {
                    data: 'tujuan',
                    render: function (data, _type, _row) {
                        let str = `<span class="text-muted">${data.kd_pc} | ${data.nama_pc}</span>`;
                        return str;
                    },
                },
                {
                    data: 'id_mutasi',
                    render: function (_data, _type, row) {
                        let data = moment(row.tanggal_mutasi).format("DD MMMM YYYY")
                        let str = `<span class="text-muted">${data}</span>`;
                        return str;
                    },
                },
                {
                    data: 'action'
                }],
            autoWidth: true,
            buttons: [{
                extend: 'copy',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'csv',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'print',
                className: 'btn btn-sm btn-primary'
            }],
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        });
    }

    var initValidation = function () {
        jQuery('.form-program').validate({
            errorClass: 'invalid-feedback animated fadeIn',
            errorElement: 'div',
            errorPlacement: function (e, r) {
                jQuery(r).addClass("is-invalid"), jQuery(r).parents(".form-group").append(e)
            },

            highlight: function (e) {
                jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid").addClass("is-invalid")
            },

            success: function (e) {
                jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid"), jQuery(e).remove()
            },

            rules: {
                mutasi: {
                    required: true,
                },
            },
            messages: {
                mutasi: {
                    required: 'Masukan Organisasi',
                },
            },

            submitHandler: function (form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('menyimpan..');
                $('.progress-upload-rec').css('width', 0 + '%').attr('aria-valuenow', 0).text(0 + '%');
                $('.progress-add').show();
                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('.progress-upload-rec').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete).text(percentComplete + '%');
                                console.log(percentComplete);
                                if (percentComplete === 100) {
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    url: $form.attr('action'),
                    type: method,
                    data: $form.serialize(),
                    success: function (res) {
                        if (res.data == '1') {
                            form.reset();
                            swal({
                                title: "Berhasil!",
                                text: "Data berhasil disimpan",
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                $('#modal-form').modal('hide');

                                window.tableData.api().ajax.reload();
                            });
                        } else {
                            swal("Oops...", res.data, "error");
                        }
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },

                    error: function (jqXHR, exception) {
                        swal("Oops...", jqXHR.status, "error");
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function () {
            initValidation();
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
});

/* PROCCESSING */

// modal
function openModal(target, type, id) {
    $('.progress-add').hide();
    $('#srcDataCrop').val('');

    if (type == 'update') {
        $('.modal-title').text('Ubah Riwayat Mutasi');
        $('.picker').val(null).trigger('change');
        $('.pc_asal').val(null).trigger('change');
        $('.pc_tujuan').val(null).trigger('change');
        $.get('/mutasi/get/' + id, function (res) {
            let dataEdit = res.data;
            method = 'PUT',
            $('.form-program').attr('action', '/mutasi/' + id);
            $('.form-program').attr('method', 'PUT');

            let newOption = new Option(`${dataEdit.anggota.npa} - ${dataEdit.anggota.nama_lengkap}`, dataEdit.anggota.id_anggota, true, true);
            $('.picker').append(newOption).trigger('change');

            let setAsal = new Option(`${dataEdit.asal.kd_pc} | ${dataEdit.asal.nama_pc}`, dataEdit.asal.kd_pc, true, true);
            $('.pc_asal').append(setAsal).trigger('change');

            let setTujuan = new Option(`${dataEdit.tujuan.kd_pc} ${dataEdit.tujuan.nama_pc}`, dataEdit.tujuan.kd_pc, true, true);
            $('.pc_tujuan').append(setTujuan).trigger('change');

            $('#tanggal_mutasi').val(dataEdit.tanggal_mutasi);
        });
    } else {
        method = 'POST',
        $('.form-program').attr('action', addUrl);
        $('.modal-title').text('Tambah Riwayat Mutasi');
        $('.picker').val(null).trigger('change');
        $('.pc_asal').val(null).trigger('change');
        $('.pc_tujuan').val(null).trigger('change');
        $('#tanggal_mutasi').val('');
    }
    $(target).modal('show');
}

// remove
function remove(id) {
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: '/mutasi/' + id + '/delete',
                type: 'DELETE',
                data: {id: id},
                dataType: 'json',
                success: function (resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        window.tableData.api().ajax.reload();
                    });
                },
                error: function (resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}

// select2
$('.picker').select2({
    width: '100%',
    placeholder: 'Cari Anggota...',
    ajax: {
        url: '/anggota/picker',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: `${item.npa} - ${item.nama_lengkap}`,
                        id: item.id_anggota,
                        pimpinan_cabang: item.pimpinan_cabang
                    }
                })
            };
        },
        cache: true
    }
});

$('.picker').on('select2:select', function (e) {
    $('.pc_asal').val(null).trigger('change');
    var data = e.params.data;
    if (data.pimpinan_cabang) {
        let setAsal = new Option(`${data.pimpinan_cabang.kd_pc} | ${data.pimpinan_cabang.nama_pc}`, data.pimpinan_cabang.kd_pc, true, true);
        $('.pc_asal').append(setAsal).trigger('change');
    }
});

$('.pc_asal').select2({
    width: '100%',
    placeholder: 'Pilih Asal...',
    ajax: {
        url: '/jamiyyah/picker',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: `${item.kd_pc} | ${item.nama_pc}`,
                        id: item.kd_pc
                    }
                })
            };
        },
        cache: true
    }
});

$('.pc_tujuan').select2({
    width: '100%',
    placeholder: 'Pilih Tujuan...',
    ajax: {
        url: '/jamiyyah/picker',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: `${item.kd_pc} | ${item.nama_pc}`,
                        id: item.kd_pc
                    }
                })
            };
        },
        cache: true
    }
});
