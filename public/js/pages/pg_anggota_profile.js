var $modal = $('#modal-crop');
var image = document.getElementById('image-crop');
var cropper;
$('#foto-upload').change(function (e) {
    var files = e.target.files;
    var done = function (url) {
        image.src = url;
        $modal.modal('show');
    };
    var reader;
    var file;
    var url;
    if (files && files.length > 0) {
        if (this.files[0].size > 5120000) {
            $('input#foto-upload.valid').attr('src', '');
            swal("Mohon maaf!!!", "Ukuran foto lebih dari 5 MB", "error");
            $('#foto-upload').val("")
        } else {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function (e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    }
});

$modal.on('shown.bs.modal', function () {
  cropper = new Cropper(image, {
      aspectRatio: 1,
      preview: '.preview'
  });
}).on('hidden.bs.modal', function () {
    if(cropper !== null){
        cropper.destroy();
        cropper = null;
    }
}).on("click","#cancel-crop, #cancel-crop-close", function(){
    $('#foto-upload').val(null);
    if(cropper !== null){
        cropper.destroy();
        cropper = null;
    }
});

$("#save-foto").click(function () {
  canvas = cropper.getCroppedCanvas({
      width: 160,
      height: 160,
  });

  canvas.toBlob(function (blob) {
      url = URL.createObjectURL(blob);
      var reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = function () {
          var base64data = reader.result;
          $modal.modal('hide');
          $.ajax({
            type: 'put',
            url: updateFotoUrl,
            data: {
              _token: $('meta[name="csrf-token"]').attr('content'),
              foto: base64data
            },
            beforeSend: function () {
                swal({
                    title: 'Updating Foto..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    buttons: false,
                    onOpen: () => {
                        swal.showLoading()
                    }
                });
            },
          }).done(function (resp, textStatus ) {
            if (!!resp.data) {
                swal.close();
                swal("Berhasil!", "Foto Profile telah berhasil diperbarui.", "success").then((result) => {
                    window.location.href = window.location.href;
                });
            }
          }).fail(function (resp, textStatus ) {
            swal.close();
            swal("Error!", "Foto Profile gagal disimpan. Detail " + resp.message, "error");
            $('#foto-upload').val("")
          });
      }
  });
})
