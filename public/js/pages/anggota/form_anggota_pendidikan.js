$(function () {
    disableRemovePendidikan();

});

$(document).on('click', '.addPendidikan', function () {
    addPendidikan(false);
    disableRemovePendidikan();
});

$(document).on('click', '.removePendidikan', function () {
    $(this).closest('tr').remove();
    setOrderPendidikan();
    disableRemovePendidikan();
});

$(document).on('change','.masterPendidikan',function () {
    hideJurusan($(this).closest('tr'));
});

function disableRemovePendidikan() {
    var data = $('#pendidikan_table tbody').children();
    var dataLen = data.length;
    if (dataLen<2) {
        $("#pendidikan_table tbody").find(".removePendidikan").attr("disabled", true);
    }else{
        $("#pendidikan_table tbody").find(".removePendidikan").attr("disabled", false);
    }
}

function addPendidikan(data) {
    var nomor = $('#pendidikan_table tbody').children().length;
    var num = nomor + 1;
    var html = '';
    var pendidikanId = "", jenisPendidikan="Formal",jurusan="",instansi ="", masterPendidikan="", tahunMasuk="", tahunKeluar="";
    if (data) {
        pendidikanId = data.id_pendidikan;
        tahunMasuk = data.tahun_masuk;
        tahunKeluar = data.tahun_keluar;
        jenisPendidikan = data.jenis_pendidikan?data.jenis_pendidikan:jenisPendidikan;
        masterPendidikan = data.id_master_pendidikan;
        jurusan = data.jurusan?data.jurusan:"";
        instansi = data.instansi?data.instansi:"";
    }


    html += '<tr>' +
            '<td class="num">'+num+'</td>' +
            '<td>' +
                '<div class="form-group">' +
                    '<label for="anggota-email">Tingkat Pendidikan</label>' +
                    // '<input class="form-control pendidikanId" value="'+pendidikanId+'" type="hidden" name="pendidikan.id_pendidikan['+nomor+']" >' +
                    '<select class="form-control masterPendidikan" name="pendidikan.id_master_pendidikan['+nomor+']" required>' +
                        '<option value="" >Pilih Tingkat Pendidikan </option>' ;
                        $.each(listPendidikan, function (index, obj) {
                            html+='<option value="'+obj.id_tingkat_pendidikan+'" >'+obj.pendidikan+' </option>';
                        });
    html+='</select>' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="anggota-email">Jenis Pendidikan</label>' +
                    '<select class="form-control jenisPendidikan" name="pendidikan.jenis_pendidikan['+nomor+']" >' +
                        '<option value="Formal" >Formal </option>' +
                        '<option value="Non Formal" >Non Formal </option>' +
                    '</select>' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="anggota-no-telpon">Nama Instansi</label>' +
                    '<input class="form-control instansi" value="'+instansi+'" placeholder="Nama Instansi" type="text" name="pendidikan.instansi['+nomor+']" required>' +
                '</div>' +
                '<div class="form-group jurusanGroup">' +
                    '<label for="anggota-no-telpon">Jurusan</label>' +
                    '<input class="form-control jurusan" value="'+jurusan+'" placeholder="Jurusan" type="text" name="pendidikan.jurusan['+nomor+']">' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="anggota-no-telpon">Tahun Masuk</label>' +
                    // '<input class="form-control tahunMasukShow" type="text" required>' +
                    '<input class="form-control tahunMasuk datepicker"  name="pendidikan.tahun_masuk['+nomor+']" required>' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="anggota-no-telpon">Tahun Keluar</label>' +
                    // '<input class="form-control tahunKeluarShow" type="text" required>' +
                    '<input class="form-control tahunKeluar datepicker"  name="pendidikan.tahun_keluar['+nomor+']" required>' +
                '</div>' +

            '</td>' +
            '<td>' +
                '<button type="button" class="btn btn-danger btn-sm removePendidikan" title="Delete"><i class="si si-trash" aria-hidden="true"></i></button></td>'+
            '</td>' +
        '</tr>';

    $('#pendidikan_table tbody').append(html).after(function () {
        hideJurusan($(this).children(":last-child"));
        $(this).children(":last-child").find(".masterPendidikan").val(masterPendidikan);
        $(this).children(":last-child").find(".jenisPendidikan").val(jenisPendidikan);
        if(tahunMasuk)$(this).children(":last-child").find(".tahunMasuk").val(tahunMasuk);
        if(tahunKeluar)$(this).children(":last-child").find(".tahunKeluar").val(tahunKeluar);
    });
}


function hideJurusan(html) {
    if(html.find(".masterPendidikan").val() < 10 || !html.find(".masterPendidikan").val()){
        html.find(".jurusanGroup").attr('hidden',true);
    }else {
        html.find(".jurusanGroup").attr('hidden',false);
    }
}

function setOrderPendidikan() {
    var data = $('#pendidikan_table tbody').children();
    var dataLen = data.length;
    if (dataLen) {
        for (var i = 0; i < dataLen; i++) {
            data.eq(i).find(".num").html(i + 1);
            data.eq(i).find(".pendidikanId").attr('name', 'pendidikan.id_pendidikan[' + i + ']');
            data.eq(i).find(".masterPendidikan").attr('name', 'pendidikan.id_master_pendidikan[' + i + ']');
            data.eq(i).find(".jenisPendidikan").attr('name', 'pendidikan.jenis_pendidikan[' + i + ']');
            data.eq(i).find(".instansi").attr('name', 'pendidikan.instansi[' + i + ']');
            data.eq(i).find(".jurusan").attr('name', 'pendidikan.jurusan[' + i + ']');
            data.eq(i).find(".tahunMasuk").attr('name', 'pendidikan.tahun_masuk[' + i + ']');
            data.eq(i).find(".tahunKeluar").attr('name', 'pendidikan.tahun_keluar[' + i + ']');
        }
    }
}
