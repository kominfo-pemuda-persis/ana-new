$(document).ready(function () {
    disableRemoveKeluarga();
});

$(document).on('click', '.addKelauarga', function () {
    addKeluarga(false);
    disableRemoveKeluarga();
});

$(document).on('click', '.removeKeluarga', function () {
    $(this).closest('tr').remove();
    setOrderKeluarga();
    disableRemoveKeluarga();
});

function disableRemoveKeluarga() {
    var data = $('#keluarga_table tbody').children();
    var dataLen = data.length;
    if (dataLen<2) {
        $("#keluarga_table tbody").find(".removeKeluarga").attr("disabled", true);
    }else{
        $("#keluarga_table tbody").find(".removeKeluarga").attr("disabled", false);
    }
}

function addKeluarga(data) {
    var nomor = $('#keluarga_table tbody').children().length;
    var num = nomor + 1;
    var html = '';
    var keluargaId = "", hubungan = "", namaKeluarga = "", statusAnggota = "Bukan Anggota", jumlahAnak = 0, keterangan = "", alamat = "";

    if (data) {
        keluargaId = data.id_keluarga;
        hubungan = data.hubungan;
        namaKeluarga = data.nama_keluarga;
        statusAnggota = data.status_anggota;
        jumlahAnak = data.jumlah_anak;
        keterangan = data.keterangan?data.keterangan:keterangan;
        alamat = data.alamat?data.alamat:alamat;
    }

    html += '<tr>' +
            '<td class="num">'+num+'</td>' +
            '<td>' +
                '<div class="form-group">' +
                    '<label for="anggota-email">Hubungan</label>' +
                    // '<input class="form-control keluargaId" value="'+keluargaId+'" type="hidden" name="keluarga.id_keluarga['+nomor+']" >' +
                    '<select class="form-control hubungan" name="keluarga.hubungan['+nomor+']" required>' +
                        '<option value="" >Pilih Hubungan </option>' +
                        '<option value="Suami" >Suami </option>' +
                        '<option value="Istri" >Istri </option>' +
                        '<option value="Ayah" >Ayah </option>' +
                        '<option value="Ibu" >Ibu </option>' +
                        '<option value="Anak" >Anak </option>' +
                    '</select>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-md-6">' +
                        '<div class="form-group">' +
                            '<label for="anggota-desa">Nama</label>' +
                            '<input class="form-control namaKeluarga" value="'+namaKeluarga+'" type="text" name="keluarga.nama_keluarga['+nomor+']" required>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-6">' +
                        '<div class="form-group">' +
                            '<label style="display: block" for="anggota-alamat">Status</label>' +
                            '<input type="radio" class="flat-red statusAnggota" name="keluarga.status_anggota['+nomor+']" value="Anggota" required=""><span class="mr-2 ml-1">Anggota</span>' +
                            '<input type="radio" class="flat-red statusBukanAnggota" name="keluarga.status_anggota['+nomor+']" value="Bukan Anggota" required=""><span class="mr-2 ml-1">Bukan Anggota</span>\n' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="anggota-email">Jumlah Anak</label>' +
                    '<input class="form-control jumlahAnak" value="'+jumlahAnak+'" type="number" min="0" name="keluarga.jumlah_anak['+nomor+']">' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="anggota-no-telpon">Keterangan</label>' +
                    '<input class="form-control keterangan" value="'+keterangan+'" type="text" name="keluarga.keterangan['+nomor+']">' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="anggota-no-telpon">Alamat</label>' +
                    '<input class="form-control alamat" value="'+alamat+'" type="text" name="keluarga.alamat['+nomor+']" >' +
                '</div>' +
            '</td>' +
            '<td>' +
                '<button type="button" class="btn btn-danger btn-sm removeKeluarga" title="Delete"><i class="si si-trash" aria-hidden="true"></i></button></td>'+
            '</td>' +
        '</tr>';

    $('#keluarga_table tbody').append(html).after(function () {
        $(this).children(":last-child").find(".hubungan").val(hubungan);
        if(statusAnggota == "Anggota"){
            $(this).children(":last-child").find(".statusAnggota").prop('checked', true);
            $(this).children(":last-child").find(".statusBukanAnggota").prop('checked', false);
        }else{
            $(this).children(":last-child").find(".statusAnggota").prop('checked', false);
            $(this).children(":last-child").find(".statusBukanAnggota").prop('checked', true);
        }

    });
}

function setOrderKeluarga() {
    var data = $('#keluarga_table tbody').children();
    var dataLen = data.length;
    if (dataLen) {
        for (var i = 0; i < dataLen; i++) {
            data.eq(i).find(".num").html(i + 1);
            data.eq(i).find(".Id").attr('name', 'keluarga.id_anggota[' + i + ']');
            data.eq(i).find(".keluargaId").attr('name', 'keluarga.id_keluarga[' + i + ']');
            data.eq(i).find(".hubungan").attr('name', 'keluarga.hubungan[' + i + ']');
            data.eq(i).find(".namaKeluarga").attr('name', 'keluarga.nama_keluarga[' + i + ']');
            data.eq(i).find(".statusAnggota").attr('name', 'keluarga.status_anggota[' + i + ']');
            data.eq(i).find(".statusBukanAnggota").attr('name', 'keluarga.status_anggota[' + i + ']');
            data.eq(i).find(".jumlahAnak").attr('name', 'keluarga.jumlah_anak[' + i + ']');
            data.eq(i).find(".keterangan").attr('name', 'keluarga.keterangan[' + i + ']');
            data.eq(i).find(".alamat").attr('name', 'keluarga.alamat[' + i + ']');
        }
    }
}
