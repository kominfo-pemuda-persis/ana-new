$(document).ready(function () {
    disableRemoveOrganisasi();
    $(".tahunMulai").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
    $(".tahunSelesai").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
});

$(document).on('click', '.addOrganisasi', function () {
    addOrganisasi(false);
    disableRemoveOrganisasi();
});

$(document).on('click', '.removeOrganisasi', function () {
    $(this).closest('tr').remove();
    setOrderOrganisasi();
    disableRemoveOrganisasi();
});

function disableRemoveOrganisasi() {
    var data = $('#organisasi_table tbody').children();
    var dataLen = data.length;
    if (dataLen<2) {
        $("#organisasi_table tbody").find(".removeOrganisasi").attr("disabled", true);
    }else{
        $("#organisasi_table tbody").find(".removeOrganisasi").attr("disabled", false);
    }
}

function addOrganisasi(data) {
    var nomor = $('#organisasi_table tbody').children().length;
    var num = nomor + 1;
    var html = '';
    var organisasiId = "", namaOrganisasi = "", jabatan = "", tingkat = "", lokasi = "", tahunMulai="", tahunSelesai="";

    if (data) {
        organisasiId = data.id_organisasi;
        namaOrganisasi = data.nama_organisasi;
        jabatan = data.jabatan;
        tingkat = data.tingkat;
        lokasi = data.lokasi;
        tahunMulai = data.tahun_mulai;
        tahunSelesai = data.tahun_selesai;
    }

    html += '<tr>' +
            '<td class="num">'+num+'</td>' +
            '<td>' +
                '<div class="form-group">' +
                    '<label for="nama-organisasi">Nama Organisasi</label>' +
                    // '<input class="form-control organisasiId" value="'+organisasiId+'" type="hidden" name="organisasi.id_organisasi['+nomor+']" >' +
                    '<input class="form-control namaOrganisasi" placeholder="Nama Organisasi" value="'+namaOrganisasi+'" type="text" name="organisasi.nama_organisasi['+nomor+']" required>' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="jabatan">Jabatan</label>' +
                    '<input class="form-control jabatan" placeholder="Jabatan" value="'+jabatan+'" type="text" name="organisasi.jabatan['+nomor+']">' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="tingkat">Tingkat</label>' +
                    '<input class="form-control tingkat" placeholder="Tingkat" value="'+tingkat+'" type="text" name="organisasi.tingkat['+nomor+']">' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="lokasi">Lokasi</label>' +
                    '<input class="form-control lokasi" placeholder="Lokasi" value="'+lokasi+'" type="text" name="organisasi.lokasi['+nomor+']" required>' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="tahun-mulai">Tahun Mulai</label>' +
                    '<input class="form-control tahunMulai datepicker" value="'+tahunMulai+'" name="organisasi.tahun_mulai['+nomor+']" >' +
                '</div>' +
                '<div class="form-group">' +
                    '<label for="tahun-selesai">Tahun Selesai</label>' +
                    '<input class="form-control tahunSelesai datepicker" value="'+tahunSelesai+'" name="organisasi.tahun_selesai['+nomor+']" >' +
                '</div>' +
            '</td>' +
            '<td>' +
                '<button type="button" class="btn btn-danger btn-sm removeOrganisasi" title="Delete"><i class="si si-trash" aria-hidden="true"></i></button></td>'+
            '</td>' +
        '</tr>';

    $('#organisasi_table tbody').append(html).after(function () {
        // $(this).children(":last-child").find(".hubungan").val(hubungan);


    });
}

function setOrderOrganisasi() {
    var data = $('#organisasi_table tbody').children();
    var dataLen = data.length;
    if (dataLen) {
        for (var i = 0; i < dataLen; i++) {
            data.eq(i).find(".num").html(i + 1);
            data.eq(i).find(".organisasiId").attr('name', 'organisasi.id_organisasi[' + i + ']');
            data.eq(i).find(".namaOrganisasi").attr('name', 'organisasi.nama_organisasi[' + i + ']');
            data.eq(i).find(".jabatan").attr('name', 'organisasi.jabatan[' + i + ']');
            data.eq(i).find(".tingkat").attr('name', 'organisasi.tingkat[' + i + ']');
            data.eq(i).find(".lokasi").attr('name', 'organisasi.lokasi[' + i + ']');
            data.eq(i).find(".tahunMulai").attr('name', 'organisasi.tahun_mulai[' + i + ']');
            data.eq(i).find(".tahunSelesai").attr('name', 'organisasi.tahun_selesai[' + i + ']');
        }
    }
}
