$(document).ready(function () {
    //disableRemoveTafiq();
});

$(document).on('click', '.addTafiq', function () {
    addTafiq(false);
    //disableRemoveTafiq();
});

$(document).on('click', '.removeTafiq', function () {
    $(this).closest('tr').remove();
    setOrderTafiq();
    //disableRemoveTafiq();
});

$(document).on('change', '.level_tafiq', function () {
    var val = $(".level_tafiq").val();
    var data = $('#tafiq_table tbody').children();
    var dataLen = data.length;
    if(dataLen>val){
        for(var i=dataLen; i>val; i--) {
            $("#tafiq_table tbody").children(":last-child").remove();
        }
    }
    if(dataLen<val){
        for(var i=dataLen+1; i<=val; i++) {
            addTafiq(false);
        }
    }

});


function disableRemoveTafiq() {
    var data = $('#tafiq_table tbody').children();
    var dataLen = data.length;
    if (dataLen<2) {
        $("#tafiq_table tbody").find(".removeTafiq").attr("disabled", true);
    }else{
        $("#tafiq_table tbody").find(".removeTafiq").attr("disabled", false);
    }
}

function addTafiq(data) {
    var nomor = $('#tafiq_table tbody').children().length;
    var num = nomor + 1;
    var html = '';
    var masuk = "", selesai  = "", lokasi = "";

    if (data) {
        masuk = (new Date(data.tanggal_masuk)).toISOString().slice(0,10);
        selesai = (new Date(data.tanggal_selesai)).toISOString().slice(0,10);
        lokasi = data.lokasi;
    }


    html += '<tr>' +
            '<td class="num">'+num+'</td>' +
            '<td>' +
                '<div class="row">' +
                    '<div class="form-group col-sm-4">' +
                        '<label class="control-label">Dari Tanggal</label>' +
                        '<input class="form-control masuk" value="'+masuk+'" type="date" placeholder="Tanggal Mulai" name="tafiq.tanggal_masuk['+nomor+']" required>' +
                    '</div>' +
                    '<div class="form-group col-sm-4">' +
                        '<label class="control-label">Sampai Tanggal</label>' +
                        '<input class="form-control selesai" value="'+selesai+'" type="date" placeholder="Tanggal Selesai" name="tafiq.tanggal_selesai['+nomor+']" required>' +
                    '</div>' +
                    '<div class="form-group col-sm-4">' +
                        '<label for="lokasi" class="control-label">Lokasi</label>' +
                        '<input type="text" class="form-control lokasi" value="'+lokasi+'" placeholder="Lokasi Tafiq" name="tafiq.lokasi['+nomor+']" required>' +
                    '</div>' +
                '</div>' +
            '</td>' +
            '<td>' +
                // '<button type="button" class="btn btn-danger btn-sm removeTafiq" title="Delete"><i class="si si-trash" aria-hidden="true"></i></button></td>'+
            '</td>' +
        '</tr>';

    $('#tafiq_table tbody').append(html).after(function () {

    });
}

function setOrderTafiq() {
    var data = $('#tafiq_table tbody').children();
    var dataLen = data.length;
    if (dataLen) {
        for (var i = 0; i < dataLen; i++) {
            data.eq(i).find(".num").html(i + 1);
            data.eq(i).find(".masuk").attr('name', 'tafiq.tanggal_masuk[' + i + ']');
            data.eq(i).find(".selesai").attr('name', 'tafiq.tanggal_selesai[' + i + ']');
            data.eq(i).find(".lokasi").attr('name', 'tafiq.lokasi[' + i + ']');
        }
    }
}
