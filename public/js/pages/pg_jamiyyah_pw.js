/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
let method;
var BaseTableDatatables = function () {
    var initDataTable = function () {
        window.tableData = jQuery('.js-dataTable-pw').dataTable({
          pageLength: 5,
          lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
          order: [[1, 'asc']],
          columnDefs: [{ orderable: false, targets: [0] }],
          pageLength: 10,
          lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
          processing: true,
          serverSide: false,
          ajax: dataTableUrl,
          columns: [
              { data: 'DT_RowIndex' },
              { data: 'kd_pw' },
              { data: 'nama_pw' },
              { data: 'diresmikan',
              render: function(data, _type, _row) {
                return moment(data).locale('id').format('LL');
                }
              },
              {
                  data: 'action',
                  render: function(data, _type, row) {
                      if (!row["has_permission"]) {
                          window.tableData.api().columns([4]).visible(false);
                      }
                    return data;
                  }
              }
          ],
          autoWidth: true,
          buttons: [{
              extend: 'copy',
              className: 'btn btn-sm btn-primary'
              }, {
              extend: 'csv',
              className: 'btn btn-sm btn-primary'
              }, {
              extend: 'print',
              className: 'btn btn-sm btn-primary'
          }],
          dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
      });
    }

    var initValidation = function () {
        jQuery('.form-program').validate({
            errorClass: 'invalid-feedback animated fadeIn',
            errorElement: 'div',
            errorPlacement: function (e, r) { jQuery(r).addClass("is-invalid"), jQuery(r).parents(".form-group").append(e) },

            highlight: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid").addClass("is-invalid") },

            success: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid"), jQuery(e).remove() },

            submitHandler: function (form, event) {
                event.preventDefault();
                var button = $(form).find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('menyimpan..');
                $('.progress-upload-rec').css('width', 0 + '%').attr('aria-valuenow', 0).text(0 + '%');
                $('.progress-add').show();

                if(method == "PUT"){
                    putData(form, button)
                }else{
                    postData(form, button)
                }

;
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function () {
            initValidation();
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// modal
function openModal(target, type, id) {
    $('.progress-add').hide();
    $('#srcDataCrop').val('');
    if (type == 'update') {
      $('.modal-title').text('Ubah Data PW');
        $('#kd_pw').val("Loading...");
        $('#nama_pw').val("Loading...");
        $.get(`/jamiyyah/pw/get/${id}`, function (res) {
            let data = res.data;
            method = 'PUT',
            $('.form-program').attr('action', '/jamiyyah/pw/update-data' );
            $('.form-program').attr('method', 'PUT');

            let kodePw = data.kd_pw;
            let namaPw = data.nama_pw;
            let date = data.diresmikan
            $('#kd_pw').val(kodePw);
            $('#nama_pw').val(namaPw);
            $('#diresmikan').val(date);
            $("#id_prev").val(id);
        });
    } else {
        method = 'POST',
        $('.form-program').attr('action', addUrl);
        $('.modal-title').text('Tambah Data PW');
        $('#kd_pw').val('')
        $('#nama_pw').val('');
        $('#diresmikan').val('');
    }
    $(target).modal('show');
}

function postData(form, button){
    $form = $(form);
    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('.progress-upload-rec').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete).text(percentComplete + '%');
                    console.log(percentComplete);
                    if (percentComplete === 100) {
                    }
                }
            }, false);
            return xhr;
        },
        url: $(form).attr('action'),
        type: "POST",
        data: $form.serialize(),
        success: function (res) {
            if (res.data == '1') {
                form.reset();
                swal({
                    title: "Berhasil!",
                    text: "Data berhasil disimpan",
                    type: "success",
                    confirmButtonColor: "#3498DB",
                    closeOnConfirm: true
                }).then((result) => {
                    $('#modal-form').modal('hide');

                    window.tableData.api().ajax.reload();
                });
            } else {
                swal("Oops... Sepertinya ada yang salah!", res.data, "error");
            }
            button.removeAttr('disabled');
            button.html('<i class="far fa-save mr-1"></i> Simpan');
        },

        error: function (jqXHR, exception) {
            swal("Oops... Sepertinya ada yang salah!", jqXHR.status, "error");
            button.removeAttr('disabled');
            button.html('<i class="far fa-save mr-1"></i> Simpan');
        },
    })
}

function putData(form, button){
    $form = new FormData($(form)[0]);
    $form.append('_method', 'PUT');

    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('.progress-upload-rec').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete).text(percentComplete + '%');
                    console.log(percentComplete);
                    if (percentComplete === 100) {
                    }
                }
            }, false);
            return xhr;
        },
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        },
        url: $(form).attr('action'),
        type: "POST",
        processData: false,
        contentType : false ,
        data: $form,
        success: function (res) {
            if (res.data == '1') {
                form.reset();
                swal({
                    title: "Berhasil!",
                    text: "Data berhasil disimpan",
                    type: "success",
                    confirmButtonColor: "#3498DB",
                    closeOnConfirm: true
                }).then((result) => {
                    $('#modal-form').modal('hide');

                    window.tableData.api().ajax.reload();
                });
            } else {
                swal("Oops... Sepertinya ada yang salah!", res.data, "error");
            }
            button.removeAttr('disabled');
            button.html('<i class="far fa-save mr-1"></i> Simpan');
        },

        error: function (jqXHR, exception) {
            swal("Oops... Sepertinya ada yang salah!", jqXHR.status, "error");
            button.removeAttr('disabled');
            button.html('<i class="far fa-save mr-1"></i> Simpan');
        },
    })
}

// remove
function remove(id) {
    swal({
        title: "Apakah anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: 'pw/' + id + '/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function (resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        window.tableData.api().ajax.reload();
                    });
                },
                error: function (resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}
