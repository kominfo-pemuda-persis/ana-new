/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
let method;
var BaseTableDatatables = function () {
    var initDataTable = function () {
        window.tableData = jQuery('.js-dataTable-anggota-pendidikan').dataTable({
          pageLength: 5,
          lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
          order: [[1, 'asc']],
          columnDefs: [{ orderable: false, targets: [0, 1, 6] }],
          pageLength: 10,
          lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
          processing: true,
          serverSide: false,
          ajax: dataTableUrl,
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                // index number column
                var index = iDisplayIndex + this.fnSettings()._iDisplayStart +1;
                $('td:eq(0)',nRow).html(index);
                return nRow;
            },
          columns: [
              { data: 'DT_RowIndex' },
              { data: 'id_master_pendidikan', render: function (data, type, row) {
                // switch(data) {
                //     case '2':
                //         return `<span class="text-muted">SMP/MTS</span>`;
                //     case '1':
                //         return `<span class="text-muted">SD/MI</span>`;
                //     case '3':
                //         return `<span class="text-muted">SMA/SMK/MA</span>`;
                //     case '4':
                //         return `<span class="text-muted">D1</span>`;
                //     case '5':
                //         return `<span class="text-muted">D2</span>`;
                //     case '6':
                //         return `<span class="text-muted">D3</span>`;
                //     case '7':
                //         return `<span class="text-muted">D4</span>`;
                //     case '8':
                //         return `<span class="text-muted">S1</span>`;
                //     case '9':
                //         return `<span class="text-muted">S2</span>`;
                //     case '10':
                //         return `<span class="text-muted">S3</span>`;
                //     default:
                //       return '-'
                //   }
                      return row.master_pendidikan?row.master_pendidikan.pendidikan:data;
              } },
              {
                data: 'instansi',
                render: function (data, type, row) {
                  let str = `<small class="text-muted">${data}</small>`;
                  return str;
              },
              },
              {
                data: 'jurusan',
                render: function (data, type, row) {
                    if (data) {
                        let str = `<small class="text-muted">${data}</small>`;
                        return str;
                    } else {
                        return '-';
                    }
              },
              },
              {
                data: 'id_pendidikan',
                render: function (data, type, row) {
                  let str = `<small class="text-muted">${row.tahun_masuk} s/d ${row.tahun_keluar}</small>`;
                  return str;
              }},
              {
                data: 'jenis_pendidikan',
                render: function (data, type, row) {
                  let str = `<small class="text-muted">${data}</small>`;
                  return str;
              },
              },
              {
                  data: 'id_pendidikan',
                  render: function (data, type, row) {
                      let str = `<div class="btn-group float-right"><button type="button" onclick="openModal(\'#modal-form\', \'update\', '${data}')" class="btn btn-warning btn-sm"><i class="si si-pencil"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="remove(\'${data}\')" ><i class="si si-trash"></i></button></div>`;
                      return str;
                  },
              }
          ],
          autoWidth: true,
          buttons: [{
              extend: 'copy',
              className: 'btn btn-sm btn-primary'
              }, {
              extend: 'csv',
              className: 'btn btn-sm btn-primary'
              }, {
              extend: 'print',
              className: 'btn btn-sm btn-primary'
          }],
          dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
      });
    }

    var initValidation = function () {
        jQuery('.form-program').validate({
            errorClass: 'invalid-feedback animated fadeIn',
            errorElement: 'div',
            errorPlacement: function (e, r) { jQuery(r).addClass("is-invalid"), jQuery(r).parents(".form-group").append(e) },

            highlight: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid").addClass("is-invalid") },

            success: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid"), jQuery(e).remove() },

            // rules: {
            //     title: {
            //         required: true,
            //     },
            //     isActive: {
            //         required: true,
            //     },
            // },
            // messages: {
            //     title: {
            //         required: 'Masukan Judul',
            //     },
            //     isActive: {
            //         required: 'Pilih Status',
            //     },
            // },

            submitHandler: function (form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('menyimpan..');
                $('.progress-upload-rec').css('width', 0 + '%').attr('aria-valuenow', 0).text(0 + '%');
                $('.progress-add').show();
                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('.progress-upload-rec').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete).text(percentComplete + '%');
                                console.log(percentComplete);
                                if (percentComplete === 100) {
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    url: $form.attr('action'),
                    type: method,
                    data: $form.serialize(),
                    success: function (res) {
                        if (res.data == '1') {
                            form.reset();
                            swal({
                                title: "Berhasil!",
                                text: "Data berhasil disimpan",
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                $('#modal-form').modal('hide');

                                window.tableData.api().ajax.reload();
                            });
                        } else {
                            swal("Oops...", res.data, "error");
                        }
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },

                    error: function (jqXHR, exception) {
                        swal("Oops...", jqXHR.status, "error");
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function () {
            initValidation();
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// modal
function openModal(target, type, id) {
    $('.progress-add').hide();
    $('#srcDataCrop').val('');

    if (type == 'update') {
        $('.modal-title').text('Ubah Pendidikan');
        $.get('/anggota/pendidikan/get/' + id, function (res) {
            let program = res.data;
            method = 'PUT',
                $('.form-program').attr('action', '/anggota/pendidikan/' + id );
            $('.form-program').attr('method', 'PUT');
            $('#id_master_pendidikan').val(program.id_master_pendidikan);
            $('#instansi').val(program.instansi);
            $('#jurusan').val(program.jurusan);
            $('#tahun_masuk').val(program.tahun_masuk);
            $('#tahun_keluar').val(program.tahun_keluar);
            $('#jenis_pendidikan').val(program.jenis_pendidikan);
        });
    } else {
        method = 'POST',
        $('.form-program').attr('action', addUrl);
        $('.modal-title').text('Tambah Pendidikan');
        $('id_master_pendidikan').val('');
        $('#instansi').val('');
        $('#jurusan').val('');
        $('#tahun_masuk').val('');
        $('#tahun_keluar').val('');
        $('#jenis_pendidikan').val('');
    }
    $(target).modal('show');
}

// remove
function remove(id) {
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: '/anggota/pendidikan/' + id + '/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function (resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        window.tableData.api().ajax.reload();
                    });
                },
                error: function (resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}

// restore
function restore(id){
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#5c80d1",
        confirmButtonText: "Restore!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: 'trash/'+id+'/restore',
                type: 'PUT',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    swal("Berhasil!", "Data telah berhasil di Restore.", "success").then((result) => {
                        var table = $('.js-dataTable-anggota-pendidikan-trash').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function(resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}
