/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
let method;
var BaseTableDatatables = function () {
    var initDataTable = function () {
        window.tableData = jQuery('.js-dataTable-jamiyyah').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            order: [[0, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1, 4] }],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            processing: true,
            serverSide: false,
            ajax: dataTableUrl,
            columns: [
                { data: 'DT_RowIndex' },
                {
                    data: 'nama_pimpinan',
                },
                { data: 'pimpinan' },
                { data: 'alamat' },
                {
                    data: 'status_aktif',
                    render: function (data, type, row) {
                        let str;
                        if (data == 1) {
                            str = '<span class="badge badge-success">Aktif</span>';
                        } else {
                            str = '<span class="badge badge-danger">Non Aktif</span>';
                        }
                        return str;
                    }
                },
                {
                    data: 'id_wilayah',
                    render: function (data, type, row) {
                        let button="";
                        if(pimpinan == "PW"){
                            button = '<a  href="/jamiyyah/daerah/'+data+'" class="btn btn-primary btn-sm"><i class="si si-list"></i></a>';
                        }else if(pimpinan == "PD"){
                            button = '<a  href="/jamiyyah/cabang/'+data+'" class="btn btn-primary btn-sm"><i class="si si-list"></i></a>';
                        } else {
                            button = '<a  href="/anggota/pc/'+data+'" class="btn btn-primary btn-sm"><i class="si si-users"></i></a>';
                        }
                        let str = '<div class="btn-group float-right">'+button+'<button  onclick="#" class="btn btn-warning btn-sm"><i class="si si-pencil"></i></button><button type="button" class="btn btn-danger btn-sm" onclick="#" ><i class="si si-trash"></i></button></div>';
                        return str;
                    },
                }
            ],
            autoWidth: true,
            buttons: [{
                extend: 'copy',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'csv',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'print',
                className: 'btn btn-sm btn-primary'
            }],
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        });
    };

   //DataTables Bootstrap integration
    var bsDataTables = function () {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend(true, $DataTable.defaults, {
            dom:
                '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
                '<\'row\'<\'col-sm-12\'tr>>' +
                '<\'row\'<\'col-sm-6\'i><\'col-sm-6\'p>>',
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: '_MENU_',
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>',
                },
            },
        });

        // Default class modification
        jQuery.extend(jQuery.fn.dataTable.ext.classes, { sWrapper: "dataTables_wrapper dt-bootstrap4", sFilterInput: "form-control form-control-sm", sLengthSelect: "form-control form-control-sm" }),

            jQuery.extend(!0, jQuery.fn.dataTable.defaults, { language: { lengthMenu: "_MENU_", search: "_INPUT_", searchPlaceholder: "Search..", paginate: { first: '<i class="fa fa-angle-double-left"></i>', previous: '<i class="fa fa-angle-left"></i>', next: '<i class="fa fa-angle-right"></i>', last: '<i class="fa fa-angle-double-right"></i>' } } })

    };

    // var initValidation = function () {
    //     jQuery('.form-program').validate({
    //         errorClass: 'invalid-feedback animated fadeIn',
    //         errorElement: 'div',
    //         errorPlacement: function (e, r) { jQuery(r).addClass("is-invalid"), jQuery(r).parents(".form-group").append(e) },
    //
    //         highlight: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid").addClass("is-invalid") },
    //
    //         success: function (e) { jQuery(e).parents(".form-group").find(".is-invalid").removeClass("is-invalid"), jQuery(e).remove() },
    //
    //         rules: {
    //             title: {
    //                 required: true,
    //             },
    //             isActive: {
    //                 required: true,
    //             },
    //         },
    //         messages: {
    //             title: {
    //                 required: 'Masukan Judul',
    //             },
    //             isActive: {
    //                 required: 'Pilih Status',
    //             },
    //         },
    //
    //         submitHandler: function (form) {
    //             $form = $(form);
    //             var button = $form.find('button[type="submit"]');
    //             button.attr('disabled', 'disabled');
    //             button.text('menyimpan..');
    //             $('.progress-upload-rec').css('width', 0 + '%').attr('aria-valuenow', 0).text(0 + '%');
    //             $('.progress-add').show();
    //             $.ajax({
    //                 xhr: function () {
    //                     var xhr = new window.XMLHttpRequest();
    //                     xhr.upload.addEventListener("progress", function (evt) {
    //                         if (evt.lengthComputable) {
    //                             var percentComplete = evt.loaded / evt.total;
    //                             percentComplete = parseInt(percentComplete * 100);
    //                             $('.progress-upload-rec').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete).text(percentComplete + '%');
    //                             console.log(percentComplete);
    //                             if (percentComplete === 100) {
    //                             }
    //                         }
    //                     }, false);
    //                     return xhr;
    //                 },
    //                 url: $form.attr('action'),
    //                 type: method,
    //                 data: $form.serialize(),
    //                 success: function (res) {
    //                     if (res.data == '1') {
    //                         form.reset();
    //                         swal({
    //                             title: "Berhasil!",
    //                             text: "Data berhasil disimpan",
    //                             type: "success",
    //                             confirmButtonColor: "#3498DB",
    //                             closeOnConfirm: true
    //                         }).then((result) => {
    //                             $('#modal-form').modal('hide');
    //                             var table = $('.js-dataTable-anggota-keluarga').DataTable();
    //                             table.ajax.reload();
    //                         });
    //                     } else {
    //                         swal("Oops...", res.data, "error");
    //                     }
    //                     button.removeAttr('disabled');
    //                     button.html('<i class="far fa-save mr-1"></i> Simpan');
    //                 },
    //
    //                 error: function (jqXHR, exception) {
    //                     swal("Oops...", jqXHR.status, "error");
    //                     button.removeAttr('disabled');
    //                     button.html('<i class="far fa-save mr-1"></i> Simpan');
    //                 },
    //             });
    //             return false; // required to block normal submit since you used ajax
    //         },
    //     });
    // };

    return {
        init: function () {
           // initValidation();
            bsDataTables();
            initDataTable();
        },
    };
}();

// Initialize when page loads
jQuery(function () {
    BaseTableDatatables.init();
});

/* PROCCESSING */
// modal
function openModal(target, type, id) {
    if (type == 'update') {
        $('.modal-title').text('Ubah Role');
        $.get('/role/get/' + id, function (res) {

             let role = res.data;
             method = 'PUT',

             $('.form-role').attr('action', '/role/' + id + '/update');
             $('.form-role').attr('method', 'PUT');
             $('#nama_role').val(role.nama_role);
             $("#id_role").val(role.id_role);
             $('#desc').val(role.desc);
             if(role.status_aktif == 1){
                 $('#status_aktif').prop('checked',true);
             }else{
                 $('#status_aktif').prop('checked',false);
             }

            $('#comp_aktif').show();

        });
    } else {
        let addUrl = "{{ route('role.store') }}";
        method = 'POST',
            $('.form-role').attr('action', addUrl);
        $('.modal-title').text('Tambah Role');
        $('#nama_role').val('');
        $('#desc').val('');
        $('#comp_aktif').hide();
    }
    $(target).modal('show');
}


// remove
function remove(id) {
    swal({
        title: "Apa anda yakin?",
        text: "Data akan dihapus dan tidak bisa dikembalikan!",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Batal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus!",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: 'role/delete',
                type: 'POST',
                data: { id: id },
                dataType: 'json',
                success: function (resp) {
                    swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                        var table = $('.js-dataTable-role').DataTable();
                        table.ajax.reload();
                    });
                },
                error: function (resp) {
                    alert(resp.responseJSON.message);
                }
            });
        }
    })
}
