<?php return array (
  'codeToName' => 
  array (
    0 => 'NULL',
    13 => 'CR',
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    160 => 'uni00A0',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    164 => 'currency',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    172 => 'logicalnot',
    174 => 'registered',
    176 => 'degree',
    177 => 'plusminus',
    178 => 'uni00B2',
    179 => 'uni00B3',
    180 => 'acute',
    181 => 'uni03BC',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    185 => 'uni00B9',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    305 => 'dotlessi',
    321 => 'Lslash',
    322 => 'lslash',
    338 => 'OE',
    339 => 'oe',
    352 => 'Scaron',
    353 => 'scaron',
    376 => 'Ydieresis',
    381 => 'Zcaron',
    382 => 'zcaron',
    402 => 'florin',
    710 => 'circumflex',
    711 => 'caron',
    729 => 'dotaccent',
    730 => 'ring',
    731 => 'ogonek',
    732 => 'tilde',
    1456 => 'uni05B0',
    1457 => 'uni05B1',
    1458 => 'uni05B2',
    1459 => 'uni05B3',
    1460 => 'uni05B4',
    1461 => 'uni05B5',
    1462 => 'uni05B6',
    1463 => 'uni05B7',
    1464 => 'uni05B8',
    1465 => 'uni05B9',
    1466 => 'uni05BA',
    1467 => 'uni05BB',
    1468 => 'uni05BC',
    1470 => 'uni05BE',
    1471 => 'uni05BF',
    1473 => 'uni05C1',
    1474 => 'uni05C2',
    1479 => 'uni05C7',
    1488 => 'uni05D0',
    1489 => 'uni05D1',
    1490 => 'uni05D2',
    1491 => 'uni05D3',
    1492 => 'uni05D4',
    1493 => 'uni05D5',
    1494 => 'uni05D6',
    1495 => 'uni05D7',
    1496 => 'uni05D8',
    1497 => 'uni05D9',
    1498 => 'uni05DA',
    1499 => 'uni05DB',
    1500 => 'uni05DC',
    1501 => 'uni05DD',
    1502 => 'uni05DE',
    1503 => 'uni05DF',
    1504 => 'uni05E0',
    1505 => 'uni05E1',
    1506 => 'uni05E2',
    1507 => 'uni05E3',
    1508 => 'uni05E4',
    1509 => 'uni05E5',
    1510 => 'uni05E6',
    1511 => 'uni05E7',
    1512 => 'uni05E8',
    1513 => 'uni05E9',
    1514 => 'uni05EA',
    1523 => 'uni05F3',
    1524 => 'uni05F4',
    8211 => 'endash',
    8212 => 'emdash',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8224 => 'dagger',
    8225 => 'daggerdbl',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8240 => 'perthousand',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8260 => 'fraction',
    8362 => 'newsheqel',
    8364 => 'Euro',
    8482 => 'trademark',
    8722 => 'minus',
    64257 => 'fi',
    64258 => 'fl',
    64298 => 'uniFB2A',
    64299 => 'uniFB2B',
    64300 => 'uniFB2C',
    64301 => 'uniFB2D',
    64302 => 'uniFB2E',
    64303 => 'uniFB2F',
    64304 => 'uniFB30',
    64305 => 'uniFB31',
    64306 => 'uniFB32',
    64307 => 'uniFB33',
    64308 => 'uniFB34',
    64309 => 'uniFB35',
    64310 => 'uniFB36',
    64312 => 'uniFB38',
    64313 => 'uniFB39',
    64314 => 'uniFB3A',
    64315 => 'uniFB3B',
    64316 => 'uniFB3C',
    64318 => 'uniFB3E',
    64320 => 'uniFB40',
    64321 => 'uniFB41',
    64323 => 'uniFB43',
    64324 => 'uniFB44',
    64326 => 'uniFB46',
    64327 => 'uniFB47',
    64328 => 'uniFB48',
    64329 => 'uniFB49',
    64330 => 'uniFB4A',
    64331 => 'uniFB4B',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Fredoka Medium',
  'FullName' => 'Fredoka Medium',
  'Version' => 'Version 2.000',
  'PostScriptName' => 'Fredoka-Medium',
  'Weight' => 'Bold',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '50',
  'UnderlinePosition' => '-100',
  'FontHeightOffset' => '0',
  'Ascender' => '974',
  'Descender' => '-236',
  'FontBBox' => 
  array (
    0 => '-70',
    1 => '-252',
    2 => '1212',
    3 => '950',
  ),
  'StartCharMetrics' => '313',
  'C' => 
  array (
    0 => 0.0,
    13 => 248.0,
    32 => 248.0,
    33 => 237.0,
    34 => 356.0,
    35 => 763.0,
    36 => 459.0,
    37 => 809.0,
    38 => 707.0,
    39 => 185.0,
    40 => 371.0,
    41 => 371.0,
    42 => 486.0,
    43 => 521.0,
    44 => 225.0,
    45 => 418.0,
    46 => 214.0,
    47 => 508.0,
    48 => 580.0,
    49 => 388.0,
    50 => 587.0,
    51 => 580.0,
    52 => 547.0,
    53 => 509.0,
    54 => 531.0,
    55 => 543.0,
    56 => 553.0,
    57 => 531.0,
    58 => 230.0,
    59 => 216.0,
    60 => 537.0,
    61 => 459.0,
    62 => 537.0,
    63 => 486.0,
    64 => 834.0,
    65 => 706.0,
    66 => 629.0,
    67 => 655.0,
    68 => 680.0,
    69 => 617.0,
    70 => 625.0,
    71 => 712.0,
    72 => 675.0,
    73 => 224.0,
    74 => 540.0,
    75 => 593.0,
    76 => 562.0,
    77 => 820.0,
    78 => 695.0,
    79 => 728.0,
    80 => 599.0,
    81 => 801.0,
    82 => 618.0,
    83 => 550.0,
    84 => 656.0,
    85 => 701.0,
    86 => 726.0,
    87 => 959.0,
    88 => 693.0,
    89 => 638.0,
    90 => 583.0,
    91 => 336.0,
    92 => 510.0,
    93 => 336.0,
    94 => 503.0,
    95 => 797.0,
    96 => 372.0,
    97 => 569.0,
    98 => 573.0,
    99 => 507.0,
    100 => 575.0,
    101 => 539.0,
    102 => 392.0,
    103 => 559.0,
    104 => 545.0,
    105 => 233.0,
    106 => 238.0,
    107 => 497.0,
    108 => 291.0,
    109 => 814.0,
    110 => 560.0,
    111 => 559.0,
    112 => 563.0,
    113 => 560.0,
    114 => 415.0,
    115 => 459.0,
    116 => 394.0,
    117 => 551.0,
    118 => 551.0,
    119 => 755.0,
    120 => 509.0,
    121 => 543.0,
    122 => 536.0,
    123 => 368.0,
    124 => 210.0,
    125 => 368.0,
    126 => 554.0,
    160 => 248.0,
    161 => 237.0,
    162 => 419.0,
    163 => 652.0,
    164 => 654.0,
    165 => 654.0,
    166 => 208.0,
    167 => 541.0,
    168 => 398.0,
    169 => 656.0,
    170 => 430.0,
    171 => 577.0,
    172 => 508.0,
    174 => 656.0,
    176 => 347.0,
    177 => 489.0,
    178 => 439.0,
    179 => 441.0,
    180 => 430.0,
    181 => 580.0,
    182 => 519.0,
    183 => 194.0,
    184 => 287.0,
    185 => 310.0,
    186 => 424.0,
    187 => 569.0,
    188 => 759.0,
    189 => 776.0,
    190 => 795.0,
    191 => 475.0,
    192 => 706.0,
    193 => 706.0,
    194 => 706.0,
    195 => 706.0,
    196 => 706.0,
    197 => 706.0,
    198 => 1108.0,
    199 => 655.0,
    200 => 617.0,
    201 => 617.0,
    202 => 617.0,
    203 => 617.0,
    204 => 224.0,
    205 => 224.0,
    206 => 224.0,
    207 => 224.0,
    208 => 743.0,
    209 => 695.0,
    210 => 728.0,
    211 => 728.0,
    212 => 728.0,
    213 => 728.0,
    214 => 728.0,
    215 => 464.0,
    216 => 739.0,
    217 => 701.0,
    218 => 701.0,
    219 => 701.0,
    220 => 701.0,
    221 => 638.0,
    222 => 597.0,
    223 => 586.0,
    224 => 569.0,
    225 => 569.0,
    226 => 569.0,
    227 => 569.0,
    228 => 569.0,
    229 => 569.0,
    230 => 872.0,
    231 => 506.0,
    232 => 539.0,
    233 => 539.0,
    234 => 539.0,
    235 => 539.0,
    236 => 212.0,
    237 => 212.0,
    238 => 212.0,
    239 => 212.0,
    240 => 575.0,
    241 => 560.0,
    242 => 559.0,
    243 => 559.0,
    244 => 559.0,
    245 => 559.0,
    246 => 559.0,
    247 => 465.0,
    248 => 559.0,
    249 => 551.0,
    250 => 551.0,
    251 => 551.0,
    252 => 551.0,
    253 => 543.0,
    254 => 560.0,
    255 => 543.0,
    305 => 212.0,
    321 => 629.0,
    322 => 380.0,
    338 => 1047.0,
    339 => 911.0,
    352 => 550.0,
    353 => 459.0,
    376 => 638.0,
    381 => 583.0,
    382 => 536.0,
    402 => 483.0,
    710 => 460.0,
    711 => 458.0,
    729 => 214.0,
    730 => 324.0,
    731 => 277.0,
    732 => 459.0,
    1456 => 0.0,
    1457 => 0.0,
    1458 => 0.0,
    1459 => 0.0,
    1460 => 0.0,
    1461 => 0.0,
    1462 => 0.0,
    1463 => 0.0,
    1464 => 0.0,
    1465 => 0.0,
    1466 => 0.0,
    1467 => 0.0,
    1468 => 0.0,
    1470 => 302.0,
    1471 => 0.0,
    1473 => 0.0,
    1474 => 0.0,
    1479 => 0.0,
    1488 => 641.0,
    1489 => 629.0,
    1490 => 447.0,
    1491 => 603.0,
    1492 => 603.0,
    1493 => 248.0,
    1494 => 439.0,
    1495 => 644.0,
    1496 => 640.0,
    1497 => 248.0,
    1498 => 555.0,
    1499 => 565.0,
    1500 => 523.0,
    1501 => 614.0,
    1502 => 665.0,
    1503 => 248.0,
    1504 => 439.0,
    1505 => 649.0,
    1506 => 590.0,
    1507 => 569.0,
    1508 => 584.0,
    1509 => 569.0,
    1510 => 582.0,
    1511 => 602.0,
    1512 => 540.0,
    1513 => 724.0,
    1514 => 702.0,
    1523 => 184.0,
    1524 => 345.0,
    8211 => 552.0,
    8212 => 679.0,
    8216 => 184.0,
    8217 => 178.0,
    8218 => 186.0,
    8220 => 345.0,
    8221 => 345.0,
    8222 => 345.0,
    8224 => 487.0,
    8225 => 487.0,
    8226 => 263.0,
    8230 => 676.0,
    8240 => 1190.0,
    8249 => 353.0,
    8250 => 341.0,
    8260 => 488.0,
    8362 => 939.0,
    8364 => 581.0,
    8482 => 806.0,
    8722 => 556.0,
    64257 => 618.0,
    64258 => 686.0,
    64298 => 724.0,
    64299 => 724.0,
    64300 => 724.0,
    64301 => 724.0,
    64302 => 641.0,
    64303 => 641.0,
    64304 => 641.0,
    64305 => 629.0,
    64306 => 447.0,
    64307 => 603.0,
    64308 => 603.0,
    64309 => 329.0,
    64310 => 449.0,
    64312 => 640.0,
    64313 => 307.0,
    64314 => 555.0,
    64315 => 565.0,
    64316 => 523.0,
    64318 => 665.0,
    64320 => 439.0,
    64321 => 649.0,
    64323 => 569.0,
    64324 => 584.0,
    64326 => 582.0,
    64327 => 602.0,
    64328 => 540.0,
    64329 => 724.0,
    64330 => 702.0,
    64331 => 248.0,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt0FOQZkcYBuC317Zt27Ft27adi+Qitm3btm3bto3/YmqrUpPN7GZnMrl4nqo+p+u8X3d/fcq0zECZOqPkL37Mc/kmr+bX0rq0y7d5I2/mlVLyVN7Lk3ktd+TO3JW7c0/uzX25Pw/kwTydZ0rT0rg0yQulbUqap0VapU06plM6p0t6pld6p2/6ZUBGZFRGZ0zGZXymZGqmZY7MnXfyet4tLfNh6Z15s2gWyxJZKstnhayYlbJG1sxaWSfrZv1sni2zVbbOdtk+u2TX7Jbds1feKl3zdmk+Uzf8R2VKns/P+SO/lJRupUPpWTpmv3xRWlSyTpXRpTTLw3mk9CmtSvu8VAbmoeyfL/N4HssTeTGN0yAN0zSN0iTN0jId0jbt0j490jXd0j2t0z9DMjCDMiyDS8MMzeRMyMRMypwZmW2zYObL/Fk4C2ShLJLFs1yWzjJZNqtnlaya1bJk1svG2SAbZtNsVBplk+ycHbJjdsoe2SJ7zv4fqNHKNeR9snYNFcOz2QyzsdlmlrqZa/psnuw9k2t+m6UT4F8pfUu/majqVfqXQWVAnbcDwGwoQ8rQMqwMLyPKyDKqjC5jytgyrowvEyrhD2VipWJwmTS9enK1DQ7IITksR+SoHJPjc2JOyik5LWfkrJyTc3N+LshFuSSX5vJcmatyTa7LDbmt2i7f5fs6vSYAAAAAAAAAAAAAAABQf97PB1Wzz/J5Pqq8P8mn+Tgp3UuPvFyVPVt9YWlT7dNX+fpvTni0ljr9P/i9Mn6q7yb+O6VzfXdAXSoN6rsDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADIPtm3lne8MTfl5tySA3NQDs6hOTxH5ugcmxMq2ck5NafnzJxdmZ9XGRfm4srzslxReV6da3N9bs3tOa6WOwIAoMqfMXmC8w==',
  '_version_' => 6,
);