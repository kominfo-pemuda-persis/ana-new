# ANA NEW

Sistem aplikasi manajemen data anggota Persis.
Aplikasi ini merupakan hasil penulisan ulang (re-writing) dari  aplikasi lama (legacy) "administrasi". 
Mengapa ditulis ulang?
administrasi dikembangkan menggunakan Codeigniter, yang mana perkembangannya tidak secapat Laravel. Maka diputuskan untuk ditulis ulang dengan framework Laravel. 

## Panduan Kontribusi

### Instalasi

- Clone repository 

```
git clone git@gitlab.com:kominfo-pemuda-persis/ana-new.git ana-new.local
```

- Copy .env.example

Pastikan .env.example ada dalam folder aplikasi. Kemudian rename file tersebut menjadi .env. 
Untuk pengguna macOS ataupun GNU/Linux, jalankan perintah dibawah pada console terminal 

```
cp .env.example .env
```

Untuk Windows OS jalankan perintah pada cmd

```
copy .env.example .env
```

- Composer

Pastikan composer telah terpasang pada lokal development Anda. 
Laravel membutuhkan composer untuk instalasi dependensi atau library yang dibutuhkan. 
Pastikan jalankan perintah di bawah sebelum memulai development.

```
composer install
```

- Generate Key

Jalankan perintah dibawah untuk menggenerate key pada file .env.
Key dibutuhkan untuk proses enkripsi data sensitif pada laravel seperti Cookies atau Session. 

```
php artisan key:generate
```

- Konfigurasi Database

Buka .env lalu arahkan database ke lokal masing-masing.

```
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=<NAMA DATABASE>
DB_USERNAME=<USERNAME>
DB_PASSWORD=<PASSWORD>
```

- Migrasi DB

```
php artisan migrate
```

- Seeding Data.

Pastikan jalankan composer dump autoload untuk mengupdate class cache.

```
composer dump-autoload
```

Lalu jalankan seed

```
php artisan db:seed
```

- Jalankan mesin MySQL dan start server.

```
sudo service mysql start
php artisan serve
```

- Email Testing

Layanan email yang digunakan untuk keperluan testing ialah [mailtrap](https://mailtrap.io). 
Pastikan konfigurasi email pada file .env seperti di bawah ini.

```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=<USERNAME>
MAIL_PASSWORD=<PASSWORD>
MAIL_ENCRYPTION=tls
```
Anda dapat mendaftar akun mailtrap untuk mendapatkan username dan password.

### Instalasi Menggunakan Docker

pastikan docker dan docker-compose sudah terinstal pada local komputer anda.

untuk menjalankan ana-online pada mode docker silahkan jalankan perintah di bawah pada root folder ana-online

```
docker-compose -f docker-compose-local.yml build
```

Jika docker image sudah selesai dibuat silahkan jalankan perintah

```
docker-compose -f docker-compose-local.yml up -d
```

Untuk menjalankan migrate db pada mode docker silahkan jalankan perintah

```
docker-compose -f docker-compose-local.yml exec web php artisan migrate
```

Untuk menjalankan seed db pada mode docker silahkan jalankan perintah

```
docker-compose -f docker-compose-local.yml exec web php artisan db:seed
```

CATATAN: pastikan port 80, 3306 dan 8000 tidak digunakan karena port tersebut akan digunakan selama docker ana-online berjalan

Port 80 akan menjadi default mapping port aplikasi ana online

Port 3306 akan menjadi default mapping port docker mysql

Port 8000 akan menjadi default mapping port phpmyadmin

Anda bisa melakukan penyesuain port ini dengan mengubah konfigurasi pada file `docker-compose-local.yml` pada root folder aplikasi

### Masuk ke Aplikasi

Buka `http://localhost:8000`

Berikut daftar login akun tes berdasarkan masing-masing role:

|#|Role|NPA|Password|
|---|---|---|---|
|1|anggota|99.0001|12345|
|2|superadmin|99.0002|12345|
|3|admin_pp|99.0003|12345|
|4|tasykil_pp|99.0004|12345|
|5|admin_pw|99.0005|12345|
|6|tasykil_pw|99.0006|12345|
|7|admin_pd|99.0007|12345|
|8|tasykil_pd|99.0008|12345|
|9|admin_pc|99.0009|12345|
|10|tasykil_pc|99.0010|12345|
|11|admin_pj|99.0011|12345|

## Perubahan apa saja yang terjadi ?
Terdapat perubahan-perubahan yang terjadi saat proses re-writing berlangsung, diantaranya :

### Perubahan Nama Tabel DB

|#|administrasi|ana-new|
|---|---|---|
|1|tbl_anggota|t_anggota|
|2|tbl_data_jamaah|t_monografi_pj|
|3|tbl_user|t_login|
|4|tbl_data_geografis_pc|t_??|
|5|tbl_data_geografis_pd|t_??|
|6|tbl_pendidikan|t_master_pendidikan|

### Perubahan nama kolom tabel DB
|#|administrasi|ana-new|table
|---|---|---|---|
|1|level|tingkat|t_master_pendidikan|
|1|nama|nama_lengkap|t_anggota|

## Darimana data dihasilkan ?
Berikut adalah sumber dari data yang di inginkan pada tiap halamannya : 

### Dashboard
1. usia,merital,status_keanggotaan,gol_darah menggunakan data dari tabel t_anggota
2. pendidikan menggunakan tbl_pendidikan

# Issues DB
Jika web-aplikasi ini tidak bisa meng-eksekusi RAW Query,lakukan saran dari sobat @azizramdan 
1. gunakan mysql, bukan mariadb 
2. jika menggunakan mariadb, ubah state strict menjadi strict=false






