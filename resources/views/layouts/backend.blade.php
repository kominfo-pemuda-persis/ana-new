 <!doctype html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Administrasi Online - Otonom Persis</title>

    <meta name="description" content="Administrasi Online">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Tim Kominfo">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Icons -->
    <link rel="shortcut icon" href="{{ asset('media/res/pemuda_ico.ico') }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

    <!-- Fonts and Styles -->
    @yield('css_before')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,600;0,700;1,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" id="css-main" href="{{ asset('/css/oneui.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/custome.css') }}">
    <style>
        * {
            font-family: 'Montserrat', sans-serif;
        }

        @media (max-width: 980px) {
            #page-header-logo #img-logo3 {
                width: 10%;
            }
        }
        .simplebar-content > .content-side {
            background: #2f6f36;
        }

        .nav-main-link .nav-main-link-icon,
        .nav-main-link .nav-main-link-name,
        .nav-main-link.active{
            color: #e2e4e3;
        }

        .nav-main-link.active {
            background: #b9c47d!important;
            padding: 5px;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        .nav-main-item > .open {
            background: #a1b81e;
        }

        .nav-main-item.open > .nav-main-link-submenu > .nav-main-link-icon {
            color: #fff;
        }

        .nav-main-link:hover > .nav-main-link-icon {
            color: #fff;
        }


        /* .active{
            background: #b9c47d;
        } */

        .nav-main-link.active > .nav-main-link-icon,
        .nav-main-link.active > .nav-main-link-name{
            color: #e2e4e3;
        }

        .nav-main-link:hover span,
        .nav-main-link .nav-main-link-name:hover,
        {
            color: black!important;
            transition: 0.5s;
        }

        .nav-main-link-submenu {
            cursor: pointer;
        }

        .nav-main-link:hover,
        .nav-main-link-icon:hover {
            background: #b9c47d!important;
            padding: 5px;
            color: #e2e4e3!important;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
            transition: 0.5s;
        }

        .nav-main-link {
            color: #e2e4e3;
        }
    </style>

    @yield('css_after')

</head>

<body>
        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-fixed">
            <nav id="sidebar" aria-label="Main Navigation">
                <!-- Side Header -->
                <div class="content-header bg-white-10">
                    <!-- Logo -->
                    <a class="font-w600 text-dual" href="/" id="page-header-logo">
                        <img src="{{ asset("media/res/ANA_ONLINE_4.png") }}" width="5%" id="img-logo2"/>
                        <span>
                            <img src="{{ asset("media/res/ANA_ONLINE.png") }}" width="75%" id="img-logo3">
                        </span>
                    </a>
                    <!-- END Logo -->
                    <a class="d-lg-none btn btn-sm btn-alt-secondary ms-1" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                        <i class="fa fa-fw fa-times"></i>
                      </a>
                </div>
                <!-- END Side Header -->

                <!-- Side Navigation -->
                <div class="content-side content-side-full">
                    <ul class="nav-main">
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('dashboard') ? ' active' : '' }}" href="{{url('dashboard')}}">
                                <i class="nav-main-link-icon si si-cursor"></i>
                                <span class="nav-main-link-name">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-main-heading">Data Master</li>

                        @permission('anggota.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('anggota') ? ' active' : '' }}"
                               href="{{ url('anggota') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data Anggota</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('caang.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('data-calon-anggota') ? ' active' : '' }}"
                               href="{{ url('data-calon-anggota') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data Calon Anggota</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('rekapcaang.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ request()->is('data-registrasi*') ? 'active' : '' }}"
                               href="{{ url('data-registrasi') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data Rekap Caang</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('tafiq1.*', 'tafiq3.*')
                        <li class="nav-main-item {{ request()->is('data-tafiq*') ? 'open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data Tafiq</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @permission('tafiq1.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link {{ request()->is('data-tafiq1') ? 'active' : '' }}"
                                        href="{{route('viewdata.tafiq1',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">Tafiq 1</span>
                                    </a>
                                </li>
                                @endpermission
                            </ul>
                            <ul class="nav-main-submenu">
                                @permission('tafiq2.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('data-tafiq2') ? ' active' : '' }}"
                                        href="{{route('viewdata.tafiq2',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">Tafiq 2</span>
                                    </a>
                                </li>
                                @endpermission
                            </ul>
                            <ul class="nav-main-submenu">
                                @permission('tafiq3.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('data-tafiq3') ? ' active' : '' }}"
                                        href="{{route('viewdata.tafiq3',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">Tafiq 3</span>
                                    </a>
                                </li>
                                @endpermission
                            </ul>
                        </li>
                        @endpermission

                        @permission('herregistrasi.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('data-her-registrasi') ? ' active' : '' }}"
                               href="{{ url('data-her-registrasi') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data Heregistrasi</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('mutasi.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('mutasi') ? ' active' : '' }}" href="{{url('mutasi')}}">
                                <i class="nav-main-link-icon si si-docs"></i>
                                <span class="nav-main-link-name">Data Mutasi</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('otonom.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('otonom') ? ' active' : '' }}" href="{{url('otonom')}}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data Otonom</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('jamiyyah.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('jamiyyah') ? ' active' : '' }}"
                               href="{{url('jamiyyah')}}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data Jamiyyah</span>
                            </a>
                        </li>
                        @endpermission

                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('pc') ? ' active' : '' }}"
                               href="{{url('pc')}}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data PC</span>
                            </a>
                        </li>

                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('pd/pdaerah') ? ' active' : '' }}"
                               href="{{route('pdaerah.index',[],false)}}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data PD</span>
                            </a>
                        </li>

                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('jamiyyah/pw') ? ' active' : '' }}"
                               href="{{route('pimpinanWilayah.index',[],false)}}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data PW</span>
                            </a>
                        </li>

                        @permission('bendahara.*')
                        <li class="nav-main-item {{ request()->is('bendahara*') ? 'open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu">
                                <i class="nav-main-link-icon si si-wallet"></i>
                                <span class="nav-main-link-name">Data Bendahara</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @permission('tagihan.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('bendahara/buat-tagihan*') ? ' active' : '' }}"
                                        href="{{route('bendahara.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-credit-card"></i>
                                        <span class="nav-main-link-name">Buat Tagihan</span>
                                    </a>
                                </li>
                                @endpermission
                                @permission('transaksi.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('bendahara/transaksi-tagihan*') ? ' active' : '' }}"
                                        href="{{route('transaksi.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-credit-card"></i>
                                        <span class="nav-main-link-name">Transaksi Tagihan</span>
                                    </a>
                                </li>
                                @endpermission
                                @permission('logtransaksi.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('bendahara/log-transaksi-tagihan*') ? ' active' : '' }}"
                                        href="{{route('transaksi.logtrans',[],false)}}">
                                        <i class="nav-main-link-icon si si-credit-card"></i>
                                        <span class="nav-main-link-name">Log Transaksi</span>
                                    </a>
                                </li>
                                @endpermission
                            </ul>
                        </li>
                        @endpermission

                        @permission('monografipj.*','monografipc.*','monografipd.*','monografipw.*')
                        <li class="nav-main-item {{ request()->is('jamiyyah/monografi/*') ? 'open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Data Monografi</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @permission('monografipj.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('jamiyyah/monografi/pj') ? ' active' : '' }}"
                                       href="{{route('pj.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">PJ</span>
                                    </a>
                                </li>
                                @endpermission
                                @permission('monografipc.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('jamiyyah/monografi/pc') ? ' active' : '' }}"
                                       href="{{route('pc.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">PC</span>
                                    </a>
                                </li>
                                @endpermission
                                @permission('monografipd.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('jamiyyah/monografi/pd') ? ' active' : '' }}"
                                       href="{{route('pd.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">PD</span>
                                    </a>
                                </li>
                                @endpermission

                                @permission('monografipw.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('jamiyyah/monografi/pw') ? ' active' : '' }}"
                                       href="{{route('pw.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">PW</span>
                                    </a>
                                </li>
                                @endpermission
                            </ul>
                        </li>
                        @endpermission

                        @permission('indexperformapj.*','indexperformapc.*','indexperformapd.*','indexperformapw.*')
                        <li class="nav-main-item {{ request()->is('jamiyyah/performa/*') ? 'open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Index Performa</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @permission('indexperformapj.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('jamiyyah/performa/pj') ? ' active' : '' }}"
                                       href="{{route('performa.pj.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">PJ</span>
                                    </a>
                                </li>
                                @endpermission

                                @permission('indexperformapc.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('jamiyyah/performa/pc') ? ' active' : '' }}"
                                       href="{{route('performa.pc.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">PC</span>
                                    </a>
                                </li>
                                @endpermission

                                @permission('indexperformapd.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('jamiyyah/performa/pd') ? ' active' : '' }}"
                                       href="{{route('performa.pd.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">PD</span>
                                    </a>
                                </li>
                                @endpermission

                                @permission('indexperformapw.*')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('jamiyyah/performa/pw') ? ' active' : '' }}"
                                       href="{{route('performa.pw.index',[],false)}}">
                                        <i class="nav-main-link-icon si si-users"></i>
                                        <span class="nav-main-link-name">PW</span>
                                    </a>
                                </li>
                                @endpermission
                            </ul>
                        </li>
                        @endpermission

                        <li class="nav-main-item">
                            <a class="nav-main-link {{ Route::is('doc_file.*') ? 'active' : '' }}"
                               href="{{ url('data-document') }}">
                                <i class="nav-main-link-icon si si-book-open"></i>
                                <span class="nav-main-link-name">Data Dokumen</span>
                            </a>
                        </li>

                        <li class="nav-main-item">
                            <a class="nav-main-link {{ Route::is('esyahadah.*') ? 'active' : '' }}"
                               href="{{ route('esyahadah.index') }}">
                                <i class="nav-main-link-icon si si-book-open"></i>
                                <span class="nav-main-link-name">E-syahadah</span>
                            </a>
                        </li>

                        <li class="nav-main-heading">Data Settings</li>

                        @permission('users.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ Route::is('data-user.*') ? 'active' : '' }}"
                               href="{{ url('data-user') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">User</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('role.*')
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ Route::is('role.*') ? 'active' : '' }}"
                               href="{{ url('role') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Role & Permission</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('role.assignment')
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ Route::is('role-user.*') ? 'active' : '' }}"
                               href="{{ url('role-user') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Role Assignment</span>
                            </a>
                        </li>
                        @endpermission
                    </ul>
                </div>
                <!-- END Side Navigation -->
            </nav>
        <!-- END Sidebar -->

        <!-- Header -->
        <header id="page-header">
            <!-- Header Content -->
            <div class="content-header">
                <!-- Left Section -->
                <div class="d-flex align-items-center">
                    <!-- Toggle Sidebar -->
                    <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
                    <button type="button" class="btn btn-sm btn-dual mr-2 d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                        <i class="fa fa-fw fa-bars"></i>
                    </button>
                    <!-- END Toggle Sidebar -->

                    <!-- Toggle Mini Sidebar -->
                    <button type="button" class="btn btn-sm btn-dual mr-2 d-lg-block d-none" data-toggle="layout" data-action="sidebar_mini_toggle">
                        <i class="fa fa-fw fa-bars"></i>
                    </button>

                    <!-- Open Search Section (visible on smaller screens) -->
                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                    {{-- <button type="button" class="btn btn-sm btn-dual" data-toggle="layout" data-action="header_search_on">
                        <i class="si si-magnifier"></i>
                    </button> --}}
                    <!-- END Open Search Section -->

                    <!-- Search Form (visible on larger screens) -->
                    {{-- <form class="d-none d-sm-inline-block" action="/dashboard" method="POST">
                        @csrf
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control form-control-alt" placeholder="Search.." id="page-header-search-input2" name="page-header-search-input2">
                            <div class="input-group-append">
                                <span class="input-group-text bg-body border-0">
                                    <i class="si si-magnifier"></i>
                                </span>
                            </div>
                        </div>
                    </form> --}}
                    <!-- END Search Form -->
                </div>
                <!-- END Left Section -->

                @php
                    $profileAnggota = Auth::user()->anggota;
                    $photoProfile = asset('images/anggota/default.png');
                    if(!$profileAnggota->foto || $profileAnggota->foto=="default.png") {
                        $photoProfile=asset('images/anggota/default.png');
                    }else{
                        $photoProfile=Storage::disk('s3')->url('images/anggota/'.$profileAnggota->foto);
                    }
                @endphp
                <!-- Right Section -->
                <div class="d-flex align-items-center">
                    <!-- User Dropdown -->
                    <div class="dropdown d-inline-block ml-2">
                        <button type="button" class="btn d-none d-md-block btn-sm btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="d-none d-sm-inline-block ml-1">
                                <!-- set nama -->
                                {{
                                        Auth::user()->anggota->nama_lengkap
                                    }}
                                <!-- set role user -->
                                (@foreach(Auth::user()->roles as $role) {{$role->display_name}} @endforeach)
                            </span>
                            <i class="fa fa-fw fa-angle-down d-none d-sm-inline-block"></i>
                        </button>
                        <button type="button" class="p-0 btn d-md-none bg-white" id="page-header-user-dropdown-mobile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div>
                                <img class="rounded-circle" width="40" height="40" src="{{ $photoProfile }}" alt="profile picture">
                            </div>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right p-0 border-0 font-size-sm" aria-labelledby="page-header-user-dropdown">
                            <div class="p-3 text-center bg-primary">
                                <img class="photo-profile photo-profile--small img-avatar-thumb" src="{{ $photoProfile }}" alt="">
                            </div>
                            <div class="p-2">
                                <h5 class="dropdown-header text-uppercase">Actions</h5>

                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggota.profil',Auth::user()->anggota->id_anggota) }}">
                                    <span>Profil</span>
                                    <i class="si si-user ml-1"></i>
                                </a>

                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggota.profileCard',Auth::user()->anggota->id_anggota) }}">
                                    <span>E-KTA</span>
                                    <i class="si si-credit-card ml-1"></i>
                                </a>

                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggotaKeluarga.index',Auth::user()->anggota->id_anggota) }}">
                                    <span>Keluarga</span>
                                    <i class="si si-people ml-1"></i>
                                </a>

                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggotaPendidikan.index',Auth::user()->anggota->id_anggota) }}">
                                    <span>Pendidikan</span>
                                    <i class="si si-graduation ml-1"></i>
                                </a>
                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggotaPekerjaan.index',Auth::user()->anggota->id_anggota) }}">
                                    <span>Pekerjaan</span>
                                    <i class="si si-energy ml-1"></i>
                                </a>
                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggotaKeterampilan.index',Auth::user()->anggota->id_anggota) }}">
                                    <span>Keterampilan</span>
                                    <i class="si si-star ml-1"></i>
                                </a>
                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggotaOrganisasi.index',Auth::user()->anggota->id_anggota) }}">
                                    <span>Organisasi</span>
                                    <i class="si si-organization ml-1"></i>
                                </a>
                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggotaTraining.index',Auth::user()->anggota->id_anggota) }}">
                                    <span>Training</span>
                                    <i class="si si-badge ml-1"></i>
                                </a>
                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggotaTafiq.index',Auth::user()->anggota->id_anggota) }}">
                                    <span>Tafiq</span>
                                    <i class="si si-book-open ml-1"></i>
                                </a>
                                <a class="btn dropdown-item d-flex align-items-center justify-content-between" href="{{ route('anggota.password-form') }}">
                                    <span>Password</span>
                                    <i class="si si-people ml-1"></i>
                                </a>

                                <form action="{{route('logout')}}" method="post">
                                    @csrf
                                    <button type="submit" class="dropdown-item d-flex align-items-center justify-content-between">
                                        <span>Log Out</span>
                                        <i class="si si-logout ml-1"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END User Dropdown -->
                </div>
                <!-- END Right Section -->
            </div>
            <!-- END Header Content -->

            <!-- Header Search -->
            <div id="page-header-search" class="overlay-header bg-white">
                <div class="content-header">
                    <form class="w-100" action="/dashboard" method="POST">
                        @csrf
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                <button type="button" class="btn btn-danger" data-toggle="layout" data-action="header_search_off">
                                    <i class="fa fa-fw fa-times-circle"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Header Search -->

            <!-- Header Loader -->
            <!-- Please check out the Loaders page under Components category to see examples of showing/hiding it -->
            <div id="page-header-loader" class="overlay-header bg-white">
                <div class="content-header">
                    <div class="w-100 text-center">
                        <i class="fa fa-fw fa-circle-notch fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- END Header Loader -->
        </header>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            @yield('content')
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        <footer id="page-footer" class="bg-body-light">
            <div class="content py-3">
                <div class="row font-size-sm">
                    <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
                        Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600" href="https://www.facebook.com/kominfopemudapersis" target="_blank">Tim Kominfo</a>
                    </div>
                    <div class="col-sm-6 order-sm-1 py-1 text-center text-sm-left">
                        <a class="font-w600" href="https://www.facebook.com/kominfopemudapersis" target="_blank">ANA Pemuda Persis</a> &copy; <span data-toggle="year-copy"></span>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- OneUI Core JS -->
    <script src="{{ asset('js/oneui.app.js') }}"></script>

    <!-- Laravel Scaffolding JS -->
    <script src="{{ asset('js/laravel.app.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>

    @yield('js_after')
</body>

</html>
