<!DOCTYPE html>
<html lang="en">
<style type="text/css">
     @font-face {
        font-family: 'Poppins';
        src: url('{{storage_path("fonts/Fredoka-Medium.ttf")}}') format('truetype');
      }
      body {
        font-family: "Poppins";
      }
</style>
<body style="position: relative;color: rgb(68, 67, 67); margin: 0;">
    <img style="position: absolute" width="100%" height="100%"
        src="{{ $path_image }}{{ $filename }}">
    <p style="position: absolute; top: 215px; left: 273px; font-size: 14px">
        {{ $no_urut }}</p>
    <p style="position: absolute; top: 240px; left: 623px; font-size: 20px; max-width: 1000px;">
        {{ $nama }} </p>
    <p style="position: absolute; top: 280px; left: 760px; font-size: 16px">
        {{ $tempat_tanggal_lahir }}</p>
    <p style="position: absolute; top: 310px; left: 683px; font-size: 16px">
        {{ $pc }}</p>
    <p style="position: absolute; top: 360px; left: 739px; font-size: 16px">
        {{ $tanggal_maruf }}</p>
    <p style="position: absolute; top: 389px; left: 733px; font-size: 16px">
        {{ $tempat_maruf }}</p>
    <img style="position: absolute; top: 460px; left: 613px; object-fit: cover; width: 100px; height: 130px;" src="{{ $foto }}">
</body>

</html>
