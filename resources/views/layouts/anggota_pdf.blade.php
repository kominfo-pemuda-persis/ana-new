<!DOCTYPE html>
<html lang="en">
    <body style="position: relative;color: rgb(68, 67, 67); margin: 0;">
        <table style="font-size: 12px; border-collapse: collapse">
            <thead style="color: white; background: green">
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">NO</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">FOTO</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">NPA</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">NAMA</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">TEMPAT</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">TANGGAL LAHIR</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">PEKERJAAN</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">PW</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">PD</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">PC</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">PJ</th>
                <th style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 16px; padding-right: 16px; font-size: 10px">STATUS</th>
            </thead>
            <tbody>
                @foreach ($data as $key => $item)
                    <tr>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px; text-align: center">{{ $key + 1 }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">
                            @if ($item['foto'] === 'default.png')
                                <img width="40" height="40" src="https://dev.anaonline.id/images/anggota/default.png">
                            @else
                                <img width="40" height="40" src="{{ $item['linkFoto'] }}">
                            @endif
                        </td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['npa'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['nama_lengkap'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['tempat_lahir'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['tanggal_lahir'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['pekerjaan'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['nama_pw'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['nama_pd'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['nama_pc'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['nama_pj'] }}</td>
                        <td style="border: 1px solid #241f1f;padding-top: 10px; padding-bottom: 10px; padding-left: 10px; padding-right: 10px">{{ $item['status_Aktif'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
