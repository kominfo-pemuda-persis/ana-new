<!DOCTYPE html>
<html lang="en">
<style type="text/css">
    @font-face {
        font-family: 'Poppins';
        src: url('{{ storage_path('fonts/Fredoka-Medium.ttf') }}') format('truetype');
    }

    body {
        font-family: "Poppins";
    }
</style>

<body style="position: relative;color: rgb(68, 67, 67); margin: 0;">
    <img style="position: absolute" width="100%" height="100%" src="{{ $path_image }}{{ $filename }}">
    <p style="position: absolute;top: 210px;left: 235px;font-size: 14px">
        {{ $no_urut }}</p>
    <p style="position: absolute;top: 183px;left: 555px;font-size: 20px;max-width: 1000px;color: white;">
        {{ $nama }} </p>
    <p style="position: absolute;top: 216px;left: 696px;font-size: 16px;color: white;">
        {{ $tempat_tanggal_lahir }}</p>
    <p style="position: absolute;top: 240px;left: 624px;font-size: 16px;color: white;">
        {{ $pc }}</p>
    <p style="position: absolute;top: 284px;left: 676px;font-size: 16px;color: white;">
        {{ $tanggal_maruf }}</p>
    <p style="position: absolute;top: 309px;left: 670px;font-size: 16px;color: white;">
        {{ $tempat_maruf }}</p>
    <p style="position: absolute;top: 427px;left: 796px;font-size: 16px;color: white;">
        {{ $kehadiran }}</p>
    <p style="position: absolute;top: 434px;left: 880px;font-size: 12px;color: white;">
        {{ $kehadiran_predikat }}</p>
    <p style="position: absolute;top: 449px;left: 796px;font-size: 16px;color: white;">
        {{ $keaktifan }}</p>
    <p style="position: absolute;top: 456px;left: 880px;font-size: 12px;color: white;">
        {{ $keaktifan_predikat }}</p>
    <p style="position: absolute;top: 468px;left: 796px;font-size: 16px;color: white;">
        {{ $makalah }}</p>
    <p style="position: absolute;top: 476px;left: 880px;font-size: 12px;color: white;">
        {{ $makalah_predikat }}</p>
    <p style="position: absolute;top: 488px;left: 796px;font-size: 16px;color: white;">
        {{ $presentasi_makalah }}</p>
    <p style="position: absolute;top: 496px;left: 880px;font-size: 12px;color: white;">
        {{ $presentasi_makalah_predikat }}</p>
    <p style="position: absolute;top: 529px;left: 796px;font-size: 16px;color: white;">
        {{ $total }}</p>
    <p style="position: absolute;top: 537px;left: 880px;font-size: 12px;color: white;">
        {{ $total_predikat }}</p>
    <img style="position: absolute;top: 29px;left: 855px;width: 91px;height: 117px;"
        src="{{ $foto }}">
</body>

</html>
