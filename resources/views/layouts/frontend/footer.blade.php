<footer id="footer" class="bg-one">
    <div class="top-footer">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                    <h3>about</h3>
                    <p>
                        Tujuan utama dibuatnya sistem informasi anaonline, sebagai upaya
                        responsif terhadap pengelolaan data anggota Pemuda Persis yang
                        berbasis sistem teknologi informasi, Maka dengan adanya sistem
                        ini, kita bisa lebih mudah dalam mengelola database anggota
                        Pemuda Persis seluruh Indonesia
                    </p>
                </div>
                <!-- End of .col-sm-3 -->

                <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                    <ul>
                        <li>
                            <h3>Our Portfolio</h3>
                        </li>
                        <li><a href="http://persis.or.id/">persis.or.id</a></li>
                        <li><a href="http://anaonline.id/">anaonline.id</a></li>
                        <li><a href="about.html">anisaonline.id</a></li>
                        <li><a href="http://peta-dakwah.netlify.app/">peta-dakwah.id</a></li>
                    </ul>
                </div>
                <!-- End of .col-sm-3 -->

                <div class="col-lg-2 col-md-6 mb-5 mb-md-0">
                    <ul>
                        <li>
                            <h3>Quick Links</h3>
                        </li>
                        <li><a href="{{ url('/about') }}">About</a></li>
                        <li><a href="{{ url('portfolio') }}">Portfolio</a></li>
                        <li><a href="{{ url('team') }}">Team</a></li>
                        <li><a href="{{ url('contact') }}">Contact</a></li>
                    </ul>
                </div>
                <!-- End of .col-sm-3 -->

                <div class="col-lg-3 col-md-6">
                    <ul>
                        <li>
                            <h3>Connect with us Socially</h3>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/profile.php?id=100066747181849">Facebook</a>
                        </li>
                        <li>
                            <a href="https://twitter.com/pp_pemudapersis">Twitter</a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/pp-pemuda-persis/">Linkedin</a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/c/PPPemudaPersis">Youtube</a>
                        </li>
                        <li>
                            <a href="https://github.com/KominfoPemudaPersis">Github</a>
                        </li>
                    </ul>
                </div>
                <!-- End of .col-sm-3 -->
            </div>
        </div>
        <!-- end container -->
    </div>
    <div class="footer-bottom">
        <h6>PP Pemuda Persis © 2022. All rights reserved.</h6>
    </div>
</footer>
<!-- end footer -->

<!-- end Footer Area
========================================== -->