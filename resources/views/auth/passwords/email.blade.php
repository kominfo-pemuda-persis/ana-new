<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Login</title>

    <meta name="description"
          content="OneUI - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts and Styles -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href="{{ mix('/css/oneui.css') }}">
    <link rel="stylesheet" id="css-theme" href="{{ asset('/css/themes/amethyst.css') }} ">
    <link rel="stylesheet" id="css-main" href="{{ asset('/css/custome.css') }}">


    <!-- Scripts -->
    <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
</head>
<body>

<div id="page-container" class="side-trans-enabled">
    <main id="main-container">
        <div class="bg-image" style="background-image: {{ asset('/media/photos/photo2@2x.jpg') }}">
            <div class="hero-static bg-white-95">
                <div class="content">
                    <div class="row justify-content-center">
                        <div class="col-md-8 col-lg-6 col-xl-4">
                            <div class="block block-themed block-fx-shadow mb-0">
                                <div class="block-header bg-success">
                                    <h3 class="block-title">Lupa Password</h3>
                                    <div class="block-options">
                                        <a class="btn-block-option font-size-sm" href="{{ route('login') }}">Sudah punya akun?login di sini</a>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="p-sm-3 px-lg-4 py-lg-5">
                                        <div class="text-center">
                                            <img src="{{ asset("media/res/ANA ONLINE 1.png") }}" width="20%"/>
                                            <h1 class="mb-2">
                                                <span>
                                                      <img src="{{ asset("media/res/ANA ONLINE 2.png") }}" width="45%">
                                                </span>
                                            </h1>

                                        </div>
                                        @include('layouts.alert')
                                        <form class="js-validation-signin" action="{{ route('reset.password.email.npa') }}"
                                              method="POST" novalidate="novalidate">
                                            @csrf
                                            <div class="py-3">
                                                <div class="form-group">
                                                    <input type="text"
                                                           class="form-control form-control-alt form-control-lg"
                                                           id="payload" name="payload" placeholder="Masukan Email Pendaftaran atau NPA">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <button type="submit" href=""
                                                       class="btn btn-block btn-danger">
                                                        <i class="fa fa-fw fa-paper-plane mr-1"></i> Kirim
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content content-full font-size-sm text-muted text-center">
                    <strong>PP Pemuda Persis</strong> © <span data-toggle="year-copy"
                                                              class="js-year-copy-enabled">{{ now()->year }}</span>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- END Page Container -->

<!-- OneUI Core JS -->
<script src="{{ mix('js/oneui.app.js') }}"></script>

<!-- Laravel Scaffolding JS -->
<script src="{{ mix('js/laravel.app.js') }}"></script>
<!-- Page JS Plugins -->
<script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('js/pages/op_auth_signin.min.js') }}"></script>
</body>
</html>
