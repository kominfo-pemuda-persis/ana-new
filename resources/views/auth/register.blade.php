<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Registrasi Akun</title>

    <meta name="description"
          content="OneUI - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts and Styles -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href="{{ mix('/css/oneui.css') }}">
    <link rel="stylesheet" id="css-theme" href="{{ asset('/css/themes/amethyst.min.css') }} ">
    <link rel="stylesheet" id="css-main" href="{{ asset('/css/custome.css') }}">
    @if(isset($anggota))
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    @endif


    <!-- Scripts -->

</head>
<body>

<div id="page-container">
    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div class="hero-static d-flex align-items-center">
            <div class="w-100">
                <!-- Sign In Section -->
                <div class="content content-full bg-white form-auth-bg">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-8 col-xl-6">
                            <div class="block block-themed block-fx-shadow mb-0">
                                <div class="block-header bg-success">
                                    <h3 class="block-title">Buat Akun ANA Online</h3>
                                    <div class="block-options">
                                        {{--                                                <a class="btn-block-option font-size-sm" href="" data-toggle="modal" data-target="#one-signup-terms"></a>--}}
                                        <a class="btn-block-option js-tooltip-enabled" href="{{ route('login') }}"
                                           data-toggle="tooltip" data-placement="left" title=""
                                           data-original-title="Sign In">
                                            Sudah Punya Akun? Login Disini
                                            <i class="fa fa-sign-in-alt"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="p-sm-3 px-lg-4 py-lg-5">
                                        <div class="text-center">
                                            <img src="{{ asset("media/res/ANA ONLINE 1.png") }}" width="15%"/>
                                            <h1 class="mb-2">
                                                <span>
                                                    <img src="{{ asset("media/res/ANA ONLINE 2.png") }}" width="35%">
                                                </span>
                                            </h1>
                                            <p>Ikuti langkah berikut dan isilah formulir untuk membuat akun baru</p>
                                        </div>
                                        @include('layouts.alert')
                                        <form class="js-validation-signup" action="{{route("register.create")}}"
                                              method="POST" novalidate="novalidate">
                                            @csrf
                                            <div class="py-3">
                                                <div class="form-group">
                                                    <label for="npa">Nomor Pokok Anggota (NPA)</label>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <input type="text"
                                                                   class="form-control form-control-lg form-control-alt"
                                                                   id="npa" name="npa"
                                                                   placeholder="NPA | Contoh: 04.0001"
                                                                   value="{{ old("npa", @$anggota->npa) }}">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <button id="check-npa-btn"
                                                                    formaction="{{route('register.ceknpa')}}"
                                                                    type="submit" class="btn btn-block btn-danger"><i
                                                                    class="fa fa-fw fa-search mr-1"></i> Cek NPA
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if(isset($anggota))
                                                    <div class="alert-success p-3 mb-4">
                                                        <p class="font-weight-bold m-1">NPA : <span>{{ $anggota->npa }}</span></p>
                                                        <p class="font-weight-bold m-1">Nama : <span>{{ $anggota->nama_lengkap }}</span></p>
                                                        <p class="font-weight-bold m-1">Email : <span id="anggota-email">{{ $anggota->email }}</span></p>
                                                    </div>

                                                    <div class="text-center">
                                                        <p>Kami akan mengirimkan link verifikasi ke email anda yang terdaftar</p>
                                                        <button id="send-email-btn" type="submit" class="btn px-4 btn-success">Kirim Email</button>
                                                    </div>
                                                @endif
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END Sign In Section -->

                <!-- Footer -->
                <div class="font-size-sm text-center text-muted py-3 form-auth-footer">
                    <strong>ANA Online</strong> &copy; <span data-toggle="year-copy"></span>
                </div>
                <!-- END Footer -->
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->

<!-- OneUI Core JS -->
<script src="{{ mix('js/oneui.app.js') }}"></script>

<!-- Laravel Scaffolding JS -->
<script src="{{ mix('js/laravel.app.js') }}"></script>
<!-- Page JS Plugins -->
<script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('js/pages/op_auth_signin.min.js') }}"></script>
@if(isset($anggota))
<script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/pages/pg_auth_register.js') }}"></script>
@endif
</body>
</html>
