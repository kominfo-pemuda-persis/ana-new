<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Login</title>

        <meta name="description" content="OneUI - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Icons -->
{{--        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">--}}
{{--        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">--}}
{{--        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">--}}

        <!-- Fonts and Styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <link rel="stylesheet" id="css-main" href="{{ mix('/css/oneui.css') }}">
        <link rel="stylesheet" id="css-theme" href="{{ asset('/css/themes/amethyst.css') }} ">
        <link rel="stylesheet" id="css-main" href="{{ asset('/css/custome.css') }}">


        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
            window.addEventListener("load", () => {
                document.querySelector("#show-hide-password").addEventListener("click", () => {
                    let passwordEl = document.querySelector("#password");
                    let iconEl = document.querySelector("#icon-password");
                    let attr = passwordEl.getAttribute("type");

                    if(attr === "password"){
                        passwordEl.setAttribute("type", "text");
                        iconEl.classList.remove("fa-eye")
                        iconEl.classList.add("fa-eye-slash")
                    }else{
                        passwordEl.setAttribute("type", "password");
                        iconEl.classList.remove("fa-eye-slash")
                        iconEl.classList.add("fa-eye")
                    }
                })
            })
        </script>
        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-V33JPYB1SF"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', 'G-V33JPYB1SF');
        </script>
    </head>
    <body>

    <div id="page-container" class="side-trans-enabled">
        <main id="main-container">
            <div class="bg-image" style="background-image: {{ asset('/media/photos/photo2@2x.jpg') }}">
                <div class="hero-static bg-white-95">
                    <div class="content">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-lg-6 col-xl-4">
                                <div class="block block-themed block-fx-shadow mb-0">
                                    <div class="block-header bg-success">
                                        <h3 class="block-title"><a href="/" style="color:#fff">🔙 Kembali</a></h3>
                                    </div>
                                    <div class="block-content">
                                        <div class="p-sm-3 px-lg-4 py-lg-5">
                                            <div class="text-center">
                                                <img src="{{ asset("media/res/ANA ONLINE 1.png") }}" width="25%"/>
                                                <h1 class="mb-2">
                                                    <span>
                                                      <img src="{{ asset("media/res/ANA ONLINE 2.png") }}" width="45%">
                                                    </span>
                                                </h1>

                                            </div>
                                            @include('layouts.alert')
                                            <form class="js-validation-signin" action="{{ route('login.cek') }}" method="POST" novalidate="novalidate">
                                                @csrf
                                                <div class="py-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-alt form-control-lg" id="npa" name="npa" placeholder="NPA | Contoh: 04.0001">
                                                    </div>
                                                    <div class="input-group">
                                                        <input type="password" class="form-control form-control-alt form-control-lg" id="password" name="password" placeholder="Kata Sandi / Password">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-primary outline-none" type="button" id="show-hide-password">
                                                                <i id="icon-password" class="fa fa-eye-slash" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="login_remember" name="login_remember">
                                                            <label class="custom-control-label font-w400" for="login_remember">Ingatkan Saya</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">

                                                    <div class="col-md-6">
                                                        <a type="button" href="{{ url('register') }}" class="btn btn-block btn-danger">
                                                            <i class="fa fa-fw fa-user-plus mr-1"></i> Buat Akun
                                                        </a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <button type="submit" class="btn btn-block btn-primary">
                                                            <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Masuk
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="form-group row">
                                                <div class="col-md-12 text-center">
                                                    <a type="button" href="{{ route('password.request') }}">
                                                        <i class="fa fa-fw fa-key mr-1"></i> Lupa Password?
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content content-full font-size-sm text-muted text-center">
                        <strong>PP Pemuda Persis</strong> © <span data-toggle="year-copy" class="js-year-copy-enabled">{{ now()->year }}</span>
                    </div>
                </div>
            </div>
        </main>
    </div>
        <!-- END Page Container -->

        <!-- OneUI Core JS -->
        <script src="{{ mix('js/oneui.app.js') }}"></script>

        <!-- Laravel Scaffolding JS -->
        <script src="{{ mix('js/laravel.app.js') }}"></script>
        <!-- Page JS Plugins -->
        <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

        <!-- Page JS Code -->
        <script src="{{ asset('js/pages/op_auth_signin.min.js') }}"></script>
    </body>
</html>
