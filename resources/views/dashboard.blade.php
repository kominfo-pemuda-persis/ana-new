@extends('layouts.backend')
@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">

@endsection
@section('js_after')
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>


    <script src="https://code.highcharts.com/highcharts.js"></script>

    {{--    datatable regs--}}
    <script src="{{ asset('js/dashboard/datatable_already_regs.js') }}"></script>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-V33JPYB1SF"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-V33JPYB1SF');
    </script>

    <script type="text/javascript">

        $(function () {

            $('#cUsia').highcharts({
                chart: {
                    type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        viewDistance: 25,
                        depth: 40
                    }
                },
                title: {
                    text: 'Grafik Usia Anggota Pemuda Persis'
                },
                xAxis: {
                    categories: ['Range Usia']
                },
                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Jumlah'
                    }
                },
                tooltip: {
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
                },
                series: [
                        @foreach ($usiaSummary as $data)
                    {
                        name: ' {{$data->range_umur}}',
                        data: [{{$data->jumlah}}],
                        stack: 'usia'
                    },
                    @endforeach
                ]
            });
            $('#cJenis').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Grafik Jenis Keanggotaan Anggota Pemuda Persis'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
                },
                series:[
                    {
                        name: "Jenis Keanggotaan",
                        colorByPoint: true,
                        data: [
                                @foreach ($statusKeanggotaanSummary as $data)
                            {
                                name: '{{$data->status}}',
                                y: {{$data->jumlah}},
                            },
                            @endforeach
                        ]
                    }
                ]
            });
            $('#cstatus').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Grafik status Keanggotaan Anggota Pemuda Persis'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
                },
                series:[
                    {
                        name: "Status Keanggotaan",
                        colorByPoint: true,
                        data: [
                                @foreach ($statusSummary as $data)
                            {
                                name: '{{$data->status_aktif}}',
                                y: {{$data->jumlah}},
                            },
                            @endforeach
                        ]
                    }
                ]
            });
            $('#cPendidikan').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Grafik Pendidikan Anggota Pemuda Persis'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
                },
                series:[
                    {
                        name: "Pendidikan",
                        colorByPoint: true,
                        data: [
                                @foreach ($pendidikanSummary as $data)
                            {
                                name: '{{$data->level_pendidikan}}',
                                y: {{$data->jumlah}},
                            },
                            @endforeach
                        ]
                    }
                ],
            });
            $('#cMerital').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Grafik Status Merital Anggota Pemuda Persis'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
                },
                series:[
                    {
                        name: "Merital",
                        colorByPoint: true,
                        data: [
                                @foreach ($meritalSummary as $merital)
                            {
                                name: '{{$merital->status}}',
                                y: {{$merital->jumlah}},
                            },
                            @endforeach
                        ]
                    }
                ]
            });
            $('#cDarah').highcharts({
                chart: {
                    type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        viewDistance: 25,
                        depth: 40
                    }
                },
                title: {
                    text: 'Grafik Golongan Darah Anggota Pemuda Persis'
                },
                xAxis: {
                    categories: ['Status']
                },
                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Jumlah'
                    }
                },
                tooltip: {
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
                },
                series: [
                        @foreach ($golDarahSummary as $data)
                    {
                        name: '{{$data->gol}}',
                        data: [{{$data->jumlah}}],
                        stack: 'usia'
                    },
                    @endforeach
                ]
            });
        });
    </script>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-V33JPYB1SF"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-V33JPYB1SF');
    </script>

@endsection
@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Dashboard</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
            <div class="row justify-content-center">
                <div class="block-content">
                    <div id="sum_monografi" class="row">
                        @foreach($sum_monografis as $title => $mono)
                            <div class="col-lg-3 col-md-6 col-sm-12 col-xl-3">
                                <div class="block block-rounded d-flex flex-column">
                                    <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                        <dl class="mb-0">
                                            <dt class="font-size-h2 font-w700">{{ $mono }}</dt>
                                            <dd class="text-muted mb-0">Monografi {{$title}}</dd>
                                        </dl>
                                        <div class="item item-rounded bg-body">
                                            <i class="fa fa-map-marker-alt font-size-h3 text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                        <a class="font-w500 d-flex align-items-center" href="/jamiyyah/monografi/{{Str::lower($title)}}">
                                            Detail Info
                                            <i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div><!-- /.row -->
                    <div id="sum_performa" class="row">
                        @foreach($sum_performas as $title => $perf)
                            <div class="col-lg-3 col-md-6 col-sm-12 col-xl-3">
                                <div class="block block-rounded d-flex flex-column">
                                    <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                        <dl class="mb-0">
                                            <dt class="font-size-h2 font-w700">{{ $perf }}</dt>
                                            <dd class="text-muted mb-0">Performa {{$title}}</dd>
                                        </dl>
                                        <div class="item item-rounded bg-body">
                                            <i class="fa fa-tasks font-size-h3 text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                        <a class="font-w500 d-flex align-items-center" href="/jamiyyah/performa/{{Str::lower($title)}}">
                                            Detail Info
                                            <i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div><!-- /.row -->
                   <div id="sum_tafiq" class="row">
    @foreach($sum_tafiq as $tafiqTitle => $count)
    <div class="col-lg-3 col-md-6 col-sm-12 col-xl-3">
        <div class="block block-rounded d-flex flex-column">
            <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                <dl class="mb-0">
                    <dt class="font-size-h2 font-w700">{{ $count }}</dt>
                    <dd class="text-muted mb-0">{{ $tafiqTitle }}</dd>
                </dl>
                <div class="item item-rounded bg-body">
                    <i class="fa fa-tasks font-size-h3 text-primary"></i>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
                    <div id="sum_user_anggota" class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="block block-rounded d-flex flex-column">
                                <div
                                    class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                    <dl class="mb-0">
                                        <dt class="font-size-h2 font-w700">{{ $anggota }}</dt>
                                        <dd class="text-muted mb-0">Anggota</dd>
                                    </dl>
                                    <div class="item item-rounded bg-body">
                                        <i class="fa fa-user font-size-h3 text-primary"></i>
                                    </div>
                                </div>
                                <div
                                    class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                    <a class="font-w500 d-flex align-items-center" href="/anggota">
                                        Detail Info
                                        <i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="block block-rounded d-flex flex-column">
                                <div
                                    class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                    <dl class="mb-0">
                                        <dt class="font-size-h2 font-w700">{{ $heregistrasi }}</dt>
                                        <dd class="text-muted mb-0">Sudah Heregistrasi</dd>
                                    </dl>
                                    <div class="item item-rounded bg-body">
                                        <i class="fa fa-user font-size-h3 text-primary"></i>
                                    </div>
                                </div>
                                <div
                                    class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                    <a class="font-w500 d-flex align-items-center" href="/data-user">
                                        Detail Info
                                        <i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="block block-rounded d-flex flex-column">
                                <div
                                    class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                    <dl class="mb-0">
                                        <dt class="font-size-h2 font-w700">{{ $userTeregistrasi }}</dt>
                                        <dd class="text-muted mb-0">User Teregistrasi</dd>
                                    </dl>
                                    <div class="item item-rounded bg-body">
                                        <i class="fa fa-user font-size-h3 text-primary"></i>
                                    </div>
                                </div>
                                <div
                                    class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                    <a class="font-w500 d-flex align-items-center" href="/data-user">
                                        Detail Info
                                        <i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div>
            </div>
        <div class="row justify-content-center">
            <div class="col-md-12 col-xl-12">
                <div class="block">
                    <div class="block-content">
                        <!-- Data PC Yang sudah ada -->
                        <div class="box box-success">
                            <div class="box-header with-border">
                                    <h3 class="box-title">Data Heregistrasi Online Anggota Pemuda Persis</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="dataTable_wrapper">
                                                <table id="data_pc" class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:20px" class="number">#</th>
                                                        <th>PW</th>
                                                        <th>PD</th>
                                                        <th>PC</th>
                                                        <th>Jumlah Anggota</th>
                                                        <th>Sudah HER</th>
                                                        <th>Belum HER</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php($no = 1)
                                                    @php($total = 0)
                                                    @foreach ($alreadyRegs as $areg)
                                                        <tr class="pilih">
                                                            {{--                                                        <td>{{ $no++ }} </td>--}}
                                                            <td></td>
                                                            <td>{{ $areg->pw }} </td>
                                                            <td>{{ $areg->pd }} </td>
                                                            <td>{{ $areg->pc }} </td>
                                                            <td>{{ $areg->jumlah }} </td>
                                                            <td>{{ $areg->SUDAH_HER }} </td>
                                                            <td>{{ $areg->BELUM_HER }} </td>
                                                            {{--                                                        @php($total += $areg->jumlah)--}}

                                                        </tr>
                                                    @endforeach
                                                    {{--                                                <tr>--}}
                                                    {{--                                                    <td colspan="4" align="center">--}}
                                                    {{--                                                        <bold>Jumlah Total</bold>--}}
                                                    {{--                                                    </td>--}}
                                                    {{--                                                    <td>{{$total}}</td>--}}
                                                    {{--                                                </tr>--}}
                                                    </tbody>
                                                    <tfoot>
                                                    {{--                                                <tr>--}}
                                                    {{--                                                    <th style="width:20px">#</th>--}}
                                                    {{--                                                    <th>PW</th>--}}
                                                    {{--                                                    <th>PD</th>--}}
                                                    {{--                                                    <th>PC</th>--}}
                                                    {{--                                                    <th>Jumlah</th>--}}
                                                    {{--                                                </tr>--}}

                                                    <tr>
                                                        <th colspan="4" style="text-align:right">
                                                            <bold>Total</bold>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                    </tfoot>

                                                </table>
                                            </div><!-- box-body -->

                                        </div><!-- box -->
                                    </div><!-- panel-body -->
                                </div> <!-- panel-body  -->
                            </div> <!-- panel-body -->
                            <!-- Data Golongan Darah -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box box-danger">
                                        <div class="bo box-header">
                                            <h1 class="box-title">Data Statistik Golongan Darah Anggota Pemuda Persis</h1>
                                        </div><!-- /.box-header -->
                                        <div class="box box-body">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Golongan Darah</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php(@$no = 1)
                                                @php($total = 0)
                                                @foreach ($golDarahSummary as $data)
                                                    <tr>
                                                        <td> {{$no}} </td>
                                                        <td> {{$data->gol}} </td>
                                                        <td> {{$data->jumlah}} </td>
                                                    </tr>
                                                    @php($total += $data->jumlah)
                                                    @php($no++)
                                                @endforeach
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <bold>Jumlah Total</bold>
                                                    </td>
                                                    <td>{{$total}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="box box-success">
                                        <div class="box box-body">
                                            <!-- fungsi yang ditampilkan di browser  -->
                                            <div id="cDarah" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div> <!-- Data Golongan Darah -->
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box box-danger">
                                        <div class="bo box-header">
                                            <h1 class="box-title">Data Statistik Status Merital Anggota Pemuda Persis</h1>
                                        </div><!-- /.box-header -->
                                        <div class="box box-body">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Status</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php($no = 1)
                                                @php($total = 0)
                                                @foreach ($meritalSummary as $merital)

                                                    <tr>
                                                        <td>{{$no}}</td>
                                                        <td>{{$merital->status}}</td>
                                                        <td>{{$merital->jumlah}}</td>
                                                    </tr>
                                                    @php($total += $merital->jumlah)
                                                    @php($no++)
                                                @endforeach
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <bold>Jumlah Total</bold>
                                                    </td>
                                                    <td>{{$total}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="box box-success">
                                        <div class="box box-body">
                                            <!-- fungsi yang ditampilkan di browser  -->
                                            <div id="cMerital" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div> <!-- Data Status Merital -->
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box box-info">
                                        <div class="bo box-header">
                                            <h1 class="box-title">Data Statistik Usia Anggota Pemuda Persis</h1>
                                        </div><!-- /.box-header -->
                                        <div class="box box-body">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Range Usia</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php($no = 1)
                                                @php($total = 0)
                                                @foreach ($usiaSummary as $data)

                                                    <tr>
                                                        <td> {{$no}} </td>
                                                        <td> {{$data->range_umur}} </td>
                                                        <td> {{$data->jumlah}} </td>
                                                    </tr>

                                                    @php($total += $data->jumlah)
                                                    @php($no++)
                                                @endforeach
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <bold>Jumlah Total</bold>
                                                    </td>
                                                    <td>{{$total}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="box box-info">
                                        <div class="box box-body">
                                            <!-- fungsi yang ditampilkan dibrowser  -->
                                            <div id="cUsia" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div> <!-- Usia -->

                            </div>
                            <!-- Data  Pendidikan -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box box-warning">
                                        <div class="bo box-header">
                                            <h1 class="box-title">Data Statistik Pendidikan Anggota Pemuda Persis</h1>
                                        </div><!-- /.box-header -->
                                        <div class="box box-body">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Level Pendidikan</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php($no = 1)
                                                @php($total = 0)
                                                @foreach ($pendidikanSummary as $data)
                                                    <tr>
                                                        <td> {{$no}} </td>
                                                        <td> {{$data->level_pendidikan}} </td>
                                                        <td> {{$data->jumlah}} </td>
                                                    </tr>

                                                    @php($total += $data->jumlah)
                                                    @php($no++)
                                                @endforeach


                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <bold>Jumlah Total</bold>
                                                    </td>
                                                    <td> {{$total}} </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="box box-success">
                                        <div class="box box-body">
                                            <div id="cPendidikan" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div> <!-- Usia -->
                            </div>
                            <!-- Data Jenis Keanggotaan -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box box-danger">
                                        <div class="bo box-header">
                                            <h1 class="box-title">Data Statistik Jenis Keanggotaan Anggota Pemuda
                                                Persis</h1>
                                        </div><!-- /.box-header -->
                                        <div class="box box-body">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Jenis Anggota</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php($no = 1)
                                                @php($total4 = 0)
                                                @foreach ($statusKeanggotaanSummary as $data)
                                                    <tr>
                                                        <td> {{$no}} </td>
                                                        <td> {{$data->status}} </td>
                                                        <td> {{$data->jumlah}} </td>
                                                    </tr>

                                                    @php($total4 += $data->jumlah)
                                                    @php($no++)
                                                @endforeach

                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <bold>Jumlah Total</bold>
                                                    </td>
                                                    <td> {{$total4}} </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="box box-success">
                                        <div class="box box-body">
                                            <!-- fungsi yang ditampilkan di browser  -->
                                            <div id="cJenis" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div> <!-- Data Status Merital -->
                            </div>
                            <!-- Data Status Keanggotaan -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box box-danger">
                                        <div class="bo box-header">
                                            <h1 class="box-title">Data Statistik Status Keanggotaan Anggota Pemuda
                                                Persis</h1>
                                        </div><!-- /.box-header -->
                                        <div class="box box-body">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Status Anggota</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php($no = 1)
                                                @php($total4 = 0)
                                                @foreach ($statusSummary as $data)
                                                    <tr>
                                                        <td> {{$no}} </td>
                                                        <td> {{$data->status_aktif}} </td>
                                                        <td> {{$data->jumlah}} </td>
                                                    </tr>

                                                    @php($total4 += $data->jumlah)
                                                    @php($no++)
                                                @endforeach

                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <bold>Jumlah Total</bold>
                                                    </td>
                                                    <td> {{$total4}} </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="box box-success">
                                        <div class="box box-body">
                                            <!-- fungsi yang ditampilkan di browser  -->
                                            <div id="cstatus" style="height: 400px"></div>
                                        </div>
                                    </div>
                                </div> <!-- Data Status Anggota -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </div>
    <!-- END Page Content -->
@endsection
