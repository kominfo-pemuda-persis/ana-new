@extends('layouts.backend')

@section('js_after')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
            integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"
            integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script>
        $(document).ready(() => {
            let dropifyCSV = $('.csv-file-container-0 > .dropify-csv').dropify({
                messages: {
                    'default': 'Drag dan drop dokumen csv kedalam kotak ini',
                    'replace': 'Drag dan drop atau click untuk mengganti dokumen csv',
                    'remove': 'Hapus',
                    'error': 'Terjadi kesalahan'
                },
                error: {
                    'fileExtension': 'Format file tidak diizinkan. File yang diizinkan hanya (@{{ value }}).'
                }
            });

            $("#form-esyahadah").submit(function(e){
                e.preventDefault();
                let $form = $("#form-esyahadah");
                if($("input[name='csv']").prop("files").length <= 0){
                    swal.close();
                    swal("Upload ditolak", "Silahkan sertakan File CSV dengan benar", "error");
                    return;
                }

                swal({
                        title: 'Loading..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    })

                    var button = $form.find('button[type="submit"]');
                    button.attr('disabled', 'disabled');
                    button.text('processing..');


                    $.ajax({
                        url: $form.attr('action'),
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        processData: false,
                        contentType : false,
                        data: new FormData($form[0]),
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        success: function (res) {
                            swal({
                                title: "Berhasil!",
                                text: res.message,
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                button.removeAttr('disabled');
                                button.html('<i class="fa fa-fw fa-save mr-1"></i> Buat E-syahadah');
                                window.location.reload();
                            });
                        },

                        error: function (jqXHR, exception) {
                            swal.close();
                            let message = `Terjadi kendala teknis, Mohon kontak Bidang Administrasi PP. <br> info error: <b>${jqXHR.responseJSON.message}</b>`
                            swal("Upload ditolak", message, "error");
                            button.removeAttr('disabled');
                            button.html('<i class="fa fa-fw fa-save mr-1"></i> Buat E-syahadah');
                        },
                    });
            })

        })
    </script>
@endsection
@section('css_after')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
          integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection
@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    E-syahadah <small
                        class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">E-syahadah</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="bg-white overflow-scroll">
        <form id="form-esyahadah" action="{{ route('generate.esyahadah.pdf')}}">
            @csrf
            <div>
                <div class="content content-full text-center">
                    <div class="flex flex-row items-center pt-6">
                        <div class="col-xl-6 col-lg-6 col-sm-12 mx-auto text-left">
                            <div class="form-group">
                                <label for="foto-transfer" class="control-label">Dokumen CSV</label>
                                <p style="font-size: 14px">(Silahkan Download template excel nya <a
                                    href="{{ env('S3_STATIC_FOLDER') }}CAANG.CSV">di sini</a>)</p>
                                <div style="width: 100%;margin-top: 0.5rem;font-size: 0.875rem;color: #d26a5c;"
                                     class="invalid-feedback-file normal-form">
                                </div>

                                <div class="row text-left">
                                    <div class="col-sm-12 mx-auto text-left csv-file-container-0" style="cursor: pointer; height: 196px; margin-top: 10px">
                                        <input type="file" name="csv" id="foto-upload"
                                               class="dropify-csv form-control" data-allowed-file-extensions="csv">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="m-2 d-inline-block pb-6">
                        <button class="btn btn-success px-4 py-2" type="submit">
                            <i class="fa fa-fw fa-save mr-1"></i> Buat E-syahadah
                        </button>
                    </span>
                </div>
        </form>
    </div>
    </div>
    <!-- END Page Content -->
@endsection
