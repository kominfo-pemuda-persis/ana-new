<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Administrasi Online - Otonom Persis</title>

    <meta name="description" content="Administrasi Online">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Tim Kominfo">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Icons -->
    <link rel="shortcut icon" href="{{ asset('media/res/pemuda_ico.ico') }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

    <!-- Fonts and Styles -->
    @yield('css_before')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href="{{ asset('/css/oneui.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/custome.css') }}">



    <!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="{{ asset('/css/themes/amethyst.css') }}"> -->
    @yield('css_after')

    <!-- Scripts -->

</head>

<body>
        <div id="page-container" class="sidebar-o enable-page-overlay sidebar-dark side-scroll page-header-fixed">

            <nav id="sidebar" aria-label="Main Navigation">
                <!-- Side Header -->
                <div class="content-header bg-white-10">
                    <!-- Logo -->
                    <a class="font-w600 text-dual" href="/">
                        <img src="{{ asset("media/res/ANA_ONLINE_4.png") }}" width="20%"/>
                        <span>
                            <img src="{{ asset("media/res/ANA_ONLINE_3.png") }}" width="75%">
                        </span>
                    </a>
                    <!-- END Logo -->

                </div>
                <!-- END Side Header -->

                <!-- Side Navigation -->
                <div class="content-side content-side-full">
                    <ul class="nav-main">
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('dashboard') ? ' active' : '' }}" href="{{url('dashboard')}}">
                                <i class="nav-main-link-icon si si-cursor"></i>
                                <span class="nav-main-link-name">Dashboard</span>
                            </a>
                        </li>
                        
                        <li class="nav-main-heading">Data Settings</li>

                        
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ Route::is('data-user.*') ? 'active' : '' }}"
                               href="{{ url('data-user') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">User</span>
                            </a>
                        </li>
                        
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ Route::is('data-user.*') ? 'active' : '' }}"
                               href="{{ url('role') }}">
                                <i class="nav-main-link-icon si si-users"></i>
                                <span class="nav-main-link-name">Role & Permission</span>
                            </a>
                        </li>
                    </ul>
                </div>  
        </nav>
        <!-- END Sidebar -->

        <!-- Main Container -->
        <main id="main-container">
            <div class="content content-full mt-5">
                <div class="row">
                    <div class="col-sm-12 d-flex flex-column align-items-sm-center">
                        <div class="d-flex justify-content-center">
                            <img class="img-fluid" src="{{ asset("media/errors/errors.png") }}" width="150px">
                        </div>
                        <div class="mt-4">
                            <h1 class="text-center mb-1 text-danger">Warning Error 403 (Forbidden)!</h1>
                            <h3 class="text-center">Antum tidak diizinkan untuk mengakses halaman ini. <br> Silahkan hubungi admin antum di PC/PD/PW/PP.</h3>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        <footer id="page-footer" class="bg-body-light">
            <div class="content py-3">
                <div class="row font-size-sm">
                    <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
                        Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600" href="https://www.facebook.com/kominfopemudapersis" target="_blank">Tim Kominfo</a>
                    </div>
                    <div class="col-sm-6 order-sm-1 py-1 text-center text-sm-left">
                        <a class="font-w600" href="https://www.facebook.com/kominfopemudapersis" target="_blank">ANA Pemuda Persis</a> &copy; <span data-toggle="year-copy"></span>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- OneUI Core JS -->
    <script src="{{ asset('js/oneui.app.js') }}"></script>

    <!-- Laravel Scaffolding JS -->
    <script src="{{ asset('js/laravel.app.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>

    @yield('js_after')
</body>

</html>
