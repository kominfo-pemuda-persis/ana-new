@extends('layouts.frontend')
@section('title', 'Portfolio - Ana Online Landing Page')
@section('content')
<section class="single-page-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Portfolio</h2>
        <ol class="breadcrumb header-bradcrumb justify-content-center">
          <li class="breadcrumb-item">
            <a href="index.html" class="text-white">Home</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">
            Portfolio
          </li>
        </ol>
      </div>
    </div>
  </div>
</section>

<!-- Start Portfolio Section
		=========================================== -->

<section class="portfolio section-sm" id="portfolio">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-xl-6 col-lg-8">
        <!-- section title -->
        <div class="title text-center">
          <h2>Our Works</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro,
            veritatis. Lorem ipsum dolor sit amet, consectetur adipisicing
            elit. Minima, vitae?
          </p>
          <div class="border"></div>
        </div>
        <!-- /section title -->
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="portfolio-filter">
          <button type="button" data-filter="all">All</button>
          <button type="button" data-filter="android">Android</button>
          <button type="button" data-filter="website">Web App</button>
          <button type="button" data-filter="design">
            Design Web & Android
          </button>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="filtr-container">
              <div class="col-md-3 col-sm-6 col-xs-6 filtr-item" data-category="website">
                <div class="portfolio-block">
                  <img class="img-fluid" src="{{ asset('frontend/images/portfolio/portfolio-1.png') }}" alt="" />
                  <div class="caption">
                    <a class="search-icon" href="{{ asset('frontend/images/portfolio/portfolio-1.png') }}"
                      data-lightbox="image-1">
                      <i class="tf-ion-ios-search-strong"></i>
                    </a>
                    <h4><a href="portfolio.html">Anisa Online</a></h4>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-6 filtr-item" data-category="website">
                <div class="portfolio-block">
                  <img class="img-fluid" src="{{ asset('frontend/images/portfolio/portfolio-2.png')}}" alt="" />
                  <div class="caption">
                    <a class="search-icon" href="{{ asset('frontend/images/portfolio/portfolio-2.png')}}"
                      data-lightbox="image-1">
                      <i class="tf-ion-ios-search-strong"></i>
                    </a>
                    <h4><a href="portfolio.html">Ana Online</a></h4>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-6 filtr-item" data-category="website">
                <div class="portfolio-block">
                  <img class="img-fluid" src="{{ asset('frontend/images/portfolio/portfolio-3.png') }}" alt="" style="
                          background-color: rgb(139, 139, 139) !important;
                          border-radius: 5px;
                        " />
                  <div class="caption">
                    <a class="search-icon" href="{{ asset('frontend/images/portfolio/portfolio-3.png') }}"
                      data-lightbox="image-1">
                      <i class="tf-ion-ios-search-strong"></i>
                    </a>
                    <h4><a href="portfolio.html">Persis</a></h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /end col-lg-12 -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- End section -->

@endsection