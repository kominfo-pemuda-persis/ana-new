@extends('layouts.frontend')
@section('title', 'Contact - Ana Online Landing Page')
@section('content')
<section class="single-page-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Contact Us</h2>
        <ol class="breadcrumb header-bradcrumb justify-content-center">
          <li class="breadcrumb-item">
            <a href="index.html" class="text-white">Home</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">
            Contact Us
          </li>
        </ol>
      </div>
    </div>
  </div>
</section>

<!--Start Contact Us
	=========================================== -->
<section class="contact-us" id="contact">
  <div class="container">
    <div class="row justify-content-center">
      <!-- section title -->
      <div class="col-xl-6 col-lg-8">
        <div class="title text-center">
          <h2>Get In Touch</h2>
          <p>أنا مسلم قبل كل شيء</p>
          <div class="border"></div>
        </div>
      </div>
      <!-- /section title -->
    </div>
    <div class="row">
      <!-- Form -->
      <div class="contact-form col-md-12">
        <ul class="d-flex justify-content-center">
          <li>
            <a href="https://www.instagram.com/pp_pemudapersis/">Instagram</a>
          </li>
          <li class="ml-2">
            <a href="https://web.facebook.com/profile.php?id=100066747181849">Facebook</a>
          </li>
          <li class="ml-2">
            <a href="https://www.youtube.com/c/pppemudapersis">Youtube</a>
          </li>
          <li class="ml-2">
            <a href="https://twitter.com/pp_pemudapersis">Twitter</a>
          </li>
        </ul>
        <ul class="d-flex justify-content-center mt-3">
          <li class="ml-2">
            <a href="https://www.linkedin.com/in/pp-pemudapersis/">Linkedin</a>
          </li>
          <li class="ml-2">
            <a href="https://www.linkedin.com/company/pp-pemuda-persis/">Linkedin Company</a>
          </li>
          <li class="ml-2">
            <a href="https://s.id/anaonline">System Anaonline</a>
          </li>
        </ul>
      </div>
      <!-- ./End Contact Form -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- end section -->

<!--================================
=            Google Map            =
=================================-->
<div class="google-map">
  <iframe
    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d8179.461570466177!2d107.607665!3d-6.914674!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x189a05f37b735e92!2zNsKwNTQnNTIuOCJTIDEwN8KwMzYnMjcuNiJF!5e1!3m2!1sen!2sus!4v1665212398213!5m2!1sen!2sus"
    width="100%" height="450" style="border: 0" allowfullscreen="" loading="lazy"
    referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>
<!--====  End of Google Map  ====-->

@endsection