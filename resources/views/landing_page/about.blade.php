@extends('layouts.frontend')
@section('title', 'About - Ana Online Landing Page')

@section('content')

<body id="body">
  <!--
  Start Preloader
  ==================================== -->
  <div id="preloader">
    <div class="preloader">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  <!--
  End Preloader
  ==================================== -->
  <section class="single-page-header">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>About Us</h2>
          <ol class="breadcrumb header-bradcrumb justify-content-center">
            <li class="breadcrumb-item">
              <a href="index.html" class="text-white">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">
              About Us
            </li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="about-shot-info section-sm">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 mt-20">
          <h2 class="mb-3">We Create Anaonline<br /></h2>
          <p>
            Tujuan utama dibuatnya sistem informasi anaonline, sebagai upaya
            responsif terhadap pengelolaan data anggota Pemuda Persis yang
            berbasis sistem teknologi informasi, Maka dengan adanya sistem
            ini, kita bisa lebih mudah dalam mengelola database anggota Pemuda
            Persis seluruh Indonesia
          </p>
        </div>
        <div class="col-lg-6 mt-4 mt-lg-0 text-center">
          <img loading="lazy" class="img-fluid" src="{{ asset('frontend/images/img/logo.png') }}" width="320" alt="" />
        </div>
      </div>
    </div>
  </section>

  <section class="company-mission section-sm bg-gray">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3>Our Mission</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere
            in suscipit voluptatum totam dolores assumenda vel, quia earum
            voluptatem blanditiis vero accusantium saepe aliquid nulla nemo
            accusamus, culpa inventore! Culpa nemo aspernatur tenetur, at quam
            aliquid reprehenderit obcaecati dicta illum mollitia, perferendis
            hic, beatae voluptates? Ex labore, obcaecati harum nam.
          </p>
          <img loading="lazy" src="{{ asset('frontend/images/company/company-image-2.jpg') }}" alt=""
            class="img-fluid mt-30" />
        </div>
        <div class="col-md-6 mt-5 mt-md-0">
          <h3>Our Vision</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere
            in suscipit voluptatum totam dolores assumenda vel, quia earum
            voluptatem blanditiis vero accusantium saepe aliquid nulla nemo
            accusamus, culpa inventore! Culpa nemo aspernatur tenetur, at quam
            aliquid reprehenderit obcaecati dicta illum mollitia, perferendis
            hic, beatae voluptates? Ex labore, obcaecati harum nam.
          </p>
          <img loading="lazy" src="{{ asset('frontend/images/company/company-image-3.jpg') }}" alt=""
            class="img-fluid mt-30" />
        </div>
      </div>
    </div>
  </section>

  <!--
Start Call To Action
==================================== -->
  <section class="call-to-action-2 section">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a
            tempor eros. Pellentesque elementum nulla sed justo tempor posuere
            sit amet sit amet ligula.
          </h2>
        </div>
      </div>
      <!-- End row -->
    </div>
    <!-- End container -->
  </section>
  <!-- End section -->

  <section class="section gallery">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-8">
          <div class="title text-center">
            <h2>Sneak Peak Gallery</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore
              numquam esse vitae recusandae qui aspernatur, blanditiis,
              laboriosam dignissimos dolore facere provident ex? Porro,
              praesentium consectetur tempore. Nulla, error eius dolorem.
            </p>
            <div class="border"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="company-gallery">
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-1.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-2.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-3.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-4.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-5.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-5.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-1.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-2.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-3.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-4.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-5.jpg') }}" alt="" />
            <img loading="lazy" src="{{ asset('frontend/images/company/gallery-5.jpg') }}" alt="" />
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Start Our Team
		=========================================== -->
  <section class="team" id="team">
    <div class="container">
      <div class="row justify-content-center">
        <!-- section title -->
        <div class="col-xl-6 col-lg-8">
          <div class="title text-center">
            <h2>Our Team</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Doloremque quasi tempora obcaecati, quis similique quos.
            </p>
            <div class="border"></div>
          </div>
        </div>
        <!-- /section title -->
      </div>
      <div class="row">
        <!-- team member -->
        <div class="col-lg-4 col-md-6">
          <div class="team-member text-center">
            <div class="member-photo">
              <!-- member photo -->
              <img loading="lazy" class="img-fluid" src="{{ asset('frontend/images/team/member-1.jpg') }}"
                alt="Meghna" />
              <!-- /member photo -->
            </div>

            <!-- member name & designation -->
            <div class="member-content">
              <h3>Michael Jonson</h3>
              <span>Head Of Marketing</span>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Aspernatur necessitatibus ullam, culpa odio.
              </p>
            </div>
            <!-- /member name & designation -->
          </div>
        </div>
        <!-- end team member -->

        <!-- team member -->
        <div class="col-lg-4 col-md-6">
          <div class="team-member text-center">
            <div class="member-photo">
              <!-- member photo -->
              <img loading="lazy" class="img-fluid" src="{{ asset('frontend/images/team/member-2.jpg') }}"
                alt="Meghna" />
              <!-- /member photo -->
            </div>

            <!-- member name & designation -->
            <div class="member-content">
              <h3>Carrick Mollenkamp</h3>
              <span>Web Developer</span>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Aspernatur necessitatibus ullam, culpa odio.
              </p>
            </div>
            <!-- /member name & designation -->
          </div>
        </div>
        <!-- end team member -->

        <!-- team member -->
        <div class="col-lg-4 col-md-6">
          <div class="team-member text-center">
            <div class="member-photo">
              <!-- member photo -->
              <img loading="lazy" class="img-fluid" src="{{ asset('frontend/images/team/member-3.jpg') }}"
                alt="Meghna" />
              <!-- /member photo -->
            </div>

            <!-- member name & designation -->
            <div class="member-content">
              <h3>David Gauthier</h3>
              <span>Head Of Management</span>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Aspernatur necessitatibus ullam, culpa odio.
              </p>
            </div>
            <!-- /member name & designation -->
          </div>
        </div>
        <!-- end team member -->
      </div>
      <!-- End row -->
    </div>
    <!-- End container -->
  </section>
  <!-- End section -->
  <!--

Start Call To Action
==================================== -->
  <section class="call-to-action section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-8 text-center">
          <h2>Let's Join Something Together</h2>
          <p>
            Proin gravida nibh vel velit auctor aliquet. Aenean sollicudin
            bibendum auctor, nisi elit consequat ipsum, nesagittis sem nid
            elit. Duis sed odio sitain elit.
          </p>
          <a href="contact.html" class="btn btn-main">Telegram Us</a>
        </div>
      </div>
      <!-- End row -->
    </div>
    <!-- End container -->
  </section>
  <!-- End section -->
  @endsection