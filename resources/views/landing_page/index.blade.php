@extends('layouts.frontend')
@section('title', 'Ana Online Landing Page')
@section('content')
<!--
  Start Preloader
  ==================================== -->
<div id="preloader">
  <div class="preloader">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
</div>
<!--
  End Preloader
  ==================================== -->
<div class="hero-slider">
  <div class="slider-item th-fullpage hero-area" style="background-image: url(frontend/images/slider/slider-1.jpg)">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1 data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".1">
            Crafting Digital <br />
            Experience
          </h1>
          <p data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".5">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod,
            <br />
            veritatis tempore nostrum id officia quaerat eum corrupti,
            <br />
            ipsa aliquam velit.
          </p>
          <a data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".8" class="btn btn-main"
            href="{{ url('/about') }}">About Us</a>
        </div>
      </div>
    </div>
  </div>
  <div class="slider-item th-fullpage hero-area" style="background-image: url(frontend/images/slider/slider-2.jpg)">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1 data-duration-in=".3" data-animation-in="fadeInDown" data-delay-in=".1">
            We Have Portfolio
          </h1>
          <p data-duration-in=".3" data-animation-in="fadeInDown" data-delay-in=".5">
            Create just what you need for your Perfect Website. Choose from
            a wide range <br />
            of Elements & simply put them on our Canvas.
          </p>
          <a data-duration-in=".3" data-animation-in="fadeInDown" data-delay-in=".8" class="btn btn-main"
            href="{{ url('/portfolio') }}">Portfolio Us</a>
        </div>
      </div>
    </div>
  </div>
</div>

<!--
Start About Section
==================================== -->
<section class="about-2 section" id="about">
  <div class="container">
    <div class="row justify-content-center">
      <!-- section title -->
      <div class="col-lg-6">
        <div class="title text-center">
          <h2>Features Ana-online</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quibusdam reprehenderit accusamus labore iusto, aut, eum itaque
            illo totam tempora eius.
          </p>
          <div class="border"></div>
        </div>
      </div>
      <!-- /section title -->
    </div>

    <div class="row">
      <div class="col-md-6 mb-4 mb-md-0 mr-5 text-center">
        <img loading="lazy" src="{{ asset('frontend/images/about/about-2.png') }}" class="img-fluid" alt="" />
      </div>
      <div class="col-md-5 ml-4">
        <ul class="checklist">
          <li>E-Ma'ruf (Caang)</li>
          <li>Registrasi Anggota</li>
          <li>Heregistrasi Angota</li>
          <li>TAFIQ 1</li>
          <li>TAFIQ 2</li>
          <li>TAFIQ 3</li>
          <li>Unduh Dokumen</li>
          <li>Helpdesk Anaonline</li>
        </ul>
        <a href="https://s.id/anaonline" class="btn btn-main mt-20">Selengkapnya</a>
      </div>
    </div>
    <!-- End row -->
  </div>
  <!-- End container -->
</section>
<!-- End section -->

<!--
Start Call To Action
==================================== -->
<section class="call-to-action section">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-6 col-lg-8 text-center">
        <h2>Let's Join Something Together</h2>
        <p>
          Proin gravida nibh vel velit auctor aliquet. Aenean sollicudin
          bibendum auctor, nisi elit consequat ipsum, nesagittis sem nid
          elit. Duis sed odio sitain elit.
        </p>
        <a href="contact.html" class="btn btn-main">Telegram Us</a>
      </div>
    </div>
    <!-- End row -->
  </div>
  <!-- End container -->
</section>
<!-- End section -->

<!--
Start About Section
==================================== -->
<section class="service-2 section">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <!-- section title -->
        <div class="title text-center">
          <h2>What we get</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem
            ipsum dolor sit amet, consectetur adipisicing elit. Voluptates,
            earum.
          </p>
          <div class="border"></div>
        </div>
        <!-- /section title -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="row text-center">
          <div class="col-md-6 col-sm-6">
            <div class="service-item">
              <i class="tf-ion-ios-briefcase-outline"></i>
              <h4>New Friends</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Fusce vitae risus nec dui venenatis.
              </p>
            </div>
          </div>
          <!-- END COL -->
          <div class="col-md-6 col-sm-6">
            <div class="service-item">
              <i class="tf-ion-ios-briefcase-outline"></i>
              <h4>Relation Between Volunter</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Fusce vitae risus nec dui venenatis.
              </p>
            </div>
          </div>
          <!-- END COL -->
          <div class="col-md-6 col-sm-6">
            <div class="service-item">
              <i class="tf-ion-ios-briefcase-outline"></i>
              <h4>Experience Skills</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Fusce vitae risus nec dui venenatis.
              </p>
            </div>
          </div>
          <!-- END COL -->
          <div class="col-md-6 col-sm-6">
            <div class="service-item">
              <i class="tf-ion-ios-briefcase-outline"></i>
              <h4>Compactness Team</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Fusce vitae risus nec dui venenatis.
              </p>
            </div>
          </div>
          <!-- END COL -->
        </div>
      </div>
    </div>
    <!-- End row -->
  </div>
  <!-- End container -->
</section>
<!-- End section -->

<!-- Start Testimonial
=========================================== -->
<section class="testimonial section" id="testimonial">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <!-- testimonial wrapper -->
        <div class="testimonial-slider">
          <!-- testimonial single -->
          <div class="item text-center">
            <i class="tf-ion-chatbubbles"></i>
            <!-- client info -->
            <div class="client-details">
              <p>Kopdar di bali lagi yuu gais..</p>
            </div>
            <!-- /client info -->
            <!-- client photo -->
            <div class="client-thumb">
              <img loading="lazy" src="{{ asset('frontend/images/team/member-1.jpg') }}" class="img-fluid" alt="" />
            </div>
            <div class="client-meta">
              <h3>William Martin</h3>
              <span>Senior Frontend Developer</span>
            </div>
            <!-- /client photo -->
          </div>
          <!-- /testimonial single -->

          <!-- testimonial single -->
          <div class="item text-center">
            <i class="tf-ion-chatbubbles"></i>
            <!-- client info -->
            <div class="client-details">
              <p>Mantapp</p>
            </div>
            <!-- /client info -->
            <!-- client photo -->
            <div class="client-thumb">
              <img loading="lazy" src="{{ asset('frontend/images/team/member-2.jpg') }}" class="img-fluid" alt="" />
            </div>
            <div class="client-meta">
              <h3>Emma Harrison</h3>
              <span>Fullstack Developer</span>
            </div>
            <!-- /client photo -->
          </div>
          <!-- /testimonial single -->

          <!-- testimonial single -->
          <div class="item text-center">
            <i class="tf-ion-chatbubbles"></i>
            <!-- client info -->
            <div class="client-details">
              <p>Senyumin Ajah</p>
            </div>
            <!-- /client info -->
            <!-- client photo -->
            <div class="client-thumb">
              <img loading="lazy" src="{{ asset('frontend/images/team/member-3.jpg') }}" class="img-fluid" alt="" />
            </div>
            <div class="client-meta">
              <h3>Alexander Lucas</h3>
              <span>DevOps | Security System</span>
            </div>
            <!-- /client photo -->
          </div>
          <!-- /testimonial single -->
        </div>
      </div>
      <!-- end col lg 12 -->
    </div>
    <!-- End row -->
  </div>
  <!-- End container -->
</section>
<!-- End Section -->

<!--
Start Blog Section
=========================================== -->
<section class="blog" id="blog">
  <div class="container">
    <div class="row justify-content-center">
      <!-- section title -->
      <div class="col-xl-6 col-lg-8">
        <div class="title text-center">
          <h2>Gallery <span class="color">Kopdar</span></h2>
          <div class="border"></div>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Ducimus facere accusamus, reprehenderit libero inventore nam.
          </p>
        </div>
      </div>
      <!-- /section title -->
    </div>

    <div class="row">
      <!-- single blog post -->
      <article class="col-lg-4 col-md-6">
        <div class="post-item">
          <div class="media-wrapper">
            <img loading="lazy" src="{{ asset('frontend/images/blog/post-1.jpg') }}" alt="amazing caves coverimage"
              class="img-fluid" />
          </div>

          <div class="content">
            <h3><a href="single-post.html">Reasons to Smile</a></h3>
            <p>
              Anim pariatur cliche reprehenderit, enim eiusmod high life
              accusamus terry richardson ad squid. 3 wolf moon officia aute,
              non skateboard dolor brunch.
            </p>
          </div>
        </div>
      </article>
      <!-- /single blog post -->

      <!-- single blog post -->
      <article class="col-lg-4 col-md-6">
        <div class="post-item">
          <div class="media-wrapper">
            <img loading="lazy" src="{{ asset('frontend/images/blog/post-2.jpg') }}" alt="amazing caves coverimage"
              class="img-fluid" />
          </div>

          <div class="content">
            <h3><a href="single-post.html">A Few Moments</a></h3>
            <p>
              Anim pariatur cliche reprehenderit, enim eiusmod high life
              accusamus terry richardson ad squid. 3 wolf moon officia aute,
              non skateboard dolor brunch.
            </p>
          </div>
        </div>
      </article>
      <!-- end single blog post -->

      <!-- single blog post -->
      <article class="col-lg-4 col-md-6">
        <div class="post-item">
          <div class="media-wrapper">
            <img loading="lazy" src="{{ asset('frontend/images/blog/post-3.jpg') }}" alt="amazing caves coverimage"
              class="img-fluid" />
          </div>

          <div class="content">
            <h3><a href="single-post.html">Hints for Life</a></h3>
            <p>
              Anim pariatur cliche reprehenderit, enim eiusmod high life
              accusamus terry richardson ad squid. 3 wolf moon officia aute,
              non skateboard dolor brunch.
            </p>
          </div>
        </div>
      </article>
      <!-- end single blog post -->
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>
<!-- end section -->
@endsection