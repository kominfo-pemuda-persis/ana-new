@extends('layouts.simple')

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{ asset('media/photos/photo36@2x.jpg') }}');">
        <div class="hero bg-white overflow-hidden">
            <div class="hero-inner">
                <div class="content content-full text-center">
                    <h1 class="display-4 font-w300 mb-3 invisible" data-toggle="appear" data-class="animated fadeInDown">
                        <img src="{{ asset("media/res/ANA ONLINE 1.png") }}" width="5%"/>
                        <span>
                            <img src="{{ asset("media/res/ANA ONLINE 2.png") }}" width="20%">
                        </span>
                    </h1>
                    <h2 class="h3 font-w400 text-muted mb-5 invisible" data-toggle="appear" data-class="animated fadeInDown" data-timeout="300">
                        Selamat datang di sistem Informasi Manajemen Anggota Pemuda Persis.
                    </h2>
                    <span class="m-2 d-inline-block invisible" data-toggle="appear" data-class="animated fadeInUp" data-timeout="600">
                        <a class="btn btn-success px-4 py-2" href="{{route('login')}}">
                            <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Masuk
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->
@endsection

@section('js_after')
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-V33JPYB1SF"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-V33JPYB1SF');
    </script>
@endsection

