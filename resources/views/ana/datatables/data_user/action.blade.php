<div class="d-flex">
    @if(Permission::checkPermission('users.update'))
        <a href="/data-user/{{ $id_login }}/edit" class="btn btn-sm btn-warning mr-1" title="Edit data">
            <i class="fas fa-pencil-alt"></i>
        </a>
    @endif

    @if(Permission::checkPermission('users.read'))
        <a href="/data-user/{{ $id_login }}" class="btn btn-sm btn-info" title="Detail data">
            <i class="fas fa-eye"></i>
        </a>
    @endif
</div>
