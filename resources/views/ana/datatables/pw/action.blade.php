<div class="btn-group float-right">
    @if(Permission::checkPermission('masterpw.update'))
        <button type="button" onclick="openModal('#modal-form', 'update', '{{ $kd_pw }}')"
                class="btn btn-warning btn-sm"><i class="si si-pencil"></i></button>
    @endif

    @if(Permission::checkPermission('masterpw.delete'))
        <button type="button" class="btn btn-danger btn-sm" onclick="remove('{{ $kd_pw }}')">
            <i class="si si-trash"></i>
        </button>
    @endif

</div>
