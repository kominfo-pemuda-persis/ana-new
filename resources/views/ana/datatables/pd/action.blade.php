<div class="btn-group float-right">
    @if(Permission::checkPermission('masterpd.update'))
        <button type="button" onclick="openModal('#modal-form', 'update', '{{ $kd_pd }}')"
                class="btn btn-warning btn-sm"><i class="si si-pencil"></i></button>
    @endif

    @if(Permission::checkPermission('masterpd.delete'))
        <button type="button" class="btn btn-danger btn-sm" onclick="remove('{{ $kd_pd }}')">
            <i class="si si-trash"></i>
        </button>
    @endif
</div>
