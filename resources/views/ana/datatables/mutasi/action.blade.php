<div class="btn-group float-right">
    @if(Permission::checkPermission('mutasi.update') && isset($anggota))
        <button type="button" onclick="openModal('#modal-form', 'update', '{{ $id_mutasi }}')"
                class="btn btn-warning btn-sm"><i class="si si-pencil"></i></button>
    @endif

    @if(Permission::checkPermission('mutasi.delete'))
        <button type="button" class="btn btn-danger btn-sm" onclick="remove('{{ $id_mutasi }}')">
            <i class="si si-trash"></i></button>
    @endif
</div>
