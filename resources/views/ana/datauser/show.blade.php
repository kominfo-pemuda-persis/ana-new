@extends('layouts.backend')

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Data User <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Edit</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('data-user.index') }}">Data User</a>
                    </li>
                    <li class="breadcrumb-item">Edit</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="content">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Form <small>Edit Data User</small></h3>
        </div>
        <div class="block-content block-content-full">
            <div class="row text-center">
                <div class="col">
                    @if($user->anggota->foto == "default.png")
                    <img width="150px" src="/images/anggota/default.png">
                    @else
                    <img src="{{ Storage::url($user->anggota->foto) }}" alt="foto" height="200">
                    @endif
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-3">
                    <strong>NPA</strong>
                </div>
                <div class="col">
                    {{ $user->npa }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Nama Lengkap</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->nama_lengkap }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Pimpinan Wilayah</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->pimpinanWilayah->nama_pw ?? '-' }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Pimpinan Daerah</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->pimpinanDaerah->nama_pd ?? '-' }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Pimpinan Cabang</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->pimpinanCabang->nama_pc ?? '-' }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Desa</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->village->nama }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Pimpinan Jamaah</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->pj }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Email</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->email }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Nomor Telpon</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->no_telpon }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-3">
                    <strong>Alamat</strong>
                </div>
                <div class="col">
                    {{ $user->anggota->alamat }}
                </div>
            </div>
            <div class="row mt-5">
                <div class="col">
                    <a href="{{ route('data-user.edit', ['id_login' => $user->id_login]) }}"
                       class="btn btn-primary form-control">Edit Data</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
