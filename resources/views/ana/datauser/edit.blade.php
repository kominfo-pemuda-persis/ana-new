@extends('layouts.backend')

@section('css_before')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          rel="stylesheet"/>
@endsection

@section('content')
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data User <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Edit</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ route('data-user.index') }}">Data User</a>
                        </li>
                        <li class="breadcrumb-item">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Form <small>Edit Data User</small></h3>
            </div>
            <div class="block-content block-content-full">
                @if (Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        Data berhasil disimpan
                    </div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        Data gagal disimpan
                    </div>
                @endif
                <form action="{{ route('data-user.update', ['id_login' => $user->id_login]) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="npa">NPA</label>
                        <input type="text" class="form-control" id="npa" value="{{ $user->npa }}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="nama-lengkap">Nama Lengkap</label>
                        <input type="text" class="form-control" name="nama_lengkap" id="nama-lengkap"
                               value="{{ $user->anggota->nama_lengkap }}" autocomplete="name" required>
                    </div>
                    <div class="form-group">
                        <label for="pw">Pimpinan Wilayah</label>
                        <select name="pw" id="pw" class="custom-select" required>
                            <option>Pilih Pimpinan Wilayah</option>
                            @foreach ($pw as $item)
                                <option
                                    value="{{ $item->kd_pw }}" {{ $item->kd_pw == $user->anggota->pw ? 'selected' : '' }}>{{ $item->nama_pw }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pd">Pimpinan Daerah</label>
                        <select name="pd" id="pd" class="custom-select" required>
                            <option>Pilih Pimpinan Daerah</option>
                            @foreach ($pd as $item)
                                <option
                                    value="{{ $item->kd_pd }}" {{ $item->kd_pd == $user->anggota->pd ? 'selected' : '' }}>{{ $item->nama_pd }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pc">Pimpinan Cabang</label>
                        <select name="pc" id="pc" class="custom-select" required>
                            <option>Pilih Pimpinan Cabang</option>
                            @foreach ($pc as $item)
                                <option
                                    value="{{ $item->kd_pc }}" {{ $item->kd_pc == $user->anggota->pc ? 'selected' : '' }}>{{ $item->nama_pc }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="desa">Desa</label>
                        <input type="text" class="form-control" name="desa" id="desa"
                               value="{{ $user->anggota->village->nama }}" required>
                    </div>
                    <div class="form-group">
                        <label for="pj">Pimpinan Jamaah</label>
                        <input type="text" class="form-control" name="pj" id="pj" value="{{ $user->anggota->pj }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea name="alamat" id="alamat" rows="3" class="form-control"
                                  required>{{ $user->anggota->alamat }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" id="email"
                               value="{{ $user->anggota->email }}" autocomplete="email" required>
                    </div>
                    <div class="form-group">
                        <label for="no-telpon">Nomor Telepon</label>
                        <input type="text" class="form-control" name="no_telpon" id="no-telpon"
                               value="{{ $user->anggota->no_telpon }}" autocomplete="tel" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password Baru</label>
                        <div class="input-group">
                            <input type="password" class="form-control" id="password" autocomplete="new-password"
                                   name="password" placeholder="Masukkan password baru">
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="button" id="show-hide-password">
                                    <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary form-control mt-4">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js_after')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $('#show-hide-password').click(function () {
            const passEl = $('#password');
            const passType = $(passEl).attr('type');

            if (passType === 'text') {
                $(passEl).attr('type', 'password');
                $('i', this).addClass('fa-eye-slash').removeClass('fa-eye');
            } else {
                $(passEl).attr('type', 'text');
                $('i', this).removeClass('fa-eye-slash').addClass('fa-eye');
            }
        });

        $('#pw, #pd, #pc').select2({
            theme: 'bootstrap'
        });

    </script>
@endsection
