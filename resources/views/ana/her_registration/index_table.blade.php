@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.3.3/css/fixedColumns.bootstrap4.min.css">
    <style>
        .DTFC_LeftBodyLiner {
            overflow-x: hidden
        }

        .img-caang {
            object-fit: scale-down;
            width: 100%;
        }

        table.ana-table {
            background: white !important;
        }

        .DTFC_LeftBodyLiner table.dataTable.ana-table tbody tr td {
            overflow-wrap: anywhere;
        }
    </style>
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/locales.js') }}"></script>
    <script src="{{ asset('/js/plugins/exceljs.min.js') }}"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>


    <!-- Page JS Code -->
    <script>
        moment.locale('id');
        let dataTableUrl = "{{ route('herregistrasi.datatables') }}";
        var columns = [
            {
                data: 'id',
                title: '#',
                searchable: false,
            },
            {
                data: 'name',
                title: 'Nama',
                order: 'asc',
            },
            {
                title: 'Email',
                data: 'email'
            },
            {
                title: 'PC',
                data: 'nama_pc',
                render: function (data, type, row) {
                    return row.anggota?.pimpinan_cabang?.nama_pc ?? undefined;
                },
                orderable: false,
                defaultContent: '-',
            },
            {
                title: 'PD',
                data: 'nama_pd',
                render: function (data, type, row) {
                    return row.anggota?.pimpinan_daerah?.nama_pd ?? undefined;
                },
                orderable: false,
                defaultContent: '-',
            },
            {
                title: 'PW',
                data: 'nama_pw',
                render: function (data, type, row) {
                    return row.anggota?.pimpinan_wilayah?.nama_pw ?? undefined;
                },
                orderable: false,
                defaultContent: '-',
            },
            {
                title: 'NPA',
                data: 'npa'
            },
            {
                title: 'No HP',
                data: 'phone_number'
            },
            {
                title: 'Tanggal Bayar',
                data: 'created_at',
                render: function (data) {
                    return moment(data).format("DD MMMM YYYY HH:mm")
                }
            },
            {
                title: 'Status',
                data: 'status',
                className: 'text-center',
                render: function (data) {
                    let textClass = "";
                    let textLabel = "";
                    if(data === "APPROVED"){
                        textClass = "badge-success";
                        textLabel = "Approved";
                    }else if(data === "REJECTED"){
                        textClass = "badge-danger"
                        textLabel = "Rejected";
                    }else{
                        textClass = "badge-primary"
                        textLabel = "Submitted";
                    }
                    let str =`<span class="badge badge-pill ${textClass}">${textLabel}</span>`;
                    return str;
                },
            },
            {
                title: 'Foto',
                data: 'link_foto',
                className: 'text-center',
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    let content = ``
                    if(row.file.length > 0){
                        let img =  ``
                        row.file.forEach(function (dataFile) {
                            if(dataFile.filetype === "image"){
                                img += `<div class="col-lg-6">
                            <img style="max-width: 760px;"src="${row.s3_path + dataFile.path_folder + '/' + dataFile.path}" />
                            </div>`
                            }
                        })
                        content = `<div class="row">${img}</div>`
                    }else{
                        content = `<img style="max-width: 760px;"src="${data}" />`
                    }

                    let str =
                        `<a href="" data-toggle="modal" data-target="#img_${row.id}">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--mdi" width="32" height="32" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M14 2l6 6v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h8m4 18V9h-5V4H6v16h12m-1-7v6H7l5-5l2 2m-4-5.5A1.5 1.5 0 0 1 8.5 12A1.5 1.5 0 0 1 7 10.5A1.5 1.5 0 0 1 8.5 9a1.5 1.5 0 0 1 1.5 1.5z" fill="currentColor"></path></svg>
                        </a>

                        <div class="modal fade" id="img_${row.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-lg modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Foto Bukti Transfer</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        ${content}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>`;
                    return str;
                },
            },
            {
                title: 'File Excel',
                data: 'link_excel',
                className: 'text-center',
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    let content = ``
                    if(row.file.length > 0){
                        let excel =  ``
                        row.file.forEach(function (dataFile) {
                            if(dataFile.filetype === "excel"){
                                excel += `
                            <div class="col-lg-12 col-md-6 col-sm-12 col-xl-12">
                                <div class="block block-rounded d-flex bg-body-light flex-column">
                                    <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                        <dl class="mb-0 text-left">
                                            <dt class="font-size-h2 font-w700">File</dt>
                                            <dd class="text-muted mb-0">${dataFile.path}</dd>
                                        </dl>
                                        <div class="item item-rounded bg-body">
                                            <i class="fa fa-file-excel font-size-h3 text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                        <a class="font-w500 d-flex align-items-center" href="${row.s3_path + dataFile.path_folder + '/' + dataFile.path}">
                                            download
                                            <i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>`
                            }
                        })
                        content = `<div class="row">${excel}</div>`
                    }else{
                        let excel = `<div class="col-lg-12 col-md-6 col-sm-12 col-xl-12">
                                <div class="block block-rounded d-flex bg-body-light flex-column">
                                    <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
                                        <dl class="mb-0 text-left">
                                            <dt class="font-size-h2 font-w700">File</dt>
                                            <dd class="text-muted mb-0">${row.path_excel}</dd>
                                        </dl>
                                        <div class="item item-rounded bg-body">
                                            <i class="fa fa-file-excel font-size-h3 text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                        <a class="font-w500 d-flex align-items-center" href="${data}">
                                            download
                                            <i class="fa fa-arrow-alt-circle-right ml-1 opacity-25 font-size-base"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>`;
                        content = `<div class="row">${excel}</div>`
                    }
                    let str =
                        `
                        <a href="" data-toggle="modal" data-target="#excel_${row.id}">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--fluent" width="32" height="32" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><g fill="none"><path d="M10 2v4.5A1.5 1.5 0 0 0 11.5 8H16v8.5a1.5 1.5 0 0 1-1.5 1.5H9.743A5.5 5.5 0 0 0 4 9.207V3.5A1.5 1.5 0 0 1 5.5 2H10zm1 .25V6.5a.5.5 0 0 0 .5.5h4.25L11 2.25zM5.5 19a4.5 4.5 0 1 0 0-9a4.5 4.5 0 0 0 0 9zm-2.354-4.146a.5.5 0 0 1 .708-.708L5 15.293V12.5a.5.5 0 0 1 1 0v2.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.351.146h-.006a.5.5 0 0 1-.348-.144l-.003-.003l-2-2z" fill="currentColor"></path></g></svg>
                        </a>
                        <div class="modal fade" id="excel_${row.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-lg modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">List File Excel</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        ${content}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                    return str;
                },
            },
            {
                title: 'Aksi',
                data: 'id',
                className: 'text-center',
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    let str =
                        `
            <a onclick="setApproved(this)" data-obj='${JSON.stringify(row)}' type="button" class="text-white btn-sm btn btn-success">
              Approve
            </a>
            <a onclick="setRejected(this)" data-obj='${JSON.stringify(row)}' type="button" class="text-white btn-sm btn btn-danger">
              Reject
            </a>`;
                    if(row.status !== 'APPROVED' && row.status !== 'REJECTED'){
                        return str;
                    }else{
                        return "-"
                    }

                },
            }
        ]

        $('#dataTable-her-registrasi').on('draw.dt', function () {
            setTimeout(() => {
                const hiddenThead = $('.DTFC_LeftWrapper .DTFC_LeftBodyWrapper .DTFC_LeftBodyLiner .ana-table thead th');
                const realThead = $('.DTFC_LeftWrapper .DTFC_LeftHeadWrapper .ana-table thead th').map((i, el) => {
                    $(hiddenThead[i]).css("min-width", $(el).css("width"));
                    $(hiddenThead[i]).css("max-width", $(el).css("width"));
                });

            }, 0);
        });
        let tableHerRegistrasi = $('#dataTable-her-registrasi').DataTable({
            columnDefs: [{
                width: 100,
                targets: 0
            }],
            pageLength: 10,
            lengthMenu: [
                [5, 10, 25, 50, 100, 250, 500, 1000],
                [5, 10, 25, 50, 100, 250, 500, 1000]
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: dataTableUrl,
            },
            // ajax: dataTableUrl,
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                // index number column
                var index = iDisplayIndex + this.fnSettings()._iDisplayStart + 1;
                $('td:eq(0)', nRow).html(index);
                return nRow;
            },
            columns: columns,
            autoWidth: false,
        });

        function setRejected(el) {
            swal({
                title: "Apakah anda yakin?",
                text: "Data akan direject dan tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Reject!",
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {
                    processDataReject(el)
                }
            })
        }

        function processDataReject(el) {
            let data = $(el).data().obj;

            swal({
                title: "Rejecting Data...",
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                },
            });
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                'url': `/data-her-registrasi/set-status/${data.id}/REJECTED`,
                'method':'POST',
                'dataType': 'json',
                processData: false,
                'contentType': 'application/json',
                'success': function (resp) {
                    swal.close();
                    swal({
                        title: "Berhasil!",
                        html: `Reject berhasil`,
                        type: "success",
                        confirmButtonColor: "#3498DB",
                        closeOnConfirm: true
                    }).then((result) => {
                        tableHerRegistrasi.draw();

                    });
                },
                error: function (resp) {
                    swal.close();
                    swal("Error!", "Data gagal diproses. Detail: " + resp.responseJSON.message, "error");
                }

            });

        }


        function processDataApproval(el) {
            let data = $(el).data().obj;
            let npaData = [];
            let emailData = [];

            swal({
                title: "Approving Data...",
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                },
            });
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                'url':'{{ route("herregistrasi.proses_approval") }}',
                'method':'POST',
                'dataType': 'json',
                processData: false,
                'contentType': 'application/json',
                'data':JSON.stringify({
                    "emailUploader": data.email,
                    "id": data.id
                }),
                'success': function (resp) {
                    swal.close();
                    swal({
                        title: "Berhasil!",
                        html: `Approval berhasil</br> ${resp.warning}`,
                        type: "success",
                        confirmButtonColor: "#3498DB",
                        closeOnConfirm: true
                    }).then((result) => {
                        tableHerRegistrasi.draw();
                    });
                },
                error: function (resp) {
                    swal.close();
                    swal("Error!", "Data gagal diproses. Detail: " + resp.responseJSON.message, "error");
                }

            });
        }

        function setApproved(el) {

            swal({
                title: "Apakah anda yakin?",
                text: "Data akan diapprove dan tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonText: "Approve!",
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {
                    processDataApproval(el)
                }
            })

        }

    </script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data Heregistrasi <small
                        class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Data Heregistrasi</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Table <small>Data Heregistrasi</small></h3>
            </div>
            <div class="block-content block-content-full">
                <div class="table-responsive">
                    <table id="dataTable-her-registrasi" class="ana-table table table-bordered">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
