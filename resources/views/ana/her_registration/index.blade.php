@extends('layouts.simple')
@section('js_after')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
            integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"
            integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        function addNewDropify(el){
            let numRand = Math.random().toString(16).substr(2, 8);
            let random = `excel-file-container-${numRand}`;
            let stringElement = `<div class="col-lg-6 col-sm-12 mx-auto text-left ${random}" style="cursor: pointer; height: 196px; margin-top: 10px">
                                        <input type="file" class="form-control dropify-excel" name="file-excel[]" id="file-excel"
                                               data-allowed-file-extensions="xls xlsx">
                                    </div>`
            $(stringElement).insertBefore($(el));
            let dropEl = $(`.${random} > .dropify-excel`).dropify({
                messages: {
                    'default': 'Drag dan drop dokumen excel kedalam kotak ini',
                    'replace': 'Drag dan drop atau click untuk mengganti dokumen excel',
                    'remove': 'Hapus',
                    'error': 'Terjadi kesalahan'
                },
                error: {
                    'fileExtension': 'Format file tidak diizinkan. File yang diizinkan hanya (@{{ value }}).'
                }
            });

            dropEl.on('dropify.afterClear', function(event, element){
                dropEl.data('dropify').destroy();
                $(`.${random}`).remove();
            });
        }

        function addNewDropifyPhoto(el){
            let numRand = Math.random().toString(16).substr(2, 8);
            let random = `photo-file-container-${numRand}`;
            let stringElement = `<div class="col-lg-6 col-sm-12 mx-auto text-left ${random}" style="cursor: pointer; height: 196px; margin-top: 10px">
                                        <input type="file" name="foto-upload[]" id="foto-upload"
                                               class="dropify-foto form-control" data-allowed-file-extensions="jpg jpeg png">
                                    </div>`
            $(stringElement).insertBefore($(el));
            let dropEl = $(`.${random} > .dropify-foto`).dropify({
                messages: {
                    'default': 'Drag dan drop bukti transfer kedalam kotak ini',
                    'replace': 'Drag dan drop atau click untuk mengganti bukti transfer',
                    'remove': 'Hapus',
                    'error': 'Terjadi kesalahan'
                },
                error: {
                    'fileExtension': 'Format file tidak diizinkan. File yang diizinkan hanya (@{{ value }}).'
                }
            });

            dropEl.on('dropify.afterClear', function(event, element){
                dropEl.data('dropify').destroy();
                $(`.${random}`).remove();
            });
        }

        $(document).ready(() => {
            let dropifyExcel = $('.excel-file-container-0 > .dropify-excel').dropify({
                messages: {
                    'default': 'Drag dan drop dokumen excel kedalam kotak ini',
                    'replace': 'Drag dan drop atau click untuk mengganti dokumen excel',
                    'remove': 'Hapus',
                    'error': 'Terjadi kesalahan'
                },
                error: {
                    'fileExtension': 'Format file tidak diizinkan. File yang diizinkan hanya (@{{ value }}).'
                }
            });

            let dropifyPhoto = $('.photo-file-container-0 > .dropify-foto').dropify({
                messages: {
                    'default': 'Drag dan drop bukti transfer kedalam kotak ini',
                    'replace': 'Drag dan drop atau click untuk mengganti bukti transfer',
                    'remove': 'Hapus',
                    'error': 'Terjadi kesalahan'
                },
                error: {
                    'fileExtension': 'Format file tidak diizinkan. File yang diizinkan hanya (@{{ value }}).'
                }
            });

            dropifyPhoto.on('dropify.afterClear', function(event, element){
                dropifyPhoto.data('dropify').destroy();
                $(`.photo-file-container-0`).remove();
            });

            $("#her-registration").validate({
                rules: {
                    name: "required",
                    npa: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    phone_number: {
                        required: true,
                        digits: true
                    },
                    "file-excel": "required",
                    "foto-upload": "required"
                },
                messages: {
                    name: "Silahkan isi nama anda",
                    npa: "Silahkan isi NPA anda",
                    email: {
                        required: "Silahkan isi email anda",
                        email: "Format email salah (contoh email admin@anaonline.id)"
                    },
                    phone_number: {
                        required: "Silahkan isi nomor HP anda",
                        digits: "Silahkan masukan hanya angka"
                    },
                    "file-excel": "Silahkan sertakan dokumen excel",
                    "foto-upload": "Silahkan sertakan bukti transfer"
                },
                errorClass: "is-invalid",
                submitHandler: function (form, event) {
                    $form = $(form);
                    event.preventDefault();

                    if($("input[name='file-excel[]']").prop("files").length <= 0){
                        swal.close();
                        swal("Upload ditolak", "Silahkan sertakan File Excel dengan benar", "error");
                        return;
                    }

                    if($("input[name='foto-upload[]']").prop("files").length <= 0){
                        swal.close();
                        swal("Upload ditolak", "Silahkan sertakan Bukti Transfer dengan benar", "error");
                        return;
                    }

                    swal({
                        title: 'Saving data..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    })

                    var button = $form.find('button[type="submit"]');
                    button.attr('disabled', 'disabled');
                    button.text('menyimpan..');


                    $.ajax({
                        url: $form.attr('action'),
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        processData: false,
                        contentType : false,
                        data: new FormData(form),
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        success: function (res) {
                            form.reset();
                            swal({
                                title: "Berhasil!",
                                text: res.message,
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                button.removeAttr('disabled');
                                button.html('<i class="far fa-save mr-1"></i> Simpan');
                                window.location = '/heregistrasi';
                            });
                        },

                        error: function (jqXHR, exception) {
                            swal.close();
                            let message = `Terjadi kendala teknis, Mohon kontak Bidang Administrasi PP. <br> info error: <b>${jqXHR.responseJSON.message}</b>`
                            swal("Upload ditolak", message, "error");
                            button.removeAttr('disabled');
                            button.html('<i class="far fa-save mr-1"></i> Simpan');
                        },
                    });
                },
                errorPlacement: function (error, element) {
                    let name = $(element).attr("name");
                    if (name === 'file-excel') {
                        console.log(element.parent().parent().find(".invalid-feedback-file.normal-form").append(error));
                    }else if (name === 'foto-upload') {
                        console.log(element.parent().parent().find(".invalid-feedback-file.normal-form").append(error));
                    } else {
                        element.parent().find(".invalid-feedback.normal-form").append(error);
                    }
                }
            });



        })
    </script>
@endsection

@section('css_after')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
          integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('content')
    <div class="bg-white overflow-scroll">
        <form id="her-registration" action="{{ route("post.heregistrasi") }}">
            @csrf
            <div>
                <div class="content content-full text-center">
                    <h1 class="display-4 font-w300 mb-3">
                        <img src="{{ asset("media/res/ANA ONLINE 1.png") }}" width="5%"/>
                        <span>
                            <img src="{{ asset("media/res/ANA ONLINE 2.png") }}" width="20%">
                        </span>
                    </h1>
                    <h2 class="h3 font-w400 text-muted mb-5">
                        Formulir Heregistrasi
                        <p style="font-size: 16px" class="text-danger"><b>(Ajuan Heregistrasi dilakukan secara
                                kolektif oleh Admin PC)<br>
                                <b>(Transfer dikirim ke rekening Bank Syariah Indonesia (BSI) 7213125022 atas nama PP PEMUDA PERSIS) </b></p>
                    </h2>
                    <div class="flex flex-row items-center">
                        <div class="col-xl-4 col-lg-6 col-sm-12 mx-auto text-left">
                            <div class="form-group">
                                <label for="name">Nama Lengkap</label>
                                <input class="form-control" type="text" id="name" name="name"
                                       required>
                                <div class="invalid-feedback normal-form">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input class="form-control" type="text" id="email" name="email"
                                       required>
                                <div class="invalid-feedback normal-form">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="npa">NPA</label>
                                <input class="form-control" type="text" id="npa" name="npa"
                                       required>
                                <div class="invalid-feedback normal-form">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone_number">Nomer HP</label>
                                <input class="form-control" type="text" id="phone_number" name="phone_number"
                                       required>
                                <div class="invalid-feedback normal-form">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="file-excel" class="control-label">Dokumen Excel</label>
                                <h2>
                                    <p style="font-size: 14px">(Silahkan Download template excel nya <a
                                            href="{{ env('S3_STATIC_FOLDER') }}PCNOMERPC_NAMAPIMPINANCABANG.xlsx">di
                                            sini</a>)</p>
                                </h2>
                                <div style="width: 100%;margin-top: 0.5rem;font-size: 0.875rem;color: #d26a5c;"
                                     class="invalid-feedback-file normal-form">
                                </div>
                                <div class="row text-left excel-input-container">
                                    <div class="col-lg-12 col-sm-12 mx-auto text-left excel-file-container-0" style="cursor: pointer; height: 196px; margin-top: 10px">
                                        <input type="file" class="form-control dropify-excel" name="file-excel[]" id="file-excel"
                                               data-allowed-file-extensions="xls xlsx">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="foto-transfer" class="control-label">Foto Bukti
                                    Transfer</label>
                                <p style="font-size: 14px">(pastikan foto jelas dan tidak blur)</p>
                                <div style="width: 100%;margin-top: 0.5rem;font-size: 0.875rem;color: #d26a5c;"
                                     class="invalid-feedback-file normal-form">
                                </div>

                                <div class="row text-left photo-input-container">
                                    <div class="col-lg-6 col-sm-12 mx-auto text-left photo-file-container-0" style="cursor: pointer; height: 196px; margin-top: 10px">
                                        <input type="file" name="foto-upload[]" id="foto-upload"
                                               class="dropify-foto form-control" data-allowed-file-extensions="jpg jpeg png">
                                    </div>
                                    <div onclick="addNewDropifyPhoto(this)" class="h-full col-lg-6 col-sm-12" style="cursor: pointer; height: 196px; margin-top: 10px">
                                        <div style="position: relative; border: 2px dashed #E5E5E5; height: 100%">
                                            <div class="text-center" style="position: absolute; top: 40%; width: 100%">
                                                <i class="fa fa-fw fa-plus"></i>
                                                <p style="font-size: 10px; width: 100%; text-align: center">Klik untuk tambah photo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="m-2 d-inline-block">
                        <button class="btn btn-success px-4 py-2" type="submit">
                            <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Submit
                        </button>
                    </span>
                </div>
        </form>
    </div>
@endsection
