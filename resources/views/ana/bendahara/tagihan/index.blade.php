@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection



@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data Tagihan
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Data Tagihan</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Table <small>Data Tagihan</small></h3>
                @permission('bendahara.create')
                @if(in_array($data['superadmin'], $data['role']))
                <button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top"
                        title="Tambah Tagihan" onclick="openModal('#modal-form','add','/bendahara/store-tagihan')">
                    <span class="si si-plus"></span> Tambah</button>
                @endif
                @endpermission
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="js-dataTable-tagihan">
                    <thead>
                        <tr>
                            <th style="width: 5%;">No</th>
                            <th style="width: 20%;">Nama Tagihan</th>
                            <th style="width: 20%;">Nominal</th>
                            <th style="width: 20%;">Status</th>
                            @if(in_array($data['superadmin'], $data['role']))
                            <th style="width: 80px;">Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tagihan as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>Rp. {{ number_format($item->jumlah) }}</td>
                            <td>{{ $item->status == 1 ? 'Active' : 'Inactive' }}</td>
                            @if(in_array($data['superadmin'], $data['role']) || in_array($data['admin_pp'], $data['role']) )
                            <td>
                                <button type="button" class="btn btn-warning" onclick="editForm({{ $item->id }})">Edit</button>
                                <button type="button" class="btn btn-danger" onclick="deleteData({{ $item->id }})">Delete</button>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
    @includeIf('ana.bendahara.tagihan.form')
@endsection


@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/moment.min.js') }}"></script>
    <script>
        function openModal(target, type, url) {
            $form = $('#btn-form');
            var button = $form.find('button[type="submit"]');
                        button.attr('disabled', 'disabled');
                        button.text('menyimpan..');

            if(type == 'add') {
                $('#modal-form [name=nama').val('');
                $('#modal-form [name=jumlah').val('');
                $('#modal-form [name=status').val('');
                $('#modal-form form')[0].reset();
                $('#modal-form form').attr('action', url);
                $('#modal-form [name=_method]').val('post');
                $(target).modal('show');
                $('.modal-title').text('Tambah Tagihan')
                $('body').on('click', '#btn-form', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $.post($('#modal-form form').attr('action'), $('#modal-form form').serialize())
                    .done((res) => {
                        $('#modal-form').modal('hide');
                        swal({
                            title: "Berhasil!",
                            text: res.message,
                            type: "success",
                            confirmButtonColor: "#3498DB",
                            closeOnConfirm: true
                        }).then((result) => {
                            button.removeAttr('disabled');
                            button.html('<i class="far fa-save mr-1"></i> Simpan');
                            window.location = '/bendahara/buat-tagihan';
                        });
                    })
                    .fail((errors) => {
                        alert('Tidak dapat menyimpan data');
                        return;
                    });
                })
            }    
        }

        function editForm(id) {
            let url = "/bendahara/update-tagihan/"+id;
            $('#modal-form').modal('show');
            $('#modal-form .modal-title').text('Edit Tagihan');
            $('#modal-form form')[0].reset();
            $('#modal-form form').attr('action', url);
            $('#modal-form [name=_method]').val('put');
            $.get(url)
                .done((response) => {
                    $('#modal-form [name=nama').val(response.nama);
                    $('#modal-form [name=jumlah').val(response.jumlah);
                    $('#modal-form [name=status').val(response.status);
                    swal({
                        title: "Berhasil! update",
                        text: res.message,
                        type: "success",
                        confirmButtonColor: "#3498DB",
                        closeOnConfirm: true
                    }).then((result) => {
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                        window.location = '/bendahara/buat-tagihan';
                    });
                })
                .fail((errors) => {
                    alert('Tidak dapat menampilkan data');
                    return;
                });
        }

        function deleteData(id) {
            let url = "/bendahara/delete-tagihan/"+id;
            swal({
                    title: 'Delete Tagihan',
                    text: "Apakah Anda Yakin Ingin Menghapus tagihan ini?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "DELETE",
                            headers: {
                                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                            },
                            url: url,
                            success:function(res) {
                                window.location = '/bendahara/buat-tagihan';
                            }
                        });
                    }
                });
        }

        var jumlah = document.getElementById("jumlah");
            jumlah.addEventListener("keyup", function(e) {
                jumlah.value = convertRupiah(this.value, "Rp. ");
            });
            jumlah.addEventListener('keydown', function(event) {
                return isNumberKey(event);
            });

            function convertRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, "").toString(),
                split  = number_string.split(","),
                sisa   = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);
            
                if (ribuan) {
                    separator = sisa ? "." : "";
                    rupiah += separator + ribuan.join(".");
                }
            
                rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
                return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
            }
            
            function isNumberKey(evt) {
                key = evt.which || evt.keyCode;
                if ( 	key != 188 // Comma
                    && key != 8 // Backspace
                    && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
                    && (key < 48 || key > 57) // Non digit
                    ) 
                {
                    evt.preventDefault();
                    return;
                }
            }

            $('#js-dataTable-tagihan').DataTable();
    </script>
@endsection