@extends('layouts.backend')

@section('css_before')
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
{{--
<link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
    rel="stylesheet" />@endsection

@section('content')
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Data Tagihan
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Data Tagihan</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="/dashboard">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Table <small>Data Transaksi</small></h3>
        </div>
        <div class="block-content block-content-full">
            @if($errors->any())
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <p>{{ $error }}</p>
            </div>
            @endforeach
            @endif
            @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{!! \Session::get('success') !!}</p>
            </div>
            @endif
            @if (\Session::has('danger'))
            <div class="alert alert-danger">
                <p>{!! \Session::get('danger') !!}</p>
            </div>
            @endif
            <div class="row">
                <div class="col-6">
                    <form action="{{ route('transaksi.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label class="form-label">Anggota</label>
                            <select id="anggota" class="form-control" name="npa" required>
                                <option value="#">[-- Pilih Anggota --]</option>
                                @foreach($currentAnggota as $item)
                                <option value="{{ $item->npa }}">
                                    {{ $item->npa }} - {{ $item->nama_lengkap }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="form-tagihan">
                            <label class="form-label">Bayar Tagihan</label>
                            <select id="tagihan" class="form-control" name="tagihan_id" required>
                                @foreach($tagihan as $item)
                                <option value="{{ $item->id }}">
                                    {{ $item->nama }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="" id="form-total">
                            <label class="form-label">Nominal</label>
                            <input type="text" name="pembayaran" class="form-control" id="jumlah_bayar"
                                placeholder="Rp. -">
                        </div>
                        <div class="form-group" style="" id="form-total">
                            <label class="form-label">Bulan</label>
                            <select name="bulan_id" id="" class="form-control">
                                <option disabled selected>-----Pilih Bulan-------</option>
                                @foreach ($bulanAll as $item)
                                <option value={{ $item->id }}> {{ $item->nama_bulan }}</option>
                                @endforeach
                            </select>
                        </div>

                        {{-- <div class="form-group mb-2" style="" id="form-keterangan">
                            <label class="form-label">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" rows="3" class="form-control"
                                value="-"></textarea>
                        </div> --}}
                        <div class="d-flex">
                            <button class="btn btn-primary mr-auto" id="btn-simpan">Simpan</button>
                        </div>
                    </form>
                </div>
                <div class="col-6">
                    <div class=" mb-5">
                        <h6>Daftar Tagihan</h6>
                        @foreach ($tagihan as $item)
                        <span class="badge rounded-pill bg-primary text-white">{{ $item->nama }} - {{ "Rp. ".
                            number_format($item->jumlah) }}</span>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <div class="card">
                        <hr>
                        <div class="card-header">
                            <div class="card-options d-flex justify-content-between">
                                <h5 class="card-title">Histori Transaksi</h5>
                                <div>
                                    <a href="#" class="btn btn-outline-primary btn-sm ml-2" download="true">Export</a>
                                    <a href="#!cetak" class="btn btn-primary btn-sm ml-2"
                                        id="mass-cetak">Setor</a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table card-table table-hover table-vcenter text-wrap" id="js-dataTable-transaksi">
                                <thead>
                                    <tr>
                                        <th class="w-1">No.</th>
                                        <th>Tanggal</th>
                                        <th>Bulan</th>
                                        <th>NPA</th>
                                        <th>Nama Lengkap</th>
                                        <th>PW</th>
                                        <th>PD</th>
                                        <th>PC</th>
                                        <th>Tagihan</th>
                                        <th>Pembayaran</th>
                                        <th>Sisa</th>
                                        <th>Dibayarkan</th>
                                        <th>Setor/Export</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1 ?>
                                    @foreach ($tf as $index => $item)
                                    <tr>
                                        <td><span class="text-muted">{{ $no++ }}</span></td>
                                        <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                                        <td>{{ $item->nama_bulan }}</td>
                                        <td>{{ $item->npa }}</td>
                                        <td>{{ $item->nama_lengkap }}</td>
                                        <td>{{ $item->nama_pc }}</td>
                                        <td>{{ $item->nama_pd }}</td>
                                        <td>{{ $item->nama_pw }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>
                                            <span class="badge rounded-pill bg-danger text-white">{{ ($item->jumlah -
                                                $item->paid) > 0 ? "Rp. ". number_format($item->jumlah - $item->paid) :
                                                'lunas' }}</span>
                                        </td>
                                        <td>
                                            <span class="badge rounded-pill bg-success text-white">{{ "Rp. ".
                                                number_format($item->pay, 0, ',', '.') }}</span>
                                        </td>
                                        <td>
                                            <span class="badge rounded-pill bg-success text-white">{{ "Rp. ".
                                                number_format($item->paid, 0, ',', '.') }}</span>
                                        </td>
                                        {{-- <td style="max-width:150px;">{{ $item->keterangan }}</td> --}}
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input tandai"
                                                    name="setor" data-id="{{ $item->npa }}">
                                                <span class="custom-control-label">Setor</span>
                                            </label>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->

<!-- END Fade In Block Modal -->
@endsection
@section('js_after')
<!-- Page JS Plugins -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.0/js/toastr.js"></script>
<script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('/js/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('/js/plugins/moment/locales.js') }}"></script>

<script>
    $('#anggota, #tagihan').select2({
            theme: 'bootstrap',
    });

    var jumlah = document.getElementById("jumlah_bayar");
    jumlah.addEventListener("keyup", function(e) {
        jumlah.value = convertRupiah(this.value, "Rp. ");
    });
    jumlah.addEventListener('keydown', function(event) {
        return isNumberKey(event);
    });

    function convertRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split  = number_string.split(","),
        sisa   = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    
        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }
    
        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
    }
    
    function isNumberKey(evt) {
        key = evt.which || evt.keyCode;
        if ( 	key != 188 // Comma
            && key != 8 // Backspace
            && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
            && (key < 48 || key > 57) // Non digit
            ) 
        {
            evt.preventDefault();
            return;
        }
    }

    $('#js-dataTable-transaksi').DataTable();

    $('body').on('click', '#mass-cetak', function() {
        if(confirm('Setelah Anda setor harap konfirmasi Adm. PP Pemuda Persis')) {
            let ids = new Array();
            $('.tandai').each(function() {
                let id = $(this).data('id');
                if (this.checked) {
                    ids.push(id);
                }
            });
            $.ajax({
                type: 'POST',
                url: '/bendahara/transaksi-tagihan/setor',
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                data: {
                    npa : ids
                },
                dataType: 'json',
                success:function (res) {
                    if(res.code == 200) {
                        swal("Alert!", res.message, "success").then((result) => {
                            window.location.href = res.url;
                        });
                    }else{
                        swal("Alert!", res.message, "danger").then((result) => {
                            window.location.href = res.url;
                        });
                    }
                }
            });
        }
    })
</script>


@endsection