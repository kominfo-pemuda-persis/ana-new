@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />@endsection
    
@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    {{-- <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script> --}}
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.0/js/toastr.js"></script>
    <script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="{{ asset('/js/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/locales.js') }}"></script>

    <script>
        $('#anggota, #tagihan').select2({
            theme: 'bootstrap',
        });

        $('#js-dataTable-pc').DataTable();
    </script>
    


@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data Log Transaksi
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Data Log Transaksi</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Table <small>Data Log Transaksi</small></h3>
                <a href="{{ route('transaksi.list') }}" class="btn btn-primary btn-sm">List Konfirmasi Tagihan</a>
            </div>
            <div class="block-content block-content-full">
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{!! \Session::get('success') !!}</p>
                </div>
                @endif
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter" id="js-dataTable-pc">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">No</th>
                            <th style="width: 15%;">Nama PC</th>
                            <th style="width: 15%;">Tagihan</th>
                            <th style="width: 13%;">Sudah Setor</th>
                            <th style="width: 13%;">Belum Setor</th>
                            <th style="width: 13%;">Total Setor</th>
                            <th style="width: 13%;">Tahun Setor</th>
                            <th style="width: 13%;">Download</th>
                            @if(in_array($data['superadmin'], $data['role']) || in_array($data['admin_pp'], $data['role']) )
                            <th>#</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($log as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->nama_pc }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->jml_masuk }}</td>
                            <td>{{ $item->blm_setor }}</td>
                            <td>Rp. {{ number_format($item->total_setor) }}</td>
                            <td>{{ $item->tahun }}</td>
                            <td>
                                <a href="">Export Excel</a> |
                                <a href="">Export PDF</a>
                            </td>
                            @if(in_array($data['superadmin'], $data['role']) || in_array($data['admin_pp'], $data['role']) )
                            <td>                        
                                <a href="/bendahara/log-transaksi-tagihan/tarik-saldo/{{ $item->id }}" onclick="return confirm('Apakah Anda ingin tarik saldo ini?')" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top"
                                title="Penarikan Saldo">Penarikan Saldo</a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->

<!-- END Fade In Block Modal -->
@endsection