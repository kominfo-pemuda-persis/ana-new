@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/pg_anggota_profile.js') }}"></script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 font-w300 my-2">
                    Monografi PD
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Monografi PD</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h2 class="block-title text-center">Detail Monografi PD<small></small></h2>
            </div>
            <div class="block-content block-content-full">
                <div class="text-center">
                    @if(!$monografiPD->foto || $monografiPD->foto=="default.png" || $monografiPD->foto=="")
                    <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ asset('media/photos/desa.png') }}" >
                    @else
                    <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ Storage::disk('s3')->url('images/monografi/pd/'.$monografiPD->foto) }}" >
                    @endif
                </div>
                <hr>
                <section id="data-anggota">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Monografi PD<small></small></h3></td>
                                </tr>
                                <tr>
                                    <td width="15%">PD</td>
                                    <td width="1%">:</td>
                                    <td>{{ $monografiPD->kd_pd }} {{ $monografiPD->nama_pd }}</td>
                                </tr>
                                <tr>
                                    <td>PW</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->pw->kd_pw }} {{ $monografiPD->pw->nama_pw }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->getRelationValue('provinsi')->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Kabupaten/Kota</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->getRelationValue('kabupaten')->nama }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Geografis<small></small></h3></td>
                                </tr>
                                <tr>
                                    <td>Latitude / Garis Lintang</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->latitude }}</td>
                                </tr>
                                <tr>
                                    <td>Longitude / Garis Bujur</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->longitude }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->email }}</td>
                                </tr>
                                <tr>
                                    <td>Nomor Telepon</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->no_kontak }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Lengkap</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->alamat_utama }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Alternatif</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->alamat_alternatif }}</td>
                                </tr>
                                <tr>
                                    <td>Luas Wilayah Kerja</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->luas }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Utara</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->bw_utara }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Selatan</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->bw_selatan }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Timur</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->bw_timur }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Barat</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->bw_barat }}</td>
                                </tr>
                                <tr>
                                    <td>Jarak dari ibukota negara</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->jarak_dari_ibukota_negara }}</td>
                                </tr>
                                <tr>
                                    <td>Jarak dari ibukota provinsi</td>
                                    <td>:</td>
                                    <td>{{  $monografiPD->jarak_dari_ibukota_provinsi  }}</td>
                                </tr>
                                <tr>
                                    <td>Jarak dari ibukota kabupaten / kota</td>
                                    <td>:</td>
                                    <td>{{ $monografiPD->jarak_dari_ibukota_kabupaten }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-tasykil">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td><h3 class="block-title">Data Pimpinan / Tasykil</h3></td>
                                </tr>
                                <tr>
                                    <td>Ketua</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->ketua }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Ketua</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_ketua }}</td>
                                </tr>
                                <tr>
                                    <td>Sekretaris</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->sekretaris }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Sekretaris</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_sekretaris }}</td>
                                </tr>
                                <tr>
                                    <td>Bendahara</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bendahara }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bendahara</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bendahara }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Jam'iyyah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_jamiyyah }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Jam'iyyah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_jamiyyah }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Kaderisasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_kaderisasi }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Kaderisasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_kaderisasi }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Administrasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_administrasi }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Administrasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_administrasi }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Pendidikan</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_pendidikan }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Pendidikan</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_pendidikan }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Da’wah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_dakwah }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Da’wah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_dakwah }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Kominfo</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_humas_publikasi }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Kominfo</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_humas_publikasi }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang H.A.L</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_hal }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang H.A.L</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_hal }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Olahraga & Seni</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_or_seni }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Olahraga & Seni</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_or_seni }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Sosial</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_sosial }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Sosial</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_sosial }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Ekonomi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->bid_ekonomi }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Ekonomi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->wkl_bid_ekonomi }}</td>
                                </tr>
                                <tr>
                                    <td>Penasehat 1</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->penasehat1 }}</td>
                                </tr>
                                <tr>
                                    <td>Penasehat 2</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->penasehat2 }}</td>
                                </tr>
                                <tr>
                                    <td>Penasehat 3</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->penasehat3 }}</td>
                                </tr>
                                <tr>
                                    <td>Penasehat 4</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->penasehat4 }}</td>
                                </tr>
                                <tr>
                                    <td>Pembantu Umum 1</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->pembantu_umum2 }}</td>
                                </tr>
                                <tr>
                                    <td>Pembantu Umum 2</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->pembantu_umum2 }}</td>
                                </tr>
                                <tr>
                                    <td>Pembantu Umum 3</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->pembantu_umum3 }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-pendidikan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Waktu Ngantor<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Setiap Hari</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->hari }}</td>
                                </tr>
                                <tr>
                                    <td>Pukul</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->pukul }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Musda terakhir Masehi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->musda_terakhir_m }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Musda terakhir Hijriah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->musda_terakhir_h }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-keterampilan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Keanggotaan<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Anggota Biasa</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->anggota_biasa }}</td>
                                </tr>
                                <tr>
                                    <td>Anggota Tersiar</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->anggota_tersiar }}</td>
                                </tr>
                                <tr>
                                    <td>Anggota Istimewa</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPD->anggota_istimewa }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
@endsection
