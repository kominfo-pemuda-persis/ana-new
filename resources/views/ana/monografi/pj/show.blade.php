@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Page JS Code -->
{{--    <script src="{{ asset('js/pages/pg_anggota_profile.js') }}"></script>--}}
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 font-w300 my-2">
                    Monografi PJ
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Monografi PJ</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h2 class="block-title text-center">Detail Monografi PJ<small></small></h2>
            </div>
            <div class="block-content block-content-full">
                <div class="text-center">
                    @if(!$monografiPJ->photo || $monografiPJ->photo=="default.png" || $monografiPJ->photo=="")
                    <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ asset('media/photos/desa.png') }}" >
                    @else
                    <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ Storage::disk('s3')->url('images/monografi/pj/'.$monografiPJ->photo) }}" >
                    @endif
                </div>
                <hr>
                <section id="data-anggota">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Monografi PJ<small></small></h3></td>
                                </tr>
                                <tr>
                                    <td width="30%">PJ</td>
                                    <td width="1%">:</td>
                                    <td>{{ $monografiPJ->kode_pj }} {{ $monografiPJ->nama_pj }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">PC</td>
                                    <td width="1%">:</td>
                                    <td>{{ $monografiPJ->pc->kd_pc }} {{ $monografiPJ->pc->nama_pc }}</td>
                                </tr>
                                <tr>
                                    <td width="15%">PD</td>
                                    <td width="1%">:</td>
                                    <td>{{ $monografiPJ->pd->kd_pd }} {{ $monografiPJ->pd->nama_pd }}</td>
                                </tr>
                                <tr>
                                    <td>PW</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->pw->kd_pw }} {{ $monografiPJ->pw->nama_pw }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->getRelationValue('provinsi')->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Kabupaten/Kota</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->getRelationValue('kabupaten')->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->getRelationValue('kecamatan')->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Desa</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->getRelationValue('desa')->nama }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Geografis<small></small></h3></td>
                                </tr>
                                <tr>
                                    <td>Latitude / Garis Lintang</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->latitude }}</td>
                                </tr>
                                <tr>
                                    <td>Longitude / Garis Bujur</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->longitude }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->email }}</td>
                                </tr>
                                <tr>
                                    <td>Nomor Telepon</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->no_telpon }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Lengkap</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->alamat }}</td>
                                </tr>
                                <tr>
                                    <td>Luas Wilayah Kerja</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->luas }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Utara</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->bw_utara }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Selatan</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->bw_selatan }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Timur</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->bw_timur }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Barat</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->bw_barat }}</td>
                                </tr>
                                <tr>
                                    <td>Jarak dari ibukota provinsi</td>
                                    <td>:</td>
                                    <td>{{  $monografiPJ->jarak_provinsi  }}</td>
                                </tr>
                                <tr>
                                    <td>Jarak dari ibukota kabupaten / kota</td>
                                    <td>:</td>
                                    <td>{{ $monografiPJ->jarak_kabupaten }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-tasykil">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td><h3 class="block-title">Data Pimpinan / Tasykil</h3></td>
                                </tr>
                                <tr>
                                    <td>Ketua</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->ketua }}</td>
                                </tr>
                                <tr>
                                    <td>Sekretaris</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->sekretaris }}</td>
                                </tr>
                                <tr>
                                    <td>Bendahara</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->bendahara }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-pendidikan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Waktu Ngantor<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Setiap Hari</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->hari_ngantor }}</td>
                                </tr>
                                <tr>
                                    <td>Pukul</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->waktu_ngantor }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Musjam terakhir Masehi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->musjam_terakhir_masehi }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Musjam terakhir Hijriah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->musjam_terakhir_hijriyyah }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-keterampilan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Keanggotaan<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Anggota Biasa</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->anggota_biasa }}</td>
                                </tr>
                                <tr>
                                    <td>Anggota Istimewa</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->anggota_luar_biasa }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-training">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Mantan Anggota<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Tidak Her-Registrasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->tdk_herReg }}</td>
                                </tr>
                                <tr>
                                    <td>Mutasi Ke Persis</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->mutasi_persis }}</td>
                                </tr>
                                <tr>
                                    <td>Mutasi Tempat</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->mutasi_tempat }}</td>
                                </tr>
                                <tr>
                                    <td>Mengundurkan diri</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->mengundurkan_diri }}</td>
                                </tr>
                                <tr>
                                    <td>Meninggal dunia</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->meninggal_dunia }}</td>
                                </tr>
                                <tr>
                                    <td>Calon Anggota / Simpatisan</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPJ->calon_anggota }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
@endsection
