@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/hijri-datepicker/css/bootstrap-datetimepicker.css') }}">
    <style>
        ul.monografi-pc-tab li a {
            height: 100%;
        }
    </style>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Tambah Data Jam'iyyah <small
                        class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Monografi</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Tambah Data Jam'iyyah</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('pj.index',[],false)}}">Data Monografi PJ</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Form <small>Edit Data Monografi Jam'iyyah tingkat PJ</small></h3>
            </div>

            <div class="text-center">
                <h2>DATA MONOGRAFI JAM’IYYAH</h2>
                <h3>PJ PEMUDA PERSATUAN ISLAM</h3>
            </div>

            <div class="monografi-pj-wizard block-content block-content-full">

                <ul class="nav monografi-pj-tab nav-tabs nav-tabs-block nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#step1" id="step1-tab" data-toggle="tab">Data Pimpinan
                            Cabang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step2" id="step2-tab" data-toggle="tab">Data Geografis</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step3" id="step3-tab" data-toggle="tab">Data Pimpinan / Tasykil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step4" id="step4-tab" data-toggle="tab">Waktu Ngantor</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step5" id="step5-tab" data-toggle="tab">Data Keanggotaan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step6" id="step6-tab" data-toggle="tab">Data Mantan Anggota</a>
                    </li>
                </ul>

                <form id="monografi-pj-form">
                    @csrf
                    <div class="block-content block-content-full tab-content px-md-5" style="min-height: 300px;">
                        <div class="tab-pane active" id="step1" role="tabpanel">

                            <div class="row">
                                <div class="col-lg-12">
                                    @if($errors->any())
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>

                                            @foreach($errors->all() as $error)
                                                {{ $error }}<br/>
                                            @endforeach
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-hf-email">Nomor PW</label>
                                        <div class="col-sm-2">
                                            <input type="text" value="{{ $monografiPJ->pw->kd_pw }}"
                                                   class="form-control" id="noPW" name="kd_pw"
                                                   placeholder="Nomor PW">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="{{ $monografiPJ->pw->nama_pw }}"
                                                   class="form-control" id="namaPW"
                                                   placeholder="Nama PW" readonly required>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-success" data-toggle="modal"
                                                    data-target="#myModalPW"><i class="fa fa-search mr-1"> Cari</i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-hf-email">Nomor PD</label>
                                        <div class="col-sm-2">
                                            <input type="text" value="{{ $monografiPJ->pd->kd_pd }}"
                                                   class="form-control" id="noPD" name="kd_pd"
                                                   placeholder="Nomor PD">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="{{ $monografiPJ->pd->nama_pd }}"
                                                   class="form-control" id="namaPD" name="namaPD"
                                                   placeholder="Nama PD" readonly required>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-info" data-toggle="modal"
                                                    data-target="#myModalPD"><i class="fa fa-search mr-1"> Cari</i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-hf-email">Nomor PC</label>

                                        <div class="col-sm-2">
                                            <input type="text" value="{{ $monografiPJ->pc->kd_pc }}"
                                                   class="form-control" id="noPC" name="kd_pc"
                                                   placeholder="Nomor PC">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="{{ $monografiPJ->pc->nama_pc }}"
                                                   class="form-control" id="namaPC" name="namaPC"
                                                   placeholder="Nama PC" readonly required>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-warning" data-toggle="modal"
                                                    data-target="#myModalPC"><i class="fa fa-search mr-1"> Cari</i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-hf-email">Nomor PJ</label>
                                        <div class="col-sm-2">
                                            <input type="text" value="{{ $monografiPJ->kode_pj }}" class="form-control"
                                                   id="noPJ" name="kode_pj" required
                                                   placeholder="Nomor PJ">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="{{ $monografiPJ->nama_pj }}" class="form-control"
                                                   id="namaPJ" required
                                                   placeholder="Nama PC" name="nama_pj">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="pw" class="col-sm-2 control-label">Provinsi</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="provinsi" id="provinces" required>
                                                <option value="" disable="true" selected="true">Pilih Propinsi
                                                </option>
                                                @foreach ($provinces as $key => $value)
                                                    @if($monografiPJ->provinsi == $value->id)
                                                        <option value="{{$value->id}}"
                                                                selected>{{ $value->nama }}</option>
                                                    @else
                                                        <option value="{{$value->id}}">{{ $value->nama }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pd" class="col-sm-2 control-label">Kabupaten/Kota</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="kabupaten" id="regencies" required>
                                                <option value="">Pilih Kabupaten/Kota</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pc" class="col-sm-2 control-label">Kecamatan</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="kecamatan" id="districts" required>
                                                <option value="" disable="true" selected="true">Pilih Kecamatan
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pc" class="col-sm-2 control-label">Desa</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="desa" id="villages" required>
                                                <option value="" disable="true" selected="true">Pilih Desa
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="start_periode" class="col-sm-2 control-label">Periode Mulai</label>
                                        <div class="col-sm-3">
                                            <div class="date" data-provide="datepicker"
                                                 data-date-format="dd-mm-yyyy">

                                                <input type="text" name="start_periode" required
                                                       class="form-control start-periode"
                                                       placeholder="0000-00-00">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <label for="end_periode" class="col-sm-3 control-label">Periode Berakhir</label>
                                        <div class="col-sm-4">
                                            <div class="date" data-provide="datepicker"
                                                 data-date-format="dd-mm-yyyy">
                                                <input type="text" name="end_periode" required
                                                       class="form-control end-periode"
                                                       placeholder="0000-00-00">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="step2" role="tabpanel">

                            <div class="form-group row">
                                <label for="latitude" class="col-sm-2 control-label">Latitude / Garis Lintang</label>
                                <div class="col-sm-4">
                                    <input type="number" value="{{ $monografiPJ->latitude }}" step="any"
                                           class="form-control" name="latitude"
                                           placeholder="Latitude / Garis Lintang" required=""
                                           title="Masukkan garis lintang">
                                </div>
                                <label for="longitude" class="col-sm-2 control-label">Longitude / Garis Bujur</label>
                                <div class="col-sm-4">
                                    <input type="number" value="{{ $monografiPJ->longitude }}" step="any"
                                           class="form-control" name="longitude"
                                           placeholder="Longitude / Garis Bujur" required=""
                                           title="Masukkan garis bujur">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-4">
                                    <input type="email" value="{{ $monografiPJ->email }}" class="form-control"
                                           id="inputEmail3" name="email" required
                                           placeholder="Email">
                                </div>
                                <label for="no_telpon" class="col-sm-2 control-label">Nomor Telepon</label>
                                <div class="col-sm-4">
                                    <input type="tel" value="{{ $monografiPJ->no_telpon }}" class="form-control"
                                           id="no_telpon" name="no_telpon"
                                           pattern=".{9,14}"
                                           required="" title="9 to 14 characters" placeholder="Nomor Telpon">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="alamat" class="col-sm-2 control-label">Alamat Lengkap</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $monografiPJ->alamat }}" class="form-control"
                                           name="alamat" required
                                           placeholder="Alamat Lengkap"
                                           required="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Luas Wilayah Kerja" class="col-sm-2 control-label">Luas Wilayah
                                    Kerja</label>
                                <div class="col-sm-4">
                                    <input type="number" min=0 value="{{ $monografiPJ->luas }}" class="form-control"
                                           name="luas" required
                                           placeholder="Luas Wilayah Kerja">
                                </div>
                                <label for="Batas Wilayah Utara" class="col-sm-2 control-label">Batas Wilayah
                                    Utara</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $monografiPJ->bw_utara }}" class="form-control"
                                           name="bw_utara" required
                                           placeholder="Batas Wilayah Utara">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Batas Wilayah Selatan" class="col-sm-2 control-label">Batas Wilayah
                                    Selatan</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $monografiPJ->bw_selatan }}" class="form-control"
                                           name="bw_selatan" required
                                           placeholder="Batas Wilayah Selatan">
                                </div>
                                <label for="Batas Wilayah Timur" class="col-sm-2 control-label">Batas Wilayah
                                    Timur</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $monografiPJ->bw_timur }}" class="form-control"
                                           name="bw_timur" required
                                           placeholder="Batas Wilayah Timur">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Batas Wilayah Barat" class="col-sm-2 control-label">Batas Wilayah
                                    Barat</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $monografiPJ->bw_barat }}" class="form-control"
                                           name="bw_barat" required
                                           placeholder="Batas Wilayah Barat">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Batas Wilayah Barat" class="col-sm-2 control-label">Jarak dari ibukota
                                    provinsi</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $monografiPJ->jarak_provinsi }}" class="form-control"
                                           name="jarak_provinsi" required
                                           placeholder="Jarak dari ibukota provinsi">
                                </div>
                                <label for="Batas Wilayah Barat" class="col-sm-2 control-label">Jarak dari ibukota
                                    kabupaten / kota</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $monografiPJ->jarak_kabupaten }}" class="form-control"
                                           name="jarak_kabupaten" required
                                           placeholder="Jarak dari ibukota kabupaten / kota">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputFile" class="col-sm-2 control-label">Foto Kantor</label>
                                <div class="col-sm-10">
                                    <div tabindex="500" class="btn btn-success btn-file"><i
                                            class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">Pilih Foto Kantor Sekretariat </span><input
                                            type="file" id="foto" name="photo" accept="image/*" class=""></div>
                                </div>
                                <div class="col-md-12 mb-2">
                                    @if(!$monografiPJ->photo || $monografiPJ->photo=="default.png" || $monografiPJ->photo=="")
                                    <img id="preview-image" alt="preview image" style="max-height: 250px;" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif" >
                                    @else
                                    <img id="preview-image" alt="preview image" style="max-height: 250px;" src="{{ Storage::disk('s3')->url('images/monografi/pj/'.$monografiPJ->photo) }}" >
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="step3" role="tabpanel">

                            <div class="form-group row">
                                <label for="nama_istri" class="col-sm-2 control-label">Ketua</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control ui-autocomplete-input" name="ketua"
                                           id="nama1" value="{{ $kejamiyyahanPJ->ketua }}"
                                           placeholder="Nama Ketua" required="" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama_istri" class="col-sm-2 control-label">Sekretaris</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $kejamiyyahanPJ->sekretaris }}"
                                           class="form-control autocomplete nama ui-autocomplete-input"
                                           name="sekretaris" id="nama3" placeholder="Sekretaris" required=""
                                           autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama_istri" class="col-sm-2 control-label">Bendahara</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control ui-autocomplete-input" name="bendahara"
                                           id="nama5" value="{{ $kejamiyyahanPJ->bendahara }}"
                                           placeholder="Bendahara" required="" autocomplete="off">
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="step4" role="tabpanel">

                            <div class="form-group row">
                                <label class="col-sm-2 control-label">Setiap Hari</label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="hari_ngantor">
                                        <option>-- Pilih Hari --</option>
                                        <option value="Ahad"
                                        @if($kejamiyyahanPJ->hari_ngantor == "Ahad") {{ "selected" }} @endif>Ahad
                                        </option>
                                        <option value="Senin"
                                        @if($kejamiyyahanPJ->hari_ngantor == "Senin") {{ "selected" }} @endif>Senin
                                        </option>
                                        <option value="Selasa"
                                        @if($kejamiyyahanPJ->hari_ngantor == "Selasa") {{ "selected" }} @endif>Selasa
                                        </option>
                                        <option value="Rabu"
                                        @if($kejamiyyahanPJ->hari_ngantor == "Rabu") {{ "selected" }} @endif>Rabu
                                        </option>
                                        <option value="Kamis"
                                        @if($kejamiyyahanPJ->hari_ngantor == "Kamis") {{ "selected" }} @endif>Kamis
                                        </option>
                                        <option value="Jum'at"
                                        @if($kejamiyyahanPJ->hari_ngantor == "Jum'at") {{ "selected" }} @endif>Jum'at
                                        </option>
                                        <option value="Sabtu"
                                        @if($kejamiyyahanPJ->hari_ngantor == "Sabtu") {{ "selected" }} @endif>Sabtu
                                        </option>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label">Pukul</label>
                                <div class="col-sm-3 bootstrap-timepicker">
                                    <div class="bootstrap-timepicker-widget dropdown-menu">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><a href="#" data-action="incrementHour"><i
                                                            class="glyphicon glyphicon-chevron-up"></i></a></td>
                                                <td class="separator">&nbsp;</td>
                                                <td><a href="#" data-action="incrementMinute"><i
                                                            class="glyphicon glyphicon-chevron-up"></i></a></td>
                                                <td class="separator">&nbsp;</td>
                                                <td class="meridian-column"><a href="#" data-action="toggleMeridian"><i
                                                            class="glyphicon glyphicon-chevron-up"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td><span class="bootstrap-timepicker-hour">07</span></td>
                                                <td class="separator">:</td>
                                                <td><span class="bootstrap-timepicker-minute">00</span></td>
                                                <td class="separator">&nbsp;</td>
                                                <td><span class="bootstrap-timepicker-meridian">AM</span></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" data-action="decrementHour"><i
                                                            class="glyphicon glyphicon-chevron-down"></i></a></td>
                                                <td class="separator"></td>
                                                <td><a href="#" data-action="decrementMinute"><i
                                                            class="glyphicon glyphicon-chevron-down"></i></a></td>
                                                <td class="separator">&nbsp;</td>
                                                <td><a href="#" data-action="toggleMeridian"><i
                                                            class="glyphicon glyphicon-chevron-down"></i></a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="input-group">
                                        <input type="time" class="form-control timepicker" required
                                               value="{{ $kejamiyyahanPJ->waktu_ngantor }}" name="waktu_ngantor">

                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 control-label">Musjam terakhir</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label">Tanggal Masehi</label>
                                <div class="col-sm-3">
                                    <div class="input-group date" data-provide="datepicker"
                                         data-date-format="dd-mm-yyyy">
                                        <input type="text" value="{{ $kejamiyyahanPJ->musjam_terakhir_masehi }}" required
                                               name="musjam_terakhir_masehi" class="form-control masehi-date-input"
                                               placeholder="0000-00-00">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-sm-2 control-label">Tanggal Hijriah</label>
                                <div class="col-sm-3">
                                    <div class="input-group date" data-provide="datepicker"
                                         data-date-format="dd-mm-yyyy">
                                        <input type="text" value="{{ $kejamiyyahanPJ->musjam_terakhir_hijriyyah }}"
                                               name="musjam_terakhir_hijriyyah" class="form-control hijri-date-input"
                                               id="tanggal-hijriah" required
                                               placeholder="0000-00-00">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="step5" role="tabpanel">
                            <div class="form-group row">
                                <label for="biasa" class="col-sm-2 control-label">Anggota Biasa</label>
                                <div class="col-sm-10">
                                    <input type="number" min=0 value="{{ $kejamiyyahanPJ->anggota_biasa }}"
                                           class="form-control" name="anggota_biasa" required
                                           placeholder="Anggota Biasa">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="istimewa" class="col-sm-2 control-label">Anggota Istimewa</label>
                                <div class="col-sm-10">
                                    <input type="number" min=0 value="{{ $kejamiyyahanPJ->anggota_luar_biasa }}"
                                           class="form-control" name="anggota_luar_biasa" required
                                           placeholder="Anggota Istimewa">
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="step6" role="tabpanel">
                            <div class="form-group row">
                                <label for="biasa" class="col-sm-2 control-label">Tidak Her-Registrasi</label>
                                <div class="col-sm-4">
                                    <input type="number" min=0 value="{{ $kejamiyyahanPJ->tidak_herReg }}"
                                           class="form-control" name="tidak_herReg" required
                                           placeholder="Tidak Her-Registrasi">
                                </div>
                                <label for="istimewa" class="col-sm-2 control-label">Mutasi Ke Persis</label>
                                <div class="col-sm-4">
                                    <input type="number" min=0 value="{{ $kejamiyyahanPJ->mutasi_persis }}"
                                           class="form-control" name="mutasi_persis" required
                                           placeholder="Mutasi Ke Persis">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kehormatan" class="col-sm-2 control-label">Mutasi Tempat</label>
                                <div class="col-sm-4">
                                    <input type="number" min=0 value="{{ $kejamiyyahanPJ->mutasi_tempat }}"
                                           class="form-control" name="mutasi_tempat" required
                                           placeholder="Mutasi Tempat">
                                </div>
                                <label for="kehormatan" class="col-sm-2 control-label">Mengundurkan diri</label>
                                <div class="col-sm-4">
                                    <input type="number" min=0 value="{{ $kejamiyyahanPJ->mengundurkan_diri }}"
                                           class="form-control" name="mengundurkan_diri" required
                                           placeholder="Mengundurkan diri">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kehormatan" class="col-sm-2 control-label">Meninggal dunia</label>
                                <div class="col-sm-4">
                                    <input type="number" min=0 value="{{ $kejamiyyahanPJ->meninggal_dunia }}"
                                           class="form-control" name="meninggal_dunia" required
                                           placeholder="Meninggal dunia">
                                </div>
                                <label for="kehormatan" class="col-sm-2 control-label">Calon Anggota /
                                    Simpatisan</label>
                                <div class="col-sm-4">
                                    <input type="number" min=0 value="{{ $kejamiyyahanPJ->calon_anggota }}"
                                           class="form-control" name="calon_anggota" required
                                           placeholder="Calon Anggota / Simpatisan">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="block-content block-content-sm block-content-full bg-body-light rounded-bottom">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-secondary" data-wizard="prev">
                                    <i class="fa fa-angle-left mr-1"></i> Kembali
                                </button>
                            </div>
                            <div class="col-6 text-right">
                                <button type="button" class="btn btn-secondary" data-wizard="next">
                                    Selanjutnya <i class="fa fa-angle-right ml-1"></i>
                                </button>
                                <button type="submit" class="btn btn-primary d-none" data-wizard="finish">
                                    <i class="fa fa-check mr-1"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal PC -->
    <div class="modal fade" id="myModalPC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PC</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPC" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PC</th>
                            <th>Nama PC</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal PC -->

    <!-- Modal PD -->
    <div class="modal fade" id="myModalPD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PD</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPD" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PD</th>
                            <th>Nama PD</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal PD -->

    <!-- Modal PW -->
    <div class="modal fade" id="myModalPW" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PW</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPW" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PW</th>
                            <th>Nama PW</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no =1)
                        @foreach ($dataPW as $data_pw)
                            <tr class="pilih3" data-kd_pw="{{$data_pw->kd_pw}}"
                                data-nama_pw="{{$data_pw->nama_pw}}">
                                <td>{{$no++}}</td>
                                <td>{{$data_pw->kd_pw}}</td>
                                <td>{{$data_pw->nama_pw}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal PW -->

@endsection

@section('js_after')
    <!-- Page JS Hijri-Date-Picker Code -->
    <script src="{{ asset('js/plugins/moment/moment-with-locales.js')}}"></script>
    <script src="{{ asset('js/plugins/hijri-datepicker/js/moment-hijri.js')}}"></script>
    <script src="{{ asset('js/plugins/hijri-datepicker/js/bootstrap-hijri-datetimepicker.min.js')}}"></script>

    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <!-- <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script> -->
    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ url('/jamiyyah/datatables/pw')  }}";
        let pimpinan = "PW";
        let firstLoadRegencies = true;
        let firstLoadDistrics = true;
    </script>
    <!--Kode PC-->
    <script type="text/javascript">

        //foto preview
        $('#foto').change(function () {

            let reader = new FileReader();

            reader.onload = (e) => {

                $('#preview-image').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);

        });

        //  jika dipilih, npa akan masuk ke input dan modal di tutup
        // Kode PC
        $(document).on('click', '.pilih1', function (e) {
            document.getElementById("noPC").value = $(this).attr('data-kd_pc');
            document.getElementById("namaPC").value = $(this).attr('data-nama_pc');
            $('#myModalPC').modal('hide');
        });
        // Kode PD
        $(document).on('click', '.pilih2', function (e) {
            let kdPD = $(this).attr('data-kd_pd');

            document.getElementById("noPD").value = $(this).attr('data-kd_pd');
            document.getElementById("namaPD").value = $(this).attr('data-nama_pd');

            document.getElementById("noPC").value = "";
            document.getElementById("namaPC").value = "";

            $('#myModalPD').modal('hide');

            $.get(`/get-related-pc/${kdPD}`, function (response) {
                $("#lookupPC").dataTable().api().destroy();
                $("#lookupPC tbody").empty();
                let data = "";
                $.each(response, function (index, item) {
                    data = data + `<tr class="pilih1" data-kd_pc="${item.kd_pc}"
                                data-nama_pc="${item.nama_pc}">
                                    <td>${index + 1}</td>
                                    <td>${item.kd_pc}</td>
                                    <td>${item.nama_pc}</td>
                                </tr>`
                });
                $("#lookupPC tbody").html(data);
                $("#lookupPC").dataTable();
            });
        })

        // Kode PW
        $(document).on('click', '.pilih3', function (e) {
            let kdPW = $(this).attr('data-kd_pw');
            document.getElementById("noPW").value = kdPW;
            document.getElementById("namaPW").value = $(this).attr('data-nama_pw');

            document.getElementById("noPD").value = "";
            document.getElementById("namaPD").value = "";

            document.getElementById("noPC").value = "";
            document.getElementById("namaPC").value = "";

            $('#myModalPW').modal('hide');

            $.get(`/get-related-pd/${kdPW}`, function (response) {
                $("#lookupPD").dataTable().api().destroy();
                $("#lookupPD tbody").empty();
                let data = "";
                $.each(response, function (index, item) {
                    data = data + `<tr class="pilih2" data-kd_pd="${item.kd_pd}"
                                data-nama_pd="${item.nama_pd}">
                                    <td>${index + 1}</td>
                                    <td>${item.kd_pd}</td>
                                    <td>${item.nama_pd}</td>
                                </tr>`
                });

                $("#lookupPD tbody").html(data);
                $("#lookupPD").dataTable();
            });
        })

        //  tabel lookup npa
        $(function () {
            $("#lookupPW").dataTable();
        });

        // laravel file manager
        $(document).ready(function () {
            $("#provinces").change();

            let dateStart = `{{ $monografiPJ->start_periode  }}`;
            let dateEnd = `{{ $monografiPJ->end_periode  }}`;

            if(dateStart === '0000-00-00'){
                $(".start-periode").val("");
            }else{
                let start = moment("{{ $monografiPJ->start_periode  }}").format("MMMM YYYY")
                $(".start-periode").val(start);
            }

            if(dateEnd === '0000-00-00'){
                $(".end-periode").val("");
            }else{
                let end = moment("{{ $monografiPJ->end_periode }}").format("MMMM YYYY")
                $(".end-periode").val(end);
            }

        });

        $(function () {

            initHijrDatePicker();

            initHijrDatePickerDefault();

            $('.disable-date').hijriDatePicker({

                minDate: "2020-01-01",
                maxDate: "2021-01-01",
                viewMode: "years",
                hijri: false,
                debug: true
            });

        });

        // Hijri Input Date
        function initHijrDatePicker() {

            $.fn.hijriDatePicker.defaults.icons.today = 'Hari Ini';
            $.fn.hijriDatePicker.defaults.icons.close = 'Close';

            $(".hijri-date-input").hijriDatePicker({
                language: "ar-sa",
                format: "DD-MM-YYYY",
                hijriFormat: "iYYYY-iMM-iDD",
                dayViewHeaderFormat: "MMMM YYYY",
                hijriDayViewHeaderFormat: "iMMMM iYYYY",
                showSwitcher: false,
                allowInputToggle: true,
                showTodayButton: false,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                hijri: true,
                debug: true,
                showClear: false,
                showTodayButton: true,
                showClose: true
            });

            $(".masehi-date-input").hijriDatePicker({
                locale: "en-US",
                format: "YYYY-MM-DD",
                allowInputToggle: true,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                debug: true,
                showClear: false,
                showSwitcher: false,
                hijri: false,
                showTodayButton: true,
                showClose: true
            });

            $(".hijri-date-input").on('dp.change dp.hide', function (e) {
                if (e.date) {
                    $(this).val(e.date.format("iYYYY-iMM-iDD"))
                    $(".masehi-date-input").val(e.date.format('YYYY-MM-DD'))
                }
            });

            $(".masehi-date-input").on('dp.change dp.hide', function (e) {
                if (e.date) {
                    $(this).val(e.date.format('YYYY-MM-DD'))
                    $(".hijri-date-input").val(e.date.format("iYYYY-iMM-iDD"))
                }
            });

            $(".start-periode").hijriDatePicker({
                locale: "en-US",
                format: "MMMM YYYY",
                allowInputToggle: true,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                debug: true,
                showClear: false,
                showSwitcher: false,
                hijri: false,
                showTodayButton: true,
                showClose: true
            });

            $(".end-periode").hijriDatePicker({
                locale: "en-US",
                format: "MMMM YYYY",
                allowInputToggle: true,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                debug: true,
                showClear: false,
                showSwitcher: false,
                hijri: false,
                showTodayButton: true,
                showClose: true
            });

            $(".start-periode").on('dp.hide', function (e) {
                if(e.date > moment($(".end-periode").val())){
                    window.alert("tanggal mulai tidak boleh lebih besar dari tanggal berakhir")
                    $(this).val(moment($(".end-periode").val()).subtract(1, 'months').format("MMMM YYYY"))
                };
            })

            $(".end-periode").on('dp.change', function (e) {
                if(e.date < moment($(".start-periode").val())){
                    window.alert("tanggal berakhir tidak boleh lebih kecil dari tanggal mulai")
                    $(this).val(moment($(".start-periode").val()).add(1, 'months').format("MMMM YYYY"))
                };
            })

        }

        function initHijrDatePickerDefault() {

            $(".hijri-date-default").hijriDatePicker();
        }
    </script>


    <script type="text/javascript">
        $('#provinces').on('change', function (e) {
            console.log(" provinces change");
            console.log(e);
            var province_id = e.target.value;
            console.log(province_id);
            $.get('/regencies?province_id=' + province_id, function (data) {
                console.log(data);
                $('#regencies').empty();
                $('#regencies').append('<option value="" disable="true" selected="true">=== Pilih Kabupaten/Kota ===</option>');

                $('#districts').empty();
                $('#districts').append('<option value="" disable="true" selected="true">=== Pilih Kecamatan ===</option>');

                $('#villages').empty();
                $('#villages').append('<option value="" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, regenciesObj) {
                    $('#regencies').append('<option value="' + regenciesObj.id + '">' + regenciesObj.nama + '</option>');
                })

                if (firstLoadRegencies) {
                    $("#regencies").val("{{ $monografiPJ->kabupaten }}");
                    $("#regencies").change();
                    firstLoadRegencies = false;
                }
            });
        });

        $('#regencies').on('change', function (e) {
            console.log(" regencies change");
            console.log(e);
            var regencies_id = e.target.value;
            $.get('/districts?regencies_id=' + regencies_id, function (data) {
                console.log(data);
                $('#districts').empty();
                $('#districts').append('<option value="" disable="true" selected="true">=== Pilih Kecamatan ===</option>');

                $.each(data, function (index, districtsObj) {
                    $('#districts').append('<option value="' + districtsObj.id + '">' + districtsObj.nama + '</option>');
                })

                if (firstLoadDistrics) {
                    $("#districts").val("{{ $monografiPJ->kecamatan }}");
                    $("#districts").change();
                    firstLoadDistrics = false;
                }
            });
        });

        $('#districts').on('change', function (e) {
            console.log(e);
            var districts_id = e.target.value;
            $.get('/village?districts_id=' + districts_id, function (data) {
                console.log(data);
                $('#villages').empty();
                $('#villages').append('<option value="" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, villagesObj) {
                    $('#villages').append('<option value="' + villagesObj.id + '">' + villagesObj.nama + '</option>');
                })
                $('#villages').val("{{ $monografiPJ->desa }}");
            });
        });

        $(".monografi-pj-wizard").bootstrapWizard({
            'tabClass': 'nav nav-tabs',
            'nextSelector': '[data-wizard="next"]',
            'previousSelector': '[data-wizard="prev"]',
            'firstSelector': '[data-wizard="first"]',
            'lastSelector': '[data-wizard="last"]',
            'finishSelector': '[data-wizard="finish"]',
            'backSelector': '[data-wizard="back"]',
            'onNext': function (tab, navigation, index) {
                var $valid = $("#monografi-pj-form").valid();
                if (!$valid) {
                    return false;
                }
            },
            'onTabClick': function (tab, navigation, index) {
                return false;
            }
        });

        $("#monografi-pj-form").submit(function (e) {
            e.preventDefault();
            let form = new FormData($('#monografi-pj-form')[0]);
            form.append('_method', 'PUT');
            let start = moment("01" + form.get("start_periode")).format("YYYY-MM-DD");
            let end = moment("01" + form.get("end_periode")).format("YYYY-MM-DD");
            form.set("start_periode", start);
            form.set("end_periode", end);

            swal({
                title: 'Saving data..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: `{{ route('pj.update', [$monografiPJ->id], false) }}`,
                processData: false,
                contentType: false,
                data: form,
                type: 'POST',
                success: function (resp) {
                    if (resp.status == true) {
                        swal.close();
                        swal("Berhasil!", "Data telah berhasil diperbarui.", "success").then((result) => {
                            window.location = `{{ route('pj.index',[], false) }}`;
                        });
                    } else {
                        swal.close();
                        swal("Error!", "Data gagal disimpan. Detail " + resp.message, "error");
                    }
                },
                error: function (resp) {
                    swal.close();
                    alert(resp.responseJSON.message);
                }
            });
        })

    </script>
@endsection
