@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">

@endsection

@section('content')
    <!-- Hero -->
    {{-- <main id="main-container"> --}}
        <!-- Hero -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">
                        Data Pimpinan Jamaah <small
                            class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                    </h1>
                    <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-alt">
                            <li class="breadcrumb-item">Data Pimpinan Jamaah</li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a class="link-fx" href="/dashboard">Dashboard</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- Page Content -->
        <div class="content">
            <!-- Dynamic Table with Export Buttons -->
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Table <small>Data Pimpinan Jamaah</small></h3>
                    @if(Permission::checkPermission('monografipj.create'))
                        <a href="{{route('pj.create')}}" class="btn btn-success btn-sm"><span
                                class="si si-plus"></span> Tambah</a>
                    @endif
                </div>
                <div class="block-content block-content-full">
                    <div class="table-responsive">
                        <table
                            class="table table-bordered table-striped table-vcenter js-dataTable-monografi-pj dataTable no-footer"
                            id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                            <thead>
                            <tr role="row">
                                <th>Photo</th>
                                <th>Kode PJ</th>
                                <th>Nama Jamaah</th>
                                <th>PC</th>
                                <th>PD</th>
                                <th>PW</th>
                                <th>Desa</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($monografiPJ as $pj)
                                <tr>
                                    <td class="font-w600 font-size-sm">
                                        @if($pj->photo != null && $pj->photo != "")
                                            <img width="70px"
                                                src="{{ Storage::disk('s3')->url('images/monografi/pj/'. $pj->photo) }}"
                                                alt="{{$pj->photo}}">
                                        @else
                                            <img width="70px" src="{{ asset('media/photos/desa.png') }}"
                                                alt="{{$pj->photo}}">
                                        @endif
                                    </td>
                                    <td class="font-w600 font-size-sm">{{$pj->kode_pj}}</td>
                                    <td class="font-w600 font-size-sm">{{$pj->nama_pj}}</td>
                                    <td class="font-w600 font-size-sm">{{$pj->pc->nama_pc}}</td>
                                    <td class="font-w600 font-size-sm">{{$pj->pd->nama_pd}}</td>
                                    <td class="font-w600 font-size-sm">{{$pj->pw->nama_pw}}</td>
                                    <td class="font-w600 font-size-sm">{{$pj->getRelationValue('desa')->nama}}</td>
                                    <td>
                                        <div class="btn-group float-right"></div>

                                        @if(Permission::checkPermission('monografipj.read'))
                                            <a href="{{ route('pj.show', $pj->id, false) }}" class="btn btn-primary btn-sm"><i
                                                    class="si si-list"></i></a>
                                        @endif

                                        @if(Permission::checkPermission('monografipj.update'))
                                            <a class="btn btn-warning btn-sm"
                                            href="{{ route('pj.edit', $pj->id, false) }}"><i
                                                    class="si si-pencil"></i></a>
                                        @endif

                                        @if(Permission::checkPermission('monografipj.delete'))
                                            <button type="button" class="btn btn-danger btn-sm"
                                                    onclick="remove('{{ route('pj.destroy', $pj->id, false) }}')">
                                                <i class="si si-trash"></i></button>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="font-w600 font-size-sm" align="center" colspan="8">Tidak ada data monografi</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- END Page Content -->
    {{-- </main> --}}
    <!-- END Page Content -->


@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>


    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ url('/jamiyyah/datatables/pc')  }}";
        let pimpinan = "PC";

        jQuery('.js-dataTable-monografi-pj').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            order: [[0, 'asc']],
            autoWidth: true,
            buttons: [{
                extend: 'copy',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'csv',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'print',
                className: 'btn btn-sm btn-primary'
            }],
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        })

    </script>
    <!--Kode PC-->
    <script type="text/javascript">
        //  jika dipilih, npa akan masuk ke input dan modal di tutup
        // Kode PC
        $(document).on('click', '.pilih1', function (e) {
            document.getElementById("noPC").value = $(this).attr('data-kd_pc');
            document.getElementById("namaPC").value = $(this).attr('data-nama_pc');
            $('#myModalPC').modal('hide');
        });
        // Kode PD
        $(document).on('click', '.pilih2', function (e) {
            document.getElementById("noPD").value = $(this).attr('data-kd_pd');
            document.getElementById("namaPD").value = $(this).attr('data-nama_pd');
            $('#myModalPD').modal('hide');
        });
        // Kode PW
        $(document).on('click', '.pilih3', function (e) {
            document.getElementById("noPW").value = $(this).attr('data-kd_pw');
            document.getElementById("namaPW").value = $(this).attr('data-nama_pw');
            $('#myModalPW').modal('hide');
        });

        //  tabel lookup npa
        $(function () {
            $("#lookupPC").dataTable();
            $("#lookupPD").dataTable();
            $("#lookupPW").dataTable();
        });

        // laravel file manager
        $(document).ready(function () {
            $('#lfm').filemanager('foto');
        });

        $(function () {

            initHijrDatePicker();

            initHijrDatePickerDefault();

            $('.disable-date').hijriDatePicker({

                minDate: "2020-01-01",
                maxDate: "2021-01-01",
                viewMode: "years",
                hijri: false,
                debug: true
            });

        });

        function initHijrDatePicker() {

            $(".hijri-date-input").hijriDatePicker({
                language: "ar-sa",
                format: "DD-MM-YYYY",
                hijriFormat: "iYYYY-iMM-iDD",
                dayViewHeaderFormat: "MMMM YYYY",
                hijriDayViewHeaderFormat: "iMMMM iYYYY",
                showSwitcher: true,
                allowInputToggle: true,
                showTodayButton: false,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                hijri: false,
                debug: true,
                showClear: true,
                showTodayButton: true,
                showClose: true
            });
        }

        function initHijrDatePickerDefault() {

            $(".hijri-date-default").hijriDatePicker();
        }

        function remove(route) {
            swal({
                title: "Apa anda yakin?",
                text: "Data akan dihapus dan tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus!",
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {

                    swal({
                        title: 'Deleting data..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    });

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url: route,
                        type: 'DELETE',
                        dataType: 'json',
                        success: function (resp) {
                            if (resp == true) {
                                swal.close();
                                swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                                    window.location.reload();
                                });
                            }
                        },
                        error: function (resp) {
                            swal.close();
                            alert(resp.responseJSON.message);
                        }
                    });
                }
            })
        }
    </script>


    <script type="text/javascript">
        $('#provinces').on('change', function (e) {
            console.log(e);
            var province_id = e.target.value;
            $.get('/regencies?province_id=' + province_id, function (data) {
                console.log(data);
                $('#regencies').empty();
                $('#regencies').append('<option value="0" disable="true" selected="true">=== Pilih Kabupaten/Kota ===</option>');

                $('#districts').empty();
                $('#districts').append('<option value="0" disable="true" selected="true">=== Pilih Kecamatan ===</option>');

                $('#villages').empty();
                $('#villages').append('<option value="0" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, regenciesObj) {
                    $('#regencies').append('<option value="' + regenciesObj.id + '">' + regenciesObj.nama + '</option>');
                })
            });
        });

        $('#regencies').on('change', function (e) {
            console.log(e);
            var regencies_id = e.target.value;
            $.get('/districts?regencies_id=' + regencies_id, function (data) {
                console.log(data);
                $('#districts').empty();
                $('#districts').append('<option value="0" disable="true" selected="true">=== Pilih Kecamatan ===</option>');

                $.each(data, function (index, districtsObj) {
                    $('#districts').append('<option value="' + districtsObj.id + '">' + districtsObj.nama + '</option>');
                })
                //
                // $.each(data, function () {
                //     console.log("ID: " + this.id);
                //     console.log("Kecamatan ID: " + this.kecamatan_id);
                //     console.log("Name: " + this.nama);
                // });
            });
        });

        $('#districts').on('change', function (e) {
            console.log(e);
            var districts_id = e.target.value;
            $.get('/village?districts_id=' + districts_id, function (data) {
                console.log(data);
                $('#villages').empty();
                $('#villages').append('<option value="0" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, villagesObj) {
                    $('#villages').append('<option value="' + villagesObj.id + '">' + villagesObj.nama + '</option>');
                    console.log("|" + villagesObj.id + "|" + villagesObj.kecamatan_id + "|" + villagesObj.nama);
                })
                //
                // $.each(data, function () {
                //     console.log("ID: " + this.id);
                //     console.log("Kecamatan ID: " + this.kecamatan_id);
                //     console.log("Name: " + this.nama);
                // });
            });
        });

    </script>


@endsection
