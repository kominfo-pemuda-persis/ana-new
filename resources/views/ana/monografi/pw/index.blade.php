@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">

@endsection

@section('content')
    <!-- Hero -->
    {{-- <main id="main-container"> --}}
        <!-- Hero -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">
                        Data Pimpinan Wilayah <small
                            class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                    </h1>
                    <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-alt">
                            <li class="breadcrumb-item">Data Pimpinan Wilayah</li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a class="link-fx" href="/dashboard">Dashboard</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- Page Content -->
        <div class="content">
            <!-- Dynamic Table with Export Buttons -->
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Table <small>Data Pimpinan Wilayah</small></h3>
                    @if(Permission::checkPermission('monografipw.create'))
                        <a href="{{route('pw.create')}}" class="btn btn-success btn-sm"><span
                                class="si si-plus"></span> Tambah</a>
                    @endif
                </div>
                <div class="block-content block-content-full">
                    <div class="table-responsive">
                        <table
                            class="table table-bordered table-striped table-vcenter js-dataTable-monografi-pw dataTable no-footer"
                            id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                            <thead>
                            <tr role="row">
                                <th>Foto</th>
                                <th>PW</th>
                                <th>Email</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($monografiPW as $pw)
                                <tr>
                                    <td class="font-w600 font-size-sm">
                                        @if($pw->foto != null && $pw->foto != "")
                                            <img width="70px"
                                                src="{{ Storage::disk('s3')->url('images/monografi/pw/'. $pw->foto) }}"
                                                alt="{{$pw->foto}}">
                                        @else
                                            <img width="70px" src="{{ asset('media/photos/desa.png') }}"
                                                alt="{{$pw->photo}}">
                                        @endif
                                    </td>
                                    <td class="font-w600 font-size-sm">{{$pw->nama_pw}}</td>
                                    <td class="font-w600 font-size-sm">{{$pw->email}}</td>
                                    <td>
                                        <div class="btn-group float-right"></div>
                                        @if(Permission::checkPermission('monografipw.read'))
                                            <a href="{{ route('pw.show', $pw->id, false) }}"
                                            class="btn btn-primary btn-sm"><i class="si si-list"></i></a>
                                        @endif

                                        @if(Permission::checkPermission('monografipw.update'))
                                            <a href="{{ route('pw.edit', $pw->id, false) }}"
                                            class="btn btn-warning btn-sm"><i class="si si-pencil"></i></a>
                                        @endif

                                        @if(Permission::checkPermission('monografipw.delete'))
                                            <button type="button" class="btn btn-danger btn-sm"
                                                    onclick="remove('{{ route('pw.destroy', $pw->id, false) }}')">
                                                <i class="si si-trash"></i></button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    {{-- </main> --}}
    <!-- END Page Content -->


@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>


    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ url('/jamiyyah/datatables/pw')  }}";
        let pimpinan = "PW";

        jQuery('.js-dataTable-monografi-pw').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            order: [[0, 'asc']],
            autoWidth: true,
            buttons: [{
                extend: 'copy',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'csv',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'print',
                className: 'btn btn-sm btn-primary'
            }],
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        })


        function remove(route) {
            swal({
                title: "Apa anda yakin?",
                text: "Data akan dihapus dan tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus!",
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {

                    swal({
                        title: 'Deleting data..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    });

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url: route,
                        type: 'DELETE',
                        dataType: 'json',
                        success: function (resp) {
                            if (resp == true) {
                                swal.close();
                                swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                                    window.location.reload();
                                });
                            }
                        },
                        error: function (resp) {
                            swal.close();
                            alert(resp.responseJSON.message);
                        }
                    });
                }
            })
        }
    </script>
@endsection

