@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/pg_anggota_profile.js') }}"></script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 font-w300 my-2">
                    Monografi PW
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Monografi PW</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h2 class="block-title text-center">Detail Monografi PW<small></small></h2>
            </div>
            <div class="block-content block-content-full">
                <div class="text-center">
                    @if(!$monografiPW->foto || $monografiPW->foto=="default.png" || $monografiPW->foto=="")
                    <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ asset('media/photos/desa.png') }}" >
                    @else
                    <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ Storage::disk('s3')->url('images/monografi/pw/'.$monografiPW->foto) }}" >
                    @endif
                </div>
                <hr>
                <section id="data-anggota">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Monografi PW<small></small></h3></td>
                                </tr>
                                <tr>
                                    <td>PW</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->kd_pw }} {{ $monografiPW->nama_pw }}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->getRelationValue('provinsi')->nama }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Geografis<small></small></h3></td>
                                </tr>
                                <tr>
                                    <td>Latitude / Garis Lintang</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->latitude }}</td>
                                </tr>
                                <tr>
                                    <td>Longitude / Garis Bujur</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->longitude }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->email }}</td>
                                </tr>
                                <tr>
                                    <td>Nomor Telepon</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->no_kontak }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Lengkap</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->alamat_utama }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Alternatif</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->alamat_alternatif }}</td>
                                </tr>
                                <tr>
                                    <td>Luas Wilayah Kerja</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->luas }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Utara</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->bw_utara }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Selatan</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->bw_selatan }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Timur</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->bw_timur }}</td>
                                </tr>
                                <tr>
                                    <td>Batas Wilayah Barat</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->bw_barat }}</td>
                                </tr>
                                <tr>
                                    <td>Jarak dari ibukota negara</td>
                                    <td>:</td>
                                    <td>{{ $monografiPW->jarak_dari_ibukota_negara }}</td>
                                </tr>
                                <tr>
                                    <td>Jarak dari ibukota provinsi</td>
                                    <td>:</td>
                                    <td>{{  $monografiPW->jarak_dari_ibukota_provinsi  }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-tasykil">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <td><h3 class="block-title">Data Pimpinan / Tasykil</h3></td>
                                </tr>
                                <tr>
                                    <td>Ketua</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->ketua }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Ketua</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_ketua }}</td>
                                </tr>
                                <tr>
                                    <td>Sekretaris</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->sekretaris }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Sekretaris</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_sekretaris }}</td>
                                </tr>
                                <tr>
                                    <td>Bendahara</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bendahara }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bendahara</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bendahara }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Jam'iyyah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_jamiyyah }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Jam'iyyah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_jamiyyah }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Kaderisasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_kaderisasi }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Kaderisasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_kaderisasi }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Administrasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_administrasi }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Administrasi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_administrasi }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Pendidikan</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_pendidikan }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Pendidikan</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_pendidikan }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Da’wah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_dakwah }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Da’wah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_dakwah }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Kominfo</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_humas_publikasi }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Kominfo</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_humas_publikasi }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang H.A.L</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_hal }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang H.A.L</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_hal }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Olahraga & Seni</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_or_seni }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Olahraga & Seni</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_or_seni }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Sosial</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_sosial }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Sosial</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_sosial }}</td>
                                </tr>
                                <tr>
                                    <td>Bidang Ekonomi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->bid_ekonomi }}</td>
                                </tr>
                                <tr>
                                    <td>Wakil Bidang Ekonomi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->wkl_bid_ekonomi }}</td>
                                </tr>
                                <tr>
                                    <td>Penasehat 1</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->penasehat1 }}</td>
                                </tr>
                                <tr>
                                    <td>Penasehat 2</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->penasehat2 }}</td>
                                </tr>
                                <tr>
                                    <td>Penasehat 3</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->penasehat3 }}</td>
                                </tr>
                                <tr>
                                    <td>Penasehat 4</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->penasehat4 }}</td>
                                </tr>
                                <tr>
                                    <td>Pembantu Umum 1</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->pembantu_umum2 }}</td>
                                </tr>
                                <tr>
                                    <td>Pembantu Umum 2</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->pembantu_umum2 }}</td>
                                </tr>
                                <tr>
                                    <td>Pembantu Umum 3</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->pembantu_umum3 }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-pendidikan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Waktu Ngantor<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Setiap Hari</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->hari }}</td>
                                </tr>
                                <tr>
                                    <td>Pukul</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->pukul }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Muswil terakhir Masehi</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->musda_terakhir_m }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Muswil terakhir Hijriah</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->musda_terakhir_h }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-keterampilan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Keanggotaan<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Anggota Biasa</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->anggota_biasa }}</td>
                                </tr>
                                <tr>
                                    <td>Anggota Tersiar</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->anggota_tersiar }}</td>
                                </tr>
                                <tr>
                                    <td>Anggota Istimewa</td>
                                    <td>:</td>
                                    <td>{{ $kejamiyyahanPW->anggota_istimewa }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
@endsection
