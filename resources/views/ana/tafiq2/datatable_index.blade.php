@extends('layouts.backend')

@section('css_before')
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.3.3/css/fixedColumns.bootstrap4.min.css">
<style>
    .DTFC_LeftBodyLiner {
        overflow-x: hidden
    }

    .img-caang {
        object-fit: scale-down;
        width: 100%;
    }

    table.ana-table {
        background: white !important;
    }

    .DTFC_LeftBodyLiner table.dataTable.ana-table tbody tr td {
        overflow-wrap: anywhere;
    }
</style>
@endsection

@section('js_after')
<!-- Page JS Plugins -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/js/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('/js/plugins/moment/locales.js') }}"></script>
<script src="{{ asset('/js/plugins/exceljs.min.js') }}"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>


<!-- Page JS Code -->
<script>
    let tanggalStart = null;
        let tanggalEnd = null;
        moment.locale('id');
        let dataTableUrl = "{{ route('datatable.tafiq2') }}";
        let selectedDataTafiq2 = [];
        function removeTafiqData(id){
            swal({
                title: "Apa anda yakin?",
                text: "Data akan dihapus dan tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus!",
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url:`/data-tafiq2/${id}/delete`,
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(resp) {
                            swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                                tableTafiq2.ajax.reload();
                            });
                        },
                        error: function(resp) {
                            alert(resp.responseJSON.message);
                        }
                    });
                }
            })
        }

        function checkAllData(e) {
            let dataRaw = jQuery('#dataTable-tafiq2')
                .dataTable()
                .api()
                .rows()
                .data().toArray();

            if (e.checked) {
                selectedDataTafiq2 = dataRaw.map(val => {
                    return val.id;
                });
            } else {
                selectedDataTafiq2 = [];
            }

            dataRaw.forEach(function (val) {
                $(`.caang-${val.id}`).prop("checked", e.checked)
            })
        }

        function approveData(e, level) {
            let url = '/data-tafiq2/set-status';

            let levelMapping = {
                admin_pc : "pc",
                admin_pd : "pd",
                admin_pp : "pp",
                admin_pw : "pw",
            }

            if (selectedDataTafiq2.length <= 0) {
                swal("", "Silahkan select data terlebih dahulu", "error")
                return;
            }

            swal({
                title: `Approve ${selectedDataTafiq2.length} data?`,
                text: "Data akan diapporve dan tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Approve!",
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {

                    swal({
                        title: "Approving Data...",
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        onOpen: () => {
                            swal.showLoading();
                        },
                    });

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url,
                        type: 'POST',
                        data: JSON.stringify({
                            level: levelMapping[level],
                            id: selectedDataTafiq2
                        }),
                        dataType: 'json',
                        processData: false,
                        contentType: 'application/json',
                        success: function (resp) {
                            swal("Berhasil!", "Data telah berhasil diapprove.", "success").then((result) => {
                                tableTafiq2.ajax.reload();
                                selectedDataTafiq2 = [];
                            });
                        },
                        error: function (resp) {
                            let message = "silahkan kontak admin"
                            if (resp.responseJSON !== undefined) {
                                message += `Detail. ${resp.responseJSON.message}`
                            }
                            swal("Gagal!", message, "error");
                        }
                    });
                }
            })


        }

        function setDataTafiq2(e) {
            let data = $(e).data()
            let id = data.id;
            if (e.checked) {
                selectedDataTafiq2.push(id)
            } else {
                selectedDataTafiq2 = selectedDataTafiq2.filter((val) => id !== val)
            }
        }

        var columns = [
            {
                data: 'id',
                title: '#',
                searchable: false,
            },
            {
                data: 'id',
                searchable: false,
                orderable: false,
                render: function (data, type, row) {
                    // console.log(row);
                    let selected = "";

                    if (selectedDataTafiq2.includes(data)) {
                        selected = "checked"
                    }

                    return `<div class="text-center">
                        <input
                            oninput="setDataTafiq2(this)"
                            ${selected}
                            class="text-center caang-${row.id}" type="checkbox"
                            value="${row.id}" data-id='${row.id}' />
                        </div>`;
                }
            },
            {
                data: 'anggota.foto',
                title: 'Photo',
                "data": "img",
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    return '<img class="img-caang" height="93px" src="' + row.linkFoto + '">';
                }
            },
            {
                data: 'anggota.npa',
                title: 'NPA'
            },
            {
                data: 'anggota.nama_lengkap',
                title: 'Nama'
            },
            {
                data: 'lokasi',
                title: 'lokasi'
            },
            {
                data: 'anggota.pimpinan_cabang.nama_pc',
                title: 'PC'
            },
            {
                data: 'anggota.pimpinan_daerah.nama_pd',
                title: 'PD'
            },
            {
                data: 'anggota.pimpinan_wilayah.nama_pw',
                title: 'PW'
            },
            {
                data: 'kehadiran',
                title: 'Kehadiran'
            },
            {
                data: 'keaktifan',
                title: 'Keaktifan'
            },
            {
                data: 'makalah',
                title: 'Makalah'
            },
            {
                data: 'presentasi_makalah',
                title: 'Presentasi Makalah'
            },
            {
                data: 'total_nilai',
                title: 'Total',
                orderable: false,
                searchable: false,
            },
            {
                title: 'Status',
                data: 'status',
                orderable: false,
                searchable: false,
                className: 'text-center',
                render: function (_data, _type, row) {
                    let textClass = "";
                    let textLabel = "";
                    if(row.total_nilai > 10 && row.total_nilai < 51){
                        textClass = "badge-danger";
                        textLabel = "Kurang (D)";
                    }else if(row.total_nilai > 50 && row.total_nilai < 71){
                        textClass = "badge-warning"
                        textLabel = "Cukup (C)";
                    }else if(row.total_nilai > 70 && row.total_nilai < 86){
                        textClass = "badge-primary"
                        textLabel = "Baik (B)";
                    }else{
                        textClass = "badge-success"
                        textLabel = "Sangat Baik (A)";
                    }
                    let str =`<span class="badge badge-pill ${textClass}">${textLabel}</span>`;
                    if(row.total_nilai < 10) str = "-";
                    return str;
                },
            },
            {
                data: 'tanggal_tafiq_start',
                title: 'Tanggal Mulai',
                render: function(data, _type, _row) {
                    return moment(data).locale('id').format('LL');
                }
            },
            {
                data: 'tanggal_tafiq_end',
                title: 'Tanggal Selesai',
                render: function(data, _type, _row) {
                    return moment(data).locale('id').format('LL');
                }
            },
            {
                title: 'Aksi',
                data: 'id',
                className: 'text-center',
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    let edit = "";
                    let trash = "";
                    let detail = `<a href="/data-tafiq2/${data}" class="btn btn-info btn-sm">Detail</a>`
                    if(row.can_edit){
                        edit = `<a href="/data-tafiq2/${data}/edit" class="btn btn-warning btn-sm"><i class="si si-pencil"></i></a>`
                    }
                    if(row.can_delete){
                        trash = `<button type="button" class="btn btn-danger btn-sm" onclick="removeTafiqData(\'${data}\')" ><i class="si si-trash"></i></button>`;
                    }
                    let content = `${edit}${trash}`;
                    let container = `<div class="btn-group float-right">${content}</div>`
                    return container;
                },
            }
        ]

        $('#dataTable-tafiq2').on('draw.dt', function () {
            setTimeout(() => {
                const hiddenThead = $('.DTFC_LeftWrapper .DTFC_LeftBodyWrapper .DTFC_LeftBodyLiner .ana-table thead th');
                const realThead = $('.DTFC_LeftWrapper .DTFC_LeftHeadWrapper .ana-table thead th').map((i, el) => {
                    $(hiddenThead[i]).css("min-width", $(el).css("width"));
                    $(hiddenThead[i]).css("max-width", $(el).css("width"));
                });

            }, 0);
        });

        let tableTafiq2 = $('#dataTable-tafiq2').DataTable({
            pageLength: 10,
            lengthMenu: [
                [5, 10, 25, 50, 100, 250, 500, 1000],
                [5, 10, 25, 50, 100, 250, 500, 1000]
            ],
            processing: true,
            serverSide: true,
            columnDefs: [
                {width: 100, targets: 3},
                {width: 200, targets: 4},
                {width: 200, targets: 5},
                {width: 120, targets: 6},
                {width: 120, targets: 7},
                {width: 120, targets: 8},
                {width: 160, targets: 11},
                {width: 160, targets: 12}
            ],
            ajax: {
                url: dataTableUrl,
                data: function (d) {
                    d.status_approval = $('.searchApprovalStatus').val();
                    if (tanggalStart) {
                        d.createdStart = tanggalStart;
                    }
                    if (tanggalEnd) {
                        d.createdEnd = tanggalEnd;
                    }
                }
            },
            // ajax: dataTableUrl,
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                // index number column
                var index = iDisplayIndex + this.fnSettings()._iDisplayStart + 1;
                $('td:eq(0)', nRow).html(index);
                return nRow;
            },
            columns: columns,
        });

        $("#export-data-tafiq").click(function (e) {
            swal({
                title: "Exporting Data...",
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                },
            });

            setTimeout(function () {
                let workbook = new ExcelJS.Workbook();
                workbook.addWorksheet('Data Tafiq PC Export');
                let worksheet = workbook.getWorksheet(1);
                let dataRaw = jQuery('#dataTable-tafiq2')
                    .dataTable()
                    .api()
                    .rows()
                    .data().toArray();

                worksheet.views = [{
                    state: 'frozen',
                    xSplit: 4,
                    ySplit: 1
                }];
                worksheet.getRow(1).height = 22;
                worksheet.getRow(1).alignment = {
                    vertical: 'middle',
                    horizontal: 'center',
                    indent: 1
                };
                worksheet.columns = [{
                    header: 'No',
                    key: 'no',
                    width: 7
                },
                    {
                        header: 'Nama Lengkap',
                        key: 'nama_lengkap',
                        width: 22.20
                    },
                    {
                        header: 'Npa',
                        key: 'npa',
                        width: 22.20
                    },
                    {
                        header: 'Nama PW',
                        key: 'pw',
                        width: 16.63
                    },
                    {
                        header: 'Nama PD',
                        key: 'pd',
                        width: 16.63
                    },
                    {
                        header: 'Nama PC',
                        key: 'pc',
                        width: 16.63
                    }
                ];
                worksheet.getRow(1).eachCell(function (cell, colNumber) {
                    cell.fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: {
                            argb: "077b38"
                        },
                    }
                    cell.font = {
                        name: "Calibri",
                        family: 2,
                        color: {
                            argb: "ffffff"
                        },
                        size: 11,
                    }
                })

                dataRaw.forEach(function (item, index) {
                    worksheet.addRow([
                        index + 1,
                        item.anggota.nama_lengkap,
                        item.anggota.npa,
                        item.anggota.pimpinan_wilayah.nama_pw,
                        item.anggota.pimpinan_daerah.nama_pd,
                        item.anggota.pimpinan_cabang.nama_pc
                    ]);


                    worksheet.getRow(index + 2).height = 80;

                    worksheet.getRow(index + 2).eachCell(function (cell, colNumber) {
                        cell.font = {
                            name: "Calibri",
                            family: 2,
                            color: {
                                argb: "000000"
                            },
                            size: 11,
                        }
                    });
                    worksheet.getRow(index + 2).alignment = {
                        vertical: 'middle',
                        horizontal: 'left',
                        indent: 1
                    };
                })

                workbook.xlsx.writeBuffer({
                    base64: true
                })
                    .then(function (xls64) {
                        // build anchor tag and attach file (works in chrome)
                        var a = document.createElement("a");
                        var data = new Blob([xls64], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        });

                        var url = URL.createObjectURL(data);
                        a.href = url;
                        a.download = "export.xlsx";
                        document.body.appendChild(a);
                        a.click();
                        setTimeout(function () {
                                document.body.removeChild(a);
                                window.URL.revokeObjectURL(url);
                            },
                            0);
                        swal.close();
                    })
                    .catch(function (error) {
                        swal.close();
                    });
            }, 3000);
        });

        $('.searchApprovalStatus').change(function () {
            tableTafiq2.draw();
        });

        $("#tanggal_daftar_start").change(function (ev) {
            const tanggalMulai = moment(ev.target.value);
            tanggalStart = tanggalMulai.format('YYYY-MM-DD 00:00:00')
            tableTafiq2.draw();
        });

        $("#tanggal_daftar_end").change(function (ev) {
            const tanggalSampai = moment(ev.target.value);
            tanggalEnd = tanggalSampai.format('YYYY-MM-DD 00:00:00')
            tableTafiq2.draw();
        });

        $("#reset").click(function () {
            $('.searchApprovalStatus').val('');
            $("#tanggal_daftar_end").val('');
            $("#tanggal_daftar_start").val('')
            tanggalEnd = null;
            tanggalStart = null;
            tableTafiq2.search("").draw();
        });

</script>
@endsection

@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Data Tafiq 2 <small
                    class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Data Tafiq 2</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="/dashboard">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Table <small>Data Tafiq 2</small></h3>
        </div>
        <div class="block-header" style="position: sticky;top: 53px;z-index: 300;background: white;">
            <div class="float-right" role="group" aria-label="Basic example">
                @if(in_array($admin_pc, $role))
                <a href="#" onclick="approveData(this, '{{ $admin_pc }}')" class="btn btn-primary btn-sm mr-2"><span
                        class="si si-check"></span>
                    Approve</a>
                <button id="export-data-tafiq" class="btn btn-success btn-sm" style="margin-right: 5px"><span
                        class="si si-plus"></span> Export Data
                </button>
                @endif
                @if(in_array($admin_pd, $role))
                <a href="#" onclick="approveData(this, '{{ $admin_pd }}')" class="btn btn-primary btn-sm mr-2"><span
                        class="si si-check"></span>
                    Approve</a>
                <button id="export-data-tafiq" class="btn btn-success btn-sm" style="margin-right: 5px"><span
                        class="si si-plus"></span> Export Data
                </button>
                @endif
                @if(in_array($admin_pw, $role))
                <a href="#" onclick="approveData(this, '{{ $admin_pw }}')" class="btn btn-primary btn-sm mr-2"><span
                        class="si si-check"></span>
                    Approve</a>
                <button id="export-data-tafiq" class="btn btn-success btn-sm" style="margin-right: 5px"><span
                        class="si si-plus"></span> Export Data
                </button>
                @endif
                @if(in_array($admin_pp, $role) || in_array($superadmin, $role))
                <a href="#" onclick="approveData(this, 'admin_pp')" class="btn btn-primary btn-sm mr-2"><span
                        class="si si-check"></span>
                    Approve</a>
                <button id="export-data-tafiq" class="btn btn-success btn-sm" style="margin-right: 5px"><span
                        class="si si-plus"></span> Export Data
                </button>
                @endif
                <button id="reset" class="btn btn-success btn-sm" style="margin-right: 5px"><span
                    class="si si-refresh"></span> Reset Filter
                </button>
            </div>
        </div>
        <div class="block-header" id="advance-filter">
            <form class="w-100" id="filterFrom">
                <div style="border: 1px solid #e9e9e9; padding:10px;">
                    <div class="row">
                        <div class="col-auto">
                            <div class="form-group">
                                <label><strong>Tanggal Mulai:</strong></label>
                                <input type="date" class="form-control" id="tanggal_daftar_start"
                                        name="tanggal_daftar_start" required>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="form-group">
                                <label><strong>Tanggal Selesai:</strong></label>
                                <input type="date" class="form-control" id="tanggal_daftar_end"
                                        name="tanggal_daftar_end" required>
                            </div>
                        </div>
                        @if(in_array($admin_pp, $role) || in_array($superadmin, $role))
                        <div class="col-auto">
                            <div class="form-group">
                                <label><strong>Status Approval</strong></label>
                                <select class="form-control searchApprovalStatus" style="width: 200px">
                                    <option value="" selected>-</option>
                                    <option value="INIT">SUBMITTED</option>
                                    <option value="APPROVED_BY_PC">APPROVED BY PC</option>
                                    <option value="APPROVED_BY_PD">APPROVED BY PD</option>
                                    <option value="APPROVED_BY_PW">APPROVED BY PW</option>
                                    <option value="APPROVED_BY_PP">APPROVED BY PP</option>
                                </select>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <div class="block-content block-content-full">
            <div class="table-responsive">
                <table id="dataTable-tafiq2" class="ana-table table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>
                                <div class="text-center">
                                    <input oninput="checkAllData(this)" type="checkbox">
                                </div>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
