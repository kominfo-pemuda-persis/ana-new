@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"
            integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- Page JS Code -->
    <script>
        function getJSONForm(formElement){
            let obj = {};
            let form = $(formElement).serializeArray().filter(function(val, index){
                return !['_token', 'date', 'npa_select'].includes(val.name);
            }).forEach(function(val, index){
                obj[val.name] = val.value
            })
            return obj;
        }

        $("#form-tafiq2").validate({
            rules: {
                lokasi: "required",
                tanggal_tafiq_start: "required",
                tanggal_tafiq_end: "required",
                npa_select: "required",
                date: "required"
            },
            messages: {
                npa_select: "Silahkan isi NPA Anda",
                date: "Silahkan isi Tanggal Tafiq 2 Anda",
                lokasi: "Silahkan isi Lokasi Anda",
                tanggal_tafiq_start: "Silahkan isi Tanggal Mulai Anda",
                tanggal_tafiq_end: "Silahkan isi Tanggal Akhir Anda",
            },
            errorClass: "is-invalid",
            submitHandler: function (form, event) {
                $form = $(form);
                event.preventDefault();

                swal({
                    title: 'Saving data..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    onOpen: () => {
                        swal.showLoading()
                    }
                })

                let formJSON = getJSONForm(form);
                formJSON["_method"] = "PUT";

                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('menyimpan..');


                $.ajax({
                    url: $form.attr('action'),
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    contentType : "application/json",
                    dataType: "json",
                    data: JSON.stringify(formJSON),
                    type: 'POST',
                    success: function () {
                        form.reset();
                        swal({
                            title: "Berhasil!",
                            text: "berhasil update data",
                            type: "success",
                            confirmButtonColor: "#3498DB",
                            closeOnConfirm: true
                        }).then((result) => {
                            button.removeAttr('disabled');
                            button.html('<i class="far fa-save mr-1"></i> Simpan');
                            window.location = '/data-tafiq2';
                        });
                    },

                    error: function (jqXHR, exception) {
                        swal.close();
                        let message = `Terjadi kendala teknis, Mohon kontak Bidang Administrasi PP. <br> info error: <b>${jqXHR.responseJSON.message}</b>`
                        swal("Upload ditolak", message, "error");
                        button.removeAttr('disabled');
                        button.html('<i class="far fa-save mr-1"></i> Simpan');
                    },
                });
            },
            errorPlacement: function (error, element) {
                let name = $(element).attr("name");
                if (name === 'file-excel') {
                    console.log(element.parent().parent().find(".invalid-feedback-file.normal-form").append(error));
                }else if (name === 'foto-upload') {
                    console.log(element.parent().parent().find(".invalid-feedback-file.normal-form").append(error));
                } else {
                    element.parent().find(".invalid-feedback.normal-form").append(error);
                }
            }
        });
        $('#tafiq2-reg-date').flatpickr(
        {
            mode: "range",
            dateFormat: "Y-m-d",
            onChange: function(selectedDates){
                let start = this.formatDate(selectedDates[0], "j F Y")
                let end = "";

                $("#tanggal_tafiq_start").val(this.formatDate(selectedDates[0], "Y-m-d"));

                if(selectedDates.length > 1){
                    end = this.formatDate(selectedDates[1], "j F Y")
                    $("#tanggal_tafiq_end").val(this.formatDate(selectedDates[1], "Y-m-d"));
                }

                $("#tafiq2-reg-date").val(`${start} - ${end}`);
            },
            defaultDate: ["{{ $data->tanggal_tafiq_start }}", "{{ $data->tanggal_tafiq_end }}"],
            onClose: function (selectedDates) {

                if(selectedDates.length <= 0) return;

                let start = this.formatDate(selectedDates[0], "j F Y")
                let end = ""

                if(selectedDates.length < 2){
                    end_tmp = new Date(selectedDates[0]);
                    end_tmp.setDate(selectedDates[0].getDate() + 1);
                    selectedDates[1] = end_tmp;
                    end = this.formatDate(selectedDates[1], "j F Y")
                }else{
                    end = this.formatDate(selectedDates[1], "j F Y")
                }

                $("#tafiq2-reg-date").val(`${start} - ${end}`);
                $("#tanggal_tafiq_start").val(this.formatDate(selectedDates[0], "y-m-d"));
                $("#tanggal_tafiq_end").val(this.formatDate(selectedDates[1], "y-m-d"));
            }
        });

    </script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Edit Data Tafiq 2 <small
                        class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Edit Data Tafiq 2</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Form Edit <small>Data Tafiq 2</small></h3>
            </div>
            <div class="block-content block-content-full">
                <form id="form-tafiq2" action="{{ route("update.tafiq2", $data->id) }}">
                    @csrf
                    <div>
                        <div class="content content-full text-center" style="height: 100vh">
                            <div class="flex flex-row items-center mb-5">
                                <div class="col-xl-6 col-lg-6 col-sm-12 mx-auto text-left">
                                    <div class="form-group">
                                        <label for="npa">NPA</label>
                                        <input value="{{ $data->npa }}" readonly class="form-control" type="text" id="npa" name="npa"
                                               placeholder="Input NPA" required>
                                        <div class="invalid-feedback normal-form">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lokasi">Lokasi</label>
                                        <input value="{{ $data->lokasi }}" class="form-control" type="text" id="lokasi" name="lokasi"
                                               placeholder="Input lokasi" required>
                                        <div class="invalid-feedback normal-form">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lokasi">Kehadiran</label>
                                        <input value="{{ $data->kehadiran }}" class="form-control" type="number" id="kehadiran" name="kehadiran"
                                               placeholder="Nilai kehadiran">
                                        <div class="invalid-feedback normal-form">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lokasi">Keaktifan</label>
                                        <input value="{{ $data->keaktifan }}" class="form-control" type="number" id="keaktifan" name="keaktifan"
                                               placeholder="Nilai Keaktifan">
                                        <div class="invalid-feedback normal-form">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lokasi">Makalah</label>
                                        <input value="{{ $data->makalah }}" class="form-control" type="number" id="makalah" name="makalah"
                                               placeholder="Nilai Makalah">
                                        <div class="invalid-feedback normal-form">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lokasi">Presentasi Makalah</label>
                                        <input value="{{ $data->presentasi_makalah }}" class="form-control" type="number"
                                        id="presentasi_makalah" name="presentasi_makalah"
                                               placeholder="Nilai Makalah">
                                        <div class="invalid-feedback normal-form">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="anggota-reg-date">Tanggal Tafiq 2</label>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Select date" type="date" name="date" id="tafiq2-reg-date" required>
                                            <div class="invalid-feedback normal-form">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input class="form-control" type="hidden" id="tanggal_tafiq_start" name="tanggal_tafiq_start"
                                    required>
                            <input class="form-control" type="hidden" id="tanggal_tafiq_end" name="tanggal_tafiq_end"
                                    required>
                            <span class="m-2 d-inline-block">
                                <button class="btn btn-success px-4 py-2" type="submit">
                                    <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Submit
                                </button>
                            </span>
                        </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
