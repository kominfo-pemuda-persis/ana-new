@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2.min.css') }}">

@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/locales.js') }}"></script>
    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ route('anggotaMutasi.datatables') }}";
        let addUrl = "{{ route('anggotaMutasi.store') }}";
    </script>
    <script src="{{ asset('js/pages/pg_anggota_mutasi.js?') . Str::random(25)  }}"></script>
@endsection

@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Data Mutasi <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Data Anggota</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="/dashboard">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Table <small>Mutasi Anggota</b></small></h3>
            @if(Permission::checkPermission('mutasi.create'))
                <button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top"
                        title="Tambah Anggota Mutasi" onclick="openModal('#modal-form','add')">
                    <span class="si si-plus"></span> Tambah</button>
            @endif
        </div>
        <div class="block-content block-content-full">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-anggota-mutasi">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">#</th>
                            <th class="d-none d-sm-table-cell" >Anggota</th>
                            <th class="d-none d-sm-table-cell" >Asal</th>
                            <th class="d-none d-sm-table-cell" >Tujuan</th>
                            <th class="d-none d-sm-table-cell" >Tanggal Mutasi</th>
                            <th class="text-center" style="width: 80px;">#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->

    <!-- Fade In Block Modal -->
<div class="modal fade" id="modal-form" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title modal-title">Modal Title</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form class="form-program" action="{{ route('anggotaMutasi.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="block-content font-size-sm row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="anggota">Data Anggota <span class="text-danger">*</span></label>
                                <br>
                                <select id="anggota" class="picker form-control" name="id_anggota" required></select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="asal">Asal <span class="text-danger">*</span></label>
                                <select id="asal" class="pc_asal form-control" name="asal" required></select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tujuan">Tujuan <span class="text-danger">*</span></label>
                                <select id="tujuan" class="pc_tujuan form-control" name="tujuan" required></select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tanggal_mutasi">Tanggal Mutasi <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" id="tanggal_mutasi" name="tanggal_mutasi" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="progress push progress-add" style="display:none">
                                <div style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-upload-rec progress-bar progress-bar-striped progress-bar-animated bg-info">0%</div>
                            </div>
                        </div>
                    </div>

                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-success" id="btn-form" ><i class="far fa-save mr-1"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Fade In Block Modal -->
@endsection
