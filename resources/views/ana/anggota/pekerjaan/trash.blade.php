@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ route('anggotaPekerjaan.trash.datatables', $id_anggota) }}";
        let columns;
    </script>
    <script src="{{ asset('js/pages/base_datatables.js') }}"></script>
    <script src="{{ asset('js/pages/pg_anggota_pekerjaan.js') }}"></script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data Anggota Pekerjaan <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400
                    text-muted">List</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Data Anggota Pekerjaan</li>
                        <li class="breadcrumb-item">Dashboard
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('anggotaPekerjaan.trash', $id_anggota)}}">Trash</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Table Sampah <small>Data Anggota Pekerjaan</small></h3>
                <a href="{{route('anggotaPekerjaan.index', $id_anggota)}}" class="btn btn-danger btn-sm"
                   style="margin-right: 5px"><span
                        class="fa fa-arrow-left"></span> Kembali</a>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-anggota-pekerjaan-trash">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th class="d-none d-sm-table-cell">Pekerjaan</th>
                        <th>Tahun Mulai & Selesai</th>
                        <th>Alamat</th>
                        <th>keterangan</th>
                        <th class="text-center" style="width: 80px;">#</th>

                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
@endsection
