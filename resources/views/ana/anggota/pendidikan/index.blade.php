@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ route('anggotaPendidikan.datatables', $anggota->id_anggota) }}";
        let addUrl = "{{ route('anggotaPendidikan.store') }}";
    </script>
    <script src="{{ asset('js/pages/pg_anggota_pendidikan.js') }}"></script>
    <script>
        $(function () {
            $(document).on('focusin', '.datepicker', function(){
                $(this).datepicker({
                    format: "yyyy",
                    viewMode: "years",
                    minViewMode: "years"
                });
            });
        })
    </script>
@endsection

@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Data Pendidikan <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Data Pendidikan</li>
                    <li class="breadcrumb-item"> <a class="link-fx" href="/anggota">Data Anggota</a></li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="/dashboard">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Table <small>Pendidikan - Anggota : <b>{{$anggota->nama_lengkap}}</b></small></h3>
            <button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Tambah Anggota Pendidikan" onclick="openModal('#modal-form','add')"><span class="si si-plus"></span> Tambah</button>
        </div>
        <div class="block-content block-content-full">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-anggota-pendidikan">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">#</th>
                            <th class="d-none d-sm-table-cell" >Pendidikan</th>
                            <th>Instasi</th>
                            <th>Jurusan</th>
                            <th>Tahun Masuk & Keluar</th>
                            <th>Jenis Pendidikan</th>
                            <th class="text-center" style="width: 80px;">#</th>

                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->

    <!-- Fade In Block Modal -->
<div class="modal fade" id="modal-form" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title modal-title">Modal Title</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form class="form-program" action="{{ route('anggotaPendidikan.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="block-content font-size-sm row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="id_master_pendidikan">Jenjang <span class="text-danger">*</span></label>
                                <select class="form-control" id="id_master_pendidikan" name="id_master_pendidikan" required>
                                    <option value="">Pilih Status</option>
                                    @foreach( $jenjang as $iterator )
                                        <option value="{{ $iterator->id_tingkat_pendidikan }}">{{ $iterator->pendidikan }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="instansi">Nama Instansi <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="instansi" name="instansi" required>
                                <input type="hidden" name="id_anggota" value="{{ $anggota->id_anggota }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="jurusan">Jurusan</label>
                                <input type="text" class="form-control" id="jurusan" name="jurusan" >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="jenis_pendidikan">Jenis Pendidikan </label>
                                <select class="form-control" id="jenis_pendidikan" name="jenis_pendidikan" require>

                                    <option value="Formal">Formal</option>
                                    <option value="Non Formal">Non Formal</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tahun_masuk">Tahun Masuk <span class="text-danger">*</span></label>
                                <input class="form-control datepicker" id="tahun_masuk" name="tahun_masuk" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tahun_keluar">Tahun Keluar <span class="text-danger">*</span></label>
                                <input class="form-control datepicker" id="tahun_keluar" name="tahun_keluar" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="progress push progress-add" style="display:none">
                                <div style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-upload-rec progress-bar progress-bar-striped progress-bar-animated bg-info">0%</div>
                            </div>
                        </div>
                    </div>

                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-success" id="btn-form" ><i class="far fa-save mr-1"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Fade In Block Modal -->
@endsection
