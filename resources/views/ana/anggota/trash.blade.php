@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ route('anggota.trash.datatables') }}";
        jQuery('.js-dataTable-anggota-trash').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            order: [[0, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1, 4] }],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            processing: true,
            serverSide: false,
            ajax: dataTableUrl,
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                // index number column
                var index = iDisplayIndex + this.fnSettings()._iDisplayStart +1;
                $('td:eq(0)',nRow).html(index);
                return nRow;
            },
            columns: [
                { data: 'DT_RowIndex' },
                {
                    data: 'id_anggota',
                    render: function (data, type, row) {
                        return row.npa;
                    }
                },
                { data: 'nama_lengkap' },
                {
                    render: function (data, type, row) {
                        return row.pimpinan_cabang.nama_pc;
                    }
                },
                {
                    data: 'status_Aktif',
                    render: function (data, type, row) {
                        let str;
                        if (data === 'Aktif') {
                            str = '<span class="badge badge-success">Aktif</span>';
                        } else {
                            str = '<span class="badge badge-danger">Non Aktif</span>';
                        }
                        return str;
                    }
                },
                {
                    data: 'id_anggota',
                    render: function (data, type, row) {
                        let str = `<button type="button" class="btn btn-primary btn-sm" onclick="restore(\'${data}\')" ><i class="fa fa-recycle"></i></button>
                        <button type="button" class="btn btn-danger btn-sm" onclick="destroy(\'${data}\')" ><i class="fa fa-trash"></i></button>`;
                        return str;
                    },
                }
            ],
            autoWidth: true,
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        })
    </script>
    <script src="{{ asset('js/pages/pg_anggota_index.js') }}"></script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data Anggota <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Data Anggota</li>
                        <li class="breadcrumb-item">Dashboard
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('anggota.trash')}}">Trash</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Table Sampah <small>Data Anggota</small></h3>
                <a href="{{route('anggota.index')}}" class="btn btn-danger btn-sm" style="margin-right: 5px"><span class="fa fa-arrow-left"></span> Kembali</a>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-anggota-trash">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">#</th>
                            <th style="width: 20%;">NPA</th>
                            <th class="d-none d-sm-table-cell" >Nama</th>
                            <th class="d-none d-sm-table-cell" >Nama PC</th>
                            <th style="width: 15%;">Status</th>
                            <th class="text-center" style="width: 120px;">#</th>

                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
@endsection
