@extends('layouts.backend')


@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/additional-methods.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script>
        var toList= {!! Permission::checkPermission('anggota.read_pw') ||
                        Permission::checkPermission('anggota.read_pd') ||
                        Permission::checkPermission('anggota.read_pc') ||
                        Permission::checkPermission('anggota.read_all') ? '"/anggota"' : 'window.location.href' !!}
        $(document).ready(function () {
            var dataTafiq = {!! $anggota->tafiq !!};
            $('.level_tafiq').val('{{$anggota->level_tafiq}}'?'{{$anggota->level_tafiq}}':0);
            if(dataTafiq.length > 0){
                $.each(dataTafiq,function (i,obj) {
                    addTafiq(obj);
                });
            }
        });
    </script>

    <script src="{{ asset('js/pages/form_wizard_anggota.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/pg_anggota.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_tafiq.js?id=') . Str::random(25) }}"></script>

@endsection

@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Data Tafiq <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Data Anggota Tafiq</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="/anggota">Data Anggota</a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="/dashboard">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Form <small>Tafiq (Tazwidul Fityanil Quran) - Anggota : <b>{{$anggota->nama_lengkap}}</b></small></h3>
        </div>
        <div class="block-content block-content-full">
            <hr>
            <br>
            <form class="js-wizard-anggota-form" action="{{ route('anggotaTafiq.update', $anggota->id_anggota) }}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="level" class="control-label">Level TAFIQ</label>
                    <select name="level_tafiq" class="form-control level_tafiq">
                        <option value="0">Belum Tafiq</option>
                        <option value="1">Tafiq I</option>
                        <option value="2">Tafiq II</option>
                        <option value="3">Tafiq III</option>
                    </select>
                </div>
                <table class="table table-striped table-bordered" id="tafiq_table" >
                    <thead>
                    <tr>
                        <th  style="width:5%">No</th>
                        <th  >Form</th>
                        <th  style="width:5%">Action</th>
                    </tr>
                    </thead>
                    <tbody >
                    </tbody>


                </table>
                <div class="block-content block-content-full text-right border-top">
                    <a class="btn btn-sm btn-light" href="/anggota">Close</a>
                    <button type="submit" class="btn btn-sm btn-success" id="btn-form" ><i class="far fa-save mr-1"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>


<div >

</div>


@endsection
