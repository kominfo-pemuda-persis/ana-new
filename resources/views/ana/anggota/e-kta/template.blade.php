<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href="{{ asset('/css/oneui.css') }}">
    <style>
    #mail {
        color: black !important;
        width: 20cm;
        min-height: 12.8cm;
        background: white;
        padding: 0cm 0cm !important;
        position: absolute;
        font-size: 12 !important;
    }

    #mail .row {
        padding-top: 0cm !important;
    }

    .imgHeader {
        position: absolute;
        top: 0;
        right: 0;
        left: 0
    }

    .btn-download {
        margin-top: 25px;
    }
    </style>
</head>
<body>
<div id="mail" onclick="downloadEkta()"
     style="padding: 50px 70px; font-family:'Times New Roman', Times, serif; text-align:justify;line-height: 1 !important;">
    <div class="row">
        <img class="imgHeader" src="{{ asset('images/kta/front.png') }}" width="100%" alt="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="img-photo float right" style="margin-top: 137px;margin-right: 81px;float: right;">
                    @if(!$anggota->foto || $anggota->foto=="default.png")
                    <img id="preview-image" alt="{{$anggota->nama_lengkap}}" alt="preview image" style="width: 131px; border-radius: 25px;margin-right: 6px;margin-top: 45px;" src="{{ asset('images/anggota/default.png') }}" >
                    @else
                    <img id="preview-image" alt="{{$anggota->nama_lengkap}}" alt="preview image" style="width: 131px; border-radius: 25px;margin-right: 6px;margin-top: 45px;" src="{{ Storage::disk('s3')->url('images/anggota/'.$anggota->foto) }}">
                    @endif
                </div>

                <h2 class="font-w600" style="padding-top: 161px;margin-left: 35px;font-size: 18px;color: #fdd659 !important;
                ">{{$anggota->nama_lengkap}}</h2>

                <h2 class="font-w600" style="margin-top: -45.5px;margin-left: 399px;font-size: 18px;color: #fdd659 !important;
                ">{{$anggota->npa}}</h2>

                <h2 class="font-w600"
                    style="padding-top: 7px; margin-left: 35px; font-size: 18px;color: #fdd659 !important;">{{$anggota->tempat_lahir}}
                    , {{$anggota->tanggal_lahir === null ? "-" : $anggota->tanggal_lahir->format('d F Y')}}</h2>

                <h2 class="font-w600"
                    style="padding-top: 5px; margin-left: 35px; font-size: 18px;color: #fdd659 !important; position:absolute; z-index: 6;">{{ $anggota->pekerjaan }}</h2>

                <h2 class="font-w600"
                    style="padding-top: 50px; margin-left: 35px; font-size: 18px;color: #fdd659 !important;">{{ substr($anggota->alamat, 0, 30) }}
                    <br>{{ substr($anggota->alamat, 30, 60) }}<br>{{ substr($anggota->alamat, 60) }}<br></h2>

                <h2 class="font-w600" style="padding-top: 20px;margin-left: 35px;position: absolute;font-size: 14px;">
                    Berlaku sampai {{ date('d-m-Y', strtotime($anggota->masa_aktif_kta)) }}</h2>

                <div class="text-center">
                    <h2 class="float-right"
                        style="margin-right: 90px; margin-top: -68px; font-size: 11px; color: #ffffff !important;">
                        Pimpinan Pusat <br>Pemuda Persatuan Islam
                        <br><br><br><br><br><br> <span style="text-decoration-line: underline;">Ibrahim Nasrul Haq AlFahmi</span>
                        <br>NPA: 09.2830</h2>
                    <img style="margin-top: -45px;   margin-right: -135px;"
                         src="{{ asset('images/kta/ttd_ketum_white.png') }}" class="float-right" width="20%" alt="">
                </div>
            </div>
    </div>
</div>
<button class="btn btn-default btn-download text-center" onclick="downloadEkta()"><i class="si si-cloud-download"></i>
    Download
</button>
</body>
<!-- OneUI Core JS -->
<script src="{{ asset('js/oneui.app.js') }}"></script>

<!-- Laravel Scaffolding JS -->
<script src="{{ asset('js/laravel.app.js') }}"></script>

<script src="{{ asset('/js/plugins/html2canvas/html2canvas.js') }}"></script>

<script>
@if($anggota->pimpinanWilayah && $anggota->pimpinanDaerah && $anggota->pimpinanCabang && $anggota->nama_pj)
$.ajax({
    type: "GET",
    url: "/anggota/photo/{{$anggota->id_anggota}}",
    success: function (data) {
        document.getElementById('preview-image')
        .setAttribute('src', data);
    }
 });
 @endif
 function b64toBlob(b64Data, contentType, sliceSize) {
     contentType = contentType || '';
     sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
}


function downloadEkta() {
    let element = document.getElementById("mail"); // global letiable
    // Generate screenshot and download
    html2canvas(element).then(function(canvas) {
    // Generate the base64 representation of the canvas
        let a = document.createElement('a');
            a.href = canvas.toDataURL("image/png");
            a.download= "EKTA-"+{{ $anggota->npa }}+".png";
            a.click();
    });
}

</script>
</html>
