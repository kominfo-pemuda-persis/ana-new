@extends('layouts.backend')

@section('js_after')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"
            integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script>
        function showHidePassword(el){
            let button = $(el);
            let input = button.parent().parent().find("input");
            let icon = button.find("i");
            let attr = input.attr("type")
            if(attr === "password"){
                input.attr("type", "text");
                icon.addClass("fa-eye")
                icon.removeClass("fa-eye-slash")
            }else{
                input.attr("type", "password");
                icon.addClass("fa-eye-slash")
                icon.removeClass("fa-eye")
            }
        }

        $(document).ready(() => {
            $("#form-password").validate({
                rules: {
                    current_password: {
                        required: true
                    },
                    new_password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_new_password: {
                        required: true,
                        equalTo: "input[name=new_password]"
                    },
                },
                messages: {
                    current_password: "Silahkan isi kata sandi saat ini",
                    new_password: {
                        required: "Silahkan isi kata sandi baru",
                        minlength: "Minimal password 6 karakter"
                    },
                    confirm_new_password: {
                        required: "Silahkan isi konfirmasi kata sandi baru",
                        equalTo: "Konfirmasi password tidak sama"
                    },
                },
                errorClass: "is-invalid",
                submitHandler: function (form, event) {
                    $form = $(form);
                    event.preventDefault();
                    swal({
                        title: 'Updating Password...',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    })

                    let button = $form.find('button[type="submit"]');
                    let buttonClone = button.clone();

                    button.attr('disabled', 'disabled');
                    button.text('menyimpan..');


                    $.ajax({
                        url: $form.attr('action'),
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        processData: false,
                        contentType : false,
                        data: new FormData(form),
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        success: function (res) {
                            form.reset();
                            swal({
                                title: "Berhasil!",
                                text: "Kata sandi Anda berhasil diperbaharui",
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                $(".submit-container").html(buttonClone)
                                // window.location.reload();
                            });
                        },

                        error: function (jqXHR, exception) {
                            swal.close();
                            let message = `Terjadi kendala teknis, Mohon kontak Bidang Administrasi PP. <br> info error: <b>${jqXHR.responseJSON.message}</b>`
                            swal("Upload ditolak", message, "error");
                            $(".submit-container").html(buttonClone);
                        },
                    });
                },
                errorPlacement: function (error, element) {
                    element.parent().find(".invalid-feedback.normal-form").append(error);
                }
            })
        })
    </script>
@endsection
@section('css_after')
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection
@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Form Ganti Password
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Form Ganti Password</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="bg-white overflow-scroll">
        <form id="form-password" action="{{ route('anggota.update-password')}}">
            @csrf
            <div>
                <div class="content content-full text-center">
                    <div class="flex flex-row items-center pt-6">
                        <h3 class="pb-2">Form Ganti Password</h3>
                        <div class="col-xl-6 col-lg-6 col-sm-12 mx-auto text-left">
                            <div class="form-group">
                                <div class="row text-left">
                                    <label for="current_password">Kata Sandi Saat Ini</label>
                                    <div class="input-group mb-4">
                                        <input type="password" class="form-control form-control-alt form-control-lg" name="current_password" placeholder="Kata Sandi Saat Ini">
                                        <div class="input-group-append">
                                            <button onclick="showHidePassword(this)" class="btn btn-primary outline-none" type="button">
                                                <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                        <div class="invalid-feedback normal-form"></div>
                                    </div>
                                    <label for="new_password">Kata Sandi Baru</label>
                                    <div class="input-group mb-4">
                                        <input type="password" class="form-control form-control-alt form-control-lg" name="new_password" placeholder="Kata Sandi Baru">
                                        <div class="input-group-append">
                                            <button onclick="showHidePassword(this)" class="btn btn-primary outline-none" type="button">
                                                <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                        <div class="invalid-feedback normal-form"></div>
                                    </div>
                                    <label for="confirm_new_password">Konfirmasi Kata Sandi Baru</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control form-control-alt form-control-lg" name="confirm_new_password" placeholder="Konfirmasi Kata Sandi Baru">
                                        <div class="input-group-append">
                                            <button onclick="showHidePassword(this)" class="btn btn-primary outline-none" type="button">
                                                <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                        <div class="invalid-feedback normal-form"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="submit-container m-2 d-inline-block pb-6">
                        <button class="btn btn-success px-4 py-2" type="submit">
                            <i class="fa fa-fw fa-save mr-1"></i> Ganti Password
                        </button>
                    </span>
                </div>
        </form>
    </div>
    </div>
    <!-- END Page Content -->
@endsection
