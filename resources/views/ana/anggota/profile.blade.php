@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/cropperjs/cropper.min.css') }}">
    <style>
        .preview {
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
        }

        .modal-lg {
            max-width: 1000px !important;
        }
    </style>
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/cropperjs/cropper.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>
        var updateFotoUrl = '{{ route('anggota.editFotoProfile', $anggota->id_anggota) }}';
    </script>
    <script src="{{ asset('js/pages/pg_anggota_profile.js?id=') . Str::random(25) }}"></script>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-V33JPYB1SF"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-V33JPYB1SF');
    </script>

@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 font-w300 my-2">
                    Profil Anggota : {{$anggota->npa}} - {{$anggota->nama_lengkap}}
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Profil Anggota</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h2 class="block-title text-center">Detail Anggota<small></small></h2>
            </div>
            <div class="block-content block-content-full">
            <section>
                <div class="row">
                    <div class="col-md-12 text-center">
                        @if(!$anggota->foto || $anggota->foto=="default.png")
                        <img id="preview-image" alt="{{$anggota->nama_lengkap}}" class="photo-profile" src="{{ asset('images/anggota/default.png') }}" >
                        @else
                        <img id="preview-image" alt="{{$anggota->nama_lengkap}}" class="photo-profile" src="{{ Storage::disk('s3')->url('images/anggota/'.$anggota->foto) }}">
                        @endif
                        <div class="d-flex justify-content-center mt-2">
                            <input type="file" name="foto-upload" id="foto-upload" class="d-none">
                            <button class="btn btn-warning btn-lg" onClick="$('#foto-upload').click();">
                                <i class="si si-pencil mr-2"></i> Update Foto Profile
                            </button>
                        </div>
                    </div>
                </div>
            </section>
            <hr>
                <section id="data-anggota">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3"><h3 class="block-title">Data Anggota<small></small></h3></td>
                                    </tr>
                                    <tr>
                                        <td width="30%">NPA</td>
                                        <td width="1%">:</td>
                                        <td>{{$anggota->npa}}</td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Role</td>
                                        <td width="1%">:</td>
                                        @if(!is_null($roles))
                                            <td>@foreach($roles as $role) {{$role->display_name}} @endforeach</td>
                                        @else
                                            <td>-</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td width="15%">Nama Lengkap</td>
                                        <td width="1%">:</td>
                                        <td>{{$anggota->nama_lengkap}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tempat Lahir</td>
                                        <td>:</td>
                                        <td>{{$anggota->tempat_lahir}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:</td>
                                        <td>{{$anggota->tanggal_lahir === null ? "-" : $anggota->tanggal_lahir->format('d F Y')}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td>{{$anggota->status_merital}}</td>
                                    </tr>
                                    <tr>
                                        <td>Golongan Darah</td>
                                        <td>:</td>
                                        <td>{{$anggota->gol_darah}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><h3 class="block-title">Kontak<small></small></h3></td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Email</td>
                                        <td width="1%">:</td>
                                        <td>{{$anggota->email}}</td>
                                    </tr>
                                    <tr>
                                        <td width="15%">Nomor Telepon</td>
                                        <td width="1%">:</td>
                                        <td>{{$anggota->no_telpon}}</td>
                                    </tr>
                                    <tr>
                                        <td width="15%">Nomor Telepon Lain</td>
                                        <td width="1%">:</td>
                                        <td>{{$anggota->no_telpon2}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3"><h3 class="block-title">Data Jamiyyah & Alamat<small></small></h3></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{$anggota->alamat}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pimpinan Jama'ah</td>
                                        <td>:</td>
                                        <td>{{$anggota->nama_pj}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pimpinan Cabang</td>
                                        <td>:</td>
                                        <td>{{$anggota->pimpinanCabang->nama_pc}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pimpinan Daerah</td>
                                        <td>:</td>
                                        <td>{{$anggota->pimpinanDaerah->nama_pd}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pimpinan Wilayah</td>
                                        <td>:</td>
                                        <td>{{$anggota->pimpinanWilayah->nama_pw}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pendaftaran Anggota</td>
                                        <td>:</td>
                                        <td>{{$anggota->reg_date->format('d F Y')}}</td>
                                    </tr>
                                    <tr>
                                        <td>Masa Aktif KTA</td>
                                        <td>:</td>
                                        <td>{{$anggota->masa_aktif_kta === null ? "-" : $anggota->masa_aktif_kta->format('d F Y') ?? "-"}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="d-flex justify-content-center">
                        <a href="/anggota/{{$anggota->id_anggota}}/edit-profile" class="btn btn-warning btn-lg">
                            <i class="si si-pencil mr-2"></i> Update Data Diri
                        </a>
                    </div>
                </section>
                <section id="data-keluarga">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Keluarga<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Hubungan</th>
                                        <th>Alamat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($anggota->keluarga)> 0)
                                    @foreach ($anggota->keluarga as $keluarga)
                                        <tr>
                                            <td>{{ $keluarga->nama_keluarga }}</td>
                                            <td>{{ $keluarga->hubungan }}</td>
                                            <td>{{ $keluarga->alamat }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3"  align="center">Tidak ada data!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-pendidikan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Pendidikan<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Instansi</th>
                                        <th>Jurusan</th>
                                        <th>Tahun</th>
                                        <th>Jenis</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($anggota->pendidikan)>0)
                                    @foreach ($anggota->pendidikan as $pendidikan)
                                        <tr>
                                            <td>{{ $pendidikan->instansi }}</td>
                                            <td>{{ $pendidikan->jurusan }}</td>
                                            <td>{{ $pendidikan->tahun_masuk }} s/d {{$pendidikan->tahun_keluar}}</td>
                                            <td>{{ $pendidikan->jenis_pendidikan }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4"  align="center">Tidak ada data!</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-keterampilan">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Keterampilan/Keahlian<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($anggota->keterampilan)>0)
                                    @foreach ($anggota->keterampilan as $keterampilan)
                                        <tr>
                                            <td>{{ $keterampilan->keterampilan }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td align="center">Tidak ada data!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-training">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Training<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Penyelengara</th>
                                        <th>Tempat</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($anggota->training)>0)
                                    @foreach ($anggota->training as $training)
                                        <tr>
                                            <td>{{ $training->nama_training }}</td>
                                            <td>{{ $training->penyelenggara }}</td>
                                            <td>{{ $training->tempat }}</td>
                                            <td>{{ $training->tanggal_mulai }} s/d {{ $training->tanggal_selesai }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" align="center">Tidak ada data!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-organisasi">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Organisasi<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Tahun/Masa jabatan</th>
                                        <th>Tingkat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($anggota->organisasi)>0)
                                    @foreach ($anggota->organisasi as $organisasi)
                                        <tr>
                                            <td>{{ $organisasi->nama_organisasi }}</td>
                                            <td>{{ $organisasi->jabatan }}</td>
                                            <td>{{ $organisasi->tahun_mulai }} s/d {{$organisasi->tahun_selesai }}</td>
                                            <td>{{ $organisasi->tingkat }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" align="center">Tidak ada data!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="data-organisasi">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless">
                                <tr>
                                    <td colspan="3"><h3 class="block-title">Data Tafiq<small></small></h3></td>
                                </tr>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Lokasi</th>
                                        <th>Dari Tanggal</th>
                                        <th>Sampai Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($anggota->tafiq)>0)
                                    @foreach ($anggota->tafiq as $tafiq)
                                        <tr>
                                            <td>{{ $tafiq->lokasi }}</td>
                                            <td>{{ $tafiq->tanggal_masuk->format('d F Y') }}</td>
                                            <td>{{ $tafiq->tanggal_selesai->format('d F Y') }} </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" align="center">Tidak ada data!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <div >
                <a href="/anggota/{{$anggota->id_anggota}}/edit" class="btn btn-warning btn-lg btn-block">Update Profile Data <i class="si si-pencil"></i></a>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>

    <!-- Modal Croper image -->
    <div class="modal fade" id="modal-crop" data-backdrop="static" data-keyboard="false" tabindex="-1"  aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Pengaturan Gambar</h5>
                    <button id="cancel-crop-close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <div id="crop-section" class="row">
                            <div class="col-md-8 text-center" style="background: #1b1e21">
                                <div style="min-height: 20px;max-height: 430px;width: 324px;margin: auto;">
                                    <img id="image-crop" style="object-fit: contain" src="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="cancel-crop" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="save-foto">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
