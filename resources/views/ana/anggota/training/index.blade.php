@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ route('anggotaTraining.datatables', $anggota->id_anggota) }}";
        let addUrl = "{{ route('anggotaTraining.store') }}";
    </script>
    <script src="{{ asset('js/pages/pg_anggota_training.js') }}"></script>
@endsection

@section('content')
<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
                Data Training <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
            </h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">Data Anggota</li>
                    <li class="breadcrumb-item" aria-current="page">
                        <a class="link-fx" href="/dashboard">Dashboard</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table with Export Buttons -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Table <small>Training - Anggota : <b>{{$anggota->nama_lengkap}}</b></small></h3>
            <a href="{{ route('anggotaTraining.trash', $anggota->id_anggota) }}" class="btn btn-danger btn-sm"
                style="margin-right:
             5px"><span
                     class="si si-trash"></span> Sampah</a>
            <button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Tambah Anggota Training" onclick="openModal('#modal-form','add')"><span class="si si-plus"></span> Tambah</button>
        </div>
        <div class="block-content block-content-full">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-anggota-training">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">#</th>
                            <th class="d-none d-sm-table-cell" >Nama</th>
                            <th class="d-none d-sm-table-cell" >Penyelenggara</th>
                            <th class="d-none d-sm-table-cell" >Tempat</th>
                            <th class="d-none d-sm-table-cell" >Waktu Training</th>
                            <th class="text-center" style="width: 80px;">#</th>

                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table with Export Buttons -->
</div>
<!-- END Page Content -->

    <!-- Fade In Block Modal -->
<div class="modal fade" id="modal-form" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title modal-title">Modal Title</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form class="form-program" action="{{ route('anggotaTraining.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="block-content font-size-sm row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nama_training">Nama Training <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="nama_training" name="nama_training" required>
                                <input type="hidden" name="id_anggota" value="{{ $anggota->id_anggota }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="penyelenggara">Penyelenggara <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="penyelenggara" name="penyelenggara" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tempat">Tempat <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="tempat" name="tempat" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tanggal_mulai">Tanggal Mulai <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" id="tanggal_mulai" name="tanggal_mulai" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tanggal_selesai">Tanggal Selesai <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" id="tanggal_selesai" name="tanggal_selesai" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="progress push progress-add" style="display:none">
                                <div style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-upload-rec progress-bar progress-bar-striped progress-bar-animated bg-info">0%</div>
                            </div>
                        </div>
                    </div>

                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-success" id="btn-form" ><i class="far fa-save mr-1"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Fade In Block Modal -->
@endsection
