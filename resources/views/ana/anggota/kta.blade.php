@extends('layouts.backend')

@section('css_before')
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
<style>
    #mail {
        color: black !important;
        width: 20cm;
        min-height: 25.6cm;
        background: white;
        padding: 0cm 0cm !important;
        position: relative;
        font-size: 12 !important;
    }

    #mail .row {
        padding-top: 0cm !important;
    }

    .imgHeader {
        position: absolute;
        top: 0;
        right: 0;
        left: 0
    }

    .imgBack {
        position: absolute;
        top: 486px !important;
        right: 0;
        left: 0
    }


    .btn-download {
        margin-top: 25px;
    }

    @keyframes zoom-effect {
        0% {
            transform: scale(1) rotate(0deg);
        }

        100% {
            transform: scale(4) rotate(360deg);
        }
    }
</style>
@endsection

@section('js_after')
<!-- Page JS Plugins -->
<!-- Page JS Plugins -->
<script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/js/plugins/html2canvas/html2canvas.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>
<script src="{{ asset('/js/plugins/html2pdf/dist/html2pdf.bundle.js') }}"></script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-V33JPYB1SF"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-V33JPYB1SF');
</script>

@php
    $profileAnggota = $anggota;
    $photoProfile = asset('images/anggota/default.png');
    if(!$profileAnggota->foto || $profileAnggota->foto=="default.png") {
        $photoProfile=asset('images/anggota/default.png');
    }else{
        $photoProfile=Storage::disk('s3')->url('images/anggota/'.$profileAnggota->foto);

        try {
            // Mengambil konten gambar dari URL dan mengonversi menjadi base64
            $imageContent = file_get_contents($photoProfile);
            $base64Image = 'data:image/' . pathinfo($photoProfile, PATHINFO_EXTENSION) . ';base64,' . base64_encode($imageContent);
            $photoProfile = $base64Image; // Mengganti URL dengan Base64
        } catch (\Exception $e) {
            // Jika gagal membaca dari S3, gunakan URL asli sebagai fallback
            $photoProfile = $photoProfile;
        }
    }
@endphp

<!-- Page JS Code -->
<script>
    $('#page-footer').hide();
    $(function() {
        $('#page-header').hide();
        let dataFoto = '';

        $('#page-container').addClass('sidebar-mini');
        let foto = "{{$anggota->foto}}";
        if (foto !== "default.png") {
            $.ajax({
                type: "GET",
                url: "/anggota/photo/{{$anggota->id_anggota}}",
                success: function(data) {
                    dataFoto = data;
                    $("#img-profile").css('background-image', 'url(' + data + ')');
                }
            });
        }else{
            $("#img-profile").css('background-image', 'url({{asset('images/anggota/default.png')}})');
        }
    });

    async function downloadEkta(type) {
        const npm = "{{ $anggota->npa }}";
        const element = document.getElementById("mail");

        if (!element) {
            console.error("Element with ID 'mail' tidak ditemukan");
            return;
        }

        if (type === 'image') {
            try {
                // Render elemen ke kanvas
                const canvas = await html2canvas(element, {
                    useCORS: true, // Mengizinkan pemuatan gambar dari domain lain (S3)
                    allowTaint: false, // Tidak mengizinkan gambar tanpa izin lintas domain
                    logging: true,
                    scale: 2, // Resolusi lebih tinggi
                    backgroundColor: "#ffffff", // Warna latar belakang
                    imageTimeout: 15000, // Timeout gambar
                });

                // Konversi kanvas ke blob
                const blob = await new Promise((resolve) => canvas.toBlob(resolve, "image/png", 1.0));

                // Unduh file langsung dari blob
                const a = document.createElement("a");
                const url = URL.createObjectURL(blob); // Buat URL dari blob
                a.href = url;
                a.download = `EKTA-${npm}.png`;
                a.click();

                // Bersihkan URL setelah selesai
                URL.revokeObjectURL(url);
            } catch (error) {
                console.error("Error generating image:", error);
            }
        } else if (type === 'pdf') {
            try {
               // Buat kanvas menggunakan html2canvas
                const canvas = await html2canvas(element, {
                    useCORS: true,           // Mengizinkan sumber gambar lintas domain
                    allowTaint: false,       // Tidak mengizinkan elemen tanpa izin lintas domain
                    logging: true,           // Untuk debug
                    scale: 2,                // Perbesar untuk kualitas lebih baik
                    backgroundColor: "#ffffff",
                    imageTimeout: 15000,     // Timeout untuk gambar
                });

                // Ukuran elemen HTML
                const elementWidth = element.offsetWidth * 2; // Faktor skala 2
                const elementHeight = element.offsetHeight * 2;

                // Ukuran halaman PDF (dalam piksel, disesuaikan dengan kanvas)
                const pdfWidth = elementWidth;
                const pdfHeight = elementHeight;

                // Buat PDF menggunakan jsPDF
                const pdf = new jsPDF('landscape', 'px', [pdfWidth, pdfHeight]);

                // Tambahkan kanvas sebagai gambar ke PDF
                const imageData = canvas.toDataURL("image/png"); // Konversi kanvas ke data URL
                pdf.addImage(imageData, "PNG", 0, 0, pdfWidth, pdfHeight);

                // Unduh PDF
                pdf.save(`EKTA-${npm}.pdf`);
                console.log("PDF berhasil diunduh.");
            } catch (error) {
                console.error("Error generating PDF:", error);
            }
        }
    }

    function downloadPdf() {
        const { jsPDF } = window.jspdf; // Memastikan jsPDF diakses dengan benar
        const element = document.getElementById("mail");

        html2canvas(element).then(function(canvas) {
            const pdf = new jsPDF('landscape', 'px', [canvas.width, canvas.height]); // Menggunakan mode landscape dan ukuran canvas
            const imgData = canvas.toDataURL("image/png");

            // Menambahkan gambar ke PDF dengan posisi dan ukuran dinamis
            pdf.addImage(imgData, "PNG", 0, 0, canvas.width, canvas.height); // Menambahkan gambar dengan ukuran dan posisi yang dinamis
            pdf.save("download.pdf");
        });
    }


    // Panggil fungsi downloadPdf saat tombol diklik
    document.querySelector(".btn-download-pdf").addEventListener("click", downloadPdf);
</script>
@endsection

@section('content')
<!-- Page Content -->
<div style="margin-top: -50px">
    <div class="content">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="btn-group btn-group-sm mr-2 mb-2" role="group" aria-label="Small Outline Secondary First group">
                    <button type="button" onclick="downloadEkta('image')" class="btn btn-outline-secondary"><i class="fa fa-file-image"></i> Download Image</button>
                    <button type="button" class="btn btn-outline-secondary btn-download-pdf"><i class="fa fa-file-pdf"></i> Download PDF</button>
                </div>
            </div>
        </div>
    </div>
    <center>
        <div id="mail" style="padding: 50px 70px; font-family:'Times New Roman', Times, serif; text-align:justify;line-height: 1 !important;">
            <div class="row">
                <img class="imgHeader" src="{{ asset('images/kta/front-new.png') }}" width="100%" alt="">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="img-photo float right" style="margin-top: 137px;margin-right: 81px;float: right;">
                        <img id="img-profile" src="{{ $photoProfile }}"
                        style="
                        width: 133px;
                        border-radius: 15px 15px 15px 15px;
                        margin-right: 5px;
                        height: 176px;
                        background-size: cover;
                        z-index: -1;
                        background-position: center;
                        background-repeat: no-repeat;"
                            alt="preview image" />
                    </div>

                    <h2 class="font-w600" style="padding-top: 135px; margin-left: 40px;font-size: 18px;color: #5e5b5b !important; padding-right: 226px;
                        ">{{$anggota->npa}}</h2>

                    <h2 class="font-w600" style="padding-top: 5px;margin-left: 40px;font-size: 18px;color: #5e5b5b !important; padding-right: 226px;
                        ">{{$anggota->nama_lengkap}}</h2>

                    <h2 class="font-w600" style="padding-top: 5px; margin-left: 40px; font-size: 18px;color: #5e5b5b !important; padding-right: 226px;">{{$anggota->tempat_lahir}}, {{$anggota->tanggal_lahir === null ? "-" : $anggota->tanggal_lahir->format('d F Y')}}</h2>

                    <h2 class="font-w600"
                        style="margin-left: 40px;position: absolute;width: 485px;font-size: 18px;color: #5e5b5b !important;">{{ $anggota->alamat }}
                    </h2>

                    <h2 class="font-w600"
                        style="margin-top: -15px;margin-left: 40px;font-size: 18px;bottom: -85px;color: #5e5b5b !important;position:absolute;z-index: 6;">{{ $anggota->pekerjaan }}</h2>

                    <h2 class="font-w600 text-white" style="padding-top: 45px;margin-left: 40px;bottom: -139px;position: absolute;font-size: 14px;">Berlaku sampai {{ date('d-m-Y', strtotime($anggota->masa_aktif_kta)) }}</h2>

                    <div class="text-center">
                        <!-- <h2 class="float-right" style="margin-right: 90px; margin-top: -68px; font-size: 11px; color: #ffffff !important;">Pimpinan Pusat <br>Pemuda Persatuan Islam
                            <br><br><br><br><br><br> <span style="text-decoration-line: underline;">Ibrahim Nasrul Haq Alfahmi, S.Pd</span> <br>NPA: 09.2830</h2>
                            <img style="margin-top: -45px;   margin-right: -50px;" src="{{ asset('images/cap/cap.png') }}" class="float-right" width="9.5%" alt="">
                            <img style="margin-top: -30px;   margin-right: -170px;" src="{{ asset('images/kta/ttd_ketum_white.png') }}" class="float-right" width="20%" alt=""> -->

                    </div>
                </div>
                <img class="imgBack" src="{{ asset('images/kta/back_new.png') }}" width="100%" alt="">
            </div>
        </div>
    </center>
</div>

<!-- <div class="col-md-12 text-center">
        <button class="btn btn-default btn-download" onclick="downloadEkta()"><i class="si si-cloud-download"></i> Download</button>
    </div> -->
<!-- END Page Content -->
@endsection
