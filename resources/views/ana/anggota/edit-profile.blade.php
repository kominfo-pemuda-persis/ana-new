@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <style>
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

    .modal-lg {
        max-width: 1000px !important;
    }

    </style>
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/additional-methods.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script>
        let method = 'PUT';
        var listPendidikan = {!! $listPendidikan; !!};

        // Setter PC
        function setPc(kdPc, manaPc) {
            document.getElementById("noPC").value = kdPc;
            document.getElementById("namaPC").value = manaPc;
        };

        // Setter PD
        function setPd(kdPd, namaPd) {
            document.getElementById("noPD").value = kdPd;
            document.getElementById("namaPD").value = namaPd;

            document.getElementById("noPC").value = "";
            document.getElementById("namaPC").value = "";

            $.get(`/get-related-pc/${kdPd}`, function (response) {
                $("#lookupPC").dataTable().api().destroy();
                $("#lookupPC tbody").empty();
                let data = "";
                $.each(response, function (index, item) {
                    data = data + `<tr class="pilih1" data-kd_pc="${item.kd_pc}"
                                data-nama_pc="${item.nama_pc}">
                                    <td>${index + 1}</td>
                                    <td>${item.kd_pc}</td>
                                    <td>${item.nama_pc}</td>
                                </tr>`
                });
                $("#lookupPC tbody").html(data);
                $("#lookupPC").dataTable();
            });
        };

        // Setter PW
        function setPw(kdPw, namaPw) {
            document.getElementById("noPW").value = kdPw;
            document.getElementById("namaPW").value = namaPw;

            document.getElementById("noPD").value = "";
            document.getElementById("namaPD").value = "";

            document.getElementById("noPC").value = "";
            document.getElementById("namaPC").value = "";

            $.get(`/get-related-pd/${kdPw}`, function (response) {
                $("#lookupPD").dataTable().api().destroy();
                $("#lookupPD tbody").empty();
                let data = "";
                $.each(response, function (index, item) {
                    data = data + `<tr class="pilih2" data-kd_pd="${item.kd_pd}"
                                data-nama_pd="${item.nama_pd}">
                                    <td>${index + 1}</td>
                                    <td>${item.kd_pd}</td>
                                    <td>${item.nama_pd}</td>
                                </tr>`
                });

                $("#lookupPD tbody").html(data);

                $("#lookupPD").dataTable();
            });
        };

        // Kode PC
        $(document).on('click', '.pilih1', function (e) {
            setPc($(this).attr('data-kd_pc'), $(this).attr('data-nama_pc'));
            $('#myModalPC').modal('hide');
        });
        // Kode PD
        $(document).on('click', '.pilih2', function (e) {
            setPd($(this).attr('data-kd_pd'), $(this).attr('data-nama_pd'));
            $('#myModalPD').modal('hide');

        });
        // Kode PW
        $(document).on('click', '.pilih3', function (e) {
            setPw($(this).attr('data-kd_pw'), $(this).attr('data-nama_pw'));
            let kdPW = $(this).attr('data-kd_pw');
            $('#myModalPW').modal('hide');
        });
        //  tabel lookup npa
        $(function () {
            $("#lookupPW").dataTable();
            $(document).on('focusin', '.datepicker', function(){
                $(this).datepicker({
                    format: "yyyy",
                    viewMode: "years",
                    minViewMode: "years"
                });
            });
        });

        // $(function () {
        $(document).ready(function () {
            var dataKeluarga = {!! $anggota->keluarga !!};
            var dataPendidikan = {!! $anggota->pendidikan !!};
            var dataOrganisasi = {!! $anggota->organisasi !!};
            var dataTafiq = {!! $anggota->tafiq !!};

            $('#anggota-status-merital').val('{{$anggota->status_merital}}');
            $('#anggota-jenis').val('{{$anggota->jenis_keanggotaan}}');
            $('#anggota-gol-darah').val('{{$anggota->gol_darah}}');
            $('#anggota-status-aktif').val('{{$anggota->status_aktif}}');
            $('.pendidikan_terakhir').val('{{$anggota->pendidikan_terakhir}}');
            $('.level_tafiq').val('{{$anggota->level_tafiq}}'?'{{$anggota->level_tafiq}}':0);

            $('#provinces').val('{{$anggota->provinsi}}');
            regencies('{{$anggota->provinsi}}','{{$anggota->kota}}');
            districts('{{$anggota->kota}}','{{$anggota->kecamatan}}');
            villages('{{$anggota->kecamatan}}','{{$anggota->desa}}');



            if(dataKeluarga.length > 0){
                $.each(dataKeluarga,function (i,obj) {
                    addKeluarga(obj);
                });

            }else{
                addKeluarga(false);
            }
            if(dataPendidikan.length > 0){
                $.each(dataPendidikan,function (i,obj) {
                    addPendidikan(obj);
                });
            }else{
                addPendidikan(false);
            }
            if(dataOrganisasi.length > 0){
                $.each(dataOrganisasi,function (i,obj) {
                    addOrganisasi(obj);
                });
            }else{
                addOrganisasi(false);
            }
            if(dataTafiq.length > 0){
                $.each(dataTafiq,function (i,obj) {
                    addTafiq(obj);
                });
            }else{
                // addTafiq(false);
            }
            if ({{ $anggota && $anggota->pimpinanWilayah}}) setPw('{{$anggota->pimpinanWilayah->kd_pw}}', '{{$anggota->pimpinanWilayah->nama_pw}}');
            if ({{ $anggota && $anggota->pimpinanDaerah}}) setPd('{{$anggota->pimpinanDaerah->kd_pd}}', '{{$anggota->pimpinanDaerah->nama_pd}}');
            if ({{ $anggota && $anggota->pimpinanCabang}}) setPc('{{$anggota->pimpinanCabang->kd_pc}}', '{{$anggota->pimpinanCabang->nama_pc}}');
        });

        $('#provinces').change( function (e) {
            var province_id = e.target.value;
            regencies(province_id);
        });

        $('#regencies').change( function (e) {
            var regencies_id = e.target.value;
            districts(regencies_id);
        });

        $('#districts').on('change', function (e) {
            var districts_id = e.target.value;
            villages(districts_id);
        });

        $('#villages').on('change', function (e) {
            console.log($( "#villages option:selected" ).text());

            $("#namaDesa").val($( "#villages option:selected" ).text());
        });

        function regencies(province_id,regencies_id=null) {
            $.get('/regencies?province_id=' + province_id, function (data) {
                $('#regencies').empty();
                var html = '<option ' ;
                if(!regencies_id) {
                    html +='disable="true" selected="true"';
                }
                html +=' value="" >=== Pilih Kabupaten/Kota ===</option>';
                $('#regencies').append(html);
                $('#districts').empty();
                $('#districts').append('<option value="" disable="true" selected="true">=== Pilih Kecamatan ===</option>');

                $('#villages').empty();
                $('#villages').append('<option value="" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, regenciesObj) {
                    if(regencies_id == regenciesObj.id){
                        $('#regencies').append('<option selected="true" value="' + regenciesObj.id + '">' + regenciesObj.nama + '</option>');
                    }else{
                        $('#regencies').append('<option value="' + regenciesObj.id + '">' + regenciesObj.nama + '</option>');
                    }

                });

            });
        }
        function districts(regencies_id, districts_id=null) {
            $.get('/districts?regencies_id=' + regencies_id, function (data) {
                $('#districts').empty();
                var html = '<option ' ;
                if(!districts_id) {
                    html +='disable="true" selected="true"';
                }

                $('#districts').append(html +=' value="">=== Pilih Kecamatan ===</option>');

                $('#villages').empty();
                $('#villages').append('<option value="" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');
                $.each(data, function (index, districtsObj) {
                    if(districts_id == districtsObj.id){
                        $('#districts').append('<option selected="true" value="' + districtsObj.id + '">' + districtsObj.nama + '</option>');
                    }else {
                        $('#districts').append('<option value="' + districtsObj.id + '">' + districtsObj.nama + '</option>');
                    }
                });

            });

        }
        function villages(districts_id, villages_id=null) {
            $.get('/village?districts_id=' + districts_id, function (data) {
                $('#villages').empty();
                var html = '<option ' ;
                if(!villages_id) {
                    html += 'disable="true" selected="true"';
                }

                $('#villages').append(html += ' value="" >=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, villagesObj) {
                    if (villages_id == villagesObj.id) {
                        $('#villages').append('<option selected="true" value="' + villagesObj.id + '">' + villagesObj.nama + '</option>');
                        $("#namaDesa").val(villagesObj.nama);
                    } else {
                        $('#villages').append('<option value="' + villagesObj.id + '">' + villagesObj.nama + '</option>');
                    }
                });

            });

        }

        var toList= '/anggota/profil/{{ $anggota->id_anggota }}';
        @permission('anggota.read_all','anggota.read_pw','anggota.read_pd','anggota.read_pc','anggota.read_pj')
            toList= '/anggota';
        @endpermission

        $('#update-anggota-form').on('submit', function(e) {
            $.ajax({
                type: 'put',
                url: '{{ route('anggota.update', $anggota->id_anggota) }}',
                data: $(this).serialize(),
                beforeSend: function () {
                    swal({
                        title: 'Updating data..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        buttons: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    });
                },
              }).done(function (resp, textStatus ) {
                if (!!resp.data) {
                    swal.close();
                    swal("Berhasil!", "Data telah berhasil diperbarui.", "success").then((result) => {
                        window.location.href = toList;
                    });
                }
              }).fail(function (resp, textStatus ) {
                swal.close();
                swal("Error!", "Data gagal disimpan. Detail " + resp.message, "error");
              });
            e.preventDefault();
        })
    </script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/pg_anggota.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_keluarga.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_pendidikan.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_organisasi.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_tafiq.js?id=') . Str::random(25) }}"></script>

@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Update Anggota <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Updating</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Update Anggota</li>
                        @if (Permission::checkPermission('anggota.read_pw') || Permission::checkPermission('anggota.read_pd') || Permission::checkPermission('anggota.read_pc') || Permission::checkPermission('anggota.read_pj') || Permission::checkPermission('anggota.read_all'))
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/anggota">Data Anggota</a>
                        </li>
                        @else
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{ route('anggota.profil',Auth::user()->anggota->id_anggota) }}">Profile Anggota</a>
                        </li>
                        @endif
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Form <small>Update {{ $anggota->nama_lengkap }}</small></h3>
            </div>
            <form id="update-anggota-form" class="row">
                @csrf
                <div class="col col-6">
                    <div class="block-content">
                        <h5 class="mb-2">DATA ANGGOTA</h5>
                        <div class="form-group">
                            <label for="anggota-npa" class="block-title">NPA</label>
                            <input class="form-control" type="hidden" value="{{ $anggota->id_anggota }}" id="id" required>
                            <input class="form-control" readonly type="text" id="anggota-npa" value="{{ $anggota->npa }}" name="npa" required>
                        </div>
                        <div class="form-group">
                            <label for="anggota-name" class="block-title">Nama Lengkap</label>
                            <input class="form-control" type="text" id="anggota-name" value="{{ $anggota->nama_lengkap }}" name="nama_lengkap" required>
                        </div>
                        <div class="form-group">
                            <label for="anggota-birthplace" class="block-title">Tempat Lahir</label>
                            <input class="form-control" type="text" value="{{ $anggota->tempat_lahir }}" id="anggota-birthplace" name="tempat_lahir" required>
                        </div>
                        <div class="form-group">
                            <label for="anggota-birthdate" class="block-title">Tanggal Lahir</label>
                            <input class="form-control" type="date"
                                    value="{{$anggota->tanggal_lahir === null ? "" : $anggota->tanggal_lahir->format("Y-m-d") }}"
                                    id="anggota-birthdate" name="tanggal_lahir" required>
                        </div>
                        <div class="form-group">
                            <label for="anggota-gol-darah" class="block-title">Golongan Darah</label>
                            <select class="form-control" id="anggota-gol-darah" name="gol_darah" required>
                                <option value="">Pilih Golongan Darah</option>
                                <option value="O" {{ $anggota->gol_darah == 'O' ? 'selected' : '' }}>O
                                </option>
                                <option value="A" {{ $anggota->gol_darah == 'A' ? 'selected' : '' }}>A
                                </option>
                                <option value="B" {{ $anggota->gol_darah == 'B' ? 'selected' : '' }}>B
                                </option>
                                <option value="AB" {{ $anggota->gol_darah == 'AB' ? 'selected' : '' }}>AB
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="anggota-status-merital" class="block-title">Status Merital</label>
                            <select class="form-control" id="anggota-status-merital" name="status_merital"
                                    required>
                                {% set status_merital = $anggota-?status_merital %}
                                <option value="">Pilih Status</option>
                                <option value="SINGLE" {{ $anggota->status_merital == 'SINGLE' ? 'selected' : '' }}>SINGLE</option>
                                <option value="MENIKAH" {{ $anggota->status_merital == 'MENIKAH' ? 'selected' : '' }}>MENIKAH</option>
                                <option value="DUDA" {{ $anggota->status_merital == 'DUDA' ? 'selected' : '' }}>DUDA</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="anggota-pekerjaan" class="block-title">Pekerjaan</label>
                            <input class="form-control" type="text" value="{{ $anggota->pekerjaan }}"
                                    id="anggota-pekerjaan" name="pekerjaan" required>
                        </div>
                        <div class="form-group">
                            <label for="anggota-status-aktif" class="block-title">Status</label>
                            <select class="form-control" id="anggota-status-aktif" name="status_aktif" required>
                                <option value="">Pilih Status</option>
                                <option value="ACTIVE">ACTIVE</option>
                                <option value="NON ACTIVE">NON ACTIVE</option>
                                <option value="TIDAK HER-REGISTRASI">TIDAK HER-REGISTRASI</option>
                                <option value="MUTASI">MUTASI</option>
                                <option value="MENINGGAL">MENINGGAL</option>
                                <option value="MENGUNDURKAN DIRI">MENGUNDURKAN DIRI</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col col-6">
                    <div class="block-content">
                        <h5 class="mb-2">DATA JAMIYYAH</h5>
                        <div class="form-group">
                            <label for="anggota-reg-date" class="block-title">Tanggal Ma'aruf</label>
                            <input class="form-control" type="date" value="{{ date('Y-m-d',strtotime($anggota->reg_date)) }}"  id="anggota-reg-date" name="reg_date" required>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-md-12 col-form-label block-title py-0" for="example-hf-email">PW</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="noPW" name="pw" value="{{ $anggota->pw }}"
                                        placeholder="Nomor PW" readonly>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="namaPW" name="namaPW" value="{{ $anggota->pimpinanWilayah?$anggota->pimpinanWilayah->nama_pw:'' }}"
                                        placeholder="Nama PW" readonly required>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-success" data-toggle="modal"
                                        data-target="#myModalPW"><i class="fa fa-search mr-1"> Cari</i>
                                </button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-md-12 col-form-label block-title py-0" for="example-hf-email">PD</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="noPD" name="pd" value="{{ $anggota->pd }}"
                                        placeholder="Nomor PD" readonly >
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="namaPD" name="namaPD" value="{{ $anggota->pimpinanDaerah?$anggota->pimpinanDaerah->nama_pd:'' }}"
                                        placeholder="Nama PD" readonly required>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info" data-toggle="modal"
                                        data-target="#myModalPD"><i class="fa fa-search mr-1"> Cari</i>
                                </button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-md-12 col-form-label block-title py-0" for="example-hf-email">PC</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="noPC" name="pc" value="{{ $anggota->pc }}"
                                        placeholder="Nomor PC" readonly>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="namaPC" name="namaPC" value="{{ $anggota->pimpinanCabang?$anggota->pimpinanCabang->nama_pc:'' }}"
                                        placeholder="Nama PC" readonly required>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-warning" data-toggle="modal"
                                        data-target="#myModalPC"><i class="fa fa-search mr-1"> Cari</i>
                                </button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-md-12 col-form-label block-title py-0" for="example-hf-email">PJ</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="noPJ" name="pj"
                                        value="{{ $anggota->pj }}"
                                        placeholder="Nomor PJ">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="namaPJ" placeholder="Nama PJ"
                                        value="{{ $anggota->nama_pj }}">
                            </div>
                        </div>

                        @if($isKtaFormShown)
                            <div class="form-group">
                                <label for="anggota-aktif-date" class="block-title">Masa Aktif KTA</label>
                                <input class="form-control" type="date" value="{{$anggota->masa_aktif_kta === null ? "" : $anggota->masa_aktif_kta->format("Y-m-d") }}"
                                       id="anggota-aktif-date" name="masa_aktif_kta" required>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="anggota-jenis" class="block-title">Jenis Keanggotaan</label>
                            <select class="form-control" id="anggota-jenis" name="jenis_keanggotaan" required>
                                <option value="">Pilih Status</option>
                                <option value="BIASA">BIASA</option>
                                <option value="TERSIAR">TERSIAR</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col col-6">
                    <div class="block-content">
                        <h5 class="mb-2">Alamat</h5>
                        <div class="form-group">
                            <label for="pw" class="block-title">Provinsi</label>
                            <select class="form-control" name="provinsi" id="provinces" required>
                                <option value="" disable="true" selected="true">Pilih Propinsi
                                </option>
                                @foreach ($provinces as $key => $value)
                                    <option value="{{$value->id}}">{{ $value->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pd" class="control-label">Kabupaten/Kota</label>
                            <select class="form-control" name="kota" id="regencies" required>
                                <option value="">Pilih Kabupaten/Kota</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pc" class="control-label">Kecamatan</label>
                            <select class="form-control" name="kecamatan" id="districts" required>
                                <option value="" disable="true" selected="true">Pilih Kecamatan
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pc" class="control-label">Desa</label>
                            <select class="form-control" name="desa" id="villages" required>
                                <option value="" disable="true" selected="true">Pilih Desa
                                </option>
                            </select>
                            <input type="hidden" name="namaDesa" id="namaDesa">
                        </div>
                        <div class="form-group">
                            <label for="anggota-alamat">Alamat</label>
                            <textarea class="form-control" type="text" id="anggota-alamat" name="alamat" required rows="3">{{ $anggota->alamat }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col col-6 d-flex flex-column justify-content-between">
                    <div class="block-content">
                        <h5 class="mb-2">Kontak</h5>
                        <div class="form-group">
                            <label for="anggota-email">Email</label>
                            <input class="form-control" type="email" value="{{ $anggota->email }}" id="anggota-email" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="anggota-no-telpon">Nomor Telpon</label>
                            <input class="form-control" type="text" value="{{ $anggota->no_telpon }}" id="anggota-no-telpon" name="no_telpon" required>
                        </div>
                        <div class="form-group">
                            <label for="anggota-no-telpon-lain">Nomor Telpon Lain</label>
                            <input class="form-control" type="text" value="{{ $anggota->no_telpon2 }}" id="anggota-no-telpon-lain" name="no_telpon2">
                        </div>
                    </div>
                    <div class="d-flex justify-content-end p-3">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check mr-1"></i> Simpan
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Page Content -->


    <!--region modalPC-->
    <div class="modal fade" id="myModalPC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Kode PC</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <table id="lookupPC" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PC</th>
                            <th>Nama PC</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--endregion-->

    <!--region modalPD-->
    <div class="modal fade" id="myModalPD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Kode PD</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <table id="lookupPD" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PD</th>
                            <th>Nama PD</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--endregion-->

    <!--region modalPW-->
    <div class="modal fade" id="myModalPW" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Kode PW</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <table id="lookupPW" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PW</th>
                            <th>Nama PW</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no =1)
                        @foreach ($dataPW as $data_pw)
                            <tr class="pilih3" data-kd_pw="{{$data_pw->kd_pw}}"
                                data-nama_pw="{{$data_pw->nama_pw}}">
                                <td>{{$no++}}</td>
                                <td>{{$data_pw->kd_pw}}</td>
                                <td>{{$data_pw->nama_pw}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--endregion-->
@endsection
