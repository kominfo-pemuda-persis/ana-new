@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/cropperjs/cropper.min.css') }}">
    <style>
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

    .modal-lg {
        max-width: 1000px !important;
    }

    </style>
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/additional-methods.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/js/plugins/cropperjs/cropper.min.js') }}"></script>

    <script>
        let method = 'PUT';

        var listPendidikan = {!! $listPendidikan; !!};

        // Kode PC
        $(document).on('click', '.pilih1', function (e) {
            document.getElementById("noPC").value = $(this).attr('data-kd_pc');
            document.getElementById("namaPC").value = $(this).attr('data-nama_pc');
            $('#myModalPC').modal('hide');
        });
        // Kode PD
        $(document).on('click', '.pilih2', function (e) {
            let kdPD = $(this).attr('data-kd_pd');

            document.getElementById("noPD").value = $(this).attr('data-kd_pd');
            document.getElementById("namaPD").value = $(this).attr('data-nama_pd');

            document.getElementById("noPC").value = "";
            document.getElementById("namaPC").value = "";

            $('#myModalPD').modal('hide');

            $.get(`/get-related-pc/${kdPD}`, function (response) {
                $("#lookupPC").dataTable().api().destroy();
                $("#lookupPC tbody").empty();
                let data = "";
                $.each(response, function (index, item) {
                    data = data + `<tr class="pilih1" data-kd_pc="${item.kd_pc}"
                                data-nama_pc="${item.nama_pc}">
                                    <td>${index + 1}</td>
                                    <td>${item.kd_pc}</td>
                                    <td>${item.nama_pc}</td>
                                </tr>`
                });
                $("#lookupPC tbody").html(data);
                $("#lookupPC").dataTable();
            });
        });
        // Kode PW
        $(document).on('click', '.pilih3', function (e) {
            let kdPW = $(this).attr('data-kd_pw');
            document.getElementById("noPW").value = kdPW;
            document.getElementById("namaPW").value = $(this).attr('data-nama_pw');

            document.getElementById("noPD").value = "";
            document.getElementById("namaPD").value = "";

            document.getElementById("noPC").value = "";
            document.getElementById("namaPC").value = "";

            $('#myModalPW').modal('hide');

            $.get(`/get-related-pd/${kdPW}`, function (response) {
                $("#lookupPD").dataTable().api().destroy();
                $("#lookupPD tbody").empty();
                let data = "";
                $.each(response, function (index, item) {
                    data = data + `<tr class="pilih2" data-kd_pd="${item.kd_pd}"
                                data-nama_pd="${item.nama_pd}">
                                    <td>${index + 1}</td>
                                    <td>${item.kd_pd}</td>
                                    <td>${item.nama_pd}</td>
                                </tr>`
                });

                $("#lookupPD tbody").html(data);

                $("#lookupPD").dataTable();
            });
        });
        //  tabel lookup npa
        $(function () {
            $("#lookupPW").dataTable();
            $(document).on('focusin', '.datepicker', function(){
                $(this).datepicker({
                    format: "yyyy",
                    viewMode: "years",
                    minViewMode: "years"
                });
            });
        });

        // $(function () {
        $(document).ready(function () {
            var dataKeluarga = {!! $anggota->keluarga !!};
            var dataPendidikan = {!! $anggota->pendidikan !!};
            var dataOrganisasi = {!! $anggota->organisasi !!};
            var dataTafiq = {!! $anggota->tafiq !!};


            $('#anggota-status-merital').val('{{$anggota->status_merital}}');
            $('#anggota-jenis').val('{{$anggota->jenis_keanggotaan}}');
            $('#anggota-gol-darah').val('{{$anggota->gol_darah}}');
            $('#anggota-status-aktif').val('{{$anggota->status_aktif}}');
            $('.pendidikan_terakhir').val('{{$anggota->pendidikan_terakhir}}');
            $('.level_tafiq').val('{{$anggota->level_tafiq}}'?'{{$anggota->level_tafiq}}':0);


            $('#provinces').val('{{$anggota->provinsi}}');
            regencies('{{$anggota->provinsi}}','{{$anggota->kota}}');
            districts('{{$anggota->kota}}','{{$anggota->kecamatan}}');
            villages('{{$anggota->kecamatan}}','{{$anggota->desa}}');


            if(dataKeluarga.length > 0){
                $.each(dataKeluarga,function (i,obj) {
                    addKeluarga(obj);
                });

            }else{
                addKeluarga(false);
            }
            if(dataPendidikan.length > 0){
                $.each(dataPendidikan,function (i,obj) {
                    addPendidikan(obj);
                });
            }else{
                addPendidikan(false);
            }
            if(dataOrganisasi.length > 0){
                $.each(dataOrganisasi,function (i,obj) {
                    addOrganisasi(obj);
                });
            }else{
                addOrganisasi(false);
            }
            if(dataTafiq.length > 0){
                $.each(dataTafiq,function (i,obj) {
                    addTafiq(obj);
                });
            }else{
                // addTafiq(false);
            }

        });

        $('#provinces').change( function (e) {
            var province_id = e.target.value;
            regencies(province_id);
        });

        $('#regencies').change( function (e) {
            var regencies_id = e.target.value;
            districts(regencies_id);
        });

        $('#districts').on('change', function (e) {
            var districts_id = e.target.value;
            villages(districts_id);
        });

        $('#villages').on('change', function (e) {
            console.log($( "#villages option:selected" ).text());

            $("#namaDesa").val($( "#villages option:selected" ).text());
        });

        function regencies(province_id,regencies_id=null) {
            $.get('/regencies?province_id=' + province_id, function (data) {
                $('#regencies').empty();
                var html = '<option ' ;
                if(!regencies_id) {
                    html +='disable="true" selected="true"';
                }
                html +=' value="" >=== Pilih Kabupaten/Kota ===</option>';
                $('#regencies').append(html);
                $('#districts').empty();
                $('#districts').append('<option value="" disable="true" selected="true">=== Pilih Kecamatan ===</option>');

                $('#villages').empty();
                $('#villages').append('<option value="" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, regenciesObj) {
                    if(regencies_id == regenciesObj.id){
                        $('#regencies').append('<option selected="true" value="' + regenciesObj.id + '">' + regenciesObj.nama + '</option>');
                    }else{
                        $('#regencies').append('<option value="' + regenciesObj.id + '">' + regenciesObj.nama + '</option>');
                    }

                });

            });
        }
        function districts(regencies_id, districts_id=null) {
            $.get('/districts?regencies_id=' + regencies_id, function (data) {
                $('#districts').empty();
                var html = '<option ' ;
                if(!districts_id) {
                    html +='disable="true" selected="true"';
                }

                $('#districts').append(html +=' value="">=== Pilih Kecamatan ===</option>');

                $('#villages').empty();
                $('#villages').append('<option value="" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');
                $.each(data, function (index, districtsObj) {
                    if(districts_id == districtsObj.id){
                        $('#districts').append('<option selected="true" value="' + districtsObj.id + '">' + districtsObj.nama + '</option>');
                    }else {
                        $('#districts').append('<option value="' + districtsObj.id + '">' + districtsObj.nama + '</option>');
                    }
                });

            });

        }
        function villages(districts_id, villages_id=null) {
            $.get('/village?districts_id=' + districts_id, function (data) {
                $('#villages').empty();
                var html = '<option ' ;
                if(!villages_id) {
                    html += 'disable="true" selected="true"';
                }

                $('#villages').append(html += ' value="" >=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, villagesObj) {
                    if (villages_id == villagesObj.id) {
                        $('#villages').append('<option selected="true" value="' + villagesObj.id + '">' + villagesObj.nama + '</option>');
                        $("#namaDesa").val(villagesObj.nama);
                    } else {
                        $('#villages').append('<option value="' + villagesObj.id + '">' + villagesObj.nama + '</option>');
                    }
                });

            });

        }

        // END Crop Image
        var $modal = $('#modal-crop');
        var image = document.getElementById('image-crop');
        var cropper;
        $('#foto-upload').change(function (e) {
            var files = e.target.files;
            var done = function (url) {
                image.src = url;
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;
            if (files && files.length > 0) {
                if (this.files[0].size > 5120000) {
                    $('#foto-upload').attr('value', '');
                    $('input#foto-upload.valid').attr('src', '');
                    swal("Mohon maaf!!!", "Ukuran foto lebih dari 5 MB", "error");
                } else {
                    file = files[0];
                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            }
        });
        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                scalable: false,
                preview: '.preview'
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        }).on("click","#cancel-crop, #cancel-crop-close", function(){
            $('#foto-upload').val(null);
            cropper.destroy();
            cropper = null;
        });

        $("#clear-foto-file").click(function (e) {
            e.preventDefault();
            $('#foto-upload').val(null);
            $('#preview-image').attr("src", "https://www.riobeauty.co.uk/images/product_image_not_found.gif");
        })

        $("#crop").click(function () {
            canvas = cropper.getCroppedCanvas({
                width: 160,
                height: 160,
            });

            canvas.toBlob(function (blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    $('input#foto').val(base64data);
                    $('img#preview-image').attr('src', base64data);
                    $modal.modal('hide');
                }
            });
        })
        // END Crop Image

        var toList= '/anggota/profil/{{ $anggota->id_anggota }}';
        @permission('anggota.read_all','anggota.read_pw','anggota.read_pd','anggota.read_pc','anggota.read_pj')
            toList= '/anggota';
        @endpermission

    </script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/form_wizard_anggota.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/pg_anggota.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_keluarga.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_pendidikan.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_organisasi.js?id=') . Str::random(25) }}"></script>
    <script src="{{ asset('js/pages/anggota/form_anggota_tafiq.js?id=') . Str::random(25) }}"></script>

@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Update Anggota <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Updating</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Update Anggota</li>
                        @if (Permission::checkPermission('anggota.read_pw') || Permission::checkPermission('anggota.read_pd') || Permission::checkPermission('anggota.read_pc') || Permission::checkPermission('anggota.read_pj') || Permission::checkPermission('anggota.read_all'))
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/anggota">Data Anggota</a>
                        </li>
                        @else
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{ route('anggota.profil',Auth::user()->anggota->id_anggota) }}">Profile Anggota</a>
                        </li>
                        @endif
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Form <small>Update {{ $anggota->nama_lengkap }}</small></h3>
            </div>
            <div class="block-content block-content-full">
                <div class="js-wizard-validation">
                    <ul class="nav nav-tabs nav-tabs-block nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#wizard-validation-step1" data-toggle="tab">Data Anggota</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#wizard-validation-step2" data-toggle="tab">Data Keanggotaan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#wizard-validation-step3" data-toggle="tab">Alamat & Kontak</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#wizard-validation-step4" data-toggle="tab">Keluarga & Orang
                                Tua</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#wizard-validation-step5" data-toggle="tab">Pendidikan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#wizard-validation-step6" data-toggle="tab">Organisasi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#wizard-validation-step7" data-toggle="tab">Tafiq</a>
                        </li>
                    </ul>
                    <form class="js-wizard-anggota-form" action="{{ route('anggota.update', $anggota->id_anggota) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="block-content block-content-full tab-content px-md-5" style="min-height: 300px;">
                            <div class="tab-pane active" id="wizard-validation-step1" role="tabpanel">
                                <div class="form-group">
                                    <label for="anggota-npa">NPA</label>
                                    <input class="form-control" type="hidden" value="{{ $anggota->id_anggota }}" id="id" required>
                                    <input class="form-control" readonly type="text" id="anggota-npa" value="{{ $anggota->npa }}" name="npa" required>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-name">Nama Lengkap</label>
                                    <input class="form-control" type="text" id="anggota-name" value="{{ $anggota->nama_lengkap }}" name="nama_lengkap" required>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-birthplace">Tempat Lahir</label>
                                    <input class="form-control" type="text" value="{{ $anggota->tempat_lahir }}" id="anggota-birthplace" name="tempat_lahir" required>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-birthdate">Tanggal Lahir</label>
                                    <input class="form-control" type="date"
                                           value="{{$anggota->tanggal_lahir === null ? "" : $anggota->tanggal_lahir->format("Y-m-d") }}"
                                           id="anggota-birthdate" name="tanggal_lahir" required>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-gol-darah">Golongan Darah</label>
                                    <select class="form-control" id="anggota-gol-darah" name="gol_darah" required>
                                        <option value="">Pilih Golongan Darah</option>
                                        <option value="O" {{ $anggota->gol_darah == 'O' ? 'selected' : '' }}>O
                                        </option>
                                        <option value="A" {{ $anggota->gol_darah == 'A' ? 'selected' : '' }}>A
                                        </option>
                                        <option value="B" {{ $anggota->gol_darah == 'B' ? 'selected' : '' }}>B
                                        </option>
                                        <option value="AB" {{ $anggota->gol_darah == 'AB' ? 'selected' : '' }}>AB
                                        </option>
                                        <option value="-" {{ $anggota->gol_darah == '-' ? 'selected' : '' }}>TIDAK TAHU
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-status-merital">Status Merital</label>
                                    <select class="form-control" id="anggota-status-merital" name="status_merital"
                                            required>
                                        {% set status_merital = $anggota-?status_merital %}
                                        <option value="">Pilih Status</option>
                                        <option value="SINGLE">SINGLE</option>
                                        <option value="MENIKAH">MENIKAH</option>
                                        <option value="DUDA">DUDA</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-pekerjaan">Pekerjaan</label>
                                    <input class="form-control" type="text" value="{{ $anggota->pekerjaan }}"
                                           id="anggota-pekerjaan" name="pekerjaan" required>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-status-aktif">Status</label>
                                    <select class="form-control" name="status_aktif" required>
                                        <option value="">Pilih Status</option>
                                        <option value="ACTIVE" {{ $anggota->status_aktif == 'ACTIVE' ? 'selected' : '' }}>ACTIVE</option>
                                        <option value="NON ACTIVE" {{ $anggota->status_aktif == 'NON ACTIVE' ? 'selected' : '' }}>NON ACTIVE</option>
                                        <option value="TIDAK HER-REGISTRASI" {{ $anggota->status_aktif == 'TIDAK HEREGISTRASI' ? 'selected' : '' }}>TIDAK HER-REGISTRASI</option>
                                        <option value="MUTASI" {{ $anggota->status_aktif == 'MUTASI' ? 'selected' : '' }}>MUTASI</option>
                                        <option value="MENINGGAL" {{ $anggota->status_aktif == 'MENINGGAL' ? 'selected' : '' }}>MENINGGAL</option>
                                        {{-- <option value="MENGUNDURKAN DIRI" {{ $anggota->status_aktif == 'ACTIVE' ? 'selected' : '' }}>MENGUNDURKAN DIRI</option> --}}
                                    </select>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputFile" class="col-sm-2 control-label">Foto Diri</label>
                                    <div class="col-sm-10">
                                        <div class="row">
                                            <div tabindex="500" class="btn btn-success btn-file mr-2"><i
                                                    class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">Pilih Foto Diri</span>
                                                <input type="file" name="foto-upload" id="foto-upload"
                                                       class=""></div>
                                            <input type="hidden" name="foto" id="foto">
                                            <button id="clear-foto-file" class="btn btn-danger btn-sm" style="margin-right: 5px"><span
                                                    class="si si-trash"></span>Hapus Foto</button>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-2 mt-4">
                                        @if(!$anggota->foto || $anggota->foto=="default.png")
                                            <img id="preview-image" alt="preview image" style="max-height: 250px;"
                                                 src=" {{ Storage::disk("s3")->url("static/" . $anggota->foto) }} ">
                                        @else
                                        <img id="preview-image" alt="preview image" style="max-height: 250px;"
                                                 src="{{ Storage::disk('s3')->url('images/anggota/'.$anggota->foto) }}">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="wizard-validation-step2" role="tabpanel">
                                <div class="form-group">
                                    <label for="anggota-reg-date">Tanggal Ma'aruf</label>
                                    <input class="form-control" type="date" value="{{ date('Y-m-d',strtotime($anggota->reg_date)) }}"  id="anggota-reg-date" name="reg_date" required>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-md-12 col-form-label" for="example-hf-email">PW</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="noPW" name="pw" value="{{ $anggota->pw }}"
                                               placeholder="Nomor PW" readonly>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="namaPW" name="namaPW" value="{{ $anggota->pimpinanWilayah?$anggota->pimpinanWilayah->nama_pw:'' }}"
                                               placeholder="Nama PW" readonly required>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                                data-target="#myModalPW"><i class="fa fa-search mr-1"> Cari</i>
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-md-12 col-form-label" for="example-hf-email">PD</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="noPD" name="pd" value="{{ $anggota->pd }}"
                                               placeholder="Nomor PD" readonly >
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="namaPD" name="namaPD" value="{{ $anggota->pimpinanDaerah?$anggota->pimpinanDaerah->nama_pd:'' }}"
                                               placeholder="Nama PD" readonly required>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                                data-target="#myModalPD"><i class="fa fa-search mr-1"> Cari</i>
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-md-12 col-form-label" for="example-hf-email">PC</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="noPC" name="pc" value="{{ $anggota->pc }}"
                                               placeholder="Nomor PC" readonly>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="namaPC" name="namaPC" value="{{ $anggota->pimpinanCabang?$anggota->pimpinanCabang->nama_pc:'' }}"
                                               placeholder="Nama PC" readonly required>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="btn btn-warning" data-toggle="modal"
                                                data-target="#myModalPC"><i class="fa fa-search mr-1"> Cari</i>
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-md-12 col-form-label" for="example-hf-email">PJ</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="noPJ" name="pj"
                                               value="{{ $anggota->pj }}"
                                               placeholder="Nomor PJ">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="namaPJ" placeholder="Nama PJ"
                                               value="{{ $anggota->nama_pj }}">
                                    </div>
                                </div>
                                @if($isKtaFormShown)
                                <div class="form-group">
                                    <label for="anggota-aktif-date">Masa Aktif KTA</label>
                                    <input class="form-control" type="date" value="{{$anggota->masa_aktif_kta === null ? "" : $anggota->masa_aktif_kta->format("Y-m-d") }}"
                                           id="anggota-aktif-date" name="masa_aktif_kta" required>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label for="anggota-jenis">Jenis Keanggotaan</label>
                                    <select class="form-control" id="anggota-jenis" name="jenis_keanggotaan" required>
                                        <option value="">Pilih Status</option>
                                        <option value="BIASA">BIASA</option>
                                        <option value="TERSIAR">TERSIAR</option>
                                    </select>
                                </div>
                            </div>
                            <div class="tab-pane" id="wizard-validation-step3" role="tabpanel">
                                <div class="form-group">
                                    <label class="font-w300">Alamat</label>
                                </div>
                                <div class="form-group row">
                                    <label for="pw" class="col-sm-2 control-label">Provinsi</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="provinsi" id="provinces" required>
                                            <option value="" disable="true" selected="true">Pilih Propinsi
                                            </option>
                                            @foreach ($provinces as $key => $value)
                                                <option value="{{$value->id}}">{{ $value->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pd" class="col-sm-2 control-label">Kabupaten/Kota</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="kota" id="regencies" required>
                                            <option value="">Pilih Kabupaten/Kota</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pc" class="col-sm-2 control-label">Kecamatan</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="kecamatan" id="districts" required>
                                            <option value="" disable="true" selected="true">Pilih Kecamatan
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pc" class="col-sm-2 control-label">Desa</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="desa" id="villages" required>
                                            <option value="" disable="true" selected="true">Pilih Desa
                                            </option>
                                        </select>
                                        <input type="hidden" name="namaDesa" id="namaDesa">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-alamat">Alamat</label>
                                    <textarea class="form-control" type="text" id="anggota-alamat" name="alamat" required rows="3">{{ $anggota->alamat }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label class="font-w300">Kontak</label>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-email">Email</label>
                                    <input class="form-control" type="email" value="{{ $anggota->email }}" id="anggota-email" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-no-telpon">Nomor Telpon</label>
                                    <input class="form-control" type="text" value="{{ $anggota->no_telpon }}" id="anggota-no-telpon" name="no_telpon" required>
                                </div>
                                <div class="form-group">
                                    <label for="anggota-no-telpon-lain">Nomor Telpon Lain</label>
                                    <input class="form-control" type="text" value="{{ $anggota->no_telpon2 }}" id="anggota-no-telpon-lain" name="no_telpon2">
                                </div>
                            </div>
                            <!--region keluarga-->
                            <div class="tab-pane" id="wizard-validation-step4" role="tabpanel">
                                <div class="form-group">
                                    <label class="font-w300">Keluarga</label>
                                </div>
                                <table class="table table-striped table-bordered" id="keluarga_table" >
                                    <thead>
                                        <tr>
                                            <th  style="width:5%">No</th>
                                            <th  >Form</th>
                                            <th  style="width:5%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><button type="button" class="btn btn-info btn-sm addKelauarga">Add</button></td></tr>
                                    </tfoot>

                                </table>

                            </div>
                            <!--endregion-->

                            <!--region pendidikan-->
                            <div class="tab-pane" id="wizard-validation-step5" role="tabpanel">
                                <div class="form-group">
                                    <label class="font-w300">Pendidikan</label>
                                </div>
                                <div class="form-group">
                                    <label for="nama-sd">Pendidikan Terakhir</label>
                                    <select class="form-control pendidikan_terakhir" id="pendidikan-terakhir" name="pendidikan_terakhir"
                                            required>
                                        <option value="">Pilih Pendidikan Terakhir</option>
                                        @foreach($listPendidikan as $item)
                                            <option
                                                value="{{ $item->id_tingkat_pendidikan }}">{{ $item->pendidikan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <table class="table table-striped table-bordered" id="pendidikan_table" >
                                    <thead>
                                        <tr>
                                            <th  style="width:5%">No</th>
                                            <th  >Form</th>
                                            <th  style="width:5%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><button type="button" class="btn btn-info btn-sm addPendidikan">Add</button></td>
                                        </tr>
                                    </tfoot>

                                </table>
                            </div>
                            <!--endregion-->


                            <!--region organisasi-->

                            <div class="tab-pane" id="wizard-validation-step6" role="tabpanel">
                                <div class="form-group">
                                    <label class="font-w300">Organisasi</label>
                                </div>
                                    <table class="table table-striped table-bordered" id="organisasi_table" >
                                        <thead>
                                            <tr>
                                                <th  style="width:5%">No</th>
                                                <th  >Form</th>
                                                <th  style="width:5%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><button type="button" class="btn btn-info btn-sm addOrganisasi">Add</button></td>
                                            </tr>
                                        </tfoot>

                                    </table>

                            </div>
                            <!--endregion-->

                            <!--region tafiq-->
                            <div class="tab-pane" id="wizard-validation-step7" role="tabpanel">
                                <div class="form-group">
                                    <label class="font-w300">TAFIQ</label>
                                </div>
                                <h3 align='center'><span class="label label-success col-sm-12"><i
                                            class="fa fa-area-chart"> Data TAFIQ (Tazwidul Fityanil Quran)</i></span>
                                </h3>
                                <hr>
                                <br>
                                <div class="form-group">
                                    <label for="level" class="control-label">Level TAFIQ</label>
                                    <select name="level_tafiq" class="form-control level_tafiq">
                                        <option value="0">Belum Tafiq</option>
                                        <option value="1">Tafiq I</option>
                                        <option value="2">Tafiq II</option>
                                        <option value="3">Tafiq III</option>
                                    </select>
                                </div>
                                <table class="table table-striped table-bordered" id="tafiq_table" >
                                    <thead>
                                    <tr>
                                        <th  style="width:5%">No</th>
                                        <th  >Form</th>
                                        <th  style="width:5%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody >
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
{{--                                        <td><button type="button" class="btn btn-info btn-sm addTafiq">Add</button></td>--}}
                                    </tr>
                                    </tfoot>

                                </table>
                            </div>
                            <!--endregion-->
                        </div>
                        <div class="block-content block-content-sm block-content-full bg-body-light rounded-bottom">
                            <div class="row">
                                <div class="col-6">
                                    <button type="button" class="btn btn-secondary" data-wizard="prev">
                                        <i class="fa fa-angle-left mr-1"></i> Kembali
                                    </button>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="button" class="btn btn-secondary" data-wizard="next">
                                        Selanjutnya <i class="fa fa-angle-right ml-1"></i>
                                    </button>
                                    <button type="submit" class="btn btn-primary d-none" data-wizard="finish">
                                        <i class="fa fa-check mr-1"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->


    <!--region modalPC-->
    <div class="modal fade" id="myModalPC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PC</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPC" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PC</th>
                            <th>Nama PC</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--endregion-->

    <!--region modalPD-->
    <div class="modal fade" id="myModalPD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PD</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPD" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PD</th>
                            <th>Nama PD</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--endregion-->

    <!--region modalPW-->
    <div class="modal fade" id="myModalPW" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PW</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPW" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PW</th>
                            <th>Nama PW</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no =1)
                        @foreach ($dataPW as $data_pw)
                            <tr class="pilih3" data-kd_pw="{{$data_pw->kd_pw}}"
                                data-nama_pw="{{$data_pw->nama_pw}}">
                                <td>{{$no++}}</td>
                                <td>{{$data_pw->kd_pw}}</td>
                                <td>{{$data_pw->nama_pw}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--endregion-->

    <!-- Modal Croper image -->
    <div class="modal fade" id="modal-crop" data-backdrop="static" data-keyboard="false" tabindex="-1"  aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Pengaturan Gambar</h5>
                    <button id="cancel-crop-close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <div class="row">
                            <div class="col-md-8 text-center" style="background: #1b1e21">
                                <div style="min-height: 20px;max-height: 430px;width: 324px;margin: auto;">
                                    <img id="image-crop" style="object-fit: contain" src="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="cancel-crop" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>
@endsection
