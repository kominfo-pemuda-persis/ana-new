@extends('layouts.backend')

@section('content')
    <style>
        .modules {
            column-count: 3;
            column-gap: 1em;
        }
        .module {
            display: inline-block;
            margin: 0 0 1em;
            width: 100%;
        }
    </style>
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Assign Permission to Role {{ $role['display_name'] }}
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Role Permission</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">

        {!! session('message') !!}

        <!-- Dynamic Table with Export Buttons -->
        <div class="block">

            <div class="block-content block-content-full">
                <form method="POST" action="/role/{{ $role['id_role'] }}/assign">
                    @csrf
                    <div class="modules">
                        @foreach ($permissions as $module => $modulePermission)
                            <div class="module border-bottom">
                                <div class="form-group">
                                    <h4>{{ $module }}</h4>
                                    @foreach ($modulePermission as $permission)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="{{ $module }}[{{ $permission }}]" value="1" {{ in_array($permission, $myPermissions[$module]??[])?'checked':'' }}>
                                        <label class="form-check-label" for="example-checkbox-default1">{{ $permission }}</label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <button type="submit" class="btn btn-primary">Assign Permissions</button>
                </form>
            </div>


        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
@endsection