@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ route('role.datatables') }}";
    </script>
    <script src="{{ asset('js/pages/pg_role_master.js?id=') . Str::random(25)}}"></script>

@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data Role <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Data Role</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Table <small>Data Role</small></h3>
                    <button type="button" class="btn btn-success btn-sm" onclick="openModal('#modal-form','add')"><span class="si si-plus"></span> Tambah</button>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized in js/pages/tables_datatables.js -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-role">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 80px;">#</th>
                                <th style="width: 20%;">Nama</th>
                                <th style="width: 20%;">Slug</th>
                                <th class="d-none d-sm-table-cell" >Keterangan</th>
                                <th style="width: 15%;">Status</th>
                                <th class="text-center" style="width: 80px;">#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->

    <!-- Fade In Block Modal -->
    <div class="modal fade" id="modal-form" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title modal-title">Tambah Role</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <form class="form-role"  method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="block-content font-size-sm row">
                            <div class="col-md-12">
                                <input type="hidden" id="id_role" name="id_role" required>
                                <div class="form-group">
                                    <label for="display_name">Nama Role <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="display_name" name="display_name" required>
                                </div>
                                <div class="form-group">
                                    <label for="name">Slug <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label for="description">Deskripsi</label>
                                    <textarea class="form-control" id="description" name="description" required rows="4"></textarea>
                                </div>
                                <div class="form-group" id="comp_aktif">
                                    <label for="status_aktif">Status Aktif</label>
                                    <div class="custom-control custom-switch mb-1">
                                        <input type="checkbox" class="custom-control-input" id="status_aktif" name="status_aktif" >
                                        <label class="custom-control-label" for="status_aktif">Aktif</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block-content block-content-full text-right border-top">
                            <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-sm btn-success" id="btn-form" onclick="save()"><i class="far fa-save mr-1"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END Fade In Block Modal -->
@endsection

<script>
    function save() {
        $s_aktif = 0;
        if($('#status_aktif').is(':checked')){
            $s_aktif = 1;
        }else{
            $s_aktif = 0;
        }
        swal({
            title: "Peringatan",
            text: "Apa anda yakin?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Batal",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Simpan",
            closeOnConfirm: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    url: '{{ route("role.store") }}',
                    type: 'POST',
                    data: {
                        'id_role':$('#id_role').val(),
                        'display_name':$('#display_name').val(),
                        'name':$('#name').val(),
                        'description':$('#description').val(),
                        'status_aktif':$s_aktif
                    },
                    dataType: 'json',
                    success: function (resp) {

                        swal("Berhasil!", "Data telah berhasil disimpan.", "success").then((result) => {
                            location.reload(); });
                    },
                    error: function (resp) {
                        alert(resp.responseJSON.message);
                    }
                });
            }
        })
    }


</script>
