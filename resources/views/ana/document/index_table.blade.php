@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.3.3/css/fixedColumns.bootstrap4.min.css">
    <style>
        .DTFC_LeftBodyLiner {
            overflow-x: hidden
        }

        .img-caang {
            object-fit: scale-down;
            width: 100%;
        }

        table.ana-table {
            background: white !important;
        }

        .DTFC_LeftBodyLiner table.dataTable.ana-table tbody tr td {
            overflow-wrap: anywhere;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
          integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/locales.js') }}"></script>
    <script src="{{ asset('/js/plugins/exceljs.min.js') }}"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
            integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"
            integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-V33JPYB1SF"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-V33JPYB1SF');
    </script>

    <!-- Page JS Code -->
    <script>
        $(document).ready(function(){
            $("#button_upload").click(function(){
                $("#pdf_upload").modal("show");
            })
            $(".dropify-doc").dropify();
        })

        function uploadFile(el){
            let button = $(el);
            if($("#document-file").get(0).files.length <= 0){
                alert("please select file document")
            }else{
                let data = new FormData();
                data.append('file', $("#document-file").get(0).files[0])

                button.attr('disabled', 'disabled');
                button.text('menyimpan..');

                swal({
                    title: 'Upload data..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    onOpen: () => {
                        swal.showLoading()
                    }
                })

                $.ajax({
                    url: `{{ route("doc_file.upload")}}`,
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    processData: false,
                    contentType : false,
                    data,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    success: function (res) {
                        swal({
                            title: "Berhasil!",
                            text: res.message,
                            type: "success",
                            confirmButtonColor: "#3498DB",
                            closeOnConfirm: true
                        }).then((result) => {
                            button.removeAttr('disabled');
                            button.text('Upload File');
                            $("#pdf_upload").modal("hide");
                            $(".dropify-clear").trigger("click");
                            tableDoc.draw();
                        });
                    },

                    error: function (jqXHR, exception) {
                        swal.close();
                        let message = `Terjadi kendala teknis, Mohon kontak Bidang Administrasi PP. <br> info error: <b>${jqXHR.responseJSON.message}</b>`
                        swal("Upload ditolak", message, "error");
                        button.removeAttr('disabled');
                        button.text('Upload File');
                    },
                });
            }
        }

        moment.locale('id');
        let dataTableUrl = "{{ route('doc_file.datatables') }}";
        var columns = [
            {
                data: 'id',
                title: '#',
                searchable: false,
            },
            {
                data: 'anggota.nama_lengkap',
                title: 'Pemilik Dokumen',
                order: 'asc',
            },
            {
                title: 'NPA',
                data: 'anggota.npa'
            },
            {
                data: 'path',
                title: 'Nama File',
                order: 'asc',
            },
            {
                title: `Tanggal dibuat`,
                data: 'created_at',
                render: function (data) {
                    return moment(data).format("DD MMMM YYYY")
                }
            },
            {
                title: 'Aksi',
                data: 'id',
                className: 'text-center',
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    let str =
                        `
            <a onclick="setDownload(this)" data-obj='${JSON.stringify(row)}' type="button" class="text-white btn-sm btn btn-success">
              Download
            </a>
            <a onclick="setViewPdf(this)" data-obj='${JSON.stringify(row)}' type="button" class="text-white btn-sm btn btn-primary">
              View
            </a>
            `;

                if(row.can_delete){
                    str += `<a onclick="deletePdf(this)" data-obj='${JSON.stringify(row)}' type="button" class="text-white btn-sm btn btn-danger">
                        Delete
                        </a>`;
                }
                    return str;

                },
            }
        ]

        $('#dataTable-doc-file').on('draw.dt', function () {
            setTimeout(() => {
                const hiddenThead = $('.DTFC_LeftWrapper .DTFC_LeftBodyWrapper .DTFC_LeftBodyLiner .ana-table thead th');
                const realThead = $('.DTFC_LeftWrapper .DTFC_LeftHeadWrapper .ana-table thead th').map((i, el) => {
                    $(hiddenThead[i]).css("min-width", $(el).css("width"));
                    $(hiddenThead[i]).css("max-width", $(el).css("width"));
                });

            }, 0);
        });
        let tableDoc = $('#dataTable-doc-file').DataTable({
            scrollX: true,
            columnDefs: [{
                width: 100,
                targets: 0
            }],
            pageLength: 10,
            lengthMenu: [
                [5, 10, 25, 50, 100, 250, 500, 1000],
                [5, 10, 25, 50, 100, 250, 500, 1000]
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: dataTableUrl,
            },
            // ajax: dataTableUrl,
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                // index number column
                var index = iDisplayIndex + this.fnSettings()._iDisplayStart + 1;
                $('td:eq(0)', nRow).html(index);
                return nRow;
            },
            columns: columns,
            autoWidth: false,
        });

        function setViewPdf(el){
            $("#pdf_iframe").attr("src", "");
            let data = $(el).data("obj")
            let url = `https://drive.google.com/viewerng/viewer?embedded=true&url=${data.link}#toolbar=0&scrollbar=0`
            $("#pdf_viewer").modal('show');
            $("#pdf_title").text(data.path);
            $("#pdf_iframe").attr("src", url);
        }

        function deletePdf(el){
            let data = $(el).data("obj")
            swal({
                title: "Apa anda yakin?",
                text: "Data akan dihapus dan tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus!",
                closeOnConfirm: false
            }).then((result) => {
                if(result.value){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url:`{{ route('doc_file.delete')}}`,
                        type: 'POST',
                        data: { id: data.id },
                        dataType: 'json',
                        success: function(resp) {
                            swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                                tableDoc.draw();
                            });
                        },
                        error: function(resp) {
                            alert(resp.responseJSON.message);
                        }
                    });
                }
            })
        }

        function setDownload(el){
            Swal.fire({
                // 'Menyiapkan Dokumen',
                // 'Mohon tunggu, dokumen kamu sedang disiapkan.',
                // 'info'
                title: 'Menyiapkan Dokumen',
                text: "Mohon tunggu, dokumen kamu sedang disiapkan.",
                icon: 'info',
                showConfirmButton: false,
                onOpen: async () => {
                    Swal.showLoading()
                    let data = $(el).data("obj")
                    await downloadFile(data.link);
                    Swal.close()
                },
            })
        }

        function downloadFile(url, filename = '') {
            if (filename.length === 0) filename = url.split('/').pop();
            return new Promise(function(resolve, reject) {
                const req = new XMLHttpRequest();
                req.open('GET', url, true);
                req.responseType = 'blob';
                req.onload = function () {
                    const blob = new Blob([req.response], {
                        type: 'application/pdf',
                    });

                    const isIE = false || !!document.documentMode;
                    if (isIE) {
                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        const windowUrl = window.URL || window.webkitURL;
                        const href = windowUrl.createObjectURL(blob);
                        const a = document.createElement('a');
                        a.setAttribute('download', filename);
                        a.setAttribute('href', href);
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                        resolve();
                    }
                };
                req.onerror = function(){
                    reject("Network error");
                }
                req.send();
            });
            // const response = await fetch(url, {
            //     headers: {
            //         "Content-Type": "application/pdf",
            //     },
            // });
            // const blob = await response.blob();
            // console.log(blob)
            // const isIE = false || !!document.documentMode;
            // if (isIE) {
            //     window.navigator.msSaveBlob(blob, filename);
            // } else {
            //     const windowUrl = window.URL || window.webkitURL;
            //     const href = windowUrl.createObjectURL(blob);
            //     const a = document.createElement('a');
            //     a.setAttribute('download', filename);
            //     a.setAttribute('href', href);
            //     document.body.appendChild(a);
            //     a.click();
            //     document.body.removeChild(a);
            // }
        }
    </script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data Dokumen <small
                        class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Data Dokumen</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Table <small>Data Dokumen</small></h3>
                @if($role)
                <button id="button_upload" class="btn btn-success btn-sm" style="margin-right: 5px"><span
                    class="si si-plus"></span> Upload Dokumen
                </button>
                @endif
            </div>
            <div class="block-content block-content-full">
                <table id="dataTable-doc-file" class="ana-table table table-bordered">
                </table>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal PDF VIEW -->
    <div class="modal fade" id="pdf_viewer" tabindex="-1" role="dialog" aria-labelledby="pdf_viewer"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="pdf_title">Loading...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="min-height: 500px">
                    <iframe
                        src=""
                        id="pdf_iframe"
                        frameBorder="0"
                        scrolling="auto"
                        height="500px"
                        width="100%"
                    ></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add PDF -->
    <div class="modal fade" id="pdf_upload" tabindex="-1" role="dialog" aria-labelledby="pdf_viewer"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="pdf_title">Upload PDF</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                     <div class="row text-left photo-input-container">
                        <div class="col-sm-12 mx-auto text-left" style="cursor: pointer;">
                            <input type="file" name="documen" id="document-file"
                                    class="dropify-doc form-control" data-allowed-file-extensions="pdf">
                        </div>
                        {{-- <div onclick="addNewDropifyPhoto(this)" class="h-full col-lg-6 col-sm-12" style="cursor: pointer; height: 196px; margin-top: 10px">
                            <div style="position: relative; border: 2px dashed #E5E5E5; height: 100%">
                                <div class="text-center" style="position: absolute; top: 40%; width: 100%">
                                    <i class="fa fa-fw fa-plus"></i>
                                    <p style="font-size: 10px; width: 100%; text-align: center">Klik untuk tambah photo</p>
                                </div>
                            </div>
                        </div> --}}
                        <div class="mx-auto" style="padding-left: 14px;padding-right: 14px;padding-top: 10px;width: 100%;">
                            <a onclick="uploadFile(this)" type="button" class="text-white btn-sm btn btn-success">
                                Upload File
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
