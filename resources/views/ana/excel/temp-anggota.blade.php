<div>
    <div>
        <table>
            <thead>
            <tr>
                <th class="d-none d-sm-table-cell">NPA</th>
                <th class="d-none d-sm-table-cell">Nama Lengkap</th>
                <th class="d-none d-sm-table-cell">Tempat Lahir</th>
                <th class="d-none d-sm-table-cell">Tanggal Lahir</th>
                <th class="d-none d-sm-table-cell">Pekerjaan</th>
                <th class="d-none d-sm-table-cell">Gol Darah</th>
                <th class="d-none d-sm-table-cell">Email</th>
                <th class="d-none d-sm-table-cell">No Telpon</th>
                <th class="d-none d-sm-table-cell">Alamat</th>
                <th class="d-none d-sm-table-cell">Level Tafiq</th>
            </tr>
            @php
                use Faker\Factory;
                $faker = Factory::create('id_ID')
            @endphp
            @for ($i = 1; $i <= 9; $i++)
                <tr>
                    <td>{{  '20.000'. $i }}</td>
                    <td>{{  'Uzumaki Naruto' .$i }}</td>
                    <td>Konohagakure</td>
                    <td>{{ $faker->dateTimeThisCentury->format('Y-m-d')}}</td>
                    <td>{{ 'Ustadz/Guru Madrasah' }}</td>
                    <td>{{ $faker->randomElement(['A', 'B', 'AB', 'O']), }}</td>
                    <td>{{ 'uzumaki_naruto'.$i.'@konohagakure.co.jp' }}</td>
                    <td>{{ '\'+628132141150'.$i }}</td>
                    <td>{{ 'Konohagakure' }}</td>
                    <td>{{ 'BELUM' }}</td>
                </tr>
            @endfor
            </thead>
        </table>
    </div>
