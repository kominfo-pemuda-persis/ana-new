@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/hijri-datepicker/css/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/performa_pd.css') }}">
    <style>
        ul.performa-pw-tab li a{
            height: 100%;
        }
    </style>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Edit Performa PW <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Adding</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('performa.pw.index')}}">Index Performa PW</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Form <small>Edit Performa Jam'iyyah tingkat Pimpinan Wilayah</small></h3>
            </div>

            <img class="img-responsive img-rounded center-block hoverZoomLink" src="{{ asset('/media/photos/pemuda%2520persis.jpg') }}" width="150"><br>
            <h1 align="center"><bold>PIMPINAN PUSAT</bold></h1>
            <h1 align="center"><bold>PEMUDA PERSATUAN ISLAM</bold></h1>
            <hr>
            <h3 align="center"><bold>INDEKS PERFORMA JAM’IYYAH (IPJ)</bold></h3>
            <h3 align="center"><bold>UNTUK TINGKAT PIMPINAN Wilayah (PW)</bold></h3>
            <hr>
            <!--<h4 align='left'><bold>PETUNJUK PENGISIAN</bold></h4>-->
            <h3 align="left">
                <span class="label label-success col-sm-12 control-label">
                    <i class="fa fa-edit"> PETUNJUK PENGISIAN</i>
                </span>
            </h3>
            <div class="row">
                <div class="col-sm-12">&nbsp;</div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <ol>
                        <li>Silakan isi data isian di bawah ini dengan klik checkbox <i class=" fa fa-check-circle-o"></i> pada setiap pilihan sesuai dengan kondisi yang sebenarnya.</li>
                        <li>Prinsip pengisian IPJ ini adalah
                            untuk mengetahui dan memahami kondisi riil dan objektif dari setiap PW,
                            sehingga bisa diketahui potensi kekuatan apa saja yang dimiliki PW juga
                            bisa diidentifikasi masalah dan hambatan yang dihadapi oleh PW sehingga
                            bisa diformulasikan dengan tepat solusi dari setiap permasalahan
                            tersebut.</li>
                    </ol>
                </div>
            </div>

            <div class="performa-pw-wizard block-content block-content-full">
                <ul class="performa-pw-tab nav nav-tabs nav-tabs-block nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#step1" id="step1-tab" data-toggle="tab">Data Awal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step2" id="step2-tab" data-toggle="tab">Tasykil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step3" id="step3-tab" data-toggle="tab">Aktivitas Jam'iyyah & Partisipasi Anggota</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step4" id="step4-tab" data-toggle="tab">Kaderisasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step5" id="step5-tab" data-toggle="tab">Administrasi Jam'iyyah</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step6" id="step6-tab" data-toggle="tab">Musyawarah</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step7" id="step7-tab" data-toggle="tab">Keuangan Jam'iyyah</a>
                    </li>
                </ul>
                <form id="performa-pw-form">
                    @csrf
                    <div class="block-content block-content-full tab-content px-md-5" style="min-height: 300px;">
                        <div class="tab-pane active" id="step1" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-hf-email">Nomor PW</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" value="{{ $data->pw->kd_pw }}"
                                                   id="noPW" name="noPW"
                                                   placeholder="Nomor PW">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" value="{{ $data->pw->nama_pw }}"
                                                   id="namaPW" name="namaPW"
                                                   placeholder="Nama PW" readonly required>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-success" data-toggle="modal"
                                                    data-target="#myModalPW"><i class="fa fa-search mr-1"> Cari</i></button>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="pw" class="col-sm-2 control-label">Provinsi</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="provinces" id="provinces">
                                                <option value="0" disable="true" selected="true">Pilih Propinsi
                                                </option>
                                                @foreach ($provinces as $key => $value)
                                                    @if($data->provinsi == $value->id)
                                                        <option value="{{$value->id}}" selected>{{ $value->nama }}</option>
                                                    @else
                                                        <option value="{{$value->id}}">{{ $value->nama }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_pc" class="col-sm-2 control-label">Alamat Lengkap</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" value="{{ $data->alamat }}"
                                                   id="alamat" name="alamat" placeholder="Alamat Lengkap">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_pc" class="col-sm-2 control-label">Nama Ketua PW</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" value="{{ $data->ketua_pw }}"
                                                   id="ketua_pw" name="ketua_pw" placeholder="Nama Ketua PW">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_hp" class="col-sm-2 control-label">Nomor HP Ketua PW</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" value="{{ $data->no_hp }}"
                                                   id="no_hp" name="no_hp" placeholder="Nomer HP Ketua PW">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="start_periode" class="col-sm-2 control-label">Periode Mulai</label>
                                        <div class="col-sm-3">
                                            <div class="date" data-provide="datepicker"
                                                 data-date-format="dd-mm-yyyy">
                                                <input type="text" name="start_periode" required
                                                       class="form-control start-periode"
                                                       placeholder="0000-00-00">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <label for="end_periode" class="col-sm-3 control-label">Periode Berakhir</label>
                                        <div class="col-sm-4">
                                            <div class="date" data-provide="datepicker"
                                                 data-date-format="dd-mm-yyyy">
                                                <input type="text" name="end_periode" required
                                                       class="form-control end-periode"
                                                       placeholder="0000-00-00">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="exampleInputFile" class="col-sm-2 control-label">Foto</label>
                                        <div class="col-sm-10">
                                            <div tabindex="500" class="btn btn-success btn-file"><i
                                                    class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">Pilih Foto Diri</span><input
                                                    type="file" name="foto" id="foto" accept="image/*" class=""></div>
                                        </div>
                                        <div class="col-md-12 mb-2">
                                            @if(!$data->foto || $data->foto=="default.png")
                                            <img id="preview-image" alt="preview image" style="max-height: 250px;" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif" >
                                            @elseif($data->pw)
                                            <img id="preview-image" alt="preview image" style="max-height: 250px;" src="{{ Storage::disk('s3')->url('images/performa/'.($data->pw?$data->pw->nama_pw:'pw').'/'.$data->foto) }}" >
                                            @else
                                            <img id="preview-image" alt="preview image" style="max-height: 250px;" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif" >
                                            @endif

                                            <!-- <img id="preview-image"
                                                src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
                                                alt="preview image" style="max-height: 250px;"> -->
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        {{-- tasykil --}}
                        <div class="tab-pane" id="step2" role="tabpanel">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <ol>
                                            <div class="col-sm-12">
                                                <li>Apakah komposisi tasykil utuh sesuai SK pengesahan?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="20" value="Utuh sesuai SK"
                                                               @if($data->q1a == "Utuh sesuai SK") checked @endif required=""> Utuh sesuai SK <br>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="15" value="70'%' sesuai SK"
                                                               @if($data->q1a == "70'%' sesuai SK") checked @endif required=""> 70% sesuai SK <br>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="10" value="50'%' sesuai SK"
                                                               @if($data->q1a == "50'%' sesuai SK") checked @endif required=""> 50% sesuai SK <br>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="5" value="25'%' sesuai SK"
                                                               @if($data->q1a == "25'%' sesuai SK") checked @endif required=""> 25% sesuai SK <br>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="2" value="Kurang 25% sesuai SK"
                                                               @if($data->q1a == "Kurang 25% sesuai SK") checked @endif required=""> Kurang 25% sesuai SK <br>
                                                    </label>
                                                </div><br>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Apakah setiap tasykil mempunyai pedoman jam'iyyah?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q2a" data-category="c1" data-point="20" value="Semua mempunyai"
                                                               @if($data->q2a == "Semua mempunyai") checked @endif required=""> Semua mempunyai <br>
                                                        <input type="radio" class="flat-red" name="q2a" data-category="c1" data-point="10" value="Sebagian besar mempunyai"
                                                               @if($data->q2a == "Sebagian besar mempunyai") checked @endif required=""> Sebagian besar mempunyai <br>
                                                        <input type="radio" class="flat-red" name="q2a" data-category="c1" data-point="5" value="Sebagian besar tidak mempunyai"
                                                               @if($data->q2a == "Sebagian besar tidak mempunyai") checked @endif required=""> Sebagian besar tidak mempunyai <br>
                                                        <input type="radio" class="flat-red" name="q2a" data-category="c1" data-point="2" value="Semua tidak mempunyai"
                                                               @if($data->q2a == "Semua tidak mempunyai") checked @endif required=""> Semua tidak mempunyai <br>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Apakah setiap tasykil memahami tugas pokoknya masing-masing?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q3a" data-category="c1" data-point="20" value="Semua memahami"
                                                               @if($data->q3a == "Semua memahami") checked @endif required=""> Semua memahami <br>
                                                        <input type="radio" class="flat-red" name="q3a" data-category="c1" data-point="10" value="Sebagian besar memahami"
                                                               @if($data->q3a == "Sebagian besar memahami") checked @endif required=""> Sebagian besar memahami <br>
                                                        <input type="radio" class="flat-red" name="q3a" data-category="c1" data-point="5" value="Sebagian besar tidak memahami"
                                                               @if($data->q3a == "Sebagian besar tidak memahami") checked @endif required=""> Sebagian besar tidak memahami <br>
                                                        <input type="radio" class="flat-red" name="q3a" data-category="c1" data-point="2" value="Semua tidak memahami"
                                                               @if($data->q3a == "Semua tidak memahami") checked @endif required=""> Semua tidak memahami <br>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Apakah setiap tasykil membuat laporan kinerja secara berkala?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q4a" data-category="c1" data-point="20" value="Membuat secara berkala"
                                                               @if($data->q4a == "Membuat secara berkala") checked @endif required=""> Membuat secara berkala <br>
                                                        <input type="radio" class="flat-red" name="q4a" data-category="c1" data-point="5" value="Pernah membuat"
                                                               @if($data->q4a == "Pernah membuat") checked @endif required=""> Pernah membuat <br>
                                                        <input type="radio" class="flat-red" name="q4a" data-category="c1" data-point="2" value="Tidak pernah"
                                                               @if($data->q4a == "Tidak pernah") checked @endif required=""> Tidak pernah <br>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Apakah Ketua membuat progress report tahunan?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q5a" data-category="c1" data-point="20" value="Membuat secara berkala"
                                                               @if($data->q5a == "Membuat secara berkala") checked @endif required=""> Membuat secara berkala <br>
                                                        <input type="radio" class="flat-red" name="q5a" data-category="c1" data-point="5" value="Pernah membuat"
                                                               @if($data->q5a == "Pernah membuat") checked @endif required=""> Pernah membuat <br>
                                                        <input type="radio" class="flat-red" name="q5a" data-category="c1" data-point="2" value="Tidak pernah"
                                                               @if($data->q5a == "Tidak pernah") checked @endif required=""> Tidak pernah <br>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Berjalankah jadwal ngantor rutin tasykil (minimal tiga kali sebulan)?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q6a" data-category="c1" data-point="20" value="Berjalan > tiga kali sebulan"
                                                               @if($data->q6a == "Berjalan > tiga kali sebulan") checked @endif required=""> Berjalan > tiga kali sebulan <br>
                                                        <input type="radio" class="flat-red" name="q6a" data-category="c1" data-point="5" value="Berjalan < tiga kali sebulan"
                                                               @if($data->q6a == "Berjalan < tiga kali sebulan") checked @endif required=""> Berjalan < tiga kali sebulan <br>
                                                        <input type="radio" class="flat-red" name="q6a" data-category="c1" data-point="2" value="Tidak berjalan"
                                                               @if($data->q6a == "Tidak berjalan") checked @endif required=""> Tidak berjalan <br>
                                                    </label>
                                                </div>
                                            </div>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--  Aktivitas Jam'iyyah & Partisipasi Anggota --}}
                        <div class="tab-pane" id="step3" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <li>Apakah jumlah anggota di atas 25 orang?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q1b" data-category="c2" data-point="10" value="Jumlah anggota > 25 orang"
                                                   @if($data->q1b == "Jumlah anggota > 25 orang") checked @endif required=""> Jumlah anggota > 25 orang <br>
                                            <input type="radio" class="flat-red" name="q1b" data-category="c2" data-point="2" value="Jumlah anggota kurang dari 25 orang"
                                                   @if($data->q1b == "Jumlah anggota kurang dari 25 orang") checked @endif required=""> Jumlah anggota kurang dari 25 orang <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Bagaimana frekuensi pertemuan tasykil dengan anggota?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="10" value="Sering dengan seluruh anggota"
                                                   @if($data->q2b == "Sering dengan seluruh anggota") checked @endif required=""> Sering dengan seluruh anggota <br>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="8" value="Sering dengan sebagian besar anggota"
                                                   @if($data->q2b == "Sering dengan sebagian besar anggota") checked @endif required=""> Sering dengan sebagian besar anggota <br>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="6" value="Sering dengan sebagian kecil anggota"
                                                   @if($data->q2b == "Sering dengan sebagian kecil anggota") checked @endif required=""> Sering dengan sebagian kecil anggota <br>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="4" value="Jarang"
                                                   @if($data->q2b == "Jarang") checked @endif required=""> Jarang <br>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="2" value="Tidak pernah"
                                                   @if($data->q2b == "Tidak pernah") checked @endif required=""> Tidak pernah <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah PW memiliki kegiatan rutin (minimal tiga kali sebulan)?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q3b" data-category="c2" data-point="10" value="Berjalan > tiga kali sebulan"
                                                   @if($data->q3b == "Berjalan > tiga kali sebulan") checked @endif required=""> Berjalan > tiga kali sebulan <br>
                                            <input type="radio" class="flat-red" name="q3b" data-category="c2" data-point="4" value="Berjalan < tiga kali sebulan"
                                                   @if($data->q3b == "Berjalan < tiga kali sebulan") checked @endif required=""> Berjalan < tiga kali sebulan <br>
                                            <input type="radio" class="flat-red" name="q3b" data-category="c2" data-point="2" value="Tidak berjalan"
                                                   @if($data->q3b == "Tidak berjalan") checked @endif required=""> Tidak berjalan <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah kegiatan rutin itu berjalan dengan lancar?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q4b" data-category="c2" data-point="10" value="Kegiatan rutin > tiga kali sebulan"
                                                   @if($data->q4b == "Kegiatan rutin > tiga kali sebulan") checked @endif required=""> Kegiatan rutin > tiga kali sebulan <br>
                                            <input type="radio" class="flat-red" name="q4b" data-category="c2" data-point="4" value="Kegiatan rutin jamaah kurang dari tiga kali sebulan"
                                                   @if($data->q4b == "Kegiatan rutin jamaah kurang dari tiga kali sebulan") checked @endif required=""> Kegiatan rutin jamaah kurang dari tiga kali sebulan <br>
                                            <input type="radio" class="flat-red" name="q4b" data-category="c2" data-point="2" value="Tidak ada"
                                                   @if($data->q4b == "Tidak ada") checked @endif required=""> Tidak ada <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Bagaimana rata-rata kehadiran anggota dalam kegiatan rutin PW? (sertakan bukti fisik)</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q5b" data-category="c2" data-point="10" value="100% anggota hadir"
                                                   @if($data->q5b == "100% anggota hadir") checked @endif required=""> 100% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q5b" data-category="c2" data-point="6" value="75% anggota hadir"
                                                   @if($data->q5b == "75% anggota hadir") checked @endif required=""> 75% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q5b" data-category="c2" data-point="4" value="50% anggota hadir"
                                                   @if($data->q5b == "50% anggota hadir") checked @endif required=""> 50% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q5b" data-category="c2" data-point="2" value="Kurang dari 50% anggota hadir"
                                                   @if($data->q5b == "Kurang dari 50% anggota hadir") checked @endif required=""> Kurang dari 50% anggota hadir <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah ada kegiatan insidental di PW?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q6b" data-category="c2" data-point="10" value="Ada"
                                                   @if($data->q6b == "Ada") checked @endif required=""> Ada <br>
                                            <input type="radio" class="flat-red" name="q6b" data-category="c2" data-point="2" value="Tidak"
                                                   @if($data->q6b == "Tidak") checked @endif required=""> Tidak <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Bagaimana rata-rata kehadiran anggota dalam kegiatan insidental PW? (sertakan bukti fisik)</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q7b" data-category="c2" data-point="10" value="100% anggota hadir"
                                                   @if($data->q7b == "100% anggota hadir") checked @endif required=""> 100% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q7b" data-category="c2" data-point="6" value="75% anggota hadir"
                                                   @if($data->q7b == "75% anggota hadir") checked @endif required=""> 75% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q7b" data-category="c2" data-point="4" value="50% anggota hadir"
                                                   @if($data->q7b == "50% anggota hadir") checked @endif required=""> 50% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q7b" data-category="c2" data-point="2" value="Kurang dari 50% anggota hadir"
                                                   @if($data->q7b == "Kurang dari 50% anggota hadir") checked @endif required=""> Kurang dari 50% anggota hadir<br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah ada kegiatan jamiyyah bersama Persis dan otonom lainnya?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q8b" data-category="c2" data-point="10" value="Ada dan selalu mengikuti"
                                                   @if($data->q8b == "Ada dan selalu mengikuti") checked @endif required=""> Ada dan selalu mengikuti <br>
                                            <input type="radio" class="flat-red" name="q8b" data-category="c2" data-point="6" value="Ada dan sering mengikuti"
                                                   @if($data->q8b == "Ada dan sering mengikuti") checked @endif required=""> Ada dan sering mengikuti <br>
                                            <input type="radio" class="flat-red" name="q8b" data-category="c2" data-point="4" value="Ada dan jarang mengikuti"
                                                   @if($data->q8b == "Ada dan jarang mengikuti") checked @endif required=""> Ada dan jarang mengikuti <br>
                                            <input type="radio" class="flat-red" name="q8b" data-category="c2" data-point="2" value="Tidak ada"
                                                   @if($data->q8b == "Tidak ada") checked @endif required=""> Tidak ada <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah ada kegiatan yang melibatkan organisasi lain?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q9b" data-category="c2" data-point="10" value="Ada dan selalu mengikuti"
                                                   @if($data->q9b == "Ada dan selalu mengikuti") checked @endif required=""> Ada dan selalu mengikuti <br>
                                            <input type="radio" class="flat-red" name="q9b" data-category="c2" data-point="6" value="Ada dan sering mengikuti"
                                                   @if($data->q9b == "Ada dan sering mengikuti") checked @endif required=""> Ada dan sering mengikuti <br>
                                            <input type="radio" class="flat-red" name="q9b" data-category="c2" data-point="4" value="Ada dan jarang mengikuti"
                                                   @if($data->q9b == "Ada dan jarang mengikuti") checked @endif required=""> Ada dan jarang mengikuti <br>
                                            <input type="radio" class="flat-red" name="q9b" data-category="c2" data-point="2" value="Tidak ada"
                                                   @if($data->q9b == "Tidak ada") checked @endif required=""> Tidak ada <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Bagaimana publikasi/undangan pada setiap kegiatan PW?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="10" value="Setiap kegiatan ada publikasi/undangan tertulis"
                                                   @if($data->q10b == "Setiap kegiatan ada publikasi/undangan tertulis") checked @endif required=""> Setiap kegiatan ada publikasi/undangan tertulis <br>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="8" value="Setiap kegiatan ada publikasi/undangan lisan"
                                                   @if($data->q10b == "Setiap kegiatan ada publikasi/undangan lisan") checked @endif required=""> Setiap kegiatan ada publikasi/undangan lisan <br>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="6" value="Sebagian besar kegiatan ada publikasi/undangan tertulis"
                                                   @if($data->q10b == "Sebagian besar kegiatan ada publikasi/undangan tertulis") checked @endif required=""> Sebagian besar kegiatan ada publikasi/undangan tertulis <br>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="4" value="Sebagian besar kegiatan ada publikasi/undangan lisan"
                                                   @if($data->q10b == "Sebagian besar kegiatan ada publikasi/undangan lisan") checked @endif required=""> Sebagian besar kegiatan ada publikasi/undangan lisan <br>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="2" value="Tidak ada"
                                                   @if($data->q10b == "Tidak ada") checked @endif required=""> Tidak ada <br>
                                        </label>
                                    </div>
                                </div>
                            </ol>
                        </div>

                        {{--  Kaderisasi --}}
                        <div class="tab-pane" id="step4" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Jumlah calon anggota yang tercatat ?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q1c" data-category="c3" data-point="40" value="Lebih dari 10 orang"
                                                       @if($data->q1c == "Lebih dari 10 orang") checked @endif required=""> Lebih dari 10 orang <br>
                                                <input type="radio" class="flat-red" name="q1c" data-category="c3" data-point="10" value="Kurang dari 10 orang"
                                                       @if($data->q1c == "Kurang dari 10 orang") checked @endif required=""> Kurang dari 10 orang <br>
                                                <input type="radio" class="flat-red" name="q1c" data-category="c3" data-point="5" value="Tidak ada"
                                                       @if($data->q1c == "Tidak ada") checked @endif required=""> Tidak ada <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Adakah program pembinaan bagi para calon anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q2c" data-category="c3" data-point="40" value="Ada"
                                                       @if($data->q2c == "Ada") checked @endif required=""> Ada <br>
                                                <input type="radio" class="flat-red" name="q2c" data-category="c3" data-point="5" value="Tidak ada"
                                                       @if($data->q2c == "Tidak ada") checked @endif required=""> Tidak ada <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Jumlah calon anggota yang mengikuti program pembinaan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q3c" data-category="c3" data-point="40" value="Semua calon anggota yang tercatat mengikuti program pembinaan"
                                                       @if($data->q3c == "Semua calon anggota yang tercatat mengikuti program pembinaan") checked @endif required=""> Semua calon anggota yang tercatat mengikuti program pembinaan <br>
                                                <input type="radio" class="flat-red" name="q3c" data-category="c3" data-point="20" value="Sebagian besar calon  anggota yang tercatat mengikuti program pembinaan"
                                                       @if($data->q3c == "Sebagian besar calon  anggota yang tercatat mengikuti program pembinaan") checked @endif required=""> Sebagian besar calon  anggota yang tercatat mengikuti program pembinaan <br>
                                                <input type="radio" class="flat-red" name="q3c" data-category="c3" data-point="10" value="Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan"
                                                       @if($data->q3c == "Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan") checked @endif required=""> Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan <br>
                                                <input type="radio" class="flat-red" name="q3c" data-category="c3" data-point="5" value="Tidak ada calon anggota yang mengikuti program pembinaan"
                                                       @if($data->q3c == "Tidak ada calon anggota yang mengikuti program pembinaan") checked @endif required=""> Tidak ada calon anggota yang mengikuti program pembinaan <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </ol>
                        </div>

                        {{-- step5 Administrasi jamiyyah --}}
                        <div class="tab-pane" id="step5" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki sekretariat yang definitif?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q1d" data-category="c4" data-point="10" value="Sudah"
                                                       @if($data->q1d == "Sudah") checked @endif required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q1d" data-category="c4" data-point="2" value="Belum"
                                                       @if($data->q1d == "Belum") checked @endif required=""> Belum <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah sekretariat sudah berfungsi sebagai sentra kegiatan jam'iyyah (rapat, ngantor,dll)?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q2d" data-category="c4" data-point="10" value="Sudah"
                                                       @if($data->q2d == "Sudah") checked @endif required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q2d" data-category="c4" data-point="6" value="Sebagian besar sudah"
                                                       @if($data->q2d == "Sebagian besar sudah") checked @endif required=""> Sebagian besar sudah <br>
                                                <input type="radio" class="flat-red" name="q2d" data-category="c4" data-point="4" value="Sebagian besar belum"
                                                       @if($data->q2d == "Sebagian besar belum") checked @endif required=""> Sebagian besar belum <br>
                                                <input type="radio" class="flat-red" name="q2d" data-category="c4" data-point="2" value="Belum"
                                                       @if($data->q2d == "Belum") checked @endif required=""> Belum <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW memiliki perangkat kesekretariatan (komputer/mesin tik)?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q3d" data-category="c4" data-point="10" value="Sudah"
                                                       @if($data->q3d == "Sudah") checked @endif required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q3d" data-category="c4" data-point="2" value="Belum"
                                                       @if($data->q3d == "Belum") checked @endif required=""> Belum <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki Buku Agenda Surat Masuk?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q4d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q4d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q4d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q4d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Agenda Surat Masuk tersebut sudah digunakan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="10" value="Selalu digunakan"
                                                       @if($data->q5d == "Selalu digunakan") checked @endif required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="8" value="Sering digunakan"
                                                       @if($data->q5d == "Sering digunakan") checked @endif required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="6" value="Jarang digunakan"
                                                       @if($data->q5d == "Jarang digunakan") checked @endif required=""> Jarang digunakan <br>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="4" value="Belum digunakan"
                                                       @if($data->q5d == "Belum digunakan") checked @endif required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="2" value="Tidak ada Buku Agenda Surat Masuk"
                                                       @if($data->q5d == "Tidak ada Buku Agenda Surat Masuk") checked @endif required=""> Tidak ada Buku Agenda Surat Masuk <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah surat-surat masuk minimal satu tahun terakhir sudah diarsipkan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q6d" data-category="c4" data-point="10" value="Seluruh surat masuk diarsipkan"
                                                       @if($data->q6d == "Seluruh surat masuk diarsipkan") checked @endif required=""> Seluruh surat masuk diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q6d" data-category="c4" data-point="6" value="Sebagian besar surat masuk diarsipkan"
                                                       @if($data->q6d == "Sebagian besar surat masuk diarsipkan") checked @endif required=""> Sebagian besar surat masuk diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q6d" data-category="c4" data-point="4" value="Sebagian besar surat masuk tidak diarsipkan"
                                                       @if($data->q6d == "Sebagian besar surat masuk tidak diarsipkan") checked @endif required=""> Sebagian besar surat masuk tidak diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q6d" data-category="c4" data-point="2" value="Surat-surat masuk tidak diarsipkan"
                                                       @if($data->q6d == "Surat-surat masuk tidak diarsipkan") checked @endif required=""> Surat-surat masuk tidak diarsipkan <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki Buku Agenda Surat Keluar?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q7d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q7d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q7d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q7d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Agenda Surat Keluar tersebut sudah digunakan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="10" value="Selalu digunakan"
                                                       @if($data->q8d == "Selalu digunakan") checked @endif required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="8" value="Sering digunakan"
                                                       @if($data->q8d == "Sering digunakan") checked @endif required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="6" value="Jarang digunakan"
                                                       @if($data->q8d == "Jarang digunakan") checked @endif required=""> Jarang digunakan<br>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="4" value="Belum digunakan"
                                                       @if($data->q8d == "Belum digunakan") checked @endif required=""> Belum digunakan<br>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="2" value="Tidak ada Buku Agenda Surat Keluar"
                                                       @if($data->q8d == "Tidak ada Buku Agenda Surat Keluar") checked @endif required=""> Tidak ada Buku Agenda Surat Keluar<br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah surat-surat keluar minimal satu tahun terakhir sudah diarsipkan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q9d" data-category="c4" data-point="10" value="Seluruh surat keluar diarsipkan"
                                                       @if($data->q9d == "Seluruh surat keluar diarsipkan") checked @endif required=""> Seluruh surat keluar diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q9d" data-category="c4" data-point="6" value="Sebagian besar surat keluar diarsipkan"
                                                       @if($data->q9d == "Sebagian besar surat keluar diarsipkan") checked @endif required=""> Sebagian besar surat keluar diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q9d" data-category="c4" data-point="4" value="Sebagian besar surat keluar tidak diarsipkan"
                                                       @if($data->q9d == "Sebagian besar surat keluar tidak diarsipkan") checked @endif required=""> Sebagian besar surat keluar tidak diarsipkan<br>
                                                <input type="radio" class="flat-red" name="q9d" data-category="c4" data-point="2" value="Surat-surat keluar tidak diarsipkan"
                                                       @if($data->q9d == "Surat-surat keluar tidak diarsipkan") checked @endif required=""> Surat-surat keluar tidak diarsipkan<br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki Buku Induk Anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q10d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q10d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q10d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q10d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Induk Anggota sudah digunakan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q11d" data-category="c4" data-point="10" value="Sudah digunakan"
                                                       @if($data->q11d == "Sudah digunakan") checked @endif required=""> Sudah digunakan <br>
                                                <input type="radio" class="flat-red" name="q11d" data-category="c4" data-point="4" value="Belum digunakan"
                                                       @if($data->q11d == "Belum digunakan") checked @endif required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q11d" data-category="c4" data-point="2" value="Tidak ada Buku Induk Anggota"
                                                       @if($data->q11d == "Tidak ada Buku Induk Anggota") checked @endif required=""> Tidak ada Buku Induk Anggota <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah semua data dalam Buku Induk Anggota sudah terisi?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="10" value="Sudah terisi"
                                                       @if($data->q12d == "Sudah terisi") checked @endif required=""> Sudah terisi <br>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="8" value="Sebagian besar data sudah diisi"
                                                       @if($data->q12d == "Sebagian besar data sudah diisi") checked @endif required=""> Sebagian besar data sudah diisi <br>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="6" value="Sebagian besar data belum diisi"
                                                       @if($data->q12d == "Sebagian besar data belum diisi") checked @endif required=""> Sebagian besar data belum diisi <br>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="4" value="Belum terisi"
                                                       @if($data->q12d == "Belum terisi") checked @endif required=""> Belum terisi <br>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="2" value="Tidak ada Buku Induk Anggota"
                                                       @if($data->q12d == "Tidak ada Buku Induk Anggota") checked @endif required=""> Tidak ada Buku Induk Anggota <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah data yang terdapat dalam Buku Induk Anggota sesuai dengan keadaan sesungguhnya?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="10" value="Sesuai"
                                                       @if($data->q13d == "Sesuai") checked @endif required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="8" value="Sebagian besar sesuai"
                                                       @if($data->q13d == "Sebagian besar sesuai") checked @endif required=""> Sebagian besar sesuai <br>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="6" value="Sebagian besar tidak sesuai"
                                                       @if($data->q13d == "Sebagian besar tidak sesuai") checked @endif required=""> Sebagian besar tidak sesuai <br>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="4" value="Tidak sesuai"
                                                       @if($data->q13d == "Tidak sesuai") checked @endif required=""> Tidak sesuai <br>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="2" value="Tidak ada Buku Induk Anggota"
                                                       @if($data->q13d == "Tidak ada Buku Induk Anggota") checked @endif required=""> Tidak ada Buku Induk Anggota <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki kop surat?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q14d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q14d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q14d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q14d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah kop surat tersebut sudah sesuai dengan Pedoman Administrasi Jam’iyyah?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q15d" data-category="c4" data-point="10" value="Sesuai"
                                                       @if($data->q15d == "Sesuai") checked @endif required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q15d" data-category="c4" data-point="2" value="Tidak sesuai"
                                                       @if($data->q15d == "Tidak sesuai") checked @endif required=""> Tidak sesuai <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki stempel?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q16d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q16d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q16d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q16d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah stempel tersebut sesuai dengan Pedoman Administrasi Jam’iyyah?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q17d" data-category="c4" data-point="10" value="Sesuai"
                                                       @if($data->q17d == "Sesuai") checked @endif required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q17d" data-category="c4" data-point="2" value="Tidak sesuai"
                                                       @if($data->q17d == "Tidak sesuai") checked @endif required=""> Tidak sesuai <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah surat resmi PW sudah sesuai dengan Pedoman Administrasi (bentuk, format, penomoran, dll)?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q18d" data-category="c4" data-point="10" value="Sesuai"
                                                       @if($data->q18d == "Sesuai") checked @endif required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q18d" data-category="c4" data-point="2" value="Tidak sesuai"
                                                       @if($data->q18d == "Tidak sesuai") checked @endif required=""> Tidak sesuai <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki Buku Agenda Kegiatan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q19d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q19d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q19d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q19d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Agenda Kegiatan sudah digunakan sebagaimana mestinya?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="10" value="Selalu digunakan"
                                                       @if($data->q20d == "Selalu digunakan") checked @endif required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="8" value="Sering digunakan"
                                                       @if($data->q20d == "Sering digunakan") checked @endif required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="6" value="Jarang digunakan"
                                                       @if($data->q20d == "Jarang digunakan") checked @endif required=""> Jarang digunakan <br>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="4" value="Belum digunakan"
                                                       @if($data->q20d == "Belum digunakan") checked @endif required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="2" value="Tidak ada Buku Agenda Kegiatan"
                                                       @if($data->q20d == "Tidak ada Buku Agenda Kegiatan") checked @endif required=""> Tidak ada Buku Agenda Kegiatan <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah jamaah sudah memiliki Buku Daftar Hadir Kegiatan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q21d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q21d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q21d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q21d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Daftar Hadir Kegiatan dipakai dalam setiap kegiatan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="10" value="Selalu digunakan"
                                                       @if($data->q22d == "Selalu digunakan") checked @endif required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="8" value="Sering digunakan"
                                                       @if($data->q22d == "Sering digunakan") checked @endif required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="6" value="Jarang digunakan"
                                                       @if($data->q22d == "Jarang digunakan") checked @endif required=""> Jarang digunakan <br>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="4" value="Belum digunakan"
                                                       @if($data->q22d == "Belum digunakan") checked @endif required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="2" value="Tidak ada Buku Daftar Hadir Kegiatan"
                                                       @if($data->q22d == "Tidak ada Buku Daftar Hadir Kegiatan") checked @endif required=""> Tidak ada Buku Daftar Hadir Kegiatan <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki Buku Notulen Rapat/Musyawarah?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q23d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q23d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q23d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q23d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Notulen tersebut dipakai dalam setiap rapat/musyawarah?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="10" value="Selalu digunakan"
                                                       @if($data->q24d == "Selalu digunakan") checked @endif required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="8" value="Sering digunakan"
                                                       @if($data->q24d == "Sering digunakan") checked @endif required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="6" value="Jarang digunakan"
                                                       @if($data->q24d == "Jarang digunakan") checked @endif required=""> Jarang digunakan <br>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="4" value="Belum digunakan"
                                                       @if($data->q24d == "Belum digunakan") checked @endif required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="2" value="Tidak ada Buku Notulen"
                                                       @if($data->q24d == "Tidak ada Buku Notulen") checked @endif required=""> Tidak ada Buku Notulen <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW sudah memiliki Buku Daftar Tamu?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q25d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q25d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q25d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q25d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah jamaah sudah memiliki Buku Inventaris?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q26d" data-category="c4" data-point="10" value="Memiliki"
                                                       @if($data->q26d == "Memiliki") checked @endif required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q26d" data-category="c4" data-point="2" value="Belum memiliki"
                                                       @if($data->q26d == "Belum memiliki") checked @endif required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah semua data dalam buku inventaris tersebut lengkap?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="10" value="Sudah"
                                                       @if($data->q27d == "Sudah") checked @endif required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="8" value="Sebagian besar data sudah lengkap diisi"
                                                       @if($data->q27d == "Sebagian besar data sudah lengkap diisi") checked @endif required=""> Sebagian besar data sudah lengkap diisi <br>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="6" value="Sebagian besar data belum lengkap diisi"
                                                       @if($data->q27d == "Sebagian besar data belum lengkap diisi") checked @endif required=""> Sebagian besar data belum lengkap diisi <br>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="4" value="Belum"
                                                       @if($data->q27d == "Belum") checked @endif required=""> Belum <br>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="2" value="Tidak ada Buku Inventaris"
                                                       @if($data->q27d == "Tidak ada Buku Inventaris") checked @endif required=""> Tidak ada Buku Inventaris <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah data dalam buku inventaris sudah sesuai dengan kenyataannya?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="10" value="Sudah"
                                                       @if($data->q28d == "Sudah") checked @endif required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="8" value="Sebagian besar data sudah sesuai"
                                                       @if($data->q28d == "Sebagian besar data sudah sesuai") checked @endif required=""> Sebagian besar data sudah sesuai <br>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="6" value="Sebagian besar data belum sesuai"
                                                       @if($data->q28d == "Sebagian besar data belum sesuai") checked @endif required=""> Sebagian besar data belum sesuai <br>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="4" value="Belum"
                                                       @if($data->q28d == "Belum") checked @endif required=""> Belum <br>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="2" value="Tidak ada Buku Inventaris"
                                                       @if($data->q28d == "Tidak ada Buku Inventaris") checked @endif required=""> Tidak ada Buku Inventaris <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </ol>
                        </div>

                        {{-- step6 Musyawarah --}}
                        <div class="tab-pane" id="step6" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah masa jihad PW sesuai masa jihad yang tertuang dalam SK pengesahan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q1e" data-category="c5" data-point="10" value="Sesuai"
                                                       @if($data->q1e == "Sesuai") checked @endif required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q1e" data-category="c5" data-point="2" value="Tidak sesuai"
                                                       @if($data->q1e == "Tidak sesuai") checked @endif required=""> Tidak sesuai <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah SK pengesahan PW/tasykil masih ada?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q2e" data-category="c5" data-point="10" value="Ada"
                                                       @if($data->q2e == "Ada") checked @endif required=""> Ada <br>
                                                <input type="radio" class="flat-red" name="q2e" data-category="c5" data-point="2" value="Tidak ada"
                                                       @if($data->q2e == "Tidak ada") checked @endif required=""> Tidak ada <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Berapa kali PW melakukan musyawarah kerja selama masa jihad?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q3e" data-category="c5" data-point="10" value="Satu tahun satu kali"
                                                       @if($data->q3e == "Satu tahun satu kali") checked @endif required=""> Satu tahun satu kali <br>
                                                <input type="radio" class="flat-red" name="q3e" data-category="c5" data-point="4" value="Satu kali"
                                                       @if($data->q3e == "Satu kali") checked @endif required=""> Satu kali <br>
                                                <input type="radio" class="flat-red" name="q3e" data-category="c5" data-point="2" value="Belum pernah"
                                                       @if($data->q3e == "Belum pernah") checked @endif required=""> Belum pernah <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PD-PD?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q4e" data-category="c5" data-point="10" value="Ya"
                                                       @if($data->q4e == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q4e" data-category="c5" data-point="2" value="Tidak"
                                                       @if($data->q4e == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PP Pemuda?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q5e" data-category="c5" data-point="10" value="Ya"
                                                       @if($data->q5e == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q5e" data-category="c5" data-point="2" value="Tidak"
                                                       @if($data->q5e == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PW Persis?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q6e" data-category="c5" data-point="10" value="Ya"
                                                       @if($data->q6e == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q6e" data-category="c5" data-point="2" value="Tidak"
                                                       @if($data->q6e == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PW Persistri?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q7e" data-category="c5" data-point="10" value="Ya"
                                                       @if($data->q7e == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q7e" data-category="c5" data-point="2" value="Tidak"
                                                       @if($data->q7e == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PW Pemudi?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q8e" data-category="c5" data-point="10" value="Ya"
                                                       @if($data->q8e == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q8e" data-category="c5" data-point="2" value="Tidak"
                                                       @if($data->q8e == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Berapa kali PW melakukan rapat koordinasi dengan PD dalam satu tahun?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q9e" data-category="c5" data-point="10" value="> 6 kali setahun"
                                                       @if($data->q9e == "> 6 kali setahun") checked @endif required=""> > 6 kali setahun <br>
                                                <input type="radio" class="flat-red" name="q9e" data-category="c5" data-point="6" value=Antara 3 – 5 kali setahun
                                                       @if($data->q9e == "Antara 3 – 5 kali setahun") checked @endif required=""> Antara 3 – 5 kali setahun <br>
                                                <input type="radio" class="flat-red" name="q9e" data-category="c5" data-point="4" value="Kurang dari 3 kali setahun"
                                                       @if($data->q9e == "Kurang dari 3 kali setahun") checked @endif required=""> Kurang dari 3 kali setahun <br>
                                                <input type="radio" class="flat-red" name="q9e" data-category="c5" data-point="2" value="Belum pernah"
                                                       @if($data->q9e == "Belum pernah") checked @endif required=""> Belum pernah <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Bagaimana rata-rata kehadiran PD dalam rapat koordinasi tersebut?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q10e" data-category="c5" data-point="10" value="Semua PJ hadir"
                                                       @if($data->q10e == "Semua PJ hadir") checked @endif required=""> Semua PJ hadir <br>
                                                <input type="radio" class="flat-red" name="q10e" data-category="c5" data-point="6" value="75% PJ hadir"
                                                       @if($data->q10e == "75% PJ hadir") checked @endif required=""> 75% PJ hadir <br>
                                                <input type="radio" class="flat-red" name="q10e" data-category="c5" data-point="4" value="50% PJ hadir"
                                                       @if($data->q10e == "50% PJ hadir") checked @endif required=""> 50% PJ hadir <br>
                                                <input type="radio" class="flat-red" name="q10e" data-category="c5" data-point="2" value="Kurang dari 50% PJ hadir"
                                                       @if($data->q10e == "Kurang dari 50% PJ hadir") checked @endif required=""> Kurang dari 50% PJ hadir <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </ol>
                        </div>

                        {{-- step7 Keuangan jamiyyah --}}
                        <div class="tab-pane" id="step7" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Bagaimana partisipasi anggota dalam iuran wajib?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q1f" data-category="c6" data-point="40" value="100% anggota memenuhi"
                                                       @if($data->q1f == "100% anggota memenuhi") checked @endif required=""> 100% anggota memenuhi <br>
                                                <input type="radio" class="flat-red" name="q1f" data-category="c6" data-point="20" value="75% anggota memenuhi"
                                                       @if($data->q1f == "75% anggota memenuhi") checked @endif required=""> 75% anggota memenuhi <br>
                                                <input type="radio" class="flat-red" name="q1f" data-category="c6" data-point="10" value="50% anggota memenuhi"
                                                       @if($data->q1f == "50% anggota memenuhi") checked @endif required=""> 50% anggota memenuhi <br>
                                                <input type="radio" class="flat-red" name="q1f" data-category="c6" data-point="5" value="Kurang dari 50% anggota memenuhi"
                                                       @if($data->q1f == "Kurang dari 50% anggota memenuhi") checked @endif required=""> Kurang dari 50% anggota memenuhi <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW rutin setiap bulan menjemput iuran anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q2f" data-category="c6" data-point="40" value="Ya"
                                                       @if($data->q2f == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q2f" data-category="c6" data-point="5" value="Tidak"
                                                       @if($data->q2f == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW memiliki catatan lengkap tentang tagihan iuran anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q3f" data-category="c6" data-point="40" value="Ya"
                                                       @if($data->q3f == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q3f" data-category="c6" data-point="5" value="Tidak"
                                                       @if($data->q3f == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW mengusahakan sumber-sumber pemasukan keuangan lain selain iuran anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q4f" data-category="c6" data-point="40" value="Ya"
                                                       @if($data->q4f == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q4f" data-category="c6" data-point="5" value="Tidak"
                                                       @if($data->q4f == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PW memiliki pembukuan keuangan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q5f" data-category="c6" data-point="40" value="Ya"
                                                       @if($data->q5f == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q5f" data-category="c6" data-point="5" value="Tidak"
                                                       @if($data->q5f == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah sistem pembukuan keuangan jam'iyyah sudah sesuai dengan Pedoman Pembukuan Keuangan Jam'iyyah?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q6f" data-category="c6" data-point="40" value="Ya"
                                                       @if($data->q6f == "Ya") checked @endif required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q6f" data-category="c6" data-point="5" value="Tidak"
                                                       @if($data->q6f == "Tidak") checked @endif required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Bagaimana kondisi neraca/saldo tahunan PW?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q7f" data-category="c6" data-point="40" value="Saldo"
                                                @if($data->q7f == "Saldo") checked @endif required=""> Saldo <br>
                                         <input type="radio" class="flat-red" name="q7f" data-category="c6" data-point="10" value="Nol"
                                                @if($data->q7f == "Nol") checked @endif required=""> Nol <br>
                                         <input type="radio" class="flat-red" name="q7f" data-category="c6" data-point="5" value="Defisit"
                                                @if($data->q7f == "Defisit") checked @endif required=""> Defisit <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </ol>
                        </div>
                    </div>
                    <div class="block-content block-content-sm block-content-full bg-body-light rounded-bottom">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-secondary" data-wizard="prev">
                                    <i class="fa fa-angle-left mr-1"></i> Kembali
                                </button>
                            </div>
                            <div class="col-6 text-right">
                                <button type="button" class="btn btn-secondary" data-wizard="next">
                                    Selanjutnya <i class="fa fa-angle-right ml-1"></i>
                                </button>
                                <button type="submit" class="btn btn-primary d-none" data-wizard="finish">
                                    <i class="fa fa-check mr-1"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal PW -->
    <div class="modal fade" id="myModalPW" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PW</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPW" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PW</th>
                            <th>Nama PW</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($dataPW as $data_pw)
                            <tr class="pilih3" data-kd_pw="{{$data_pw->kd_pw}}"
                                data-nama_pw="{{$data_pw->nama_pw}}">
                                <td>{{$data_pw->kd}}</td>
                                <td>{{$data_pw->kd_pw}}</td>
                                <td>{{$data_pw->nama_pw}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal PW -->

@endsection

@section('js_after')
    <!-- Page JS Hijri-Date-Picker Code -->
    <script src="{{ asset('js/plugins/moment/moment-with-locales.js')}}"></script>
    <script src="{{ asset('js/plugins/hijri-datepicker/js/moment-hijri.js')}}"></script>
    <script src="{{ asset('js/plugins/hijri-datepicker/js/bootstrap-hijri-datetimepicker.min.js')}}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <!-- <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script> -->
    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ url('/jamiyyah/datatables/pw')  }}";
        let pimpinan = "PW";
        let firstLoadRegencies = true;
    </script>
    <!--Kode PD-->
    <script type="text/javascript">


        //foto preview
        $('#foto').change(function () {

            let reader = new FileReader();

            reader.onload = (e) => {

                $('#preview-image').attr('src', e.target.result);
                // $('#preview-image').attr('width', "250px");
            }

            reader.readAsDataURL(this.files[0]);

        });


        //  jika dipilih, npa akan masuk ke input dan modal di tutup
        // Kode PW
        $(document).on('click', '.pilih3', function (e) {
            document.getElementById("noPW").value = $(this).attr('data-kd_pw');
            document.getElementById("namaPW").value = $(this).attr('data-nama_pw');
            $('#myModalPW').modal('hide');
        });

        //  tabel lookup npa
        $(function () {
            $("#lookupPW").dataTable();
        });

        $(".performa-pw-wizard").bootstrapWizard({
            'tabClass' : 'nav nav-tabs',
            'nextSelector' : '[data-wizard="next"]',
            'previousSelector' : '[data-wizard="prev"]',
            'firstSelector' : '[data-wizard="first"]',
            'lastSelector' : '[data-wizard="last"]',
            'finishSelector' : '[data-wizard="finish"]',
            'backSelector' : '[data-wizard="back"]',
            'onNext': function(tab, navigation, index) {
                var $valid = $("#performa-pw-form").valid();
                if(!$valid) {
                    return false;
                }
            },
            'onTabClick': function(tab, navigation, index) {
                return false;
            }
        });

        // laravel file manager
        $(document).ready(function () {
            $("#provinces").change();

            let start = moment("{{ $data->start_periode }}").format("MMMM YYYY")
            let end = moment("{{ $data->end_periode }}").format("MMMM YYYY")

            $(".start-periode").val(start);
            $(".end-periode").val(end);

            $(".start-periode").hijriDatePicker({
                locale: "en-US",
                format: "MMMM YYYY",
                allowInputToggle: true,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                debug: true,
                showClear: false,
                showSwitcher: false,
                hijri: false,
                showTodayButton: true,
                showClose: true
            });

            $(".end-periode").hijriDatePicker({
                locale: "en-US",
                format: "MMMM YYYY",
                allowInputToggle: true,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                debug: true,
                showClear: false,
                showSwitcher: false,
                hijri: false,
                showTodayButton: true,
                showClose: true
            });

            $(".start-periode").on('dp.hide', function (e) {
                if(e.date > moment($(".end-periode").val())){
                    window.alert("tanggal mulai tidak boleh lebih besar dari tanggal berakhir")
                    $(this).val(moment($(".end-periode").val()).subtract(1, 'months').format("MMMM YYYY"))
                };
            })

            $(".end-periode").on('dp.change', function (e) {
                if(e.date < moment($(".start-periode").val())){
                    window.alert("tanggal berakhir tidak boleh lebih kecil dari tanggal mulai")
                    $(this).val(moment($(".start-periode").val()).add(1, 'months').format("MMMM YYYY"))
                };
            })
        });
    </script>


    <script type="text/javascript">
        $('#provinces').on('change', function (e) {
            console.log(e);
            var province_id = e.target.value;
            $.get('/regencies?province_id=' + province_id, function (data) {
                $('#regencies').empty();
                $('#regencies').append('<option value="0" disable="true" selected="true">=== Pilih Kabupaten/Kota ===</option>');

                $.each(data, function (index, regenciesObj) {
                    $('#regencies').append('<option value="' + regenciesObj.id + '">' + regenciesObj.nama + '</option>');
                })

                if(firstLoadRegencies){
                    $("#regencies").val("{{ $data->kota }}");
                    $("#regencies").change();
                    firstLoadRegencies = false;
                }
            });
        });

        $("#performa-pw-form").submit(function (e) {
            e.preventDefault();

            let total = 0;
            let c1 = 0;
            let c2 = 0;
            let c3 = 0;
            let c4 = 0;
            let c5 = 0;
            let c6 = 0;

            $('input[type=radio]:checked').each(function(index, el){
                let point = parseInt($(el).data("point"));

                total = total + point;

                let category = $(el).data("category");

                if(category === "c1") {
                    c1 = c1 + point;
                }else if(category === "c2") {
                    c2 = c2 + point;
                }else if(category === "c3") {
                    c3 = c3 + point;
                }else if(category === "c4") {
                    c4 = c4 + point;
                }else if(category === "c5") {
                    c5 = c5 + point;
                }else if(category === "c6") {
                    c6 = c6 + point;
                }
            });

            let payload = new FormData($('#performa-pw-form')[0]);
            payload.append("total_point", total);
            payload.append("point_c1", c1);
            payload.append("point_c2", c2);
            payload.append("point_c3", c3);
            payload.append("point_c4", c4);
            payload.append("point_c5", c5);
            payload.append("point_c6", c6);
            payload.append('_method', 'PUT');

            let start = moment("01" + payload.get("start_periode")).format("YYYY-MM-DD");
            let end = moment("01" + payload.get("end_periode")).format("YYYY-MM-DD");
            payload.set("start_periode", start);
            payload.set("end_periode", end);

            swal({
                title: 'Updating data..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: `{{ route('performa.pw.update', $data->kd, false) }}`,
                processData: false,
                contentType : false,
                data: payload,
                type: 'POST',
                success: function (resp) {
                    if (resp.status == true) {
                        swal.close();
                        swal("Berhasil!", "Data telah berhasil diperbarui.", "success").then((result) => {
                            window.location = `{{ route('performa.pw.index',[], false) }}`;
                        });
                    }else{
                        swal.close();
                        swal("Error!", "Data gagal disimpan. Detail " + resp.message, "error");
                    }
                },
                error: function (resp) {
                    swal.close();
                    alert(resp.responseJSON.message);
                }
            });
        })



    </script>

    <script src="{{ asset('js/pages/pg_jamiyyah_master.js') }}"></script>
@endsection
