@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/performa_pd.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/hijri-datepicker/css/bootstrap-datetimepicker.css') }}">
    <style>
        ul.performa-pc-tab li a {
            height: 100%;
        }
    </style>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Tambah Performa PC <small
                        class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Adding</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="{{route('performa.pc.index')}}">Index Performa PC</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Form <small>Input Performa Jam'iyyah tingkat Pimpinan Cabang</small></h3>
            </div>

            <img class="img-responsive img-rounded center-block hoverZoomLink"
                 src="{{ asset('/media/photos/pemuda%2520persis.jpg') }}" width="150"><br>
            <h1 align="center">
                <bold>PIMPINAN PUSAT</bold>
            </h1>
            <h1 align="center">
                <bold>PEMUDA PERSATUAN ISLAM</bold>
            </h1>
            <hr>
            <h3 align="center">
                <bold>INDEKS PERFORMA JAM’IYYAH (IPJ)</bold>
            </h3>
            <h3 align="center">
                <bold>UNTUK TINGKAT PIMPINAN CABANG (PC)</bold>
            </h3>
            <hr>
            <!--<h4 align='left'><bold>PETUNJUK PENGISIAN</bold></h4>-->
            <h3 align="left">
                <span class="label label-success col-sm-12 control-label">
                    <i class="fa fa-edit"> PETUNJUK PENGISIAN</i>
                </span>
            </h3>
            <div class="row">
                <div class="col-sm-12">&nbsp;</div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <ol>
                        <li>Silakan isi data isian di bawah ini dengan klik checkbox <i
                                class=" fa fa-check-circle-o"></i> pada setiap pilihan sesuai dengan kondisi yang
                            sebenarnya.
                        </li>
                        <li>Prinsip pengisian IPJ ini adalah
                            untuk mengetahui dan memahami kondisi riil dan objektif dari setiap PC,
                            sehingga bisa diketahui potensi kekuatan apa saja yang dimiliki PC juga
                            bisa diidentifikasi masalah dan hambatan yang dihadapi oleh PC sehingga
                            bisa diformulasikan dengan tepat solusi dari setiap permasalahan
                            tersebut.
                        </li>
                    </ol>
                </div>
            </div>

            <div class="performa-pc-wizard block-content block-content-full">
                <ul class="performa-pc-tab nav nav-tabs nav-tabs-block nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#step1" id="step1-tab" data-toggle="tab">Data Awal</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step2" id="step2-tab" data-toggle="tab">Tasykil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step3" id="step3-tab" data-toggle="tab">Aktivitas Jam'iyyah &
                            Partisipasi Anggota</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step4" id="step4-tab" data-toggle="tab">Kaderisasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step5" id="step5-tab" data-toggle="tab">Administrasi Jam'iyyah</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step6" id="step6-tab" data-toggle="tab">Musyawarah</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step7" id="step7-tab" data-toggle="tab">Keuangan Jam'iyyah</a>
                    </li>
                </ul>
                <form id="performa-pc-form">
                    @csrf
                    <div class="block-content block-content-full tab-content px-md-5" style="min-height: 300px;">
                        <div class="tab-pane active" id="step1" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-hf-email">Nomor PW</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="noPW" name="kd_pw"
                                                   placeholder="Nomor PW">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="namaPW" name="namaPW"
                                                   placeholder="Nama PW" readonly required>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-success" data-toggle="modal"
                                                    data-target="#myModalPW"><i class="fa fa-search mr-1"> Cari</i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-hf-email">Nomor PD</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="noPD" name="kd_pd"
                                                   placeholder="Nomor PD">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="namaPD" name="namaPD"
                                                   placeholder="Nama PD" readonly required>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-info" data-toggle="modal"
                                                    data-target="#myModalPD"><i class="fa fa-search mr-1"> Cari</i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="example-hf-email">Nomor PC</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="noPC" name="kd_pc"
                                                   placeholder="Nomor PC">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="namaPC" name="namaPC"
                                                   placeholder="Nama PC" readonly required>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-warning" data-toggle="modal"
                                                    data-target="#myModalPC"><i class="fa fa-search mr-1"> Cari</i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="pw" class="col-sm-2 control-label">Provinsi</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="provinces" id="provinces">
                                                <option value="0" disable="true" selected="true">Pilih Propinsi
                                                </option>
                                                @foreach ($provinces as $key => $value)
                                                    <option value="{{$value->id}}">{{ $value->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pd" class="col-sm-2 control-label">Kabupaten/Kota</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="regencies" id="regencies">
                                                <option value="">Pilih Kabupaten/Kota</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pc" class="col-sm-2 control-label">Kecamatan</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="districts" id="districts">
                                                <option value="0" disable="true" selected="true">Pilih Kecamatan
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_pc" class="col-sm-2 control-label">Alamat Lengkap</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="alamat" name="alamat"
                                                   placeholder="Alamat Lengkap">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_pc" class="col-sm-2 control-label">Nama Ketua PC</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="ketua_pc" name="ketua_pc"
                                                   placeholder="Nama Ketua PC">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_hp" class="col-sm-2 control-label">Nomor HP Ketua PC</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="no_hp" name="no_hp"
                                                   placeholder="Nomer HP Ketua PC">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="start_periode" class="col-sm-2 control-label">Periode Mulai</label>
                                        <div class="col-sm-3">
                                            <div class="date" data-provide="datepicker"
                                                 data-date-format="dd-mm-yyyy">
                                                <input type="text" name="start_periode" required
                                                       class="form-control start-periode"
                                                       placeholder="0000-00-00">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <label for="end_periode" class="col-sm-3 control-label">Periode Berakhir</label>
                                        <div class="col-sm-4">
                                            <div class="date" data-provide="datepicker"
                                                 data-date-format="dd-mm-yyyy">
                                                <input type="text" name="end_periode" required
                                                       class="form-control end-periode"
                                                       placeholder="0000-00-00">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="exampleInputFile" class="col-sm-2 control-label">Foto</label>
                                        <div class="col-sm-10">
                                            <div tabindex="500" class="btn btn-success btn-file"><i
                                                    class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">Pilih Foto Diri</span><input
                                                    type="file" name="foto" id="foto" accept="image/*" class=""></div>
                                        </div>
                                        <div class="col-md-12 mb-2">
                                            <img id="preview-image"
                                                src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
                                                alt="preview image" style="max-height: 250px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- tasykil --}}
                        <div class="tab-pane" id="step2" role="tabpanel">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <ol>
                                            <div class="col-sm-12">
                                                <li>Apakah komposisi tasykil utuh sesuai SK pengesahan?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="20" value="Utuh sesuai SK" required=""> Utuh sesuai SK <br>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="15" value="70'%' sesuai SK" required=""> 70% sesuai SK <br>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="10" value="50'%' sesuai SK" required=""> 50% sesuai SK <br>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="5" value="25'%' sesuai SK" required=""> 25% sesuai SK <br>
                                                        <input type="radio" class="flat-red" name="q1a" data-category="c1" data-point="2" value="25'%' sesuai SK" required=""> Kurang 25% sesuai SK <br>
                                                    </label>
                                                </div>
                                                <br>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Apakah setiap tasykil mempunyai pedoman jam'iyyah?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q2a" data-category="c1" data-point="20" value="Semua mempunyai" required=""> Semua mempunyai <br>
                                                        <input type="radio" class="flat-red" name="q2a" data-category="c1" data-point="10" value="Sebagian besar mempunyai" required=""> Sebagian besar mempunyai <br>
                                                        <input type="radio" class="flat-red" name="q2a" data-category="c1" data-point="5" value="Sebagian besar tidak mempunyai" required=""> Sebagian besar tidak mempunyai <br>
                                                        <input type="radio" class="flat-red" name="q2a" data-category="c1" data-point="2" value="Semua tidak mempunyai" required=""> Semua tidak mempunyai <br>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Apakah setiap tasykil memahami tugas pokoknya masing-masing?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q3a" data-category="c1" data-point="20" value="Semua memahami" required=""> Semua memahami <br>
                                                        <input type="radio" class="flat-red" name="q3a" data-category="c1" data-point="10" value="Sebagian besar memahami" required=""> Sebagian besar memahami <br>
                                                        <input type="radio" class="flat-red" name="q3a" data-category="c1" data-point="5" value="Sebagian besar tidak memahami" required=""> Sebagian besar tidak memahami <br>
                                                        <input type="radio" class="flat-red" name="q3a" data-category="c1" data-point="2" value="Semua tidak memahami" required=""> Semua tidak memahami <br>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Apakah setiap tasykil membuat laporan kinerja secara berkala?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q4a" data-category="c1" data-point="20" value="Membuat secara berkala" required=""> Membuat secara berkala <br>
                                                        <input type="radio" class="flat-red" name="q4a" data-category="c1" data-point="5" value="Pernah membuat" required=""> Pernah membuat <br>
                                                        <input type="radio" class="flat-red" name="q4a" data-category="c1" data-point="2" value="Tidak pernah" required=""> Tidak pernah <br>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Apakah Ketua membuat progress report tahunan?</li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q5a" data-category="c1" data-point="20" value="Membuat secara berkala" required=""> Membuat secara berkala <br>
                                                        <input type="radio" class="flat-red" name="q5a" data-category="c1" data-point="5" value="Pernah membuat" required=""> Pernah membuat <br>
                                                        <input type="radio" class="flat-red" name="q5a" data-category="c1" data-point="2" value="Tidak pernah" required=""> Tidak pernah <br>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <li>Berjalankah jadwal ngantor rutin tasykil (minimal tiga kali
                                                    sebulan)?
                                                </li>
                                                <div class="col-sm-12">
                                                    <label>
                                                        <input type="radio" class="flat-red" name="q6a" data-category="c1" data-point="20" value="Berjalan > tiga kali sebulan" required=""> Berjalan > tiga kali sebulan <br>
                                                        <input type="radio" class="flat-red" name="q6a" data-category="c1" data-point="5" value="Berjalan < tiga kali sebulan" required=""> Berjalan < tiga kali sebulan <br>
                                                        <input type="radio" class="flat-red" name="q6a" data-category="c1" data-point="2" value="Tidak berjalan" required=""> Tidak berjalan <br>
                                                    </label>
                                                </div>
                                            </div>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--  Aktivitas Jam'iyyah & Partisipasi Anggota --}}
                        <div class="tab-pane" id="step3" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <li>Apakah jumlah anggota di atas 25 orang?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q1b" data-category="c2" data-point="10" value="Jumlah anggota > 25 orang" required=""> Jumlah anggota > 25 orang <br>
                                            <input type="radio" class="flat-red" name="q1b" data-category="c2" data-point="2" value="Jumlah anggota kurang dari 25 orang" required=""> Jumlah anggota kurang dari 25 orang <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Bagaimana frekuensi pertemuan tasykil dengan anggota?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="10" value="Sering dengan seluruh anggota" required=""> Sering dengan seluruh anggota <br>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="8" value="Sering dengan sebagian besar anggota" required=""> Sering dengan sebagian besar anggota <br>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="6" value="Sering dengan sebagian kecil anggota" required=""> Sering dengan sebagian kecil anggota <br>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="4" value="Jarang" required=""> Jarang <br>
                                            <input type="radio" class="flat-red" name="q2b" data-category="c2" data-point="2" value="Tidak pernah" required=""> Tidak pernah <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah PC memiliki kegiatan rutin (minimal tiga kali sebulan)?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q3b" data-category="c2" data-point="10" value="Berjalan > tiga kali sebulan" required=""> Berjalan > tiga kali sebulan <br>
                                            <input type="radio" class="flat-red" name="q3b" data-category="c2" data-point="4" value="Berjalan < tiga kali sebulan" required=""> Berjalan < tiga kali sebulan <br>
                                            <input type="radio" class="flat-red" name="q3b" data-category="c2" data-point="2" value="Tidak berjalan" required=""> Tidak berjalan <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah kegiatan rutin itu berjalan dengan lancar?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q4b" data-category="c2" data-point="10" value="Kegiatan rutin > tiga kali sebulan" required=""> Kegiatan rutin > tiga kali sebulan <br>
                                            <input type="radio" class="flat-red" name="q4b" data-category="c2" data-point="4" value="Kegiatan rutin jamaah kurang dari tiga kali sebulan" required=""> Kegiatan rutin jamaah kurang dari tiga kali sebulan <br>
                                            <input type="radio" class="flat-red" name="q4b" data-category="c2" data-point="2" value="Tidak ada" required=""> Tidak ada <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Bagaimana rata-rata kehadiran anggota dalam kegiatan rutin PC? (sertakan bukti
                                        fisik)
                                    </li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q5b" data-category="c2" data-point="10" value="100% anggota hadir" required=""> 100% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q5b" data-category="c2" data-point="6" value="75% anggota hadir" required=""> 75% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q5b" data-category="c2" data-point="4" value="50% anggota hadir" required=""> 50% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q5b" data-category="c2" data-point="2" value="Kurang dari 50% anggota hadir" required=""> Kurang dari 50% anggota hadir <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah ada kegiatan insidental di PC?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q6b" data-category="c2" data-point="10" value="Ada" required=""> Ada <br>
                                            <input type="radio" class="flat-red" name="q6b" data-category="c2" data-point="2" value="Tidak" required=""> Tidak <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Bagaimana rata-rata kehadiran anggota dalam kegiatan insidental PC? (sertakan
                                        bukti fisik)
                                    </li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q7b" data-category="c2" data-point="10" value="100% anggota hadir" required=""> 100% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q7b" data-category="c2" data-point="6" value="75% anggota hadir" required=""> 75% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q7b" data-category="c2" data-point="4" value="50% anggota hadir" required=""> 50% anggota hadir <br>
                                            <input type="radio" class="flat-red" name="q7b" data-category="c2" data-point="2" value="Kurang dari 50% anggota hadir" required=""> Kurang dari 50% anggota hadir<br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah ada kegiatan jamiyyah bersama Persis dan otonom lainnya?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q8b" data-category="c2" data-point="10" value="Ada dan selalu mengikuti" required=""> Ada dan selalu mengikuti <br>
                                            <input type="radio" class="flat-red" name="q8b" data-category="c2" data-point="6" value="Ada dan sering mengikuti" required=""> Ada dan sering mengikuti <br>
                                            <input type="radio" class="flat-red" name="q8b" data-category="c2" data-point="4" value="Ada dan jarang mengikuti" required=""> Ada dan jarang mengikuti <br>
                                            <input type="radio" class="flat-red" name="q8b" data-category="c2" data-point="2" value="Tidak ada" required=""> Tidak ada <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Apakah ada kegiatan yang melibatkan organisasi lain?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q9b" data-category="c2" data-point="10" value="Ada dan selalu mengikuti" required=""> Ada dan selalu mengikuti <br>
                                            <input type="radio" class="flat-red" name="q9b" data-category="c2" data-point="6" value="Ada dan sering mengikuti" required=""> Ada dan sering mengikuti <br>
                                            <input type="radio" class="flat-red" name="q9b" data-category="c2" data-point="4" value="Ada dan jarang mengikuti" required=""> Ada dan jarang mengikuti <br>
                                            <input type="radio" class="flat-red" name="q9b" data-category="c2" data-point="2" value="Tidak ada" required=""> Tidak ada <br>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <li>Bagaimana publikasi/undangan pada setiap kegiatan PC?</li>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="10" value="Setiap kegiatan ada publikasi/undangan tertulis" required=""> Setiap kegiatan ada publikasi/undangan tertulis <br>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="8" value="Setiap kegiatan ada publikasi/undangan lisan" required=""> Setiap kegiatan ada publikasi/undangan lisan <br>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="6" value="Sebagian besar kegiatan ada publikasi/undangan tertulis" required=""> Sebagian besar kegiatan ada publikasi/undangan tertulis <br>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="4" value="Sebagian besar kegiatan ada publikasi/undangan lisan" required=""> Sebagian besar kegiatan ada publikasi/undangan lisan <br>
                                            <input type="radio" class="flat-red" name="q10b" data-category="c2" data-point="2" value="Tidak ada"> Tidak ada <br>
                                        </label>
                                    </div>
                                </div>
                            </ol>
                        </div>

                        {{--  Kaderisasi --}}
                        <div class="tab-pane" id="step4" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Jumlah calon anggota yang tercatat ?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q1c" data-category="c3" data-point="40" value="Lebih dari 10 orang" required=""> Lebih dari 10 orang <br>
                                                <input type="radio" class="flat-red" name="q1c" data-category="c3" data-point="10" value="Kurang dari 10 orang" required=""> Kurang dari 10 orang <br>
                                                <input type="radio" class="flat-red" name="q1c" data-category="c3" data-point="5" value="Tidak ada" required=""> Tidak ada <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Adakah program pembinaan bagi para calon anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q2c" data-category="c3" data-point="40" value="Ada" required=""> Ada <br>
                                                <input type="radio" class="flat-red" name="q2c" data-category="c3" data-point="5" value="Tidak ada" required=""> Tidak ada <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Jumlah calon anggota yang mengikuti program pembinaan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q3c" data-category="c3" data-point="40" value="Semua calon anggota yang tercatat mengikuti program pembinaan" required=""> Semua calon anggota yang tercatat mengikuti program pembinaan <br>
                                                <input type="radio" class="flat-red" name="q3c" data-category="c3" data-point="20" value="Sebagian besar calon  anggota yang tercatat mengikuti program pembinaan" required=""> Sebagian besar calon  anggota yang tercatat mengikuti program pembinaan <br>
                                                <input type="radio" class="flat-red" name="q3c" data-category="c3" data-point="10" value="Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan" required=""> Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan <br>
                                                <input type="radio" class="flat-red" name="q3c" data-category="c3" data-point="5" value="Tidak ada calon anggota yang mengikuti program pembinaan" required=""> Tidak ada calon anggota yang mengikuti program pembinaan <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </ol>
                        </div>

                        {{-- step5 Administrasi jamiyyah --}}
                        <div class="tab-pane" id="step5" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki sekretariat yang definitif?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q1d" data-category="c4" data-point="10" value="Sudah" required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q1d" data-category="c4" data-point="2" value="Belum" required=""> Belum <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah sekretariat sudah berfungsi sebagai sentra kegiatan jam'iyyah (rapat,
                                            ngantor,dll)?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q2d" data-category="c4" data-point="10" value="Sudah" required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q2d" data-category="c4" data-point="6" value="Sebagian besar sudah" required=""> Sebagian besar sudah <br>
                                                <input type="radio" class="flat-red" name="q2d" data-category="c4" data-point="4" value="Sebagian besar belum" required=""> Sebagian besar belum <br>
                                                <input type="radio" class="flat-red" name="q2d" data-category="c4" data-point="2" value="Belum" required=""> Belum <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC memiliki perangkat kesekretariatan (komputer/mesin tik)?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q3d" data-category="c4" data-point="10" value="Sudah" required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q3d" data-category="c4" data-point="2" value="Belum" required=""> Belum <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki Buku Agenda Surat Masuk?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q4d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q4d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Agenda Surat Masuk tersebut sudah digunakan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="10" value="Selalu digunakan" required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="8" value="Sering digunakan" required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="6" value="Jarang digunakan" required=""> Jarang digunakan <br>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="4" value="Belum digunakan" required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q5d" data-category="c4" data-point="2" value="Tidak ada Buku Agenda Surat Masuk" required=""> Tidak ada Buku Agenda Surat Masuk <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah surat-surat masuk minimal satu tahun terakhir sudah diarsipkan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q6d" data-category="c4" data-point="10" value="Seluruh surat masuk diarsipkan" required=""> Seluruh surat masuk diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q6d" data-category="c4" data-point="6" value="Sebagian besar surat masuk diarsipkan" required=""> Sebagian besar surat masuk diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q6d" data-category="c4" data-point="4" value="Sebagian besar surat masuk tidak diarsipkan" required=""> Sebagian besar surat masuk tidak diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q6d" data-category="c4" data-point="2" value="Surat-surat masuk tidak diarsipkan" required=""> Surat-surat masuk tidak diarsipkan <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki Buku Agenda Surat Keluar?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q7d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q7d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Agenda Surat Keluar tersebut sudah digunakan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="10" value="Selalu digunakan" required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="8" value="Sering digunakan" required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="6" value="Jarang digunakan" required=""> Jarang digunakan<br>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="4" value="Belum digunakan" required=""> Belum digunakan<br>
                                                <input type="radio" class="flat-red" name="q8d" data-category="c4" data-point="2" value="Tidak ada Buku Agenda Surat Keluar" required=""> Tidak ada Buku Agenda Surat Keluar<br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah surat-surat keluar minimal satu tahun terakhir sudah diarsipkan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q9d" data-category="c4" data-point="10" value="Seluruh surat keluar diarsipkan" required=""> Seluruh surat keluar diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q9d" data-category="c4" data-point="6" value="Sebagian besar surat keluar diarsipkan" required=""> Sebagian besar surat keluar diarsipkan <br>
                                                <input type="radio" class="flat-red" name="q9d" data-category="c4" data-point="4" value="Sebagian besar surat keluar tidak diarsipkan" required=""> Sebagian besar surat keluar tidak diarsipkan<br>
                                                <input type="radio" class="flat-red" name="q9d" data-category="c4" data-point="2" value="Surat-surat keluar tidak diarsipkan" required=""> Surat-surat keluar tidak diarsipkan<br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki Buku Induk Anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q10d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q10d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Induk Anggota sudah digunakan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q11d" data-category="c4" data-point="10" value="Sudah digunakan" required=""> Sudah digunakan <br>
                                                <input type="radio" class="flat-red" name="q11d" data-category="c4" data-point="4" value="Belum digunakan" required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q11d" data-category="c4" data-point="2" value="Tidak ada Buku Induk Anggota" required=""> Tidak ada Buku Induk Anggota <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah semua data dalam Buku Induk Anggota sudah terisi?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="10" value="Sudah terisi" required=""> Sudah terisi <br>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="8" value="Sebagian besar data sudah diisi" required=""> Sebagian besar data sudah diisi <br>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="6" value="Sebagian besar data belum diisi" required=""> Sebagian besar data belum diisi <br>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="4" value="Belum terisi" required=""> Belum terisi <br>
                                                <input type="radio" class="flat-red" name="q12d" data-category="c4" data-point="2" value="Tidak ada Buku Induk Anggota" required=""> Tidak ada Buku Induk Anggota <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah data yang terdapat dalam Buku Induk Anggota sesuai dengan keadaan
                                            sesungguhnya?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="10" value="Sesuai" required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="8" value="Sebagian besar sesuai" required=""> Sebagian besar sesuai <br>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="6" value="Sebagian besar tidak sesuai" required=""> Sebagian besar tidak sesuai <br>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="4" value="Tidak sesuai" required=""> Tidak sesuai <br>
                                                <input type="radio" class="flat-red" name="q13d" data-category="c4" data-point="2" value="Tidak ada Buku Induk Anggota" required=""> Tidak ada Buku Induk Anggota <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki kop surat?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q14d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q14d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah kop surat tersebut sudah sesuai dengan Pedoman Administrasi
                                            Jam’iyyah?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q15d" data-category="c4" data-point="10" value="Sesuai" required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q15d" data-category="c4" data-point="2" value="Tidak sesuai" required=""> Tidak sesuai <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki stempel?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q16d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q16d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah stempel tersebut sesuai dengan Pedoman Administrasi Jam’iyyah?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q17d" data-category="c4" data-point="10" value="Sesuai" required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q17d" data-category="c4" data-point="2" value="Tidak sesuai" required=""> Tidak sesuai <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah surat resmi PC sudah sesuai dengan Pedoman Administrasi (bentuk,
                                            format, penomoran, dll)?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q18d" data-category="c4" data-point="10" value="Sesuai" required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q18d" data-category="c4" data-point="2" value="Tidak sesuai" required=""> Tidak sesuai <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki Buku Agenda Kegiatan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q19d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q19d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Agenda Kegiatan sudah digunakan sebagaimana mestinya?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="10" value="Selalu digunakan" required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="8" value="Sering digunakan" required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="6" value="Jarang digunakan" required=""> Jarang digunakan <br>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="4" value="Belum digunakan" required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q20d" data-category="c4" data-point="2" value="Tidak ada Buku Agenda Kegiatan" required=""> Tidak ada Buku Agenda Kegiatan <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah jamaah sudah memiliki Buku Daftar Hadir Kegiatan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q21d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q21d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Daftar Hadir Kegiatan dipakai dalam setiap kegiatan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="10" value="Selalu digunakan" required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="8" value="Sering digunakan" required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="6" value="Jarang digunakan" required=""> Jarang digunakan <br>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="4" value="Belum digunakan" required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q22d" data-category="c4" data-point="2" value="Tidak ada Buku Daftar Hadir Kegiatan" required=""> Tidak ada Buku Daftar Hadir Kegiatan <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki Buku Notulen Rapat/Musyawarah?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q23d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q23d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah Buku Notulen tersebut dipakai dalam setiap rapat/musyawarah?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="10" value="Selalu digunakan" required=""> Selalu digunakan <br>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="8" value="Sering digunakan" required=""> Sering digunakan <br>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="6" value="Jarang digunakan" required=""> Jarang digunakan <br>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="4" value="Belum digunakan" required=""> Belum digunakan <br>
                                                <input type="radio" class="flat-red" name="q24d" data-category="c4" data-point="2" value="Tidak ada Buku Notulen" required=""> Tidak ada Buku Notulen <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC sudah memiliki Buku Daftar Tamu?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q25d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q25d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah jamaah sudah memiliki Buku Inventaris?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q26d" data-category="c4" data-point="10" value="Memiliki" required=""> Memiliki <br>
                                                <input type="radio" class="flat-red" name="q26d" data-category="c4" data-point="2" value="Belum memiliki" required=""> Belum memiliki <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah semua data dalam buku inventaris tersebut lengkap?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="10" value="Sudah" required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="8" value="Sebagian besar data sudah lengkap diisi" required=""> Sebagian besar data sudah lengkap diisi <br>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="6" value="Sebagian besar data belum lengkap diisi" required=""> Sebagian besar data belum lengkap diisi <br>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="4" value="Belum" required=""> Belum <br>
                                                <input type="radio" class="flat-red" name="q27d" data-category="c4" data-point="2" value="Tidak ada Buku Inventaris" required=""> Tidak ada Buku Inventaris <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah data dalam buku inventaris sudah sesuai dengan kenyataannya?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="10" value="Sudah" required=""> Sudah <br>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="8" value="Sebagian besar data sudah sesuai" required=""> Sebagian besar data sudah sesuai <br>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="6" value="Sebagian besar data belum sesuai" required=""> Sebagian besar data belum sesuai <br>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="4" value="Belum" required=""> Belum <br>
                                                <input type="radio" class="flat-red" name="q28d" data-category="c4" data-point="2" value="Tidak ada Buku Inventaris" required=""> Tidak ada Buku Inventaris <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </ol>
                        </div>

                        {{-- step6 Musyawarah --}}
                        <div class="tab-pane" id="step6" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah masa jihad PC sesuai masa jihad yang tertuang dalam SK pengesahan?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q1e" data-category="c5" data-point="10" value="Sesuai" required=""> Sesuai <br>
                                                <input type="radio" class="flat-red" name="q1e" data-category="c5" data-point="2" value="Tidak sesuai" required=""> Tidak sesuai <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah SK pengesahan PC/tasykil masih ada?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q2e" data-category="c5" data-point="10" value="Ada" required=""> Ada <br>
                                                <input type="radio" class="flat-red" name="q2e" data-category="c5" data-point="2" value="Tidak ada" required=""> Tidak ada <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Berapa kali PC melakukan musyawarah kerja selama masa jihad?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q3e" data-category="c5" data-point="10" value="Satu tahun satu kali" required=""> Satu tahun satu kali <br>
                                                <input type="radio" class="flat-red" name="q3e" data-category="c5" data-point="4" value="Satu kali" required=""> Satu kali <br>
                                                <input type="radio" class="flat-red" name="q3e" data-category="c5" data-point="2" value="Belum pernah" required=""> Belum pernah <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PJ-PJ?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q4e" data-category="c5" data-point="10" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q4e" data-category="c5" data-point="2" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PD Pemuda?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q5e" data-category="c5" data-point="10" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q5e" data-category="c5" data-point="2" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PC Persis?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q6e" data-category="c5" data-point="10" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q6e" data-category="c5" data-point="2" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PC
                                            Persistri?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q7e" data-category="c5" data-point="10" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q7e" data-category="c5" data-point="2" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PC Pemudi?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q8e" data-category="c5" data-point="10" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q8e" data-category="c5" data-point="2" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Berapa kali PC melakukan rapat koordinasi dengan PJ dalam satu tahun?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q9e" data-category="c5" data-point="10" value="> 6 kali setahun" required=""> > 6 kali setahun <br>
                                                <input type="radio" class="flat-red" name="q9e" data-category="c5" data-point="6" value="Antara 3 – 5 kali setahun" required=""> Antara 3 – 5 kali setahun <br>
                                                <input type="radio" class="flat-red" name="q9e" data-category="c5" data-point="4" value="Kurang dari 3 kali setahun" required=""> Kurang dari 3 kali setahun <br>
                                                <input type="radio" class="flat-red" name="q9e" data-category="c5" data-point="2" value="Belum pernah" required=""> Belum pernah <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Bagaimana rata-rata kehadiran PJ dalam rapat koordinasi tersebut?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q10e" data-category="c5" data-point="10" value="Semua PJ hadir" required=""> Semua PJ hadir <br>
                                                <input type="radio" class="flat-red" name="q10e" data-category="c5" data-point="6" value="75% PJ hadir" required=""> 75% PJ hadir <br>
                                                <input type="radio" class="flat-red" name="q10e" data-category="c5" data-point="4" value="50% PJ hadir" required=""> 50% PJ hadir <br>
                                                <input type="radio" class="flat-red" name="q10e" data-category="c5" data-point="2" value="Kurang dari 50% PJ hadir" required=""> Kurang dari 50% PJ hadir <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </ol>
                        </div>

                        {{-- step7 Keuangan jamiyyah --}}
                        <div class="tab-pane" id="step7" role="tabpanel">
                            <ol>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Bagaimana partisipasi anggota dalam iuran wajib?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q1f" data-category="c6" data-point="40" value="100% anggota memenuhi" required=""> 100% anggota memenuhi <br>
                                                <input type="radio" class="flat-red" name="q1f" data-category="c6" data-point="20" value="75% anggota memenuhi" required=""> 75% anggota memenuhi <br>
                                                <input type="radio" class="flat-red" name="q1f" data-category="c6" data-point="10" value="50% anggota memenuhi" required=""> 50% anggota memenuhi <br>
                                                <input type="radio" class="flat-red" name="q1f" data-category="c6" data-point="5" value="Kurang dari 50% anggota memenuhi" required=""> Kurang dari 50% anggota memenuhi <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC rutin setiap bulan menjemput iuran anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q2f" data-category="c6" data-point="40" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q2f" data-category="c6" data-point="5" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC memiliki catatan lengkap tentang tagihan iuran anggota?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q3f" data-category="c6" data-point="40" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q3f" data-category="c6" data-point="5" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC mengusahakan sumber-sumber pemasukan keuangan lain selain iuran
                                            anggota?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q4f" data-category="c6" data-point="40" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q4f" data-category="c6" data-point="5" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah PC memiliki pembukuan keuangan?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q5f" data-category="c6" data-point="40" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q5f" data-category="c6" data-point="5" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Apakah sistem pembukuan keuangan jam'iyyah sudah sesuai dengan Pedoman
                                            Pembukuan Keuangan Jam'iyyah?
                                        </li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q6f" data-category="c6" data-point="40" value="Ya" required=""> Ya <br>
                                                <input type="radio" class="flat-red" name="q6f" data-category="c6" data-point="5" value="Tidak" required=""> Tidak <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <li>Bagaimana kondisi neraca/saldo tahunan PC?</li>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="radio" class="flat-red" name="q7f" data-category="c6" data-point="40" value="Saldo" required=""> Saldo <br>
                                                <input type="radio" class="flat-red" name="q7f" data-category="c6" data-point="10" value="Nol" required=""> Nol <br>
                                                <input type="radio" class="flat-red" name="q7f" data-category="c6" data-point="5" value="Defisit" required=""> Defisit <br>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </ol>
                        </div>
                    </div>
                    <div class="block-content block-content-sm block-content-full bg-body-light rounded-bottom">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-secondary" data-wizard="prev">
                                    <i class="fa fa-angle-left mr-1"></i> Kembali
                                </button>
                            </div>
                            <div class="col-6 text-right">
                                <button type="button" class="btn btn-secondary" data-wizard="next">
                                    Selanjutnya <i class="fa fa-angle-right ml-1"></i>
                                </button>
                                <button type="submit" class="btn btn-primary d-none" data-wizard="finish">
                                    <i class="fa fa-check mr-1"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Modal PC -->
    <div class="modal fade" id="myModalPC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PC</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPC" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PC</th>
                            <th>Nama PC</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal PC -->

    <!-- Modal PD -->
    <div class="modal fade" id="myModalPD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PD</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPD" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PD</th>
                            <th>Nama PD</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal PD -->

    <!-- Modal PW -->
    <div class="modal fade" id="myModalPW" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:1200px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kode PW</h4>
                </div>
                <div class="modal-body">
                    <table id="lookupPW" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Kode PW</th>
                            <th>Nama PW</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no =1)
                        @foreach ($dataPW as $data_pw)
                            <tr class="pilih3" data-kd_pw="{{$data_pw->kd_pw}}"
                                data-nama_pw="{{$data_pw->nama_pw}}">
                                <td>{{$no++}}</td>
                                <td>{{$data_pw->kd_pw}}</td>
                                <td>{{$data_pw->nama_pw}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal PW -->

@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <!-- <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script> -->
    <!-- Page JS Hijri-Date-Picker Code -->
    <script src="{{ asset('js/plugins/moment/moment-with-locales.js')}}"></script>
    <script src="{{ asset('js/plugins/hijri-datepicker/js/moment-hijri.js')}}"></script>
    <script src="{{ asset('js/plugins/hijri-datepicker/js/bootstrap-hijri-datetimepicker.min.js')}}"></script>


    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ url('/jamiyyah/datatables/pw')  }}";
        let pimpinan = "PW";
    </script>
    <!--Kode PC-->
    <script type="text/javascript">

        //foto preview
        $('#foto').change(function () {

            let reader = new FileReader();

            reader.onload = (e) => {

                $('#preview-image').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);

        });


        //  jika dipilih, npa akan masuk ke input dan modal di tutup
        // Kode PC
        $(document).on('click', '.pilih1', function (e) {
            document.getElementById("noPC").value = $(this).attr('data-kd_pc');
            document.getElementById("namaPC").value = $(this).attr('data-nama_pc');
            $('#myModalPC').modal('hide');
        });
        // Kode PD
        $(document).on('click', '.pilih2', function (e) {
            let kdPD = $(this).attr('data-kd_pd');

            document.getElementById("noPD").value = $(this).attr('data-kd_pd');
            document.getElementById("namaPD").value = $(this).attr('data-nama_pd');

            document.getElementById("noPC").value = "";
            document.getElementById("namaPC").value = "";

            $('#myModalPD').modal('hide');

            $.get(`/get-related-pc/${kdPD}`, function (response) {
                $("#lookupPC").dataTable().api().destroy();
                $("#lookupPC tbody").empty();
                let data = "";
                $.each(response, function (index, item) {
                    data = data + `<tr class="pilih1" data-kd_pc="${item.kd_pc}"
                                data-nama_pc="${item.nama_pc}">
                                    <td>${index + 1}</td>
                                    <td>${item.kd_pc}</td>
                                    <td>${item.nama_pc}</td>
                                </tr>`
                });
                $("#lookupPC tbody").html(data);
                $("#lookupPC").dataTable();
            });
        });
        // Kode PW
        $(document).on('click', '.pilih3', function (e) {
            let kdPW = $(this).attr('data-kd_pw');
            document.getElementById("noPW").value = kdPW;
            document.getElementById("namaPW").value = $(this).attr('data-nama_pw');

            document.getElementById("noPD").value = "";
            document.getElementById("namaPD").value = "";

            document.getElementById("noPC").value = "";
            document.getElementById("namaPC").value = "";

            $('#myModalPW').modal('hide');

            $.get(`/get-related-pd/${kdPW}`, function (response) {
                $("#lookupPD").dataTable().api().destroy();
                $("#lookupPD tbody").empty();
                let data = "";
                $.each(response, function (index, item) {
                    data = data + `<tr class="pilih2" data-kd_pd="${item.kd_pd}"
                                data-nama_pd="${item.nama_pd}">
                                    <td>${index + 1}</td>
                                    <td>${item.kd_pd}</td>
                                    <td>${item.nama_pd}</td>
                                </tr>`
                });

                $("#lookupPD tbody").html(data);
                $("#lookupPD").dataTable();
            });
        })
        //  tabel lookup npa
        $(function () {
            $("#lookupPW").dataTable();
        });

        $(".performa-pc-wizard").bootstrapWizard({
            'tabClass': 'nav nav-tabs',
            'nextSelector': '[data-wizard="next"]',
            'previousSelector': '[data-wizard="prev"]',
            'firstSelector': '[data-wizard="first"]',
            'lastSelector': '[data-wizard="last"]',
            'finishSelector': '[data-wizard="finish"]',
            'backSelector': '[data-wizard="back"]',
            'onNext': function (tab, navigation, index) {
                var $valid = $("#performa-pc-form").valid();
                if (!$valid) {
                    return false;
                }
            },
            'onTabClick': function (tab, navigation, index) {
                return false;
            }
        });

        // laravel file manager
        // $(document).ready(function () {
        //     $('#lfm').filemanager('foto');
        // });

        //period
        $(document).ready(function () {

            $(".start-periode").hijriDatePicker({
                locale: "en-US",
                format: "MMMM YYYY",
                allowInputToggle: true,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                debug: true,
                showClear: false,
                showSwitcher: false,
                hijri: false,
                showTodayButton: true,
                showClose: true
            });

            $(".end-periode").hijriDatePicker({
                locale: "en-US",
                format: "MMMM YYYY",
                allowInputToggle: true,
                useCurrent: true,
                isRTL: false,
                viewMode: 'months',
                keepOpen: false,
                debug: true,
                showClear: false,
                showSwitcher: false,
                hijri: false,
                showTodayButton: true,
                showClose: true
            });

            $(".start-periode").on('dp.hide', function (e) {
                if(e.date > moment($(".end-periode").val())){
                    swal("tanggal mulai tidak boleh lebih besar dari tanggal berakhir")
                    $(this).val(moment($(".end-periode").val()).subtract(1, 'months').format("MMMM YYYY"))
                };
            })

            $(".end-periode").on('dp.hide', function (e) {
                if(e.date < moment($(".start-periode").val())){
                    swal("tanggal berakhir tidak boleh lebih kecil dari tanggal mulai")
                    $(this).val(moment($(".start-periode").val()).add(1, 'months').format("MMMM YYYY"))
                };
            })
        });

    </script>


    <script type="text/javascript">
        $('#provinces').on('change', function (e) {
            console.log(e);
            var province_id = e.target.value;
            $.get('/regencies?province_id=' + province_id, function (data) {
                console.log(data);
                $('#regencies').empty();
                $('#regencies').append('<option value="0" disable="true" selected="true">=== Pilih Kabupaten/Kota ===</option>');

                $('#districts').empty();
                $('#districts').append('<option value="0" disable="true" selected="true">=== Pilih Kecamatan ===</option>');

                $('#villages').empty();
                $('#villages').append('<option value="0" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, regenciesObj) {
                    $('#regencies').append('<option value="' + regenciesObj.id + '">' + regenciesObj.nama + '</option>');
                })
            });
        });

        $('#regencies').on('change', function (e) {
            console.log(e);
            var regencies_id = e.target.value;
            $.get('/districts?regencies_id=' + regencies_id, function (data) {
                console.log(data);
                $('#districts').empty();
                $('#districts').append('<option value="0" disable="true" selected="true">=== Pilih Kecamatan ===</option>');

                $.each(data, function (index, districtsObj) {
                    $('#districts').append('<option value="' + districtsObj.id + '">' + districtsObj.nama + '</option>');
                })
            });
        });

        $('#districts').on('change', function (e) {
            console.log(e);
            var districts_id = e.target.value;
            $.get('/village?districts_id=' + districts_id, function (data) {
                console.log(data);
                $('#villages').empty();
                $('#villages').append('<option value="0" disable="true" selected="true">=== Pilih Desa/Kelurahan ===</option>');

                $.each(data, function (index, villagesObj) {
                    $('#villages').append('<option value="' + villagesObj.id + '">' + villagesObj.nama + '</option>');
                })
            });
        });

        $("#performa-pc-form").submit(function (e) {
            e.preventDefault();

            let total = 0;
            let c1 = 0;
            let c2 = 0;
            let c3 = 0;
            let c4 = 0;
            let c5 = 0;
            let c6 = 0;

            $('input[type=radio]:checked').each(function(index, el){
                let point = parseInt($(el).data("point"));

                total = total + point;

                let category = $(el).data("category");

                if(category === "c1") {
                    c1 = c1 + point;
                }else if(category === "c2") {
                    c2 = c2 + point;
                }else if(category === "c3") {
                    c3 = c3 + point;
                }else if(category === "c4") {
                    c4 = c4 + point;
                }else if(category === "c5") {
                    c5 = c5 + point;
                }else if(category === "c6") {
                    c6 = c6 + point;
                }
            });

            let payload = new FormData($('#performa-pc-form')[0]);
            payload.append("total_point", total);
            payload.append("point_c1", c1);
            payload.append("point_c2", c2);
            payload.append("point_c3", c3);
            payload.append("point_c4", c4);
            payload.append("point_c5", c5);
            payload.append("point_c6", c6);

            //period
            let start = moment("01" + payload.get("start_periode")).format("YYYY-MM-DD");
            let end = moment("01" + payload.get("end_periode")).format("YYYY-MM-DD");
            payload.set("start_periode", start);
            payload.set("end_periode", end);


            swal({
                title: 'Updating data..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                url: `{{ route('performa.pc.store', [], false) }}`,
                processData: false,
                contentType: false,
                data: payload,
                type: 'POST',
                success: function (resp) {
                    if (resp.status == true) {
                        swal.close();
                        swal("Berhasil!", "Data telah berhasil diperbarui.", "success").then((result) => {
                            window.location = `{{ route('performa.pc.index',[], false) }}`;
                        });
                    } else {
                        swal.close();
                        swal("Error!", "Data gagal disimpan. Detail " + resp.message, "error");
                    }
                },
                error: function (resp) {
                    swal.close();
                    alert(resp.responseJSON.message);
                }
            });
        })


    </script>

    <script src="{{ asset('js/pages/pg_jamiyyah_master.js') }}"></script>
@endsection
