@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">

@endsection

@section('content')
        <!-- Hero -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">
                        Data Performa PJ <small
                            class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                    </h1>
                    <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-alt">
                            <li class="breadcrumb-item">Data Performa PJ</li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a class="link-fx" href="/dashboard">Dashboard</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- Page Content -->
        <div class="content">
            <!-- Dynamic Table with Export Buttons -->
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Table <small>Data Performa PJ</small></h3>
                    @if(Permission::checkPermission('indexperformapj.create'))
                        <a href="{{route('performa.pj.create')}}" class="btn btn-success btn-sm"><span
                                class="si si-plus"></span> Tambah</a>
                    @endif
                </div>
                <div class="block-content block-content-full">
                    <div class="table-responsive">
                        <table
                            class="table table-bordered table-striped table-vcenter js-dataTable-performa-pj dataTable no-footer"
                            id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                            <thead>
                            <tr role="row">
                                <th>Foto</th>
                                <th>PJ</th>
                                <th>Kecamatan</th>
                                <th>Desa</th>
                                <th>Alamat</th>
                                <th>Ketua PJ</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($performaPJ as $item)
                                <tr>
                                    <td class="font-w600 font-size-sm">
                                        @if(!$item->foto || $item->foto=="default.png")
                                            <img width="70px" src="{{ asset('images/anggota/default.png') }}">
                                        @elseif($item->pw && $item->pd && $item->pc && $item->nama_pj)
                                            <img width="70px"
                                                src="{{ Storage::disk('s3')->url('images/performa/pj/'.($item->pw?$item->pw->nama_pw:'pw').'/'.($item->pd?$item->pd->nama_pd:'pd').'/'.($item->pc?$item->pc->nama_pc:'pc').'/'.($item->nama_pj?$item->nama_pj:'pj').'/'.$item->foto) }}">
                                        @else
                                            <img width="70px" src="{{ asset('images/anggota/default.png') }}">
                                        @endif
                                    </td>
                                    <td class="font-w600 font-size-sm">{{ $item->nama_pj }}</td>
                                    <td class="font-w600 font-size-sm">{{ $item->getRelationValue('kecamatan')->nama }}</td>
                                    <td class="font-w600 font-size-sm">{{ $item->getRelationValue('desa')->nama }}</td>
                                    <td class="font-w600 font-size-sm">{{ $item->alamat }}</td>
                                    <td class="font-w600 font-size-sm">{{ $item->ketua_pj }}</td>
                                    <td>
                                        <div class="btn-group float-right"></div>

                                        @if(Permission::checkPermission('indexperformapj.read'))
                                            <a href="{{ route('performa.pj.show', $item->kd, false) }}"
                                            class="btn btn-primary btn-sm"><i class="si si-list"></i></a>
                                        @endif

                                        @if(Permission::checkPermission('indexperformapj.update'))
                                            <a href="{{ route('performa.pj.edit', $item->kd, false) }}"
                                            class="btn btn-warning btn-sm"><i class="si si-pencil"></i></a>
                                        @endif

                                        @if(Permission::checkPermission('indexperformapj.delete'))
                                            <button type="button" class="btn btn-danger btn-sm"
                                                    onclick="remove('{{ route('performa.pj.destroy', $item->kd, false) }}')">
                                                <i class="si si-trash"></i></button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->


@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>


    <!-- Page JS Code -->
    <script>
        let dataTableUrl = "{{ url('/jamiyyah/datatables/pc')  }}";
        let pimpinan = "PC";

        jQuery('.js-dataTable-performa-pj').dataTable({
            pageLength: 5,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            order: [[0, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1, 4] }],
            autoWidth: true,
            buttons: [{
                extend: 'copy',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'csv',
                className: 'btn btn-sm btn-primary'
            }, {
                extend: 'print',
                className: 'btn btn-sm btn-primary'
            }],
            dom: "<'row'<'col-sm-12'<'text-center bg-body-light py-2 mb-2'B>>>" + "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>"
        });


        function remove(route) {
            swal({
                title: "Apa anda yakin?",
                text: "Data akan dihapus dan tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus!",
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {

                    swal({
                        title: 'Deleting data..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    });

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        url: route,
                        type: 'DELETE',
                        dataType: 'json',
                        success: function (resp) {
                            if (resp == true) {
                                swal.close();
                                swal("Berhasil!", "Data telah berhasil dihapus.", "success").then((result) => {
                                    window.location.reload();
                                });
                            }
                        },
                        error: function (resp) {
                            swal.close();
                            alert(resp.responseJSON.message);
                        }
                    });
                }
            })
        }
    </script>
@endsection
