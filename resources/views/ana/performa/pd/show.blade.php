@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/performa_pd.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/pg_anggota_profile.js') }}"></script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 font-w300 my-2">
                    Performa PD
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Performa PD</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <img class="img-responsive img-rounded center-block hoverZoomLink pt-4"
                 src="{{ asset('/media/photos/pemuda%2520persis.jpg') }}" width="150"><br>
            <h1 align="center">
                <bold>PIMPINAN PUSAT</bold>
            </h1>
            <h1 align="center">
                <bold>PEMUDA PERSATUAN ISLAM</bold>
            </h1>
            <hr>
            <h3 align="center">
                <bold>INDEKS PERFORMA JAM’IYYAH (IPJ)</bold>
            </h3>
            <h3 align="center">
                <bold>UNTUK TINGKAT PIMPINAN DAERAH (PD)</bold>
            </h3>

            <div class="block-content block-content-full p-4">
            <div class="text-center">
                @if(!$data->foto || $data->foto=="default.png")
                <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ asset('images/anggota/default.png') }}" >
                @elseif($data->pw && $data->pd)
                <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ Storage::disk('s3')->url('images/performa/'.($data->pw?$data->pw->nama_pw:'pw').'/'.($data->pd?$data->pd->nama_pd:'pd').'/'.$data->foto) }}" >
                @else
                <img id="preview-image" alt="preview image" style="max-height: 150px;" src="{{ asset('images/anggota/default.png') }}" >
                @endif
            </div>
                <!--region general-->
                <hr>
                <section class="">
                    <div class="row pb-2">
                        <div class="col-md-12">
                            <h3 class="block-title">General Information</h3>
                            <div class="row pt-2">
                                <div class="col-md-8">PD</div>
                                <div class="col-md-4">{{ $data->pd->nama_pd }}</div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-md-8">Alamat</div>
                                <div class="col-md-4">{{ $data->alamat }}</div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-md-8">Ketua PD</div>
                                <div class="col-md-4">{{ $data->ketua_pd }}</div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-md-8">No HP</div>
                                <div class="col-md-4">{{ $data->no_hp }}</div>
                            </div>
                        </div>
                    </div>
                </section>
                <hr>

                <!--endregion-->

                <!--region Tasykil-->
                <section class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="block-title">Tasykil</h3>
                            <div class="row pt-2">
                                <div class="col-md-8">Indeks</div>
                                <div class="col-md-4">Jawaban</div>
                            </div>

                            <ol class="p-3">
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah komposisi tasykil utuh sesuai SK pengesahan?</li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q1a }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah setiap tasykil mempunyai pedoman jam'iyyah?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q2a }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah setiap tasykil memahami tugas pokoknya masing-masing?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q3a }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah setiap tasykil membuat laporan kinerja secara berkala?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q4a }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah Ketua membuat progress report tahunan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q5a }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Berjalankah jadwal ngantor rutin tasykil (minimal tiga kali sebulan)?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q6a }}</div>
                                </div>
                            </ol>
                        </div>
                    </div>
                </section>
                <hr>
                <!--endregion-->

                <!--region Aktivitas Jam'iyyah & Partisipasi Anggota-->
                <section class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="block-title">Aktivitas Jam'iyyah & Partisipasi Anggota</h3>
                            <div class="row pt-2">
                                <div class="col-md-8">Indeks</div>
                                <div class="col-md-4">Jawaban</div>
                            </div>
                            <ol class="p-3">
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah jumlah anggota di atas 25 orang?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q1b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Bagaimana frekuensi pertemuan tasykil dengan anggota?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q2b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD memiliki kegiatan rutin (minimal tiga kali sebulan)?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q3b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah kegiatan rutin itu berjalan dengan lancar?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q4b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Bagaimana rata-rata kehadiran anggota dalam kegiatan rutin PD? (sertakan
                                            bukti fisik)
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q5b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah ada kegiatan insidental di PD?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q6b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Bagaimana rata-rata kehadiran anggota dalam kegiatan insidental PD?
                                            (sertakan bukti fisik)
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q7b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah ada kegiatan jamiyyah bersama Persis dan otonom lainnya?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q8b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah ada kegiatan yang melibatkan organisasi lain?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q9b }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Bagaimana publikasi/undangan pada setiap kegiatan PD?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q10b }}</div>
                                </div>
                            </ol>
                        </div>
                    </div>
                </section>
                <hr>
                <!--endregion-->

                <!--region Kaderisasi-->
                <section class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="block-title">Kaderisasi</h3>
                            <div class="row pt-2">
                                <div class="col-md-8">Indeks</div>
                                <div class="col-md-4">Jawaban</div>
                            </div>
                            <ol class="p-3">
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Jumlah calon anggota yang tercatat ?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q1c }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Adakah program pembinaan bagi para calon anggota?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q2c }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Jumlah calon anggota yang mengikuti program pembinaan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q3c }}</div>
                                </div>
                            </ol>
                        </div>
                    </div>
                </section>
                <hr>
                <!--endregion-->

                <!--region Administrasi Jam'iyyah-->
                <section class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="block-title">Administrasi Jam'iyyah</h3>
                            <div class="row pt-2">
                                <div class="col-md-8">Indeks</div>
                                <div class="col-md-4">Jawaban</div>
                            </div>
                            <ol class="p-3">
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki sekretariat yang definitif?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q1d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah sekretariat sudah berfungsi sebagai sentra kegiatan jam'iyyah (rapat,
                                            ngantor,dll)?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q2d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD memiliki perangkat kesekretariatan (komputer/mesin tik)?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q3d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki Buku Agenda Surat Masuk?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q4d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah Buku Agenda Surat Masuk tersebut sudah digunakan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q5d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah surat-surat masuk minimal satu tahun terakhir sudah diarsipkan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q6d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki Buku Agenda Surat Keluar?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q7d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah Buku Agenda Surat Keluar tersebut sudah digunakan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q8d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah surat-surat keluar minimal satu tahun terakhir sudah diarsipkan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q9d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki Buku Induk Anggota?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q10d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah Buku Induk Anggota sudah digunakan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q11d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah semua data dalam Buku Induk Anggota sudah terisi?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q12d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah data yang terdapat dalam Buku Induk Anggota sesuai dengan keadaan
                                            sesungguhnya?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q13d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki kop surat?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q14d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah kop surat tersebut sudah sesuai dengan Pedoman Administrasi
                                            Jam’iyyah?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q15d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki stempel?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q16d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah stempel tersebut sesuai dengan Pedoman Administrasi Jam’iyyah?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q17d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah surat resmi PD sudah sesuai dengan Pedoman Administrasi (bentuk,
                                            format, penomoran, dll)?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q18d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki Buku Agenda Kegiatan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q19d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah Buku Agenda Kegiatan sudah digunakan sebagaimana mestinya?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q20d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah jamaah sudah memiliki Buku Daftar Hadir Kegiatan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q21d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah Buku Daftar Hadir Kegiatan dipakai dalam setiap kegiatan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q22d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki Buku Notulen Rapat/Musyawarah?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q23d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah Buku Notulen tersebut dipakai dalam setiap rapat/musyawarah?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q24d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD sudah memiliki Buku Daftar Tamu?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q25d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah jamaah sudah memiliki Buku Inventaris?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q26d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah semua data dalam buku inventaris tersebut lengkap?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q27d }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah data dalam buku inventaris sudah sesuai dengan kenyataannya?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q28d }}</div>
                                </div>
                            </ol>
                        </div>
                    </div>
                </section>
                <hr>
                <!--endregion-->


                <!--region Musyawarah-->
                <section class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="block-title">Musyawarah</h3>
                            <div class="row pt-2">
                                <div class="col-md-8">Indeks</div>
                                <div class="col-md-4">Jawaban</div>
                            </div>
                            <ol class="p-3">
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah masa jihad PD sesuai masa jihad yang tertuang dalam SK pengesahan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q1e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah SK pengesahan PD/tasykil masih ada?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q2e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Berapa kali PD melakukan musyawarah kerja selama masa jihad?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q3e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PC-PC?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q4e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PW Pemuda?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q5e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PD Persis?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q6e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PD Persistri?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q7e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah hasil musyawarah kerja tersebut disosialisasikan kepada PD Pemudi?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q8e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Berapa kali PD melakukan rapat koordinasi dengan PC dalam satu tahun?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q9e }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Bagaimana rata-rata kehadiran PC dalam rapat koordinasi tersebut?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q10e }}</div>
                                </div>
                            </ol>
                        </div>
                    </div>
                </section>
                <hr>
                <!--endregion-->

                <!--region Keuangan Jam'iyyah-->
                <section class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="block-title">Keuangan Jam'iyyah</h3>
                            <div class="row pt-2">
                                <div class="col-md-8">Indeks</div>
                                <div class="col-md-4">Jawaban</div>
                            </div>
                            <ol class="p-3">
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Bagaimana partisipasi anggota dalam iuran wajib?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q1f }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD rutin setiap bulan menjemput iuran anggota?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q2f }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD memiliki catatan lengkap tentang tagihan iuran anggota?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q3f }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD mengusahakan sumber-sumber pemasukan keuangan lain selain iuran
                                            anggota?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q4f }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah PD memiliki pembukuan keuangan?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q5f }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Apakah sistem pembukuan keuangan jam'iyyah sudah sesuai dengan Pedoman
                                            Pembukuan Keuangan Jam'iyyah?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q6f }}</div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-8">
                                        <li>Bagaimana kondisi neraca/saldo tahunan PD?
                                        </li>
                                    </div>
                                    <div class="col-md-4">{{ $data->q7f }}</div>
                                </div>
                            </ol>
                        </div>
                    </div>
                </section>
                <hr>
                <!--endregion-->

            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->
@endsection
