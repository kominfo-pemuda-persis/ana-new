@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/moment/locales.js') }}"></script>

    <!-- Page JS Code -->
    <script>
        moment.locale('id');
        let dataTableUrl = "{{ route('pimpinanCabang.datatables') }}";
        let addUrl = "{{ route('pimpinanCabang.store') }}";
    </script>
<script src="{{ asset('js/pages/pg_pc.js?id=') . Str::random(25) }}"></script>
@endsection

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    Data PC <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">List</small>
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">Data PC</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="/dashboard">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table with Export Buttons -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Table <small>Data PC</small></h3>
{{--                    <a href="#" class="btn btn-danger btn-sm" style="margin-right: 5px"><span class="si si-trash"></span> Sampah</a>--}}

                {{--                check permission create--}}
                @permission('masterpc.create')
                <button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Tambah PC" onclick="openModal('#modal-form','add')"><span class="si si-plus"></span> Tambah</button>
                @endpermission
            </div>
            <div class="block-content block-content-full">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-pc">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 80px;">No</th>
                                <th style="width: 20%;">Kode PC</th>
                                <th>Nama PC</th>
                                <th>Nama PD</th>
                                <th>Nama PW</th>
                                <th style="width: 15%;">Diresmikan</th>
                                <th class="text-center" style="width: 80px;">#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Dynamic Table with Export Buttons -->
    </div>
    <!-- END Page Content -->

<!-- Fade In Block Modal -->
{{--    check permission create or update--}}
@permission('masterpc.create','masterpc.update')
<div class="modal fade" id="modal-form" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title modal-title">Tambah PC</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form class="form-program" action="{{ route('pimpinanCabang.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_prev" id="id_prev">
                    <div class="block-content font-size-sm row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>PW <span class="text-danger">*</span></label>
                                <br>
                                <select id="pw" class="picker form-control" name="kd_pw" required></select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>PD <span class="text-danger">*</span></label>
                                <br>
                                <select id="pd" class="picker form-control" name="kd_pd" required></select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kode PC <span class="text-danger">*</span></label>
                                <br>
                                <input type="text" id="kd_pc" class="form-control" name="kd_pc" required></input>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama PC <span class="text-danger">*</span></label>
                                <br>
                                <input type="text" id="nama_pc" class="form-control" name="nama_pc" required></input>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="diresmikan">Diresmikan <span class="text-danger">*</span></label>
                                <input type="date" id="diresmikan" class="form-control" name="diresmikan" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="progress push progress-add" style="display:none">
                                <div style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-upload-rec progress-bar progress-bar-striped progress-bar-animated bg-info">0%</div>
                            </div>
                        </div>
                    </div>

                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-success" id="btn-form" ><i class="far fa-save mr-1"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endpermission
<!-- END Fade In Block Modal -->
@endsection
