@extends('layouts.simple')
@section('js_after')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"
    integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    function getJSONForm(formElement){
            let obj = {};
            let form = $(formElement).serializeArray().filter(function(val, index){
                return !['_token', 'date', 'npa_select'].includes(val.name);
            }).forEach(function(val, index){
                obj[val.name] = val.value
            })
            return obj;
        }

        // select2
        $('.picker').select2({
            width: '100%',
            placeholder: 'Cari Anggota...',
            ajax: {
                url: '/anggota/picker?level_tafiq=2',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: `${item.npa} - ${item.nama_lengkap}`,
                                id: item.id_anggota,
                                npa: item.npa,
                                nama: item.nama_lengkap,
                                email: item.email,
                                pimpinan_cabang: item.pimpinan_cabang
                            }
                        })
                    };
                },
                cache: true
            }
        });

        $('.picker').on("select2:select", function (e) {
            $("input[name=npa]").val(e.params.data.npa)
            $("input[name=nama]").val(e.params.data.nama)
            $("input[name=email]").val(e.params.data.email)
        })

         $('#tafiq3-reg-date').flatpickr(
            {
                mode: "range",
                dateFormat: "Y-m-d",
                onChange: function(selectedDates){
                    let start = this.formatDate(selectedDates[0], "j F Y")
                    let end = "";

                    $("#tanggal_tafiq_start").val(this.formatDate(selectedDates[0], "Y-m-d"));

                    if(selectedDates.length > 1){
                        end = this.formatDate(selectedDates[1], "j F Y")
                        $("#tanggal_tafiq_end").val(this.formatDate(selectedDates[1], "Y-m-d"));
                    }

                    $("#tafiq3-reg-date").val(`${start} - ${end}`);
                },
                onClose: function (selectedDates) {

                    if(selectedDates.length <= 0) return;

                    let start = this.formatDate(selectedDates[0], "j F Y")
                    let end = ""

                    if(selectedDates.length < 2){
                        end_tmp = new Date(selectedDates[0]);
                        end_tmp.setDate(selectedDates[0].getDate() + 1);
                        selectedDates[1] = end_tmp;
                        end = this.formatDate(selectedDates[1], "j F Y")
                    }else{
                        end = this.formatDate(selectedDates[1], "j F Y")
                    }

                    $("#tafiq3-reg-date").val(`${start} - ${end}`);
                    $("#tanggal_tafiq_start").val(this.formatDate(selectedDates[0], "y-m-d"));
                    $("#tanggal_tafiq_end").val(this.formatDate(selectedDates[1], "y-m-d"));
                }
            });

            $("#form-tafiq3").validate({
                rules: {
                    lokasi: "required",
                    tanggal_tafiq_start: "required",
                    tanggal_tafiq_end: "required",
                    npa_select: "required",
                    date: "required"
                },
                messages: {
                    npa_select: "Silahkan isi NPA Anda",
                    date: "Silahkan isi Tanggal Tafiq 1 Anda",
                    lokasi: "Silahkan isi Lokasi Anda",
                    tanggal_tafiq_start: "Silahkan isi Tanggal Mulai Anda",
                    tanggal_tafiq_end: "Silahkan isi Tanggal Akhir Anda",
                },
                errorClass: "is-invalid",
                submitHandler: function (form, event) {
                    $form = $(form);
                    event.preventDefault();

                    swal({
                        title: 'Saving data..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    })

                    var button = $form.find('button[type="submit"]');
                    button.attr('disabled', 'disabled');
                    button.text('menyimpan..');


                    $.ajax({
                        url: $form.attr('action'),
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        },
                        contentType : "application/json",
                        dataType: "json",
                        data: JSON.stringify(getJSONForm(form)),
                        type: 'POST',
                        success: function (res) {
                            form.reset();
                            swal({
                                title: "Berhasil!",
                                text: res.message,
                                type: "success",
                                confirmButtonColor: "#3498DB",
                                closeOnConfirm: true
                            }).then((result) => {
                                button.removeAttr('disabled');
                                button.html('<i class="far fa-save mr-1"></i> Simpan');
                                window.location = '/tafiq3';
                            });
                        },

                        error: function (jqXHR, exception) {
                            swal.close();
                            let message = `Terjadi kendala teknis, Mohon kontak Bidang Administrasi PP. <br> info error: <b>${jqXHR.responseJSON.message}</b>`
                            swal("Upload ditolak", message, "error");
                            button.removeAttr('disabled');
                            button.html('<i class="far fa-save mr-1"></i> Simpan');
                        },
                    });
                },
                errorPlacement: function (error, element) {
                    let name = $(element).attr("name");
                    if (name === 'file-excel') {
                        console.log(element.parent().parent().find(".invalid-feedback-file.normal-form").append(error));
                    }else if (name === 'foto-upload') {
                        console.log(element.parent().parent().find(".invalid-feedback-file.normal-form").append(error));
                    } else {
                        element.parent().find(".invalid-feedback.normal-form").append(error);
                    }
                }
            });
</script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-V33JPYB1SF"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-V33JPYB1SF');
</script>

@endsection

@section('css_after')
<link rel="stylesheet" href="{{ asset('/js/plugins/sweetalert2/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<style>
    .select2-container .select2-selection--single {
        height: 40px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 37px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 37px !important;
    }

    #tafiq3-reg-date .form-control:disabled,
    .form-control[readonly] {
        background-color: #ffffff
    }
</style>
@endsection

@section('content')
<div class="bg-white overflow-scroll">
    <form id="form-tafiq3" action="{{ route("post.tafiq3") }}">
        @csrf
        <div>
            <div class="content content-full text-center" style="height: 100vh">
                <h1 class="display-4 font-w300 mb-3">
                    <img src="{{ asset("media/res/ANA ONLINE 1.png") }}" width="5%" />
                    <span>
                        <img src="{{ asset("media/res/ANA ONLINE 2.png") }}" width="20%">
                    </span>
                </h1>
                <h2 class="h3 font-w400 text-muted mb-2">
                    Form Tafiq 3
                </h2>
                <div class="flex flex-row items-center mb-5">
                    <div class="col-xl-4 col-lg-6 col-sm-12 mx-auto text-left">
                        <div class="form-group">
                            <label for="npa">NPA</label>
                            <select id="npa_tafiq3" name="npa_select" class="picker form-control" required></select>
                            <div class="invalid-feedback normal-form">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lokasi">Lokasi</label>
                            <input class="form-control" type="text" id="lokasi" name="lokasi" placeholder="Input lokasi"
                                required>
                            <div class="invalid-feedback normal-form">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="anggota-reg-date">Tanggal Tafiq 3</label>
                            <div class="form-group">
                                <input class="form-control" placeholder="Select date" type="date" name="date"
                                    id="tafiq3-reg-date" required>
                                <div class="invalid-feedback normal-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input class="form-control" type="hidden" id="tanggal_tafiq_start" name="tanggal_tafiq_start" required>
                <input class="form-control" type="hidden" id="tanggal_tafiq_end" name="tanggal_tafiq_end" required>
                <input class="form-control" type="hidden" id="npa" name="npa" required>
                <input class="form-control" type="hidden" id="nama" name="nama" required>
                <input class="form-control" type="hidden" id="email" name="email" required>
                <span class="m-2 d-inline-block">
                    <button class="btn btn-success px-4 py-2" type="submit">
                        <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Submit
                    </button>
                </span>
            </div>
    </form>
</div>
@endsection
