#!/bin/bash

echo \#\!\/bin\/bash >>init.sh

echo export APP_ENV=$APP_ENV_PROD >>init.sh
echo export APP_DEBUG=false >>init.sh
echo export APP_KEY=$APP_KEY >>init.sh
echo export REPOSITORY_ECR_URL=$REPOSITORY_ECR_URL >>init.sh
echo export CONTAINER_APP_PROD=$CONTAINER_APP_PROD >>init.sh

echo export SESSION_DRIVER=$PROD_SESSION_DRIVER >>init.sh

echo export PROD_MYSQL_PASSWORD=$PROD_MYSQL_PASSWORD >>init.sh
echo export PROD_MYSQL_USER=$PROD_MYSQL_USER >>init.sh
echo export PROD_MYSQL_DATABASE=$PROD_MYSQL_DATABASE >>init.sh
echo export PROD_MYSQL_PORT=$PROD_MYSQL_PORT >>init.sh
echo export PROD_MYSQL_HOST=$PROD_MYSQL_HOST >>init.sh
echo export PROD_MYSQL_CONNECTION=$PROD_MYSQL_CONNECTION >>init.sh

echo export PROD_PORT_MAPPING=$PROD_PORT_MAPPING >>init.sh
echo export PROD_PORT_MAPPING_PHPMYADMIN=$PROD_PORT_MAPPING_PHPMYADMIN >>init.sh

echo export AWS_BUCKET_PROD=$AWS_BUCKET_PROD >>init.sh
echo export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION >>init.sh
echo export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY >>init.sh
echo export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID >>init.sh

echo export PROD_MAIL_ENCRYPTION=$PROD_MAIL_ENCRYPTION >>init.sh
echo export PROD_MAIL_PASSWORD=$PROD_MAIL_PASSWORD >>init.sh
echo export PROD_MAIL_USERNAME=$PROD_MAIL_USERNAME >>init.sh
echo export PROD_MAIL_PORT=$PROD_MAIL_PORT >>init.sh
echo export PROD_MAIL_HOST=$PROD_MAIL_HOST >>init.sh
echo export PROD_MAIL_DRIVER=$PROD_MAIL_DRIVER >>init.sh
echo export MAIL_FROM_ADDRESS=$MAIL_FROM_ADDRESS >>init.sh
echo export MAIL_FROM_NAME=$MAIL_FROM_NAME >>init.sh
echo export MAILJET_HOST=$MAILJET_HOST >>init.sh
echo export MAILJET_PORT=$MAILJET_PORT >>init.sh
echo export MAILJET_APIKEY=$MAILJET_APIKEY >>init.sh
echo export MAILJET_APISECRET=$MAILJET_APISECRET >>init.sh

echo export TAG_ANA=$CI_COMMIT_REF_NAME >>init.sh
echo export S3_STATIC_FOLDER=$S3_STATIC_FOLDER_PROD >>init.sh
echo export SENTRY_LARAVEL_DSN=$SENTRY_LARAVEL_DSN >>init.sh
echo export SENTRY_TRACES_SAMPLE_RATE=$SENTRY_TRACES_SAMPLE_RATE >>init.sh

echo export NOCAPTCHA_SECRET_PROD=$NOCAPTCHA_SECRET_PROD >>init.sh
echo export NOCAPTCHA_SITEKEY_PROD=$NOCAPTCHA_SITEKEY_PROD >>init.sh

echo "aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin $ECR_URL" >>init.sh
echo docker-compose down >>init.sh
echo docker system prune -af >>init.sh
echo docker pull $REPOSITORY_ECR_URL:$CI_COMMIT_REF_NAME >>init.sh
echo docker-compose up -d >>init.sh
