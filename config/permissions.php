<?php

return [
    'module_permissions' => [
        'dashboard' => [
            'read'
        ],
        'caang' => [
            'read',
            'update',
            'delete',
            'export',
        ],
        'tafiq1' => [
            'read',
            'update',
            'delete',
            'approve'
        ],
        'tafiq2' => [
            'read',
            'update',
            'delete',
            'approve'
        ],
        'tafiq3' => [
            'read',
            'update',
            'delete',
            'approve'
        ],
        'herregistrasi' => [
            'read',
            'approve',
            'setstatus',
        ],
        'rekapcaang' => [
            'read',
            'approve',
            'setstatus',
        ],
        'anggota' => [
            'create',
            'read_pj',
            'read_pc',
            'read_pd',
            'read_pw',
            'read_all',
            'update_pj',
            'update_pc',
            'update_pd',
            'update_pw',
            'update_all',
            'delete',
        ],
        'mutasi' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'otonom' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'bendahara' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'tagihan' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'transaksi' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'logtransaksi' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'jamiyyah' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'role' => [
            'read',
            'create',
            'update',
            'delete',
            'assignment'
        ],
        'permission' => [
            'read',
            'update',
            'delete'
        ],
        'users' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'masterpj' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'masterpc' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'masterpd' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'masterpw' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'monografipw' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'monografipd' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'monografipc' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'monografipj' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'indexperformapw' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'indexperformapd' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'indexperformapc' => [
            'create',
            'read',
            'update',
            'delete'
        ],
        'indexperformapj' => [
            'create',
            'read',
            'update',
            'delete'
        ],
    ],
];
