<?php

return [
    'role_permissions' => [
        'anggota' => [
            'dashboard' => ['read'],
        ],
        'superadmin' => [],
        'admin_pp' => [
            'dashboard' => ['read'],
            'caang' => [
                'read',
                'update',
                'delete',
                'export',
            ],
            'herregistrasi' => [
                'read',
                'approve',
                'setstatus',
            ],
            'rekapcaang' => [
                'read',
                'approve',
                'setstatus',
            ],
            'anggota' => [
                'create',
                'read_pj',
                'read_pc',
                'read_pd',
                'read_pw',
                'read_all',
                'update_pj',
                'update_pc',
                'update_pd',
                'update_pw',
                'update_all',
                'delete',
            ],
            'mutasi' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'role' => [
                'read',
                'update',
            ],
            'permission' => [
                'read',
                'update',
            ],
            'users' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'monografipw' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapw' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'bendahara' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'tagihan' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'transaksi' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'logtransaksi' => [
                'create',
                'read',
                'update',
                'delete',
            ]
        ],
        'tasykil_pp' => [
            'dashboard' => ['read'],
            'anggota' => [
                'read_pj',
                'read_pc',
                'read_pd',
                'read_pw',
                'read_all',
                'update_pj',
                'update_pc',
                'update_pd',
                'update_pw',
                'update_all',
            ],
            'mutasi' => [
                'read',
                'update',
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'role' => [
                'read',
            ],
            'permission' => [
                'read',
            ],
            'users' => [
                'read',
                'update',
            ],
            'monografipw' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapw' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'bendahara' => [
                'read',
            ],
            'logtransaksi' => [
                'create',
                'read',
                'update',
                'delete',
            ]
        ],
        'admin_pw' => [
            'dashboard' => ['read'],
            'anggota' => [
                'read_pj',
                'read_pc',
                'read_pd',
                'read_pw',
                'update_pj',
                'update_pc',
                'update_pd',
                'update_pw',
            ],
            'mutasi' => [
                'read',
                'update',
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'role' => [
                'read',
            ],
            'permission' => [
                'read',
            ],
            'users' => [
                'read',
                'update',
            ],
            'monografipw' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapw' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'bendahara' => [
                'read',
            ],
            'logtransaksi' => [
                'create',
                'read',
                'update',
                'delete',
            ]
        ],
        'tasykil_pw' => [
            'dashboard' => ['read'],
            'anggota' => [
                'read_pj',
                'read_pc',
                'read_pd',
                'read_pw',
                'update_pj',
                'update_pc',
                'update_pd',
                'update_pw',
            ],
            'mutasi' => [
                'read',
                'update',
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'role' => [
                'read',
            ],
            'permission' => [
                'read',
            ],
            'users' => [
                'read',
                'update',
            ],
            'monografipw' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapw' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'bendahara' => [
                'read',
            ]
        ],
        'admin_pd' => [
            'dashboard' => ['read'],
            'anggota' => [
                'read_pj',
                'read_pc',
                'read_pd',
                'update_pj',
                'update_pc',
                'update_pd',
            ],
            'mutasi' => [
                'read',
                'update',
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'role' => [
                'read',
            ],
            'permission' => [
                'read',
            ],
            'users' => [
                'read',
                'update',
            ],
            'monografipd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'bendahara' => [
                'read',
            ],
            'logtransaksi' => [
                'create',
                'read',
                'update',
                'delete',
            ]
        ],
        'tasykil_pd' => [
            'dashboard' => ['read'],
            'anggota' => [
                'read_pj',
                'read_pc',
                'read_pd',
                'update_pj',
                'update_pc',
                'update_pd',
            ],
            'mutasi' => [
                'read',
                'update',
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'role' => [
                'read',
            ],
            'permission' => [
                'read',
            ],
            'users' => [
                'read',
                'update',
            ],
            'monografipd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapd' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'bendahara' => [
                'read',
            ]
        ],
        'admin_pc' => [
            'dashboard' => ['read'],
            'anggota' => [
                'create',
                'read_pj',
                'read_pc',
                'update_pj',
                'update_pc',
            ],
            'mutasi' => [
                'create',
                'read',
                'update',
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'role' => [
                'read',
            ],
            'permission' => [
                'read',
            ],
            'users' => [
                'create',
                'read',
                'update',
                'delete'
            ],'monografipc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'bendahara' => [
                'read',
            ],
            'transaksi' => [
                'create',
                'read',
                'update',
                'delete',
            ]
        ],
        'tasykil_pc' => [
            'dashboard' => ['read'],
            'anggota' => [
                'create',
                'read_pj',
                'read_pc',
                'update_pj',
                'update_pc',
            ],
            'mutasi' => [
                'create',
                'read',
                'update',
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'role' => [
                'read',
            ],
            'permission' => [
                'read',
            ],
            'users' => [
                'create',
                'read',
                'update',
                'delete'
            ],
            'monografipc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapc' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapj' => [
                'create',
                'read',
                'update',
                'delete',
            ]
        ],
        'admin_pj' => [
            'dashboard' => ['read'],
            'anggota' => [
                'read_pj',
                'update_pj',
            ],
            'mutasi' => [
                'read',
                'update',
            ],
            'otonom' => [
                'read',
            ],
            'jamiyyah' => [
                'read',
                'update',
            ],
            'role' => [
                'read',
            ],
            'permission' => [
                'read',
            ],
            'users' => [
                'read',
                'update',
            ],
            'monografipj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
            'indexperformapj' => [
                'create',
                'read',
                'update',
                'delete',
            ],
        ],
    ],
];
