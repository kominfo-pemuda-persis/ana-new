<?php

use App\Http\Controllers\GoogleAnalyticsController;
use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Lfm;

Route::get("/", "DashboardController@landing_page");
Route::view("/about", 'landing_page.about');
Route::view("/portfolio", 'landing_page.portfolio');
Route::view("/team", 'landing_page.team');
Route::view("/contact", 'landing_page.contact');
Route::get("/anaonline", "DashboardController@goToSlash");
Route::get("/analytics", GoogleAnalyticsController::class);


Route::get("/registrasi", "RegistrasiController@index")->name("registrasi");
Route::post(
    "/proses-form-registrasi",
    "RegistrasiController@processRegistrationForm"
)->name("post.registrasi");

Route::get("/heregistrasi", "HerRegistrasiController@index")->name(
    "heregistrasi"
);

Route::get("/tafiq1", "Tafiq1Controller@index")->name(
    "tafiq1.index"
);

Route::post("/tafiq1", "Tafiq1Controller@postData")->name(
    "post.tafiq1"
);

Route::get("/tafiq2", "Tafiq2Controller@index")->name(
    "tafiq2.index"
);

Route::post("/tafiq2", "Tafiq2Controller@postData")->name(
    "post.tafiq2"
);

Route::get("/tafiq3", "Tafiq3Controller@index")->name(
    "tafiq3.index"
);

Route::post("/tafiq3", "Tafiq3Controller@postData")->name(
    "post.tafiq3"
);

Route::post(
    "/proses-form-heregistrasi",
    "HerRegistrasiController@processHerRegistrationForm"
)->name("post.heregistrasi");

Route::middleware("auth")->group(function () {
    Route::match(["get", "post"], "/dashboard", [
        "middleware" => ["permission:dashboard.*"],
        "as" => "dashboard",
        "uses" => "DashboardController@index",
    ]);

    Route::group([
        "prefix" => "/data-user", "middleware" => ["permission:users.*"]
    ], function(){
        Route::get("/", "DataUserController@index")->name("data-user.index");
        Route::get("/{id_login}", "DataUserController@show")->name("data-user.show");
        Route::get("/{id_login}/edit", "DataUserController@edit")->name("data-user.edit");
        Route::put('/{id_login}/update', "DataUserController@update")->name("data-user.update");

        // destroy
        Route::delete(
            "/{id}/delete",
            "DataUserController@destroy"
        )->name("data-user.delete");
    });

    // Route::resource("/data-user", "DataUserController")->middleware([
    //     "permission:users.*",
    // ]);

    Route::group([
        "prefix" => "/data-tafiq1"
    ], function () {
        Route::get("/", "Tafiq1Controller@viewData")->name("viewdata.tafiq1");
        Route::get("/table", "Tafiq1Controller@dataTable")->name("datatable.tafiq1");
        Route::post("/set-status", "Tafiq1Controller@setStatus")->name(
            "post.status.tafiq1"
        );
        Route::delete("/{id}/delete", "Tafiq1Controller@delete")->name(
            "delete.tafiq1"
        );

        Route::get("/{id}/edit", "Tafiq1Controller@indexEdit")->name(
            "view.edit.tafiq1"
        );
        Route::put("/{id}/edit", "Tafiq1Controller@updateData")->name(
            "update.tafiq1"
        );
    });

    Route::group([
        "prefix" => "/data-tafiq2"
    ], function () {
        Route::get("/", "Tafiq2Controller@viewData")->name("viewdata.tafiq2");
        Route::get("/table", "Tafiq2Controller@dataTable")->name("datatable.tafiq2");
        Route::post("/set-status", "Tafiq2Controller@setStatus")->name(
            "post.status.tafiq2"
        );
        Route::delete("/{id}/delete", "Tafiq2Controller@delete")->name(
            "delete.tafiq2"
        );

        Route::get("/{id}/edit", "Tafiq2Controller@indexEdit")->name(
            "view.edit.tafiq2"
        );
        Route::put("/{id}/edit", "Tafiq2Controller@updateData")->name(
            "update.tafiq2"
        );
    });

    Route::group([
        "prefix" => "/data-tafiq3"
    ], function () {
        Route::get("/", "Tafiq3Controller@viewData")->name("viewdata.tafiq3");
        Route::get("/table", "Tafiq3Controller@dataTable")->name("datatable.tafiq3");
        Route::post("/set-status", "Tafiq3Controller@setStatus")->name(
            "post.status.tafiq3"
        );
        Route::delete("/{id}/delete", "Tafiq3Controller@delete")->name(
            "delete.tafiq3"
        );

        Route::get("/{id}/edit", "Tafiq3Controller@indexEdit")->name(
            "view.edit.tafiq3"
        );
        Route::put("/{id}/edit", "Tafiq3Controller@updateData")->name(
            "update.tafiq3"
        );
    });

    Route::group([
        "prefix" => "/esyahadah"
    ], function () {
        Route::get("/", "EsyahadahController@index")->name("esyahadah.index");
        Route::get("/pdf/{id}", "EsyahadahController@generatePDF")->name("esyahadah.pdf");
        Route::get("/tafiq1/pdf/{npa}", "Tafiq1Controller@viewPDF")->name("esyahadah.tafiq1.pdf");
        Route::get("/tafiq2/pdf/{npa}", "Tafiq2Controller@viewPDF")->name("esyahadah.tafiq2.pdf");
        Route::get("/tafiq3/pdf/{npa}", "Tafiq3Controller@viewPDF")->name("esyahadah.tafiq3.pdf");
        Route::get("/tafiq1/view/{npa}", "Tafiq1Controller@viewEsyahadahHTML")->name("esyahadah.tafiq1.view");
        Route::get("/tafiq2/view/{npa}", "Tafiq2Controller@viewEsyahadahHTML")->name("esyahadah.tafiq2.view");
        Route::get("/tafiq3/view/{npa}", "Tafiq3Controller@viewEsyahadahHTML")->name("esyahadah.tafiq3.view");
        Route::post("/send-email", "EsyahadahController@sendEmailEsyahadah")->name("generate.esyahadah.pdf");
    });

    Route::group([
        "prefix" => "/data-document"
    ], function () {
        Route::get("/", "FileDocumentController@index")->name("doc_file.index");
        Route::get("/datatables", "FileDocumentController@dataTables")->name("doc_file.datatables");
        Route::post("/upload", "FileDocumentController@uploadDocument")->name("doc_file.upload");
        Route::post("/delete", "FileDocumentController@deleteDocument")->name("doc_file.delete");
    });

    Route::group(
        ["prefix" => "role", "middleware" => ["permission:role.*"]],
        function () {
            Route::get("/", "RoleController@index")->name("role.index");
            Route::get("/add", "RoleController@create")->name("role.create");
            Route::get("/get/{id}", "RoleController@get")->name("role.get");
            Route::get("/datatables", "RoleController@dataTables")->name(
                "role.datatables"
            );
            Route::get("/permission/{id}", "RoleController@permission")->name(
                "role.permission"
            );
            // manipulate data
            Route::post("/store", "RoleController@store")->name("role.store");
            Route::post("/{id}/assign", "RoleController@assign")->name(
                "role.assign"
            );
            Route::put("/{id}/update", "RoleController@update")->name(
                "role.update"
            );
            Route::post("/delete", "RoleController@delete")->name(
                "role.delete"
            );
        }
    );

    Route::group(
        ["prefix" => "anggota", "middleware" => ["permission:anggota.*"]],
        function () {
            //chek exist
            // Route::get('/npa', 'AnggotaController@npa')->name('anggota.npa');
            // Route::get('/email', 'AnggotaController@email')->name('anggota.email');

            // Anggota Per PC
            Route::get("/pc/{id}", "AnggotaController@indexPc")->name(
                "anggota.pc"
            );
            // Anggota
            Route::get("/", "AnggotaController@index")->name("anggota.index");
            Route::get("/picker", "AnggotaController@picker")->name(
                "anggota.picker"
            )->withoutMiddleware(["auth", "permission:anggota.*"]);
            Route::get("/add", "AnggotaController@create")->name(
                "anggota.create"
            );
            // Route::get('/{id}/edit', 'AnggotaController@edit')->name('anggota.edit');
            // Route::get('/profil/{id}', 'AnggotaController@profile')->name('anggota.profil');
            Route::get("/datatables", "AnggotaController@dataTables")->name(
                "anggota.datatables"
            );
            Route::get(
                "/datatables/{id}",
                "AnggotaController@dataTablesPc"
            )->name("anggota.datatablesPc");

            // manipulate data
            Route::post("/store", "AnggotaController@store")->name(
                "anggota.store"
            );
            // Route::put('/{id}/update', 'AnggotaController@update')->name('anggota.update');
            Route::delete("/{id}/delete", "AnggotaController@delete")->name(
                "anggota.delete"
            );

            // trash
            Route::get("/trash", "AnggotaController@trash")->name(
                "anggota.trash"
            );
            Route::put(
                "/trash/{id}/restore",
                "AnggotaController@restore"
            )->name("anggota.trash.restore");
            Route::get(
                "/trash/datatables",
                "AnggotaController@dataTablesTrashAnggota"
            )->name("anggota.trash.datatables");

            // destroy
            Route::delete(
                "/trash/{id}/delete",
                "AnggotaController@destroy"
            )->name("anggota.trash.destroy");

            //export template tabel anggota
            Route::get("/export-template", "AnggotaController@exportTemplate");
            Route::post(
                "/export-data-anggota",
                "AnggotaController@exportDataAnggota"
            )->name("anggota.export.data");

            Route::post(
                "/export-pdf-anggota",
                "AnggotaController@exportPDF"
            )->name("anggota.export.pdf");

            //import excel data anggota
            Route::get("/show", "AnggotaController@showImport");
            Route::post("/import", "AnggotaController@import");
        }
    );

    // profile
    Route::group(["prefix" => "anggota"], function () {
        //chek exist
        Route::get("/npa", "AnggotaController@npa")->name("anggota.npa");
        Route::get("/email", "AnggotaController@email")->name("anggota.email");

        // update password
        Route::get("/password-form", "AnggotaController@showPasswordForm")
            ->name("anggota.password-form");
        Route::post("/update-password", "AnggotaController@updatePassword")
            ->name("anggota.update-password");

        Route::get("/profil/{id}", "AnggotaController@profile")->name(
            "anggota.profil"
        );
        Route::put("/{id}/update", "AnggotaController@update")->name(
            "anggota.update"
        );
        Route::get("/{id}/edit", "AnggotaController@edit")->name(
            "anggota.edit"
        );
        Route::get("/{id}/edit-profile", "AnggotaController@editProfile")->name(
            "anggota.edit-profile"
        );
        Route::get("/card/{id}", "AnggotaController@profileCard")->name(
            "anggota.profileCard"
        );
        Route::get("/photo/{id}", "AnggotaController@getImageProfile")->name(
            "anggota.getImageProfile"
        );
        Route::put(
            "/{id}/edit-foto",
            "AnggotaController@updateImageProfile"
        )->name("anggota.editFotoProfile");
        Route::get(
            "/template/ekta/{id}",
            "AnggotaController@templateCard"
        )->name("anggota.templateCard");
    });

    // anggota Keluarga
    Route::group(["prefix" => "anggota/keluarga"], function () {
        Route::get("/{id_anggota}", "AnggotaKeluargaController@index")->name(
            "anggotaKeluarga.index"
        );
        Route::get(
            "/datatables/{id_anggota}",
            "AnggotaKeluargaController@dataTables"
        )->name("anggotaKeluarga.datatables");
        Route::get("/get/{id}", "AnggotaKeluargaController@get")->name(
            "anggotaKeluarga.get"
        );
        // manipulate data
        Route::post("/store", "AnggotaKeluargaController@store")->name(
            "anggotaKeluarga.store"
        );
        Route::put("/{id}/update", "AnggotaKeluargaController@update")->name(
            "anggotaKeluarga.update"
        );
        Route::delete("/{id}/delete", "AnggotaKeluargaController@delete")->name(
            "anggotaKeluarga.delete"
        );

        // trash
        Route::get(
            "{id_anggota}/trash",
            "AnggotaKeluargaController@trash"
        )->name("anggotaKeluarga.trash");
        Route::put(
            "{id_anggota}/trash/{id_anggota_keluarga}/restore",
            "AnggotaKeluargaController@restore"
        )->name("anggotaKeluarga.trash.restore");
        Route::get(
            "{id_anggota}/trash/datatables",
            "AnggotaKeluargaController@dataTablesTrash"
        )->name("anggotaKeluarga.trash.datatables");
    });

    // anggota Pendidikan
    Route::group(["prefix" => "anggota/pendidikan"], function () {
        Route::get("/{id_anggota}", "AnggotaPendidikanController@index")->name(
            "anggotaPendidikan.index"
        );
        Route::get(
            "/datatables/{id_anggota}",
            "AnggotaPendidikanController@dataTables"
        )->name("anggotaPendidikan.datatables");
        Route::get("/get/{id}", "AnggotaPendidikanController@get")->name(
            "anggotaPendidikan.get"
        );
        // manipulate data
        Route::post("/store", "AnggotaPendidikanController@store")->name(
            "anggotaPendidikan.store"
        );
        Route::put("/{id}", "AnggotaPendidikanController@update")->name(
            "anggotaPendidikan.update"
        );
        Route::delete(
            "/{id}/delete",
            "AnggotaPendidikanController@delete"
        )->name("anggotaPendidikan.delete");

        // trash
        Route::get(
            "{id_anggota}/trash",
            "AnggotaPendidikanController@trash"
        )->name("anggotaPendidikan.trash");
        Route::put(
            "{id_anggota}/trash/{id_anggota_keluarga}/restore",
            "AnggotaPendidikanController@restore"
        )->name("anggotaPendidikan.trash.restore");
        Route::get(
            "{id_anggota}/trash/datatables",
            "AnggotaPendidikanController@dataTablesTrash"
        )->name("anggotaPendidikan.trash.datatables");
    });

    // anggota Pekerjaan
    Route::group(["prefix" => "anggota/pekerjaan"], function () {
        Route::get("/{id_anggota}", "AnggotaPekerjaanController@index")->name(
            "anggotaPekerjaan.index"
        );
        Route::get(
            "/datatables/{id_anggota}",
            "AnggotaPekerjaanController@dataTables"
        )->name("anggotaPekerjaan.datatables");
        Route::get("/get/{id}", "AnggotaPekerjaanController@get")->name(
            "anggotaPekerjaan.get"
        );
        // manipulate data
        Route::post("/store", "AnggotaPekerjaanController@store")->name(
            "anggotaPekerjaan.store"
        );
        Route::put("/{id}", "AnggotaPekerjaanController@update")->name(
            "anggotaPekerjaan.update"
        );
        Route::delete(
            "/{id}/delete",
            "AnggotaPekerjaanController@delete"
        )->name("anggotaPekerjaan.delete");

        // trash
        Route::get(
            "{id_anggota}/trash",
            "AnggotaPekerjaanController@trash"
        )->name("anggotaPekerjaan.trash");
        Route::put(
            "{id_anggota}/trash/{id_anggota_keluarga}/restore",
            "AnggotaPekerjaanController@restore"
        )->name("anggotaPekerjaan.trash.restore");
        Route::get(
            "{id_anggota}/trash/datatables",
            "AnggotaPekerjaanController@dataTablesTrash"
        )->name("anggotaPekerjaan.trash.datatables");
    });

    // anggota Keterampilan
    Route::group(["prefix" => "anggota/keterampilan"], function () {
        Route::get(
            "/{id_anggota}",
            "AnggotaKeterampilanController@index"
        )->name("anggotaKeterampilan.index");
        Route::get(
            "/datatables/{id_anggota}",
            "AnggotaKeterampilanController@dataTables"
        )->name("anggotaKeterampilan.datatables");
        Route::get("/get/{id}", "AnggotaKeterampilanController@get")->name(
            "anggotaKeterampilan.get"
        );
        // manipulate data
        Route::post("/store", "AnggotaKeterampilanController@store")->name(
            "anggotaKeterampilan.store"
        );
        Route::put("/{id}", "AnggotaKeterampilanController@update")->name(
            "anggotaKeterampilan.update"
        );
        Route::delete(
            "/{id}/delete",
            "AnggotaKeterampilanController@delete"
        )->name("anggotaKeterampilan.delete");

        // trash
        Route::get(
            "{id_anggota}/trash",
            "AnggotaKeterampilanController@trash"
        )->name("anggotaKeterampilan.trash");
        Route::put(
            "{id_anggota}/trash/{id_anggota_keluarga}/restore",
            "AnggotaKeterampilanController@restore"
        )->name("anggotaKeterampilan.trash.restore");
        Route::get(
            "{id_anggota}/trash/datatables",
            "AnggotaKeterampilanController@dataTablesTrash"
        )->name("anggotaKeterampilan.trash.datatables");
    });

    // anggota tafiq
    Route::group(["prefix" => "anggota/tafiq"], function () {
        Route::get("/{id_anggota}", "AnggotaTafiqController@index")->name(
            "anggotaTafiq.index"
        );
        Route::put("/{id}", "AnggotaTafiqController@update")->name(
            "anggotaTafiq.update"
        );
    });
});

// anggota Organisasi
Route::group(["prefix" => "anggota/organisasi"], function () {
    Route::get("/{id_anggota}", "AnggotaOrganisasiController@index")->name(
        "anggotaOrganisasi.index"
    );
    Route::get(
        "/datatables/{id_anggota}",
        "AnggotaOrganisasiController@dataTables"
    )->name("anggotaOrganisasi.datatables");
    Route::get("/get/{id}", "AnggotaOrganisasiController@get")->name(
        "anggotaOrganisasi.get"
    );
    // manipulate data
    Route::post("/store", "AnggotaOrganisasiController@store")->name(
        "anggotaOrganisasi.store"
    );
    Route::put("/{id}", "AnggotaOrganisasiController@update")->name(
        "anggotaOrganisasi.update"
    );
    Route::delete("/{id}/delete", "AnggotaOrganisasiController@delete")->name(
        "anggotaOrganisasi.delete"
    );

    // trash
    Route::get("{id_anggota}/trash", "AnggotaOrganisasiController@trash")->name(
        "anggotaOrganisasi.trash"
    );
    Route::put(
        "{id_anggota}/trash/{id_anggota_keluarga}/restore",
        "AnggotaOrganisasiController@restore"
    )->name("anggotaOrganisasi.trash.restore");
    Route::get(
        "{id_anggota}/trash/datatables",
        "AnggotaOrganisasiController@dataTablesTrash"
    )->name("anggotaOrganisasi.trash.datatables");
});

// anggota training
Route::group(["prefix" => "anggota/training"], function () {
    Route::get("/{id_anggota}", "AnggotaTrainingController@index")->name(
        "anggotaTraining.index"
    );
    Route::get(
        "/datatables/{id_anggota}",
        "AnggotaTrainingController@dataTables"
    )->name("anggotaTraining.datatables");
    Route::get("/get/{id}", "AnggotaTrainingController@get")->name(
        "anggotaTraining.get"
    );
    // manipulate data
    Route::post("/store", "AnggotaTrainingController@store")->name(
        "anggotaTraining.store"
    );
    Route::put("/{id}", "AnggotaTrainingController@update")->name(
        "anggotaTraining.update"
    );
    Route::delete("/{id}/delete", "AnggotaTrainingController@delete")->name(
        "anggotaTraining.delete"
    );

    // trash
    Route::get("{id_anggota}/trash", "AnggotaTrainingController@trash")->name(
        "anggotaTraining.trash"
    );
    Route::put(
        "{id_anggota}/trash/{id_anggota_keluarga}/restore",
        "AnggotaTrainingController@restore"
    )->name("anggotaTraining.trash.restore");
    Route::get(
        "{id_anggota}/trash/datatables",
        "AnggotaTrainingController@dataTablesTrash"
    )->name("anggotaTraining.trash.datatables");
});

// Otonom
Route::group(
    ["prefix" => "otonom", "middleware" => ["permission:otonom.*"]],
    function () {
        Route::get("/", "OtonomController@index")->name("otonom.index");
        Route::get("/add", "OtonomController@create")->name("otonom.create");
        Route::get("/{id}/edit", "OtonomController@edit")->name("otonom.edit");
        Route::get("/datatables", "OtonomController@dataTables")->name(
            "otonom.datatables"
        );
        // manipulate data
        Route::post("/store", "OtonomController@store")->name("otonom.store");
        Route::put("/{id}/update", "OtonomController@update")->name(
            "otonom.update"
        );
        Route::delete("/{id}/delete", "OtonomController@delete")->name(
            "otonom.delete"
        );
    }
);

Route::group(
    ["prefix" => "role-user", "middleware" => ["permission:users.*"]],
    function () {
        Route::get("/", "UserManagementController@index")->name(
            "role-user.index"
        );
        //        Route::get('/add', 'RoleUserController@create')->name('role-user.create');
        Route::get("/get/{id}", "UserManagementController@get")->name(
            "role-user.get"
        );
        Route::get("/datatables", "UserManagementController@dataTables")->name(
            "role-user.datatables"
        );
        //        // manipulate data
        //        Route::post('/store', 'RoleUserController@store')->name('role-user.store');
        Route::post("/update", "UserManagementController@update")->name(
            "role-user.update"
        );
        Route::post("/delete", "UserManagementController@delete")->name(
            "role-user.delete"
        );
    }
);

Route::group(
    ["prefix" => "jamiyyah", "middleware" => ["permission:jamiyyah.*"]],
    function () {
        Route::get("/", "JamiyyahController@index")->name("jamiyyah.index");
        Route::get("/picker", "JamiyyahController@picker")->name(
            "jamiyyah.picker"
        );
        Route::get("/daerah/{id}", "JamiyyahController@showPD")->name(
            "jamiyyah.daerah"
        );
        Route::get("/cabang/{id}", "JamiyyahController@showPC")->name(
            "jamiyyah.cabang"
        );
        Route::get("/add", "JamiyyahController@create")->name(
            "jamiyyah.create"
        );
        Route::get("/get/{id}", "JamiyyahController@get")->name("jamiyyah.get");
        Route::get(
            "/datatables/pd/{id}",
            "JamiyyahController@dataTablesPD"
        )->name("jamiyyah.datatablesPD");
        Route::get(
            "/datatables/pc/{id}",
            "JamiyyahController@dataTablesPC"
        )->name("jamiyyah.datatablesPC");
        // manipulate data
        Route::post("/store", "JamiyyahController@store")->name(
            "jamiyyah.store"
        );
        Route::put("/{id}/update", "JamiyyahController@update")->name(
            "jamiyyah.update"
        );
        Route::post("/delete", "JamiyyahController@delete")->name(
            "jamiyyah.delete"
        );
        Route::get("/datatables/pw", "JamiyyahController@dataTablesPW")->name(
            "jamiyyah.datatablesPW"
        );
    }
);

// Data PC
Route::group(
    ["prefix" => "pc"],
    function () {
        Route::get("/", "PcController@index")->name("pimpinanCabang.index");
        Route::get("/datatables", "PcController@dataTables")->name(
            "pimpinanCabang.datatables"
        );
        Route::post("/store", "PcController@store")->name(
            "pimpinanCabang.store"
        );
        Route::get("/get/{id}", "PcController@get")->name("pimpinanCabang.get");
        Route::put("update", "PcController@update")->name(
            "pimpinanCabang.update"
        );
        Route::delete("/{id}/delete", "PcController@delete")->name(
            "pimpinanCabang.delete"
        );
    }
);

// Data Bendahara
Route::group(
    ["prefix" => "bendahara"],
    function () {
        //tagihan
        Route::get("/buat-tagihan", "BendaharaController@index")->name("bendahara.index");
        Route::get("/buat-tagihan/datatables", "BendaharaController@dataTables")->name("bendahara.datatables");
        Route::get("/update-tagihan/{id}", "BendaharaController@show")->name("bendahara.show");
        Route::put("/update-tagihan/{id}", "BendaharaController@update")->name("bendahara.update");
        Route::delete("/delete-tagihan/{id}", "BendaharaController@delete")->name("bendahara.delete");
        Route::post("/store-tagihan", "BendaharaController@store")->name("bendahara.store");
        Route::get("/data", "BendaharaController@data")->name("bendahara.data");

        //transaksi-tagihan
        Route::get("/transaksi-tagihan", "TransaksiController@index")->name("transaksi.index");
        Route::post("/transaksi-tagihan/store", "TransaksiController@store")->name("transaksi.store");
        Route::post("/transaksi-tagihan/setor", "TransaksiController@setor")->name("transaksi.setor");

        //log transaksi
        Route::get("/list/log-transaksi-tagihan", "TransaksiController@list_log_transaksi")->name("transaksi.list");
        Route::get("/log-transaksi-tagihan/tarik-saldo/{id}", "TransaksiController@tarik_saldo")->name("transaksi.tarik");
        Route::get("/log-transaksi-tagihan", "TransaksiController@log_transaksi")->name("transaksi.logtrans");
        Route::get("/konfirmasi/{id}", "TransaksiController@konfirmasi")->name("transaksi.konfirmasi");
    }
);

// Data Jamiyyah/PW
Route::group(
    [
        "prefix" => "jamiyyah/pw",
        "middleware" => ["auth"],
    ],
    function () {
        Route::get("/", "PwController@index")->name("pimpinanWilayah.index");
        Route::get("/datatables", "PwController@dataTables")->name(
            "pimpinanWilayah.datatables"
        );
        Route::post("/store", "PwController@store")->name(
            "pimpinanWilayah.store"
        );
        Route::get("/get/{id}", "PwController@get")->name(
            "pimpinanWilayah.get"
        );
        Route::put("/update-data", "PwController@update")->name(
            "pimpinanWilayah.update"
        );
        Route::delete("/{id}/delete", "PwController@delete")->name(
            "pimpinanWilayah.delete"
        );
    }
);

// anggota mutasi
Route::group(
    ["prefix" => "mutasi", "middleware" => ["permission:mutasi.*", "auth"]],
    function () {
        Route::get("/", "AnggotaMutasiController@index")->name(
            "anggotaMutasi.index"
        );
        Route::get("/datatables", "AnggotaMutasiController@dataTables")->name(
            "anggotaMutasi.datatables"
        );
        Route::get("/get/{id}", "AnggotaMutasiController@get")->name(
            "anggotaMutasi.get"
        );
        // manipulate data
        Route::post("/store", "AnggotaMutasiController@store")->name(
            "anggotaMutasi.store"
        );
        Route::put("/{id}", "AnggotaMutasiController@update")->name(
            "anggotaMutasi.update"
        );
        Route::delete("/{id}/delete", "AnggotaMutasiController@delete")->name(
            "anggotaMutasi.delete"
        );
    }
);

Route::group(["prefix" => "register"], function () {
    Route::get("/", "Auth\RegisterController@index")->name("register.index");
    Route::post("/get", "Auth\RegisterController@get")->name("register.ceknpa");
    Route::get("/activation/{id}", "Auth\RegisterController@activation")->name(
        "register.activation"
    );
    // manipulate data
    Route::post("/create", "Auth\RegisterController@store")->name(
        "register.create"
    );
});

Route::group(["prefix" => "login"], function () {
    Route::get("/", "Auth\LoginController@index")->name("login.index");
    Route::post("/cek", "Auth\LoginController@cek_login")->name("login.cek");
});

Route::view("/setting/modul", "ana.modul.index");
Route::view("/setting/modul/{id}", "ana.menu.index");
Route::view("/account/role", "ana.role.index");

// Examples
Route::view("/examples/plugin-helper", "examples.plugin_helper");
Route::view("/examples/plugin-init", "examples.plugin_init");
Route::view("/examples/blank", "examples.blank");

// prototipe
//Route::view('/auth/login', 'auth.login');
Route::view("/email/activation", "email.activation");

Auth::routes();

Route::get("/provinces", "CountryController@provinces");
Route::get("/regencies", "CountryController@regencies");
Route::get("/districts", "CountryController@districts");
Route::get("/village", "CountryController@villages");

Route::group(
    ["prefix" => "laravel-filemanager", "middleware" => ["web", "auth"]],
    function () {
        Lfm::routes();
    }
);

Route::group(["prefix" => "/jamiyyah/monografi"], function () {
    Route::resource("pj", "MonografiPjController")->middleware([
        "auth",
        "permission:monografipj.*",
    ]);
    Route::resource("pc", "MonografiPcController")->middleware([
        "auth",
        "permission:monografipc.*",
    ]);
    Route::resource("pd", "MonografiPdController")->middleware([
        "auth",
        "permission:monografipd.*",
    ]);
    Route::resource("pw", "MonografiPwController")->middleware([
        "auth",
        "permission:monografipw.*",
    ]);
});

Route::group(
    ["prefix" => "/jamiyyah/performa", "as" => "performa."],
    function () {
        Route::resource("/pd", "PerformaPdController")->middleware([
            "auth",
            "permission:indexperformapd.*",
        ]);
        Route::resource("/pc", "PerformaPcController")->middleware([
            "auth",
            "permission:indexperformapc.*",
        ]);
        Route::resource("/pw", "PerformaPwController")->middleware([
            "auth",
            "permission:indexperformapw.*",
        ]);
        Route::resource("/pj", "PerformaPjController")->middleware([
            "auth",
            "permission:indexperformapj.*",
        ]);
    }
);

// Data PD
Route::group(["prefix" => "pd"], function () {
    Route::resource("pdaerah", "PDaerahController")->middleware([
        "auth"
    ]);
    Route::get("/datatables/pd/", "PDaerahController@datatables")->name(
        "pd.datatables"
    );
    Route::get("/get/{id}", "PDaerahController@get")->name("pd.get");
    // Route::put('/update', 'PDaerahController@update')->name('pd.updatedata'); //bentrok namanya dengan pd monografi
    Route::put("/update", "PDaerahController@update");
    Route::get("/get-pd-select2", "PDaerahController@getPcByName")->name("pd.select2");
    Route::delete("/{id}/delete", "PDaerahController@delete")->name(
        "pd.delete"
    );
});

//dropdown chain PW, PD, PC
Route::get("/get-pw", "PwController@getAllPwPublic");
Route::get("/get-pw-select2", "PwController@getPWByName");
Route::get("/get-related-pd/{pw}", "PwController@getRelatedPD");
Route::get("/get-related-pc/{pd}", "PDaerahController@getRelatedPC");

Route::post(
    "/reset-password-email-or-npa",
    "Auth\ForgotPasswordController@checkUserExist"
)->name("reset.password.email.npa");

Route::get(
    "/reset-password-anggota/{token}",
    "Auth\ForgotPasswordController@checkToken"
)->name("reset.password.token");

Route::post(
    "/reset-password-verification/{token}",
    "Auth\ForgotPasswordController@verifyUserAndPassword"
)->name("reset.password.verification");

Route::group(
    [
        "prefix" => "data-her-registrasi",
        "middleware" => ["permission:herregistrasi.*", "auth"],
    ],
    function () {
        Route::get("/datatables", "HerRegistrasiController@dataTables")->name(
            "herregistrasi.datatables"
        );
        Route::post(
            "/proses-approval",
            "HerRegistrasiController@prosesApproval"
        )->name("herregistrasi.proses_approval");
        Route::get("/", "HerRegistrasiController@indexTable")->name(
            "herregistrasi.index_table"
        );
        Route::post(
            "/set-status/{id}/{status}",
            "HerRegistrasiController@setStatus"
        )->name("herregistrasi.set_status");
    }
);

Route::group(
    [
        "prefix" => "data-registrasi",
        "middleware" => ["permission:rekapcaang.*", "auth"],
    ],
    function () {
        Route::get("/datatables", "RegistrasiController@dataTables")->name(
            "registrasi.datatables"
        );
        Route::post(
            "/proses-approval",
            "RegistrasiController@prosesApproval"
        )->name("registrasi.proses_approval");
        Route::get("/", "RegistrasiController@indexTable")->name(
            "registrasi.index_table"
        );
        Route::post(
            "/set-status/{id}/{status}",
            "RegistrasiController@setStatus"
        )->name("registrasi.set_status");
    }
);

Route::group(["prefix" => "caang"], function () {
    Route::get("/", "CaangAdminController@viewPublicForm");
});

Route::group(
    [
        "prefix" => "data-calon-anggota",
        "middleware" => ["permission:caang.*", "auth"],
    ],
    function () {
        Route::get("/", "CaangAdminController@index")->name("calon.caang.index");
        Route::post(
            "/mass-approve",
            "CaangAdminController@setApproveCaang"
        )->name("caang.mass_approve");
        Route::post(
            "/mass-approve-pc",
            "CaangAdminController@setApproveCaangLevelPC"
        )->name("caang.mass_approve_pc");
        Route::post(
            "/mass-approve-pd",
            "CaangAdminController@setApproveCaangLevelPD"
        )->name("caang.mass_approve_pd");
        Route::post(
            "/mass-approve-pw",
            "CaangAdminController@setApproveCaangLevelPW"
        )->name("caang.mass_approve_pw");
        Route::get("/datatables", "CaangAdminController@dataTables")->name(
            "caang.datatables"
        );
        Route::get("/{id}", "CaangAdminController@detail")->name(
            "caang.detail"
        );
        Route::delete("/{id}/delete", "CaangAdminController@delete")->name(
            "caang.delete"
        );
        Route::get("/{id}/edit", "CaangAdminController@edit")->name(
            "caang.show-edit"
        );
        Route::put("/update", "CaangAdminController@update")->name(
            "calon.caang.update"
        );
        Route::put(
            "/{id}/edit-foto",
            "CaangAdminController@updateImageProfile"
        )->name("caang.editFotoProfile");
    }
);
