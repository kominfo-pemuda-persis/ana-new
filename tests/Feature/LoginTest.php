<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function testMakeSureLoginRouteIsAccessible()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    public function testSubmitLoginWithCorrectUser()
    {
        $this->seed();
        $response = $this->call('POST', '/login/cek', ['npa' => '99.0002', 'password' => 12345]);
        $response->assertStatus(302);
        $response->assertRedirect('dashboard');
    }

    public function testSubmitLoginWithIncorrectUser()
    {
        $response = $this->call('POST', '/login/cek', ['npa' => '99.0002', 'password' => 123456]);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }
}
