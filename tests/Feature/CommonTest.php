<?php

namespace Tests\Feature;

use App\Facades\AnaOnline;
use Tests\TestCase;

class CommonTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGenerateEsyahadahNumberCorrectly()
    {
        $result1 = AnaOnline::generateNoEsyahadah(1, 10, 2022);
        $result2 = AnaOnline::generateNoEsyahadah(999999, 10, 2022);
        $result3 = AnaOnline::generateNoEsyahadah(100000, 10, 2022);
        $result4 = AnaOnline::generateNoEsyahadah(1000000, 10, 2022);
        $result5 = AnaOnline::generateNoEsyahadah(2, 10, 2022, 8);
        $expected1 = "000001/A.6.2/M/B.3-C.1/10/2022";
        $expected2 = "999999/A.6.2/M/B.3-C.1/10/2022";
        $expected3 = "100000/A.6.2/M/B.3-C.1/10/2022";
        $expected4 = "1000000/A.6.2/M/B.3-C.1/10/2022";
        $expected5 = "00000002/A.6.2/M/B.3-C.1/10/2022";
        $this->assertEquals($result1, $expected1);
        $this->assertEquals($result2, $expected2);
        $this->assertEquals($result3, $expected3);
        $this->assertEquals($result4, $expected4);
        $this->assertEquals($result5, $expected5);
    }

    public function testMakeSurePathStaticFolderS3Correct()
    {
        $static_path = env("S3_STATIC_FOLDER");
        $filename = "esyahadah.jpeg";

        $result = $static_path . $filename;
        $expected = "https://kominfo-ana-online-dev2.s3-ap-southeast-1.amazonaws.com/static/esyahadah.jpeg";

        $this->assertEquals($result, $expected);
    }

    public function testMakeSurePredikatEsyahadahTafiq1ValueCorrectlyMapped()
    {
        $predikatValueD = 30;
        $predikatExpectedD = AnaOnline::getPredikatD();
        $predikatFinalD = AnaOnline::cekPredikatFromValue($predikatValueD);

        $predikatValueC = 60;
        $predikatExpectedC = AnaOnline::getPredikatC();
        $predikatFinalC = AnaOnline::cekPredikatFromValue($predikatValueC);

        $predikatValueB = 70;
        $predikatExpectedB = AnaOnline::getPredikatC();
        $predikatFinalB = AnaOnline::cekPredikatFromValue($predikatValueB);

        $predikatValueA = 90;
        $predikatExpectedA = AnaOnline::getPredikatA();
        $predikatFinalA = AnaOnline::cekPredikatFromValue($predikatValueA);

        $this->assertEquals($predikatFinalD, $predikatExpectedD);
        $this->assertEquals($predikatFinalC, $predikatExpectedC);
        $this->assertEquals($predikatFinalB, $predikatExpectedB);
        $this->assertEquals($predikatFinalA, $predikatExpectedA);
    }
}
