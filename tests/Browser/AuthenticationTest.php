<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AuthenticationTest extends DuskTestCase
{
    use DatabaseMigrations;
//    use RefreshDatabase;
    protected $user;

    public function setUp() : void
    {
        parent::setUp();
        $this->user = factory('App\User')->create();
    }

    /** @test */
    public function testAuthentication()
    {
        $user = $this->user;
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)->visit('/home')
                ->assertSee('Dashboard')
                ->assertAuthenticatedAs($user)->logout()->assertGuest();
        });
    }
}

