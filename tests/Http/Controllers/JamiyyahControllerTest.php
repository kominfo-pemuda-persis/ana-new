<?php

namespace Tests\Http\Controllers;

use App\Http\Controllers\JamiyyahController;
use App\Model\PerformaPD;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class JamiyyahControllerTest extends TestCase
{
    use WithoutMiddleware;

    public function testShowFormPerformaPd(){
        // $user = factory(User::class)->create();
        // $this->actingAs($user);

        // $permission = Permission::create(['group' => 'categories' , 'name' => 'view categories' , 'label' => 'view categories']);

        // $role = Role::find($user->role_id);

        // $role->givePermissionTo($permission);
        $this->get(route('performa.pd.create'))->assertStatus(200);
    }

    public function testSavePerformaPd()
    {
        $performa_pd = factory(PerformaPD::class)->make();
        echo $performa_pd;
        $this->assertEquals(1,1);
    }

    public function testUpdatePerformaPd()
    {
        $this->assertTrue(true);
    }
}

