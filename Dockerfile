FROM php:7.4.5-fpm

RUN apt-get update
RUN apt-get install -y libsodium-dev git zip unzip
RUN docker-php-ext-install mysqli pdo pdo_mysql sodium exif

# install composer

RUN curl -s https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Copy existing application directory contents
RUN mkdir /app

COPY . /app

RUN chown -R www-data:www-data /app/storage
RUN chmod -R 755 /app/storage
RUN chmod -R 755 /app/public

WORKDIR /app
COPY .env.example .env

RUN composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts

# updating class cache

RUN php artisan key:generate

RUN composer dump-autoload --optimize --classmap-authoritative

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
