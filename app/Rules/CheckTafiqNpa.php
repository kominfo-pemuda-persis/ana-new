<?php

namespace App\Rules;

use App\Model\Anggota;
use Illuminate\Contracts\Validation\Rule;

class CheckTafiqNpa implements Rule
{
    private $npa;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($npa)
    {
        $this->npa = $npa;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = false;
        $anggota = Anggota::where('npa', $value)->first();
        if($anggota->status_aktif === "ACTIVE" &&
            $anggota->status_her === "SUDAH_BAYAR"){
                $result = true;
        }
        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $npa = $this->npa;
        return "Anggota dengan NPA : ${npa} tidak aktif atau belum HER";
    }
}
