<?php

namespace App\Rules;

use Barryvdh\Debugbar\Facades\Debugbar;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ReCaptcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $response = Http::get("https://www.google.com/recaptcha/api/siteverify", [
            'secret' => env('NOCAPTCHA_SECRET'),
            'response' => $value
        ]);

        $result = $response->json();
//        return $response->json()["success"];
//        dd($result);
        if ($result["score"] < 0.5) {
            return $this->message();
        }
        Log::info(json_encode($result));
        Debugbar::info($result);

        return $result["success"];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The google recaptcha is required.';
    }
}
