<?php

namespace App\Jobs;

use App\Facades\AnaOnline;
use App\Mail\Tafiq1Approved;
use App\Model\Anggota;
use App\Model\AnggotaTafiq;
use App\Model\EsyahadahTafiq1;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ApprovalTafiq1Processor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            $counter = $this->checkCounter();
            $esyahadah = $this->buildModelEsyahadah($counter);
            $link = $this->storeEsyahadahAsset($esyahadah);
            // $this->sendMessage($esyahadah, $link);
            $this->sendEmailAssets($link);
        });
    }

    private function updateDataAnggota($esyahadah){
        Anggota::where('id_anggota', $this->data->anggota->id_anggota)->update([
            "level_tafiq" => 1
        ]);

        AnggotaTafiq::insert([
            'id_anggota' => $this->data->anggota->id_anggota,
            'tanggal_masuk' => $this->data->tanggal_tafiq_start,
            'lokasi' => $this->data->lokasi,
            'id_esyahadah' => $esyahadah->id,
            'tanggal_selesai' => $this->data->tanggal_tafiq_end,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

    private function sendEmailAssets($link)
    {
        Mail::to(trim($this->data->anggota->email))->queue(
            new Tafiq1Approved(
                $this->data->anggota->nama_lengkap,
                $this->data->anggota->npa,
                $link
            ));
    }

    private function checkCounter(){
        return AnaOnline::checkNoUrutEsyahadahTafiq1();
    }

    private function buildModelEsyahadah($counter){
        $esyahadah = new EsyahadahTafiq1();
        $esyahadah->tahun = now()->year;
        $esyahadah->bulan = now()->month;
        $esyahadah->created_at = now();
        $esyahadah->updated_at = now();
        $esyahadah->id_tafiq1 = $this->data->id;
        $esyahadah->no_urut = $counter->counter;
        $esyahadah->save();
        return $esyahadah;
    }

    private function storeEsyahadahAsset($esyahadah)
    {
        $link = "esyahadah/tafiq1/{$this->data->anggota->npa}.pdf";
        Storage::disk('s3')
            ->put($link, $esyahadah->getPDF(
                $esyahadah->id, 'output'));
        return Storage::disk('s3')->url($link);
    }

    // private function sendMessage($anggota, $link)
    // {
    //     $token = env('PUSH_WA_TOKEN');
    //     $target = $anggota->no_telpon;
    //     $url = "https://dash.pushwa.com/api/kirimPesan";

    //     $message = "_Bismillahirrahmaanirrahiim_,\n\n
    //     Ahlan wa sahlan bi washiyyati rosulillah akhi {$anggota->nama_lengkap} ({$anggota->npa}).\n\n
    //     Antum telah lulus TAFIQ 1. Kami tunggu di Halaqoh Pasca TAFIQ 1, TAFIQ 2, Halaqoh Pasca TAFIQ 2 & TAFIQ 3.\n\n
    //     E-Syahadah TAFIQ 1 antum bisa diunduh/download di {$link}.\n\n
    //     Klik tombol dibawah untuk mengunduh/download E-Syahadah TAFIQ 1 antum.\n\n
    //     Admin PP. Pemuda Persis \"Ana Muslimun Qabla Kulli Syain\"\n\n
    //     Dizkro:\n
    //     - Join telegram group --> https://t.me/csanaonline jika mengalami kendala\n
    //     - Join telegram group --> https://t.me/kominfopemudapersis\n
    //     - PLAYLIST VIDEO TUTORIAL ANAONLINE --> https://www.youtube.com/playlist?list=PLyyNZDxI4KezJiXmEJ12823pQH524Fa24";


    //     $response = Http::post($url, [
    //         "token" => $token,
    //         "target" => $target,
    //         "type" => "image",
    //         "delay" => "10",
    //         "message" => $message,
    //         "url" => "https://annisa-online.s3.ap-southeast-1.amazonaws.com/static/annisa_online.jpeg"
    //     ]);

    //     return $response->json();
    // }
}
