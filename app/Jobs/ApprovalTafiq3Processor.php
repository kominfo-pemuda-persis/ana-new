<?php

namespace App\Jobs;

use App\Facades\AnaOnline;
use App\Mail\Tafiq3Approved;
use App\Model\Anggota;
use App\Model\AnggotaTafiq;
use App\Model\EsyahadahTafiq3;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ApprovalTafiq3Processor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            $counter = $this->checkCounter();
            $esyahadah = $this->buildModelEsyahadah($counter);
            $link = $this->storeEsyahadahAsset($esyahadah);
            $this->sendEmailAssets($link);
        });

    }

    private function updateDataAnggota($esyahadah){
        Anggota::where('id_anggota', $this->data->anggota->id_anggota)->update([
            "level_tafiq" => 3
        ]);

        AnggotaTafiq::insert([
            'id_anggota' => $this->data->anggota->id_anggota,
            'tanggal_masuk' => $this->data->tanggal_tafiq_start,
            'lokasi' => $this->data->lokasi,
            'id_esyahadah' => $esyahadah->id,
            'tanggal_selesai' => $this->data->tanggal_tafiq_end,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

    private function sendEmailAssets($link)
    {
        Mail::to(trim($this->data->anggota->email))->queue(
            new Tafiq3Approved(
                $this->data->anggota->nama_lengkap,
                $this->data->anggota->npa,
                $link
            ));
    }

    private function checkCounter(){
        return AnaOnline::checkNoUrutEsyahadahTafiq3();
    }

    private function buildModelEsyahadah($counter){
        $esyahadah = new EsyahadahTafiq3();
        $esyahadah->tahun = now()->year;
        $esyahadah->bulan = now()->month;
        $esyahadah->created_at = now();
        $esyahadah->updated_at = now();
        $esyahadah->id_tafiq3 = $this->data->id;
        $esyahadah->no_urut = $counter->counter;
        $esyahadah->save();
        return $esyahadah;
    }

    private function storeEsyahadahAsset($esyahadah)
    {
        $link = "esyahadah/tafiq3/{$this->data->anggota->npa}.pdf";
        Storage::disk('s3')
            ->put($link, $esyahadah->getPDF(
                $esyahadah->id, 'output'));
        return Storage::disk('s3')->url($link);
    }
}
