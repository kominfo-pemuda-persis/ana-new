<?php

namespace App\Jobs;

use Exception;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use App\Model\Anggota;
use App\Facades\AnaOnline;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ApprovalCaangProcessor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const TABLE_TEMP = "t_temp_last_npa";
    const TRESHOLD = 1987;
    const YEAR_LIMIT = 2026;
    const MONTH_LIMIT = 12;
    const DAY_LIMIT = 30;
    const YEAR_ADDITION = 41;

    // array data
    private $data;
    private $emailList;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->emailList = collect([]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->processData($this->data);
        });
    }

    private function buildModelAnggota($param) : Anggota {

        [
            "counter" => $counter,
            "key" => $key,
            "caang" => $caang,
            "id" => $id,
            "masa_aktif" => $masa_aktif
        ] = $param;

        $date = date("y");
        $format = sprintf("%04d", $counter + ($key + 1));

        $npa = "{$date}.{$format}";
        Log::channel("caang")->info("npa", [$npa]);
        $data = new Anggota();
        $data->id_anggota = $id;
        $data->npa = $npa;
        $data->nama_lengkap = $caang->nama_lengkap;
        $data->tempat_lahir = $caang->tempat_lahir;
        $data->tanggal_lahir = $caang->tanggal_lahir;
        $data->status_merital = $caang->status_merital;
        $data->provinsi = $caang->provinsi;
        $data->kota = $caang->kota;
        $data->kecamatan = $caang->kecamatan;
        $data->desa = $caang->desa;
        $data->gol_darah = $caang->gol_darah;
        $data->email = $caang->email;
        $data->no_telpon = $caang->no_telpon;
        $data->no_telpon2 = $caang->no_telpon2;
        $data->alamat = $caang->alamat;
        $data->no_telpon2 = $caang->no_telpon2;
        $data->foto = $caang->foto;
        $data->reg_date = Carbon::now();
        $data->masa_aktif_kta = $masa_aktif->toDateString();
        $data->pw = $caang->pw;
        $data->pd = $caang->pd;
        $data->pc = $caang->pc;
        $data->pj = $caang->pj;
        $data->status_her = "SUDAH_BAYAR";
        $data->status_aktif = "ACTIVE";
        $data->nama_pj = $caang->nama_pj;
        return $data;
    }


    private function processApprovalData($data, $id) : void {

        $counter = $data["last_npa"]->first()->counter;
        $data["caang"]->each(function ($caang, $key) use ($counter) {
            $masa_aktif = new Carbon();
            $id = Uuid::uuid4()->getHex();

            if ($caang->tanggal_lahir->year >= self::TRESHOLD) {
                $masa_aktif->year = self::YEAR_LIMIT;
                $masa_aktif->month = self::MONTH_LIMIT;
                $masa_aktif->day = self::DAY_LIMIT;
            } else {
                $masa_aktif = $caang->tanggal_lahir
                    ->copy()
                    ->addYear(self::YEAR_ADDITION);
            }

            $data = $this->buildModelAnggota([
                "counter" => $counter,
                "key" => $key,
                "caang" => $caang,
                "id" => $id,
                "masa_aktif" => $masa_aktif
            ]);

            $status = $data->save();

            Log::channel("custom")->info("status", [$status]);

            if ($status) {
                $this->emailList->push([
                    "npa" => $data->npa,
                    "email" => $caang->email,
                    "name" => $caang->nama_lengkap,
                    "tempat_maruf" => $caang->tempat_maruf,
                    "tanggal_maruf" => Carbon::parse($caang->tanggal_maruf_start)
                ]);

                $this->sendApprovalMessage($caang, $data);
            }
        });

        $this->updateNPALastTemp([
            "id" => $id,
            "counter" => $counter,
            "caang" => $data["caang"]
        ]);

        $res = AnaOnline::sendEmailEsyahadah("system", "", $this->emailList);
        if ($res->original['error']){
            $this->fail(new Exception($res->original['message'], $res->original['code']));
        }
    }

    private function sendApprovalMessage($dataAnggota, $dataNPA)
    {
        $token = env('PUSH_WA_TOKEN');
        $target = $dataAnggota->no_telpon;
        $url = "https://dash.pushwa.com/api/kirimPesan";
        $link = "esyahadah/maruf/" . $dataNPA->npa . ".pdf";
        $generateLink = Storage::disk('s3')->url($link);

        $message = "_Bismillahirrahmaanirrahiim_,\n\n
        Ahlan wa sahlan bi washiyyati rosulillah akhi *{$dataAnggota->nama_lengkap}*.\n\n
        Selamat bergabung di Jam'iyyah Pemuda Persatuan Islam (PERSIS). Data antum sudah ada di anaonline.id dengan NPA {$dataNPA->npa}. Untuk melihat E-KTA antum,
        silakan lakukan aktivasi di anaonline.id.\n\n E-Syahadah antum bisa diunduh/download di {$generateLink}.\n\n Admin PP. Pemuda Persis \"Ana Muslimun Qabla Kulli Syain\"\n\n Dizkro:\n
        - Join telegram group --> https://t.me/csanaonline jika mengalami kendala\n
        - Join telegram group --> https://t.me/kominfopemudapersis\n
        - PLAYLIST VIDEO TUTORIAL ANAONLINE --> https://www.youtube.com/playlist?list=PLyyNZDxI4KezJiXmEJ12823pQH524Fa24";

        $response = Http::post($url, [
            "token" => $token,
            "target" => $target,
            "type" => "image",
            "delay" => "10",
            "message" => $message,
            "url" => "https://annisa-online.s3.ap-southeast-1.amazonaws.com/static/annisa_online.jpeg"
        ]);

        return $response->json();
    }


    private function updateNPALastTemp($param)
    {
        [
            "counter" => $counter,
            "caang" => $caang,
            "id" => $id,
        ] = $param;

        Log::channel("custom")->info("updateNPALastTemp counter", [$counter]);

        $date = date("y");
        $dateFull = date("Y");
        $caangCount = $caang->count();
        $format = sprintf("%04d", $counter + $caangCount);
        $npa = "{$date}.{$format}";

        Log::channel("custom")->info("last npa", [$npa]);

        DB::table(self::TABLE_TEMP)
            ->where("id", $id)
            ->update([
                "npa" => $npa,
                "year" => $dateFull,
                "prefix" => $date,
                "suffix" => $format,
                "counter" => $counter + $caangCount
            ]);
    }

    private function processData($data)
    {
        $yearInDB = intval($data["last_npa"]->first()->year);
        $yearNow = intval(date("Y"));
        $id = $data["last_npa"]->first()->id;
        $date = date("y");
        $dateFull = date("Y");
        $format = sprintf("%04d", 0);
        $npa = "{$date}.{$format}";

        if ($yearInDB < $yearNow) {
            DB::table(self::TABLE_TEMP)
                ->where("id", $id)
                ->update([
                    "npa" => $npa,
                    "year" => $dateFull,
                    "prefix" => $date,
                    "suffix" => $format,
                    "counter" => 0,
                ]);
            $data["last_npa"] = DB::table(self::TABLE_TEMP)
                ->get()
                ->take(1);
        } else {
            $this->processApprovalData($data, $id);
        }
    }
}
