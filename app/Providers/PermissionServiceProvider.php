<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('permission', function(){
                return new \App\Helpers\Permission;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Permission directive
        Blade::directive('permission', function ($expression) {
            return "<?php if(Permission::checkPermission({$expression})): ?>";
        });
        Blade::directive('endpermission', function ($expression) {
            return "<?php endif; ?>";
        });

        // Role directive
        Blade::directive('role', function ($expression) {
            return "<?php if(Permission::checkRole({$expression})): ?>";
        });
        Blade::directive('endrole', function ($expression) {
            return "<?php endif; ?>";
        });

        // Ability directive
        Blade::directive('ability', function ($expression) {
            list($roles, $permissions) = explode(',', $expression);
            return "<?php if(Permission::checkAbility({$roles},{$permissions})): ?>";
        });
        Blade::directive('endability', function ($expression) {
            return "<?php endif; ?>";
        });
    }
}
