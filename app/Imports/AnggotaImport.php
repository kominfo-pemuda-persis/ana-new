<?php

namespace App\Imports;

use App\Model\Anggota;
use ErrorException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Ramsey\Uuid\Uuid;

class AnggotaImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable;

    public function model(array $row)
    {
        $pw = Auth::user()->anggota->pw;
        $pd = Auth::user()->anggota->pd;
        $pc = Auth::user()->anggota->pc;
        $provinsi = Auth::user()->anggota->provinsi;
        $kota = Auth::user()->anggota->kota;
        $kecamatan = Auth::user()->anggota->kecamatan;
        $desa = Auth::user()->anggota->desa;
        $id_anggota = Uuid::uuid4()->getHex();
        $foto = 'default.png';
        $reg_date = Carbon::now();

        $anggota = new Anggota();

        $anggota->id_anggota = $id_anggota;
        $anggota->npa = trim($row['npa']);
        $anggota->nama_lengkap = $row['nama_lengkap'];
        $anggota->tempat_lahir = $row['tempat_lahir'];
        $anggota->tanggal_lahir = $this->transformDate($row['tanggal_lahir']);
        $anggota->pekerjaan = $row['pekerjaan'];
        $anggota->gol_darah = $row['gol_darah'];
        $anggota->email = trim($row['email']);
        $anggota->no_telpon = $row['no_telpon'];
        $anggota->alamat = $row['alamat'];
        $anggota->pendidikan_terakhir = 1;
        $anggota->status_aktif = 'ACTIVE';
        $anggota->reg_date = $reg_date;
        $anggota->foto = $foto;
        $anggota->pw = $pw;
        $anggota->pd = $pd;
        $anggota->pc = $pc;
        $anggota->provinsi = $provinsi;
        $anggota->kota = $kota;
        $anggota->kecamatan = $kecamatan;
        $anggota->desa = $desa;
        $anggota->level_tafiq = $row['level_tafiq'];
        $anggota->save();

        return $anggota;
    }

    public function rules(): array
    {
        return [
            '*.email' => ['email', 'unique:t_anggota,email'],
            '*.npa' => ['required', 'unique:t_anggota,npa'],
            '*.tanggal_lahir' => ['required', 'date_format:Y-m-d'],
        ];
    }

    public function customValidationMessages()
{
    return [
        '*.email.email' => 'Format pada kolom Email tidak benar (perhatikan spasi dan karakter lain)',
        '*.npa.required' => 'Silahkan isi Kolom NPA',
        '*.tanggal_lahir.required' => 'Silahkan isi Kolom Tanggal Lahir',
        '*.tanggal_lahir.date_format' => 'Format tanggal lahir tidak benar. silahkan gunakan format Y-m-d. contoh 2023-11-23',
        '*.npa.unique' => 'NPA sudah terpakai. silahakan pilih NPA yang belum ada',
    ];
}

    public function transformDate($value, $format = 'Y-m-d')
    {
        return \Carbon\Carbon::createFromFormat($format, $value);
    }
}
