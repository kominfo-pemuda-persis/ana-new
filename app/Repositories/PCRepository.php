<?php

namespace App\Repositories;

use App\Model\PC;

class PCRepository extends BaseRepository
{
    public function setModel()
    {
        return new PC();
    }
}
