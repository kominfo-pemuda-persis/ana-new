<?php

namespace App\Repositories;

use App\Model\UserLogin;
use Exception;
use Illuminate\Support\Facades\DB;

class UserLoginRepository extends BaseRepository
{
    public function setModel()
    {
        return new UserLogin();
    }

    public function allWithRelation($raw = false)
    {
        if(!$raw) {
            return $this->model->with([
                'anggota.pimpinanCabang',
                'anggota.pimpinanDaerah',
                'anggota.village',
                'anggota.pimpinanWilayah'])->get();
        }else{
            return $this->model->with([
                'anggota.pimpinanCabang',
                'anggota.pimpinanDaerah',
                'anggota.village',
                'anggota.pimpinanWilayah']);
        }
    }

    public function showWithRelation($model)
    {
        return $model->load(['anggota', 'anggota.pimpinanCabang', 'anggota.pimpinanDaerah', 'anggota.pimpinanWilayah']);
    }

    public function update(array $data, object $model)
    {
        try {
            DB::transaction(function () use ($data, $model) {
                if ($data['password']) {
                    $model->update([
                        'password' => $data['password'],
                    ]);
                }

                unset($data['password']);

                $model->anggota()->update($data);
            });
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function findUserById($id)
    {
        return UserLogin::find($id);
    }

    public function find($id_login)
    {
        return UserLogin::find($id_login);
    }
}
