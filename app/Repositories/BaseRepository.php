<?php

namespace App\Repositories;

abstract class BaseRepository
{
    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Configure the Model
     *
     * @return  Illuminate\Database\Eloquent\Model
     */
    abstract protected function setModel();

    public function __construct()
    {
        $this->model = $this->setModel();
    }

    /**
     * Get all data
     *
     * @return  Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Store data
     *
     * @param   array   $data
     *
     * @return  Illuminate\Database\Eloquent\Model
     */
    public function store(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update data
     *
     * @param   array   $data
     * @param   Illuminate\Database\Eloquent\Model  $model
     *
     * @return  int
     */
    public function update(array $data, object $model)
    {
        return $model->update($data);
    }

    /**
     * Delete data
     *
     * @param   Illuminate\Database\Eloquent\Model  $model
     *
     * @return  boolean
     */
    public function destroy(object $model)
    {
        return $model->delete();
    }

    /**
     * Get all deleted data
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function allTrashed()
    {
        return $this->model
            ->onlyTrashed()
            ->get();
    }

    /**
     * Restore data
     *
     * @param   int $id
     *
     * @return  boolean
     */
    public function restore(int $id)
    {
        $primaryKey = $this->model->getKeyName();

        return $this->model::onlyTrashed()->where([$primaryKey => $id])->firstOrFail()->restore();
    }
}
