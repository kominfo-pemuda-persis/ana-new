<?php

namespace App\Repositories;

use App\Model\PW;

class PWRepository extends BaseRepository
{
    public function setModel()
    {
        return new PW();
    }
}
