<?php

namespace App\Repositories;

use App\Model\PD;

class PDRepository extends BaseRepository
{
    public function setModel()
    {
        return new PD();
    }
}
