<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BlameableObserver
{

  public function creating(Model $model){

    if(Auth::check()){
      $model->created_by = optional(Auth::user()->anggota)->email;
      $model->updated_by = optional(Auth::user()->anggota)->email; 
    } else {
      $model->created_by = "guest";
      $model->updated_by = "guest";
    }

  }

  public function updating(Model $model){

    if(Auth::check()){
      $model->updated_by = optional(Auth::user()->anggota)->email;
    } else {
      $model->updated_by = "guest";
    }

  }

}
