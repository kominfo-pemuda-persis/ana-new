<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EsyahadahMail extends Mailable
{
    use Queueable, SerializesModels;

    private $link;
    private $npa;
    private $name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($npa, $name, $link)
    {
        $this->npa = $npa;
        $this->name = $name;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('aktivasi@anaonline.id', 'Admin ANA Online')
            ->subject("Esyahadah Berhasil dibuat")
            ->attach($this->link, ['as' => 'esyahadah.pdf', 'mime' => 'application/pdf' ])
            ->view('email.esyahadah', [
                "name" => $this->name,
                "link" => $this->link,
                "npa" => $this->npa
            ]);
    }
}
