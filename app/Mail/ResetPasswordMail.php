<?php

namespace App\Mail;

use App\Model\UserLogin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $link;

    /**
     * Create a new message instance.
     *
     * @param UserLogin $user
     * @param $link
     */
    public function __construct(UserLogin $user, $link = "")
    {
        $this->user = $user;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('aktivasi@anaonline.id', 'Admin ANA Online')
            ->subject("Reset Akun ANA Online")
            ->view('email.reset_password', [
                "user" => $this->user,
                "link" => $this->link
            ]);
    }
}
