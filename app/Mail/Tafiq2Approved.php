<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Tafiq2Approved extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $npa, $link)
    {
        $this->name = $name;
        $this->npa = $npa;
        $this->link= $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('aktivasi@anaonline.id', 'Admin ANA Online')
            ->subject("Selamat Antum Lulus Tafiq 2")
            ->view('email.tafiq2_approved', [
                'name' => $this->name,
                'npa' => $this->npa,
                'link' => $this->link
            ]);
    }
}
