<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Tafiq3Approved extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $npa;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $npa, $link)
    {
        $this->name = $name;
        $this->npa = $npa;
        $this->link= $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('aktivasi@anaonline.id', 'Admin ANA Online')
            ->subject("Selamat Antum Lulus Tafiq 3")
            ->view('email.tafiq3_approved', [
                'name' => $this->name,
                'npa' => $this->npa,
                'link' => $this->link
            ]);
    }
}
