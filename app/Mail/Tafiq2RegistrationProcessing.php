<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Tafiq2RegistrationProcessing extends Mailable
{
    use Queueable, SerializesModels;

    private $npa;
    private $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($npa, $name)
    {
        $this->npa = $npa;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('aktivasi@anaonline.id', 'Admin ANA Online')
            ->subject("Registrasi Peserta Tafiq 2")
            ->view('email.tafiq2', [
                "name" => $this->name,
                "npa" => $this->npa
            ]);
    }
}
