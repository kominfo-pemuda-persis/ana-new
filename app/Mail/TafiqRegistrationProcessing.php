<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TafiqRegistrationProcessing extends Mailable
{
    use Queueable, SerializesModels;

    private $npa;
    private $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($npa, $name)
    {
        $this->npa = $npa;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('aktivasi@anaonline.id', 'Admin ANA Online')
            ->subject("Registrasi Peserta Tafiq 1")
            ->view('email.tafiq1', [
                "name" => $this->name,
                "npa" => $this->npa
            ]);
    }
}
