<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationProcessing extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('aktivasi@anaonline.id', 'Admin ANA Online')
            ->subject("Data Ajuan Calon Anggota Telah Diterima")
            ->view('email.rekap_ajuan_caang');
    }
}
