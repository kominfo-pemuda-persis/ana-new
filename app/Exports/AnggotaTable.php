<?php

namespace App\Exports;

use App\Model\Anggota;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AnggotaTable implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function view(): View
    {
        return view('ana.excel.temp-anggota', [
                'anggota' => Anggota::all()
            ]);
    }
}
