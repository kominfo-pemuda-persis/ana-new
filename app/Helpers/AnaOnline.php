<?php namespace App\Helpers;

use App\Mail\EsyahadahMail;
use App\Model\Anggota;
use App\Model\Esyahadah;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Carbon\Carbon;
use Exception;
use Faker\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class AnaOnline {

    private $STATUS_APPROVAL = [
        "INIT",
        "APPROVED_BY_PC",
        "APPROVED_BY_PD",
        "APPROVED_BY_PW",
        "APPROVED_BY_PP"
    ];

    private $PREDIKAT = [
        "Kurang (D)",
        "Cukup (C)",
        "Baik (B)",
        "Sangat Baik (A)"
    ];

    private $LEVEL_TAFIQ = [
        "TAFIQ_1",
        "TAFIQ_2",
        "TAFIQ_3"
    ];

    private $faker;

    private const TABLE_TEMP_ESYAHADAH = "t_temp_last_no_urut_esyahadah";

    public function __construct() {
        $this->faker = Factory::create('id_ID');
    }

    public function checkNoUrutEsyahadahTafiq1()
    {
        $db = DB::table('t_temp_last_no_urut_esyahadah_tafiq1');
        $data = $db->get();
        if($data->isEmpty()){
            $db->insert([
                "year" => now()->year,
                "counter" => 1
            ]);
            $data = $db->first();
        }else{
            $data = $db->first();
            $db->where('id', $data->id)->update([
                "year" => now()->year,
                "counter" => ++$data->counter
            ]);
            $data = $db->first();
        }
        return $data;
    }

    public function checkNoUrutEsyahadahTafiq2()
    {
        $db = DB::table('t_temp_last_no_urut_esyahadah_tafiq2');
        $data = $db->get();
        if($data->isEmpty()){
            $db->insert([
                "year" => now()->year,
                "counter" => 1
            ]);
            $data = $db->first();
        }else{
            $data = $db->first();
            $db->where('id', $data->id)->update([
                "year" => now()->year,
                "counter" => ++$data->counter
            ]);
            $data = $db->first();
        }
        return $data;
    }

    public function checkNoUrutEsyahadahTafiq3()
    {
        $db = DB::table('t_temp_last_no_urut_esyahadah_tafiq3');
        $data = $db->get();
        if($data->isEmpty()){
            $db->insert([
                "year" => now()->year,
                "counter" => 1
            ]);
            $data = $db->first();
        }else{
            $data = $db->first();
            $db->where('id', $data->id)->update([
                "year" => now()->year,
                "counter" => ++$data->counter
            ]);
            $data = $db->first();
        }
        return $data;
    }

    public function generateNoEsyahadah($no, $bulan, $tahun, $leading = 6)
    {
        $no_with_leading_zero = sprintf("%0{$leading}d", $no);
        return "{$no_with_leading_zero}/A.6.2/M/B.3-C.1/{$bulan}/{$tahun}";
    }

    public function cekPredikatFromValue(int $predikat)
    {
        $finalPredikat = "-";
        if($predikat <= 50) $finalPredikat = $this->PREDIKAT[0];
        if($predikat > 50 && $predikat <= 70) $finalPredikat = $this->PREDIKAT[1];
        if($predikat > 70 && $predikat <= 85) $finalPredikat = $this->PREDIKAT[2];
        if($predikat > 85) $finalPredikat = $this->PREDIKAT[3];
        return $finalPredikat;
    }

    public function getPredikatA()
    {
        return $this->PREDIKAT[3];
    }

    public function getPredikatB()
    {
        return $this->PREDIKAT[2];
    }

    public function getPredikatC()
    {
        return $this->PREDIKAT[1];
    }

    public function getPredikatD()
    {
        return $this->PREDIKAT[0];
    }

    public function generateNoEsyahadahTafiq1($no, $bulan, $tahun, $leading = 6)
    {
        $no_with_leading_zero = sprintf("%0{$leading}d", $no);
        return "{$no_with_leading_zero}/A.6.2/T1/B.3-C.1/{$bulan}/{$tahun}";
    }

    public function generateNoEsyahadahTafiq2($no, $bulan, $tahun, $leading = 6)
    {
        $no_with_leading_zero = sprintf("%0{$leading}d", $no);
        return "{$no_with_leading_zero}/A.6.2/T2/B.3-C.1/{$bulan}/{$tahun}";
    }

    public function generateNoEsyahadahTafiq3($no, $bulan, $tahun, $leading = 6)
    {
        $no_with_leading_zero = sprintf("%0{$leading}d", $no);
        return "{$no_with_leading_zero}/A.6.2/T3/B.3-C.1/{$bulan}/{$tahun}";
    }

    public function generateRandomNPA()
    {
        $npa = sprintf('%02d', $this->faker->randomNumber(2)) . '.' . sprintf('%04d', $this->faker->randomNumber(4));
        return $npa;
    }

    public function getStatusApproval()
    {
        return $this->STATUS_APPROVAL;
    }

    public function getUrlFotoAnggota($anggota)
    {
        $image = Storage::disk('s3')->url('images/anggota/'. $anggota->foto);
        return $image;
    }

    public function getStatusApprovalInit(): string
    {
        return $this->STATUS_APPROVAL[0];
    }

    public function getStatusApprovalApprovedByPC(): string
    {
        return $this->STATUS_APPROVAL[1];
    }

    public function getStatusApprovalApprovedByPD(): string
    {
        return $this->STATUS_APPROVAL[2];
    }

    public function getStatusApprovalApprovedByPW(): string
    {
        return $this->STATUS_APPROVAL[3];
    }

    public function getStatusApprovalApprovedByPP(): string
    {
        return $this->STATUS_APPROVAL[4];
    }

    public function getLevelTafiq1(): string
    {
        return $this->LEVEL_TAFIQ[0];
    }

    public function getLevelTafiq2(): string
    {
        return $this->LEVEL_TAFIQ[1];
    }

    public function getLevelTafiq3(): string
    {
        return $this->LEVEL_TAFIQ[2];
    }

    private function prepareDataCSV($param)
    {
        if($param['mode'] === "upload"){
            $reader = ReaderEntityFactory::createCSVReader();
            $reader->open($param['path']);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $index => $row) {
                    if($index > 1){
                        $param['data']['csv']->add([
                            'npa' => $row->getCellAtIndex(1)->getValue(),
                            'tempat_maruf' => $row->getCellAtIndex(2)->getValue(),
                            'tanggal_maruf' => Carbon::createFromFormat("dmY", $row->getCellAtIndex(3)->getValue())
                        ]);

                    }
                }
            }

        }elseif($param['mode'] === "system"){
            $param['data']['csv'] = $param['data']['csv']
                ->concat($param['csv']);
        }

    }

    private function checkNoUrutEsyahadah($data)
    {
        if ($data["last_no_urut"]->isEmpty()) {
            DB::table(self::TABLE_TEMP_ESYAHADAH)->insert([
                "year" => date("Y"),
                "counter" => 0,
            ]);
            $data["last_no_urut"] = DB::table(self::TABLE_TEMP_ESYAHADAH)
                ->get()
                ->take(1);
        }
    }

    public function savePDF($id, $mode = 'stream')
    {
        $esyahadah = Esyahadah::with(
            [
                'anggota',
                'anggota.pimpinanCabang',
                'anggota.pimpinanDaerah',
                'anggota.pimpinanWilayah',
                'anggota.village'
            ])->findOrFail($id);

        $data = [
           'no_urut' => AnaOnline::generateNoEsyahadah($esyahadah->no_urut, $esyahadah->bulan, $esyahadah->tahun),
           'nama' => "-",
           'tempat_tanggal_lahir' => "-",
           'pc' => "-",
           'tanggal_maruf' => "{$esyahadah->formatted_tanggal_maruf->format('d F Y') }",
           'tempat_maruf' => "{$esyahadah->tempat_maruf}",
           'foto' => "-",
           'pc_user' => null,
           'pw' => null,
           'pd' => null,
           'namaDesa' => null,
           'path_image' => env('S3_STATIC_FOLDER', "https://kominfo-ana-online-dev2.s3-ap-southeast-1.amazonaws.com/static/"),
           'filename' => "esyahadah.jpeg"
        ];

        $finalData = optional($esyahadah->anggota, function($anggota) use ($data, $esyahadah){
            $data['nama'] = $anggota->nama_lengkap;
            $data['tempat_tanggal_lahir'] = "{$anggota->tempat_lahir}, {$esyahadah->formatted_tanggal_lahir}";

            $data['pc'] = optional($anggota->pimpinanCabang, function($pc){
                return $pc->nama_pc;
            });

            $data['pw'] = optional($anggota->pimpinanWilayah, function($pw){
                return $pw->nama_pw;
            });

            $data['pd'] = optional($anggota->pimpinanDaerah, function($pd){
                return $pd->nama_pd;
            });

            $data['namaDesa'] = optional($anggota->village, function($desa){
                return $desa->nama;
            });

            if($anggota->foto === "default.png" ){
                $data['foto'] = "https://dev.anaonline.id/images/anggota/default.png";
            }else if (is_null($anggota->nama_pj)) {
                $data['foto'] = Storage::disk('s3')->url('images/anggota/' . $data['pw'] . '/' . $data['pd'] . '/' . $data['pc'] . '/' . $data['namaDesa'] . '/' . $anggota->foto);
            } else if(!is_null($anggota->nama_pj)) {
                $data['foto'] = Storage::disk('s3')->url('images/anggota/' . $data['pw'] . '/' . $data['pd'] . '/' . $data['pc'] . '/' . $anggota->nama_pj . '/' . $anggota->foto);
            }

            return $data;

        });

        $data = $finalData ?? $data;
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadView('layouts.esyahadah', $data)->setPaper('a4', 'landscape');

        if($mode === "stream") return $pdf->stream();
        if($mode === "output") return $pdf->output();
        return "blank";

    }

    private function processEsyahadah($data): bool
    {
        return DB::transaction(function () use (&$data) {
            $last_no_urut = $data["last_no_urut"]->first();
            $data['counter'] = intval($last_no_urut->counter);
            Log::channel("custom")->info("trans_counter", [$data['counter']]);
            Log::channel("csv")->info("trans_csv", [$data['csv']]);
            $data['csv']->each(function ($value) use(&$data){
                $data['anggota'] = Anggota::where('npa', $value['npa'])->first();
                $esyahadah = new Esyahadah();
                optional($data['anggota'], function ($item) use (&$esyahadah, $value, &$data){
                    $data['counter'] = $data['counter'] + 1;
                    $data['id_anggota'] = $item->id_anggota;
                    $esyahadah->id_anggota = $item->id_anggota;
                    $esyahadah->tahun = $value['tanggal_maruf']->year;
                    $esyahadah->bulan = $value['tanggal_maruf']->month;
                    $esyahadah->no_urut = $data['counter'];
                    $esyahadah->tempat_maruf = $value['tempat_maruf'];
                    $esyahadah->tanggal_maruf = $value['tanggal_maruf'];
                    $esyahadah->created_at = Carbon::now();
                    $esyahadah->updated_at = Carbon::now();
                    $esyahadah->save();
                    $link = "esyahadah/maruf/" . $item->npa . ".pdf";
                    Storage::disk('s3')->put($link, $this->savePDF($esyahadah->id, 'output'));
                    $data['mail']->add([
                        'npa' => $item->npa,
                        'name' => $item->nama_lengkap,
                        'email' => $item->email,
                        'link' => Storage::disk('s3')->url($link)
                    ]);
                });
            });

            Log::channel("custom")->info("data_irwan", $data);

            if(is_null($data['id_anggota'])) throw new Exception(
                "NPA yang dapat di-generate hanya NPA yang berawal 21.xxxx & 22.xxxx", 400);

            DB::table(self::TABLE_TEMP_ESYAHADAH)
                ->where("id", $last_no_urut->id)
                ->update([
                    "counter" => $data['counter'],
                    "year" => date('Y'),
                    "id_anggota" => $data['id_anggota']
            ]);

            $data['mail']->each(function ($value)
            {
                Mail::to(trim($value['email']))->queue(
                    new EsyahadahMail(
                        $value['npa'],
                        $value['name'],
                        $value['link']));
            });

            return true;

        });
    }

    public function sendEmailEsyahadah($mode = 'upload', $path = "", $csv = []){
        try{
            $data = [];
            $data["last_no_urut"] = DB::table(self::TABLE_TEMP_ESYAHADAH)
                ->get()->take(1);
            $data['csv'] = collect([]);
            $data['mail'] = collect([]);
            $data['counter'] = 0;
            $data['id_anggota'] = null;

            $this->prepareDataCSV([
                "mode" => $mode,
                "path" => $path,
                "csv" => $csv,
                "data" => &$data
            ]);

            Log::channel("custom")->info("data csv", [$data['csv']]);

            $this->checkNoUrutEsyahadah($data);
            $status = $this->processEsyahadah($data);

            if ($status) {
                return response()->json(
                    [
                        "message" => "success",
                        "error" => false,
                        "code" => ResponseAlias::HTTP_ACCEPTED,
                    ],
                    ResponseAlias::HTTP_ACCEPTED
                );
            } else {
                throw new Exception("Error Generating Esyahadah Data");
            }

        } catch (Exception $e) {
            return response()->json(
                [
                    "message" => $e->getMessage(),
                    "error" => true,
                    "code" => ResponseAlias::HTTP_INTERNAL_SERVER_ERROR,
                ],
                ResponseAlias::HTTP_INTERNAL_SERVER_ERROR
            );
        }


    }

}
