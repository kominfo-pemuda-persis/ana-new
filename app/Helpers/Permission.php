<?php namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class Permission {

	private $userId;
	private $userRoles;
	private $userPermissions;

	public function __construct()
	{
		$this->userId = Auth::user()->id_login;
		$this->userRoles = \App\Model\RoleUser::where('user_id', $this->userId)->get();

		$this->userPermissions = [];
		foreach ($this->userRoles as $role) {
			$permissions = \App\Model\PermissionRole::where('role_id', $role->role_id)->get();
			foreach ($permissions as $perm) {
				$this->userPermissions[$perm->module][] = $perm->permission;
			}
		}
	}

	public function getUserRoles()
    {
    	$data = [];
    	foreach ($this->userRoles as $role) {
    		$data[$role->role_id] = $role->role;
    	}
        return $data;
    }

    public function getUserPermissions()
    {
        return $this->userPermissions;
    }

    /**
     *  Check if current user has access to the permission
     *
     * string $permissionString		i.e. modulename.permissionname
     * string $permissionString		i.e. modulename.*
     * return boolean
     */
    public function checkPermission(...$permissionString)
    {
    	// Allow superadmin
    	if($this->checkRole('superadmin')) return true;

        // check for multiple permission input
        if(count($permissionString) > 1)
            foreach ($permissionString as $perm) {
                $result = $this->checkPermission($perm);
                if($result === true) return true;
            }

    	// Parse permissionString
    	list($module, $permission) = explode('.', $permissionString[0]);

    	// Get allowed module
    	$allowedModule = $this->userPermissions[$module] ?? null;

    	// Prevent access if the module is not available in userPermissions
    	if (! $allowedModule) return false;

    	// Allow access if there is any of the module permission
    	if ($permission == '*') return true;

    	// Allow access only if user has spesific module permission
    	if (in_array($permission, $allowedModule)) return true;

    	return false;
    }

    /**
     *  Check if current user is in the role
     *
     * string $rolename
     * return boolean
     */
    public function checkRole(...$rolename)
    {
    	foreach ($this->getUserRoles() as $id => $role) {
    		if(in_array($role->name, $rolename)) return true;
    	}

    	return false;
    }

    /**
     *  Check if current user is in the role or has the permission
     *
     * string $rolename
     * string $permissionString
     * return boolean
     */
    public function checkAbility($rolename, $permissionString)
    {
        $rolename = explode('|', $rolename);
        $permissionString = explode('|', $permissionString);
     
    	if($this->checkRole(...$rolename)) return true;
        if($this->checkPermission(...$permissionString)) return true;
    	return false;
    }
}