<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_lengkap' => ['required'],
            'pw' => ['required'],
            'pd' => ['required'],
            'pc' => ['required'],
            'desa' => ['required'],
            'pj' => ['required'],
            'alamat' => ['required'],
            'email' => ['required'],
            'no_telpon' => ['required'],
            'password' => ['sometimes', 'nullable'],
        ];
    }
}
