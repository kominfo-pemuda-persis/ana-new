<?php

namespace App\Http\Controllers;

// use Auth;
use DataTables;
use Storage;
use DB;
use Illuminate\Http\Request;
use App\Model\MasterOtonom;

class OtonomController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ana.otonom.index');        
    }

    public function datatables(){
        $otonom = DB::table('t_master_otonom')
            ->select('id_otonom','nama_otonom')
            ->orderBy('id_otonom', 'asc')
            ->get();
        return Datatables::of($otonom)
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $destroyOtonom = Otonom::where('id_otonom', $id)->first();
        $destroyOtonom->delete();
        if ($destroyOtonom) {
            return response()->json(['message' => 'Success deleting data', 'data' => $destroyOtonom]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }
    }
}
