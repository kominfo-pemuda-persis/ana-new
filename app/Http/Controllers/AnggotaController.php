<?php

namespace App\Http\Controllers;

// use Auth;
use App\Exports\AnggotaTable;
use App\Facades\Permission;
use App\Imports\AnggotaImport;
use App\Model\Anggota;
use App\Model\AnggotaKeluarga;
use App\Model\AnggotaOrganisasi;
use App\Model\AnggotaPendidikan;
use App\Model\AnggotaTafiq;
use App\Model\JamiyyahMaster;
use App\Model\MasterPendidikan;
use App\Model\PD;
use App\Model\Province;
use App\Model\PW;
use App\Model\UserLogin;
use DataTables;
use Exception;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Ramsey\Uuid\Uuid;
use Throwable;

class AnggotaController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $listPendidikan = MasterPendidikan::all();
        $data['listPendidikan'] = $listPendidikan;
        return view('ana.anggota.index', $data);
    }

    private function getAuthAnggota()
    {
        $currentNPA = Auth::user()->npa;
        return Anggota::where('npa', $currentNPA)->first();
    }


    public function indexPC(Request $request)
    {
        $id = $request->id;
        $pc = JamiyyahMaster::where('id_wilayah', $id)->first();
        if ($pc->pimpinan == 'PC') {
            $nama_pc = "Pimpinan Cabang " . $pc->nama_pimpinan;
        }

        $listPendidikan = MasterPendidikan::all();
        $data['listPendidikan'] = $listPendidikan;

        $data['nama_pc'] = $nama_pc;
        $data['id_pc'] = $id;
        return view('ana.anggota.pc.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $dataPW = PW::all();
        $provinces = Province::all();
        $listPendidikan = MasterPendidikan::all();
        $isKtaFormShown = Permission::checkRole("superadmin") || Permission::checkRole("admin_pp");
        return view('ana.anggota.add', compact(
            'dataPW', 'listPendidikan', 'provinces', 'isKtaFormShown'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $data_array = $request->all();
        $request->validate([
            'npa' => 'required|max:10|unique:t_anggota,npa,NULL,id,deleted_at,NULL',
            'nama_lengkap' => 'required|max:50',
            'tempat_lahir' => 'required|max:50',
            'tanggal_lahir' => 'required',
            'pekerjaan' => 'required|max:25',
            'pj' => 'nullable|max:50',
            'gol_darah' => 'nullable|max:3',
            'email' => 'required|max:50|unique:t_anggota,email,NULL,id,deleted_at,NULL',
            'no_telpon' => 'required|max:15',
            'no_telpon2' => 'nullable|max:15',
            'alamat' => 'required|max:100',
            "foto-upload" => "required|mimes:jpeg,png,jpg|max:2048",
        ]);

        DB::beginTransaction();
        try {

            $data = new Anggota();
            $data->id_anggota = Uuid::uuid4()->getHex();
            $data->npa = $request->Input('npa');
            $data->nama_lengkap = $request->Input('nama_lengkap');
            $data->tempat_lahir = $request->Input('tempat_lahir');
            $data->tanggal_lahir = $request->Input('tanggal_lahir');
            $data->status_merital = $request->Input('status_merital');
            $data->pw = $request->Input('pw');
            $data->pd = $request->Input('pd');
            $data->pc = $request->Input('pc');
            $data->pj = $request->Input('pj');
            $data->nama_pj = $request->Input('namaPJ');
            $data->provinsi = $request->Input('provinsi');
            $data->kota = $request->Input('kota');
            $data->kecamatan = $request->Input('kecamatan');
            $data->desa = $request->Input('desa');
            $data->pekerjaan = $request->Input('pekerjaan');
            $data->gol_darah = $request->Input('gol_darah');
            $data->email = trim($request->Input('email'));
            $data->no_telpon = $request->Input('no_telpon');
            $data->no_telpon2 = $request->Input('no_telpon2');
            $data->alamat = $request->Input('alamat');
            $data->jenis_keanggotaan = $request->Input('jenis_keanggotaan');
            $data->status_aktif = 'ACTIVE';
            $data->masa_aktif_kta = $request->Input('masa_aktif_kta');
            $data->reg_date = $request->Input('reg_date');
            $data->pendidikan_terakhir = $request->Input('pendidikan_terakhir');
            $data->level_tafiq = $request->Input('level_tafiq');

            $dir = 'images/anggota/';

            if ($request->foto) {
                $file_base64 = $request->foto;
                $filename = $request->Input('nama_lengkap') . '_' . date('dmY_His') . '.' . 'png';
                $this->uploadImageFromBase64toAWS($file_base64, $dir, $filename);
                $data->foto = $filename;
            } else {
                $data->foto = "default.png";
            }

            $colKeluarga = array();
            $colPendidikan = array();
            $colOrganisasi = array();
            $colTafiq = array();

            foreach ($data_array as $key => $value) {
                if (strpos($key, 'keluarga_') !== false) {
                    $colKeluarga[substr($key, strlen('keluarga_'))] = $value;
                }
                if (strpos($key, 'pendidikan_') !== false) {
                    $colPendidikan[substr($key, strlen('pendidikan_'))] = $value;
                }
                if (strpos($key, 'organisasi_') !== false) {
                    $colOrganisasi[substr($key, strlen('organisasi_'))] = $value;
                }
                if (strpos($key, 'tafiq_') !== false) {
                    $colTafiq[substr($key, strlen('tafiq_'))] = $value;
                }
            }

            $arrKeluarga = [];
            foreach ($colKeluarga as $key => $val) {
                $arr = [];
                if (count($arrKeluarga) == 0) {
                    foreach ($val as $k => $v) {
//                    $arrKeluarga[$k] = ['id_anggota'=> $anggota->id_anggota];
                        $arrKeluarga[$k] = [];
                    }
                }
                foreach ($val as $k => $v) {
                    $arr[$key] = $v;
                    $arrKeluarga[$k] = Arr::add($arrKeluarga[$k], $key, $v);
                }
            }

            $arrPendidikan = [];
            foreach ($colPendidikan as $key => $val) {
                $arr = [];
                if (is_array($val)) {
                    if (count($arrPendidikan) == 0) {
                        foreach ($val as $k => $v) {
//                    $arrPendidikan[$k] = ['id_anggota'=> $anggota->id_anggota];
                            $arrPendidikan[$k] = [];
                        }
                    }
                    foreach ($val as $k => $v) {
                        $arr[$key] = $v;
                        $arrPendidikan[$k] = Arr::add($arrPendidikan[$k], $key, $v);
                    }
                }
            }

            $arrOrganisasi = [];
            foreach ($colOrganisasi as $key => $val) {
                $arr = [];
                if (count($arrOrganisasi) == 0) {
                    foreach ($val as $k => $v) {
//                    $arrOrganisasi[$k] = ['id_anggota'=> $anggota->id_anggota];
                        $arrOrganisasi[$k] = [];
                    }
                }
                foreach ($val as $k => $v) {
                    $arr[$key] = $v;
                    $arrOrganisasi[$k] = Arr::add($arrOrganisasi[$k], $key, $v);
                }
            }

            $arrTafiq = [];
            foreach ($colTafiq as $key => $val) {
                $arr = [];
                if (count($arrTafiq) == 0) {
                    foreach ($val as $k => $v) {
//                    $arrOrganisasi[$k] = ['id_anggota'=> $anggota->id_anggota];
                        $arrTafiq[$k] = [];
                    }
                }
                foreach ($val as $k => $v) {
                    $arr[$key] = $v;
                    $arrTafiq[$k] = Arr::add($arrTafiq[$k], $key, $v);
                }
            }


            $data->save();
            $data->keluarga()->createMany($arrKeluarga);
            $data->pendidikan()->createMany($arrPendidikan);
            $data->organisasi()->createMany($arrOrganisasi);
            $data->tafiq()->createMany($arrTafiq);


            DB::commit();
            return response()->json(['message' => 'Success adding data', 'data' => 1]);
        } catch (Throwable $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('ana.anggota.detail');
    }


    public function picker(Request $request)
    {
        $anggota = Anggota::with(['pimpinanCabang' => function($query){ $query->select('kd_pc', 'nama_pc');}])
                ->select('id_anggota', 'npa', 'nama_lengkap', 'pc', 'email');

        if ($request->has('q')) {
            $q = $request->get('q');
            $anggota->where(function($query) use($q) {
                $query->where('npa','like', '%' . $q . '%')
                    ->orWhere('nama_lengkap', 'like', '%' . $q . '%');
            });
        }

        if ($request->has('level_tafiq')) {
            $level_tafiq = $request->get('level_tafiq');
            $anggota->where('level_tafiq', '=', $level_tafiq);
            $anggota->where('status_aktif', '=', "ACTIVE");
            $anggota->where('status_her', '=', "SUDAH_BAYAR");
        }

        $anggota = $anggota->take(20)->get();

        if (!$anggota) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json($anggota);
    }

    public function datatables(Request $request)
    {
        $start = $request->get('start') ?? 0;
        $length = $request->get('length') ?? 10;
        // $draw = $request->get('draw') ?? 1;
        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        // $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        // $searchValue = $search_arr['value']; // Search value

        // Get current authenticated anggota to get pw, pd, and pc id
        $authAnggota = $this->getAuthAnggota();

        if (Permission::checkPermission('anggota.read_pw') || Permission::checkPermission('anggota.read_pd') || Permission::checkPermission('anggota.read_pc') || Permission::checkPermission('anggota.read_all')) {
            $anggota = Anggota::
            select('id_anggota', 'nama_lengkap', 'npa', 'status_Aktif', 'pw', 'pd', 'pc', 'nama_pj', 'tempat_lahir', 'tanggal_lahir', 'pekerjaan', 'foto', 't_pw.nama_pw', 't_pd.nama_pd', 't_pc.nama_pc', 'desa', 't_desa.nama')->whereIn('status_aktif', ['ACTIVE', 'NON ACTIVE', 'TIDAK HEREGISTRASI', 'MUTASI', 'MENINGGAL'])
                ->join('t_pw', 't_anggota.pw', '=', 't_pw.kd_pw')
                ->join('t_pd', 't_anggota.pd', '=', 't_pd.kd_pd')
                ->join('t_pc', 't_anggota.pc', '=', 't_pc.kd_pc')
                ->join('t_desa', 't_anggota.desa', '=', 't_desa.id');
        } else {
            $anggota = Anggota::select('id_anggota', 'nama_lengkap', 'npa', 'status_Aktif', 'pw', 'pd', 'pc');
        }

        //filternya di sini
        if (Permission::checkRole('admin_pj') || Permission::checkRole('tasykil_pj')) {
            $anggota->where('pc', $authAnggota->pc)->where('pj', $authAnggota->pj);
        } else if (Permission::checkRole('admin_pc') || Permission::checkRole('tasykil_pc')) {
            $anggota->where('pc', $authAnggota->pc);
        } else if (Permission::checkRole('admin_pd') || Permission::checkRole('tasykil_pd')) {

            $pd = PD::where('kd_pd', $authAnggota->pd)->with("relatedPC")->get();

            $listPC = $pd->map(function ($value) {
                return $value->relatedPC->map(function ($value2) {
                    return $value2->kd_pc;
                });
            })->collapse();

            $anggota->whereIn('pc', $listPC);
        } else if (Permission::checkRole('admin_pw') || Permission::checkRole('tasykil_pw')) {

            $relatedPD = PW::where('kd_pw', $authAnggota->pw)
                ->first()->relatedPD->pluck('kd_pd');

            $pd = PD::whereIn('kd_pd', $relatedPD)->with("relatedPC")->get();

            $listPC = $pd->map(function ($value) {
                return $value->relatedPC->map(function ($value2) {
                    return $value2->kd_pc;
                });
            })->collapse();

            $anggota->whereIn('pc', $listPC);
        }

        // Added advance serarch
        // Filter by gol_darah ('A', 'B', 'AB', 'O')
        if ($request->get('gol_darah')) {
            $anggota->where('gol_darah', $request->get('gol_darah') );
        }

        // Filter by jenis_keanggotaan (BIASA', 'TERSIAR')
        if ($request->get('jenis_keanggotaan')) {
            $anggota->where('jenis_keanggotaan', $request->get('jenis_keanggotaan'));
        }

        // Filter by status_aktif ('ACTIVE', 'NON ACTIVE', 'MENINGGAL', 'TIDAK HER-REGISTRASI', 'MUTASI')
        if ($request->get('status_aktif')) {
            $anggota->where('status_aktif', $request->get('status_aktif'));
        }

        // Filter by pendidikan_terakhir (KOSONG, SD, MI, SMP, MTs, SMA, SMK, STM, MA, MLN, D1, D2, D3, D4, S1, S2, S3)
        if ($request->get('pendidikan_terakhir')) {
            $anggota->where('pendidikan_terakhir', $request->get('pendidikan_terakhir'));
        }

        // Filter by level_tafiq ('SUDAH','BELUM',1, 2, 3)
        if ($request->get('level_tafiq')) {
            $anggota->where('level_tafiq', $request->get('level_tafiq'));
        }

        // status Marital
        if ($request->get('status_merital')) {
            $anggota->where('status_merital', $request->get('status_merital') );
        }

        // status Her
        if ($request->get('status_her')) {
            $anggota->where('status_her', $request->get('status_her') );
        }

        // $anggota->skip($start)->take($length);
        $anggota->orderBy($columnName, $columnSortOrder);

        if (Permission::checkPermission('anggota.read_pw') || Permission::checkPermission('anggota.read_pd') || Permission::checkPermission('anggota.read_pc') || Permission::checkPermission('anggota.read_all')) {
            return Datatables::of($anggota)
                // ->addIndexColumn()
                ->addColumn('linkFoto', function (Anggota $anggota) {
                    $image = Storage::disk('s3')->url('images/anggota/'. $anggota->foto);
                    return $image;
                })
                ->make(true);
        }
        return Datatables::of($anggota)
            // ->addIndexColumn()
            ->make(true);
    }

    public function datatablesPC(Request $request)
    {
        $id = $request->id;
        $anggota = DB::table('t_anggota')
            ->select('id_anggota', 'nama_lengkap', 'npa', 'status_Aktif')
            ->where('pc', $id)
            ->orderBy('id_anggota', 'asc')
            ->get();
        return Datatables::of($anggota)
            ->addIndexColumn()
            ->make(true);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $id = $request->id;

        $data['anggota'] = Anggota::with('keluarga', 'pendidikan', 'organisasi', 'tafiq', 'pimpinanCabang', 'pimpinanDaerah', 'pimpinanWilayah', 'village')
            ->find($id);
        $listPendidikan = MasterPendidikan::all();

        $provinces = Province::all();
        $dataPW = PW::all();
        $data['isKtaFormShown'] = Permission::checkRole("superadmin") || Permission::checkRole("admin_pp");

        $authAnggota = $this->getAuthAnggota();
        if (Permission::checkPermission('anggota.update_all'))
            return view('ana.anggota.edit', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if (Permission::checkPermission('anggota.update_pw') && $authAnggota->pw == $data['anggota']->pw)
            return view('ana.anggota.edit', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if (Permission::checkPermission('anggota.update_pd') && $authAnggota->pd == $data['anggota']->pd)
            return view('ana.anggota.edit', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if (Permission::checkPermission('anggota.update_pc') && $authAnggota->pc == $data['anggota']->pc)
            return view('ana.anggota.edit', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if (Permission::checkPermission('anggota.update_pj') && $authAnggota->pj == $data['anggota']->pj)
            return view('ana.anggota.edit', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if ($id == $authAnggota->id_anggota)
            return view('ana.anggota.edit', $data, compact('listPendidikan', 'provinces', 'dataPW'));

        abort(403, 'Unauthorized to edit data');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function editProfile(Request $request)
    {
        $id = $request->id;

        $data['anggota'] = Anggota::with('keluarga', 'pendidikan', 'organisasi', 'tafiq', 'pimpinanCabang', 'pimpinanDaerah', 'pimpinanWilayah', 'village')
            ->find($id);
        $listPendidikan = MasterPendidikan::all();

        $provinces = Province::all();
        $dataPW = PW::all();
        $data['isKtaFormShown'] = Permission::checkRole("superadmin") || Permission::checkRole("admin_pp");

        $authAnggota = $this->getAuthAnggota();

        if (Permission::checkPermission('anggota.update_all'))
            return view('ana.anggota.edit-profile', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if (Permission::checkPermission('anggota.update_pw') && $authAnggota->pw == $data['anggota']->pw)
            return view('ana.anggota.edit-profile', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if (Permission::checkPermission('anggota.update_pd') && $authAnggota->pd == $data['anggota']->pd)
            return view('ana.anggota.edit-profile', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if (Permission::checkPermission('anggota.update_pc') && $authAnggota->pc == $data['anggota']->pc)
            return view('ana.anggota.edit-profile', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if (Permission::checkPermission('anggota.update_pj') && $authAnggota->pj == $data['anggota']->pj)
            return view('ana.anggota.edit-profile', $data, compact('listPendidikan', 'provinces', 'dataPW'));
        else if ($id == $authAnggota->id_anggota)
            return view('ana.anggota.edit-profile', $data, compact('listPendidikan', 'provinces', 'dataPW'));

        abort(403, 'Unauthorized to edit data');
    }
    /**
     * Profile the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function profile(Request $request)
    {
        $id = $request->id;
        $data['anggota'] = Anggota::with('pimpinanCabang', 'pimpinanDaerah', 'pimpinanWilayah', 'keluarga', 'keterampilan', 'mutasi', 'organisasi', 'pekerjaan', 'pendidikan', 'training', 'village')
            ->where('id_anggota', $id)->first();
        if (!$data['anggota']) abort(404);
        $roles = UserLogin::where('npa', $data['anggota']->npa)->first();
        if (is_null($roles)) {
            $data['roles'] = $roles;
        } else {
            $data['roles'] = $roles->roles;
        }
        return view('ana.anggota.profile', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $data = $request->all();
        $request->validate([
            'npa' => 'required|max:10',
            'nama_lengkap' => 'required|max:50',
            'tempat_lahir' => 'required|max:50',
            'tanggal_lahir' => 'required',
            'pekerjaan' => 'required|max:25',
            'pj' => 'nullable|max:50',
            'gol_darah' => 'nullable|max:3',
            'email' => 'required|max:50',
            'no_telpon' => 'required|max:15',
            'no_telpon2' => 'nullable|max:15',
            'alamat' => 'required|max:100',
            "foto-upload" => "nullable|mimes:jpeg,png,jpg|max:2048",
        ]);

        $anggota = Anggota::where('id_anggota', $id)
            ->first();

        if($anggota->foto) {
            $anggotaCaangProfileBefore = "anggota_".$anggota->foto;
            Storage::disk('s3')->delete('images/anggota/' .$anggotaCaangProfileBefore);

            $anggotaUpdate = $anggota->foto;
            Storage::disk('s3')->delete('images/anggota/' .$anggotaUpdate);
        }
        if (!$anggota) {
            return response()->json(['message' => 'Not found data'], 404);
        }

        $namaPJ = $request->Input('namaPJ');
        $data['nama_pj'] = $namaPJ;

        $dir = 'images/anggota/';

        if ($request->foto) {
            $file_base64 = $request->foto;
            $filename = "anggota_update_".$request->Input('nama_lengkap') . '_' . date('dmY_His') . '.' . 'png';
            $this->uploadImageFromBase64toAWS($file_base64, $dir, $filename);
            $data['foto'] = $filename;
        }
        else {
            $data['foto'] = $anggota->foto;
        }

        $colKeluarga = array();
        $colPendidikan = array();
        $colOrganisasi = array();
        $colTafiq = array();

        foreach ($data as $key => $value) {
            if (strpos($key, 'keluarga_') !== false) {
                $colKeluarga[substr($key, strlen('keluarga_'))] = $value;
            }
            if (strpos($key, 'pendidikan_') !== false) {
                $colPendidikan[substr($key, strlen('pendidikan_'))] = $value;
            }
            if (strpos($key, 'organisasi_') !== false) {
                $colOrganisasi[substr($key, strlen('organisasi_'))] = $value;
            }
            if (strpos($key, 'tafiq_') !== false) {
                $colTafiq[substr($key, strlen('tafiq_'))] = $value;
            }
        }

        $arrKeluarga = [];
        foreach ($colKeluarga as $key => $val) {
            $arr = [];
            if (count($arrKeluarga) == 0) {
                foreach ($val as $k => $v) {
//                    $arrKeluarga[$k] = ['id_anggota'=> $anggota->id_anggota];
                    $arrKeluarga[$k] = [];
                }
            }
            foreach ($val as $k => $v) {
                $arr[$key] = $v;
                $arrKeluarga[$k] = Arr::add($arrKeluarga[$k], $key, $v);
            }
        }

        $arrPendidikan = [];
        foreach ($colPendidikan as $key => $val) {
            $arr = [];
            if (is_array($val)) {
                if (count($arrPendidikan) == 0) {
                    foreach ($val as $k => $v) {
//                    $arrPendidikan[$k] = ['id_anggota'=> $anggota->id_anggota];
                        $arrPendidikan[$k] = [];
                    }
                }
                foreach ($val as $k => $v) {
                    $arr[$key] = $v;
                    $arrPendidikan[$k] = Arr::add($arrPendidikan[$k], $key, $v);
                }
            }
        }

        $arrOrganisasi = [];
        foreach ($colOrganisasi as $key => $val) {
            $arr = [];
            if (count($arrOrganisasi) == 0) {
                foreach ($val as $k => $v) {
//                    $arrOrganisasi[$k] = ['id_anggota'=> $anggota->id_anggota];
                    $arrOrganisasi[$k] = [];
                }
            }
            foreach ($val as $k => $v) {
                $arr[$key] = $v;
                $arrOrganisasi[$k] = Arr::add($arrOrganisasi[$k], $key, $v);
            }
        }

        $arrTafiq = [];
        foreach ($colTafiq as $key => $val) {
            $arr = [];
            if (count($arrTafiq) == 0) {
                foreach ($val as $k => $v) {
//                    $arrOrganisasi[$k] = ['id_anggota'=> $anggota->id_anggota];
                    $arrTafiq[$k] = [];
                }
            }
            foreach ($val as $k => $v) {
                $arr[$key] = $v;
                $arrTafiq[$k] = Arr::add($arrTafiq[$k], $key, $v);
            }
        }

        DB::beginTransaction();
        try {

            $anggota->update($data);
            $anggota->keluarga()->delete();
            $anggota->keluarga()->createMany($arrKeluarga);
            $anggota->pendidikan()->delete();
            $anggota->pendidikan()->createMany($arrPendidikan);
            $anggota->organisasi()->delete();
            $anggota->organisasi()->createMany($arrOrganisasi);
            $anggota->tafiq()->delete();
            $anggota->tafiq()->createMany($arrTafiq);


            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['message' => 'Success updating data', 'data' => 1]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function delete(Request $request)
    {
        if (!Permission::checkPermission('anggota.delete'))
            return response()->json(['message' => 'Unauthorized to delete data'], 403);

        $id = $request->id;
        $destroyAnggota = Anggota::where('id_anggota', $id)->first();
        $destroyAnggota->delete();
        if ($destroyAnggota) {
            return response()->json(['message' => 'Success deleting data', 'data' => $destroyAnggota]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }
    }

    public function dataTablesTrashAnggota()
    {
        $authAnggota = Auth::user()->anggota;
        $anggota = Anggota::with("pimpinanCabang")
            ->onlyTrashed()
            ->orderBy('id_anggota', 'asc');

        if (Permission::checkRole('admin_pj') || Permission::checkRole('tasykil_pj')) {
            $anggota->where('pj', $authAnggota->pj);
        } else if (Permission::checkRole('admin_pc') || Permission::checkRole('tasykil_pc')) {
            $anggota->where('pc', $authAnggota->pc);
        } else if (Permission::checkRole('admin_pd') || Permission::checkRole('tasykil_pd')) {

            $pd = PD::where('kd_pd', $authAnggota->pd)->with("relatedPC")->get();

            $listPC = $pd->map(function ($value) {
                return $value->relatedPC->map(function ($value2) {
                    return $value2->kd_pc;
                });
            })->collapse();

            $anggota->whereIn('pc', $listPC);
        } else if (Permission::checkRole('admin_pw') || Permission::checkRole('tasykil_pw')) {

            $relatedPD = PW::where('kd_pw', $authAnggota->pw)
                ->first()->relatedPD->pluck('kd_pd');

            $pd = PD::whereIn('kd_pd', $relatedPD)->with("relatedPC")->get();

            $listPC = $pd->map(function ($value) {
                return $value->relatedPC->map(function ($value2) {
                    return $value2->kd_pc;
                });
            })->collapse();

            $anggota->whereIn('pc', $listPC);
        }
        return Datatables::of($anggota)
            ->addIndexColumn()
            ->make(true);
    }

    public function trash()
    {
        return view('ana.anggota.trash');
    }

    public function restore($id)
    {
        $restoreAnggota = Anggota::withTrashed()->find($id)->restore();
        if ($restoreAnggota) {
            return response()->json(['message' => 'Success restore data', 'data' => $restoreAnggota]);
        } else {
            return response()->json(['message' => 'Failed restore data'], 500);
        }
    }

    public function destroy($id)
    {
        $destroyAnggota = Anggota::withTrashed()->find($id)->forceDelete();
        if ($destroyAnggota) {
            return response()->json(['message' => 'Success restore data', 'data' => $destroyAnggota]);
        } else {
            return response()->json(['message' => 'Failed restore data'], 500);
        }
    }

    public function saveAnggotaOrganisasi($data, $id)
    {
        if ($data['nama_organisasi_1'] != null &&
            $data['tahun_selesai_org_1'] != null &&
            $data['tahun_mulai_org_1'] != null) {

            $org1 = new AnggotaOrganisasi();
            $org1->nama_organisasi = $data['nama_organisasi_1'];
            $org1->jabatan = $data['jabatan_org_1'];
            $org1->tahun_mulai = $data['tahun_mulai_org_1'];
            $org1->tahun_selesai = $data['tahun_selesai_org_1'];
            $org1->lokasi = $data['lokasi_org_1'];
            $org1->id_anggota = $id;
            $org1->save();
        }

        if ($data['nama_organisasi_2'] != null &&
            $data['tahun_selesai_org_2'] != null &&
            $data['tahun_mulai_org_2'] != null) {

            $org2 = new AnggotaOrganisasi();
            $org2->nama_organisasi = $data['nama_organisasi_2'];
            $org2->jabatan = $data['jabatan_org_2'];
            $org2->tahun_mulai = $data['tahun_mulai_org_2'];
            $org2->tahun_selesai = $data['tahun_selesai_org_2'];
            $org2->lokasi = $data['lokasi_org_2'];
            $org2->id_anggota = $id;
            $org2->save();
        }


        if ($data['nama_organisasi_3'] != null &&
            $data['tahun_selesai_org_3'] != null &&
            $data['tahun_mulai_org_3'] != null) {

            $org3 = new AnggotaOrganisasi();
            $org3->nama_organisasi = $data['nama_organisasi_3'];
            $org3->jabatan = $data['jabatan_org_3'];
            $org3->tahun_mulai = $data['tahun_mulai_org_3'];
            $org3->tahun_selesai = $data['tahun_selesai_org_3'];
            $org3->lokasi = $data['lokasi_org_3'];
            $org3->id_anggota = $id;
            $org3->save();
            return true;
        }
    }

    public function saveAnggotaTafiq($data, $id)
    {
        if ($data['tafiq_mulai1'] != null &&
            $data['tafiq_sampai1'] != null) {

            $tafiq1 = new AnggotaTafiq();
            $tafiq1->id_anggota = $id;
            $tafiq1->tanggal_masuk = $data['tafiq_mulai1'];
            $tafiq1->tanggal_selesai = $data['tafiq_sampai1'];
            $tafiq1->lokasi = $data['tafiq_lokasi1'];
            $tafiq1->save();

        }

        if ($data['tafiq_mulai2'] != null &&
            $data['tafiq_sampai2'] != null) {

            $tafiq2 = new AnggotaTafiq();
            $tafiq2->id_anggota = $id;
            $tafiq2->tanggal_masuk = $data['tafiq_mulai2'];
            $tafiq2->tanggal_selesai = $data['tafiq_sampai2'];
            $tafiq2->lokasi = $data['tafiq_lokasi2'];
            $tafiq2->save();

        }

        if ($data['tafiq_mulai3'] != null &&
            $data['tafiq_sampai3'] != null) {

            $tafiq3 = new AnggotaTafiq();
            $tafiq3->id_anggota = $id;
            $tafiq3->tanggal_masuk = $data['tafiq_mulai3'];
            $tafiq3->tanggal_selesai = $data['tafiq_sampai3'];
            $tafiq3->lokasi = $data['tafiq_lokasi3'];
            $tafiq3->save();

        }

        return true;
    }

    public function saveAnggotaPendidikan($data, $id)
    {
        $sd = new AnggotaPendidikan();
        $sd->id_anggota = $id;
        $sd->id_master_pendidikan = 2;
        $sd->instansi = $data['nama_sd'];
        $sd->jurusan = null;
        $sd->tahun_masuk = $data['tahun_masuk_sd'];
        $sd->tahun_keluar = $data['tahun_keluar_sd'];
        $sd->jenis_pendidikan = "Formal";
        $sd->save();

        $smp = new AnggotaPendidikan();
        $smp->id_anggota = $id;
        $smp->id_master_pendidikan = 1;
        $smp->instansi = $data['nama_smp'];
        $smp->jurusan = null;
        $smp->tahun_masuk = $data['tahun_masuk_smp'];
        $smp->tahun_keluar = $data['tahun_keluar_smp'];
        $smp->jenis_pendidikan = "Formal";
        $smp->save();

        if ($data['nama_sma'] != null &&
            $data['tahun_masuk_sma'] != null &&
            $data['tahun_keluar_sma'] != null) {
            $sma = new AnggotaPendidikan();
            $sma->id_anggota = $id;
            $sma->id_master_pendidikan = 3;
            $sma->instansi = $data['nama_sma'];
            $sma->jurusan = null;
            $sma->tahun_masuk = $data['tahun_masuk_sma'];
            $sma->tahun_keluar = $data['tahun_keluar_sma'];
            $sma->jenis_pendidikan = "Formal";
            $sma->save();
        }

        if ($data['nama_d1'] != null &&
            $data['tahun_masuk_d1'] != null &&
            $data['tahun_keluar_d1'] != null) {
            $d1 = new AnggotaPendidikan();
            $d1->id_anggota = $id;
            $d1->id_master_pendidikan = 4;
            $d1->instansi = $data['nama_d1'];
            $d1->jurusan = $data['jurusan_d1'];
            $d1->tahun_masuk = $data['tahun_masuk_d1'];
            $d1->tahun_keluar = $data['tahun_keluar_d1'];
            $d1->jenis_pendidikan = "Formal";
            $d1->save();
        }

        if ($data['nama_d2'] != null &&
            $data['tahun_masuk_d2'] != null &&
            $data['tahun_keluar_d2']) {
            $d2 = new AnggotaPendidikan();
            $d2->id_anggota = $id;
            $d2->id_master_pendidikan = 5;
            $d2->instansi = $data['nama_d2'];
            $d2->jurusan = $data['jurusan_d2'];
            $d2->tahun_masuk = $data['tahun_masuk_d2'];
            $d2->tahun_keluar = $data['tahun_keluar_d2'];
            $d2->jenis_pendidikan = "Formal";
            $d2->save();
        }

        if ($data['nama_d3'] != null &&
            $data['tahun_masuk_d3'] != null &&
            $data['tahun_keluar_d3']) {
            $d3 = new AnggotaPendidikan();
            $d3->id_anggota = $id;
            $d3->id_master_pendidikan = 5;
            $d3->instansi = $data['nama_d3'];
            $d3->jurusan = $data['jurusan_d3'];
            $d3->tahun_masuk = $data['tahun_masuk_d3'];
            $d3->tahun_keluar = $data['tahun_keluar_d3'];
            $d3->jenis_pendidikan = "Formal";
            $d3->save();
        }

        if ($data['nama_s1'] != null &&
            $data['tahun_masuk_s1'] != null &&
            $data['tahun_keluar_s1'] != null) {
            $s1 = new AnggotaPendidikan();
            $s1->id_anggota = $id;
            $s1->id_master_pendidikan = 5;
            $s1->instansi = $data['nama_s1'];
            $s1->jurusan = $data['jurusan_s1'];
            $s1->tahun_masuk = $data['tahun_masuk_s1'];
            $s1->tahun_keluar = $data['tahun_keluar_s1'];
            $s1->jenis_pendidikan = "Formal";
            $s1->save();
        }

        if ($data['nama_s2'] != null &&
            $data['tahun_masuk_s2'] != null &&
            $data['tahun_keluar_s2'] != null) {
            $s2 = new AnggotaPendidikan();
            $s2->id_anggota = $id;
            $s2->id_master_pendidikan = 5;
            $s2->instansi = $data['nama_s2'];
            $s2->jurusan = $data['jurusan_s2'];
            $s2->tahun_masuk = $data['tahun_masuk_s2'];
            $s2->tahun_keluar = $data['tahun_keluar_s2'];
            $s2->jenis_pendidikan = "Formal";
            $s2->save();
        }

        if ($data['nama_s3'] != null &&
            $data['tahun_masuk_s3'] != null &&
            $data['tahun_keluar_d3'] != null) {
            $s3 = new AnggotaPendidikan();
            $s3->id_anggota = $id;
            $s3->id_master_pendidikan = 5;
            $s3->instansi = $data['nama_s3'];
            $s3->jurusan = $data['jurusan_s3'];
            $s3->tahun_masuk = $data['tahun_masuk_s3'];
            $s3->tahun_keluar = $data['tahun_keluar_s3'];
            $s3->jenis_pendidikan = "Formal";
            $s3->save();
        }

        return true;

    }

    public function saveAnggotaKeluarga($data, $id)
    {
        $istri = new AnggotaKeluarga();
        $istri->nama_keluarga = $data['nama_istri'];
        $istri->hubungan = 'Istri';
        $istri->hubungan = 'Istri';
        $istri->id_anggota = $id;
        $istri->jumlah_anak = $data['jumlah_anak'];
        $istri->alamat = $data['alamat'];
        $istri->status_anggota = $data['status_istri'];
        $istri->keterangan = $data['keterangan'];
        $istri->save();

        $ayah = new AnggotaKeluarga();
        $ayah->nama_keluarga = $data['nama_ayah'];
        $ayah->hubungan = 'Ayah';
        $ayah->alamat = $data['alamat_orangtua'];
        $ayah->id_anggota = $id;
        $ayah->status_anggota = $data['status_ayah'];
        $ayah->save();

        $ibu = new AnggotaKeluarga();
        $ibu->nama_keluarga = $data['nama_ibu'];
        $ibu->hubungan = 'Ibu';
        $ibu->alamat = $data['alamat_orangtua'];
        $ibu->id_anggota = $id;
        $ibu->status_anggota = $data['status_ibu'];
        $ibu->save();

        return true;
    }

    public function npa(Request $request)
    {
        $data = Anggota::where('npa', $request->input('npa'));
        if ($request->input('id')) $data->where('id_anggota', '!=', $request->input('id'));
        return $data->count() > 0 ? 'false' : 'true';
    }

    public function email(Request $request)
    {
        $data = Anggota::where('email', $request->input('email'));
        if ($request->input('id')) $data->where('id_anggota', '!=', $request->input('id'));
        return $data->count() > 0 ? 'false' : 'true';
    }

    public function exportTemplate()
    {
        return Excel::download(new AnggotaTable, 'anggota.xlsx');
    }

    public function showImport()
    {
        return view('ana.excel.import');
    }

    public function import(Request $request)
    {
        try{
            $validation = validator($request->all(), [
                'file' => 'required'
            ], ['file.required' => 'Silahkan pilih file yang akan diupload']);
            $validation->validate();
            $file = $request->file('file');
            Excel::import(new AnggotaImport, $file);
            return back()->withStatus('File excel berhasil diupload');
        }catch(\Maatwebsite\Excel\Validators\ValidationException $e){
            $failures = $e->failures();
            $dataError = collect([]);

            foreach ($failures as $failure) {
                $row = $failure->row();
                $attribute = $failure->attribute();
                $error = implode(", ", $failure->errors());
                $dataError->put($attribute,
                    "Error pada Row excel ke {$row}. {$error}");
            }
            return back()->withErrors($dataError->toArray());
        }
    }

    public function exportDataAnggota(Request $request)
    {
        try {
            $url = env('S3_STATIC_FOLDER') . "excel-anggota-template.xlsx";
            return response(file_get_contents($url))
                ->header('Content-Type', 'application/vnd.ms-excel')
                ->header("Content-Disposition" , "attachment;filename=ExportData.xlsx")
                ->header('Cache-Control', 'max-age=0');
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function exportPDF(Request $request)
    {
        try {
            $data = $request->json();
            $pdf = app()->make('dompdf.wrapper');
            $pdf->loadView('layouts.anggota_pdf', ['data' => $data])->setPaper('a4', 'landscape');
            return $pdf->stream();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * uploadImageFromBase64toAWS the specified resource.
     *
     * @param string $file_base64
     * @param string $dir
     * @param string $filename
     * @return Response
     */
    public function uploadImageFromBase64toAWS($file_base64, $dir, $filename)
    {
        Storage::disk('s3')->put($dir . $filename, file_get_contents($file_base64));
    }

    /**
     * profileCard the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function profileCard(Request $request)
    {
        $id = $request->id;
        $data['anggota'] = Anggota::with('pimpinanCabang', 'pimpinanDaerah', 'pimpinanWilayah', 'keluarga', 'keterampilan', 'mutasi', 'organisasi', 'pekerjaan', 'pendidikan', 'training', 'village')
            ->where('id_anggota', $id)->first();
        if (!$data['anggota']) abort(404);
        $roles = UserLogin::where('npa', $data['anggota']->npa)->first();
        if (is_null($roles)) {
            $data['roles'] = $roles;
        } else {
            $data['roles'] = $roles->roles;
        }
        return view('ana.anggota.kta', $data);
    }

    public function templateCard(Request $request)
    {
        $id = $request->id;
        $data['anggota'] = Anggota::where('id_anggota', $id)->first();
        if (!$data['anggota']) abort(404);
        $roles = UserLogin::where('npa', $data['anggota']->npa)->first();
        if (is_null($roles)) {
            $data['roles'] = $roles;
        } else {
            $data['roles'] = $roles->roles;
        }
        return view('ana.anggota.e-kta.template', $data);
    }

    public function getImageProfile(Request $request)
    {
        $id = $request->id;
        $anggota = Anggota::with('village')->where('id_anggota', $id)->first();
        if (!$anggota) abort(404);
        $roles = UserLogin::where('npa', $anggota->npa)->first();
        if (is_null($roles)) {
            $data['roles'] = $roles;
        } else {
            $data['roles'] = $roles->roles;
        }

        $pathToFile = Storage::disk('s3')->get('images/anggota/'.$anggota->foto);

        $base64 = 'data:image/png;base64,' . base64_encode($pathToFile);
        return $base64;
    }

    public function updatePassword(Request $request)
    {
        $status = null;
        try {
            if (Hash::check($request->current_password, Auth::user()->password)) {
                $status = UserLogin::find(Auth::user()->id_login)
                    ->update(['password'=> $request->new_password]);
            }

            if ($status) {
                return response()->json(
                    [
                        "message" => "success",
                        "code" => ResponseAlias::HTTP_ACCEPTED,
                    ],
                    ResponseAlias::HTTP_ACCEPTED
                );
            } else {
                throw new Exception("Kata sandi Anda saat ini tidak cocok");
            }
        } catch (\Exception $e) {
            return response()->json(
                [
                    "message" => $e->getMessage(),
                    "code" => ResponseAlias::HTTP_INTERNAL_SERVER_ERROR,
                ],
                ResponseAlias::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function showPasswordForm()
    {
        return view("ana.anggota.change_password");
    }

    /**
     * Update the specified anggota photo profile.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function updateImageProfile(Request $request)
    {
        $id = $request->id;
        $request->validate([
            "foto-upload" => "nullable|mimes:jpeg,png,jpg|max:2048",
        ]);

        try {
            $anggota = Anggota::select('nama_lengkap', 'foto')
            ->where('id_anggota', $id)->first();

            if($anggota->foto) {
                $anggotaBefore = $anggota->foto;
                Storage::disk('s3')->delete('images/anggota/' .$anggotaBefore);

                $anggotaUpdate = "anggota_update_".$anggota->foto;
                Storage::disk('s3')->delete('images/anggota/' .$anggotaUpdate);
            }

            $dir = 'images/anggota/';

            $file_base64 = $request->foto;
            $filename = "anggota_".$anggota->nama_lengkap . '_' . date('dmY_His') . '.' . 'png';
            $this->uploadImageFromBase64toAWS($file_base64, $dir, $filename);
            $newAnggota = Anggota::where('id_anggota', $id)->first();
            DB::beginTransaction();
            $newAnggota->update(['foto'=>$filename]);
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['message' => 'Foto Profile successfully updated', 'data' => 1]);

    }
}
