<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\PD;
use App\Model\PerformaPD;
use App\Model\PerformaPW;
use App\Model\Province;
use App\Model\PW;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PerformaPdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pw = Auth::user()->anggota->pw;
        $pd = Auth::user()->anggota->pd;
        $relatedPD = PW::where('kd_pw', $pw)
            ->first()->relatedPD->pluck('kd_pd');
        $performaPD = null;

        if (Permission::checkRole('superadmin') ||
            Permission::checkRole('tasykil_pp') ||
            Permission::checkRole('admin_pp')) {
            $performaPD = PerformaPD::with('pw','pd')->get();
        } else if (Permission::checkRole('admin_pd') || Permission::checkRole('tasykil_pd')) {
            $performaPD = PerformaPD::with('pw','pd')->where('kd_pd', $pd)->get();
        } else if (Permission::checkRole('admin_pw') || Permission::checkRole('tasykil_pw')) {
            $performaPD = PerformaPD::with('pw','pd')->whereIn('kd_pd', $relatedPD)->get();
        }

        return view('ana.performa.pd.index', compact('performaPD'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.performa.pd.add', compact('dataPW', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $performa = $request->except([
            "_token", "namaPD", "namaPW",
            "noPD", "noPW", "provinces",
            "regencies", "districts"
        ]);

        $now = Carbon::now();
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['kd_pd'] = $data['noPD'];
        $performa['kd_pw'] = $data['noPW'];
        $performa['provinsi'] = $data['provinces'];
        $performa['kota'] = $data['regencies'];

        if ($request->hasFile("foto")) {
            $file = $request->file('foto');
            $filename = $performa['kd_pd'] . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->put('images/performa/pd/' . $data['namaPW'] . '/' . $data['namaPD'] . '/' . $filename, file_get_contents($file));
            $performa['foto'] = $filename;
        } else {
            $performa['foto'] = "default.png";
        }

        $status = PerformaPD::insert($performa);

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PerformaPD::find($id);

        if (!$data) abort(404);

        return view('ana.performa.pd.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PerformaPD::find($id);

        if (!$data) abort(404);

        $dataPW = PW::all();
        $provinces = Province::all();

        return view('ana.performa.pd.edit', compact(
             'dataPW',
            'provinces', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $performa = $request->except([
            "_token", "namaPD", "namaPW",
            "noPD", "noPW", "provinces",
            "regencies", "districts", "_method"
        ]);

        $now = Carbon::now();
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['kd_pd'] = $data['noPD'];
        $performa['kd_pw'] = $data['noPW'];
        $performa['provinsi'] = $data['provinces'];
        $performa['kota'] = $data['regencies'];

        if ($request->hasFile("foto")) {
            $file = $request->file('foto');
            $filename = $performa['kd_pd'] . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->put('images/performa/pd/' . $data['namaPW'] . '/' . $data['namaPD'] . '/' . $filename, file_get_contents($file));
            $performa['foto'] = $filename;
        }

        $record = PerformaPD::find($id);

        foreach ($performa as $index => $item) {
            $record->$index = $item;
        }

        $status = $record->save();

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PerformaPD::find($id)->delete();
        return response()->json($data);
    }
}
