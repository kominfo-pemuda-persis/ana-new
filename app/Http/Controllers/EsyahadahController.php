<?php

namespace App\Http\Controllers;

use App\Model\Esyahadah;
use Illuminate\Http\Request;

class EsyahadahController extends Controller
{
    public function __construct(Esyahadah $esyahadah) {
        $this->esyahadah = $esyahadah;
    }

    public function sendEmailEsyahadah(Request $request){
        return $this->esyahadah->sendEmailEsyahadah(
            "upload", $request->file('csv'));
    }

    public function index()
    {
        return view("esyahadah_index");
    }

    public function generatePDF(Request $request){
        return $this->esyahadah->savePDF($request->id);
    }
}
