<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\PerformaPJ;
use App\Model\Province;
use App\Model\PW;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PerformaPjController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pw = Auth::user()->anggota->pw;
        $pd = Auth::user()->anggota->pd;
        $pc = Auth::user()->anggota->pc;
        $pj = Auth::user()->anggota->pj;
        $performaPJ = null;

        if (Permission::checkRole('superadmin') ||
            Permission::checkRole('tasykil_pp') ||
            Permission::checkRole('admin_pp')) {
            $performaPJ = PerformaPJ::with('pw','pd','pc')->get();
        } elseif ((Permission::checkRole('admin_pw')) || (Permission::checkRole('tasykil_pw'))) {
            $performaPJ = PerformaPJ::with('pw','pd','pc')->where('kd_pw', $pw)->get();
        } elseif ((Permission::checkRole('admin_pd')) || (Permission::checkRole('tasykil_pd'))) {
            $performaPJ = PerformaPJ::with('pw','pd','pc')->where('kd_pd', $pd)->get();
        } elseif ((Permission::checkRole('admin_pc')) || (Permission::checkRole('tasykil_pc'))) {
            $performaPJ = PerformaPJ::with('pw','pd','pc')->where('kd_pc', $pc)->get();
        } elseif (Permission::checkRole('admin_pj')) {
            $performaPJ = PerformaPJ::with('pw','pd','pc')->where('kd_pj', $pj)->get();
        }

        return view('ana.performa.pj.index', compact('performaPJ'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.performa.pj.add', compact('dataPW', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $performa = $request->except([
            "_token", "namaPD", "namaPW", "namaPC",
            "noPD", "noPW", "provinces",
            "regencies", "districts"
        ]);

        $now = Carbon::now();
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['kd_pd'] = $data['noPD'];
        $performa['kd_pw'] = $data['noPW'];
        $performa['provinsi'] = $data['provinces'];
        $performa['kota'] = $data['regencies'];

        if ($request->hasFile("foto")) {
            $file = $request->file('foto');
            $filename = $data['kd_pj'] . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->put('images/performa/pj/' . $data['namaPW'] . '/' . $data['namaPD'] . '/' . $data['namaPC'] . '/' . $data['nama_pj'] . '/' . $filename, file_get_contents($file));
            $performa['foto'] = $filename;
        } else {
            $performa['foto'] = "default.png";
        }

        $status = PerformaPJ::insert($performa);

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $data = PerformaPJ::find($id);

        if(!$data) abort(404);

        return view('ana.performa.pj.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data = PerformaPJ::find($id);

        if(!$data) abort(404);

        $dataPW = PW::all();
        $provinces = Province::all();

        return view('ana.performa.pj.edit', compact(
            'dataPW',
            'provinces', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $performa = $request->except([
            "_token", "namaPD", "namaPW","namaPC",
            "noPD", "noPW", "provinces",
            "noPC",
            "regencies", "districts", "_method"
        ]);

        $now = Carbon::now();
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['kd_pd'] = $data['noPD'];
        $performa['kd_pw'] = $data['noPW'];
        $performa['kd_pc'] = $data['noPC'];
        $performa['provinsi'] = $data['provinces'];
        $performa['kota'] = $data['regencies'];

        if ($request->hasFile("foto")) {
            $file = $request->file('foto');
            $filename = $data['kd_pj'] . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->put('images/performa/pj/' . $data['namaPW'] . '/' . $data['namaPD'] . '/' . $data['namaPC'] . '/' . $data['nama_pj'] . '/' . $filename, file_get_contents($file));
            $performa['foto'] = $filename;
        }

        $record = PerformaPJ::find($id);

        foreach ($performa as $index => $item) {
            $record->$index = $item;
        }

        $status = $record->save();

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = PerformaPJ::find($id)->delete();
        return response()->json($data);
    }
}
