<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\PerformaPW;
use App\Model\Province;
use App\Model\PW;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PerformaPwController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pw = Auth::user()->anggota->pw;
        $performaPW = null;

        if (Permission::checkRole('superadmin') ||
            Permission::checkRole('tasykil_pp') ||
            Permission::checkRole('admin_pp')) {
            $performaPW = PerformaPW::with('pw')->get();
        } else if (Permission::checkRole('admin_pw')) {
            $performaPW = PerformaPW::with('pw')->where('kd_pw', $pw)->get();
        } else if (Permission::checkRole('tasykil_pw')) {
            $performaPW = PerformaPW::with('pw')->where('kd_pw', $pw)->get();
        }

        return view('ana.performa.pw.index', compact('performaPW'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.performa.pw.add', compact('dataPW', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $performa = $request->except([
            "_token", "namaPW",
            "noPW", "provinces"
        ]);

        $now = Carbon::now();
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['kd_pw'] = $data['noPW'];
        $performa['provinsi'] = $data['provinces'];

        if ($request->hasFile("foto")) {
            $file = $request->file('foto');
            $filename = $performa['kd_pw'] . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->put('images/performa/pw/' . $data['namaPW'] . '/' . $filename, file_get_contents($file));
            $performa['foto'] = $filename;
        } else {
            $performa['foto'] = "default.png";
        }

        $status = PerformaPW::insert($performa);

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PerformaPW::find($id);

        if (!$data) abort(404);

        return view('ana.performa.pw.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PerformaPW::find($id);

        if (!$data) abort(404);

        $dataPW = PW::all();
        $provinces = Province::all();

        return view('ana.performa.pw.edit', compact(
            'dataPW', 'provinces', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $performa = $request->except([
            "_token", "namaPW",
            "noPW", "provinces", "_method"
        ]);

        $now = Carbon::now();
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['kd_pw'] = $data['noPW'];
        $performa['provinsi'] = $data['provinces'];

        if ($request->hasFile("foto")) {
            $file = $request->file('foto');
            $filename = $performa['kd_pw'] . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->put('images/performa/pw/' . $data['namaPW'] . '/' . $filename, file_get_contents($file));
            $performa['foto'] = $filename;
        }

        $record = PerformaPW::find($id);

        foreach ($performa as $index => $item) {
            $record->$index = $item;
        }

        $status = $record->save();

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PerformaPW::find($id)->delete();
        return response()->json($data);
    }
}
