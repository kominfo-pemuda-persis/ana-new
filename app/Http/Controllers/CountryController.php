<?php

namespace App\Http\Controllers;


use App\Model\District;
use App\Model\Province;
use App\Model\Regency;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
    public function provinces()
    {
        $provinces = Province::all();
        return response()->json($provinces);
    }

    public function regencies()
    {
        $provinces_id = request()->get('province_id');
        $regencies = Regency::where('provinsi_id', '=', $provinces_id)->orderBy('nama', 'ASC')->get();
        return response()->json($regencies);
    }


    public function districts()
    {
        $regencies_id = request()->get('regencies_id');
        $districts = District::where('kabupaten_id', '=', $regencies_id)->orderBy('nama', 'ASC')->get();
        return response()->json($districts);
    }

    public function villages()
    {
        $districts_id = request()->get('districts_id');
//        $villages = Village::where('kecamatan_id', $districts_id)->get();
        $villages = DB::table('t_desa')->where('kecamatan_id', $districts_id)->orderBy('nama', 'ASC')->get();
//        Log::debug($villages);
        return response()->json($villages);
    }
}
