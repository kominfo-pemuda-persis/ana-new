<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Mail\HerRegistrationApproved;
use App\Mail\HerRegistrationProcessing;
use App\Mail\HerRegistrationRejected;
use App\Model\Anggota;
use App\Model\HerRegistration;
use App\Model\HerRegistrationFile;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Yajra\DataTables\Facades\DataTables;

class HerRegistrasiController extends Controller
{
    public $PATH_FILE = "heregistrasi/";

    public function index(){
        return view('ana.her_registration.index');
    }

    public function prosesApproval(Request $request)
    {
        if(! Permission::checkPermission('herregistrasi.approve'))
            return response()->json(['message' => 'Unauthorized to approve heregistrasi'], 403);

        $threshold = 1987;
        $data = $request->json()->all();
        $data['npaData'] = collect([]);
        $data['emailData'] = collect([]);


        try {
            if (!Storage::disk("local")->exists("temp")){
                Storage::disk("local")->makeDirectory("temp");
            }

            $her = HerRegistration::with('file')->findOrFail($data['id']);
            $reader = ReaderEntityFactory::createXLSXReader();

            $her->file->each(function ($value) use($reader, $data){
                if($value->filetype === "excel"){
                    Storage::disk("local")->put("temp/" .
                        $value->path, Storage::disk('s3')->get(
                            'heregistrasi/' . $value->path_folder. "/" . $value->path));
                    $reader->open(storage_path("app/temp/" . $value->path));
                    foreach ($reader->getSheetIterator() as $sheet) {
                        foreach ($sheet->getRowIterator() as $index => $row) {
                            if ($index > 1){
                                $data['npaData']->push($row->getCellAtIndex(0)->getValue());
                                $data['emailData']->push($row->getCellAtIndex(2)->getValue());
                            }
                        }
                    }
                    $reader->close();
                    Storage::disk("local")->delete("temp/" . $value->path);
                }
            });

            $data['npaData']->unique();
            $data['emailData']->unique();


            $anggota = Anggota::whereIn("npa", $data['npaData'])
                ->get()->map(function ($item) use ($threshold){
                    $masa_aktif = $item->masa_aktif_kta;

                    if(is_null($masa_aktif)){
                        $masa_aktif = new Carbon();
                    }else{
                        $masa_aktif = $item->masa_aktif_kta->copy();
                    }

                    if($item->tanggal_lahir->year >= $threshold){
                        $masa_aktif->year = 2026;
                        $masa_aktif->month = 12;
                        $masa_aktif->day = 31;
                    }else{
                        $masa_aktif = $item->tanggal_lahir->copy()->addYear(41);
                    }

                    return [
                        "id" => $item->id_anggota,
                        "npa" => $item->npa,
                        "masa_aktif_kta" => $masa_aktif->toDateString(),
                    ];
                });

            if($anggota->isEmpty()){
                throw new Exception(
                    "List NPA tidak dikenali sistem. Hubungi Admin", 500);
            }

            $status = DB::transaction(function () use ($data, $anggota) {
                $anggota->each(function ($item){
                    Anggota::where("npa", $item['npa'])
                        ->update([
                            "status_her" => "SUDAH_BAYAR",
                            "masa_aktif_kta" => $item['masa_aktif_kta'],
                        ]);
                });


                $statusData = HerRegistration::where('id', $data['id'])->update(["status" => "APPROVED"]);

                if($statusData){
                    foreach($data['emailData'] as $email){
                        Mail::to(trim($email))->send(new HerRegistrationApproved());
                    }
                    Mail::to(trim($data['emailUploader']))->send(new HerRegistrationApproved());
                    return $statusData;
                }else{
                    throw new Exception(
                        "Terjadi kesalahan pada sistem. Silahkan kontak Admin", 500);

                }

            });

            if($status){
                $npaRejected = collect($data["npaData"])->filter(function($item){
                    return is_null(Anggota::where('npa', $item)->first());
                });

                $warning = "";

                if(!$npaRejected->isEmpty()){
                    $warning = "Perhatian! : data dengan NPA <b>" . implode(", ", $npaRejected->all()) . " </b> tidak tercatat pada sistem. Silahkan hubungi Admin";
                }

                return response()->json([
                    "message" => "success",
                    "warning" => $warning,
                    "code" => ResponseAlias::HTTP_ACCEPTED
                ], ResponseAlias::HTTP_ACCEPTED);
            } else {
                throw new Exception(
                    "List NPA pada excel tidak terdaftar dalam database. Periksa kembali data anda", 400);
            }

        } catch (Exception $e) {
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function dataTables()
    {
        return Datatables::eloquent(HerRegistration::with([
            'anggota', 'anggota.pimpinanCabang', 'file',
            'anggota.pimpinanDaerah', 'anggota.pimpinanWilayah']))
            ->order(function ($query) {
                $query->orderBy('created_at', 'desc');
            })
            ->filterColumn("nama_pw", function($query, $keyword) {
                $query->whereHas("anggota.pimpinanWilayah", function ($query_anggota) use($keyword){
                    return $query_anggota->where("nama_pw", 'LIKE',  "%{$keyword}%");
                });
            })
            ->filterColumn("nama_pd", function($query, $keyword) {
                $query->whereHas("anggota.pimpinanDaerah", function ($query_anggota) use($keyword){
                    return $query_anggota->where("nama_pd", 'LIKE',  "%{$keyword}%");
                });
            })
            ->filterColumn("nama_pc", function($query, $keyword) {
                $query->whereHas("anggota.pimpinanCabang", function ($query_anggota) use($keyword){
                    return $query_anggota->where("nama_pc", 'LIKE',  "%{$keyword}%");
                });
            })
            ->addColumn('s3_path', function (HerRegistration $data) {
                return Storage::disk('s3')->url('heregistrasi/');
            })
            ->addColumn('link_foto', function (HerRegistration $data) {
                return Storage::disk('s3')->url('heregistrasi/' . $data->path_folder . '/' . $data->path_image);
            })
            ->addColumn('link_excel', function (HerRegistration $data) {
                return Storage::disk('s3')->url(
                    'heregistrasi/' . $data->path_folder . '/' . $data->path_excel);
            })
            ->make(true);
    }

    public function setStatus($id, $status)
    {
        if(! Permission::checkPermission('herregistrasi.setstatus'))
            return response()->json(['message' => 'Unauthorized to set heregistrasi status'], 403);

        try {
            $status = DB::transaction(function () use ($status, $id){
                $her = HerRegistration::with('file')->findOrFail($id);
                $her->status = $status;

                if ($her->save()){
                    Mail::to(trim($her->email))->send(new HerRegistrationRejected());
                    return true;
                }

                return false;

            });

            if($status){
                return response()->json([
                    "message" => "success",
                    "code" => ResponseAlias::HTTP_ACCEPTED
                ], ResponseAlias::HTTP_ACCEPTED);
            }else{
                throw new Exception(
                    "Terjadi kesalahan pada sistem. Silahkan kontak Admin", 500);
            }


        } catch (Exception $e) {
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function indexTable()
    {
        return view('ana.her_registration.index_table');
    }

    public function processHerRegistrationForm(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'npa' => 'required|exists:t_anggota,npa',
                'phone_number' => 'required'
            ], [
                'npa.exists' => 'NPA yang anda masukan tidak dikenali sistem.'
            ]);

            if($validator->fails()){
                throw new Exception(
                    collect($validator->errors()->all())->implode(", "), ResponseAlias::HTTP_BAD_REQUEST);
            }

            $status = DB::transaction(function () use ($req) {

                $her = new HerRegistration();
                $her->name = $req->get("name");
                $her->email = trim($req->get("email"));
                $her->npa = $req->get("npa");
                $her->path_folder = null;
                $her->path_excel = null;
                $her->path_image = null;
                $her->phone_number = $req->get("phone_number");
                $status = $her->save();

                try{
                    if($status){
                        $status = collect($req->file('file-excel'))->each(function($file) use($her){
                        $pathInfoFolder = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        $explode = explode('.', $file->getClientOriginalName());
                        $originalName = $explode[0];
                        $extension = $file->getClientOriginalExtension();
                        $rename = $originalName . '_' . date('dmY_His') . '.' . $extension;

                        $fileData = new HerRegistrationFile();
                        $fileData->her_registration_id = $her->id;
                        $fileData->path_folder = $pathInfoFolder;
                        $fileData->path = $rename;
                        $fileData->filetype = "excel";

                        $status = $fileData->save();

                        if($status){
                            Storage::disk('s3')->put(
                                $this->PATH_FILE . $pathInfoFolder . "/" .
                                $rename, file_get_contents($file));
                        }

                        return $status;

                    });

                    $status = collect($req->file('foto-upload'))->each(function ($file) use ($her){
                        $pathInfoFolder = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        $explode2 = explode('.', $file->getClientOriginalName());
                        $originalName2 = $explode2[0];
                        $extension2 = $file->getClientOriginalExtension();
                        $rename2 = $originalName2 . '_' . date('dmY_His') . '.' . $extension2;

                        $fileData = new HerRegistrationFile();
                        $fileData->her_registration_id = $her->id;
                        $fileData->path_folder = $pathInfoFolder;
                        $fileData->path = $rename2;
                        $fileData->filetype = "image";

                        $status = $fileData->save();

                        if($status){
                            Storage::disk('s3')->put(
                                $this->PATH_FILE . $pathInfoFolder . "/" .
                                $rename2, file_get_contents($file));
                        }

                        return $status;
                    });
                }

                }catch(Exception $e){
                    throw new Exception($e->getMessage(), 500);
                }

                return $status;

            });

            if ($status) {
                Mail::to(trim($req->get("email")))->queue(new HerRegistrationProcessing());
                return response()->json(['message' => 'Formulir berhasil disubmit', 'data' => 1]);
            } else {
                throw new Exception(
                    "Terjadi kesalahan pada sistem. Silahkan kontak Admin", 500);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], $e->getCode());
        }

    }
}
