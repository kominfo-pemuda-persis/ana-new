<?php

namespace App\Http\Controllers;

use App\Exports\AnggotaTable;
use App\Facades\Permission;
use App\Imports\AnggotaImport;
use App\Jobs\ApprovalCaangProcessor;
use App\Model\Anggota;
use App\Model\AnggotaKeluarga;
use App\Model\AnggotaOrganisasi;
use App\Model\AnggotaPendidikan;
use App\Model\AnggotaTafiq;
use App\Model\Caang;
use App\Model\Esyahadah;
use App\Model\MasterPendidikan;
use App\Model\PC;
use App\Model\PD;
use App\Model\Province;
use App\Model\PW;
use App\Model\UserLogin;
use Carbon\Carbon;
use DataTables;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Throwable;

// use Auth;

class CaangAdminController extends Controller
{
    public function __construct(Esyahadah $esyahadah)
    {
        $this->esyahadah = $esyahadah;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $listPendidikan = MasterPendidikan::all();
        $data["listPendidikan"] = $listPendidikan;
        $data["role"] = Auth::user()
            ->roles->pluck("name")
            ->toArray();
        $data["admin_pc"] = "admin_pc";
        $data["admin_pd"] = "admin_pd";
        $data["admin_pw"] = "admin_pw";
        $data["admin_pp"] = "admin_pp";
        $data["admin_pj"] = "admin_pj";
        $data["tasykil_pp"] = "tasykil_pp";
        $data["tasykil_pw"] = "tasykil_pw";
        $data["tasykil_pd"] = "tasykil_pd";
        $data["tasykil_pc"] = "tasykil_pc";
        $data["superadmin"] = "superadmin";
        return view("ana.caang.index", $data);
    }

    private function getAuthAnggota()
    {
        $currentNPA = Auth::user()->npa;
        return Anggota::where("npa", $currentNPA)->first();
    }

    /**
     * Profile the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function detail(Request $request)
    {
        $id = $request->id;
        $pwName = null;
        $pdName = null;
        $pcName = null;

        $caangDetail = Caang::select(
            "t_caang.*",
            "t_provinsi.nama as nama_provinsi",
            "t_kabupaten.nama as nama_kabupaten",
            "t_kecamatan.nama as nama_kecamatan",
            "t_desa.nama as nama_desa"
        )
            ->join("t_provinsi", "t_caang.provinsi", "=", "t_provinsi.id")
            ->join("t_kabupaten", "t_caang.kota", "=", "t_kabupaten.id")
            ->join("t_kecamatan", "t_caang.kecamatan", "=", "t_kecamatan.id")
            ->join("t_desa", "t_caang.desa", "=", "t_desa.id")
            ->with(["pendidikan", "organisasi", "keluarga"])
            ->where("id_caang", $id)
            ->first();
        if (!$caangDetail) {
            abort(404);
        }
        $pwName = PW::select("nama_pw")
            ->where("kd_pw", $caangDetail->pw)
            ->value("nama_pw");
        $pdName = PD::select("nama_pd")
            ->where("kd_pd", $caangDetail->pd)
            ->value("nama_pd");
        $pcName = PC::select("nama_pc")
            ->where("kd_pc", $caangDetail->pc)
            ->value("nama_pc");

        if ($caangDetail->foto !== "default.png") {
            $image = Storage::disk("s3")->url(
                "images/caang/" . $caangDetail->foto
            );
        } else {
            $image = Storage::disk("s3")->url("static/" . $caangDetail->foto);
        }
        $data["caang"] = $caangDetail;
        $data["caang"]["linkFoto"] = $image;
        $data["caang"]["pwName"] = $pwName;
        $data["caang"]["pdName"] = $pdName;
        $data["caang"]["pcName"] = $pcName;
        return view("ana.caang.detail", $data);
    }

    public function datatables(Request $request)
    {
        $start = $request->get("start") ?? 0;
        $length = $request->get("length") ?? 10;
        // $draw = $request->get('draw') ?? 1;
        $columnIndex_arr = $request->get("order");
        $columnName_arr = $request->get("columns");
        $order_arr = $request->get("order");
        // $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]["column"]; // Column index
        $columnName = $columnName_arr[$columnIndex]["data"]; // Column name
        $columnSortOrder = $order_arr[0]["dir"]; // asc or desc
        // $searchValue = $search_arr['value']; // Search value
        $createdAtStart = $request->get("createdStart");
        $createdAtEnd = $request->get("createdEnd");

        // Get current authenticated anggota to get pw, pd, and pc id
        $authAnggota = $this->getAuthAnggota();

        if (
            Permission::checkPermission("anggota.read_pw") ||
            Permission::checkPermission("anggota.read_pd") ||
            Permission::checkPermission("anggota.read_pc") ||
            Permission::checkPermission("anggota.read_all")
        ) {
            $anggota = Caang::select(
                "t_caang.*",
                "t_provinsi.nama as nama_provinsi",
                "t_kabupaten.nama as nama_kabupaten",
                "t_kecamatan.nama as nama_kecamatan",
                "t_desa.nama as nama_desa"
            )
                ->join("t_provinsi", "t_caang.provinsi", "=", "t_provinsi.id")
                ->join("t_kabupaten", "t_caang.kota", "=", "t_kabupaten.id")
                ->join(
                    "t_kecamatan",
                    "t_caang.kecamatan",
                    "=",
                    "t_kecamatan.id"
                )
                ->join("t_desa", "t_caang.desa", "=", "t_desa.id");
        } else {
            $anggota = Caang::select("id_caang", "nama_lengkap");
        }

        // filternya di sini
        if (
            Permission::checkRole("admin_pc") ||
            Permission::checkRole("tasykil_pc")
        ) {
            $anggota->where("pc", $authAnggota->pc);
            $anggota->whereIn("status", ['INIT', 'APPROVED_BY_PC', 'APPROVED_BY_PD', 'APPROVED_BY_PW', 'APPROVED_BY_PP']);
        } elseif (
            Permission::checkRole("admin_pd") ||
            Permission::checkRole("tasykil_pd")
        ) {
            $pd = PD::where("kd_pd", $authAnggota->pd)
                ->with("relatedPC")
                ->get();

            $listPC = $pd
                ->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })
                ->collapse();

            $anggota->whereIn("pc", $listPC);
            $anggota->whereIn("status", ['INIT', 'APPROVED_BY_PC', 'APPROVED_BY_PD', 'APPROVED_BY_PW', 'APPROVED_BY_PP']);
        } elseif (
            Permission::checkRole("admin_pw") ||
            Permission::checkRole("tasykil_pw")
        ) {
            $relatedPD = PW::where("kd_pw", $authAnggota->pw)
                ->first()
                ->relatedPD->pluck("kd_pd");
            $pd = PD::whereIn("kd_pd", $relatedPD)
                ->with("relatedPC")
                ->get();
            $listPC = $pd
                ->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })
                ->collapse();
            $anggota->whereIn("pc", $listPC);
            $anggota->whereIn("status", ['INIT', 'APPROVED_BY_PC', 'APPROVED_BY_PD', 'APPROVED_BY_PW', 'APPROVED_BY_PP']);
        }

        // Added advance serarch
        // Filter by gol_darah ('A', 'B', 'AB', 'O', '-')
        if ($request->get("gol_darah")) {
            $anggota->where("gol_darah", $request->get("gol_darah"));
        }

        // Filter by pendidikan_terakhir (KOSONG, SD, MI, SMP, MTs, SMA, SMK, STM, MA, MLN, D1, D2, D3, D4, S1, S2, S3)
        if ($request->get("pendidikan_terakhir")) {
            $anggota->where(
                "pendidikan_terakhir",
                $request->get("pendidikan_terakhir")
            );
        }

        // Filter by level_tafiq ('SUDAH','BELUM',1, 2, 3)
        if ($request->get("createdStart") && $request->get("createdEnd")) {
            $from = $request->get("createdStart");
            $to = $request->get("createdEnd");
            $anggota->whereBetween("reg_date", [$from, $to]);
        }

        // status Marital
        if ($request->get("status_merital")) {
            $anggota->where("status_merital", $request->get("status_merital"));
        }

        // approval status
        if ($request->get("status_approval")) {
            $anggota->where("status", $request->get("status_approval"));
        }

        // $anggota->skip($start)->take($length);
        $anggota->orderBy($columnName, $columnSortOrder);

        // if (Permission::checkPermission('anggota.read_pw') || Permission::checkPermission('anggota.read_pd') || Permission::checkPermission('anggota.read_pc') || Permission::checkPermission('anggota.read_pj') || Permission::checkPermission('anggota.read_all')) {
        if (
            Permission::checkPermission("anggota.read_pw") ||
            Permission::checkPermission("anggota.read_pd") ||
            Permission::checkPermission("anggota.read_pc") ||
            Permission::checkPermission("anggota.read_all")
        ) {
            return Datatables::of($anggota)
                ->addIndexColumn()
                ->filterColumn("nama_provinsi", function ($query, $keyword) {
                    $query->whereHas("province", function (
                        $query_provinsi
                    ) use ($keyword) {
                        return $query_provinsi->where(
                            "nama",
                            "LIKE",
                            "%{$keyword}%"
                        );
                    });
                })
                ->filterColumn("nama_kabupaten", function ($query, $keyword) {
                    $query->whereHas("regency", function (
                        $query_kabupaten
                    ) use ($keyword) {
                        return $query_kabupaten->where(
                            "nama",
                            "LIKE",
                            "%{$keyword}%"
                        );
                    });
                })
                ->filterColumn("nama_kecamatan", function ($query, $keyword) {
                    $query->whereHas("district", function (
                        $query_kecamatan
                    ) use ($keyword) {
                        return $query_kecamatan->where(
                            "nama",
                            "LIKE",
                            "%{$keyword}%"
                        );
                    });
                })
                ->filterColumn("nama_desa", function ($query, $keyword) {
                    $query->whereHas("village", function ($query_desa) use (
                        $keyword
                    ) {
                        return $query_desa->where(
                            "nama",
                            "LIKE",
                            "%{$keyword}%"
                        );
                    });
                })
                ->addColumn("linkFoto", function (Caang $anggota) {
                    $dir = "images/caang/";

                    if ($anggota->foto !== "default.png") {
                        $image = Storage::disk("s3")->url(
                            $dir . $anggota->foto
                        );
                    } else {
                        $image = Storage::disk("s3")->url(
                            "static/" . $anggota->foto
                        );
                    }
                    return $image;
                })
                ->make(true);
        }

        return Datatables::of($anggota)
            // ->addIndexColumn()
            ->make(true);
    }

    public function datatablesPC(Request $request)
    {
        $id = $request->id;
        $anggota = DB::table("t_anggota")
            ->select("id_anggota", "nama_lengkap", "npa", "status_Aktif")
            ->where("pc", $id)
            ->orderBy("id_anggota", "asc")
            ->get();
        return Datatables::of($anggota)
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $role = Permission::checkRole(
            "superadmin", "admin_pp", "admin_pj",
            "admin_pc", "admin_pd", "admin_pw");

        abort_if(!$role || !isset($request->id),
            403, "Unauthorized to edit data");

        $caang = Caang::with(
            "keluarga",
            "pendidikan",
            "organisasi",
            "pimpinanCabang",
            "pimpinanDaerah",
            "pimpinanWilayah",
            "village"
        )->find($request->id);

        if (!$caang) {
            abort(404, "Data tidak ditemukan");
        }

        $data["anggota"] = $caang;
        $listPendidikan = MasterPendidikan::all();

        $provinces = Province::all();
        $dataPW = PW::all();
        // $dataPC = PC::all();

        return view(
            "ana.caang.edit",
            $data,
            compact("listPendidikan", "provinces", "dataPW")
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function editProfile(Request $request)
    {
        $id = $request->id;

        $data["anggota"] = Anggota::with(
            "keluarga",
            "pendidikan",
            "organisasi",
            "tafiq",
            "pimpinanCabang",
            "pimpinanDaerah",
            "pimpinanWilayah",
            "village"
        )->find($id);
        $listPendidikan = MasterPendidikan::all();

        $provinces = Province::all();
        $dataPW = PW::all();

        $authAnggota = $this->getAuthAnggota();

        if (Permission::checkPermission("anggota.update_all")) {
            return view(
                "ana.anggota.edit-profile",
                $data,
                compact("listPendidikan", "provinces", "dataPW")
            );
        } elseif (
            Permission::checkPermission("anggota.update_pw") &&
            $authAnggota->pw == $data["anggota"]->pw
        ) {
            return view(
                "ana.anggota.edit-profile",
                $data,
                compact("listPendidikan", "provinces", "dataPW")
            );
        } elseif (
            Permission::checkPermission("anggota.update_pd") &&
            $authAnggota->pd == $data["anggota"]->pd
        ) {
            return view(
                "ana.anggota.edit-profile",
                $data,
                compact("listPendidikan", "provinces", "dataPW")
            );
        } elseif (
            Permission::checkPermission("anggota.update_pc") &&
            $authAnggota->pc == $data["anggota"]->pc
        ) {
            return view(
                "ana.anggota.edit-profile",
                $data,
                compact("listPendidikan", "provinces", "dataPW")
            );
        } elseif (
            Permission::checkPermission("anggota.update_pj") &&
            $authAnggota->pj == $data["anggota"]->pj
        ) {
            return view(
                "ana.anggota.edit-profile",
                $data,
                compact("listPendidikan", "provinces", "dataPW")
            );
        } elseif ($id == $authAnggota->id_anggota) {
            return view(
                "ana.anggota.edit-profile",
                $data,
                compact("listPendidikan", "provinces", "dataPW")
            );
        }

        abort(403, "Unauthorized to edit data");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        preg_match('/ to /', $request->reg_date, $is_date_range); //2022-01-01 to 2022-01-02
        $pisah = explode("-", $request->reg_date);
        if (count($is_date_range) > 0) {
            $pisahTo = explode("to", $pisah[2]);
            $pisahToSpace1 = trim($pisahTo[0]);
            $pisahToSpace2 = trim($pisahTo[1]);
            $from = $pisah[0] . '-' . $pisah[1] . '-' . $pisahToSpace1;
            $to = $pisahToSpace2 . '-' . $pisah[3] . '-' . $pisah[4];
        } else {
            $from = $pisah[0] . '-' . $pisah[1] . '-' . $pisah[2];
            $to = $pisah[0] . '-' . $pisah[1] . '-' . $pisah[2];
        }
        $strTimeFrom = strtotime($from);
        $strTimeTo = strtotime($to);
        $newformat1 = date('Y-m-d', $strTimeFrom);
        $newformat2 = date('Y-m-d', $strTimeTo);

        $id = $request->id;
        $data = $request->all();
        $dataCaang = $request->except([
            '_token', '_method', 'keluarga_hubungan', 'keluarga_nama_keluarga',
            'keluarga_status_anggota', 'keluarga_jumlah_anak', 'keluarga_keterangan',
            'keluarga_alamat', 'pendidikan_id_master_pendidikan', 'pendidikan_jenis_pendidikan',
            'pendidikan_instansi', 'pendidikan_jurusan', 'pendidikan_tahun_masuk',
            'pendidikan_tahun_keluar', 'organisasi_nama_organisasi', 'organisasi_jabatan',
            'organisasi_tingkat', 'organisasi_lokasi', 'organisasi_tahun_mulai',
            'organisasi_tahun_selesai', 'id_caang', 'id', 'status_aktif', 'namaPW',
            'namaPD', 'namaPC', 'namaPJ', 'foto-upload', 'namaDesa', 'reg_date'
        ]);


        $request->validate([
            "nama_lengkap" => "required|max:50",
            "tempat_lahir" => "required|max:50",
            "tanggal_lahir" => "required",
            "foto-upload" => "max:2048|mimes:jpeg,jpg,png",
            "pekerjaan" => "required|max:25",
            "pj" => "nullable|max:50",
            "gol_darah" => "nullable|max:25",
            "email" => "required|max:50",
            "no_telpon" => "required|max:15",
            "no_telpon2" => "nullable|max:15",
            "alamat" => "required|max:100",
        ]);

        $caang = Caang::where("id_caang", $id)->first();
        $dataCaang["tanggal_maruf_start"] = $newformat1;
        $dataCaang["tanggal_maruf_end"] = $newformat2;

        if (!$caang) {
            return response()->json(["message" => "Not found data"], 404);
        }

        $anggotaCaang = Caang::select(
            "nama_lengkap",
            "foto"
        )
            ->where("id_caang", $request->id)
            ->first();

        if ($anggotaCaang->foto) {
            $anggotaBefore = $anggotaCaang->foto;
            Storage::disk('s3')->delete('images/anggota/' . $anggotaBefore);

            $caangBefore = $anggotaCaang->foto;
            Storage::disk('s3')->delete('images/caang/' . $caangBefore);
        }

        $dirCaang = "images/caang/";
        $dirAnggota = "images/anggota/";

        if ($request->foto) {
            $file_base64 = $request->foto;
            $filename =
                "caang_update_" . $request->Input("nama_lengkap") .
                "_" .
                date("dmY_His") .
                "." .
                "png";
            $this->uploadImageFromBase64toAWS($file_base64, $dirCaang, $filename);
            $this->uploadImageFromBase64toAWS($file_base64, $dirAnggota, $filename);
            $dataCaang["foto"] = $filename;
        } else {
            $dataCaang["foto"] = $caang->foto;
        }

        $colKeluarga = [];
        $colPendidikan = [];
        $colOrganisasi = [];
        $colTafiq = [];

        foreach ($data as $key => $value) {
            if (strpos($key, "keluarga_") !== false) {
                $colKeluarga[substr($key, strlen("keluarga_"))] = $value;
            }
            if (strpos($key, "pendidikan_") !== false) {
                $colPendidikan[substr($key, strlen("pendidikan_"))] = $value;
            }
            if (strpos($key, "organisasi_") !== false) {
                $colOrganisasi[substr($key, strlen("organisasi_"))] = $value;
            }
            if (strpos($key, "tafiq_") !== false) {
                $colTafiq[substr($key, strlen("tafiq_"))] = $value;
            }
        }

        $arrKeluarga = [];
        foreach ($colKeluarga as $key => $val) {
            $arr = [];
            if (count($arrKeluarga) == 0) {
                foreach ($val as $k => $v) {
                    $arrKeluarga[$k] = [];
                }
            }
            foreach ($val as $k => $v) {
                $arr[$key] = $v;
                $arrKeluarga[$k] = Arr::add($arrKeluarga[$k], $key, $v);
            }
        }

        $arrPendidikan = [];
        foreach ($colPendidikan as $key => $val) {
            $arr = [];
            if (is_array($val)) {
                if (count($arrPendidikan) == 0) {
                    foreach ($val as $k => $v) {
                        $arrPendidikan[$k] = [];
                    }
                }
                foreach ($val as $k => $v) {
                    $arr[$key] = $v;
                    $arrPendidikan[$k] = Arr::add($arrPendidikan[$k], $key, $v);
                }
            }
        }

        $arrOrganisasi = [];
        foreach ($colOrganisasi as $key => $val) {
            $arr = [];
            if (count($arrOrganisasi) == 0) {
                foreach ($val as $k => $v) {
                    $arrOrganisasi[$k] = [];
                }
            }
            foreach ($val as $k => $v) {
                $arr[$key] = $v;
                $arrOrganisasi[$k] = Arr::add($arrOrganisasi[$k], $key, $v);
            }
        }

        $arrTafiq = [];
        foreach ($colTafiq as $key => $val) {
            $arr = [];
            if (count($arrTafiq) == 0) {
                foreach ($val as $k => $v) {
                    $arrTafiq[$k] = [];
                }
            }
            foreach ($val as $k => $v) {
                $arr[$key] = $v;
                $arrTafiq[$k] = Arr::add($arrTafiq[$k], $key, $v);
            }
        }

        try {
            DB::transaction(function () use ($dataCaang, $caang, $arrKeluarga, $arrPendidikan, $arrOrganisasi) {
                $caang->update($dataCaang);
                $caang->keluarga()->delete();
                $caang->keluarga()->createMany($arrKeluarga);
                $caang->pendidikan()->delete();
                $caang->pendidikan()->createMany($arrPendidikan);
                $caang->organisasi()->delete();
                $caang->organisasi()->createMany($arrOrganisasi);
            });
            return response()->json([
                "message" => "Success updating data",
                "data" => 1,
            ]);
        } catch (Throwable $e) {
            DB::rollBack();
            return response()->json(
                [
                    "status" => "error",
                    "message" => $e->getMessage(),
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function delete(Request $request)
    {
        if (!Permission::checkPermission("caang.delete")) {
            return response()->json(
                ["message" => "Unauthorized to delete data"],
                403
            );
        }

        $id = $request->id;
        $deleteCalonAnggota = Caang::where("id_caang", $id)->first();
        if (!$deleteCalonAnggota) {
            abort(404);
        }
        $deleteCalonAnggota->delete();
        if ($deleteCalonAnggota) {
            return response()->json([
                "message" => "Success deleting data",
                "data" => $deleteCalonAnggota,
            ]);
        } else {
            return response()->json(["message" => "Failed deleting data"], 500);
        }
    }

    public function dataTablesTrashAnggota()
    {
        $authAnggota = Auth::user()->anggota;
        $anggota = Anggota::with("pimpinanCabang")
            ->onlyTrashed()
            ->orderBy("id_anggota", "asc");

        if (
            Permission::checkRole("admin_pj") ||
            Permission::checkRole("tasykil_pj")
        ) {
            $anggota->where("pj", $authAnggota->pj);
        } elseif (
            Permission::checkRole("admin_pc") ||
            Permission::checkRole("tasykil_pc")
        ) {
            $anggota->where("pc", $authAnggota->pc);
        } elseif (
            Permission::checkRole("admin_pd") ||
            Permission::checkRole("tasykil_pd")
        ) {
            $pd = PD::where("kd_pd", $authAnggota->pd)
                ->with("relatedPC")
                ->get();

            $listPC = $pd
                ->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })
                ->collapse();

            $anggota->whereIn("pc", $listPC);
        } elseif (
            Permission::checkRole("admin_pw") ||
            Permission::checkRole("tasykil_pw")
        ) {
            $relatedPD = PW::where("kd_pw", $authAnggota->pw)
                ->first()
                ->relatedPD->pluck("kd_pd");

            $pd = PD::whereIn("kd_pd", $relatedPD)
                ->with("relatedPC")
                ->get();

            $listPC = $pd
                ->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })
                ->collapse();

            $anggota->whereIn("pc", $listPC);
        }
        return Datatables::of($anggota)
            ->addIndexColumn()
            ->make(true);
    }

    public function trash()
    {
        return view("ana.anggota.trash");
    }

    public function restore($id)
    {
        $restoreAnggota = Anggota::withTrashed()
            ->find($id)
            ->restore();
        if ($restoreAnggota) {
            return response()->json([
                "message" => "Success restore data",
                "data" => $restoreAnggota,
            ]);
        } else {
            return response()->json(["message" => "Failed restore data"], 500);
        }
    }

    public function destroy($id)
    {
        $destroyAnggota = Anggota::withTrashed()
            ->find($id)
            ->forceDelete();
        if ($destroyAnggota) {
            return response()->json([
                "message" => "Success restore data",
                "data" => $destroyAnggota,
            ]);
        } else {
            return response()->json(["message" => "Failed restore data"], 500);
        }
    }

    public function saveAnggotaOrganisasi($data, $id)
    {
        if (
            $data["nama_organisasi_1"] != null &&
            $data["tahun_selesai_org_1"] != null &&
            $data["tahun_mulai_org_1"] != null
        ) {
            $org1 = new AnggotaOrganisasi();
            $org1->nama_organisasi = $data["nama_organisasi_1"];
            $org1->jabatan = $data["jabatan_org_1"];
            $org1->tahun_mulai = $data["tahun_mulai_org_1"];
            $org1->tahun_selesai = $data["tahun_selesai_org_1"];
            $org1->lokasi = $data["lokasi_org_1"];
            $org1->id_anggota = $id;
            $org1->save();
        }

        if (
            $data["nama_organisasi_2"] != null &&
            $data["tahun_selesai_org_2"] != null &&
            $data["tahun_mulai_org_2"] != null
        ) {
            $org2 = new AnggotaOrganisasi();
            $org2->nama_organisasi = $data["nama_organisasi_2"];
            $org2->jabatan = $data["jabatan_org_2"];
            $org2->tahun_mulai = $data["tahun_mulai_org_2"];
            $org2->tahun_selesai = $data["tahun_selesai_org_2"];
            $org2->lokasi = $data["lokasi_org_2"];
            $org2->id_anggota = $id;
            $org2->save();
        }

        if (
            $data["nama_organisasi_3"] != null &&
            $data["tahun_selesai_org_3"] != null &&
            $data["tahun_mulai_org_3"] != null
        ) {
            $org3 = new AnggotaOrganisasi();
            $org3->nama_organisasi = $data["nama_organisasi_3"];
            $org3->jabatan = $data["jabatan_org_3"];
            $org3->tahun_mulai = $data["tahun_mulai_org_3"];
            $org3->tahun_selesai = $data["tahun_selesai_org_3"];
            $org3->lokasi = $data["lokasi_org_3"];
            $org3->id_anggota = $id;
            $org3->save();
            return true;
        }
    }

    public function saveAnggotaTafiq($data, $id)
    {
        if ($data["tafiq_mulai1"] != null && $data["tafiq_sampai1"] != null) {
            $tafiq1 = new AnggotaTafiq();
            $tafiq1->id_anggota = $id;
            $tafiq1->tanggal_masuk = $data["tafiq_mulai1"];
            $tafiq1->tanggal_selesai = $data["tafiq_sampai1"];
            $tafiq1->lokasi = $data["tafiq_lokasi1"];
            $tafiq1->save();
        }

        if ($data["tafiq_mulai2"] != null && $data["tafiq_sampai2"] != null) {
            $tafiq2 = new AnggotaTafiq();
            $tafiq2->id_anggota = $id;
            $tafiq2->tanggal_masuk = $data["tafiq_mulai2"];
            $tafiq2->tanggal_selesai = $data["tafiq_sampai2"];
            $tafiq2->lokasi = $data["tafiq_lokasi2"];
            $tafiq2->save();
        }

        if ($data["tafiq_mulai3"] != null && $data["tafiq_sampai3"] != null) {
            $tafiq3 = new AnggotaTafiq();
            $tafiq3->id_anggota = $id;
            $tafiq3->tanggal_masuk = $data["tafiq_mulai3"];
            $tafiq3->tanggal_selesai = $data["tafiq_sampai3"];
            $tafiq3->lokasi = $data["tafiq_lokasi3"];
            $tafiq3->save();
        }

        return true;
    }

    public function saveAnggotaPendidikan($data, $id)
    {
        $sd = new AnggotaPendidikan();
        $sd->id_anggota = $id;
        $sd->id_master_pendidikan = 2;
        $sd->instansi = $data["nama_sd"];
        $sd->jurusan = null;
        $sd->tahun_masuk = $data["tahun_masuk_sd"];
        $sd->tahun_keluar = $data["tahun_keluar_sd"];
        $sd->jenis_pendidikan = "Formal";
        $sd->save();

        $smp = new AnggotaPendidikan();
        $smp->id_anggota = $id;
        $smp->id_master_pendidikan = 1;
        $smp->instansi = $data["nama_smp"];
        $smp->jurusan = null;
        $smp->tahun_masuk = $data["tahun_masuk_smp"];
        $smp->tahun_keluar = $data["tahun_keluar_smp"];
        $smp->jenis_pendidikan = "Formal";
        $smp->save();

        if (
            $data["nama_sma"] != null &&
            $data["tahun_masuk_sma"] != null &&
            $data["tahun_keluar_sma"] != null
        ) {
            $sma = new AnggotaPendidikan();
            $sma->id_anggota = $id;
            $sma->id_master_pendidikan = 3;
            $sma->instansi = $data["nama_sma"];
            $sma->jurusan = null;
            $sma->tahun_masuk = $data["tahun_masuk_sma"];
            $sma->tahun_keluar = $data["tahun_keluar_sma"];
            $sma->jenis_pendidikan = "Formal";
            $sma->save();
        }

        if (
            $data["nama_d1"] != null &&
            $data["tahun_masuk_d1"] != null &&
            $data["tahun_keluar_d1"] != null
        ) {
            $d1 = new AnggotaPendidikan();
            $d1->id_anggota = $id;
            $d1->id_master_pendidikan = 4;
            $d1->instansi = $data["nama_d1"];
            $d1->jurusan = $data["jurusan_d1"];
            $d1->tahun_masuk = $data["tahun_masuk_d1"];
            $d1->tahun_keluar = $data["tahun_keluar_d1"];
            $d1->jenis_pendidikan = "Formal";
            $d1->save();
        }

        if (
            $data["nama_d2"] != null &&
            $data["tahun_masuk_d2"] != null &&
            $data["tahun_keluar_d2"]
        ) {
            $d2 = new AnggotaPendidikan();
            $d2->id_anggota = $id;
            $d2->id_master_pendidikan = 5;
            $d2->instansi = $data["nama_d2"];
            $d2->jurusan = $data["jurusan_d2"];
            $d2->tahun_masuk = $data["tahun_masuk_d2"];
            $d2->tahun_keluar = $data["tahun_keluar_d2"];
            $d2->jenis_pendidikan = "Formal";
            $d2->save();
        }

        if (
            $data["nama_d3"] != null &&
            $data["tahun_masuk_d3"] != null &&
            $data["tahun_keluar_d3"]
        ) {
            $d3 = new AnggotaPendidikan();
            $d3->id_anggota = $id;
            $d3->id_master_pendidikan = 5;
            $d3->instansi = $data["nama_d3"];
            $d3->jurusan = $data["jurusan_d3"];
            $d3->tahun_masuk = $data["tahun_masuk_d3"];
            $d3->tahun_keluar = $data["tahun_keluar_d3"];
            $d3->jenis_pendidikan = "Formal";
            $d3->save();
        }

        if (
            $data["nama_s1"] != null &&
            $data["tahun_masuk_s1"] != null &&
            $data["tahun_keluar_s1"] != null
        ) {
            $s1 = new AnggotaPendidikan();
            $s1->id_anggota = $id;
            $s1->id_master_pendidikan = 5;
            $s1->instansi = $data["nama_s1"];
            $s1->jurusan = $data["jurusan_s1"];
            $s1->tahun_masuk = $data["tahun_masuk_s1"];
            $s1->tahun_keluar = $data["tahun_keluar_s1"];
            $s1->jenis_pendidikan = "Formal";
            $s1->save();
        }

        if (
            $data["nama_s2"] != null &&
            $data["tahun_masuk_s2"] != null &&
            $data["tahun_keluar_s2"] != null
        ) {
            $s2 = new AnggotaPendidikan();
            $s2->id_anggota = $id;
            $s2->id_master_pendidikan = 5;
            $s2->instansi = $data["nama_s2"];
            $s2->jurusan = $data["jurusan_s2"];
            $s2->tahun_masuk = $data["tahun_masuk_s2"];
            $s2->tahun_keluar = $data["tahun_keluar_s2"];
            $s2->jenis_pendidikan = "Formal";
            $s2->save();
        }

        if (
            $data["nama_s3"] != null &&
            $data["tahun_masuk_s3"] != null &&
            $data["tahun_keluar_d3"] != null
        ) {
            $s3 = new AnggotaPendidikan();
            $s3->id_anggota = $id;
            $s3->id_master_pendidikan = 5;
            $s3->instansi = $data["nama_s3"];
            $s3->jurusan = $data["jurusan_s3"];
            $s3->tahun_masuk = $data["tahun_masuk_s3"];
            $s3->tahun_keluar = $data["tahun_keluar_s3"];
            $s3->jenis_pendidikan = "Formal";
            $s3->save();
        }

        return true;
    }

    public function saveAnggotaKeluarga($data, $id)
    {
        $istri = new AnggotaKeluarga();
        $istri->nama_keluarga = $data["nama_istri"];
        $istri->hubungan = "Istri";
        $istri->hubungan = "Istri";
        $istri->id_anggota = $id;
        $istri->jumlah_anak = $data["jumlah_anak"];
        $istri->alamat = $data["alamat"];
        $istri->status_anggota = $data["status_istri"];
        $istri->keterangan = $data["keterangan"];
        $istri->save();

        $ayah = new AnggotaKeluarga();
        $ayah->nama_keluarga = $data["nama_ayah"];
        $ayah->hubungan = "Ayah";
        $ayah->alamat = $data["alamat_orangtua"];
        $ayah->id_anggota = $id;
        $ayah->status_anggota = $data["status_ayah"];
        $ayah->save();

        $ibu = new AnggotaKeluarga();
        $ibu->nama_keluarga = $data["nama_ibu"];
        $ibu->hubungan = "Ibu";
        $ibu->alamat = $data["alamat_orangtua"];
        $ibu->id_anggota = $id;
        $ibu->status_anggota = $data["status_ibu"];
        $ibu->save();

        return true;
    }

    public function npa(Request $request)
    {
        $data = Anggota::where("npa", $request->input("npa"));
        if ($request->input("id")) {
            $data->where("id_anggota", "!=", $request->input("id"));
        }
        return $data->count() > 0 ? "false" : "true";
    }

    public function email(Request $request)
    {
        $data = Anggota::where("email", $request->input("email"));
        if ($request->input("id")) {
            $data->where("id_anggota", "!=", $request->input("id"));
        }
        return $data->count() > 0 ? "false" : "true";
    }

    public function exportTemplate()
    {
        return Excel::download(new AnggotaTable(), "anggota.xlsx");
    }

    public function showImport()
    {
        return view("ana.excel.import");
    }

    public function import(Request $request)
    {
        $file = $request->file("file");
        Excel::import(new AnggotaImport(), $file);

        return back()->withStatus("File excel berhasil diupload");
    }

    public function exportDataAnggota(Request $request)
    {
        try {
            $url = env("S3_STATIC_FOLDER") . "excel-anggota-template.xlsx";
            return response(file_get_contents($url))
                ->header("Content-Type", "application/vnd.ms-excel")
                ->header(
                    "Content-Disposition",
                    "attachment;filename=ExportData.xlsx"
                )
                ->header("Cache-Control", "max-age=0");
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * uploadImageFromBase64toAWS the specified resource.
     *
     * @param string $file_base64
     * @param string $dir
     * @param string $filename
     * @return Response
     */
    public function uploadImageFromBase64toAWS($file_base64, $dir, $filename)
    {
        Storage::disk("s3")->put(
            $dir . $filename,
            file_get_contents($file_base64)
        );
    }

    /**
     * profileCard the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function profileCard(Request $request)
    {
        $id = $request->id;
        $data["anggota"] = Anggota::with(
            "pimpinanCabang",
            "pimpinanDaerah",
            "pimpinanWilayah",
            "keluarga",
            "keterampilan",
            "mutasi",
            "organisasi",
            "pekerjaan",
            "pendidikan",
            "training",
            "village"
        )
            ->where("id_anggota", $id)
            ->first();
        if (!$data["anggota"]) {
            abort(404);
        }
        $roles = UserLogin::where("npa", $data["anggota"]->npa)->first();
        if (is_null($roles)) {
            $data["roles"] = $roles;
        } else {
            $data["roles"] = $roles->roles;
        }
        return view("ana.anggota.kta", $data);
    }

    public function templateCard(Request $request)
    {
        $id = $request->id;
        $data["anggota"] = Anggota::where("id_anggota", $id)->first();
        if (!$data["anggota"]) {
            abort(404);
        }
        $roles = UserLogin::where("npa", $data["anggota"]->npa)->first();
        if (is_null($roles)) {
            $data["roles"] = $roles;
        } else {
            $data["roles"] = $roles->roles;
        }
        return view("ana.anggota.e-kta.template", $data);
    }

    public function getImageProfile(Request $request)
    {
        $id = $request->id;
        $anggota = Anggota::with("village")
            ->where("id_anggota", $id)
            ->first();
        if (!$anggota) {
            abort(404);
        }
        $roles = UserLogin::where("npa", $anggota->npa)->first();
        if (is_null($roles)) {
            $data["roles"] = $roles;
        } else {
            $data["roles"] = $roles->roles;
        }

        $pathToFile = Storage::disk("s3")->get(
            "images/caang/" . $anggota->foto
        );

        $base64 = "data:image/png;base64," . base64_encode($pathToFile);
        return $base64;
    }

    /**
     * Update the specified anggota photo profile.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function updateImageProfile(Request $request)
    {
        $id = $request->id;
        $request->validate([
            "foto-upload" => "max:2048|mimes:jpeg,jpg,png",
        ]);

        try {
            $anggota = Caang::select(
                "nama_lengkap",
                "foto"
            )
                ->where("id_caang", $id)
                ->first();
            if ($anggota->foto) {
                $beforeAnggota = $anggota->foto;
                Storage::disk('s3')->delete('images/anggota/' . $beforeAnggota);

                $caangBefore = $anggota->foto;
                Storage::disk('s3')->delete('images/caang/' . $caangBefore);
            }

            $dir = "images/caang/";
            $dirAnggota = "images/anggota/";

            $file_base64 = $request->foto;
            $filename =
                "caang_" . str_replace(" ", "_", $anggota->nama_lengkap) . "_" . date("dmY_His");
            $this->uploadImageFromBase64toAWS($file_base64, $dir, $filename);
            $this->uploadImageFromBase64toAWS($file_base64, $dirAnggota, $filename);


            $newAnggota = Caang::where("id_caang", $id)->first();
            $newAnggota->update(["foto" => $filename]);
        } catch (Throwable $e) {
            DB::rollBack();
            return response()->json(
                [
                    "status" => "error",
                    "message" => $e->getMessage(),
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        return response()->json([
            "message" => "Foto Profile successfully updated",
            "data" => 1,
        ]);
    }

    public function viewPublicForm()
    {
        return view("ana.caang.form-public");
    }

    public function setApproveCaangLevelPW(Request $request)
    {
        $id_caang_array = $request->json()->all();
        $pw = Auth::user()->anggota->pw;

        try {
            $status = Caang::whereIn("id_caang", $id_caang_array)->update([
                "status" => "APPROVED_BY_PW",
                "pw" => $pw,
                "last_updated" => Carbon::now(),
            ]);

            if ($status) {
                return response()->json(
                    [
                        "message" => "success",
                        "code" => ResponseAlias::HTTP_ACCEPTED,
                    ],
                    ResponseAlias::HTTP_ACCEPTED
                );
            } else {
                throw new Exception("Error Approving Data");
            }
        } catch (Exception $e) {
            return response()->json(
                [
                    "message" => $e->getMessage(),
                    "code" => ResponseAlias::HTTP_INTERNAL_SERVER_ERROR,
                ],
                ResponseAlias::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function setApproveCaangLevelPD(Request $request)
    {
        $id_caang_array = $request->json()->all();
        $pd = Auth::user()->anggota->pd;

        try {
            $status = Caang::whereIn("id_caang", $id_caang_array)->update([
                "status" => "APPROVED_BY_PD",
                "pd" => $pd,
                "last_updated" => Carbon::now(),
            ]);
            if ($status) {
                return response()->json(
                    [
                        "message" => "success",
                        "code" => ResponseAlias::HTTP_ACCEPTED,
                    ],
                    ResponseAlias::HTTP_ACCEPTED
                );
            } else {
                throw new Exception("Error Approving Data");
            }
        } catch (Exception $e) {
            return response()->json(
                [
                    "message" => $e->getMessage(),
                    "code" => ResponseAlias::HTTP_INTERNAL_SERVER_ERROR,
                ],
                ResponseAlias::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function setApproveCaangLevelPC(Request $request)
    {
        $id_caang_array = $request->json()->all();
        $pc = Auth::user()->anggota->pc;

        try {
            $status = Caang::whereIn("id_caang", $id_caang_array)->update([
                "status" => "APPROVED_BY_PC",
                "pc" => $pc,
                "last_updated" => Carbon::now(),
            ]);

            if ($status) {
                return response()->json(
                    [
                        "message" => "success",
                        "code" => ResponseAlias::HTTP_ACCEPTED,
                    ],
                    ResponseAlias::HTTP_ACCEPTED
                );
            } else {
                throw new Exception("Error Approving Data");
            }
        } catch (Exception $e) {
            return response()->json(
                [
                    "message" => $e->getMessage(),
                    "code" => ResponseAlias::HTTP_INTERNAL_SERVER_ERROR,
                ],
                ResponseAlias::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function checkLastNPA($data)
    {
        if ($data["last_npa"]->isEmpty()) {
            DB::table("t_temp_last_npa")->insert([
                "npa" => date("y") . "." . sprintf("%04d", 0),
                "year" => date("Y"),
                "prefix" => date("y"),
                "suffix" => sprintf("%04d", 0),
                "counter" => 0,
            ]);
            $data["last_npa"] = DB::table("t_temp_last_npa")
                ->get()
                ->take(1);
        }
    }

    public function setApproveCaang(Request $request)
    {
        $data = [];
        $data["id_caang_array"] = $request->json()->all();
        $data["last_npa"] = DB::table("t_temp_last_npa")->get();
        $data["caang"] = Caang::whereIn(
            "id_caang",
            $data["id_caang_array"]
        )->get();

        try {
            $this->checkLastNPA($data);
            $status = Caang::whereIn("id_caang", $data["id_caang_array"])->update([
                "status" => "APPROVED_BY_PP",
                "last_updated" => Carbon::now(),
            ]);
            ApprovalCaangProcessor::dispatch($data);

            // $dataCaang = Caang::whereIn("id_caang", $data["id_caang_array"])->get();
            // foreach ($dataCaang as $caang) {
            //     $this->sendApprovalMessage($caang, $data);
            // }

            $status = Caang::whereIn("id_caang", $data["id_caang_array"])->delete();

            if ($status > 0) {
                return response()->json(
                    [
                        "message" => "success",
                        "code" => ResponseAlias::HTTP_ACCEPTED,
                    ],
                    ResponseAlias::HTTP_ACCEPTED
                );
            } else {
                throw new Exception("Error Approving Data");
            }
        } catch (Exception $e) {
            return response()->json(
                [
                    "message" => $e->getMessage() . $e->getLine(),
                    "code" => ResponseAlias::HTTP_INTERNAL_SERVER_ERROR,
                ],
                ResponseAlias::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
