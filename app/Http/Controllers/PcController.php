<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\PC;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ana.pc.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = PC::create($request->all());

        if ($data) {
            return response()->json(['message' => 'Success adding data', 'data' => 1]);
        } else {
            return response()->json(['message' => 'Failed adding data'], 500);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function datatables(){
        $pc = PC::with(['pd', 'pd.pw'])
            ->get();
        $has_permission = Permission::checkPermission('masterpc.update') || Permission::checkPermission('masterpc.delete');
        return Datatables::of($pc)
        ->addIndexColumn()
        ->addColumn('has_permission', $has_permission)
        ->addColumn('action', function ($pc){
            $action = '<div class="btn-group float-right">';
            if(Permission::checkPermission('masterpc.update')){
                $action .= '<button type="button" onclick="openModal(\'#modal-form\', \'update\',\''. $pc->kd_pc .'\')" class="btn btn-warning btn-sm"><i class="si si-pencil"></i></button>';
            }
            if(Permission::checkPermission('masterpc.delete')){
                $action .= '<button type="button" class="btn btn-danger btn-sm" onclick="remove(\'' . $pc->kd_pc . '\')" ><i class="si si-trash"></i></button>';
            }
            $action .='</div>';
            return $action;
        })
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pc = PC::find($id);

    }

    public function get($id)
    {
        $dataPc= PC::with(['pd', 'pd.pw'])
            ->where('kd_pc', $id)
            ->first();

        if (!$dataPc) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json(['message' => 'Success getting data', 'data' => $dataPc]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $mutasi = PC::where('kd_pc', $data['id_prev'])
        ->first();

        unset($data['id_prev']);

        if(!$mutasi) {
            return response()->json(['message' => 'Not found data'], 404);
        }else{
            $mutasi->kd_pc = $data['kd_pc'];
            $mutasi->kd_pd = $data['kd_pd'];
            $mutasi->nama_pc = $data['nama_pc'];
            $mutasi->diresmikan = $data['diresmikan'];
            $mutasi->save();
            return response()->json(['message' => 'Success updating data', 'data' => 1]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $destroyData = PC::where('kd_pc', $id)->delete();
        // $destroyData->delete($id);
        if ($destroyData) {
            return response()->json(['message' => 'Success deleting data', 'data' => $destroyData]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }
    }
}
