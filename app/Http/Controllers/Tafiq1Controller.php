<?php

namespace App\Http\Controllers;

use App\Facades\AnaOnline;
use App\Facades\Permission;
use App\Jobs\ApprovalTafiq1Processor;
use App\Mail\TafiqRegistrationProcessing;
use App\Model\Anggota;
use App\Model\EsyahadahTafiq1;
use App\Model\PD;
use App\Model\PW;
use App\Model\Tafiq1;
use App\Rules\CheckTafiqNpa;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Yajra\DataTables\Facades\DataTables;

class Tafiq1Controller extends Controller
{
    public function index()
    {
        return view("ana.tafiq1.index");
    }

    public function indexEdit($id)
    {
        if (!Permission::checkPermission("tafiq1.update")) {
            throw new Exception("unauthorized action", ResponseAlias::HTTP_UNAUTHORIZED);
        }
        $tafiq1 = Tafiq1::findOrFail($id);
        return view("ana.tafiq1.edit", ["data" => $tafiq1]);
    }

    public function updateData(Request $request)
    {
        try {
            if (!Permission::checkPermission("tafiq1.update")) {
                throw new Exception("unauthorized action", ResponseAlias::HTTP_UNAUTHORIZED);
            }
            $id = $request->id;
            $form = $request->except(["id", "_method", "npa"]);
            if(!isset($request->tanggal_tafiq_start)) unset($form['tanggal_tafiq_start']);
            if(!isset($request->tanggal_tafiq_end)) unset($form['tanggal_tafiq_end']);
            $tafiq1 = Tafiq1::findOrFail($id);
            $status = $tafiq1->update($form);

            if($status){
                return response()->json([
                    "message" => "sucessfully deleted",
                    "code" => ResponseAlias::HTTP_NO_CONTENT
                ], ResponseAlias::HTTP_NO_CONTENT);
            }

        } catch (Exception $e) {
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], $e->getCode() === 0 ? ResponseAlias::HTTP_INTERNAL_SERVER_ERROR : $e->getCode());
        }
    }

    public function viewData()
    {
        $data["role"] = Auth::user()
        ->roles->pluck("name")
        ->toArray();
        $data["admin_pc"] = "admin_pc";
        $data["admin_pd"] = "admin_pd";
        $data["admin_pw"] = "admin_pw";
        $data["admin_pp"] = "admin_pp";
        $data["superadmin"] = "superadmin";
        return view("ana.tafiq1.datatable_index", $data);
    }

    public function delete(Request $request)
    {
        try {
            if (!Permission::checkPermission("tafiq1.delete")) {
                throw new Exception("unauthorized action", ResponseAlias::HTTP_UNAUTHORIZED);
            }
            $id = $request->id;
            $tafiq1 = Tafiq1::findOrFail($id);
            $status = $tafiq1->delete();

            if($status){
                return response()->json([
                    "message" => "sucessfully deleted",
                    "code" => ResponseAlias::HTTP_NO_CONTENT
                ], ResponseAlias::HTTP_NO_CONTENT);
            }

        } catch (Exception $e) {
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], $e->getCode() === 0 ? ResponseAlias::HTTP_INTERNAL_SERVER_ERROR : $e->getCode());
        }

    }

    public function datatable()
    {
        $authAnggota = Auth::user()->anggota;
        $statusApproval = request('status_approval');
        $anggota = Tafiq1::with('anggota','anggota.pimpinanWilayah',
            'anggota.pimpinanDaerah','anggota.pimpinanCabang')
            ->orderBy('created_at', "DESC")
            ->distinct()
            ->groupBy('id');

        $permissionDelete = Permission::checkPermission("tafiq1.delete");
        $permissionEdit = Permission::checkPermission("tafiq1.update");
        if (
            Permission::checkRole("admin_pc") ||
            Permission::checkRole("tasykil_pc")
        ) {
            $anggota->whereHas("anggota", function($q) use($authAnggota){
                $q->where('pc', $authAnggota->pc);
            });
            $anggota->where("status", "INIT");
        } elseif (
            Permission::checkRole("admin_pd") ||
            Permission::checkRole("tasykil_pd")
        ) {
            $pd = PD::where("kd_pd", $authAnggota->pd)
                ->with("relatedPC")
                ->get();

            $listPC = $pd
                ->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })
                ->collapse();

            $anggota->whereHas("anggota", function($q) use($listPC){
                $q->whereIn("pc", $listPC);
            });

            $anggota->where("status", "APPROVED_BY_PC");
        } elseif (
            Permission::checkRole("admin_pw") ||
            Permission::checkRole("tasykil_pw")
        ) {
            $relatedPD = PW::where("kd_pw", $authAnggota->pw)
                ->first()
                ->relatedPD->pluck("kd_pd");

            $pd = PD::whereIn("kd_pd", $relatedPD)
                ->with("relatedPC")
                ->get();

            $listPC = $pd
                ->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })
                ->collapse();
            $anggota->whereHas("anggota", function($q) use($listPC){
                $q->whereIn("pc", $listPC);
            });
            $anggota->where("status", "APPROVED_BY_PD");
        } elseif (
            Permission::checkRole("admin_pp") ||
            Permission::checkRole("superadmin")
        ) {
            if(!is_null($statusApproval)) {
                $anggota->where("status", request('status_approval'));
            }
        }

        if (request()->has('createdStart')) {
            $date = request('createdStart');
            $anggota->where("tanggal_tafiq_start", $date);
        }

        if (request()->has('createdEnd')) {
            $date = request('createdEnd');
            $anggota->where("tanggal_tafiq_end", $date);
        }

        return DataTables::of($anggota)
            ->addIndexColumn()
            ->addColumn("linkFoto", function (Tafiq1 $tafiq1) {
                return AnaOnline::getUrlFotoAnggota($tafiq1->anggota);
            })
            ->addColumn("can_delete", function () use($permissionDelete){
                return $permissionDelete;
            })
            ->addColumn("total_nilai", function (Tafiq1 $tafiq1){
                return $tafiq1->nilai_total;
            })
            ->addColumn("can_edit", function () use($permissionEdit){
                return $permissionEdit;
            })
            ->filterColumn("anggota.pimpinan_wilayah.nama_pw", function($query, $keyword) {
                $query->whereHas("anggota.pimpinanWilayah", function ($query_anggota) use($keyword){
                    return $query_anggota->where("nama_pw", 'LIKE',  "%{$keyword}%");
                });
            })
            ->filterColumn("anggota.pimpinan_daerah.nama_pd", function($query, $keyword) {
                $query->whereHas("anggota.pimpinanDaerah", function ($query_anggota) use($keyword){
                    return $query_anggota->where("nama_pd", 'LIKE',  "%{$keyword}%");
                });
            })
            ->filterColumn("anggota.pimpinan_cabang.nama_pc", function($query, $keyword) {
                $query->whereHas("anggota.pimpinanCabang", function ($query_anggota) use($keyword){
                    return $query_anggota->where("nama_pc", 'LIKE',  "%{$keyword}%");
                });
            })
            ->make(true);
    }

    public function postData(Request $request)
    {
        $data = $request->json()->all();
        try {
            $validator = Validator::make($data, [
                "npa" => ["required", new CheckTafiqNpa($data['npa'])],
                "lokasi" => "required",
                "tanggal_tafiq_end" => "required",
                "tanggal_tafiq_start" => "required"
            ]);

            if($validator->fails()){
                throw new Exception(
                    collect($validator->errors()->all())->implode(", "), ResponseAlias::HTTP_BAD_REQUEST);
            }

            $statusData = Tafiq1::create($data);

            $anggota = Anggota::where('npa', trim($data["npa"]))->firstOrFail();
            $this->sendMessage($anggota);

            if($statusData){
                Mail::to(trim($data["email"]))->queue(
                        new TafiqRegistrationProcessing($data["npa"], $data["nama"]));
                return response()->json([
                    "message" => "success",
                    "code" => ResponseAlias::HTTP_ACCEPTED
                ], ResponseAlias::HTTP_ACCEPTED);
            }else{
                throw new Exception(
                    "Terjadi kesalahan pada sistem. Silahkan kontak Admin",
                    ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
            }

        } catch (Exception $e) {
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], $e->getCode() === 0 ? ResponseAlias::HTTP_INTERNAL_SERVER_ERROR : $e->getCode());
        }
    }

    public function viewPDF($npa)
    {
        $url = Storage::disk('s3')
            ->url("esyahadah/tafiq1/{$npa}.pdf");
        return Redirect::to($url);
    }

    public function viewEsyahadahHTML($npa)
    {
        $esyahadah = EsyahadahTafiq1::with([
            'tafiq1','tafiq1.anggota'])->whereHas('tafiq1.anggota', function($q) use($npa){
                $q->where('npa', $npa);
            })->first();
        return $esyahadah->getPDF($esyahadah->id, 'html');
    }

    public function setStatus(Request $request)
    {
        try {
            $role = Permission::checkRole("superadmin") ||
            Permission::checkRole("admin_pp") ||
            Permission::checkRole("admin_pc") ||
            Permission::checkRole("admin_pd") ||
            Permission::checkRole("admin_pw");

            if(!$role){
                throw new Exception(
                    "Tidak ada izin delete", ResponseAlias::HTTP_UNAUTHORIZED);
            }

            $data = $request->json()->all();
            $listLevel = [
                "pc" => AnaOnline::getStatusApprovalApprovedByPC(),
                "pd" => AnaOnline::getStatusApprovalApprovedByPD(),
                "pp" => AnaOnline::getStatusApprovalApprovedByPP(),
                "pw" => AnaOnline::getStatusApprovalApprovedByPW()
            ];

            $query = Tafiq1::with('anggota')->whereIn("id", $data['id']);
            $level = $data['level'];
            $query->update([
                "status" => $listLevel[$level],
                "updated_at" => Carbon::now(),
            ]);

            if($listLevel[$level] == AnaOnline::getStatusApprovalApprovedByPP()){
                $query->get()->each(function ($itemData) {
                    ApprovalTafiq1Processor::dispatch($itemData);
                });
            }

            return response()->json([
                "message" => "success",
                "data" => $query->get(),
                "code" => ResponseAlias::HTTP_ACCEPTED
            ], ResponseAlias::HTTP_ACCEPTED);

        } catch (Exception $e) {
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], $e->getCode() === 0 ? ResponseAlias::HTTP_INTERNAL_SERVER_ERROR : $e->getCode());
        }
    }

    private function sendMessage($anggota)
    {
        $token = env('PUSH_WA_TOKEN');
        $target = $anggota->no_telpon;
        $url = "https://dash.pushwa.com/api/kirimPesan";
        $link = "esyahadah/tafiq1/" . $anggota->npa . ".pdf";
        $generateLink = Storage::disk('s3')->url($link);

        $message = "_Bismillahirrahmaanirrahiim_,\n\n
        Ahlan wa sahlan bi washiyyati rosulillah akhi {$anggota->nama_lengkap} ({$anggota->npa}).\n\n
        Antum telah lulus TAFIQ 1. Kami tunggu di Halaqoh Pasca TAFIQ 1, TAFIQ 2, Halaqoh Pasca TAFIQ 2 & TAFIQ 3.\n\n
        E-Syahadah TAFIQ 1 antum bisa diunduh/download di {$generateLink}.\n\n
        Admin PP. Pemuda Persis \"Ana Muslimun Qabla Kulli Syain\"\n\n
        Dizkro:\n
        - Join telegram group --> https://t.me/csanaonline jika mengalami kendala\n
        - Join telegram group --> https://t.me/kominfopemudapersis\n
        - PLAYLIST VIDEO TUTORIAL ANAONLINE --> https://www.youtube.com/playlist?list=PLyyNZDxI4KezJiXmEJ12823pQH524Fa24";


        $response = Http::post($url, [
            "token" => $token,
            "target" => $target,
            "type" => "image",
            "delay" => "10",
            "message" => $message,
            "url" => "https://annisa-online.s3.ap-southeast-1.amazonaws.com/static/annisa_online.jpeg"
        ]);

        return $response->json();
    }
}
