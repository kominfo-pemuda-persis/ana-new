<?php

namespace App\Http\Controllers;

// use Auth;
use DataTables;
use Storage;
use DB;
use Illuminate\Http\Request;
use App\Model\RoleMaster;
use App\Model\PermissionRole;

class RoleController extends Controller{

    public function __construct()
    {
         $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ana.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        return view('ana.anggota.add');
    }

    /**
     * Show the form for assigning permission to role
     *
     * @return \Illuminate\Http\Response
     */
    public function permission(Request $request)
    {
        $id = $request->id;
        $data['role'] = RoleMaster::where('id_role', $id)->first();
        $myPermissions = PermissionRole::where('role_id', $id)->get();

        $data['permissions'] = config('permissions.module_permissions');
        $data['myPermissions'] = [];
        foreach ($myPermissions as $permission) {
            $data['myPermissions'][$permission['module']][] = $permission['permission'];
        }

        return view('ana.role.permission', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $save = null;
        if(empty($data['id_role'])){
            $save= RoleMaster::create([
                'name' => $data['name'],
                'display_name' => $data['display_name'],
                'description' => $data['description'],
                'status_aktif' => 1
            ]);
        }else{
            $save = RoleMaster::where('id_role', $data['id_role'])
                ->update([
                    'name' => $data['name'],
                    'display_name' => $data['display_name'],
                    'description' => $data['description'],
                    'status_aktif' => $data['status_aktif']
                ]);
        }

        if ($save) {
            return response()->json(['message' => 'Success saving data', 'data' => 1]);
        } else {
            return response()->json(['message' => 'Failed saving data'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return view('ana.anggota.detail');
    }

    public function get(Request $request){
        $id = $request->id;
        $role =  RoleMaster::where('id_role',$id)->first();

        if (!$role) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json(['message' => 'Success getting data', 'data' => $role]);
    }

    public function datatables(){
        $data =  RoleMaster::all();

        return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->id;
        $data['anggota'] = Anggota::find($id);

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $data = $request->all();

        $createRole= RoleMaster::where('id_role', $data['id_role'])
            ->updated([
            'name' => $data['name'],
            'display_name' => $data['display_name'],
            'description' => $data['description'],
            'status_aktif' => $data['status_aktif']
        ]);

        if ($createRole) {
            return response()->json(['message' => 'Success updating data', 'data' => 1]);
        } else {
            return response()->json(['message' => 'Failed updating data'], 500);
        }
    }

    /**
     * Post assign permission to role
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assign(Request $request)
    {
        $data = $request->all();
        $id = $request->id;

        // Clean all role permissions first
        PermissionRole::where('role_id', $id)->delete();

        // Rewrite all permissions
        unset($data['_token']);
        $rows = [];
        foreach ($data as $module => $permissions) {
            foreach ($permissions as $permission => $val) {
                $rows[] = ['role_id' => $id, 'module' => $module, 'permission' => $permission];
            }
        }
        $results = PermissionRole::insert($rows);

        if($results)
            $message = '<div class="alert alert-success">Data permission berhasil diperbaharui</div>';
        else
            $message = '<div class="alert alert-warning">Data permission gagal diperbaharui</div>';

        return redirect()->back()->with('message', $message);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $remove = RoleMaster::where('id_role', $id)->first();
        $remove->delete();

        if ($remove) {
            return response()->json(['message' => 'Success deleting data', 'data' => $remove]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }
    }
}
