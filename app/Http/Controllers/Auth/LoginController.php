<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\UserLogin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view("auth.login");
    }

    public function cek_login(Request $request)
    {
        $credentials = $request->only('npa', 'password');

        $rules = array(
            'npa'       => 'required',
            'password'         => 'required'
        );
        $msgs = [
            'npa.required' => 'NPA tidak boleh kosong',
            'password.required' => 'Password tidak boleh kosong'
        ];

        $validator = Validator::make($request->all(), $rules, $msgs);

        if (!$validator->fails()) {

            if (UserLogin::where('npa', $credentials['npa'])->first() === null) {
                return redirect('login')->with('error', "NPA tidak dikenali sistem");
            }

            if (Auth::attempt($credentials, $request->login_remember ? true : false)) {
                // Authentication passed...
                $data = Auth::user();
                if ($data->status_aktif == 0) {
                    Auth::logout();
                    return redirect('login')->with('error', "Akun Antum sudah tidak aktif.");
                } else if ($data->email_confirm == 0) {
                    Auth::logout();
                    return redirect('login')->with('error', "Antum belum melakukan aktivasi akun. Silahkan Cek Email Antum dan lakukan aktivasi.");
                } else if ($data->anggota->status_her === "BELUM_BAYAR") {
                    Auth::logout();
                    return redirect('login')->with('error', "Antum belum melakukan heregistrasi. Silahkan lakukan heregistrasi terlebih dahulu.");
                } else {
                    return redirect()->intended('dashboard');
                }
            }

            return redirect('login')->with('error', "Password Salah. Silahkan pilih Lupa password untuk memperbarui password Antum");
        } else {
            return redirect('login')
                ->withErrors($validator)->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
