<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ResetPasswordMail;
use App\Model\UserLogin;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function checkUserExist(Request $req)
    {
        $data = UserLogin::with('anggota')
            ->orWhereHas('anggota', function ($query) use ($req){
                return $query->where('email', $req->get('payload'));
            })
            ->orWhere("npa", $req->get('payload'))->first();

        if($data){
            $payload = "{$data->id_login}{$data->npa}{$data->anggota->nama_lengkap}" . time();
            $data->reset_password_token = md5($payload);
            $link = route("reset.password.token", $data->reset_password_token);
            $data->save();
            if($data->anggota->email){
                Mail::to($data->anggota->email)->queue(new ResetPasswordMail($data, $link));
                return redirect()->back()
                    ->with("success", "Email berhasil dikirim. Silahkan periksa akun email Anda.
                    Periksa Spam jika email belum masuk.");
                // uncomment code bellow to render in browser
                //return new ResetPasswordMail($data, $link);
            }
        }else{
            return redirect()->back()
                ->with("error", "Email atau NPA tidak dikenali. Silahkan coba Email atau NPA yang lain.");
        }
    }

    public function checkToken($token)
    {
        $data = UserLogin::where('reset_password_token', $token)->first();

        if($data){
            return view("auth.passwords.form_reset", [
                "token" => $token
            ]);
        }else{
            abort(404);
        }

    }
    public function verifyUserAndPassword(Request $req, $token)
    {
        $data = UserLogin::where('reset_password_token', $token)->first();

        if($data){
            $data->password = $req->get('password');
            $data->reset_password_token = null;
            $data->save();
            return redirect()->to("login")
                ->with("success", "Password berhasil diubah. Silahkan login kembali.");
        }else{
            abort(404);
        }

    }
}
