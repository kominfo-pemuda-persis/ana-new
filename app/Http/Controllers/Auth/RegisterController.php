<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ActivationMail;
use App\Model\Anggota;
use App\Model\RoleUser;
use App\Model\UserLogin;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(){
        return view('auth.register');
    }

    public function get(Request $r){
        $id = $r->all();

        $data = Anggota::with('master_jamiyyah')
            ->where('npa', $id['npa'])->first();

        if($data){
            return view("auth.register")
                ->with([
                    'exist'=>true,
                    'anggota'=> $data
                ]);
        }
        return redirect("register")->with("error", "Data Antum belum terdaftar. Silahkan hubungi Ketua PC antum");
    }

    public function store(Request $request){
        $data = $request->all();
        $passwordDefault = Str::random(8);
        $rules = array(
            'npa' => 'required|unique:t_login|exists:t_anggota,npa',
        );
        $msgs = [
            'npa.required' => 'NPA tidak boleh kosong',
            'npa.unique' => 'NPA sudah terdaftar',
            'npa.exists' => 'NPA tidak ditemukan'
        ];

        $validator = Validator::make($request->all(), $rules, $msgs);

        if ($validator->fails()) {
            return redirect('register')
                ->withErrors($validator)->withInput();
        }else{
            $anggota = Anggota::where('npa', $data['npa'])->first();

            //store ke t_login
            $token = $this->create_token();
            $id_login = Uuid::uuid4()->getHex();
            $save_login = UserLogin::create(
                [
                    'id_login' => $id_login,
                    'npa' => $data['npa'],
                    'password' => $passwordDefault,
                    'activ_code' => $token,
                    'email_confirm' => 0,
                    'status_aktif' => 1
                ]
            );

            if($save_login){
                $this->send_email($token, $anggota->npa, $anggota->nama_lengkap, $anggota->email, $passwordDefault);
                // save role
                RoleUser::create([
                    "role_id" => 1,
                    "user_id" => $id_login
                ]);
                return redirect('login')->with("success", "Email berhasil terkirim. Silahkan cek Email Anda untuk aktivasi akun.");
            }
        }
    }

    public function create_token(){
        return hash_hmac('sha256', base64_encode(Str::random(40)), config('app_key'));
    }

    public function send_email($token, $npa, $nama_lengkap, $email, $password)
    {
        $data['link'] = url("/register/activation/" . $token);
        $data['npa'] = $npa;
        $data['nama_lengkap'] = $nama_lengkap;
        $data['password'] = $password;
        Mail::to(trim($email))->queue(new ActivationMail($data));
    }

    public function activation($code){
        $data = UserLogin::where(['activ_code' => $code, 'email_confirm'=> 0])->first();

        if($data){
            $set_active = UserLogin::where('id_login', $data->id_login)
                ->update([
                    'email_confirm' => 1,
                    'status_aktif' => 1,
                    'activ_code' => null
                ]);

            if($set_active){
                return redirect('login')->with("success", "Alhamdulillah, akun anda telah aktif. Silahkan Login");
            }
        }else{
            return redirect('login');
        }
    }
}
