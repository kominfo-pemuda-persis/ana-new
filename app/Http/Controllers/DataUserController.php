<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Http\Requests\UserLoginRequest;
use App\Model\PD;
use App\Model\PW;
use App\Model\UserLogin;
use App\Model\Village;
use App\Repositories\PCRepository;
use App\Repositories\PDRepository;
use App\Repositories\PWRepository;
use App\Repositories\UserLoginRepository;
use DataTables;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DataUserController extends Controller
{
    private $userLoginRepository;
    private $pwRepository;
    private $pdRepository;
    private $pcRepository;

    public function __construct(
        UserLoginRepository $userLoginRepository,
        PWRepository $pwRepository,
        PDRepository $pdRepository,
        PCRepository $pcRepository
    )
    {
        $this->userLoginRepository = $userLoginRepository;
        $this->pwRepository = $pwRepository;
        $this->pdRepository = $pdRepository;
        $this->pcRepository = $pcRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = $this->userLoginRepository->allWithRelation(true);
            $anggota = Auth::user()->anggota;

            if (Permission::checkRole('admin_pj') || Permission::checkRole('tasykil_pj')) {
                $data->whereHas("anggota.pimpinanCabang", function ($query) use($anggota){
                    return $query->where('kd_pc', $anggota->pc);
                });
            } else if (Permission::checkRole('admin_pc') || Permission::checkRole('tasykil_pc')) {
                $data->whereHas("anggota.pimpinanCabang", function ($query) use($anggota){
                    return $query->where('kd_pc', $anggota->pc);
                });
            } else if (Permission::checkRole('admin_pd') || Permission::checkRole('tasykil_pd')) {

                $pd = PD::where('kd_pd', $anggota->pd)->with("relatedPC")->get();

                $listPC = $pd->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })->collapse();

                $data->whereHas("anggota.pimpinanCabang", function ($query) use($anggota, $listPC){
                    return $query->whereIn('kd_pc', $listPC);
                });

            } else if (Permission::checkRole('admin_pw') || Permission::checkRole('tasykil_pw')) {

                $relatedPD = PW::where('kd_pw', $anggota->pw)
                    ->first()->relatedPD->pluck('kd_pd');

                $pd = PD::whereIn('kd_pd', $relatedPD)->with("relatedPC")->get();

                $listPC = $pd->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })->collapse();

                $data->whereHas("anggota.pimpinanCabang", function ($query) use($anggota, $listPC){
                    return $query->whereIn('kd_pc', $listPC);
                });
            }

            return Datatables::of($data)
                ->addIndexColumn()
                ->filterColumn('nama_lengkap', function($query, $keyword) {
                    $query->whereHas("anggota", function ($query_anggota) use($keyword){
                        return $query_anggota->where("nama_lengkap", 'LIKE',  "%{$keyword}%");
                    });
                })
                ->filterColumn('email', function($query, $keyword) {
                    $query->whereHas("anggota", function ($query_anggota) use($keyword){
                        return $query_anggota->where("email", 'LIKE',  "%{$keyword}%");
                    });
                })
                ->filterColumn('nama_pj', function($query, $keyword) {
                    $query->whereHas("anggota", function ($query_anggota) use($keyword){
                        return $query_anggota->where("nama_pj", 'LIKE',  "%{$keyword}%");
                    });
                })
                ->filterColumn('anggota.pimpinan_cabang', function($query, $keyword) {
                    $query->whereHas("anggota.pimpinanCabang", function ($query_anggota) use($keyword){
                        return $query_anggota->where("nama_pc", 'LIKE',  "%{$keyword}%");
                    });
                })
                ->filterColumn('anggota.pimpinan_daerah', function($query, $keyword) {
                    $query->whereHas("anggota.pimpinanDaerah", function ($query_anggota) use($keyword){
                        return $query_anggota->where("nama_pd", 'LIKE',  "%{$keyword}%");
                    });
                })
                ->filterColumn('anggota.pimpinan_wilayah', function($query, $keyword) {
                    $query->whereHas("anggota.pimpinanWilayah", function ($query_anggota) use($keyword){
                        return $query_anggota->where("nama_pw", 'LIKE',  "%{$keyword}%");
                    });
                })
                ->addColumn('action', 'ana.datatables.data_user.action')
                ->addColumn('linkFoto', function (UserLogin $data) {
                    if ($data->anggota) {
                        $image = Storage::disk('s3')->url('images/anggota/' . $data->anggota->foto);

                        return $image;
                    } else {
                        return "images/anggota/default.png";
                    }
                })
                ->make(true);
        }

        return view('ana.datauser.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userLoginRepository->findUserById($id);

        if (!$user) {

            abort(404);
        }

        return view('ana.datauser.show', compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id_login)
    {
        $user = $this->userLoginRepository->find($id_login);
        $pw = $this->pwRepository->all();
        $pd = $this->pdRepository->all();
        $pc = $this->pcRepository->all();

        return view('ana.datauser.edit', compact('user', 'pw', 'pd', 'pc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $data_user
     * @return Response
     */
    public function update(UserLoginRequest $request, $id_login)
    {
        $user = $this->userLoginRepository->find($id_login);

        if (!$user) {
            return redirect()->route('data-user.index')->with('error', 'User not found');
        }

        $data = $request->validated();
        $namaDesa = $request->input('desa');

        $desa = Village::where('nama', $namaDesa)->first();

        if ($desa) {
            $data['desa'] = $desa->id;
        }

        try {
            $this->userLoginRepository->update($data, $user);

            return redirect()->route('data-user.index')->with('success', 'User updated successfully');
        } catch (Exception $e) {
            return redirect()->route('data-user.edit', $id_login)->with('error', 'Failed to update user');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $destroyDataUser = UserLogin::find($id)->forceDelete();
        if ($destroyDataUser) {
            return response()->json(['message' => 'Success destroy data', 'data' => $destroyDataUser]);
        } else {
            return response()->json(['message' => 'Failed restore data'], 500);
        }
    }
}
