<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\KejamiyyahanPD;
use App\Model\MonografiPd;
use App\Model\PC;
use App\Model\PD;
use App\Model\Province;
use App\Model\PW;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Throwable;

class MonografiPdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pw = Auth::user()->anggota->pw;
        $pd = Auth::user()->anggota->pd;

        if ((Permission::checkRole('superadmin')) || (Permission::checkRole('tasykil_pp')) || (Permission::checkRole('admin_pp'))) {
            $monografiPD = MonografiPd::all();
        } elseif ((Permission::checkRole('admin_pw')) || (Permission::checkRole('tasykil_pw'))) {
            $monografiPD = MonografiPd::where('kd_pw', $pw)->get();
        } elseif ((Permission::checkRole('admin_pd')) || (Permission::checkRole('tasykil_pd'))) {
            $monografiPD = MonografiPd::where('kd_pd', $pd)->get();
        }

        return view('ana.monografi.pd.index', compact('monografiPD'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pd.add', compact('dataPW', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kd_pd' => 'required',
            'nama_pd' => 'required',
            'kd_pw' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_kontak' => 'required|max:15',
            'alamat_utama' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'jarak_kabupaten' => 'max:50',
            'foto' => 'mimes:jpg,png,jpeg,bmp|file|max:5000'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => $validation->errors(),
                'status' => $status
            ]);
        } else {
            DB::beginTransaction();
            try {


                $daerah = new MonografiPd();
                $daerah->kd_pd = $request->Input('kd_pd');
                $daerah->nama_pd = $request->Input('nama_pd');
                $daerah->kd_pw = $request->Input('kd_pw');
                $daerah->provinsi = $request->Input('provinsi');
                $daerah->kota = $request->Input('kota');
                $daerah->latitude = $request->Input('latitude');
                $daerah->longitude = $request->Input('longitude');
                $daerah->email = $request->Input('email');
                $daerah->no_kontak = $request->Input('no_kontak');
                $daerah->alamat_utama = $request->Input('alamat_utama');
                $daerah->alamat_alternatif = $request->Input('alamat_alternatif');
                $daerah->luas = $request->Input('luas');
                $daerah->bw_utara = $request->Input('bw_utara');
                $daerah->bw_selatan = $request->Input('bw_selatan');
                $daerah->bw_timur = $request->Input('bw_timur');
                $daerah->bw_barat = $request->Input('bw_barat');
                $daerah->jarak_dari_ibukota_negara = $request->Input('jarak_dari_ibukota_negara');
                $daerah->jarak_dari_ibukota_provinsi = $request->Input('jarak_dari_ibukota_provinsi');
                $daerah->jarak_dari_ibukota_kabupaten = $request->Input('jarak_dari_ibukota_kabupaten');

                if($request->hasFile('foto')){
                    $file = $request->file('foto');
                    $filename = $request->nama_pd . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    Storage::disk('s3')->put('images/monografi/pd/' . $filename, file_get_contents($file));
                    $daerah->foto = $filename;
                }

                $status = $daerah->save();

                activity('Monografi PD')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PD Created');

                $jamiyyah = new KejamiyyahanPD();
                $jamiyyah->kd_monografi_pd = $daerah->id;
                $jamiyyah->kd_pd = $request->Input('kd_pd');
                $jamiyyah->nama_pd = $request->Input('nama_pd');
                $jamiyyah->kd_pw = $request->Input('kd_pw');
                $jamiyyah->provinsi = $request->Input('provinsi');
                $jamiyyah->kota = $request->Input('kota');
                $jamiyyah->ketua = $request->Input('ketua');
                $jamiyyah->wkl_ketua = $request->Input('wkl_ketua');
                $jamiyyah->sekretaris = $request->Input('sekretaris');
                $jamiyyah->wkl_sekretaris = $request->Input('wkl_sekretaris');
                $jamiyyah->bendahara = $request->Input('bendahara');
                $jamiyyah->wkl_bendahara = $request->Input('wkl_bendahara');
                $jamiyyah->bid_jamiyyah = $request->Input('bid_jamiyyah');
                $jamiyyah->wkl_bid_jamiyyah = $request->Input('wkl_bid_jamiyyah');
                $jamiyyah->bid_kaderisasi = $request->Input('bid_kaderisasi');
                $jamiyyah->wkl_bid_kaderisasi = $request->Input('wkl_bid_kaderisasi');
                $jamiyyah->bid_administrasi = $request->Input('bid_administrasi');
                $jamiyyah->wkl_bid_administrasi = $request->Input('wkl_bid_administrasi');
                $jamiyyah->bid_pendidikan = $request->Input('bid_pendidikan');
                $jamiyyah->wkl_bid_pendidikan = $request->Input('wkl_bid_pendidikan');
                $jamiyyah->bid_dakwah = $request->Input('bid_dakwah');
                $jamiyyah->wkl_bid_dakwah = $request->Input('wkl_bid_dakwah');
                $jamiyyah->bid_humas_publikasi = $request->Input('bid_humas_publikasi');
                $jamiyyah->wkl_bid_humas_publikasi = $request->Input('wkl_bid_humas_publikasi');
                $jamiyyah->bid_hal = $request->Input('bid_hal');
                $jamiyyah->wkl_bid_hal = $request->Input('wkl_bid_hal');
                $jamiyyah->bid_or_seni = $request->Input('bid_or_seni');
                $jamiyyah->wkl_bid_or_seni = $request->Input('wkl_bid_or_seni');
                $jamiyyah->bid_sosial = $request->Input('bid_sosial');
                $jamiyyah->wkl_bid_sosial = $request->Input('wkl_bid_sosial');
                $jamiyyah->bid_ekonomi = $request->Input('bid_ekonomi');
                $jamiyyah->wkl_bid_ekonomi = $request->Input('wkl_bid_ekonomi');
                $jamiyyah->penasehat1 = $request->Input('penasehat1');
                $jamiyyah->penasehat2 = $request->Input('penasehat2');
                $jamiyyah->penasehat3 = $request->Input('penasehat3');
                $jamiyyah->penasehat4 = $request->Input('penasehat4');
                $jamiyyah->pembantu_umum1 = $request->Input('pembantu_umum1');
                $jamiyyah->pembantu_umum2 = $request->Input('pembantu_umum2');
                $jamiyyah->pembantu_umum3 = $request->Input('pembantu_umum3');
                $jamiyyah->hari = $request->Input('hari');
                $jamiyyah->pukul = $request->Input('pukul');
                $jamiyyah->musda_terakhir_m = $request->Input('musda_terakhir_m');
                $jamiyyah->musda_terakhir_h = $request->Input('musda_terakhir_h');
                $jamiyyah->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyah->anggota_tersiar = $request->Input('anggota_tersiar');
                $jamiyyah->anggota_istimewa = $request->Input('anggota_istimewa');
                $status = $jamiyyah->save();

                activity('Kejamiyyahan PD')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Kejamiyyahan PD Created');

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
            }

            return response()->json([
                'status' => $status
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $monografiPD = MonografiPd::find($id);

        if (!$monografiPD) abort(404);

        $kejamiyyahanPD = $monografiPD;
        if (!$kejamiyyahanPD) abort(404);

        return view('ana.monografi.pd.show', compact(
            'monografiPD', 'kejamiyyahanPD'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $monografiPD = MonografiPd::find($id);

        if (!$monografiPD) abort(404);

        $kejamiyyahanPD = $monografiPD;

        if (!$kejamiyyahanPD) abort(404);

        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pd.edit', compact('dataPW',
            'provinces', 'monografiPD', 'kejamiyyahanPD'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kd_pd' => 'required',
            'nama_pd' => 'required',
            'kd_pw' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_kontak' => 'required|max:15',
            'alamat_utama' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'jarak_kabupaten' => 'max:50',
            'foto' => 'mimes:jpg,png,jpeg,bmp|file|max:5000'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => $validation->errors(),
                'status' => $status
            ]);
        } else {
            DB::beginTransaction();
            try {


                $daerah = MonografiPd::find($id);

                if (!$daerah) abort(404);

                $daerah->kd_pd = $request->Input('kd_pd');
                $daerah->nama_pd = $request->Input('nama_pd');
                $daerah->start_periode = $request->Input('start_periode');
                $daerah->end_periode = $request->Input('end_periode');
                $daerah->kd_pw = $request->Input('kd_pw');
                $daerah->provinsi = $request->Input('provinsi');
                $daerah->kota = $request->Input('kota');
                $daerah->latitude = $request->Input('latitude');
                $daerah->longitude = $request->Input('longitude');
                $daerah->email = $request->Input('email');
                $daerah->no_kontak = $request->Input('no_kontak');
                $daerah->alamat_utama = $request->Input('alamat_utama');
                $daerah->alamat_alternatif = $request->Input('alamat_alternatif');
                $daerah->luas = $request->Input('luas');
                $daerah->bw_utara = $request->Input('bw_utara');
                $daerah->bw_selatan = $request->Input('bw_selatan');
                $daerah->bw_timur = $request->Input('bw_timur');
                $daerah->bw_barat = $request->Input('bw_barat');
                $daerah->jarak_dari_ibukota_negara = $request->Input('jarak_dari_ibukota_negara');
                $daerah->jarak_dari_ibukota_provinsi = $request->Input('jarak_dari_ibukota_provinsi');
                $daerah->jarak_dari_ibukota_kabupaten = $request->Input('jarak_dari_ibukota_kabupaten');

                if($request->hasFile('foto')){
                    $file = $request->file('foto');
                    $filename = $request->nama_pd . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    Storage::disk('s3')->put('images/monografi/pd/' . $filename, file_get_contents($file));
                    $daerah->foto = $filename;
                }

                $status = $daerah->save();

                activity('Monografi PD')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PD Updated');

                $jamiyyah = $daerah->kejamiyyahan;

                if (!$jamiyyah) abort(404);

                $jamiyyah->kd_pd = $request->Input('kd_pd');
                $jamiyyah->nama_pd = $request->Input('nama_pd');
                $jamiyyah->kd_pw = $request->Input('kd_pw');
                $jamiyyah->provinsi = $request->Input('provinsi');
                $jamiyyah->kota = $request->Input('kota');
                $jamiyyah->ketua = $request->Input('ketua');
                $jamiyyah->wkl_ketua = $request->Input('wkl_ketua');
                $jamiyyah->sekretaris = $request->Input('sekretaris');
                $jamiyyah->wkl_sekretaris = $request->Input('wkl_sekretaris');
                $jamiyyah->bendahara = $request->Input('bendahara');
                $jamiyyah->wkl_bendahara = $request->Input('wkl_bendahara');
                $jamiyyah->bid_jamiyyah = $request->Input('bid_jamiyyah');
                $jamiyyah->wkl_bid_jamiyyah = $request->Input('wkl_bid_jamiyyah');
                $jamiyyah->bid_kaderisasi = $request->Input('bid_kaderisasi');
                $jamiyyah->wkl_bid_kaderisasi = $request->Input('wkl_bid_kaderisasi');
                $jamiyyah->bid_administrasi = $request->Input('bid_administrasi');
                $jamiyyah->wkl_bid_administrasi = $request->Input('wkl_bid_administrasi');
                $jamiyyah->bid_pendidikan = $request->Input('bid_pendidikan');
                $jamiyyah->wkl_bid_pendidikan = $request->Input('wkl_bid_pendidikan');
                $jamiyyah->bid_dakwah = $request->Input('bid_dakwah');
                $jamiyyah->wkl_bid_dakwah = $request->Input('wkl_bid_dakwah');
                $jamiyyah->bid_humas_publikasi = $request->Input('bid_humas_publikasi');
                $jamiyyah->wkl_bid_humas_publikasi = $request->Input('wkl_bid_humas_publikasi');
                $jamiyyah->bid_hal = $request->Input('bid_hal');
                $jamiyyah->wkl_bid_hal = $request->Input('wkl_bid_hal');
                $jamiyyah->bid_or_seni = $request->Input('bid_or_seni');
                $jamiyyah->wkl_bid_or_seni = $request->Input('wkl_bid_or_seni');
                $jamiyyah->bid_sosial = $request->Input('bid_sosial');
                $jamiyyah->wkl_bid_sosial = $request->Input('wkl_bid_sosial');
                $jamiyyah->bid_ekonomi = $request->Input('bid_ekonomi');
                $jamiyyah->wkl_bid_ekonomi = $request->Input('wkl_bid_ekonomi');
                $jamiyyah->penasehat1 = $request->Input('penasehat1');
                $jamiyyah->penasehat2 = $request->Input('penasehat2');
                $jamiyyah->penasehat3 = $request->Input('penasehat3');
                $jamiyyah->penasehat4 = $request->Input('penasehat4');
                $jamiyyah->pembantu_umum1 = $request->Input('pembantu_umum1');
                $jamiyyah->pembantu_umum2 = $request->Input('pembantu_umum2');
                $jamiyyah->pembantu_umum3 = $request->Input('pembantu_umum3');
                $jamiyyah->hari = $request->Input('hari');
                $jamiyyah->pukul = $request->Input('pukul');
                $jamiyyah->musda_terakhir_m = $request->Input('musda_terakhir_m');
                $jamiyyah->musda_terakhir_h = $request->Input('musda_terakhir_h');
                $jamiyyah->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyah->anggota_tersiar = $request->Input('anggota_tersiar');
                $jamiyyah->anggota_istimewa = $request->Input('anggota_istimewa');
                $status = $jamiyyah->save();

                activity('Kejamiyyahan PD')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Kejamiyyahan PD Updated');

                activity('Monografi PD')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PD Updated');

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
            }

            return response()->json([
                'status' => $status
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MonografiPd::find($id)->delete();
        activity('Monografi PD')
            ->performedOn(Auth::user())
            ->causedBy(Auth::user())
            ->log('Monografi PD deleted');
        return response()->json($data);
    }
}
