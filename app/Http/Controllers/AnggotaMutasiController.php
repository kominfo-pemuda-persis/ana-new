<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use DataTables;
use DB;
use Illuminate\Http\Request;
use App\Model\Anggota;
use App\Model\AnggotaMutasi;
use App\Model\PC;
use App\Model\PD;
use App\Model\PW;
use Illuminate\Support\Facades\Auth;

class AnggotaMutasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ana.anggota.mutasi.index');
    }

    public function datatables()
    {
        // Get current authenticated anggota to get pw, pd, and pc id
        $currentNPA = Auth::user()->npa;
        $authAnggota =  Anggota::where('npa', $currentNPA)->first();

        $anggotaMutasi = AnggotaMutasi::with(['asal', 'tujuan', 'anggota'])
            ->orderBy('id_mutasi', 'asc')
            ->whereNull('t_anggota_mutasi.deleted_at');
        //filternya di sini
        if (Permission::checkRole('admin_pj') || Permission::checkRole('tasykil_pj')) {
            $anggotaMutasi->whereHas('anggota', function ($query) use($authAnggota) {
                $query->where('pj', '=', $authAnggota->pj);
            });
        } else if (Permission::checkRole('admin_pc') || Permission::checkRole('tasykil_pc')) {
            $anggotaMutasi->whereHas('anggota', function ($query) use($authAnggota) {
                $query->where('pc', '=', $authAnggota->pc);
            });
        } else if (Permission::checkRole('admin_pd') || Permission::checkRole('tasykil_pd')) {

            $pd = PD::where('kd_pd', $authAnggota->pd)->with("relatedPC")->get();

            $listPC = $pd->map(function ($value) {
                return $value->relatedPC->map(function ($value2) {
                    return $value2->kd_pc;
                });
            })->collapse();
            $anggotaMutasi->whereHas('anggota', function ($query) use($listPC) {
                $query->whereIn('pc', $listPC);
            });
        } else if (Permission::checkRole('admin_pw') || Permission::checkRole('tasykil_pw')) {

            $relatedPD = PW::where('kd_pw', $authAnggota->pw)
                ->first()->relatedPD->pluck('kd_pd');

            $pd = PD::whereIn('kd_pd', $relatedPD)->with("relatedPC")->get();

            $listPC = $pd->map(function ($value) {
                return $value->relatedPC->map(function ($value2) {
                    return $value2->kd_pc;
                });
            })->collapse();

            $anggotaMutasi->whereHas('anggota', function ($query) use($listPC) {
                $query->whereIn('pc', $listPC);
            });
        }

        return Datatables::of($anggotaMutasi)
            ->addIndexColumn()
            ->addColumn('action', 'ana.datatables.mutasi.action')
            ->make(true);
    }

    public function get(Request $request)
    {
        $id = $request->id;
        $mutasi = AnggotaMutasi::with(['asal', 'tujuan', 'anggota'])->where('id_mutasi', $id)->first();
        if (!$mutasi) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json(['message' => 'Success getting data', 'data' => $mutasi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        try{
            $status = DB::transaction(function () use ($data) {
                AnggotaMutasi::create($data);
                $pd = PC::where('kd_pc', $data['tujuan'])->firstOrFail()->kd_pd;
                $pw = PD::where('kd_pd', $pd)->firstOrFail()->kd_pw;
                $anggota = Anggota::where('id_anggota', $data['id_anggota'])->firstOrFail();
                $anggota->pd = $pd;
                $anggota->pw = $pw;
                $anggota->pc = $data['tujuan'];
                $anggota->pj = null;
                $anggota->nama_pj = null;
                return $anggota->save();
            });

            if ($status) {
                return response()->json(['message' => 'Success Adding data', 'data' => 1]);
            } else {
                return response()->json(['message' => 'Failed Adding data'], 500);
            }

        }catch (\Exception $e) {
            return response()->json(['message' => 'Failed Adding data ' . $e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $data = $request->all();

        try{
            $status = DB::transaction(function () use ($data, $id) {
                $mutasi = AnggotaMutasi::where('id_mutasi', $id)
                    ->firstOrFail();
                $mutasi->update($data);
                $pd = PC::where('kd_pc', $data['tujuan'])->firstOrFail()->kd_pd;
                $pw = PD::where('kd_pd', $pd)->firstOrFail()->kd_pw;
                $anggota = Anggota::where('id_anggota', $data['id_anggota'])->firstOrFail();
                $anggota->pd = $pd;
                $anggota->pw = $pw;
                $anggota->pc = $data['tujuan'];
                $anggota->pj = null;
                $anggota->nama_pj = null;
                return $anggota->save();
            });

            if ($status) {
                return response()->json(['message' => 'Success Updating data', 'data' => 1]);
            } else {
                return response()->json(['message' => 'Failed Updating data'], 500);
            }

        }catch (\Exception $e) {
            return response()->json(['message' => 'Failed Updating data ' . $e->getMessage()], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $destroyData = AnggotaMutasi::where('id_mutasi', $id)->first();
        $destroyData->delete();
        if ($destroyData) {
            return response()->json(['message' => 'Success deleting data', 'data' => $destroyData]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }
    }
}
