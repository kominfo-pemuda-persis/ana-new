<?php

namespace App\Http\Controllers;

use Exception;
use App\Model\PD;
use App\Model\PW;
use App\Model\Bulan;
use App\Model\Anggota;
use App\Model\Bendahara;
use App\Model\Transaksi;
use App\Facades\Permission;
use App\Model\LogTransaksi;
use App\Model\TempLogTransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class TransaksiController extends Controller
{
    public function index()
    {
        $idCurrent = Auth::user()->anggota;
        $authAnggota = Anggota::where('npa', $idCurrent->npa)->first();
        $tagihan = Bendahara::where('status', '1')->get();
        // $transaksi = Transaksi::with('anggota', 'tagihan', 'bulan')
        //     ->orderBy('id', 'desc')
        //     ->where('status', 2)
        //     ->paginate(10);
        

        $tf = DB::table('t_bendahara')
            ->select('t_bulan.id', 't_bulan.nama_bulan', 't_pc.nama_pc', 't_pd.nama_pd', 't_pw.nama_pw', 't_transaksi.created_at', 't_anggota.nama_lengkap', 't_transaksi.npa', 't_bendahara.nama', 't_bendahara.jumlah', 't_transaksi.paid', 't_transaksi.pay')
            ->join('t_transaksi', 't_transaksi.id_tagihan', '=', 't_bendahara.id')
            ->join('t_bulan', 't_bulan.id', '=', 't_transaksi.bulan_id')
            ->join('t_anggota', 't_anggota.npa', '=', 't_transaksi.npa')
            ->join('t_pc', 't_anggota.pc', '=', 't_pc.kd_pc')
            ->join('t_pd', 't_anggota.pd', '=', 't_pd.kd_pd')
            ->join('t_pw', 't_anggota.pw', '=', 't_pw.kd_pw')
            ->where('t_transaksi.status', '=', 1)
            ->where('t_transaksi.pc','=', $idCurrent->pc)
            ->get();

        $bulanAll = Bulan::all();
        
        
        $anggota = Anggota::with('transaksi_tagihan');
        if (Permission::checkRole('admin_pj') || Permission::checkRole('tasykil_pj')) {
            $anggota->where('pj', $authAnggota->pj);
        } else if (Permission::checkRole('admin_pc') || Permission::checkRole('tasykil_pc')) {
            $anggota->where('pc', $authAnggota->pc);
        } else if (Permission::checkRole('admin_pd') || Permission::checkRole('tasykil_pd')) {
            $anggota->where('pc', $authAnggota->pc);
        } else if (Permission::checkRole('admin_pw') || Permission::checkRole('tasykil_pw')) {
            $anggota->where('pc', $authAnggota->pc);
        }

        $currentAnggota = $anggota->get();
        return view('ana.bendahara.transaksi_tagihan.index', compact('currentAnggota', 'tagihan', 'tf', 'bulanAll'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "npa" => "required",
            "tagihan_id" => "required",
            "pembayaran" => "required",
            "bulan_id" => "required"
        ],);

        if ($validator->fails()) {
            return redirect('/bendahara/transaksi-tagihan')
                ->withErrors($validator)
                ->withInput();
        }

        $hapus = trim($request->pembayaran, "Rp. ");
        $hasilHapus = str_replace(".", "", $hapus);
        $dataTransaksi = Transaksi::where('npa', $request->npa)->get()->toArray();
        $idCurrent = Auth::user()->anggota->pc;
        if ($dataTransaksi == null) {
            $data = Transaksi::insert([
                'pc' => $idCurrent,
                'npa' => $request->npa,
                'id_tagihan' => $request->tagihan_id,
                'temp_tagihan_id' => $request->tagihan_id,
                'bulan_id' => $request->bulan_id,
                'pay' => $hasilHapus,
                'paid' => $hasilHapus,
                'temp_paid' => $hasilHapus,
                'status' => 1,
                'temp_status' => 1,
                // 'keterangan' => $request->keterangan,
                'created_at' => now()
            ]);
        } else {
            $dataCurrent = Transaksi::where('npa',  $request->npa)->get();
            // $datalog = LogTransaksi::where('npa', $request->npa)->get();
            $data = Transaksi::where('npa',  $request->npa);
            // $log = LogTransaksi::where('npa', $request->npa);

            if ($dataCurrent[0]['id_tagihan'] == 0) {
                $data->update([
                    'id_tagihan' => $request->tagihan_id,
                    'temp_tagihan_id' => $request->tagihan_id,
                    'status' => 1,
                    'temp_status' => 1,
                    'pay' => $hasilHapus,
                    'bulan_id' => $request->bulan_id,
                    'paid' => ((int)$dataCurrent[0]['paid'] + $hasilHapus),
                    'temp_paid' => ((int)$dataCurrent[0]['paid'] + $hasilHapus),
                    // 'keterangan' => $request->keterangan
                ]);  
            }elseif($dataCurrent[0]['id_tagihan'] != $request->tagihan_id) {
                return redirect()->back()->with('danger', 'Transaksi gagal, karena Anda sudah bertransaksi!');
            } else {
                $data->update([
                    'pay' => $hasilHapus,
                    'bulan_id' => $request->bulan_id,
                    'paid' => ((int)$dataCurrent[0]['paid'] + $hasilHapus),
                    'temp_paid' => ((int)$dataCurrent[0]['paid'] + $hasilHapus),
                    // 'keterangan' => $request->keterangan
                ]);      
            }
        }

        return redirect()->back()->with('success', 'Transaksi, Tagihan Berhasil!');
    }

    public function setor(Request $request)
    {
        
        $idCurrent = Auth::user()->anggota->pc;
        
        $tahun = \Carbon\Carbon::now()->year;
        foreach($request->npa as $npa) {
            $transaksi = Transaksi::where('npa', $npa)->where('pc', $idCurrent);
            $transaksi->update([
                'status' => 2,
                'temp_status' => 2,
            ]);
            
            $transaksi = Transaksi::where('npa', $npa)->first();
            $log = LogTransaksi::where('id_pc', $transaksi->pc)->first();
            
            $dataSetor = Transaksi::where('temp_status', 2)
                        ->where('temp_tagihan_id', $transaksi->temp_tagihan_id)
                        ->where('pc', $idCurrent)
                        ->groupBy('temp_status')->count();

            $belumSetor = Transaksi::where('temp_status', 1)
                        ->where('temp_tagihan_id', $transaksi->temp_tagihan_id)
                        ->where('pc', $idCurrent)
                        ->groupBy('temp_status')->count();

            $totalSetor = Transaksi::where('pc', $idCurrent)
                        ->where('temp_status', 2)
                        ->where('temp_tagihan_id', $transaksi->temp_tagihan_id)
                        ->where('pc', $idCurrent)
                        ->sum('temp_paid');
        }


        $test = LogTransaksi::create([
            'id_pc' => $idCurrent,
            'id_tagihan' => 0,
            'jml_masuk' => 0,
            'blm_setor' => 0,
            'total_setor' => 0,
            'total_setor' => 0,
            'status' => 0,
            'tahun' => 0
        ]);

        if(isset($test->id_tagihan) == $transaksi->temp_tagihan_id && isset($test->id_pc) == $idCurrent ) {
            $datadata = LogTransaksi::updateOrCreate([
                'id_pc' => $idCurrent,
            ], [
                'id_tagihan' => $transaksi->temp_tagihan_id,
                'jml_masuk' => $dataSetor,
                'blm_setor' => $belumSetor,
                'total_setor' => $totalSetor,
                'total_setor' => $totalSetor,
                'status' => 0,
                'tahun' => $tahun
            ]);
        }else{
            return response()->json([
                'code' => 400,
                'message' => 'Transaksi Anda sudah ada!, Segera hubungi operator teknis, agar transaksi Anda segera diselesaikan.',
                'url' => 'transaksi-tagihan'
            ]);   
        }


        foreach($request->npa as $npa) {
            $transaksi = Transaksi::where('npa', $npa);
            $transaksi->update([
                'id_tagihan' => 0,
                'status' => 0
            ]);
        }

        return response()->json([
            'code' => 200,
            'message' => 'Selamat!, transaksi berhasil dikirim, segera konfirmasi ke Bagian Administrasi PP, agar transaksi Anda segera dikonfirmasi.',
            'url' => 'transaksi-tagihan'
        ]);
    }



    public function log_transaksi()
    {
        $data["role"] = Auth::user()
        ->roles->pluck("name")
        ->toArray();
        $data["admin_pc"] = "admin_pc";
        $data["admin_pd"] = "admin_pd";
        $data["admin_pw"] = "admin_pw";
        $data["admin_pp"] = "admin_pp";
        $data["superadmin"] = "superadmin";

        $log = DB::table('t_log_transaksi')
                ->select('t_log_transaksi.id','t_pc.nama_pc','t_bendahara.nama','t_log_transaksi.jml_masuk', 't_log_transaksi.blm_setor', 't_log_transaksi.total_setor', 't_log_transaksi.tahun', 't_log_transaksi.status')
                ->join('t_bendahara', 't_bendahara.id', '=', 't_log_transaksi.id_tagihan')
                ->join('t_pc', 't_pc.kd_pc', '=', 't_log_transaksi.id_pc')
                ->where('t_log_transaksi.status', 1)
                ->get();
        return view('ana.bendahara.log_transaksi.index', [
            'log' => $log,
            'data' => $data
        ]);
    }

    public function list_log_transaksi()
    {
        $log = DB::table('t_log_transaksi')
                ->select('t_log_transaksi.id','t_pc.nama_pc','t_bendahara.nama','t_log_transaksi.jml_masuk', 't_log_transaksi.blm_setor', 't_log_transaksi.total_setor', 't_log_transaksi.tahun', 't_log_transaksi.status')
                ->join('t_bendahara', 't_bendahara.id', '=', 't_log_transaksi.id_tagihan')
                ->join('t_pc', 't_pc.kd_pc', '=', 't_log_transaksi.id_pc')
                ->where('t_log_transaksi.status', 0)
                ->get();
        return view('ana.bendahara.log_transaksi.list_transaksi', compact('log'));
    }

    public function konfirmasi($id)
    {
        $id = LogTransaksi::where('id', $id);
        $id->update([
            'status' => 1
        ]);

        return redirect('/bendahara/log-transaksi-tagihan');
    }

    public function tarik_saldo($id)
    {
        $DataLog = LogTransaksi::where('id', $id)->first();
        $current = LogTransaksi::where('id', $id);

        TempLogTransaksi::create([
            'id_tagihan' => $DataLog->id_tagihan,
            'jml_masuk' => $DataLog->jml_masuk,
            'blm_setor' => $DataLog->blm_setor,
            'total_setor' => $DataLog->total_setor,
            'tahun' => $DataLog->tahun,
        ]);

        $current->delete();

        return redirect()->back()->with('success', 'Transaksi penarikan saldo berhasil');
    }
}
