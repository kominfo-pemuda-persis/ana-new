<?php

namespace App\Http\Controllers;

use App\Mail\SendCaang;
use App\Model\Caang;
use App\Model\CaangKeluarga;
use App\Model\CaangOrganisasi;
use App\Model\CaangPendidikan;
use App\Rules\ReCaptcha;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CaangController extends Controller
{
    public $pathS3Caang = "images/caang/";
    public $pathLokalCaang = "images/caang/";

    public function __construct()
    {
        //$this->middleware('auth')->only(['destroy', 'index', 'update']);
    }

    public function processBase64Photo(
        $image = "",
        $pathS3 = "",
        $imageName = "caang_photo_",
        $s3 = false
    ) {
        $image = str_replace("data:image/png;base64,", "", $image);
        $image = str_replace(" ", "+", $image);
        if ($s3) {
            Storage::disk("s3")->put(
                $pathS3 . $imageName . '.png',
                base64_decode($image)
            );
        } else {
            Storage::disk("public")->put(
                $pathS3 . $imageName . '.png',
                base64_decode($image)
            );
        }
        return $imageName . '.png';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->json()->all();
        try {
            $validator = Validator::make($data, [
                "nama_lengkap" => "required|max:255",
                "tempat_lahir" => "required|max:100",
//                "g-recaptcha-response" => "required|captcha",
                'g-recaptcha-response' => ['required', new ReCaptcha],
                "tanggal_lahir" => "required",
                "pekerjaan" => "required|max:25",
                "gol_darah" => "required|max:3",
                "status_merital" => "required",
                "email" => "required|email|max:50|unique:t_caang,email",
                "no_telpon" => "required|max:15",
                "no_telpon2" => "nullable|max:15",
                "alamat" => "required",
                "foto" => "required",
                "provinsi" => "required",
                "kota" => "required",
                "kecamatan" => "required",
                "desa" => "required",
                "pw" => "required",
                "pd" => "required",
                "pc" => "required",
                "kode_pj" => "nullable",
                "nama_pj" => "required",
                "tempat_maruf" => "required",
                "tanggal_maruf_start" => "required",
                "tanggal_maruf_end" => "required",
                "keluarga" => "required",
                "pendidikan_terakhir" => "required",
                "pendidikan" => "required",
                "organisasi" => "required",
            ]);

            $validator->validate();
            $status = DB::transaction(function () use ($data) {
                $dir = "images/caang/";
                $imgname =
                    "caang_" .
                    str_replace(" ", "_", $data["nama_lengkap"]) .
                    "_" .
                    date("dmY_His");
                $foto = $this->processBase64Photo(
                    $data["foto"],
                    $dir,
                    $imgname,
                    boolval(env("CAANG_S3_MODE", true))
                );

                $dataCaang = new Caang();
                $dataCaang->nama_lengkap = $data["nama_lengkap"];
                $dataCaang->tempat_lahir = $data["tempat_lahir"];
                $dataCaang->gol_darah = $data["gol_darah"];
                $dataCaang->tanggal_lahir = $data["tanggal_lahir"];
                $dataCaang->status_merital = $data["status_merital"];
                $dataCaang->pekerjaan = $data["pekerjaan"];
                $dataCaang->alamat = $data["alamat"];
                $dataCaang->provinsi = $data["provinsi"];
                $dataCaang->kota = $data["kota"];
                $dataCaang->kecamatan = $data["kecamatan"];
                $dataCaang->desa = $data["desa"];
                $dataCaang->pw = $data["pw"];
                $dataCaang->pd = $data["pd"];
                $dataCaang->pc = $data["pc"];
                $dataCaang->pj = $data["kode_pj"];
                $dataCaang->nama_pj = $data["nama_pj"];
                $dataCaang->pendidikan_terakhir = $data["pendidikan_terakhir"];
                $dataCaang->tempat_maruf = $data["tempat_maruf"];
                $dataCaang->tanggal_maruf_start = $data["tanggal_maruf_start"];
                $dataCaang->tanggal_maruf_end = $data["tanggal_maruf_end"];
                $dataCaang->no_telpon = $data["no_telpon"];
                $dataCaang->no_telpon2 = $data["no_telpon2"];
                $dataCaang->email = $data["email"];
                $dataCaang->reg_date = Carbon::now();
                $dataCaang->foto = $foto;
                $dataCaang->save();

                $this->sendMessage($dataCaang);

                $keluarga = [];
                $organisasi = [];
                $pendidikan = [];

                foreach ($data["keluarga"] as $key => $value) {
                    $new_value = Arr::add(
                        $value,
                        "id_caang",
                        $dataCaang->id_caang
                    );
                    $keluarga = Arr::add($keluarga, $key, $new_value);
                }

                foreach ($data["organisasi"] as $key => $value) {
                    $new_value = Arr::add(
                        $value,
                        "id_caang",
                        $dataCaang->id_caang
                    );
                    $organisasi = Arr::add($organisasi, $key, $new_value);
                }

                foreach ($data["pendidikan"] as $key => $value) {
                    $new_value = Arr::add(
                        $value,
                        "id_caang",
                        $dataCaang->id_caang
                    );
                    $pendidikan = Arr::add($pendidikan, $key, $new_value);
                }

                $dataCaang->keluarga()->createMany($keluarga);
                $dataCaang->organisasi()->createMany($organisasi);
                return $dataCaang->pendidikan()->createMany($pendidikan);
            });

            if ($status) {

                Mail::to(trim($data["email"]))->queue(
                    new SendCaang($data["nama_lengkap"]));
                return response()->json(
                    [
                        "message" => "success",
                        "code" => Response::HTTP_CREATED,
                    ],
                    Response::HTTP_CREATED
                );
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        return response()->json(
            [
                "status" => Response::HTTP_OK,
                "data" => Caang::findOrFail($id),
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $per_page = 10;
        if (!is_null($request->input("per_page"))) {
            $per_page = intval($request->input("per_page"));
        }
        return response()->json(
            [
                "status" => Response::HTTP_OK,
                "data" => Caang::with([
                    "pendidikan",
                    "organisasi",
                    "keluarga",
                ])->paginate($per_page),
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id)
    {
        $data = $request->json()->all();
        try {
            $validator = Validator::make($data, [
                "nama_lengkap" => "required|max:255",
                "tempat_lahir" => "required|max:100",
                "tanggal_lahir" => "required",
                "g-recaptcha-response" => "required|captcha",
                "pekerjaan" => "required|max:25",
                "gol_darah" => "required|max:3",
                "status_merital" => "required",
                "email" => [
                    "required",
                    "email",
                    Rule::unique("t_caang")->ignore($id, "id_caang"),
                    Rule::unique("t_anggota"),
                ],
                "no_telpon" => "required|max:15",
                "no_telpon2" => "nullable|max:15",
                "alamat" => "required",
                "foto" => "required",
                "provinsi" => "required",
                "kota" => "required",
                "kecamatan" => "required",
                "desa" => "required",
                "pendidikan_terakhir" => "required",
                "keluarga" => "required",
                "pendidikan" => "required",
                "organisasi" => "required",
                "kode_pj" => "nullable",
                "nama_pj" => "required",
                "tempat_maruf" => "required",
                "tanggal_maruf_start" => "required",
                "tanggal_maruf_end" => "required",
            ]);

            $validator->validate();

            $status = DB::transaction(function () use ($data, $id) {
                $foto = $this->processBase64Photo(
                    $data["foto"],
                    "caang_photo_",
                    boolval(env("CAANG_S3_MODE", true))
                );

                $dataCaang = Caang::findOrFail($id);
                $dataCaang->nama_lengkap = $data["nama_lengkap"];
                $dataCaang->tempat_lahir = $data["tempat_lahir"];
                $dataCaang->gol_darah = $data["gol_darah"];
                $dataCaang->tanggal_lahir = $data["tanggal_lahir"];
                $dataCaang->status_merital = $data["status_merital"];
                $dataCaang->pekerjaan = $data["pekerjaan"];
                $dataCaang->alamat = $data["alamat"];
                $dataCaang->provinsi = $data["provinsi"];
                $dataCaang->kota = $data["kota"];
                $dataCaang->kecamatan = $data["kecamatan"];
                $dataCaang->desa = $data["desa"];
                $dataCaang->no_telpon = $data["no_telpon"];
                $dataCaang->no_telpon2 = $data["no_telpon2"];
                $dataCaang->email = $data["email"];
                $dataCaang->pendidikan_terakhir = $data["pendidikan_terakhir"];
                $dataCaang->last_updated = Carbon::now();
                $dataCaang->foto = $foto;
                $dataCaang->pj = $data["kode_pj"];
                $dataCaang->nama_pj = $data["nama_pj"];
                $dataCaang->tempat_maruf = $data["tempat_maruf"];
                $dataCaang->tanggal_maruf_start = $data["tanggal_maruf_start"];
                $dataCaang->tanggal_maruf_end = $data["tanggal_maruf_end"];
                $keluarga = $data["keluarga"];
                $organisasi = $data["organisasi"];
                $pendidikan = $data["pendidikan"];

                foreach ($keluarga as $value) {
                    CaangKeluarga::updateOrCreate(
                        [
                            "id_keluarga" => isset($value["id_keluarga"])
                                ? $value["id_keluarga"]
                                : null,
                        ],
                        $value
                    );
                }

                foreach ($organisasi as $value) {
                    CaangOrganisasi::updateOrCreate(
                        [
                            "id_organisasi" => isset($value["id_organisasi"])
                                ? $value["id_organisasi"]
                                : null,
                        ],
                        $value
                    );
                }

                foreach ($pendidikan as $value) {
                    CaangPendidikan::updateOrCreate(
                        [
                            "id_pendidikan" => isset($value["id_pendidikan"])
                                ? $value["id_pendidikan"]
                                : null,
                        ],
                        $value
                    );
                }
                $this->sendMessage($dataCaang);
                return $dataCaang->save();

            });

            if ($status) {
                return response()->json(
                    [
                        "message" => "success",
                        "code" => Response::HTTP_OK,
                    ],
                    Response::HTTP_OK
                );
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        return response()->json(
            [
                "status" => Response::HTTP_OK,
                "data" => Caang::destroy($id),
            ],
            Response::HTTP_OK
        );
    }

    private function sendMessage($data)
    {
        $token = env('PUSH_WA_TOKEN');
        $target = $data->no_telpon;
        $url = "https://dash.pushwa.com/api/kirimPesan";

        $message = "_Bismillahirrahmaanirrahiim_,\n\n
        Ahlan wa sahlan bi washiyyati rosulillah akhi {$data->nama_lengkap}.\n\n
        Syukron katsiron! Antum telah menginput data CAANG (Calon Anggota). Kami tunggu Jihad Antum di Pemuda Persis.\n\n
        *Admin PP. Pemuda Persis* \"Ana Muslimun Qabla Kulli Syain\"\n\n
        Dizkro:\n
        - Join telegram group --> https://t.me/csanaonline jika mengalami kendala\n
        - Join telegram group --> https://t.me/kominfopemudapersis\n
        - PLAYLIST VIDEO TUTORIAL ANAONLINE --> https://www.youtube.com/playlist?list=PLyyNZDxI4KezJiXmEJ12823pQH524Fa24";

        $response = Http::post($url, [
            "token" => $token,
            "target" => $target,
            "type" => "image",
            "delay" => "10",
            "message" => $message,
            "url" => "https://annisa-online.s3.ap-southeast-1.amazonaws.com/static/annisa_online.jpeg"
        ]);

        return $response->json();
    }
}
