<?php

namespace App\Http\Controllers;

use App\Model\FileDocument;
use Illuminate\Http\Request;
use App\Facades\Permission;
use DataTables;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class FileDocumentController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index()
    {
        $role = Permission::checkRole("superadmin") || Permission::checkRole("admin_pp") ||
        Permission::checkRole("tasykil_pp");
        return view('ana.document.index_table', ["role" => $role]);
    }

    public function dataTables()
    {
        $role = Permission::checkRole("superadmin") || Permission::checkRole("admin_pp") ||
        Permission::checkRole("tasykil_pp");

        return Datatables::eloquent(FileDocument::with('anggota'))
            ->order(function ($query) {
                $query->orderBy('created_at', 'desc');
            })
            ->addColumn('link', function(FileDocument $file){
                return Storage::disk("s3")->url('files/' . $file->path);
            })
            ->addColumn('can_delete', function() use($role){
                return $role;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function uploadDocument(Request $request)
    {
        try{
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            Storage::disk('s3')->put('files/' . $filename, file_get_contents($file));
            $doc = new FileDocument();
            $doc->id_anggota = Auth::user()->anggota->id_anggota;
            $doc->path = $file->getClientOriginalName();
            $doc->filetype = $extension;
            $doc->created_at = Carbon::now();
            $doc->updated_at = Carbon::now();

            if($doc->save()){
                return response()->json([
                    "message" => "success",
                    "code" => ResponseAlias::HTTP_ACCEPTED
                ], ResponseAlias::HTTP_ACCEPTED);

            }else{
                throw new Exception(
                    "Terjadi kendala teknis", 500);
            }

        }catch(Exception $e){
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], $e->getCode() === 0 ? ResponseAlias::HTTP_INTERNAL_SERVER_ERROR : $e->getCode());
        }
    }

    public function deleteDocument(Request $request)
    {
        try{
            $role = Permission::checkRole("superadmin") || Permission::checkRole("admin_pp") ||
            Permission::checkRole("tasykil_pp");

            if(!$role){
                throw new Exception(
                    "Tidak ada izin delete", ResponseAlias::HTTP_UNAUTHORIZED);
            }

            $doc = FileDocument::find($request->get('id'));

            if(!isset($doc)){
                throw new Exception(
                    "Data tidak ditemukan", ResponseAlias::HTTP_NOT_FOUND);
            }

            Storage::disk('s3')->delete('files/' . $doc->path);

            if($doc->delete()){
                return response()->json([
                    "message" => "success",
                    "code" => ResponseAlias::HTTP_ACCEPTED
                ], ResponseAlias::HTTP_ACCEPTED);

            }else{
                throw new Exception(
                    "Terjadi kendala teknis", 500);
            }

        }catch(Exception $e){
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], $e->getCode() === 0 ? ResponseAlias::HTTP_INTERNAL_SERVER_ERROR : $e->getCode());
        }
    }
}
