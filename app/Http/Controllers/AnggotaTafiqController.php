<?php

namespace App\Http\Controllers;

use App\Model\Anggota;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class AnggotaTafiqController extends Controller
{
    public function index($id_anggota)
    {
        # code...
        $data['anggota'] = Anggota::with('tafiq')->findOrFail($id_anggota);
        return view('ana.anggota.tafiq.form', $data);

    }

    public function update(Request $request)
    {
        # code...
        $id = $request->id;
        $data = $request->all();

        $anggota = Anggota::where('id_anggota', $id)
            ->first();
        if (!$anggota) {
            return response()->json(['message' => 'Not found data'], 404);
        }

        $colTafiq = array();
        foreach ($data as $key => $value) {
            if (strpos($key, 'tafiq_') !== false) {
                $colTafiq[substr($key, strlen('tafiq_'))] = $value;
            }
        }

        $arrTafiq = [];
        foreach ($colTafiq as $key => $val) {
            $arr = [];
            if (count($arrTafiq) == 0) {
                foreach ($val as $k => $v) {
//                    $arrOrganisasi[$k] = ['id_anggota'=> $anggota->id_anggota];
                    $arrTafiq[$k] = [];
                }
            }
            foreach ($val as $k => $v) {
                $arr[$key] = $v;
                $arrTafiq[$k] = Arr::add($arrTafiq[$k], $key, $v);
            }
        }

        DB::beginTransaction();
        try {
            //code...

            $anggota->update(['level_tafiq'=>$request->input('level_tafiq')]);
            $anggota->tafiq()->delete();
            $anggota->tafiq()->createMany($arrTafiq);

            DB::commit();
        } catch (\Throwable $e) {
            //throw $th;
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
        return response()->json(['message' => 'Success updating data', 'data' => 1]);
    }
}

