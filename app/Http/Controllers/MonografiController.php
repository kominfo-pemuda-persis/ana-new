<?php

namespace App\Http\Controllers;

use App\Model\Jamaah;
use App\Model\KejamiyyahanPj;
use App\Model\PC;
use App\Model\PD;
use App\Model\Province;
use App\Model\PW;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MonografiController extends Controller
{
    public function showMonografiPJ()
    {
        $dataPJ = Jamaah::all();
        $dataPC = PC::all();
        $dataPD = PD::all();
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pj.index', compact('dataPC', 'dataPD', 'dataPW', 'provinces', 'dataPJ'));
    }

    public function addMonografiPJ()
    {
        $dataPC = PC::all();
        $dataPD = PD::all();
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pj.add', compact('dataPC', 'dataPD', 'dataPW', 'provinces'));
    }

    public function createMonografiPJ(Request $request)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kode_pj' => 'required',
            'nama_pj' => 'required|max:50',
            'no_pc' => 'required',
            'no_pd' => 'required',
            'no_pw' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'desa' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_telpon' => 'required|max:15',
            'alamat' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'jarak_kabupaten' => 'max:50',
            'ketua' => 'required',
            'sekretaris' => 'required',
            'bendahara' => 'required',
            'hari_ngantor' => 'required',
            'waktu_ngantor' => 'required',
            'musjam_terakhir_masehi' => 'required',
            'musjam_terakhir_hijriyyah' => 'required',
            'anggota_biasa' => 'required',
            'anggota_luar_biasa' => 'nullable',
            'mutasi_persis' => 'nullable',
            'mutasi_tempat' => 'nullable',
            'meninggal_dunia' => 'nullable',
            'tidak_herReg' => 'nullable',
            'mengundurkan_diri' => 'nullable',
            'calon_anggota' => 'nullable',
            'photo' => 'nullable|mimes:jpg,png,jpeg'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => $validation->errors()->getFormat(),
                'status' => $status
            ]);
        } else {
            try {
                DB::beginTransaction();

                $jamaah = new Jamaah();
                $jamaah->kode_pj = $request->Input('kode_pj');
                $jamaah->nama_pj = $request->Input('nama_pj');
                $jamaah->no_pc = $request->Input('no_pc');
                $jamaah->no_pd = $request->Input('no_pd');
                $jamaah->no_pw = $request->Input('no_pw');
                $jamaah->provinsi = $request->Input('provinsi');
                $jamaah->kabupaten = $request->Input('kabupaten');
                $jamaah->kecamatan = $request->Input('kecamatan');
                $jamaah->desa = $request->Input('desa');
                $jamaah->latitude = $request->Input('latitude');
                $jamaah->longitude = $request->Input('longitude');
                $jamaah->email = $request->Input('email');
                $jamaah->no_telpon = $request->Input('no_telpon');
                $jamaah->alamat = $request->Input('alamat');
                $jamaah->luas = $request->Input('luas');
                $jamaah->bw_utara = $request->Input('bw_utara');
                $jamaah->bw_selatan = $request->Input('bw_selatan');
                $jamaah->bw_timur = $request->Input('bw_timur');
                $jamaah->bw_barat = $request->Input('bw_barat');
                $jamaah->jarak_provinsi = $request->Input('jarak_provinsi');
                $jamaah->jarak_kabupaten = $request->Input('jarak_kabupaten');
                $jamaah->created_by = $request->Input('created_by');

                $status = $jamaah->save();

                $jamiyyahPj = new KejamiyyahanPj();
                $jamiyyahPj->kode_pj = $request->Input('kode_pj');
                $jamiyyahPj->nama_pj = $request->Input('nama_pj');
                $jamiyyahPj->no_pc = $request->Input('no_pc');
                $jamiyyahPj->no_pd = $request->Input('no_pd');
                $jamiyyahPj->no_pw = $request->Input('no_pw');
                $jamiyyahPj->provinsi = $request->Input('provinsi');
                $jamiyyahPj->kabupaten = $request->Input('kabupaten');
                $jamiyyahPj->kecamatan = $request->Input('kecamatan');
                $jamiyyahPj->desa = $request->Input('desa');
                $jamiyyahPj->ketua = $request->Input('ketua');
                $jamiyyahPj->sekretaris = $request->Input('sekretaris');
                $jamiyyahPj->bendahara = $request->Input('bendahara');
                $jamiyyahPj->hari_ngantor = $request->Input('hari_ngantor');
                $jamiyyahPj->waktu_ngantor = $request->Input('waktu_ngantor');
                $jamiyyahPj->musjam_terakhir_masehi = $request->Input('musjam_terakhir_masehi');
                $jamiyyahPj->musjam_terakhir_hijriyyah = $request->Input('musjam_terakhir_hijriyyah');
                $jamiyyahPj->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyahPj->anggota_luar_biasa = $request->Input('anggota_luar_biasa');
                $jamiyyahPj->tidak_herReg = $request->Input('tidak_herReg');
                $jamiyyahPj->mutasi_persis = $request->Input('mutasi_persis');
                $jamiyyahPj->mutasi_tempat = $request->Input('mutasi_tempat');
                $jamiyyahPj->mengundurkan_diri = $request->Input('mengundurkan_diri');
                $jamiyyahPj->meninggal_dunia = $request->Input('meninggal_dunia');
                $jamiyyahPj->calon_anggota = $request->Input('calon_anggota');
                $status = $jamiyyahPj->save();

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
            }

            Session::flash('message', 'Data Jamaah berhasil disimpan!');
            $dataPJ = Jamaah::all();
            $dataPC = PC::all();
            $dataPD = PD::all();
            $dataPW = PW::all();
            $provinces = Province::all();
            return view('ana.monografi.pj.index', compact('dataPC', 'dataPD', 'dataPW', 'provinces', 'dataPJ'));
        }
    }

    public function editMonografiPJ($id)
    {
        $dataPJ = Jamaah::find($id);
        $kejamiyyahanPj = KejamiyyahanPj::find($id);
        $dataPC = PC::all();
        $dataPD = PD::all();
        $dataPW = PW::all();
        $provinces = Province::all();

        return view('ana.monografi.pj.edit', compact('dataPC', 'dataPD', 'dataPW', 'provinces', 'dataPJ', 'kejamiyyahanPj'));
    }

//    public function updateMonografiPJ(Request $request, Jamaah $jamaah, KejamiyyahanPj $kejamiyyahanPj)
//    {
//
//        $dataPJ = Jamaah::all();
//        $kejamiyyahanPj = KejamiyyahanPj::all();
//        $dataPC = PC::all();
//        $dataPD = PD::all();
//        $dataPW = PW::all();
//        $provinces = Province::all();
//        return view('ana.monografi.pj.index', compact('dataPC', 'dataPD', 'dataPW', 'provinces', 'dataPJ', 'kejamiyyahanPj'));
//    }
}
