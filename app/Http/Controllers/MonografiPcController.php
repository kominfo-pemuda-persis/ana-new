<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\KejamiyyahanPC;
use App\Model\MonografiPc;
use App\Model\PC;
use App\Model\PD;
use App\Model\Province;
use App\Model\PW;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Throwable;

class MonografiPcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pw = Auth::user()->anggota->pw;
        $pd = Auth::user()->anggota->pd;
        $pc = Auth::user()->anggota->pc;
        $monografiPC = null;

        if ((Permission::checkRole('superadmin')) || (Permission::checkRole('tasykil_pp')) || (Permission::checkRole('admin_pp'))) {
            $monografiPC = MonografiPc::all();
        } elseif ((Permission::checkRole('admin_pw')) || (Permission::checkRole('tasykil_pw'))) {
            $monografiPC = MonografiPc::where('kd_pw', $pw)->get();
        } elseif ((Permission::checkRole('admin_pd')) || (Permission::checkRole('tasykil_pd'))) {
            $monografiPC = MonografiPc::where('kd_pd', $pd)->get();
        } elseif ((Permission::checkRole('admin_pc')) || (Permission::checkRole('tasykil_pc'))) {
            $monografiPC = MonografiPc::where('kd_pc', $pc)->get();
        }

        return view('ana.monografi.pc.index', compact('monografiPC'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pc.add', compact('dataPW', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kd_pc' => 'required',
            'kd_pd' => 'required',
            'kd_pw' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_kontak' => 'required|max:15',
            'alamat_utama' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'jarak_kabupaten' => 'max:50',
            'foto' => 'mimes:jpg,png,jpeg,bmp|file|max:5000'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => $validation->errors(),
                'status' => $status
            ]);
        } else {
            DB::beginTransaction();
            try {


                $cabang = new MonografiPc();
                $cabang->kd_pc = $request->Input('kd_pc');
                $cabang->kd_pd = $request->Input('kd_pd');
                $cabang->kd_pw = $request->Input('kd_pw');
                $cabang->provinsi = $request->Input('provinsi');
                $cabang->kota = $request->Input('kota');
                $cabang->kecamatan = $request->Input('kecamatan');
                $cabang->latitude = $request->Input('latitude');
                $cabang->longitude = $request->Input('longitude');
                $cabang->email = $request->Input('email');
                $cabang->no_kontak = $request->Input('no_kontak');
                $cabang->alamat_utama = $request->Input('alamat_utama');
                $cabang->alamat_alternatif = $request->Input('alamat_alternatif');
                $cabang->luas = $request->Input('luas');
                $cabang->bw_utara = $request->Input('bw_utara');
                $cabang->bw_selatan = $request->Input('bw_selatan');
                $cabang->bw_timur = $request->Input('bw_timur');
                $cabang->bw_barat = $request->Input('bw_barat');
                $cabang->jarak_dari_ibukota_negara = $request->Input('jarak_dari_ibukota_negara');
                $cabang->jarak_dari_ibukota_provinsi = $request->Input('jarak_dari_ibukota_provinsi');
                $cabang->jarak_dari_ibukota_kabupaten = $request->Input('jarak_dari_ibukota_kabupaten');
                $cabang->start_periode = $request->Input('start_periode');
                $cabang->end_periode = $request->Input('end_periode');


                if($request->hasFile('foto')){
                    $file = $request->file('foto');
                    $filename = $request->Input('namaPC') . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    Storage::disk('s3')->put('images/monografi/pc/' . $filename, file_get_contents($file));
                    $cabang->foto = $filename;
                }

                $status = $cabang->save();

                $jamiyyah = new KejamiyyahanPC();
                $jamiyyah->kd_monografi_pc = $cabang->id;
                $jamiyyah->kd_pc = $request->Input('kd_pc');
                $jamiyyah->kd_pd = $request->Input('kd_pd');
                $jamiyyah->kd_pw = $request->Input('kd_pw');
                $jamiyyah->provinsi = $request->Input('provinsi');
                $jamiyyah->kota = $request->Input('kota');
                $jamiyyah->kecamatan = $request->Input('kecamatan');
                $jamiyyah->ketua = $request->Input('ketua');
                $jamiyyah->wkl_ketua = $request->Input('wkl_ketua');
                $jamiyyah->sekretaris = $request->Input('sekretaris');
                $jamiyyah->wkl_sekretaris = $request->Input('wkl_sekretaris');
                $jamiyyah->bendahara = $request->Input('bendahara');
                $jamiyyah->wkl_bendahara = $request->Input('wkl_bendahara');
                $jamiyyah->bid_jamiyyah = $request->Input('bid_jamiyyah');
                $jamiyyah->wkl_bid_jamiyyah = $request->Input('wkl_bid_jamiyyah');
                $jamiyyah->bid_kaderisasi = $request->Input('bid_kaderisasi');
                $jamiyyah->wkl_bid_kaderisasi = $request->Input('wkl_bid_kaderisasi');
                $jamiyyah->bid_administrasi = $request->Input('bid_administrasi');
                $jamiyyah->wkl_bid_administrasi = $request->Input('wkl_bid_administrasi');
                $jamiyyah->bid_pendidikan = $request->Input('bid_pendidikan');
                $jamiyyah->wkl_bid_pendidikan = $request->Input('wkl_bid_pendidikan');
                $jamiyyah->bid_dakwah = $request->Input('bid_dakwah');
                $jamiyyah->wkl_bid_dakwah = $request->Input('wkl_bid_dakwah');
                $jamiyyah->bid_humas_publikasi = $request->Input('bid_humas_publikasi');
                $jamiyyah->wkl_bid_humas_publikasi = $request->Input('wkl_bid_humas_publikasi');
                $jamiyyah->bid_hal = $request->Input('bid_hal');
                $jamiyyah->wkl_bid_hal = $request->Input('wkl_bid_hal');
                $jamiyyah->bid_or_seni = $request->Input('bid_or_seni');
                $jamiyyah->wkl_bid_or_seni = $request->Input('wkl_bid_or_seni');
                $jamiyyah->bid_sosial = $request->Input('bid_sosial');
                $jamiyyah->wkl_bid_sosial = $request->Input('wkl_bid_sosial');
                $jamiyyah->bid_ekonomi = $request->Input('bid_ekonomi');
                $jamiyyah->wkl_bid_ekonomi = $request->Input('wkl_bid_ekonomi');
                $jamiyyah->penasehat1 = $request->Input('penasehat1');
                $jamiyyah->penasehat2 = $request->Input('penasehat2');
                $jamiyyah->penasehat3 = $request->Input('penasehat3');
                $jamiyyah->penasehat4 = $request->Input('penasehat4');
                $jamiyyah->pembantu_umum1 = $request->Input('pembantu_umum1');
                $jamiyyah->pembantu_umum2 = $request->Input('pembantu_umum2');
                $jamiyyah->pembantu_umum3 = $request->Input('pembantu_umum3');
                $jamiyyah->hari = $request->Input('hari');
                $jamiyyah->pukul = $request->Input('pukul');
                $jamiyyah->musycab_terakhir_m = $request->Input('musycab_terakhir_m');
                $jamiyyah->musycab_terakhir_h = $request->Input('musycab_terakhir_h');
                $jamiyyah->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyah->anggota_tersiar = $request->Input('anggota_tersiar');
                $jamiyyah->anggota_istimewa = $request->Input('anggota_istimewa');
                $jamiyyah->tdk_her = $request->Input('tdk_her');
                $jamiyyah->mutasi_ke_persis = $request->Input('mutasi_ke_persis');
                $jamiyyah->mutasi_tempat = $request->Input('mutasi_tempat');
                $jamiyyah->mengundurkan_diri = $request->Input('mengundurkan_diri');
                $jamiyyah->meninggal_dunia = $request->Input('meninggal_dunia');
                $jamiyyah->biasa = $request->Input('biasa');
                $jamiyyah->istimewa = $request->Input('istimewa');
                $jamiyyah->tersiar = $request->Input('tersiar');
                $status = $jamiyyah->save();

                activity('Monografi PC')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PC Created');

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
            }

            return response()->json([
                'status' => $status
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $monografiPC = MonografiPc::find($id);

        if (!$monografiPC) abort(404);

        $kejamiyyahanPC = $monografiPC->kejamiyyahan;
        if (!$kejamiyyahanPC) abort(404);

        return view('ana.monografi.pc.show', compact(
            'monografiPC', 'kejamiyyahanPC'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $monografiPC = MonografiPc::find($id);

        if (!$monografiPC) abort(404);

        $kejamiyyahanPC = $monografiPC->kejamiyyahan;

        if (!$kejamiyyahanPC) abort(404);

        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pc.edit', compact('dataPW',
            'provinces', 'monografiPC', 'kejamiyyahanPC'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kd_pc' => 'required',
            'kd_pd' => 'required',
            'kd_pw' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_kontak' => 'required|max:15',
            'alamat_utama' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'jarak_kabupaten' => 'max:50',
            'foto' => 'mimes:jpg,png,jpeg|file|max:5000'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => $validation->errors(),
                'status' => $status
            ]);
        } else {
            DB::beginTransaction();
            try {


                $cabang = MonografiPc::find($id);

                if (!$cabang) abort(404);

                $cabang->kd_pc = $request->Input('kd_pc');
                $cabang->kd_pd = $request->Input('kd_pd');
                $cabang->kd_pw = $request->Input('kd_pw');
                $cabang->provinsi = $request->Input('provinsi');
                $cabang->kota = $request->Input('kota');
                $cabang->kecamatan = $request->Input('kecamatan');
                $cabang->latitude = $request->Input('latitude');
                $cabang->longitude = $request->Input('longitude');
                $cabang->email = $request->Input('email');
                $cabang->no_kontak = $request->Input('no_kontak');
                $cabang->alamat_utama = $request->Input('alamat_utama');
                $cabang->alamat_alternatif = $request->Input('alamat_alternatif');
                $cabang->luas = $request->Input('luas');
                $cabang->bw_utara = $request->Input('bw_utara');
                $cabang->bw_selatan = $request->Input('bw_selatan');
                $cabang->bw_timur = $request->Input('bw_timur');
                $cabang->bw_barat = $request->Input('bw_barat');
                $cabang->jarak_dari_ibukota_negara = $request->Input('jarak_dari_ibukota_negara');
                $cabang->jarak_dari_ibukota_provinsi = $request->Input('jarak_dari_ibukota_provinsi');
                $cabang->jarak_dari_ibukota_kabupaten = $request->Input('jarak_dari_ibukota_kabupaten');
                $cabang->start_periode = $request->Input('start_periode');
                $cabang->end_periode = $request->Input('end_periode');

                if($request->hasFile('foto')){
                    $file = $request->file('foto');
                    $filename = $request->namaPC . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    Storage::disk('s3')->put('images/monografi/pc/' . $filename, file_get_contents($file));
                    $cabang->foto = $filename;
                }

                $status = $cabang->save();

                $jamiyyah = $cabang->kejamiyyahan;

                if (!$jamiyyah) abort(404);

                $jamiyyah->kd_pc = $request->Input('kd_pc');
                $jamiyyah->kd_pd = $request->Input('kd_pd');
                $jamiyyah->kd_pw = $request->Input('kd_pw');
                $jamiyyah->provinsi = $request->Input('provinsi');
                $jamiyyah->kota = $request->Input('kota');
                $jamiyyah->kecamatan = $request->Input('kecamatan');
                $jamiyyah->ketua = $request->Input('ketua');
                $jamiyyah->wkl_ketua = $request->Input('wkl_ketua');
                $jamiyyah->sekretaris = $request->Input('sekretaris');
                $jamiyyah->wkl_sekretaris = $request->Input('wkl_sekretaris');
                $jamiyyah->bendahara = $request->Input('bendahara');
                $jamiyyah->wkl_bendahara = $request->Input('wkl_bendahara');
                $jamiyyah->bid_jamiyyah = $request->Input('bid_jamiyyah');
                $jamiyyah->wkl_bid_jamiyyah = $request->Input('wkl_bid_jamiyyah');
                $jamiyyah->bid_kaderisasi = $request->Input('bid_kaderisasi');
                $jamiyyah->wkl_bid_kaderisasi = $request->Input('wkl_bid_kaderisasi');
                $jamiyyah->bid_administrasi = $request->Input('bid_administrasi');
                $jamiyyah->wkl_bid_administrasi = $request->Input('wkl_bid_administrasi');
                $jamiyyah->bid_pendidikan = $request->Input('bid_pendidikan');
                $jamiyyah->wkl_bid_pendidikan = $request->Input('wkl_bid_pendidikan');
                $jamiyyah->bid_dakwah = $request->Input('bid_dakwah');
                $jamiyyah->wkl_bid_dakwah = $request->Input('wkl_bid_dakwah');
                $jamiyyah->bid_humas_publikasi = $request->Input('bid_humas_publikasi');
                $jamiyyah->wkl_bid_humas_publikasi = $request->Input('wkl_bid_humas_publikasi');
                $jamiyyah->bid_hal = $request->Input('bid_hal');
                $jamiyyah->wkl_bid_hal = $request->Input('wkl_bid_hal');
                $jamiyyah->bid_or_seni = $request->Input('bid_or_seni');
                $jamiyyah->wkl_bid_or_seni = $request->Input('wkl_bid_or_seni');
                $jamiyyah->bid_sosial = $request->Input('bid_sosial');
                $jamiyyah->wkl_bid_sosial = $request->Input('wkl_bid_sosial');
                $jamiyyah->bid_ekonomi = $request->Input('bid_ekonomi');
                $jamiyyah->wkl_bid_ekonomi = $request->Input('wkl_bid_ekonomi');
                $jamiyyah->penasehat1 = $request->Input('penasehat1');
                $jamiyyah->penasehat2 = $request->Input('penasehat2');
                $jamiyyah->penasehat3 = $request->Input('penasehat3');
                $jamiyyah->penasehat4 = $request->Input('penasehat4');
                $jamiyyah->pembantu_umum1 = $request->Input('pembantu_umum1');
                $jamiyyah->pembantu_umum2 = $request->Input('pembantu_umum2');
                $jamiyyah->pembantu_umum3 = $request->Input('pembantu_umum3');
                $jamiyyah->hari = $request->Input('hari');
                $jamiyyah->pukul = $request->Input('pukul');
                $jamiyyah->musycab_terakhir_m = $request->Input('musycab_terakhir_m');
                $jamiyyah->musycab_terakhir_h = $request->Input('musycab_terakhir_h');
                $jamiyyah->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyah->anggota_tersiar = $request->Input('anggota_tersiar');
                $jamiyyah->anggota_istimewa = $request->Input('anggota_istimewa');
                $jamiyyah->tdk_her = $request->Input('tdk_her');
                $jamiyyah->mutasi_ke_persis = $request->Input('mutasi_ke_persis');
                $jamiyyah->mutasi_tempat = $request->Input('mutasi_tempat');
                $jamiyyah->mengundurkan_diri = $request->Input('mengundurkan_diri');
                $jamiyyah->meninggal_dunia = $request->Input('meninggal_dunia');
                $jamiyyah->biasa = $request->Input('biasa');
                $jamiyyah->istimewa = $request->Input('istimewa');
                $jamiyyah->tersiar = $request->Input('tersiar');
                $status = $jamiyyah->save();

                activity('Monografi PC')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PC Updated');

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
            }

            return response()->json([
                'status' => $status
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MonografiPc::find($id)->delete();
        activity('Monografi PC')
            ->performedOn(Auth::user())
            ->causedBy(Auth::user())
            ->log('Monografi PC Deleted');
        return response()->json($data);
    }
}
