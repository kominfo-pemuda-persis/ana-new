<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\PC;
use App\Model\PD;
use App\Model\PerformaPC;
use App\Model\Province;
use App\Model\PW;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PerformaPcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pw = Auth::user()->anggota->pw;
        $pd = Auth::user()->anggota->pd;
        $pc = Auth::user()->anggota->pc;
        $performaPC = null;

        if (Permission::checkRole('superadmin') ||
            Permission::checkRole('tasykil_pp') ||
            Permission::checkRole('admin_pp')) {
            $performaPC = PerformaPC::with('pw','pd','pc')->get();
        } elseif ((Permission::checkRole('admin_pw')) || (Permission::checkRole('tasykil_pw'))) {
            $performaPC = PerformaPC::with('pw','pd','pc')->where('kd_pw', $pw)->get();
        } elseif ((Permission::checkRole('admin_pd')) || (Permission::checkRole('tasykil_pd'))) {
            $performaPC = PerformaPC::with('pw','pd','pc')->where('kd_pd', $pd)->get();
        } elseif ((Permission::checkRole('admin_pc')) || (Permission::checkRole('tasykil_pc'))) {
            $performaPC = PerformaPC::with('pw','pd','pc')->where('kd_pc', $pc)->get();
        }
        return view('ana.performa.pc.index', compact('performaPC'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.performa.pc.add', compact('dataPW', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $performa = $request->except([
            "_token", "namaPD", "namaPW","namaPC",
            "provinces", "regencies", "districts"
        ]);

        $now = Carbon::now();
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['kd_pc'] = $data['kd_pc'];
        $performa['kd_pd'] = $data['kd_pd'];
        $performa['kd_pw'] = $data['kd_pw'];
        $performa['provinsi'] = $data['provinces'];
        $performa['kota'] = $data['regencies'];
        $performa['kecamatan'] = $data['districts'];

        if ($request->hasFile("foto")) {
            $file = $request->file('foto');
            $filename = $performa['kd_pc'] . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->put('images/performa/pc/' . $data['namaPW'] . '/' . $data['namaPD'] . '/' . $data['namaPC'] . '/' . $filename, file_get_contents($file));
            $performa['foto'] = $filename;
        } else {
            $performa['foto'] = "default.png";
        }

        $status = PerformaPC::insert($performa);


        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PerformaPC::find($id);

        if(!$data) abort(404);

        return view('ana.performa.pc.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PerformaPC::find($id);

        if(!$data) abort(404);

        $dataPW = PW::all();
        $provinces = Province::all();

        return view('ana.performa.pc.edit', compact('dataPW',
            'provinces', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $performa = $request->except([
            "_token", "namaPD", "namaPW",
            "noPD", "noPW", "noPC", "namaPC",
            "provinces",
            "regencies", "districts", "_method"
        ]);

        $now = Carbon::now();
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['last_survey'] = $now;
        $performa['created_at'] = $now;
        $performa['updated_at'] = $now;
        $performa['kd_pc'] = $data['noPC'];
        $performa['kd_pd'] = $data['noPD'];
        $performa['kd_pw'] = $data['noPW'];
        $performa['provinsi'] = $data['provinces'];
        $performa['kota'] = $data['regencies'];
        $performa['kecamatan'] = $data['districts'];


        if ($request->hasFile("foto")) {
            $file = $request->file('foto');
            $filename = $performa['kd_pc'] . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->put('images/performa/pc/' . $data['namaPW'] . '/' . $data['namaPD'] . '/' . $data['namaPC'] . '/' . $filename, file_get_contents($file));
            $performa['foto'] = $filename;
        }

        $record = PerformaPC::find($id);

        foreach ($performa as $index => $item) {
            $record->$index = $item;
        }

        $status = $record->save();

        return response()->json([
            'status' => $status
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PerformaPC::find($id)->delete();
        return response()->json($data);
    }
}
