<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\KejamiyyahanPW;
use App\Model\MonografiPw;
use App\Model\PC;
use App\Model\PD;
use App\Model\Province;
use App\Model\PW;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Throwable;

class MonografiPwController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pw = Auth::user()->anggota->pw;
        $monografiPW = null;

        if ((Permission::checkRole('superadmin')) || (Permission::checkRole('tasykil_pp')) || (Permission::checkRole('admin_pp'))) {
            $monografiPW = MonografiPw::all();
        } else if ((Permission::checkRole('admin_pw')) || (Permission::checkRole('tasykil_pw'))) {
            $monografiPW = MonografiPw::where('kd_pw', $pw)->get();
        }

        return view('ana.monografi.pw.index', compact('monografiPW'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataPC = PC::all();
        $dataPD = PD::all();
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pw.add', compact('dataPC', 'dataPD', 'dataPW', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kd_pw' => 'required',
            'nama_pw' => 'required',
            'provinsi' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_kontak' => 'required|max:15',
            'alamat_utama' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'foto' => 'mimes:jpg,png,jpeg,bmp|file|max:5000'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => $validation->errors()->getFormat(),
                'status' => $status
            ]);
        } else {
            DB::beginTransaction();
            try {


                $wilayah = new MonografiPw();
                $wilayah->kd_pw = $request->Input('kd_pw');
                $wilayah->nama_pw = $request->Input('nama_pw');
                $wilayah->start_periode = $request->Input('start_periode');
                $wilayah->end_periode = $request->Input('end_periode');
                $wilayah->provinsi = $request->Input('provinsi');
                $wilayah->latitude = $request->Input('latitude');
                $wilayah->longitude = $request->Input('longitude');
                $wilayah->email = $request->Input('email');
                $wilayah->no_kontak = $request->Input('no_kontak');
                $wilayah->alamat_utama = $request->Input('alamat_utama');
                $wilayah->alamat_alternatif = $request->Input('alamat_alternatif');
                $wilayah->luas = $request->Input('luas');
                $wilayah->bw_utara = $request->Input('bw_utara');
                $wilayah->bw_selatan = $request->Input('bw_selatan');
                $wilayah->bw_timur = $request->Input('bw_timur');
                $wilayah->bw_barat = $request->Input('bw_barat');
                $wilayah->jarak_dari_ibukota_negara = $request->Input('jarak_dari_ibukota_negara');
                $wilayah->jarak_dari_ibukota_provinsi = $request->Input('jarak_dari_ibukota_provinsi');

                if($request->hasFile('foto')){
                    $file = $request->file('foto');
                    $filename = $request->nama_pw . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    Storage::disk('s3')->put('images/monografi/pw/' . $filename, file_get_contents($file));
                    $wilayah->foto = $filename;
                }

                $status = $wilayah->save();

                $jamiyyah = new KejamiyyahanPW();
                $jamiyyah->kd_monografi_pw = $wilayah->id;
                $jamiyyah->kd_pw = $request->Input('kd_pw');
                $jamiyyah->nama_pw = $request->Input('nama_pw');
                $jamiyyah->provinsi = $request->Input('provinsi');
                $jamiyyah->ketua = $request->Input('ketua');
                $jamiyyah->wkl_ketua = $request->Input('wkl_ketua');
                $jamiyyah->sekretaris = $request->Input('sekretaris');
                $jamiyyah->wkl_sekretaris = $request->Input('wkl_sekretaris');
                $jamiyyah->bendahara = $request->Input('bendahara');
                $jamiyyah->wkl_bendahara = $request->Input('wkl_bendahara');
                $jamiyyah->bid_jamiyyah = $request->Input('bid_jamiyyah');
                $jamiyyah->wkl_bid_jamiyyah = $request->Input('wkl_bid_jamiyyah');
                $jamiyyah->bid_kaderisasi = $request->Input('bid_kaderisasi');
                $jamiyyah->wkl_bid_kaderisasi = $request->Input('wkl_bid_kaderisasi');
                $jamiyyah->bid_administrasi = $request->Input('bid_administrasi');
                $jamiyyah->wkl_bid_administrasi = $request->Input('wkl_bid_administrasi');
                $jamiyyah->bid_pendidikan = $request->Input('bid_pendidikan');
                $jamiyyah->wkl_bid_pendidikan = $request->Input('wkl_bid_pendidikan');
                $jamiyyah->bid_dakwah = $request->Input('bid_dakwah');
                $jamiyyah->wkl_bid_dakwah = $request->Input('wkl_bid_dakwah');
                $jamiyyah->bid_humas_publikasi = $request->Input('bid_humas_publikasi');
                $jamiyyah->wkl_bid_humas_publikasi = $request->Input('wkl_bid_humas_publikasi');
                $jamiyyah->bid_hal = $request->Input('bid_hal');
                $jamiyyah->wkl_bid_hal = $request->Input('wkl_bid_hal');
                $jamiyyah->bid_or_seni = $request->Input('bid_or_seni');
                $jamiyyah->wkl_bid_or_seni = $request->Input('wkl_bid_or_seni');
                $jamiyyah->bid_sosial = $request->Input('bid_sosial');
                $jamiyyah->wkl_bid_sosial = $request->Input('wkl_bid_sosial');
                $jamiyyah->bid_ekonomi = $request->Input('bid_ekonomi');
                $jamiyyah->wkl_bid_ekonomi = $request->Input('wkl_bid_ekonomi');
                $jamiyyah->penasehat1 = $request->Input('penasehat1');
                $jamiyyah->penasehat2 = $request->Input('penasehat2');
                $jamiyyah->penasehat3 = $request->Input('penasehat3');
                $jamiyyah->penasehat4 = $request->Input('penasehat4');
                $jamiyyah->pembantu_umum1 = $request->Input('pembantu_umum1');
                $jamiyyah->pembantu_umum2 = $request->Input('pembantu_umum2');
                $jamiyyah->pembantu_umum3 = $request->Input('pembantu_umum3');
                $jamiyyah->hari = $request->Input('hari');
                $jamiyyah->pukul = $request->Input('pukul');
                $jamiyyah->muswil_terakhir_m = $request->Input('muswil_terakhir_m');
                $jamiyyah->muswil_terakhir_h = $request->Input('muswil_terakhir_h');
                $jamiyyah->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyah->anggota_tersiar = $request->Input('anggota_tersiar');
                $jamiyyah->anggota_istimewa = $request->Input('anggota_istimewa');
                $status = $jamiyyah->save();

                activity('Monografi PW')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PW Created');

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
            }

            return response()->json([
                'status' => $status
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $monografiPW = MonografiPW::find($id);

        if (!$monografiPW) abort(404);

        $kejamiyyahanPW = $monografiPW;
        if (!$kejamiyyahanPW) abort(404);

        return view('ana.monografi.pw.show', compact(
            'monografiPW', 'kejamiyyahanPW'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $monografiPW = MonografiPw::find($id);

        if (!$monografiPW) { Log::debug("not monografi"); abort(404);}

        $kejamiyyahanPW = $monografiPW;

        if (!$kejamiyyahanPW) { Log::debug("not kejamiyyahan"); abort(404);}

        $dataPC = PC::all();
        $dataPD = PD::all();
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pw.edit', compact(
            'dataPC', 'dataPD', 'dataPW',
            'provinces', 'monografiPW', 'kejamiyyahanPW'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kd_pw' => 'required',
            'nama_pw' => 'required',
            'provinsi' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_kontak' => 'required|max:15',
            'alamat_utama' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'foto' => 'mimes:jpg,png,jpeg,bmp|file|max:5000'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => $validation->errors()->getFormat(),
                'status' => $status
            ]);
        } else {
            DB::beginTransaction();
            try {


                $wilayah = MonografiPw::find($id);

                $wilayah->kd_pw = $request->Input('kd_pw');
                $wilayah->nama_pw = $request->Input('nama_pw');
                $wilayah->start_periode = $request->Input('start_periode');
                $wilayah->end_periode = $request->Input('end_periode');
                $wilayah->provinsi = $request->Input('provinsi');
                $wilayah->latitude = $request->Input('latitude');
                $wilayah->longitude = $request->Input('longitude');
                $wilayah->email = $request->Input('email');
                $wilayah->no_kontak = $request->Input('no_kontak');
                $wilayah->alamat_utama = $request->Input('alamat_utama');
                $wilayah->alamat_alternatif = $request->Input('alamat_alternatif');
                $wilayah->luas = $request->Input('luas');
                $wilayah->bw_utara = $request->Input('bw_utara');
                $wilayah->bw_selatan = $request->Input('bw_selatan');
                $wilayah->bw_timur = $request->Input('bw_timur');
                $wilayah->bw_barat = $request->Input('bw_barat');
                $wilayah->jarak_dari_ibukota_negara = $request->Input('jarak_dari_ibukota_negara');
                $wilayah->jarak_dari_ibukota_provinsi = $request->Input('jarak_dari_ibukota_provinsi');

                if($request->hasFile('foto')){
                    $file = $request->file('foto');
                    $filename = $request->nama_pw . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    Storage::disk('s3')->put('images/monografi/pw/' . $filename, file_get_contents($file));
                    $wilayah->foto = $filename;
                }

                $status = $wilayah->save();

                $jamiyyah = $wilayah->kejamiyyahan;

                if (!$jamiyyah) abort(404);

                $jamiyyah->kd_monografi_pw = $wilayah->id;
                $jamiyyah->kd_pw = $request->Input('kd_pw');
                $jamiyyah->nama_pw = $request->Input('nama_pw');
                $jamiyyah->provinsi = $request->Input('provinsi');
                $jamiyyah->ketua = $request->Input('ketua');
                $jamiyyah->wkl_ketua = $request->Input('wkl_ketua');
                $jamiyyah->sekretaris = $request->Input('sekretaris');
                $jamiyyah->wkl_sekretaris = $request->Input('wkl_sekretaris');
                $jamiyyah->bendahara = $request->Input('bendahara');
                $jamiyyah->wkl_bendahara = $request->Input('wkl_bendahara');
                $jamiyyah->bid_jamiyyah = $request->Input('bid_jamiyyah');
                $jamiyyah->wkl_bid_jamiyyah = $request->Input('wkl_bid_jamiyyah');
                $jamiyyah->bid_kaderisasi = $request->Input('bid_kaderisasi');
                $jamiyyah->wkl_bid_kaderisasi = $request->Input('wkl_bid_kaderisasi');
                $jamiyyah->bid_administrasi = $request->Input('bid_administrasi');
                $jamiyyah->wkl_bid_administrasi = $request->Input('wkl_bid_administrasi');
                $jamiyyah->bid_pendidikan = $request->Input('bid_pendidikan');
                $jamiyyah->wkl_bid_pendidikan = $request->Input('wkl_bid_pendidikan');
                $jamiyyah->bid_dakwah = $request->Input('bid_dakwah');
                $jamiyyah->wkl_bid_dakwah = $request->Input('wkl_bid_dakwah');
                $jamiyyah->bid_humas_publikasi = $request->Input('bid_humas_publikasi');
                $jamiyyah->wkl_bid_humas_publikasi = $request->Input('wkl_bid_humas_publikasi');
                $jamiyyah->bid_hal = $request->Input('bid_hal');
                $jamiyyah->wkl_bid_hal = $request->Input('wkl_bid_hal');
                $jamiyyah->bid_or_seni = $request->Input('bid_or_seni');
                $jamiyyah->wkl_bid_or_seni = $request->Input('wkl_bid_or_seni');
                $jamiyyah->bid_sosial = $request->Input('bid_sosial');
                $jamiyyah->wkl_bid_sosial = $request->Input('wkl_bid_sosial');
                $jamiyyah->bid_ekonomi = $request->Input('bid_ekonomi');
                $jamiyyah->wkl_bid_ekonomi = $request->Input('wkl_bid_ekonomi');
                $jamiyyah->penasehat1 = $request->Input('penasehat1');
                $jamiyyah->penasehat2 = $request->Input('penasehat2');
                $jamiyyah->penasehat3 = $request->Input('penasehat3');
                $jamiyyah->penasehat4 = $request->Input('penasehat4');
                $jamiyyah->pembantu_umum1 = $request->Input('pembantu_umum1');
                $jamiyyah->pembantu_umum2 = $request->Input('pembantu_umum2');
                $jamiyyah->pembantu_umum3 = $request->Input('pembantu_umum3');
                $jamiyyah->hari = $request->Input('hari');
                $jamiyyah->pukul = $request->Input('pukul');
                $jamiyyah->muswil_terakhir_m = $request->Input('muswil_terakhir_m');
                $jamiyyah->muswil_terakhir_h = $request->Input('muswil_terakhir_h');
                $jamiyyah->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyah->anggota_tersiar = $request->Input('anggota_tersiar');
                $jamiyyah->anggota_istimewa = $request->Input('anggota_istimewa');
                $status = $jamiyyah->save();

                activity('Monografi PW')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PW Updated');

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
            }

            return response()->json([
                'status' => $status
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MonografiPw::find($id)->delete();
        activity('Monografi PW')
            ->performedOn(Auth::user())
            ->causedBy(Auth::user())
            ->log('Monografi PW Deleted');
        return response()->json($data);
    }
}
