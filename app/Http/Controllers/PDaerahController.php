<?php

namespace App\Http\Controllers;

use App\Model\PD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Facades\Permission;

class PDaerahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ana.pimpinan.pd.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = PD::create($request->all());

        if ($data) {
            return response()->json(['message' => 'Success adding data', 'data' => 1]);
        } else {
            return response()->json(['message' => 'Failed adding data'], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pd = PD::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $dataPd= PD::with(["pw"])
            ->where('kd_pd', $id)
            ->first();

        if (!$dataPd) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json(['message' => 'Success getting data', 'data' => $dataPd]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $mutasi = PD::where('kd_pd', $data['id_prev'])
            ->first();

        unset($data['id_prev']);

        if(!$mutasi) {
            return response()->json(['message' => 'Not found data'], 404);
        }else{
            $mutasi->kd_pd = $data['kd_pd'];
            $mutasi->nama_pd = $data['nama_pd'];
            $mutasi->diresmikan = $data['diresmikan'];
            $mutasi->kd_pw = $data['kd_pw'];
            $mutasi->save();
            return response()->json(['message' => 'Success updating data', 'data' => 1]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $destroyData = PD::where('kd_pd', $id)->delete();
        // $destroyData->delete($id);
        if ($destroyData) {
            return response()->json(['message' => 'Success deleting data', 'data' => $destroyData]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }

    }

    public function datatables(){

        $pd = PD::with(['pw'])->get();
        $has_permission = Permission::checkPermission('masterpd.update') || Permission::checkPermission('masterpd.delete');
        return Datatables::of($pd)
            ->addIndexColumn()
            ->addColumn("action", "ana.datatables.pd.action")
            ->addColumn("has_permission", $has_permission)
            ->make(true);
    }

    public function getRelatedPC($kd_pd)
    {
        return PD::where('kd_pd', $kd_pd)->first()
            ->relatedPC;
    }

    public function getPcByName(Request $request)
    {
        $data = DB::table("t_pd");
        $q = $request->get('q');
        $kd_pw = $request->get('kd_pw');
        if ($request->has('q')) {
            $data->where('nama_pd', 'like', "%{$q}%");
        }

        if ($request->has('kd_pw')) {
            $data->where('kd_pw', $kd_pw);
        }
        $data = $data->take(10)->get();

        if (!$data) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json($data);
    }
}
