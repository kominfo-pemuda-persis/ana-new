<?php

namespace App\Http\Controllers;

use App\Model\JamiyyahMaster;
use App\Model\PC;
use App\Model\PD;
use App\Model\Province;
use App\Model\PW;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class JamiyyahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('ana.jamiyyah.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function showPD($id)
    {
        $data['id_pw'] = $id;

        $pw = JamiyyahMaster::where('id_wilayah', $id)->first();
        if($pw->pimpinan == 'PW'){
            $nama_pw = "Pimpinan Wilayah ".$pw->nama_pimpinan;
        }else {
            $nama_pw = "Perwakilan Pimpinan Wilayah ".$pw->nama_pimpinan;
        }
        $data['nama_pw'] = $nama_pw;
        return view('ana.jamiyyah.index_pd', $data);

    }

    public function picker(Request $request)
    {
        if ($request->has('q')) {
            $q = $request->get('q');
            $data = PC::where('nama_pc', 'like', '%'.$q.'%')
            ->orWhere('kd_pc', 'like', '%'.$q.'%')
            ->take(20)
            ->get();
            if (!$data) {
                return response()->json(['message' => 'Not found data'], 404);
            }
            return response()->json($data);
        }
    }

    public function showPC($id)
    {
        $data['id_pd'] = $id;
        $data['id_pw'] = substr($id,0,2);

        $pd = JamiyyahMaster::where('id_wilayah', $id)->first();
        if($pd->pimpinan == 'PD'){
            $nama_pd = "Pimpinan Daerah ".$pd->nama_pimpinan;
        }else {
            $nama_pd = "Pimpinan Cabang (Non PD) ".$pd->nama_pimpinan;
        }
        $data['nama_pd'] = $nama_pd;

        return view('ana.jamiyyah.index_pc', $data);

    }

    public function datatablesPW(){
        //dd($p);
        $data =  JamiyyahMaster::whereIn('pimpinan',array("PW","Perwakilan PW"));

        return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function datatablesPD($id){
        //dd($id);
        $data = DB::Table('t_master_jamiyyah')->select('*')
            ->whereIn('pimpinan', array("PD","PC Non PD"))
            ->where('id_wilayah','LIKE',$id.'%');

        return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function datatablesPC($id){
        //dd($id);
        $data = DB::Table('t_master_jamiyyah')->select('*')
            ->whereIn('pimpinan', array("PC"))
            ->where('id_wilayah','LIKE',$id.'%');

        return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function showMonografiPC()
    {
        $dataPC = PC::all();
        $dataPD = PD::all();
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pc.index', compact('dataPC', 'dataPD', 'dataPW', 'provinces'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
