<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\PW;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class PwController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("ana.jamiyyah.pw.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = PW::create($request->all());

        if ($data) {
            return response()->json([
                "message" => "Success adding data",
                "data" => 1,
            ]);
        } else {
            return response()->json(["message" => "Failed adding data"], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function datatables()
    {
        $pw = DB::table("t_pw")
            ->select("kd_pw", "nama_pw", "diresmikan")
            ->get();
        $has_permission = Permission::checkPermission('masterpw.delete') || Permission::checkPermission('masterpw.update');
        return Datatables::of($pw)
            ->addIndexColumn()
            ->addColumn("action", "ana.datatables.pw.action")
            ->addColumn("has_permission", $has_permission)
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pw = PW::find($id);
    }

    public function get($id)
    {
        $dataPw = DB::table("t_pw")
            ->where("kd_pw", $id)
            ->select("kd_pw", "nama_pw", "diresmikan")
            ->first();

        if (!$dataPw) {
            return response()->json(["message" => "Not found data"], 404);
        }
        return response()->json([
            "message" => "Success getting data",
            "data" => $dataPw,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $mutasi = PW::where("kd_pw", $data["id_prev"])->first();

        unset($data["id_prev"]);

        if (!$mutasi) {
            return response()->json(["message" => "Not found data"], 404);
        } else {
            $mutasi->kd_pw = $data["kd_pw"];
            $mutasi->nama_pw = $data["nama_pw"];
            $mutasi->diresmikan = $data["diresmikan"];
            $mutasi->save();
            return response()->json([
                "message" => "Success updating data",
                "data" => 1,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $destroyData = PW::where("kd_pw", $id)->delete();
        // $destroyData->delete($id);
        if ($destroyData) {
            return response()->json([
                "message" => "Success deleting data",
                "data" => $destroyData,
            ]);
        } else {
            return response()->json(["message" => "Failed deleting data"], 500);
        }
    }

    public function getRelatedPD($kd_pw)
    {
        return PW::where("kd_pw", $kd_pw)->first()->relatedPD;
    }

    public function getAllPwPublic()
    {
        $pw = DB::table("t_pw")
            ->select("kd_pw", "nama_pw")
            ->get();
        return $pw;
    }

    public function getPWByName(Request $request)
    {
        $data = DB::table("t_pw");
        if ($request->has('q')) {
            $q = $request->get('q');
            $data->where('nama_pw', 'like', '%'.$q.'%');
        }
        $data = $data->take(10)->get();

        if (!$data) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json($data);
    }
}
