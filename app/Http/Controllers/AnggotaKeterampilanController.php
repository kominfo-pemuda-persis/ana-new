<?php

namespace App\Http\Controllers;

use DataTables;
use Storage;
use DB;
use Illuminate\Http\Request;
use App\Model\Anggota;
use App\Model\AnggotaKeterampilan;

class AnggotaKeterampilanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_anggota)
    {
        $id = $id_anggota;
        $data['anggota'] = Anggota::find($id);
        return view('ana.anggota.keterampilan.index', $data);
    }

    public function datatables($id_anggota){
        $anggotaKeterampilan = AnggotaKeterampilan::select('*')
            ->where('id_anggota', $id_anggota)
            ->orderBy('id_keterampilan', 'asc')
            ->get();
        return Datatables::of($anggotaKeterampilan)
        ->addIndexColumn()
        ->make(true);
    }

    public function get(Request $request)
    {
        $id = $request->id;
        $keterampilan= AnggotaKeterampilan::where('id_keterampilan', $id)
        ->first();
        if (!$keterampilan) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json(['message' => 'Success getting data', 'data' => $keterampilan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $createKeterampilan = AnggotaKeterampilan::create($data);

        if ($createKeterampilan) {
            return response()->json(['message' => 'Success adding data', 'data' => 1]);
        } else {
            return response()->json(['message' => 'Failed adding data'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $data = $request->all();

        $keterampilan = AnggotaKeterampilan::where('id_keterampilan',$id)
        ->first();
        if(!$keterampilan) {
            return response()->json(['message' => 'Not found data'], 404);
        }else{
            $keterampilan->update($data);
            return response()->json(['message' => 'Success updating data', 'data' => 1]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $destroyData = AnggotaKeterampilan::where('id_keterampilan', $id)->first();
        $destroyData->delete();
        if ($destroyData) {
            return response()->json(['message' => 'Success deleting data', 'data' => $destroyData]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }
    }

    public function dataTablesTrash($id_anggota)
    {
        $anggota_keluarga = AnggotaKeterampilan::select('*')
            ->onlyTrashed()
            ->where('id_anggota', $id_anggota)
            ->orderBy('id_anggota', 'asc')
            ->get();
        return Datatables::of($anggota_keluarga)
            ->addIndexColumn()
            ->make(true);
    }

    public function trash($id_anggota)
    {
        $data['id_anggota'] = $id_anggota;
        return view('ana.anggota.keterampilan.trash', $data);
    }

    public function restore($id_anggota, $id_anggota_keluarga)
    {
        $restoreAnggota = AnggotaKeterampilan::withTrashed()->find($id_anggota_keluarga)->restore();
        if ($restoreAnggota) {
            return response()->json(['message' => 'Success restore data', 'data' => $restoreAnggota]);
        } else {
            return response()->json(['message' => 'Failed restore data'], 500);
        }
    }
}
