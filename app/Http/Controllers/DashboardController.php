<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\Anggota;
use App\Model\HerRegistration;
use App\Model\PD;
use App\Model\PW;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $anggota = new Anggota();
        $heregistrasi = new HerRegistration();
        $loggedUser = Auth::user()->anggota;
        $sum_monografis = null;
        $sum_performas = null;
        $sum_tafiq = [];
        $data = [];

        $counterMonografiPJ = DB::table("t_monografi_pj");
        $counterMonografiPC = DB::table("t_monografi_pc");
        $counterMonografiPD = DB::table("t_monografi_pd");
        $counterMonografiPW = DB::table("t_monografi_pw");

        $counterPerformaPJ = DB::table("t_performa_pj");
        $counterPerformaPC = DB::table("t_performa_pc");
        $counterPerformaPD = DB::table("t_performa_pd");
        $counterPerformaPW = DB::table("t_performa_pw");

        $counterTafiq0 = DB::table("t_anggota")
            ->where("level_tafiq", 0)
            ->where("status_aktif", "ACTIVE")
            ->where("status_her", "SUDAH_BAYAR")
            ->count();
        $counterTafiq1 = DB::table("t_anggota")
            ->where("level_tafiq", 1)
            ->where("status_aktif", "ACTIVE")
            ->where("status_her", "SUDAH_BAYAR")
            ->count();
        $counterTafiq2 = DB::table("t_anggota")
            ->where("level_tafiq", 2)
            ->where("status_aktif", "ACTIVE")
            ->where("status_her", "SUDAH_BAYAR")
            ->count();
        $counterTafiq3 = DB::table("t_anggota")
            ->where("level_tafiq", 3)
            ->where("status_aktif", "ACTIVE")
            ->where("status_her", "SUDAH_BAYAR")
            ->count();

        if (
            Permission::checkRole("superadmin") ||
            Permission::checkRole("tasykil_pp") ||
            Permission::checkRole("admin_pp")
        ) {
            $sum_monografis = [
                "PJ" => $counterMonografiPJ->count(),
                "PC" => $counterMonografiPC->count(),
                "PD" => $counterMonografiPD->count(),
                "PW" => $counterMonografiPW->count(),
            ];

            $sum_performas = [
                "PJ" => $counterPerformaPJ->count(),
                "PC" => $counterPerformaPC->count(),
                "PD" => $counterPerformaPD->count(),
                "PW" => $counterPerformaPW->count(),
            ];
            $sum_tafiq = [
                "Belum Tafiq" => $counterTafiq0,
                "Tafiq 1" => $counterTafiq1,
                "Tafiq 2" => $counterTafiq2,
                "Tafiq 3" => $counterTafiq3,
            ];

            $data = array_merge($data, [
                "alreadyRegs" => $anggota->alreadyRegistered(),
                "usiaSummary" => $anggota->usiaSummary(),
                "meritalSummary" => $anggota->meritalSummary(),
                "statusKeanggotaanSummary" => $anggota->statusKeanggotaanSummary(),
                "statusSummary" => $anggota->statusSummary(),
                "golDarahSummary" => $anggota->golDarahSummary(),
                "pendidikanSummary" => $anggota->pendidikanSummary(),
                "anggota" => DB::table("t_anggota")
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "heregistrasi" => DB::table("t_anggota")
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "userTeregistrasi" => DB::table("t_login")->count(),
            ]);
        } elseif (
            Permission::checkRole("admin_pj") ||
            Permission::checkRole("tasykil_pj")
        ) {
            $sum_monografis = [
                "PJ" => $counterMonografiPJ
                    ->where("kode_pj", $loggedUser->pj)
                    ->count(),
                "PC" => $counterMonografiPC
                    ->where("kd_pc", $loggedUser->pc)
                    ->count(),
                "PD" => $counterMonografiPD
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PW" => $counterMonografiPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];
            $sum_performas = [
                "PJ" => $counterPerformaPJ
                    ->where("kd_pj", $loggedUser->pj)
                    ->count(),
                "PC" => $counterPerformaPC
                    ->where("kd_pc", $loggedUser->pc)
                    ->count(),
                "PD" => $counterPerformaPD
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PW" => $counterPerformaPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];
            $sum_tafiq = [];
            $sum_tafiq["Belum Tafiq"] = DB::table("t_anggota")
                ->where("level_tafiq", 0)
                ->where("status_aktif", "ACTIVE")
                ->where("status_her", "SUDAH_BAYAR")
                ->where("pj", $loggedUser->pj)
                ->count();
            for ($i = 1; $i <= 3; $i++) {
                $sum_tafiq["Tafiq" . $i] = DB::table("t_anggota")
                    ->where("level_tafiq", $i)
                    ->where("status_aktif", "ACTIVE")
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("pj", $loggedUser->pj)
                    ->count();
            }
            $data = array_merge($data, [
                "alreadyRegs" => $anggota->alreadyRegisteredByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "usiaSummary" => $anggota->usiaSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "meritalSummary" => $anggota->meritalSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "statusKeanggotaanSummary" => $anggota->statusKeanggotaanSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "statusSummary" => $anggota->statusSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "golDarahSummary" => $anggota->golDarahSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "pendidikanSummary" => $anggota->pendidikanSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "anggota" => DB::table("t_anggota")
                    ->where("pc", $loggedUser->pc)
                    ->count(),
                "heregistrasi" => DB::table("t_anggota")
                    ->where("pc", $loggedUser->pc)
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "userTeregistrasi" => DB::table("t_login")
                    ->join("t_anggota", "t_login.npa", "=", "t_anggota.npa")
                    ->where("t_anggota.pc", $loggedUser->pc)
                    ->count(),
            ]);
        } elseif (
            Permission::checkRole("admin_pc") ||
            Permission::checkRole("tasykil_pc")
        ) {
            $sum_monografis = [
                "PJ" => $counterMonografiPJ
                    ->where("kd_pc", $loggedUser->pc)
                    ->count(),
                "PC" => $counterMonografiPC
                    ->where("kd_pc", $loggedUser->pc)
                    ->count(),
                "PD" => $counterMonografiPD
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PW" => $counterMonografiPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];

            $sum_performas = [
                "PJ" => $counterPerformaPJ
                    ->where("kd_pc", $loggedUser->pc)
                    ->count(),
                "PC" => $counterPerformaPC
                    ->where("kd_pc", $loggedUser->pc)
                    ->count(),
                "PD" => $counterPerformaPD
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PW" => $counterPerformaPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];
            $sum_tafiq = [];
            $sum_tafiq["Belum Tafiq"] = DB::table("t_anggota")
                ->where("level_tafiq", 0)
                ->where("status_aktif", "ACTIVE")
                ->where("status_her", "SUDAH_BAYAR")
                ->where("pc", $loggedUser->pc)
                ->count();
            for ($i = 1; $i <= 3; $i++) {
                $sum_tafiq["Tafiq" . $i] = DB::table("t_anggota")
                    ->where("level_tafiq", $i)
                    ->where("status_aktif", "ACTIVE")
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("pc", $loggedUser->pc)
                    ->count();
            }

            $data = array_merge($data, [
                "alreadyRegs" => $anggota->alreadyRegisteredByPC(
                    $loggedUser->pc
                ),
                "usiaSummary" => $anggota->usiaSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "meritalSummary" => $anggota->meritalSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "statusKeanggotaanSummary" => $anggota->statusKeanggotaanSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "statusSummary" => $anggota->statusSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "golDarahSummary" => $anggota->golDarahSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "pendidikanSummary" => $anggota->pendidikanSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "anggota" => DB::table("t_anggota")
                    ->where("pc", $loggedUser->pc)
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "heregistrasi" => DB::table("t_anggota")
                    ->where("pc", $loggedUser->pc)
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "userTeregistrasi" => DB::table("t_login")
                    ->join("t_anggota", "t_login.npa", "=", "t_anggota.npa")
                    ->where("t_anggota.pc", $loggedUser->pc)
                    ->count(),
            ]);
        } elseif (
            Permission::checkRole("admin_pd") ||
            Permission::checkRole("tasykil_pd")
        ) {
            $pd = PD::where("kd_pd", $loggedUser->pd)
                ->with("relatedPC")
                ->get();

            $listPC = $pd
                ->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })
                ->collapse();

            $sum_monografis = [
                "PJ" => $counterMonografiPJ
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PC" => $counterMonografiPC
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PD" => $counterMonografiPD
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PW" => $counterMonografiPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];

            $sum_performas = [
                "PJ" => $counterPerformaPJ
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PC" => $counterPerformaPC->whereIn("kd_pc", $listPC)->count(),
                "PD" => $counterPerformaPD
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PW" => $counterPerformaPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];
            $sum_tafiq = [];
            $sum_tafiq["Belum Tafiq"] = DB::table("t_anggota")
                ->where("level_tafiq", 0)
                ->where("status_aktif", "ACTIVE")
                ->where("status_her", "SUDAH_BAYAR")
                ->where("pd", $loggedUser->pc)
                ->count();
            for ($i = 1; $i <= 3; $i++) {
                $sum_tafiq["Tafiq" . $i] = DB::table("t_anggota")
                    ->where("level_tafiq", $i)
                    ->where("status_aktif", "ACTIVE")
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("pd", $loggedUser->pd)
                    ->count();
            }

            $data = array_merge($data, [
                "alreadyRegs" => $anggota->alreadyRegisteredByPD(
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "usiaSummary" => $anggota->usiaSummaryByPD(
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "meritalSummary" => $anggota->meritalSummaryByPD(
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "statusKeanggotaanSummary" => $anggota->statusKeanggotaanSummaryByPD(
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "statusSummary" => $anggota->statusSummaryByPD(
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "golDarahSummary" => $anggota->golDarahSummaryByPD(
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "pendidikanSummary" => $anggota->pendidikanSummaryByPD(
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "anggota" => DB::table("t_anggota")
                    ->where("pd", $loggedUser->pd)
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "heregistrasi" => DB::table("t_anggota")
                    ->where("pd", $loggedUser->pd)
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "userTeregistrasi" => DB::table("t_login")
                    ->join("t_anggota", "t_login.npa", "=", "t_anggota.npa")
                    ->where("t_anggota.pd", $loggedUser->pd)
                    ->count(),
            ]);
        } elseif (
            Permission::checkRole("admin_pw") ||
            Permission::checkRole("tasykil_pw")
        ) {
            $relatedPD = PW::where("kd_pw", $loggedUser->pw)
                ->first()
                ->relatedPD->pluck("kd_pd");

            $pd = PD::whereIn("kd_pd", $relatedPD)
                ->with("relatedPC")
                ->get();

            $listPC = $pd
                ->map(function ($value) {
                    return $value->relatedPC->map(function ($value2) {
                        return $value2->kd_pc;
                    });
                })
                ->collapse();

            $sum_monografis = [
                "PJ" => $counterMonografiPJ
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
                "PC" => $counterMonografiPC->whereIn("kd_pc", $listPC)->count(),
                "PD" => $counterMonografiPD
                    ->whereIn("kd_pd", $relatedPD)
                    ->count(),
                "PW" => $counterMonografiPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];

            $sum_performas = [
                "PJ" => $counterPerformaPJ
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
                "PC" => $counterPerformaPC->whereIn("kd_pc", $listPC)->count(),
                "PD" => $counterPerformaPD
                    ->whereIn("kd_pd", $relatedPD)
                    ->count(),
                "PW" => $counterPerformaPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];
            $sum_tafiq = [];
            $sum_tafiq["Belum Tafiq"] = DB::table("t_anggota")
                ->where("level_tafiq", 0)
                ->where("status_aktif", "ACTIVE")
                ->where("status_her", "SUDAH_BAYAR")
                ->where("pw", $loggedUser->pw)
                ->count();
            for ($i = 1; $i <= 3; $i++) {
                $sum_tafiq["Tafiq" . $i] = DB::table("t_anggota")
                    ->where("level_tafiq", $i)
                    ->where("status_aktif", "ACTIVE")
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("pw", $loggedUser->pw)
                    ->count();
            }

            $data = array_merge($data, [
                "alreadyRegs" => $anggota->alreadyRegisteredByPW(
                    $loggedUser->pw
                ),
                "usiaSummary" => $anggota->usiaSummaryByPW($loggedUser->pw),
                "meritalSummary" => $anggota->meritalSummaryByPW(
                    $loggedUser->pw
                ),
                "statusKeanggotaanSummary" => $anggota->statusKeanggotaanSummaryByPW(
                    $loggedUser->pw
                ),
                "statusSummary" => $anggota->statusSummaryByPW($loggedUser->pw),
                "golDarahSummary" => $anggota->golDarahSummaryByPW(
                    $loggedUser->pw
                ),
                "pendidikanSummary" => $anggota->pendidikanSummaryByPW(
                    $loggedUser->pw
                ),
                "anggota" => DB::table("t_anggota")
                    ->where("pw", $loggedUser->pw)
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "heregistrasi" => DB::table("t_anggota")
                    ->where("pw", $loggedUser->pw)
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "userTeregistrasi" => DB::table("t_login")
                    ->join("t_anggota", "t_login.npa", "=", "t_anggota.npa")
                    ->whereIn("t_anggota.pc", $listPC)
                    ->count(),
            ]);
        } elseif (Permission::checkRole("anggota")) {
            $sum_monografis = [
                "PJ" => $counterMonografiPJ
                    ->where("kode_pj", $loggedUser->pj)
                    ->count(),
                "PC" => $counterMonografiPC
                    ->where("kd_pc", $loggedUser->pc)
                    ->count(),
                "PD" => $counterMonografiPD
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PW" => $counterMonografiPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];

            $sum_performas = [
                "PJ" => $counterPerformaPJ
                    ->where("kd_pj", $loggedUser->pj)
                    ->count(),
                "PC" => $counterPerformaPC
                    ->where("kd_pc", $loggedUser->pc)
                    ->count(),
                "PD" => $counterPerformaPD
                    ->where("kd_pd", $loggedUser->pd)
                    ->count(),
                "PW" => $counterPerformaPW
                    ->where("kd_pw", $loggedUser->pw)
                    ->count(),
            ];

            $data = array_merge($data, [
                "alreadyRegs" => $anggota->alreadyRegisteredByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "usiaSummary" => $anggota->usiaSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "meritalSummary" => $anggota->meritalSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "statusKeanggotaanSummary" => $anggota->statusKeanggotaanSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "statusSummary" => $anggota->statusSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "golDarahSummary" => $anggota->golDarahSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "pendidikanSummary" => $anggota->pendidikanSummaryByPC(
                    $loggedUser->pc,
                    $loggedUser->pd,
                    $loggedUser->pw
                ),
                "anggota" => DB::table("t_anggota")
                    ->where("pj", $loggedUser->pj)
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "heregistrasi" => DB::table("t_anggota")
                    ->where("pj", $loggedUser->pj)
                    ->where("status_her", "SUDAH_BAYAR")
                    ->where("status_aktif", "ACTIVE")
                    ->count(),
                "userTeregistrasi" => DB::table("t_login")
                    ->join("t_anggota", "t_login.npa", "=", "t_anggota.npa")
                    ->where("t_anggota.pj", $loggedUser->pj)
                    ->count(),
            ]);
        }

        $data = array_merge($data, [
            "sum_performas" => $sum_performas,
            "sum_monografis" => $sum_monografis,
            "sum_tafiq" => $sum_tafiq,
        ]);

        return view("dashboard", $data);
    }

    public function goToSlash()
    {
        # code...
        if (Auth::check()) {
            return redirect("dashboard");
        }
        return redirect("login");
    }

    public function landing_page()
    {
        # code...
        if (Auth::check()) {
            return redirect("dashboard");
        }
        return view("landing_page.index");
    }
}
