<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Mail\RegistrationApproved;
use App\Mail\RegistrationProcessing;
use App\Mail\RegistrationRejected;
use App\Model\Registration;
use App\Model\RekapCaangFile;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Yajra\DataTables\Facades\DataTables;

class RegistrasiController extends Controller
{
    public $PATH_FILE = "registrasi/";

    public function index()
    {
        return view('ana.registrasi.index');
    }

    public function prosesApproval(Request $request)
    {
        if (!Permission::checkPermission('rekapcaang.approve'))
            return response()->json(['message' => 'Unauthorized to approve ajuan registrasi'], 403);

        $data = $request->json()->all();

        try {
            $registrasi = Registration::findOrFail($data['id']);
            $registrasi->status = "APPROVED";
            $statusData = $registrasi->save();
            if($statusData){
                Mail::to(trim($registrasi->email))->send(new RegistrationApproved());
            }

            if($statusData){
                return response()->json([
                    "message" => "success",
                    "code" => ResponseAlias::HTTP_ACCEPTED
                ], ResponseAlias::HTTP_ACCEPTED);
            }else{
                throw new Exception(
                    "Terjadi kesalahan pada sistem. Silahkan kontak Admin", 500);

            }
        } catch (Exception $e) {
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function dataTables()
    {
        return Datatables::eloquent(Registration::with([
            'anggota', 'anggota.pimpinanCabang', 'file',
            'anggota.pimpinanDaerah', 'anggota.pimpinanWilayah']))
            ->order(function ($query) {
                $query->orderBy('created_at', 'desc');
            })
            ->addColumn('s3_path', function () {
                return Storage::disk('s3')->url('registrasi/');
            })
            ->addColumn('link_foto', function (Registration $data) {
                return Storage::disk('s3')->url('registrasi/' . $data->path_folder . '/' . $data->path_image);
            })
            ->addColumn('link_excel', function (Registration $data) {
                return Storage::disk('s3')->url(
                    'registrasi/' . $data->path_folder . '/' . $data->path_excel);
            })
            ->make(true);
    }

    public function setStatus($id, $status)
    {
        if (!Permission::checkPermission('rekapcaang.setstatus'))
            return response()->json(['message' => 'Unauthorized to set registrasi status'], 403);

        try {

            $status = DB::transaction(function () use ($status, $id){
                $reg = Registration::findOrFail($id);
                $reg->status = $status;

                if ($reg->save()){
                    Mail::to(trim($reg->email))->send(new RegistrationRejected());
                    return true;
                }

                return false;

            });

            if($status){
                return response()->json([
                    "message" => "success",
                    "code" => ResponseAlias::HTTP_ACCEPTED
                ], ResponseAlias::HTTP_ACCEPTED);
            }else{
                throw new Exception(
                    "Terjadi kesalahan pada sistem. Silahkan kontak Admin", 500);
            }

        }catch (Exception $e) {
            return response()->json([
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            ], ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function indexTable()
    {
        return view('ana.registrasi.index_table');
    }

    public function processRegistrationForm(Request $req)
    {
        try {
            $validator = Validator::make($req->all(), [
                'npa' => 'required|exists:t_anggota,npa',
                'name' => 'required',
                'email' => 'required|email',
                'phone_number' => 'required',
                'jumlah' => 'required',
                'tanggal_maruf' => 'required',
            ], [
                'npa.exists' => 'NPA yang anda masukan tidak dikenali sistem.'
            ]);

            if($validator->fails()){
                throw new Exception(
                    collect($validator->errors()->all())->implode(", "), ResponseAlias::HTTP_BAD_REQUEST);
            }

            $status = DB::transaction(function () use ($req) {

                $her = new Registration();
                $her->npa = $req->get("npa");
                $her->name = $req->get("name");
                $her->email = trim($req->get("email"));
                $her->jumlah = $req->get("jumlah");
                $her->tanggal_maruf = $req->get("tanggal_maruf");
                $her->path_folder = null;
                $her->path_folder = null;
                $her->path_excel = null;
                $her->path_image = null;
                $her->phone_number = $req->get("phone_number");
                $status = $her->save();

                if($status){

                    $status = collect($req->file('file-excel'))->each(function($file) use($her){
                        $pathInfoFolder = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        $explode = explode('.', $file->getClientOriginalName());
                        $originalName = $explode[0];
                        $extension = $file->getClientOriginalExtension();
                        $rename = $originalName . '_' . date('dmY_His') . '.' . $extension;

                        $fileData = new RekapCaangFile();
                        $fileData->rekap_caang_id = $her->id;
                        $fileData->path_folder = $pathInfoFolder;
                        $fileData->path = $rename;
                        $fileData->filetype = "excel";

                        $status = $fileData->save();

                        if($status){
                            Storage::disk('s3')->put(
                                $this->PATH_FILE . $pathInfoFolder . "/" .
                                $rename, file_get_contents($file));
                        }

                        return $status;

                    });

                    $status = collect($req->file('foto-upload'))->each(function ($file) use ($her){
                        $pathInfoFolder = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        $explode2 = explode('.', $file->getClientOriginalName());
                        $originalName2 = $explode2[0];
                        $extension2 = $file->getClientOriginalExtension();
                        $rename2 = $originalName2 . '_' . date('dmY_His') . '.' . $extension2;

                        $fileData = new RekapCaangFile();
                        $fileData->rekap_caang_id = $her->id;
                        $fileData->path_folder = $pathInfoFolder;
                        $fileData->path = $rename2;
                        $fileData->filetype = "image";

                        $status = $fileData->save();

                        if($status){
                            Storage::disk('s3')->put(
                                $this->PATH_FILE . $pathInfoFolder . "/" .
                                $rename2, file_get_contents($file));
                        }

                        return $status;
                    });

                }

                return $status;

            });

            if ($status) {
                Mail::to(trim($req->get("email")))->queue(new RegistrationProcessing());
                return response()->json(['message' => 'Formulir rekap ajuan calon anggota berhasil dikirimkan', 'data' => 1]);
            } else {
                throw new Exception(
                    "Terjadi kesalahan pada sistem. Silahkan kontak Admin", 500);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], $e->getCode());
        }

    }
}
