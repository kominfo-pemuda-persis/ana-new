<?php

namespace App\Http\Controllers;

use Exception;
use App\Model\Bendahara;
use App\Facades\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use App\Model\PC;

class BendaharaController extends Controller
{
    public function index()
    {
        $data["role"] = Auth::user()
        ->roles->pluck("name")
        ->toArray();
        $data["admin_pc"] = "admin_pc";
        $data["admin_pd"] = "admin_pd";
        $data["admin_pw"] = "admin_pw";
        $data["admin_pp"] = "admin_pp";
        $data["superadmin"] = "superadmin";

        $tagihan = Bendahara::all();

        return view('ana.bendahara.tagihan.index', [
            'data' => $data,
            'tagihan' => $tagihan
        ]);
    }

    public function show($id)
    {
        $tagihan = Bendahara::find($id);
        return response()->json($tagihan);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "nama" => "required",
            "jumlah" => "required",
        ]);

        if($validator->fails()){
            throw new Exception(
                collect($validator->errors()->all())->implode(", "), ResponseAlias::HTTP_BAD_REQUEST);
        }

        $tagihan = Bendahara::find($id);
        $hapus = trim($request->jumlah, "Rp. ");
        $hasilHapus = str_replace(".", "", $hapus);
        $data['nama'] = $request->nama;
        $data['jumlah'] = $hasilHapus;
        $data['status'] = $request->status;
        $tagihan->update($data);

        return redirect()->back()->with('success', 'your message,here');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            "nama" => "required",
            "jumlah" => "required"
        ]);

        if($validator->fails()){
            throw new Exception(
                collect($validator->errors()->all())->implode(", "), ResponseAlias::HTTP_BAD_REQUEST);
        }

        $hapus = trim($request->jumlah, "Rp. ");
        $hasilHapus = str_replace(".", "", $hapus);

        $bendahara = new Bendahara();
        $bendahara->nama = $request->nama;
        $bendahara->jumlah = $hasilHapus;
        $bendahara->status = $request->status;

        if($bendahara->save()) {
            return response()->json([
                "message" => "success",
                "code" => ResponseAlias::HTTP_ACCEPTED
            ], ResponseAlias::HTTP_ACCEPTED);
        }

    }

    public function delete($id)
    {
        $tagihan = Bendahara::find($id);
        $tagihan->delete();

        return response()->json('Data berhasil didelete', 200);
    }

    // public function data()
    // {
    //     // $authAnggota = Auth::user()->anggota;

    //     // if (
    //     //     Permission::checkRole("admin_pc") ||
    //     //     Permission::checkRole("tasykil_pc")
    //     // ) {
    //     //     $tagihan = Bendahara::with(['anggota', 'anggota.pimpinanCabang']);
    //     //     $tagihan->whereHas("anggota", function($q) use($authAnggota){
    //     //         $q->where('pc', $authAnggota->pc);
    //     //     });
    //     // } elseif(
    //     //     Permission::checkRole("admin_pd") ||
    //     //     Permission::checkRole("tasykil_pd") ||
    //     //     Permission::checkRole("admin_pw") ||
    //     //     Permission::checkRole("tasykil_pw") ||
    //     //     Permission::checkRole("admin_pp") ||
    //     //     Permission::checkRole("tasykil_pp") ||
    //     //     Permission::checkRole("superadmin")
    //     // ){
    //     //     $tagihan = Bendahara::with(['anggota', 'anggota.pimpinanCabang']);
    //     //     $tagihan->whereHas("anggota", function($q) use($authAnggota){
    //     //         return $q;
    //     //     });
    //     // }

    //     // return datatables()
    //     //     ->of($tagihan)
    //     //     ->addIndexColumn()
    //     //     ->editColumn('jumlah', function($q) {
    //     //         return "Rp. ". number_format($q->jumlah, 0, ',', '.');
    //     //     })
    //     //     ->editColumn('status', function($q) {
    //     //         $data = '';
    //     //         if ($q->status == 'wajib_semua') {
    //     //             $data = '<td>'.($q->status == 'wajib_semua' ? 'Wajib Semua' : '').'</td>';
    //     //         }else if($q->status == 'perorang') {
    //     //             $data = '<td>'.($q->status == 'perorang' ? 'Per Orang' : '').'</td>';
    //     //         }
    //     //         return $data;
    //     //     })
    //     //     ->addColumn('pc', function($q) {
    //     //          return $q->kd_pc;
    //     //     })
    //     //     ->addColumn('aksi', function($q) {
    //     //         $data = '';

    //     //         if(Permission::checkRole("admin_pc") || Permission::checkRole("superadmin")){
    //     //             $data = '
    //     //                 <a href="javascript:void(0)" class="icon" title="edit item" onclick="editForm(`'. route('bendahara.update', $q->id) .'`)">
    //     //                     <i class="fa fa-edit"></i>
    //     //                 </a>
    //     //                 <a href="javascript:void(0)" onclick="deleteData(`'. route('bendahara.delete', $q->id) .'`)" class="icon btn-delete" title="delete item")>
    //     //                     <i class="fa fa-trash"></i>
    //     //                 </a>
    //     //             ';
    //     //         }else{
    //     //             $data = '';
    //     //         }
    //     //         return $data;
    //     //     })
    //     //     ->rawColumns(['aksi','pc','status'])
    //     //     ->make(true);
    // }
}
