<?php

namespace App\Http\Controllers;

use DataTables;
use Storage;
use DB;
use Illuminate\Http\Request;
use App\Model\Anggota;
use App\Model\AnggotaPendidikan;
use App\Model\MasterPendidikan;


class AnggotaPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_anggota)
    {
        $id = $id_anggota;
        $data['anggota'] = Anggota::find($id);
        $data['jenjang'] = MasterPendidikan::all();
        return view('ana.anggota.pendidikan.index', $data);
    }

    public function datatables($id_anggota)
    {
        $anggotaPendidikan = AnggotaPendidikan::with('masterPendidikan')
            ->where('id_anggota', $id_anggota)
            ->orderBy('id_pendidikan', 'asc')
            ->get();
        return Datatables::of($anggotaPendidikan)
            ->addIndexColumn()
            ->make(true);
    }

    public function get(Request $request)
    {
        $id = $request->id;
        $pendidikan = AnggotaPendidikan::where('id_pendidikan', $id)
            ->first();
        if (!$pendidikan) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json(['message' => 'Success getting data', 'data' => $pendidikan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $createPendidikan = AnggotaPendidikan::create($data);

        if ($createPendidikan) {
            return response()->json(['message' => 'Success adding data', 'data' => 1]);
        } else {
            return response()->json(['message' => 'Failed adding data'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $data = $request->all();

        $pendidikan = AnggotaPendidikan::where('id_pendidikan', $id)
            ->first();
        if (!$pendidikan) {
            return response()->json(['message' => 'Not found data'], 404);
        } else {
            $pendidikan->update($data);
            return response()->json(['message' => 'Success updating data', 'data' => 1]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $destroyData = AnggotaPendidikan::where('id_pendidikan', $id)->first();
        $destroyData->delete();
        if ($destroyData) {
            return response()->json(['message' => 'Success deleting data', 'data' => $destroyData]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }
    }

    public function dataTablesTrash($id_anggota)
    {
        $anggota_pendidikan = AnggotaPendidikan::select('*')
            ->onlyTrashed()
            ->where('id_anggota', $id_anggota)
            ->orderBy('id_anggota', 'asc')
            ->get();
        return Datatables::of($anggota_pendidikan)
            ->addIndexColumn()
            ->make(true);
    }

    public function trash($id_anggota)
    {
        $data['anggota'] = Anggota::find($id_anggota);
        return view('ana.anggota.pendidikan.trash', $data);
    }

    public function restore($id_anggota, $id_anggota_pendidikan)
    {
        $restoreAnggota = AnggotaPendidikan::withTrashed()->find($id_anggota_pendidikan)->restore();
        if ($restoreAnggota) {
            return response()->json(['message' => 'Success restore data', 'data' => $restoreAnggota]);
        } else {
            return response()->json(['message' => 'Failed restore data'], 500);
        }
    }
}
