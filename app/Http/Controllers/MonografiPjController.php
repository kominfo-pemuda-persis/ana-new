<?php

namespace App\Http\Controllers;

use App\Facades\Permission;
use App\Model\KejamiyyahanPj;
use App\Model\MonografiPj;
use App\Model\PC;
use App\Model\PD;
use App\Model\Province;
use App\Model\PW;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PDOException;
use Throwable;

class MonografiPjController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pw = Auth::user()->anggota->pw;
        $pd = Auth::user()->anggota->pd;
        $pc = Auth::user()->anggota->pc;
        $pj = Auth::user()->anggota->pj;
        $monografiPJ = null;

        if ((Permission::checkRole('superadmin')) || (Permission::checkRole('tasykil_pp')) || (Permission::checkRole('admin_pp'))) {
            $monografiPJ = MonografiPj::all();
        } elseif ((Permission::checkRole('admin_pw')) || (Permission::checkRole('tasykil_pw'))) {
            $monografiPJ = MonografiPj::where('kd_pw', $pw)->get();
        } elseif ((Permission::checkRole('admin_pd')) || (Permission::checkRole('tasykil_pd'))) {
            $monografiPJ = MonografiPj::where('kd_pd', $pd)->get();
        } elseif ((Permission::checkRole('admin_pc')) || (Permission::checkRole('tasykil_pc'))) {
            $monografiPJ = MonografiPj::where('kd_pc', $pc)->get();
        } elseif (Permission::checkRole('admin_pj')) {
            $monografiPJ = MonografiPj::where('kode_pj', $pj)->get();
        }
        return view('ana.monografi.pj.index', compact('monografiPJ'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        $dataPW = PW::all();
        $provinces = Province::all();
        return view('ana.monografi.pj.add', compact('dataPW', 'provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kode_pj' => 'required',
            'nama_pj' => 'required|max:50',
            'kd_pc' => 'required',
            'kd_pd' => 'required',
            'kd_pw' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'desa' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_telpon' => 'required|max:15',
            'alamat' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'jarak_kabupaten' => 'max:50',
            'ketua' => 'required',
            'sekretaris' => 'required',
            'bendahara' => 'required',
            'anggota_biasa' => 'required',
            'tidak_herReg' => 'required',
            'mutasi_persis' => 'required',
            'mutasi_tempat' => 'required',
            'mengundurkan_diri' => 'required',
            'meninggal_dunia' => 'required',
            'calon_anggota' => 'required',
            'anggota_luar_biasa' => 'nullable',
            'photo' => 'mimes:jpg,png,jpeg,bmp|file|max:5000'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => json_encode($validation->errors()),
                'status' => $status
            ]);
        } else {
            DB::beginTransaction();
            try {
                // DB::beginTransaction();

                $jamaah = new MonografiPj();
                $jamaah->kode_pj = $request->Input('kode_pj');
                $jamaah->nama_pj = $request->Input('nama_pj');
                $jamaah->kd_pc = $request->Input('kd_pc');
                $jamaah->kd_pd = $request->Input('kd_pd');
                $jamaah->kd_pw = $request->Input('kd_pw');
                $jamaah->provinsi = $request->Input('provinsi');
                $jamaah->kabupaten = $request->Input('kabupaten');
                $jamaah->kecamatan = $request->Input('kecamatan');
                $jamaah->desa = $request->Input('desa');
                $jamaah->latitude = $request->Input('latitude');
                $jamaah->longitude = $request->Input('longitude');
                $jamaah->email = $request->Input('email');
                $jamaah->no_telpon = $request->Input('no_telpon');
                $jamaah->alamat = $request->Input('alamat');
                $jamaah->luas = $request->Input('luas');
                $jamaah->bw_timur = $request->Input('bw_timur');
                $jamaah->bw_barat = $request->Input('bw_barat');
                $jamaah->bw_selatan = $request->Input('bw_selatan');
                $jamaah->bw_utara = $request->Input('bw_utara');
                $jamaah->jarak_provinsi = $request->Input('jarak_provinsi');
                $jamaah->jarak_kabupaten = $request->Input('jarak_kabupaten');
                $jamaah->start_periode = $request->Input('start_periode');
                $jamaah->end_periode = $request->Input('end_periode');

//                $jamaah->created_by = $request->Input('created_by');

                if($request->hasFile('photo')){
                    $file = $request->file('photo');
                    $filename = $request->Input('nama_pj') . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    // Storage::disk('s3')->put('images/monografi/pj/' . $filename, file_get_contents($file));
                    $jamaah->photo = $filename;
                }

                // $status = $jamaah->save();
                $jamaah->save();

                $jamiyyahPj = new KejamiyyahanPj();
                $jamiyyahPj->kd_monografi_pj = $jamaah->id;
                $jamiyyahPj->kode_pj = $request->Input('kode_pj');
                $jamiyyahPj->nama_pj = $request->Input('nama_pj');
                $jamiyyahPj->kd_pc = $request->Input('kd_pc');
                $jamiyyahPj->kd_pd = $request->Input('kd_pd');
                $jamiyyahPj->kd_pw = $request->Input('kd_pw');
                $jamiyyahPj->provinsi = $request->Input('provinsi');
                $jamiyyahPj->kabupaten = $request->Input('kabupaten');
                $jamiyyahPj->kecamatan = $request->Input('kecamatan');
                $jamiyyahPj->desa = $request->Input('desa');
                $jamiyyahPj->ketua = $request->Input('ketua');
                $jamiyyahPj->sekretaris = $request->Input('sekretaris');
                $jamiyyahPj->bendahara = $request->Input('bendahara');
                $jamiyyahPj->hari_ngantor = $request->Input('hari_ngantor');
                $jamiyyahPj->waktu_ngantor = $request->Input('waktu_ngantor');
                $jamiyyahPj->musjam_terakhir_masehi = $request->Input('musjam_terakhir_masehi');
                $jamiyyahPj->musjam_terakhir_hijriyyah = $request->Input('musjam_terakhir_hijriyyah');
                $jamiyyahPj->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyahPj->anggota_luar_biasa = $request->Input('anggota_luar_biasa');
                $jamiyyahPj->tidak_herReg = $request->Input('tidak_herReg');
                $jamiyyahPj->mutasi_persis = $request->Input('mutasi_persis');
                $jamiyyahPj->mutasi_tempat = $request->Input('mutasi_tempat');
                $jamiyyahPj->mengundurkan_diri = $request->Input('mengundurkan_diri');
                $jamiyyahPj->meninggal_dunia = $request->Input('meninggal_dunia');
                $jamiyyahPj->calon_anggota = $request->Input('calon_anggota');

                // $status = $jamiyyahPj->save();
                $jamiyyahPj->save();

                activity('Monografi PJ')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PJ Created');


                if($request->hasFile('photo')){
                    $file = $request->file('photo');
                    $filename = $request->Input('nama_pj') . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    Storage::disk('s3')->put('images/monografi/pj/' . $filename, file_get_contents($file));
                    // $jamaah->photo = $filename;
                }
                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
            }

            return response()->json([
                'status' => 1//$status
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $monografiPJ = MonografiPj::find($id);

        if (!$monografiPJ) abort(404);

        $kejamiyyahanPJ = $monografiPJ;
        if (!$kejamiyyahanPJ) abort(404);

        return view('ana.monografi.pj.show', compact(
            'monografiPJ', 'kejamiyyahanPJ'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function edit($id)
    {
        $monografiPJ = MonografiPj::find($id);

        if (!$monografiPJ) abort(404);

        $kejamiyyahanPJ = $monografiPJ;

        if (!$kejamiyyahanPJ) abort(404);

        $dataPW = PW::all();
        $provinces = Province::all();

        return view('ana.monografi.pj.edit', compact('dataPW', 'provinces', 'monografiPJ', 'kejamiyyahanPJ'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $status = false;
        $validation = Validator::make($request->all(), [
            'kode_pj' => 'required',
            'nama_pj' => 'required|max:50',
            'kd_pc' => 'required',
            'kd_pd' => 'required',
            'kd_pw' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'desa' => 'required',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'email' => 'required|max:50',
            'no_telpon' => 'required|max:15',
            'alamat' => 'required|max:100',
            'luas' => 'nullable',
            'bw_timur' => 'max:50',
            'bw_barat' => 'max:50',
            'bw_selatan' => 'max:50',
            'bw_utara' => 'max:50',
            'jarak_provinsi' => 'max:50',
            'jarak_kabupaten' => 'max:50',
            'ketua' => 'required',
            'sekretaris' => 'required',
            'bendahara' => 'required',
            'photo' => 'mimes:jpg,png,jpeg,bmp|file|max:5000'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'message' => $validation->errors(),
                'status' => $status
            ]);
        } else {
            DB::beginTransaction();
            try {


                $jamaah = MonografiPj::find($id);

                if (!$jamaah) abort(404);

                $jamaah->kode_pj = $request->Input('kode_pj');
                $jamaah->nama_pj = $request->Input('nama_pj');
                $jamaah->kd_pc = $request->Input('kd_pc');
                $jamaah->kd_pd = $request->Input('kd_pd');
                $jamaah->kd_pw = $request->Input('kd_pw');
                $jamaah->provinsi = $request->Input('provinsi');
                $jamaah->kabupaten = $request->Input('kabupaten');
                $jamaah->kecamatan = $request->Input('kecamatan');
                $jamaah->desa = $request->Input('desa');
                $jamaah->latitude = $request->Input('latitude');
                $jamaah->longitude = $request->Input('longitude');
                $jamaah->email = $request->Input('email');
                $jamaah->no_telpon = $request->Input('no_telpon');
                $jamaah->alamat = $request->Input('alamat');
                $jamaah->luas = $request->Input('luas');
                $jamaah->bw_utara = $request->Input('bw_utara');
                $jamaah->bw_selatan = $request->Input('bw_selatan');
                $jamaah->bw_timur = $request->Input('bw_timur');
                $jamaah->bw_barat = $request->Input('bw_barat');
                $jamaah->jarak_provinsi = $request->Input('jarak_provinsi');
                $jamaah->jarak_kabupaten = $request->Input('jarak_kabupaten');
                $jamaah->start_periode = $request->Input('start_periode');
                $jamaah->end_periode = $request->Input('end_periode');

                if($request->has('photo')){
                    $file = $request->file('photo');
                    $filename = $request->nama_pj . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    // Storage::disk('s3')->put('images/monografi/pj/' . $filename, file_get_contents($file));
                    $jamaah->photo = $filename;
                }

                $status = $jamaah->save();

                // $jamiyyahPj = KejamiyyahanPj::find($id);
                $jamiyyahPj =  $jamaah->kejamiyyahan;

                // if (!$jamiyyahPj) abort(404);

                $jamiyyahPj->kode_pj = $request->Input('kode_pj');
                $jamiyyahPj->nama_pj = $request->Input('nama_pj');
                $jamiyyahPj->kd_monografi_pj = $jamaah->id;
                $jamiyyahPj->kd_pc = $request->Input('kd_pc');
                $jamiyyahPj->kd_pd = $request->Input('kd_pd');
                $jamiyyahPj->kd_pw = $request->Input('kd_pw');
                $jamiyyahPj->provinsi = $request->Input('provinsi');
                $jamiyyahPj->kabupaten = $request->Input('kabupaten');
                $jamiyyahPj->kecamatan = $request->Input('kecamatan');
                $jamiyyahPj->desa = $request->Input('desa');
                $jamiyyahPj->ketua = $request->Input('ketua');
                $jamiyyahPj->sekretaris = $request->Input('sekretaris');
                $jamiyyahPj->bendahara = $request->Input('bendahara');
                $jamiyyahPj->hari_ngantor = $request->Input('hari_ngantor');
                $jamiyyahPj->waktu_ngantor = $request->Input('waktu_ngantor');
                $jamiyyahPj->musjam_terakhir_masehi = $request->Input('musjam_terakhir_masehi');
                $jamiyyahPj->musjam_terakhir_hijriyyah = $request->Input('musjam_terakhir_hijriyyah');
                $jamiyyahPj->anggota_biasa = $request->Input('anggota_biasa');
                $jamiyyahPj->anggota_luar_biasa = $request->Input('anggota_luar_biasa');
                $jamiyyahPj->tidak_herReg = $request->Input('tidak_herReg');
                $jamiyyahPj->mutasi_persis = $request->Input('mutasi_persis');
                $jamiyyahPj->mutasi_tempat = $request->Input('mutasi_tempat');
                $jamiyyahPj->mengundurkan_diri = $request->Input('mengundurkan_diri');
                $jamiyyahPj->meninggal_dunia = $request->Input('meninggal_dunia');
                $jamiyyahPj->calon_anggota = $request->Input('calon_anggota');
                $status = $jamiyyahPj->save();

                activity('Monografi PJ')
                    ->performedOn(Auth::user())
                    ->causedBy(Auth::user())
                    ->log('Monografi PJ Updated');

                if($request->has('photo')){
                    $file = $request->file('photo');
                    $filename = $request->nama_pj . '_' . date('dmY_His') . '.' . $file->getClientOriginalExtension();
                    Storage::disk('s3')->put('images/monografi/pj/' . $filename, file_get_contents($file));
                    // $jamaah->photo = $filename;
                }
                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
            }

            return response()->json([
                'status' => $status
            ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $data = MonografiPj::find($id)->delete();
        activity('Monografi PJ')
            ->performedOn(Auth::user())
            ->causedBy(Auth::user())
            ->log('Monografi PJ Deleted');
        return response()->json($data);
    }
}
