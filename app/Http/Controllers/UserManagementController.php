<?php

namespace App\Http\Controllers;

use App\Model\RoleMaster;
use App\Model\UserLogin;
use DataTables;
use Illuminate\Http\Request;

class UserManagementController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = RoleMaster::all();
        $data['role'] = $role;

        return view('ana.roleuser.index')->with($data);
    }

    public function get($id_anggota){

        $role =  UserLogin::with(['anggota','roles'])->where('npa',$id_anggota)->first();

        if (!$role) {
            return response()->json(['message' => 'Not found data'], 404);
        }
        return response()->json(['message' => 'Success getting data', 'data' => $role]);
    }

    public function datatables(){
        $data = UserLogin::with(['anggota.master_jamiyyah','roles', 'anggota.pimpinanCabang'])->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function update(Request $respose)
    {
        $data = $respose->all();

        $user = UserLogin::where('npa', $data['npa'])->first();

        // Update user active status
        $user->status_aktif = $data['status_aktif'];
        $save = $user->save();

        // Sync role to user
        $roleID = $data['id_role'];
        $user->syncRoles($roleID);
//        $user->attachRole([$data['id_role']]);
        if ($save) {
            return response()->json(['message' => 'Success updating data', 'data' => 1]);
        } else {
            return response()->json(['message' => 'Failed updating data'], 500);
        }
    }


    public function delete(Request $request)
    {
        $id = $request->id;
        $remove = UserLogin::where('npa', $id)->first();
        $remove->delete();

        if ($remove) {
            return response()->json(['message' => 'Success deleting data', 'data' => $remove]);
        } else {
            return response()->json(['message' => 'Failed deleting data'], 500);
        }
    }
}
