<?php

namespace App\Http\Middleware;

use Closure;
use App\Facades\Permission as PermissionFacade;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions)
    {
        $permissionStrings = explode('|', $permissions);
        if(PermissionFacade::checkPermission(...$permissionStrings))
            return $next($request);
        else
            return abort(403, 'Access unauthorized');
    }
}
