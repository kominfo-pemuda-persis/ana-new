<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AgeCheckingClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'age:checking-clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset pengecekan usia Anggota';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = DB::table('t_anggota')->update(['has_age_checked' => false]);
        $message = "Tidak ada data";
        if($action > 0) {
            $message = "Berhasil mereset pengecekan.\nScheduler akan kembali melakukan cek dari awal anggota berusia 40 tahun ke atas";
        }
        $date = Carbon::now();
        $this->info("--------------------------------------------------------");
        $this->info("{$message}");
        $this->info("Terakhir ditinjau  : {$date->format('d F Y H:i:s')}");
        $this->info("--------------------------------------------------------");
    }
}
