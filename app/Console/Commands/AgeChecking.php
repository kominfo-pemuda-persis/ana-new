<?php

namespace App\Console\Commands;

use App\Model\Anggota;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class AgeChecking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'age:checking {--mode=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cek Usia Anggota yang lebih dari 40 tahun';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit  = 10;
        $mode   = $this->option('mode');
        $ids    = collect([]);
        $emails = collect([]);

        $anggota = Anggota::where('has_age_checked', false)->paginate($limit);

        $filteredAnggota = $anggota->filter(function($anggota){
            return $anggota->isAgeReachTreshold(Carbon::now(), 40, function($data, $age, $passed){
                if($passed){
                    $tangal_lahir = Carbon::parse($data->tanggal_lahir)->format('d F Y');
                    $this->info("======================================================");
                    $this->info("Nama           : {$data->nama_lengkap}");
                    $this->info("NPA            : {$data->npa}");
                    $this->info("Tanggal Lahir  : {$tangal_lahir}");
                    $this->info("Usia           : {$age} Tahun");
                }
            });
        });

        $filteredAnggota = $filteredAnggota->each(function($anggota) use($ids, $emails){
            $ids->push($anggota->id_anggota);
            $emails->push($anggota->email);
        });

        if($mode === "email"){
            Anggota::whereIn('id_anggota', $ids->values()->all())->get();
            // send email or do something
        }

        $response = Anggota::whereIn('id_anggota',
            $anggota->map(function($data){
                return $data->id_anggota;
            }))->update([
                'has_age_checked' => true
        ]);

        if($response > 0){

            $checked = Anggota::where('has_age_checked', true)->count();
            $unchecked = Anggota::where('has_age_checked', false)->count();
            $date = Carbon::now();

            $this->info("");
            $this->info("--------------------------------------------------------");
            $this->info("Data sudah ditinjau    : {$checked}");
            $this->info("Data belum ditinjau    : {$unchecked}");
            $this->info("Terakhir ditinjau      : {$date->format('d F Y H:i:s')}");
            $this->info("--------------------------------------------------------");

        }
    }
}
