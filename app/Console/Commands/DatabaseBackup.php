<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class DatabaseBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anaonline:db-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup database anaonline with schedule';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $dbPath = "backups";
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        $host = env('DB_HOST');
        $database = env('DB_DATABASE');
        $date = Carbon::now()->format('Y-m-d_H-i-s');
        $filename = "backup-db-anaonline-{$date}";
        $binary = env('MYSQLDUMP_PATH', 'mysqldump');
        $path = storage_path("app/{$dbPath}");
        $path_sql = "{$path}/{$filename}.sql";
        $path_zip = "{$path}/{$filename}.sql.zip";
        $exist = Storage::disk('local')->exists($dbPath);

        if (!$exist) {
           Storage::disk('local')->makeDirectory($dbPath);
        }

        $export_command = "{$binary} --user={$username} --password={$password} --host={$host} {$database} > $path_sql;";
        $tar_command = "cd $path; zip -m $path_zip {$filename}.sql;";
        $command = "{$export_command} {$tar_command}";
 
        $returnVar = NULL;
        $output  = NULL;
 
        exec($command, $output, $returnVar);

        Storage::disk('s3')->putFileAs($dbPath, 
            new File($path_zip), "{$filename}.sql.zip");
    }
}
