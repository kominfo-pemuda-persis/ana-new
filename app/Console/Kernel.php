<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $date = Carbon::now();
        $schedule->command("age:checking")
            ->timezone('Asia/Jakarta')
            ->twiceDaily(1, 5)
            ->withoutOverlapping(3)
            ->appendOutputTo(storage_path(
                "/logs/age-checking-{$date->year}-{$date->format('m')}-{$date->day}.log"));

        $schedule->command("age:checking-clear")
            ->timezone('Asia/Jakarta')
            ->weekly()
            ->withoutOverlapping(3)
            ->appendOutputTo(storage_path(
                "/logs/age-checking-clear-{$date->year}-{$date->format('m')}-{$date->day}.log"));

        $schedule->command('anaonline:db-backup')
            ->timezone('Asia/Jakarta')
            ->twiceDaily(4, 23)
            ->withoutOverlapping(3);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
