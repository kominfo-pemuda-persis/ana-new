<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class JamiyyahMaster extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'JamiyyahMaster';

    protected $table = 't_master_jamiyyah';

    protected $primaryKey = 'id_jamiyyah';
    protected $fillable = [
        'id_wilayah', 'nama_pimpinan', 'id_otonom', 'pimpinan', 'nomor', 'alamat', 'latitude', 'longitude'
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
