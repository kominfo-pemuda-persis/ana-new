<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CaangOrganisasi extends Model
{
    protected $table = 't_caang_organisasi';
    protected $primaryKey = 'id_organisasi';
    public $timestamps = false;
    protected $fillable = ['id_caang', 'id_organisasi','nama_organisasi', 'jabatan', 'tingkat', 'tahun_mulai', 'tahun_selesai', 'lokasi'];
}
