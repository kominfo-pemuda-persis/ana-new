<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class RoleUser extends Model
{
    use LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];

    protected $table = 't_role_user';

    protected $primaryKey = 'user_id';
    protected $fillable = [
        'user_id', 'role_id'
    ];

    public $timestamps = false;

    public function role()
    {
        return $this->hasOne(RoleMaster::class, 'id_role', 'role_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
