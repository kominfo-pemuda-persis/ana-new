<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AnggotaPekerjaan extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'AnggotaPekerjaan';

    protected $table = 't_anggota_pekerjaan';

    protected $casts = [
        'deleted_at' => 'date',
    ];

    protected $primaryKey = 'id_pekerjaan';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'id_anggota', 'id_pekerjaan','pekerjaan', 'keterangan', 'alamat', 'tahun_mulai', 'tahun_selesai'
    ];

    public function anggota()
    {
        return $this->belongsTo('App\Model\Anggota', 'id_anggota');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
