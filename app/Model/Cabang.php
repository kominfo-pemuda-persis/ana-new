<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Cabang extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'Cabang';

    protected $table = 't_monografi_pc';

//    protected $guarded = ['id_pc'];
    protected $fillable = [
        'kd_pc',
        'kd_pd',
        'kd_pw',
        'provinsi',
        'kota',
        'kecamatan',
        'latitude',
        'longitude',
        'email',
        'no_kontak',
        'alamat_utama',
        'alamat_alternatif',
        'luas',
        'bw_utara',
        'bw_selatan',
        'bw_timur',
        'bw_barat',
        'jarak_dari_ibukota_negara',
        'jarak_dari_ibukota_provinsi',
        'jarak_dari_ibukota_kabupaten',
        'foto',
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
