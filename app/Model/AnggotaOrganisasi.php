<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AnggotaOrganisasi extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'AnggotaOrganisasi';

    protected $table = 't_anggota_organisasi';

    protected $casts = [
        'deleted_at' => 'date',
    ];

    protected $primaryKey = 'id_organisasi';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'id_anggota', 'id_organisasi', 'nama_organisasi', 'jabatan', 'tingkat', 'tahun_mulai', 'tahun_selesai','lokasi'
    ];

    public function anggota()
    {
        return $this->belongsTo('App\Model\Anggota', 'id_anggota');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
