<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CaangKeluarga extends Model
{
    protected $table = 't_caang_keluarga';
    protected $primaryKey = 'id_keluarga';
    public $timestamps = false;
    protected $fillable = ['id_caang', 'id_keluarga', 'nama_keluarga', 'hubungan', 'jumlah_anak', 'alamat', 'keterangan', 'status_anggota'];
}
