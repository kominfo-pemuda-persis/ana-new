<?php

namespace App\Model;

use App\Model\Anggota;
use Database\Factories\Tafiq2Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;

class Tafiq2 extends Model
{
    use SoftDeletes, LogsActivity, HasFactory;
    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'tafiq2';
    protected $table = 't_tafiq2';
    protected $fillable = ["npa", "lokasi", "tanggal_tafiq_end",
        "tanggal_tafiq_start", "kehadiran", "keaktifan", "makalah",
        "presentasi_makalah"
    ];

    public function anggota()
    {
        return $this->hasOne(Anggota::class, "npa", "npa");
    }

    public function getNilaiTotalAttribute()
    {
        $raw = intval($this->kehadiran + $this->keaktifan +
            $this->makalah + $this->presentasi_makalah);
        if($raw % 4 === 0){
            return sprintf('%d', $raw/4);
        };
        return sprintf('%0.2f', $raw/4);

    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return Tafiq2Factory::new();
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
