<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MasterPendidikan extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'MasterPendidikan';

    protected $table = 't_master_pendidikan';

    protected $primaryKey = 'id_tingkat_pendidikan';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'id_tingkat_pendidikan', 'pendidikan'
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
