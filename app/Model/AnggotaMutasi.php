<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AnggotaMutasi extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'AnggotaMutasi';

    protected $table = 't_anggota_mutasi';

    protected $primaryKey = 'id_mutasi';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'id_anggota', 'id_mutasi', 'asal', 'tujuan', 'tanggal_mutasi'
    ];

    public function anggota()
    {
        return $this->belongsTo('App\Model\Anggota', 'id_anggota')->withTrashed();
    }

    public function asal()
    {
        return $this->belongsTo('App\Model\PC', 'asal', 'kd_pc');
    }

    public function tujuan()
    {
        return $this->belongsTo('App\Model\PC', 'tujuan', 'kd_pc');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
