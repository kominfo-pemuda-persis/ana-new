<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AnggotaTafiq extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'AnggotaTafiq';

    protected $dates = ['deleted_at','tanggal_masuk','tanggal_selesai'];
    protected $casts = [
        'deleted_at' => 'date',
        'tanggal_masuk' => 'date',
        'tanggal_selesai' => 'date',
    ];

    protected $table = 't_anggota_tafiq';

    protected $primaryKey = 'id_tafiq';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id_tafiq','id_anggota','tanggal_masuk',
        'tanggal_selesai','instansi','lokasi',
        'id_esyahadah'
    ];

    public function anggota()
    {
        return $this->belongsTo('App\Model\Anggota', 'id_anggota');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
