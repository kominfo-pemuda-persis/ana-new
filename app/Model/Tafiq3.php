<?php

namespace App\Model;

use Database\Factories\Tafiq3Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Tafiq3 extends Model
{
    use SoftDeletes, LogsActivity, HasFactory;
    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'tafiq3';
    protected $table = 't_tafiq3';
    protected $fillable = ["npa", "lokasi", "tanggal_tafiq_end",
        "tanggal_tafiq_start", "kehadiran", "keaktifan", "makalah",
        "presentasi_makalah", "pengujian_makalah"
    ];

    public function anggota()
    {
        return $this->hasOne(Anggota::class, "npa", "npa");
    }

    public function getNilaiTotalAttribute()
    {
        $raw = intval($this->kehadiran + $this->keaktifan +
            $this->makalah + $this->presentasi_makalah + $this->pengujian_makalah);
        if($raw % 5 === 0){
            return sprintf('%d', $raw/5);
        };
        return sprintf('%0.2f', $raw/5);
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return Tafiq3Factory::new();
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}

