<?php

namespace App\Model;

use App\Traits\Blameable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PerformaPD extends Model
{
    use SoftDeletes, LogsActivity, Blameable;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'PerformaPD';
    protected $table = "t_performa_pd";
    protected $primaryKey = "kd";
    protected $dates = ['deleted_at'];

    public function pd()
    {
        return $this->belongsTo(PD::class, 'kd_pd', 'kd_pd');
    }

    public function pw()
    {
        return $this->belongsTo(PW::class, 'kd_pw', 'kd_pw');
    }

    public function kabupaten()
    {
        return $this->hasOne(Regency::class, "id", "kota");
    }

    public function provinsi()
    {
        return $this->hasOne(Province::class, "id", "provinsi");
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
