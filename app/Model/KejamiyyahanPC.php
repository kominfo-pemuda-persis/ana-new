<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class KejamiyyahanPC extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'KejamiyyahanPC';
    protected $dates = ['deleted_at'];
    protected $table = 't_kejamiyyahan_pc';


    //    protected $fillable = [
    //        'ketua',
    //        'wkl_ketua',
    //        'sekretaris',
    //        'wkl_sekretaris',
    //        'bendahara',
    //        'wkl_bendahara',
    //        'bid_jamiyyah',
    //        'wkl_bid_jamiyyah',
    //        'bid_kaderisasi',
    //        'wkl_bid_kaderisasi',
    //        'bid_administrasi',
    //        'wkl_bid_administrasi',
    //        'bid_pendidikan',
    //        'wkl_bid_pendidikan',
    //        'bid_dakwah',
    //        'wkl_bid_dakwah',
    //        'bid_humas_publikasi',
    //        'wkl_bid_humas_publikasi',
    //        'bid_hal',
    //        'wkl_bid_hal',
    //        'bid_or_seni',
    //        'wkl_bid_or_seni',
    //        'bid_sosial',
    //        'wkl_bid_sosial',
    //        'bid_ekonomi',
    //        'wkl_bid_ekonomi',
    //        'penasehat1',
    //        'penasehat2',
    //        'penasehat3',
    //        'penasehat4',
    //        'pembantu_umum1',
    //        'pembantu_umum2',
    //        'pembantu_umum3',
    //        'hari',
    //        'pukul',
    //        'musycab_terakhir_m',
    //        'musycab_terakhir_h',
    //        'anggota_biasa',
    //        'anggota_tersiar',
    //        'anggota_istimewa',
    //        'tdk_her',
    //        'mutasi_ke_persis',
    //        'mutasi_tempat',
    //        'mengundurkan_diri',
    //        'meninggal_dunia',
    //        'biasa',
    //        'istimewa',
    //        'tersiar',
    //    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
