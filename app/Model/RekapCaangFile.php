<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RekapCaangFile extends Model
{
    protected $table = 't_rekap_caang_file';

    public function registrasi()
    {
        return $this->hasOne(Registration::class, 'id', 'rekap_caang_id');
    }
}
