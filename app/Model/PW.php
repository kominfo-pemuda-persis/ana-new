<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PW extends Model
{
    use LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'PW';

    protected $table = 't_pw';
    protected $primaryKey = "kd_pw";
    public $incrementing = false;

    protected $fillable = ['kd_pw', 'nama_pw', 'diresmikan'];

    public function relatedPD()
    {
        return $this->hasMany(PD::class, 'kd_pw');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
