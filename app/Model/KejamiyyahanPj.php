<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class KejamiyyahanPj extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'Kejamiyyahan PJ';
    protected $table = 't_kejamiyyahan_pj';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
