<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HerRegistrationFile extends Model
{
    protected $table = 't_her_registration_file';

    public function heregistrasi()
    {
        return $this->hasOne(HerRegistration::class, 'id', 'her_registration_id');
    }
}
