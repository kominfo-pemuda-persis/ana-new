<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class UserLogin extends Authenticatable
{
//    use SoftDeletes, LogsActivity;
    use LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected $table = "t_login";

    protected $primaryKey = 'id_login';
    public $timestamps = true;
    public $incrementing = false;
    protected $hidden = [
        "password",
        "remember_token",
        "reset_password_token"
    ];

    protected $fillable = [
        'id_login', 'npa', 'password', 'activ_code', 'email_confirm', 'status_aktif'
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function anggota()
    {
        return $this->hasOne(Anggota::class, 'npa', 'npa');
    }

    public function roles()
    {
        return $this->belongsToMany(RoleMaster::class, 't_role_user', 'user_id', 'role_id');
    }

    public function attachRole(RoleMaster $role)
    {
        $roleUser = \App\Model\RoleUser::insert([
            'user_id' => $this->id_login,
            'role_id' => $role->id_role,
        ]);
    }

    public function syncRoles(int $roleID)
    {
        RoleUser::updateOrCreate(
            ['user_id' => $this->id_login],
            ['role_id' => $roleID]
        );
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
