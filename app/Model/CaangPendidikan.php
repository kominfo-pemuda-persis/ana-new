<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CaangPendidikan extends Model
{
    protected $table = 't_caang_pendidikan';
    protected $primaryKey = 'id_pendidikan';
    public $timestamps = false;
    protected $fillable = ['id_caang', 'id_pendidikan','id_master_pendidikan', 'instansi', 'jurusan', 'tahun_masuk', 'tahun_keluar', 'jenis_pendidikan'];
}
