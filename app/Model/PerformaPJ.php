<?php

namespace App\Model;

use App\Traits\Blameable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PerformaPJ extends Model
{
    use SoftDeletes, LogsActivity, Blameable;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'PerformaPJ';
    protected $table = "t_performa_pj";
    protected $primaryKey = "kd";
    protected $dates = ['deleted_at'];

    public function pc()
    {
        return $this->belongsTo(PC::class, 'kd_pc', 'kd_pc');
    }

    public function pd()
    {
        return $this->belongsTo(PD::class, 'kd_pd', 'kd_pd');
    }

    public function pw()
    {
        return $this->belongsTo(PW::class, 'kd_pw', 'kd_pw');
    }

    public function kecamatan()
    {
        return $this->hasOne(District::class, "id", "kecamatan");
    }

    public function desa()
    {
        return $this->hasOne(Village::class, "id", "desa");
    }

    public function kabupaten()
    {
        return $this->hasOne(Regency::class, "id", "kota");
    }

    public function provinsi()
    {
        return $this->hasOne(Province::class,"id", "provinsi");
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
