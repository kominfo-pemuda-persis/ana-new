<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FileDocument extends Model
{
    protected $table = 't_document';

    public function anggota()
    {
        return $this->hasOne(Anggota::class, 'id_anggota', 'id_anggota');
    }
}
