<?php

namespace App\Model;

use App\Traits\Blameable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PerformaPW extends Model
{
    use SoftDeletes, LogsActivity, Blameable;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'PerformaPW';

    protected $table = "t_performa_pw";
    protected $primaryKey = "kd";
    protected $dates = ['deleted_at'];

    public function pw()
    {
        return $this->belongsTo(PW::class, 'kd_pw', 'kd_pw');
    }

    public function provinsi()
    {
        return $this->hasOne(Province::class, "id", "provinsi");
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
