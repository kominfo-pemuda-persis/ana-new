<?php

namespace App\Model;

use App\Model\Anggota;
use Spatie\Activitylog\LogOptions;
use Database\Factories\Tafiq1Factory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tafiq1 extends Model
{
    use SoftDeletes, LogsActivity, HasFactory;
    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'tafiq1';
    protected $table = 't_tafiq1';
    protected $fillable = ["npa", "lokasi", "tanggal_tafiq_end",
        "tanggal_tafiq_start", "kehadiran", "keaktifan", "makalah"];

    public function anggota()
    {
        return $this->hasOne(Anggota::class, "npa", "npa");
    }

    public function getNilaiTotalAttribute()
    {
        $raw = intval($this->kehadiran + $this->keaktifan + $this->makalah);
        if($raw % 3 === 0){
            return sprintf('%d', $raw/3);
        };
        return sprintf('%0.2f', $raw/3);
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return Tafiq1Factory::new();
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
