<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Caang extends Model
{
    use SoftDeletes, LogsActivity;
    protected $table = 't_caang';
    protected $primaryKey = 'id_caang';
    public $timestamps = false;
//    protected $dates = ['created_at', 'last_updated', 'tanggal_lahir', 'reg_date'];
    protected $casts = [
        'created_at' => 'date',
        'last_updated' => 'date',
        'deleted_at' => 'date',
        'tanggal_lahir' => 'date',
        'reg_date' => 'date',
    ];
    protected $guarded = [];

    protected $fillable = [
        'id_caang', 'npa', 'nama_lengkap', 'tempat_lahir', 'tanggal_lahir',
        'status_merital', 'pekerjaan', 'pw', 'pd', 'pc', 'pj', 'nama_pj', 'provinsi', 'kota', 'kecamatan', 'desa',
        'gol_darah', 'email', 'no_telpon', 'no_telpon2', 'alamat',
        'foto', 'reg_date', 'pendidikan_terakhir',
    ];

    public function keluarga()
    {
        return $this->hasMany(CaangKeluarga::class, "id_caang", "id_caang");
    }

    public function pendidikan()
    {
        return $this->hasMany(CaangPendidikan::class, "id_caang", "id_caang");
    }

    public function organisasi()
    {
        return $this->hasMany(CaangOrganisasi::class, "id_caang", "id_caang");
    }

    public function pimpinanCabang()
    {
        return $this->belongsTo(PC::class, 'pc', 'kd_pc');
    }

    public function pimpinanDaerah()
    {
        return $this->belongsTo(PD::class, 'pd', 'kd_pd');
    }

    public function pimpinanWilayah()
    {
        return $this->belongsTo(PW::class, 'pw', 'kd_pw');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'provinsi', 'id');
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class, 'kota', 'id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'kecamatan', 'id');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'desa', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
