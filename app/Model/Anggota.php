<?php

namespace App\Model;

use App\Traits\Blameable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Anggota extends Model
{
    use SoftDeletes, LogsActivity, Blameable;

    public $incrementing = false;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'Anggota';

//    protected $dates = ['deleted_at', 'tanggal_lahir', 'reg_date', 'masa_aktif_kta'];
    protected $casts = [
        'masa_aktif_kta' => 'date',
        'created_at' => 'date',
        'deleted_at' => 'date',
        'last_updated' => 'date',
        'tanggal_lahir' => 'date',
        'reg_date' => 'date',
    ];


    protected $table = 't_anggota';

    protected $primaryKey = 'id_anggota';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_updated';

    protected $fillable = [
        'id_anggota', 'npa', 'nama_lengkap', 'tempat_lahir', 'tanggal_lahir',
        'status_merital', 'pekerjaan', 'pw', 'pd', 'pc', 'pj', 'nama_pj', 'provinsi', 'kota', 'kecamatan', 'desa',
        'gol_darah', 'email', 'no_telpon', 'no_telpon2', 'alamat',
        'jenis_keanggotaan', 'status_aktif', 'foto',
        'masa_aktif_kta', 'reg_date', 'pendidikan_terakhir', 'level_tafiq'
    ];

    protected $attributes = [
        'foto' => '-',
        'id_otonom' => 0,
    ];

    //  public function count_anggota() {
    //     $this->db->from('t_anggota');
    //     return $this->db->count_all_results();
    // }

    // public function count_jamaah() {
    //     $this->db->from('t_data_jamaah');
    //     return $this->db->count_all_results();
    // }

    // public function count_cabang() {
    //     $this->db->from('t_data_geografis_pc');
    //     return $this->db->count_all_results();
    // }

    // public function count_pd() {
    //     $this->db->from('t_data_geografis_pd');
    //     return $this->db->count_all_results();
    // }

    // public function count_users() {
    //     $this->db->from('t_user');
    //     return $this->db->count_all_results();
    // }

    public function usiaSummary()
    {
        return DB::select("SELECT CASE
            WHEN umur < 20 THEN '0 - 19'
            WHEN umur BETWEEN 20 and 25 THEN '20 - 25'
            WHEN umur BETWEEN 26 and 30 THEN '26 - 30'
            WHEN umur BETWEEN 31 and 35 THEN '31 - 35'
            WHEN umur BETWEEN 36 and 40 THEN '36 - 40'
            WHEN umur > 40 THEN '40 >'
            WHEN umur IS NULL THEN '(NULL)'
            END as range_umur,
            COUNT(*) AS jumlah

            FROM (SELECT npa, nama_lengkap, tanggal_lahir, TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE()) AS umur FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            GROUP BY range_umur
            ORDER BY range_umur
            ");
    }

    public function usiaSummaryByPC($kdPC, $kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN umur < 20 THEN '0 - 19'
            WHEN umur BETWEEN 20 and 25 THEN '20 - 25'
            WHEN umur BETWEEN 26 and 30 THEN '26 - 30'
            WHEN umur BETWEEN 31 and 35 THEN '31 - 35'
            WHEN umur BETWEEN 36 and 40 THEN '36 - 40'
            WHEN umur > 40 THEN '40 >'
            WHEN umur IS NULL THEN '(NULL)'
            END as range_umur,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, tanggal_lahir, TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE()) AS umur FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD' AND pc = '$kdPC'
            GROUP BY range_umur
            ORDER BY range_umur
            ");
    }

    public function usiaSummaryByPD($kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN umur < 20 THEN '0 - 19'
            WHEN umur BETWEEN 20 and 25 THEN '20 - 25'
            WHEN umur BETWEEN 26 and 30 THEN '26 - 30'
            WHEN umur BETWEEN 31 and 35 THEN '31 - 35'
            WHEN umur BETWEEN 36 and 40 THEN '36 - 40'
            WHEN umur > 40 THEN '40 >'
            WHEN umur IS NULL THEN '(NULL)'
            END as range_umur,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, tanggal_lahir, TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE()) AS umur FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD'
            GROUP BY range_umur
            ORDER BY range_umur
            ");
    }

    public function usiaSummaryByPW($kdPW)
    {
        return DB::select("SELECT CASE
            WHEN umur < 20 THEN '0 - 19'
            WHEN umur BETWEEN 20 and 25 THEN '20 - 25'
            WHEN umur BETWEEN 26 and 30 THEN '26 - 30'
            WHEN umur BETWEEN 31 and 35 THEN '31 - 35'
            WHEN umur BETWEEN 36 and 40 THEN '36 - 40'
            WHEN umur > 40 THEN '40 >'
            WHEN umur IS NULL THEN '(NULL)'
            END as range_umur,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, tanggal_lahir, TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE()) AS umur FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW'
            GROUP BY range_umur
            ORDER BY range_umur
            ");
    }

    public function meritalSummary()
    {
        return DB::select("
            SELECT CASE
            WHEN status = 'Single' THEN 'Single'
            WHEN status = 'Menikah' THEN 'Menikah'
            WHEN status = 'Duda' THEN 'Duda'
            WHEN status LIKE '%-%' THEN 'Belum Diisi'
            WHEN status IS NULL THEN '(NULL)'
            END as status,
            COUNT(*) AS jumlah

            FROM (SELECT npa, status_merital as status FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            GROUP BY status
            ");
    }

    public function meritalSummaryByPC($kdPC, $kdPD, $kdPW)
    {
        return DB::select("
            SELECT CASE
            WHEN status = 'Single' THEN 'Single'
            WHEN status = 'Menikah' THEN 'Menikah'
            WHEN status = 'Duda' THEN 'Duda'
            WHEN status LIKE '%-%' THEN 'Belum Diisi'
            WHEN status IS NULL THEN '(NULL)'
            END as status,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, status_merital as status FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD' AND pc = '$kdPC'
            GROUP BY status
            ");
    }

    public function meritalSummaryByPD($kdPD, $kdPW)
    {
        return DB::select("
            SELECT CASE
            WHEN status = 'Single' THEN 'Single'
            WHEN status = 'Menikah' THEN 'Menikah'
            WHEN status = 'Duda' THEN 'Duda'
            WHEN status LIKE '%-%' THEN 'Belum Diisi'
            WHEN status IS NULL THEN '(NULL)'
            END as status,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, status_merital as status FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD'
            GROUP BY status
            ");
    }

    public function meritalSummaryByPW($kdPW)
    {
        return DB::select("
            SELECT CASE
            WHEN status = 'Single' THEN 'Single'
            WHEN status = 'Menikah' THEN 'Menikah'
            WHEN status = 'Duda' THEN 'Duda'
            WHEN status LIKE '%-%' THEN 'Belum Diisi'
            WHEN status IS NULL THEN '(NULL)'
            END as status,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, status_merital as status FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW'
            GROUP BY status
            ");
    }

    public function statusSummary()
    {
        return DB::select("SELECT CASE
            WHEN status_aktif = 'ACTIVE' THEN 'ACTIVE'
            WHEN status_aktif = 'NON ACTIVE' THEN 'NON ACTIVE'
            WHEN status_aktif = 'MUTASI' THEN 'MUTASI'
            WHEN status_aktif = 'TIDAK HER-REGISTRASI' THEN 'TIDAK HER-REGISTRASI'
            WHEN status_aktif = 'MENINGGAL' THEN 'MENINGGAL'
            WHEN status_aktif = 'MENGUNDURKAN DIRI' THEN 'MENGUNDURKAN DIRI'
            WHEN status_aktif LIKE '%-%' THEN 'Belum Diisi'
            WHEN status_aktif IS NULL THEN '(NULL)'
            END as status_aktif,
            COUNT(*) AS jumlah
            FROM (SELECT npa, status_aktif as status_aktif FROM t_anggota)  as dummy_table
            GROUP BY status_aktif
            ");
    }

    public function statusSummaryByPC($kdPC, $kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN status_aktif = 'ACTIVE' THEN 'ACTIVE'
            WHEN status_aktif = 'NON ACTIVE' THEN 'NON ACTIVE'
            WHEN status_aktif = 'MUTASI' THEN 'MUTASI'
            WHEN status_aktif = 'TIDAK HER-REGISTRASI' THEN 'TIDAK HER-REGISTRASI'
            WHEN status_aktif = 'MENINGGAL' THEN 'MENINGGAL'
            WHEN status_aktif = 'MENGUNDURKAN DIRI' THEN 'MENGUNDURKAN DIRI'
            WHEN status_aktif LIKE '%-%' THEN 'Belum Diisi'
            WHEN status_aktif IS NULL THEN '(NULL)'
            END as status_aktif,
            COUNT(*) AS jumlah
            FROM (SELECT pw, pd, pc, status_aktif as status_aktif FROM t_anggota)  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD' AND pc = '$kdPC'
            GROUP BY status_aktif
            ");
    }

    public function statusSummaryByPD($kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN status_aktif = 'ACTIVE' THEN 'ACTIVE'
            WHEN status_aktif = 'NON ACTIVE' THEN 'NON ACTIVE'
            WHEN status_aktif = 'MUTASI' THEN 'MUTASI'
            WHEN status_aktif = 'TIDAK HER-REGISTRASI' THEN 'TIDAK HER-REGISTRASI'
            WHEN status_aktif = 'MENINGGAL' THEN 'MENINGGAL'
            WHEN status_aktif = 'MENGUNDURKAN DIRI' THEN 'MENGUNDURKAN DIRI'
            WHEN status_aktif LIKE '%-%' THEN 'Belum Diisi'
            WHEN status_aktif IS NULL THEN '(NULL)'
            END as status_aktif,
            COUNT(*) AS jumlah
            FROM (SELECT pw, pd, pc, status_aktif as status_aktif FROM t_anggota)  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD'
            GROUP BY status_aktif
            ");
    }

    public function statusSummaryByPW($kdPW)
    {
        return DB::select("SELECT CASE
            WHEN status_aktif = 'ACTIVE' THEN 'ACTIVE'
            WHEN status_aktif = 'NON ACTIVE' THEN 'NON ACTIVE'
            WHEN status_aktif = 'MUTASI' THEN 'MUTASI'
            WHEN status_aktif = 'TIDAK HER-REGISTRASI' THEN 'TIDAK HER-REGISTRASI'
            WHEN status_aktif = 'MENINGGAL' THEN 'MENINGGAL'
            WHEN status_aktif = 'MENGUNDURKAN DIRI' THEN 'MENGUNDURKAN DIRI'
            WHEN status_aktif LIKE '%-%' THEN 'Belum Diisi'
            WHEN status_aktif IS NULL THEN '(NULL)'
            END as status_aktif,
            COUNT(*) AS jumlah
            FROM (SELECT pw, pd, pc, status_aktif as status_aktif FROM t_anggota)  as dummy_table
            WHERE pw = '$kdPW'
            GROUP BY status_aktif
            ");
    }

    public function statusKeanggotaanSummary()
    {
        return DB::select("SELECT CASE
            WHEN status = 'Biasa' THEN 'Biasa'
            WHEN status = 'Tersiar' THEN 'Tersiar'
            WHEN status = 'Istimewa' THEN 'Istimewa'
            WHEN status LIKE '%-%' THEN 'Belum Diisi'
            WHEN status IS NULL THEN '(NULL)'
            END as status,
            COUNT(*) AS jumlah

            FROM (SELECT npa, jenis_keanggotaan as status FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            GROUP BY status
            ");
    }

    public function statusKeanggotaanSummaryByPC($kdPC, $kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN status = 'Biasa' THEN 'Biasa'
            WHEN status = 'Tersiar' THEN 'Tersiar'
            WHEN status = 'Istimewa' THEN 'Istimewa'
            WHEN status LIKE '%-%' THEN 'Belum Diisi'
            WHEN status IS NULL THEN '(NULL)'
            END as status,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, jenis_keanggotaan as status FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD' AND pc = '$kdPC'
            GROUP BY status
            ");
    }

    public function statusKeanggotaanSummaryByPD($kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN status = 'Biasa' THEN 'Biasa'
            WHEN status = 'Tersiar' THEN 'Tersiar'
            WHEN status = 'Istimewa' THEN 'Istimewa'
            WHEN status LIKE '%-%' THEN 'Belum Diisi'
            WHEN status IS NULL THEN '(NULL)'
            END as status,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, jenis_keanggotaan as status FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD'
            GROUP BY status
            ");
    }

    public function statusKeanggotaanSummaryByPW($kdPW)
    {
        return DB::select("SELECT CASE
            WHEN status = 'Biasa' THEN 'Biasa'
            WHEN status = 'Tersiar' THEN 'Tersiar'
            WHEN status = 'Istimewa' THEN 'Istimewa'
            WHEN status LIKE '%-%' THEN 'Belum Diisi'
            WHEN status IS NULL THEN '(NULL)'
            END as status,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, jenis_keanggotaan as status FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW'
            GROUP BY status
            ");
    }

    public function golDarahSummary()
    {
        return DB::select("SELECT CASE
            WHEN gol = 'A' THEN 'A'
            WHEN gol = 'B' THEN 'B'
            WHEN gol = 'AB' THEN 'AB'
            WHEN gol = 'O' THEN 'O'
            WHEN gol LIKE '%-%' THEN 'Belum Diisi'
            WHEN gol IS NULL THEN '(NULL)'
            END as gol,
            COUNT(*) AS jumlah
            FROM (SELECT npa, gol_darah as gol FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            GROUP BY gol
            ");
    }

    public function golDarahSummaryByPC($kdPC, $kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN gol = 'A' THEN 'A'
            WHEN gol = 'B' THEN 'B'
            WHEN gol = 'AB' THEN 'AB'
            WHEN gol = 'O' THEN 'O'
            WHEN gol LIKE '%-%' THEN 'Belum Diisi'
            WHEN gol IS NULL THEN '(NULL)'
            END as gol,
            COUNT(*) AS jumlah
            FROM (SELECT pw, pd, pc, gol_darah as gol FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD' AND pc = '$kdPC'
            GROUP BY gol
            ");
    }

    public function golDarahSummaryByPD($kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN gol = 'A' THEN 'A'
            WHEN gol = 'B' THEN 'B'
            WHEN gol = 'AB' THEN 'AB'
            WHEN gol = 'O' THEN 'O'
            WHEN gol LIKE '%-%' THEN 'Belum Diisi'
            WHEN gol IS NULL THEN '(NULL)'
            END as gol,
            COUNT(*) AS jumlah
            FROM (SELECT pw, pd, pc, gol_darah as gol FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD'
            GROUP BY gol
            ");
    }

    public function golDarahSummaryByPW($kdPW)
    {
        return DB::select("SELECT CASE
            WHEN gol = 'A' THEN 'A'
            WHEN gol = 'B' THEN 'B'
            WHEN gol = 'AB' THEN 'AB'
            WHEN gol = 'O' THEN 'O'
            WHEN gol LIKE '%-%' THEN 'Belum Diisi'
            WHEN gol IS NULL THEN '(NULL)'
            END as gol,
            COUNT(*) AS jumlah
            FROM (SELECT pw, pd, pc, gol_darah as gol FROM t_anggota WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW'
            GROUP BY gol
            ");
    }

    public function pendidikanSummary()
    {
        return DB::select("SELECT CASE
            WHEN level = 'SD' THEN 'SD'
            WHEN level = 'MI' THEN 'MI'
            WHEN level = 'SMP' THEN 'SMP'
            WHEN level = 'MTs' THEN 'MTs'
            WHEN level = 'SMK' THEN 'SMK'
            WHEN level = 'SMA' THEN 'SMA'
            WHEN level = 'MA' THEN 'MA'
            WHEN level = 'STM' THEN 'STM'
            WHEN level = 'MLN' THEN 'MLN'
            WHEN level = 'D1' THEN 'D1'
            WHEN level = 'D2' THEN 'D2'
            WHEN level = 'D3' THEN 'D3'
            WHEN level = 'D4' THEN 'D4'
            WHEN level = 'S1' THEN 'S1'
            WHEN level = 'S2' THEN 'S2'
            WHEN level = 'S3' THEN 'S3'
            WHEN level LIKE 'KOSONG' THEN 'BELUM DIISI'
            WHEN level IS NULL THEN '(NULL)'
            END level_pendidikan,
            COUNT(*) AS jumlah

            FROM (SELECT npa, pendidikan as level FROM t_anggota a RIGHT JOIN t_master_pendidikan tmp ON tmp.id_tingkat_pendidikan = a.pendidikan_terakhir WHERE status_aktif = 'ACTIVE')  as dummy_table
            GROUP BY level
            ");
    }

    public function pendidikanSummaryByPC($kdPC, $kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN level = 'SD' THEN 'SD'
            WHEN level = 'MI' THEN 'MI'
            WHEN level = 'SMP' THEN 'SMP'
            WHEN level = 'MTs' THEN 'MTs'
            WHEN level = 'SMK' THEN 'SMK'
            WHEN level = 'SMA' THEN 'SMA'
            WHEN level = 'MA' THEN 'MA'
            WHEN level = 'STM' THEN 'STM'
            WHEN level = 'MLN' THEN 'MLN'
            WHEN level = 'D1' THEN 'D1'
            WHEN level = 'D2' THEN 'D2'
            WHEN level = 'D3' THEN 'D3'
            WHEN level = 'D4' THEN 'D4'
            WHEN level = 'S1' THEN 'S1'
            WHEN level = 'S2' THEN 'S2'
            WHEN level = 'S3' THEN 'S3'
            WHEN level LIKE 'KOSONG' THEN 'BELUM DIISI'
            WHEN level IS NULL THEN '(NULL)'
            END level_pendidikan,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, pendidikan as level FROM t_anggota a RIGHT JOIN t_master_pendidikan tmp ON tmp.id_tingkat_pendidikan = a.pendidikan_terakhir WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD' AND pc = '$kdPC'
            GROUP BY level
            ");
    }

    public function pendidikanSummaryByPD($kdPD, $kdPW)
    {
        return DB::select("SELECT CASE
            WHEN level = 'SD' THEN 'SD'
            WHEN level = 'MI' THEN 'MI'
            WHEN level = 'SMP' THEN 'SMP'
            WHEN level = 'MTs' THEN 'MTs'
            WHEN level = 'SMK' THEN 'SMK'
            WHEN level = 'SMA' THEN 'SMA'
            WHEN level = 'MA' THEN 'MA'
            WHEN level = 'STM' THEN 'STM'
            WHEN level = 'MLN' THEN 'MLN'
            WHEN level = 'D1' THEN 'D1'
            WHEN level = 'D2' THEN 'D2'
            WHEN level = 'D3' THEN 'D3'
            WHEN level = 'D4' THEN 'D4'
            WHEN level = 'S1' THEN 'S1'
            WHEN level = 'S2' THEN 'S2'
            WHEN level = 'S3' THEN 'S3'
            WHEN level LIKE 'KOSONG' THEN 'BELUM DIISI'
            WHEN level IS NULL THEN '(NULL)'
            END level_pendidikan,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, pendidikan as level FROM t_anggota a RIGHT JOIN t_master_pendidikan tmp ON tmp.id_tingkat_pendidikan = a.pendidikan_terakhir WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW' AND pd = '$kdPD'
            GROUP BY level
            ");
    }

    public function pendidikanSummaryByPW($kdPW)
    {
        return DB::select("SELECT CASE
            WHEN level = 'SD' THEN 'SD'
            WHEN level = 'MI' THEN 'MI'
            WHEN level = 'SMP' THEN 'SMP'
            WHEN level = 'MTs' THEN 'MTs'
            WHEN level = 'SMK' THEN 'SMK'
            WHEN level = 'SMA' THEN 'SMA'
            WHEN level = 'MA' THEN 'MA'
            WHEN level = 'STM' THEN 'STM'
            WHEN level = 'MLN' THEN 'MLN'
            WHEN level = 'D1' THEN 'D1'
            WHEN level = 'D2' THEN 'D2'
            WHEN level = 'D3' THEN 'D3'
            WHEN level = 'D4' THEN 'D4'
            WHEN level = 'S1' THEN 'S1'
            WHEN level = 'S2' THEN 'S2'
            WHEN level = 'S3' THEN 'S3'
            WHEN level LIKE 'KOSONG' THEN 'BELUM DIISI'
            WHEN level IS NULL THEN '(NULL)'
            END level_pendidikan,
            COUNT(*) AS jumlah

            FROM (SELECT pw, pd, pc, pendidikan as level FROM t_anggota a RIGHT JOIN t_master_pendidikan tmp ON tmp.id_tingkat_pendidikan = a.pendidikan_terakhir WHERE status_aktif = 'ACTIVE')  as dummy_table
            WHERE pw = '$kdPW'
            GROUP BY level
            ");
    }

    public function alreadyRegistered()
    {
        return DB::select("SELECT t_pw.nama_pw as pw,
            t_pd.nama_pd as pd, t_pc.nama_pc as pc, COUNT(t_anggota.npa) as jumlah,
           COUNT(CASE WHEN status_her = 'SUDAH_BAYAR' THEN 1 END) AS SUDAH_HER,
           COUNT(CASE WHEN status_her = 'BELUM_BAYAR' THEN 0 END) AS BELUM_HER
            FROM (((t_anggota
            INNER JOIN t_pw
            ON t_anggota.pw = t_pw.kd_pw)
            INNER JOIN t_pd
            ON t_anggota.pd = t_pd.kd_pd)
            INNER JOIN t_pc
            ON t_anggota.pc = t_pc.kd_pc)
            WHERE status_aktif = 'ACTIVE'
            GROUP BY pw, pd, pc
            ORDER BY pw, pd, pc ASC"
        );
    }

    public function alreadyRegisteredByPC($kdPc)
    {
        return DB::select("SELECT t_pw.nama_pw as pw,
            t_pd.nama_pd as pd, t_pc.nama_pc as pc, COUNT(t_anggota.npa) as jumlah,
            COUNT(CASE WHEN status_her = 'SUDAH_BAYAR' THEN 1 END) AS SUDAH_HER,
            COUNT(CASE WHEN status_her = 'BELUM_BAYAR' THEN 0 END) AS BELUM_HER
            FROM (((t_anggota
            INNER JOIN t_pw
            ON t_anggota.pw = t_pw.kd_pw)
            INNER JOIN t_pd
            ON t_anggota.pd = t_pd.kd_pd)
            INNER JOIN t_pc
            ON t_anggota.pc = t_pc.kd_pc)
            WHERE pc = '$kdPc' and status_aktif = 'ACTIVE'
            GROUP BY pw, pd, pc
            ORDER BY pw, pd, pc ASC"
        );
    }

    public function alreadyRegisteredByPD($kdPd, $kdPw)
    {
        return DB::select("SELECT t_pw.nama_pw as pw,
            t_pd.nama_pd as pd, t_pc.nama_pc as pc, COUNT(t_anggota.npa) as jumlah,
            COUNT(CASE WHEN status_her = 'SUDAH_BAYAR' THEN 1 END) AS SUDAH_HER,
            COUNT(CASE WHEN status_her = 'BELUM_BAYAR' THEN 0 END) AS BELUM_HER
            FROM (((t_anggota
            INNER JOIN t_pw
            ON t_anggota.pw = t_pw.kd_pw)
            INNER JOIN t_pd
            ON t_anggota.pd = t_pd.kd_pd)
            INNER JOIN t_pc
            ON t_anggota.pc = t_pc.kd_pc)
            WHERE pd = '$kdPd' and pw = '$kdPw' and status_aktif = 'ACTIVE'
            GROUP BY pw, pd, pc
            ORDER BY pw, pd, pc ASC"
        );
    }

    public function alreadyRegisteredByPW($kdPw)
    {
        return DB::select("SELECT t_pw.nama_pw as pw,
            t_pd.nama_pd as pd, t_pc.nama_pc as pc, COUNT(t_anggota.npa) as jumlah,
            COUNT(CASE WHEN status_her = 'SUDAH_BAYAR' THEN 1 END) AS SUDAH_HER,
            COUNT(CASE WHEN status_her = 'BELUM_BAYAR' THEN 0 END) AS BELUM_HER
            FROM (((t_anggota
            INNER JOIN t_pw
            ON t_anggota.pw = t_pw.kd_pw)
            INNER JOIN t_pd
            ON t_anggota.pd = t_pd.kd_pd)
            INNER JOIN t_pc
            ON t_anggota.pc = t_pc.kd_pc)
            WHERE pw = '$kdPw'and status_aktif = 'ACTIVE'
            GROUP BY pw, pd, pc
            ORDER BY pw, pd, pc ASC"
        );
    }

    public function master_jamiyyah()
    {
        return $this->hasOne(JamiyyahMaster::class, 'id_wilayah', 'pc');
    }

    public function keluarga()
    {
        return $this->hasMany('App\Model\AnggotaKeluarga', 'id_anggota');
    }

    public function keterampilan()
    {
        return $this->hasMany('App\Model\AnggotaKeterampilan', 'id_anggota');
    }

    public function mutasi()
    {
        return $this->hasMany('App\Model\AnggotaMutasi', 'id_anggota');
    }

    public function organisasi()
    {
        return $this->hasMany('App\Model\AnggotaOrganisasi', 'id_anggota');
    }

    public function pekerjaan()
    {
        return $this->hasMany('App\Model\AnggotaPekerjaan', 'id_anggota');
    }

    public function pendidikan()
    {
        return $this->hasMany('App\Model\AnggotaPendidikan', 'id_anggota');
    }

    public function training()
    {
        return $this->hasMany('App\Model\AnggotaTraining', 'id_anggota');
    }

    public function tafiq()
    {
        return $this->hasMany('App\Model\AnggotaTafiq', 'id_anggota');
    }

    public function dataTafiq1()
    {
        return $this->hasOne(Tafiq1::class, 'npa', 'npa');
    }

    public function transaksi_tagihan()
    {
        return $this->hasOne(Transaksi::class, 'npa');
    }

    public function pimpinanJamaah()
    {
        return $this->belongsTo(PJ::class, 'pc', 'kd_pc');
    }

    public function pimpinanCabang()
    {
        return $this->belongsTo(PC::class, 'pc', 'kd_pc');
    }

    public function pimpinanDaerah()
    {
        return $this->belongsTo(PD::class, 'pd', 'kd_pd');
    }

    public function pimpinanWilayah()
    {
        return $this->belongsTo(PW::class, 'pw', 'kd_pw');
    }

    //pw to province
    public function pwToProvince()
    {
        return $this->belongsTo(Province::class, 'pw', 'id');
    }

    //pd to kabupaten
    public function pdToRegency()
    {
        return $this->belongsTo(Regency::class, 'pd', 'id');
    }

    //pc to kecamatan
    public function pcToDistrict()
    {
        return $this->belongsTo(District::class, 'pc', 'id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'provinsi', 'id');
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class, 'kota', 'id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'kecamatan', 'id');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'desa', 'id');
    }

    public function isAgeReachTreshold(Carbon $date, $treshold = 40, $callback = null)
    {
        $age = $this->tanggal_lahir->diffInYears($date);
        $status = $age > $treshold;
        if(!is_null($callback)) $callback($this, $age, $status);
        return $status;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
