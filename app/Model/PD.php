<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PD extends Model
{
    use LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'PD';
    protected $primaryKey = "kd_pd";

    protected $table = 't_pd';
    public $incrementing = false;

    protected $fillable = ['kd_pd', 'nama_pd', 'diresmikan', 'kd_pw'];

    public function relatedPC()
    {
        return $this->hasMany(PC::class, 'kd_pd');
    }

    public function pw()
    {
        return $this->hasOne(PW::class, 'kd_pw', 'kd_pw');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
