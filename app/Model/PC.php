<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PC extends Model
{
    use LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'PC';

    protected $table = 't_pc';
    public $incrementing = false;
    protected $primaryKey = "kd_pc";

    protected $fillable = ['kd_pc', 'nama_pc', 'diresmikan', 'kd_pd'];

    public function monografiPC()
    {
        return $this->hasOne(MonografiPc::class);
    }

    public function monografiPJ()
    {
        return $this->hasOne(MonografiPj::class);
    }

    public function pd(){
        return $this->hasOne(PD::class, 'kd_pd', 'kd_pd');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
