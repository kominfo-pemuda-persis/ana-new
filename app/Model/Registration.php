<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Registration extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'Registration';
    protected $dates = ['deleted_at'];
    protected $table = 't_rekap_caang';

    public function anggota()
    {
        return $this->hasOne(Anggota::class, 'npa', 'npa');
    }

    public function file()
    {
        return $this->hasMany(RekapCaangFile::class, "rekap_caang_id", "id");
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
