<?php

namespace App\Model;

use App\Facades\AnaOnline;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EsyahadahTafiq2 extends Model
{
    protected $table = 't_esyahadah_tafiq2';

    protected $fillable = [
        'year', 'counter', 'id_tafiq2'
    ];

    public function tafiq2()
    {
        return $this->hasOne(Tafiq2::class, 'id', 'id_tafiq2');
    }

    public function getFormattedTanggalTafiqAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->tafiq2->tanggal_tafiq_start);
    }

    public function getFormattedTanggalLahirAttribute()
    {
        if(!isset($this->tafiq2->anggota)) return "-";
        return Carbon::createFromFormat(
            'Y-m-d H:i:s',
            $this->tafiq2->anggota->tanggal_lahir)->format('d F Y');
    }

    public function getPDF($id, $mode = 'stream')
    {
        $esyahadah = $this->with([
            'tafiq2',
            'tafiq2.anggota',
            'tafiq2.anggota.pimpinanCabang',
            'tafiq2.anggota.pimpinanDaerah',
            'tafiq2.anggota.pimpinanWilayah',
            'tafiq2.anggota.village'
        ])->findOrFail($id);

        $data = [
            'no_urut' => AnaOnline::generateNoEsyahadahTafiq2($esyahadah->no_urut, $esyahadah->bulan, $esyahadah->tahun),
            'nama' => "-",
            'tempat_tanggal_lahir' => "-",
            'pc' => "-",
            'tanggal_maruf' => "{$esyahadah->formatted_tanggal_tafiq->format('d F Y') }",
            'tempat_maruf' => "{$esyahadah->tafiq2->lokasi}",
            'kehadiran' => "{$esyahadah->tafiq2->kehadiran}",
            'kehadiran_predikat' => AnaOnline::cekPredikatFromValue($esyahadah->tafiq2->kehadiran),
            'keaktifan' => "{$esyahadah->tafiq2->keaktifan}",
            'keaktifan_predikat' => AnaOnline::cekPredikatFromValue($esyahadah->tafiq2->keaktifan),
            'makalah' => "{$esyahadah->tafiq2->makalah}",
            'makalah_predikat' => AnaOnline::cekPredikatFromValue($esyahadah->tafiq2->makalah),
            'presentasi_makalah' => "{$esyahadah->tafiq2->presentasi_makalah}",
            'presentasi_makalah_predikat' => AnaOnline::cekPredikatFromValue($esyahadah->tafiq2->presentasi_makalah),
            'total' => $esyahadah->tafiq2->nilai_total,
            'total_predikat' => AnaOnline::cekPredikatFromValue(intval($esyahadah->tafiq2->nilai_total)),
            'foto' => "-",
            'pw' => null,
            'pd' => null,
            'namaDesa' => null,
            'path_image' => env('S3_STATIC_FOLDER', "https://kominfo-ana-online-dev2.s3-ap-southeast-1.amazonaws.com/static/"),
            'filename' => "TAFIQ_2.png"
        ];

        $finalData = optional($esyahadah->tafiq2->anggota, function($anggota) use ($data, $esyahadah){
            $data['nama'] = $anggota->nama_lengkap;
            $data['tempat_tanggal_lahir'] = "{$anggota->tempat_lahir}, {$esyahadah->formatted_tanggal_lahir}";

            $data['pc'] = optional($anggota->pimpinanCabang, function($pc){
                return $pc->nama_pc;
            });

            $data['pw'] = optional($anggota->pimpinanWilayah, function($pw){
                return $pw->nama_pw;
            });

            $data['pd'] = optional($anggota->pimpinanDaerah, function($pd){
                return $pd->nama_pd;
            });

            $data['namaDesa'] = optional($anggota->village, function($desa){
                return $desa->nama;
            });

            $data['foto'] = AnaOnline::getUrlFotoAnggota($anggota);

            return $data;

        });

        $data = $finalData ?? $data;
        $pdf = app()->make('dompdf.wrapper');

        if($mode === "html") return view('layouts.esyahadah_tafiq2', $data);
        $pdf->loadView('layouts.esyahadah_tafiq2', $data)->setPaper('a4', 'landscape');

        if($mode === "stream") return $pdf->stream();
        if($mode === "output") return $pdf->output();
        return "blank";
    }
}
