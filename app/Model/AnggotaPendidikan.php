<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AnggotaPendidikan extends Model
{
    use SoftDeletes, LogsActivity;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'AnggotaPendidikan';

    protected $casts = [
        'deleted_at' => 'date',
    ];

    protected $table = 't_anggota_pendidikan';

    protected $primaryKey = 'id_pendidikan';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'id_anggota', 'id_master_pendidikan', 'id_pendidikan', 'instansi', 'jurusan', 'jenis_pendidikan', 'tahun_masuk', 'tahun_keluar'
    ];

    public function anggota()
    {
        return $this->belongsTo('App\Model\Anggota', 'id_anggota');
    }

    public function masterPendidikan()
    {
        return $this->belongsTo('App\Model\MasterPendidikan', 'id_master_pendidikan', 'id_tingkat_pendidikan');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
