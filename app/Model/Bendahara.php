<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;


class Bendahara extends Model
{
    use HasFactory, LogsActivity;
    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 't_bendahara';
    protected $table = 't_bendahara';
    protected $fillable = [
        'nama',
        'jumlah',
        'status'
    ];

    public function anggota()
    {
        return $this->hasMany('App\Model\Anggota', 'pc', 'kd_pc');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
