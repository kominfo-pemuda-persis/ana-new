<?php


namespace App\Model;

use App\Traits\Blameable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MonografiPw extends Model
{
    use SoftDeletes, LogsActivity, Blameable;

    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 'MonografiPW';

    protected $table = 't_monografi_pw';
    protected $dates = ['deleted_at'];

    public function pw()
    {
        return $this->belongsTo(PW::class, 'kd_pw', 'kd_pw');
    }

    public function provinsi()
    {
        return $this->hasOne(Province::class, "id", "provinsi");
    }

    public function kejamiyyahan()
    {
        return $this->hasOne(KejamiyyahanPW::class, "kd_monografi_pw", "id");
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
