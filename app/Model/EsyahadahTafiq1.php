<?php

namespace App\Model;

use App\Facades\AnaOnline;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EsyahadahTafiq1 extends Model
{
    protected $table = 't_esyahadah_tafiq1';

    protected $fillable = [
        'year', 'counter', 'id_tafiq1'
    ];

    public function tafiq1()
    {
        return $this->hasOne(Tafiq1::class, 'id', 'id_tafiq1');
    }

    public function anggota()
    {
        return $this->hasOne(Anggota::class, "npa", "npa");
    }

    public function getFormattedTanggalTafiqAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->tafiq1->tanggal_tafiq_start);
    }

    public function getFormattedTanggalLahirAttribute()
    {
        if(!isset($this->tafiq1->anggota)) return "-";
        return Carbon::createFromFormat(
            'Y-m-d H:i:s',
            $this->tafiq1->anggota->tanggal_lahir)->format('d F Y');
    }

    public function getPDF($id, $mode = 'stream')
    {
        $esyahadah = $this->with([
            'tafiq1',
            'anggota.nama_lengkap',
            'anggota.email',
            'anggota.no_telpon',
            'tafiq1.anggota.pimpinanCabang',
            'tafiq1.anggota.pimpinanDaerah',
            'tafiq1.anggota.pimpinanWilayah',
            'tafiq1.anggota.village'
        ])->findOrFail($id);

        $data = [
            'no_urut' => AnaOnline::generateNoEsyahadahTafiq1($esyahadah->no_urut, $esyahadah->bulan, $esyahadah->tahun),
            'nama' => "-",
            'tempat_tanggal_lahir' => "-",
            'pc' => "-",
            'tanggal_maruf' => "{$esyahadah->formatted_tanggal_tafiq->format('d F Y') }",
            'tempat_maruf' => "{$esyahadah->tafiq1->lokasi}",
            'kehadiran' => "{$esyahadah->tafiq1->kehadiran}",
            'kehadiran_predikat' => AnaOnline::cekPredikatFromValue($esyahadah->tafiq1->kehadiran),
            'keaktifan' => "{$esyahadah->tafiq1->keaktifan}",
            'keaktifan_predikat' => AnaOnline::cekPredikatFromValue($esyahadah->tafiq1->keaktifan),
            'makalah' => "{$esyahadah->tafiq1->makalah}",
            'makalah_predikat' => AnaOnline::cekPredikatFromValue($esyahadah->tafiq1->makalah),
            'total' => $esyahadah->tafiq1->nilai_total,
            'total_predikat' => AnaOnline::cekPredikatFromValue(intval($esyahadah->tafiq1->nilai_total)),
            'foto' => "-",
            'pw' => null,
            'pd' => null,
            'namaDesa' => null,
            'path_image' => env('S3_STATIC_FOLDER', "https://kominfo-ana-online-dev2.s3-ap-southeast-1.amazonaws.com/static/"),
            'filename' => "TAFIQ_1.png"
        ];

        $finalData = optional($esyahadah->tafiq1->anggota, function($anggota) use ($data, $esyahadah){
            $data['nama'] = $anggota->nama_lengkap;
            $data['tempat_tanggal_lahir'] = "{$anggota->tempat_lahir}, {$esyahadah->formatted_tanggal_lahir}";

            $data['pc'] = optional($anggota->pimpinanCabang, function($pc){
                return $pc->nama_pc;
            });

            $data['pw'] = optional($anggota->pimpinanWilayah, function($pw){
                return $pw->nama_pw;
            });

            $data['pd'] = optional($anggota->pimpinanDaerah, function($pd){
                return $pd->nama_pd;
            });

            $data['namaDesa'] = optional($anggota->village, function($desa){
                return $desa->nama;
            });

            $data['foto'] = AnaOnline::getUrlFotoAnggota($anggota);

            return $data;

        });

        $data = $finalData ?? $data;
        $pdf = app()->make('dompdf.wrapper');

        if($mode === "html") return view('layouts.esyahadah_tafiq1', $data);
        $pdf->loadView('layouts.esyahadah_tafiq1', $data)->setPaper('a4', 'landscape');

        if($mode === "stream") return $pdf->stream();
        if($mode === "output") return $pdf->output();
        return "blank";
    }
}
