<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class LogTransaksi extends Model
{
    use HasFactory, LogsActivity;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 't_log_transaksi';
    protected $table = 't_log_transaksi';
    protected $fillable = ["id_pc", "total", "total_setor", "id_tagihan",'jml_masuk', 'blm_setor', 'tahun', 'status'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
