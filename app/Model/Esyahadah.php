<?php

namespace App\Model;

use App\Facades\AnaOnline;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Mail\EsyahadahMail;
use App\Model\Anggota;
use Illuminate\Support\Facades\Storage;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Illuminate\Support\Facades\Mail;
use Exception;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Illuminate\Support\Facades\DB;
class Esyahadah extends Model
{
    protected $table = 't_esyahadah_maruf';

    public function anggota()
    {
        return $this->hasOne(Anggota::class, 'id_anggota', 'id_anggota');
    }

    public function getFormattedTanggalMarufAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->tanggal_maruf);
    }

    public function getFormattedTanggalLahirAttribute()
    {
        if(!isset($this->anggota)) return "-";
        return Carbon::createFromFormat(
            'Y-m-d H:i:s',
            $this->anggota->tanggal_lahir)->format('d F Y');
    }

    public function sendEmailEsyahadah($mode = 'upload', $path = "", $csv = [])
    {
        $data = [];
        $data["last_no_urut"] = DB::table("t_temp_last_no_urut_esyahadah")
            ->get()->take(1);


        $data['csv'] = collect();
        $data['mail'] = collect();
        $data['counter'] = 0;
        $data['id_anggota'] = null;

        if($mode === "upload"){
            $reader = ReaderEntityFactory::createCSVReader();
            $reader->open($path);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $index => $row) {
                    if($index > 1){
                        $data['csv']->add([
                            'npa' => $row->getCellAtIndex(1)->getValue(),
                            'tempat_maruf' => $row->getCellAtIndex(2)->getValue(),
                            'tanggal_maruf' => Carbon::createFromFormat("dmY", $row->getCellAtIndex(3)->getValue())
                        ]);

                    }
                }
            }

        }elseif($mode === "system"){
            $data['csv'] = $csv;
        }

        try{

            if ($data["last_no_urut"]->isEmpty()) {
                DB::table("t_temp_last_no_urut_esyahadah")->insert([
                    "year" => date("Y"),
                    "counter" => 0,
                ]);
                $data["last_no_urut"] = DB::table("t_temp_last_no_urut_esyahadah")
                    ->get()
                    ->take(1);
            }

            $status = DB::transaction(function () use (&$data) {
                $last_no_urut = $data["last_no_urut"]->first();
                $data['counter'] = intval($last_no_urut->counter);
                $data['csv']->each(function ($value) use(&$data){
                    $data['anggota'] = Anggota::where('npa', $value['npa'])->first();
                    $esyahadah = new Esyahadah();
                    optional($data['anggota'], function ($item) use (&$esyahadah, $value, &$data){
                        $data['counter'] = $data['counter'] + 1;
                        $data['id_anggota'] = $item->id_anggota;
                        $esyahadah->id_anggota = $item->id_anggota;
                        $esyahadah->tahun = $value['tanggal_maruf']->year;
                        $esyahadah->bulan = $value['tanggal_maruf']->month;
                        $esyahadah->no_urut = $data['counter'];
                        $esyahadah->tempat_maruf = $value['tempat_maruf'];
                        $esyahadah->tanggal_maruf = $value['tanggal_maruf'];
                        $esyahadah->created_at = Carbon::now();
                        $esyahadah->updated_at = Carbon::now();
                        $esyahadah->save();
                        $link = "esyahadah/maruf/" . $item->npa . ".pdf";
                        Storage::disk('s3')->put($link, $this->savePDF($esyahadah->id, 'output'));
                        $data['mail']->add([
                            'npa' => $item->npa,
                            'name' => $item->nama_lengkap,
                            'email' => $item->email,
                            'link' => Storage::disk('s3')->url($link)
                        ]);
                    });
                });

                if(is_null($data['id_anggota'])) throw new Exception(
                    "NPA yang dapat di-generate hanya NPA yang berawal 21.xxxx & 22.xxxx", 400);

                DB::table("t_temp_last_no_urut_esyahadah")
                        ->where("id", $last_no_urut->id)
                        ->update([
                            "counter" => $data['counter'],
                            "year" => date('Y'),
                            "id_anggota" => $data['id_anggota']
                ]);

                $data['mail']->each(function ($value)
                {
                    Mail::to(trim($value['email']))->queue(
                        new EsyahadahMail(
                            $value['npa'],
                            $value['name'],
                            $value['link']));
                });

                return true;

            });

            if ($status) {
                return response()->json(
                    [
                        "message" => "success",
                        "error" => false,
                        "code" => ResponseAlias::HTTP_ACCEPTED,
                    ],
                    ResponseAlias::HTTP_ACCEPTED
                );
            } else {
                throw new Exception("Error Generaring Esyahadah Data");
            }

        } catch (Exception $e) {
            return response()->json(
                [
                    "message" => $e->getMessage(),
                    "error" => true,
                    "code" => ResponseAlias::HTTP_INTERNAL_SERVER_ERROR,
                ],
                ResponseAlias::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function savePDF($id, $mode = 'stream')
    {
        $esyahadah = Esyahadah::with(
            [
                'anggota',
                'anggota.pimpinanCabang',
                'anggota.pimpinanDaerah',
                'anggota.pimpinanWilayah',
                'anggota.village'
            ])->findOrFail($id);

        $data = [
           'no_urut' => AnaOnline::generateNoEsyahadah($esyahadah->no_urut, $esyahadah->bulan, $esyahadah->tahun),
           'nama' => "-",
           'tempat_tanggal_lahir' => "-",
           'pc' => "-",
           'tanggal_maruf' => "{$esyahadah->formatted_tanggal_maruf->format('d F Y') }",
           'tempat_maruf' => "{$esyahadah->tempat_maruf}",
           'foto' => "-",
           'pc_user' => null,
           'pw' => null,
           'pd' => null,
           'namaDesa' => null,
           'path_image' => env('S3_STATIC_FOLDER', "https://kominfo-ana-online-dev2.s3-ap-southeast-1.amazonaws.com/static/"),
           'filename' => "esyahadah.jpeg"
        ];

        $finalData = optional($esyahadah->anggota, function($anggota) use ($data, $esyahadah){
            $data['nama'] = $anggota->nama_lengkap;
            $data['tempat_tanggal_lahir'] = "{$anggota->tempat_lahir}, {$esyahadah->formatted_tanggal_lahir}";

            $data['pc'] = optional($anggota->pimpinanCabang, function($pc){
                return $pc->nama_pc;
            });

            $data['pw'] = optional($anggota->pimpinanWilayah, function($pw){
                return $pw->nama_pw;
            });

            $data['pd'] = optional($anggota->pimpinanDaerah, function($pd){
                return $pd->nama_pd;
            });

            $data['namaDesa'] = optional($anggota->village, function($desa){
                return $desa->nama;
            });

            if($anggota->foto === "default.png" ){
                $data['foto'] = "https://dev.anaonline.id/images/anggota/default.png";
            }else if (is_null($anggota->nama_pj)) {
                $data['foto'] = Storage::disk('s3')->url('images/anggota/' . $anggota->foto);
            } else if(!is_null($anggota->nama_pj)) {
                $data['foto'] = Storage::disk('s3')->url('images/anggota/' . $anggota->foto);
            }

            return $data;

        });

        $data = $finalData ?? $data;
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadView('layouts.esyahadah', $data)->setPaper('a4', 'landscape');

        if($mode === "stream") return $pdf->stream();
        if($mode === "output") return $pdf->output();
        return "blank";

    }
}
