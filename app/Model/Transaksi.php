<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Transaksi extends Model
{
    use HasFactory, LogsActivity;
    protected static $logAttributes = true;
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logName = 't_transaksi';
    protected $table = 't_transaksi';
    protected $fillable = ["npa", "id_tagihan", "tanggal",
        "pay", "bulan_id", "paid", "keterangan", "temp_tagihan_id", "temp_status","status","temp_paid"
    ];

    public function anggota()
    {
        return $this->belongsTo(Anggota::class,'npa','npa');
    }

    public function tagihan()
    {
        return $this->belongsTo(Bendahara::class, "id_tagihan", "id");
    }

    public function bulan()
    {
        return $this->belongsTo(Bulan::class, 'bulan_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()->logOnly(['name', 'text']);
    }
}
